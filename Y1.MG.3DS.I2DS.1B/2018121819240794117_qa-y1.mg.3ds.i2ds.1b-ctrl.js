
module.exports = [
  {
    "#type": "question",
    "name": "Y1.MG.3DS.I2DS.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y1.MG.3DS.I2DS.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "range < 0"
        },
        "then": [
          {
            "#type": "variable",
            "name": "choise",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "4"
              },
              "max": {
                "#type": "expression",
                "value": "7"
              }
            }
          },
          {
            "#type": "variable",
            "name": "max",
            "value": {
              "#type": "expression",
              "value": "2"
            }
          },
          {
            "#type": "variable",
            "name": "num",
            "value": {
              "#type": "expression",
              "value": "4"
            }
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "range < 4"
            },
            "then": [
              {
                "#type": "variable",
                "name": "choise",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "8"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "10"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "max",
                "value": {
                  "#type": "expression",
                  "value": "4"
                }
              },
              {
                "#type": "variable",
                "name": "num",
                "value": {
                  "#type": "expression",
                  "value": "4"
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "choise",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "11"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "13"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "max",
                "value": {
                  "#type": "expression",
                  "value": "5"
                }
              },
              {
                "#type": "variable",
                "name": "num",
                "value": {
                  "#type": "expression",
                  "value": "3"
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "shapes",
        "value": {
          "#type": "string_array",
          "items": "Sphere|Cones|Cubes|Cylinders|Triangular Prism|Rectangular Prism"
        }
      },
      {
        "#type": "variable",
        "name": "sh",
        "value": {
          "#type": "random_many",
          "count": {
            "#type": "expression",
            "value": "4"
          },
          "items": {
            "#type": "expression",
            "value": "[0, 1, 2, 3, 4, 5]"
          }
        }
      },
      {
        "#type": "statement",
        "value": "function count_x (x, A, num) {\n    var count = 0;\n    for(var i = 0; i< num; i++){\n        if(A[i] == x){count++;}\n    }\n    return count;\n}"
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "choise > 10"
        },
        "then": [
          {
            "#type": "variable",
            "name": "sh",
            "value": {
              "#type": "random_many",
              "count": {
                "#type": "expression",
                "value": "3"
              },
              "items": {
                "#type": "expression",
                "value": "[0, 1, 2, 3, 4, 5]"
              }
            }
          },
          {
            "#type": "variable",
            "name": "answer",
            "value": {
              "#type": "expression",
              "value": "[]"
            }
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < choise"
            },
            "do": [
              {
                "#type": "if_then_else_block",
                "if": {
                  "#type": "expression",
                  "value": "i < 3"
                },
                "then": [
                  {
                    "#type": "variable",
                    "name": "answer[i]",
                    "value": {
                      "#type": "expression",
                      "value": "sh[i]"
                    }
                  }
                ],
                "else": [
                  {
                    "#type": "do_while_block",
                    "do": [
                      {
                        "#type": "variable",
                        "name": "answer[i]",
                        "value": {
                          "#type": "random_one",
                          "items": {
                            "#type": "expression",
                            "value": "sh"
                          }
                        }
                      }
                    ],
                    "while": {
                      "#type": "expression",
                      "value": "count_x(answer[i], answer, i+1) > max"
                    }
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "drop",
            "value": {
              "#type": "expression",
              "value": "[]"
            }
          },
          {
            "#type": "variable",
            "name": "type",
            "value": {
              "#type": "expression",
              "value": "[]"
            }
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < 3"
            },
            "do": [
              {
                "#type": "variable",
                "name": "type[i]",
                "value": {
                  "#type": "random_many",
                  "count": {
                    "#type": "expression",
                    "value": "count_x(sh[i], answer, choise)"
                  },
                  "items": {
                    "#type": "expression",
                    "value": "[0, 1, 2, 3, 4, 5]"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "count",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "variable",
                "name": "drop[i]",
                "value": {
                  "#type": "expression",
                  "value": "[]"
                }
              },
              {
                "#type": "variable",
                "name": "j",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "j < choise"
                },
                "do": [
                  {
                    "#type": "if_then_block",
                    "if": {
                      "#type": "expression",
                      "value": "answer[j] == sh[i]"
                    },
                    "then": [
                      {
                        "#type": "variable",
                        "name": "answer[j]",
                        "value": {
                          "#type": "expression",
                          "value": "answer[j] + '_' + type[i][count]"
                        }
                      },
                      {
                        "#type": "variable",
                        "name": "drop[i][count]",
                        "value": {
                          "#type": "expression",
                          "value": "answer[j]"
                        }
                      },
                      {
                        "#type": "variable",
                        "name": "count",
                        "value": {
                          "#type": "expression",
                          "value": "count +1"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "j",
                    "value": {
                      "#type": "expression",
                      "value": "j+1"
                    }
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.2"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "Drag and drop the objects to the 3D shapes."
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "32"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "2"
                },
                "cols": {
                  "#type": "expression",
                  "value": "2"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "240"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "780"
                },
                "height": {
                  "#type": "expression",
                  "value": "420"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "func",
                  "name": "arrayOfNumber",
                  "args": [
                    {
                      "#type": "expression",
                      "value": "4"
                    },
                    {
                      "#type": "expression",
                      "value": "0"
                    }
                  ]
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template_for",
                "variable": "$cell",
                "condition": "$cell.data < 3",
                "#prop": "cell.templates[]",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.bottom"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "$cell.width - 10"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "$cell.height - 10"
                        }
                      },
                      {
                        "#type": "prop_anchor",
                        "#prop": "anchor",
                        "x": {
                          "#type": "expression",
                          "value": "0.5"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "1"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "shape.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}shape.png"
                      }
                    ]
                  },
                  {
                    "#type": "text_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY - 105"
                        }
                      },
                      {
                        "#type": "prop_anchor",
                        "#prop": "anchor",
                        "x": {
                          "#type": "expression",
                          "value": "0.5"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "0"
                        }
                      },
                      {
                        "#type": "prop_text_contents",
                        "#prop": "",
                        "contents": "${shapes[sh[$cell.data]]}"
                      },
                      {
                        "#prop": "",
                        "style": {
                          "#type": "json",
                          "base": "text",
                          "#props": [
                            {
                              "#type": "prop_text_style_font_size",
                              "#prop": "",
                              "fontSize": {
                                "#type": "expression",
                                "value": "28"
                              }
                            },
                            {
                              "#type": "prop_text_style_fill",
                              "#prop": "",
                              "fill": "black"
                            },
                            {
                              "#type": "prop_text_style_stroke",
                              "#prop": "",
                              "stroke": "white"
                            },
                            {
                              "#type": "prop_text_style_stroke_thickness",
                              "#prop": "",
                              "strokeThickness": {
                                "#type": "expression",
                                "value": "2"
                              }
                            }
                          ]
                        }
                      }
                    ]
                  },
                  {
                    "#type": "drop_shape",
                    "multiple": true,
                    "resultMode": "default",
                    "groupName": "",
                    "#props": [
                      {
                        "#type": "prop_value",
                        "#prop": "",
                        "value": {
                          "#type": "expression",
                          "value": "drop[$cell.data]"
                        }
                      },
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.bottom - 10"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "$cell.width - 30"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "$cell.height- 40"
                        }
                      },
                      {
                        "#type": "prop_anchor",
                        "#prop": "anchor",
                        "x": {
                          "#type": "expression",
                          "value": "0.5"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "1"
                        }
                      },
                      {
                        "#type": "prop_spacing",
                        "#prop": "",
                        "spacing": {
                          "#type": "expression",
                          "value": "15"
                        }
                      },
                      {
                        "#type": "prop_scale",
                        "#prop": "",
                        "scale": {
                          "#type": "expression",
                          "value": "0.5"
                        }
                      }
                    ]
                  }
                ]
              },
              {
                "#type": "prop_grid_cell_template_for",
                "variable": "$cell",
                "condition": "$cell.data == 3",
                "#prop": "cell.templates[]",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.bottom"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "$cell.width - 10"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "$cell.height- 10"
                        }
                      },
                      {
                        "#type": "prop_anchor",
                        "#prop": "anchor",
                        "x": {
                          "#type": "expression",
                          "value": "0.5"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "1"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "shape.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}shape.png"
                      }
                    ]
                  },
                  {
                    "#type": "grid_shape",
                    "#props": [
                      {
                        "#type": "prop_grid_dimension",
                        "#prop": "",
                        "rows": {
                          "#type": "expression",
                          "value": "3"
                        },
                        "cols": {
                          "#type": "expression",
                          "value": "5"
                        }
                      },
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.bottom-10"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "$cell.width - 30"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "$cell.height- 30"
                        }
                      },
                      {
                        "#type": "prop_anchor",
                        "#prop": "anchor",
                        "x": {
                          "#type": "expression",
                          "value": "0.5"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "1"
                        }
                      },
                      {
                        "#type": "prop_grid_cell_source",
                        "#prop": "cell.source",
                        "value": {
                          "#type": "expression",
                          "value": "answer"
                        }
                      },
                      {
                        "#type": "prop_grid_random",
                        "#prop": "",
                        "random": false
                      },
                      {
                        "#type": "prop_grid_show_borders",
                        "#prop": "#showBorders",
                        "value": false
                      },
                      {
                        "#type": "prop_grid_cell_template",
                        "variable": "$cell",
                        "#prop": "cell.template",
                        "#callback": "$cell",
                        "body": [
                          {
                            "#type": "choice_custom_shape",
                            "action": "drag",
                            "value": {
                              "#type": "expression",
                              "value": "$cell.data"
                            },
                            "template": {
                              "#callback": "$choice",
                              "variable": "$choice",
                              "body": [
                                {
                                  "#type": "image_shape",
                                  "#props": [
                                    {
                                      "#type": "prop_position",
                                      "#prop": "",
                                      "x": {
                                        "#type": "expression",
                                        "value": "$cell.centerX"
                                      },
                                      "y": {
                                        "#type": "expression",
                                        "value": "$cell.centerY"
                                      }
                                    },
                                    {
                                      "#type": "prop_max_size",
                                      "#prop": "",
                                      "maxWidth": {
                                        "#type": "expression",
                                        "value": "45"
                                      },
                                      "maxHeight": {
                                        "#type": "expression",
                                        "value": "45"
                                      }
                                    },
                                    {
                                      "#type": "prop_image_key",
                                      "#prop": "",
                                      "key": "${$cell.data}.png"
                                    },
                                    {
                                      "#type": "prop_image_src",
                                      "#prop": "",
                                      "src": "${image_path}${$cell.data}.png"
                                    }
                                  ]
                                },
                                {
                                  "#type": "image_shape",
                                  "#props": [
                                    {
                                      "#type": "prop_position",
                                      "#prop": "",
                                      "x": {
                                        "#type": "expression",
                                        "value": "$cell.centerX"
                                      },
                                      "y": {
                                        "#type": "expression",
                                        "value": "$cell.centerY"
                                      }
                                    },
                                    {
                                      "#type": "prop_image_key",
                                      "#prop": "",
                                      "key": "boxans.png"
                                    },
                                    {
                                      "#type": "prop_image_src",
                                      "#prop": "",
                                      "src": "${image_path}boxans.png"
                                    }
                                  ]
                                }
                              ]
                            }
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "choise < 11"
        },
        "then": [
          {
            "#type": "variable",
            "name": "answer",
            "value": {
              "#type": "expression",
              "value": "[]"
            }
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < choise"
            },
            "do": [
              {
                "#type": "if_then_else_block",
                "if": {
                  "#type": "expression",
                  "value": "i < 4"
                },
                "then": [
                  {
                    "#type": "variable",
                    "name": "answer[i]",
                    "value": {
                      "#type": "expression",
                      "value": "sh[i]"
                    }
                  }
                ],
                "else": [
                  {
                    "#type": "do_while_block",
                    "do": [
                      {
                        "#type": "variable",
                        "name": "answer[i]",
                        "value": {
                          "#type": "random_one",
                          "items": {
                            "#type": "expression",
                            "value": "sh"
                          }
                        }
                      }
                    ],
                    "while": {
                      "#type": "expression",
                      "value": "count_x(answer[i], answer, i+1) > max"
                    }
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "drop",
            "value": {
              "#type": "expression",
              "value": "[]"
            }
          },
          {
            "#type": "variable",
            "name": "type",
            "value": {
              "#type": "expression",
              "value": "[]"
            }
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < 4"
            },
            "do": [
              {
                "#type": "variable",
                "name": "type[i]",
                "value": {
                  "#type": "random_many",
                  "count": {
                    "#type": "expression",
                    "value": "count_x(sh[i], answer, choise)"
                  },
                  "items": {
                    "#type": "expression",
                    "value": "[0, 1, 2, 3, 4, 5]"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "count",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "variable",
                "name": "drop[i]",
                "value": {
                  "#type": "expression",
                  "value": "[]"
                }
              },
              {
                "#type": "variable",
                "name": "j",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "j < choise"
                },
                "do": [
                  {
                    "#type": "if_then_block",
                    "if": {
                      "#type": "expression",
                      "value": "answer[j] == sh[i]"
                    },
                    "then": [
                      {
                        "#type": "variable",
                        "name": "answer[j]",
                        "value": {
                          "#type": "expression",
                          "value": "answer[j] + '_' + type[i][count]"
                        }
                      },
                      {
                        "#type": "variable",
                        "name": "drop[i][count]",
                        "value": {
                          "#type": "expression",
                          "value": "answer[j]"
                        }
                      },
                      {
                        "#type": "variable",
                        "name": "count",
                        "value": {
                          "#type": "expression",
                          "value": "count +1"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "j",
                    "value": {
                      "#type": "expression",
                      "value": "j+1"
                    }
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "Drag and drop the objects to the 3D shapes."
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "36"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "2"
                },
                "cols": {
                  "#type": "expression",
                  "value": "2"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "190"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "750"
                },
                "height": {
                  "#type": "expression",
                  "value": "300"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "func",
                  "name": "arrayOfNumber",
                  "args": [
                    {
                      "#type": "expression",
                      "value": "4"
                    },
                    {
                      "#type": "expression",
                      "value": "0"
                    }
                  ]
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "0"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "150"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "shape.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}shape.png"
                      }
                    ]
                  },
                  {
                    "#type": "text_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY - 68"
                        }
                      },
                      {
                        "#type": "prop_anchor",
                        "#prop": "anchor",
                        "x": {
                          "#type": "expression",
                          "value": "0.5"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "0"
                        }
                      },
                      {
                        "#type": "prop_text_contents",
                        "#prop": "",
                        "contents": "${shapes[sh[$cell.data]]}"
                      },
                      {
                        "#prop": "",
                        "style": {
                          "#type": "json",
                          "base": "text",
                          "#props": [
                            {
                              "#type": "prop_text_style_font_size",
                              "#prop": "",
                              "fontSize": {
                                "#type": "expression",
                                "value": "28"
                              }
                            },
                            {
                              "#type": "prop_text_style_fill",
                              "#prop": "",
                              "fill": "black"
                            },
                            {
                              "#type": "prop_text_style_stroke",
                              "#prop": "",
                              "stroke": "white"
                            },
                            {
                              "#type": "prop_text_style_stroke_thickness",
                              "#prop": "",
                              "strokeThickness": {
                                "#type": "expression",
                                "value": "2"
                              }
                            }
                          ]
                        }
                      }
                    ]
                  },
                  {
                    "#type": "drop_shape",
                    "multiple": true,
                    "resultMode": "default",
                    "groupName": "",
                    "#props": [
                      {
                        "#type": "prop_value",
                        "#prop": "",
                        "value": {
                          "#type": "expression",
                          "value": "drop[$cell.data]"
                        }
                      },
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 13"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "330"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "100"
                        }
                      },
                      {
                        "#type": "prop_spacing",
                        "#prop": "",
                        "spacing": {
                          "#type": "expression",
                          "value": "15"
                        }
                      },
                      {
                        "#type": "prop_scale",
                        "#prop": "",
                        "scale": {
                          "#type": "expression",
                          "value": "0.5"
                        }
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "390"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "0"
                },
                "height": {
                  "#type": "expression",
                  "value": "90"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "box.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}box.png"
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "choise"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "390"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "720"
                },
                "height": {
                  "#type": "expression",
                  "value": "90"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "answer"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "choice_custom_shape",
                    "action": "drag",
                    "value": {
                      "#type": "expression",
                      "value": "$cell.data"
                    },
                    "template": {
                      "#callback": "$choice",
                      "variable": "$choice",
                      "body": [
                        {
                          "#type": "image_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "$cell.centerX"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "$cell.centerY"
                              }
                            },
                            {
                              "#type": "prop_max_size",
                              "#prop": "",
                              "maxWidth": {
                                "#type": "expression",
                                "value": "45"
                              },
                              "maxHeight": {
                                "#type": "expression",
                                "value": "45"
                              }
                            },
                            {
                              "#type": "prop_image_key",
                              "#prop": "",
                              "key": "${$cell.data}.png"
                            },
                            {
                              "#type": "prop_image_src",
                              "#prop": "",
                              "src": "${image_path}${$cell.data}.png"
                            }
                          ]
                        },
                        {
                          "#type": "image_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "$cell.centerX"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "$cell.centerY"
                              }
                            },
                            {
                              "#type": "prop_image_key",
                              "#prop": "",
                              "key": "boxans.png"
                            },
                            {
                              "#type": "prop_image_src",
                              "#prop": "",
                              "src": "${image_path}boxans.png"
                            }
                          ]
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "statement",
        "value": "function check(x, A, num) {\n    for(var m=0; m<num; m++){\n        if(x == A[m]){\n            return 1;\n        }\n    }\n    return 0;\n}"
      },
      {
        "#type": "variable",
        "name": "loadAssets",
        "value": {
          "#type": "custom_image_list",
          "link": "${image_path}",
          "images": "0_0|0_1|0_2|0_3|0_4|0_5|0_d|1_0|1_1|1_2|1_3|1_4|1_5|1_d|2_0|2_1|2_2|2_3|2_4|2_5|2_d|3_0|3_1|3_2|3_3|3_4|3_5|3_d|4_0|4_1|4_2|4_3|4_4|4_5|4_d|5_0|5_1|5_2|5_3|5_4|5_5|5_d"
        }
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<style>\n  table{background: rgb(150, 200, 250, 0.5); width: 900px}\ntd{padding: 10px; text-align: center; border: 1px solid black}\nimg{padding: 0 5px; margin: 0 10px; max-width: 80px; max-height: 80px; }\n  </style>\n<center>\n  <table>\n  <tr><td>3D Type</td><td>Looks Like</td></tr>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < num"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<tr><td><img src='@sprite.src(${sh[i]}_d)'/></br>${shapes[sh[i]]}</td><td>"
            ]
          },
          {
            "#type": "variable",
            "name": "j",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "j < 6"
            },
            "do": [
              {
                "#type": "if_then_block",
                "if": {
                  "#type": "expression",
                  "value": "check(j, type[i], type[i].length) == 0"
                },
                "then": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<img src='@sprite.src(${sh[i]}_${j})'/>"
                    ]
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "j",
                "value": {
                  "#type": "expression",
                  "value": "j+1"
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "j",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "j < 6"
            },
            "do": [
              {
                "#type": "if_then_block",
                "if": {
                  "#type": "expression",
                  "value": "check(j, type[i], type[i].length) == 1"
                },
                "then": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<img src='@sprite.src(${sh[i]}_${j})' style='border: 3px solid green; padding: 5px'/>"
                    ]
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "j",
                "value": {
                  "#type": "expression",
                  "value": "j+1"
                }
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</td></tr>"
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</table>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="0" y="0">
    <field name="name">Y1.MG.3DS.I2DS.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y1.MG.3DS.I2DS.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="YepMQT@2uwp2rRPvmnRg1">
                                    <field name="name">range</field>
                                    <value name="value">
                                      <block type="expression" id="UxgQ^Y5]rb}vpw[D=l-!">
                                        <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="if_then_else_block" id="GNA7W$)OL].UR2i^q|rr">
                                        <value name="if">
                                          <block type="expression" id="uKCmL{Nu4!lR](Qd|G_1">
                                            <field name="value">range &lt; 0</field>
                                          </block>
                                        </value>
                                        <statement name="then">
                                          <block type="variable" id="Os@1AgcIAUewSB-;@2~n?!">
                                            <field name="name">choise</field>
                                            <value name="value">
                                              <block type="random_number" id="6:CVJSS{b8R}GoY;)=y#">
                                                <value name="min">
                                                  <block type="expression" id="Tyq(m+zdD{H+Em-mgxGf">
                                                    <field name="value">4</field>
                                                  </block>
                                                </value>
                                                <value name="max">
                                                  <block type="expression" id="+~[zz{~V%,?aS~@1O4)7u">
                                                    <field name="value">7</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id="w7X1VAU4ePN@2ZhPh6+Kd">
                                                <field name="name">max</field>
                                                <value name="value">
                                                  <block type="expression" id="`pkiMtV/6]lP4c}ij?{a">
                                                    <field name="value">2</field>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="mc)e5|ZS}o=8}zfKnuuJ">
                                                    <field name="name">num</field>
                                                    <value name="value">
                                                      <block type="expression" id="4{%1IipNhuq$GFg^u$x2">
                                                        <field name="value">4</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </statement>
                                        <statement name="else">
                                          <block type="if_then_else_block" id="l7LWD]QCyCF;xMlW3FrA">
                                            <value name="if">
                                              <block type="expression" id="P=N44Qg()0f}egSg][z?">
                                                <field name="value">range &lt; 4</field>
                                              </block>
                                            </value>
                                            <statement name="then">
                                              <block type="variable" id="zSqUSz~m^UMePpwj7$9x">
                                                <field name="name">choise</field>
                                                <value name="value">
                                                  <block type="random_number" id="y@1EL-OQ~G]Z+ZY+JO@2v8">
                                                    <value name="min">
                                                      <block type="expression" id="Z28F?qapqyN)T1j@2C1a}">
                                                        <field name="value">8</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="/N{8EQD1)(_s|d|7O`#Q">
                                                        <field name="value">10</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="./c$9nEw6x{;B8Ryss9{">
                                                    <field name="name">max</field>
                                                    <value name="value">
                                                      <block type="expression" id="SG}pmI8?`?MK11Y+uk[o">
                                                        <field name="value">4</field>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id=":/28n7O4j44u`]uIV8v?">
                                                        <field name="name">num</field>
                                                        <value name="value">
                                                          <block type="expression" id="~h#]8-z:/e,3bN(8)Wb4">
                                                            <field name="value">4</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </statement>
                                            <statement name="else">
                                              <block type="variable" id="Sha{Uuy%;}iX2,RT,@1d6">
                                                <field name="name">choise</field>
                                                <value name="value">
                                                  <block type="random_number" id="5]BkrG4KBQa%XuREc0@2u">
                                                    <value name="min">
                                                      <block type="expression" id="+M@1e,#a~m]TMcwnd9ra!">
                                                        <field name="value">11</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="cWSsD;e%VH[hP8xJC?^c">
                                                        <field name="value">13</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="V;U32JXgo?K}JG24wHaL">
                                                    <field name="name">max</field>
                                                    <value name="value">
                                                      <block type="expression" id=",WK6,B,jWNbK^O@1G9-sz">
                                                        <field name="value">5</field>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id="Dbli.5+_1bOQv{xC5-+$">
                                                        <field name="name">num</field>
                                                        <value name="value">
                                                          <block type="expression" id="(w4#iOqpl3`h{hHN!C/A">
                                                            <field name="value">3</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </statement>
                                          </block>
                                        </statement>
                                        <next>
                                          <block type="variable" id="ki;LS%H2fvM/3[~,8KJt">
                                            <field name="name">shapes</field>
                                            <value name="value">
                                              <block type="string_array" id="g{u~2[53A3h6afdtRqDN">
                                                <field name="items">Sphere|Cones|Cubes|Cylinders|Triangular Prism|Rectangular Prism</field>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id="2=679|nqI!P-[Vva#Do2">
                                                <field name="name">sh</field>
                                                <value name="value">
                                                  <block type="random_many" id="HdA[!Yi0o~UPt)lY~!H#">
                                                    <value name="count">
                                                      <block type="expression" id="6u(eU81BFww0;[LKes+H">
                                                        <field name="value">4</field>
                                                      </block>
                                                    </value>
                                                    <value name="items">
                                                      <block type="expression" id=".ZW9VoiBm{lsyNkEn|1A">
                                                        <field name="value">[0, 1, 2, 3, 4, 5]</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="statement" id="3uKn`aR)jL(FyU/QJvu)">
                                                    <field name="value">function count_x (x, A, num) {
    var count = 0;
    for(var i = 0; i&lt; num; i++){
        if(A[i] == x){count++;}
    }
    return count;
}
                                                    </field>
                                                    <next>
                                                      <block type="if_then_block" id="RrI|2StJBExGwHQnM?x?">
                                                        <value name="if">
                                                          <block type="expression" id="9nF@2LH0-bR@1vK9^M/S+M">
                                                            <field name="value">choise &gt; 10</field>
                                                          </block>
                                                        </value>
                                                        <statement name="then">
                                                          <block type="variable" id="VB-%dLmS,:d`un=EEwM=">
                                                            <field name="name">sh</field>
                                                            <value name="value">
                                                              <block type="random_many" id="FJx!jZg,N8|qA$0?wQm4">
                                                                <value name="count">
                                                                  <block type="expression" id="?,jLv!?^7]^-jt]GaMC3">
                                                                    <field name="value">3</field>
                                                                  </block>
                                                                </value>
                                                                <value name="items">
                                                                  <block type="expression" id="=f3]?X!p:,+HPDz~(=Bs">
                                                                    <field name="value">[0, 1, 2, 3, 4, 5]</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="variable" id="X2=)_=@2u9tFuk$?{bg=|">
                                                                <field name="name">answer</field>
                                                                <value name="value">
                                                                  <block type="expression" id="aBY//-D|behG+)8]Sb+T">
                                                                    <field name="value">[]</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="variable" id="cB5UB)lp{P%TO]:(_tfZ">
                                                                    <field name="name">i</field>
                                                                    <value name="value">
                                                                      <block type="expression" id="]@16~2J.61S}`ap$4I-Zq">
                                                                        <field name="value">0</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="while_do_block" id="f]]kQ_.iw8jGOu_K{:OK">
                                                                        <value name="while">
                                                                          <block type="expression" id="@20g{q{!@2x-S3WP^kH@2ED">
                                                                            <field name="value">i &lt; choise</field>
                                                                          </block>
                                                                        </value>
                                                                        <statement name="do">
                                                                          <block type="if_then_else_block" id="An(cAm4HS=M9(NFU$`Ia">
                                                                            <value name="if">
                                                                              <block type="expression" id="@2Dr;Yyv_Vj#M8XWsJKiS">
                                                                                <field name="value">i &lt; 3</field>
                                                                              </block>
                                                                            </value>
                                                                            <statement name="then">
                                                                              <block type="variable" id="T]$S`6U;Fu@1,6L8{Y@1O,">
                                                                                <field name="name">answer[i]</field>
                                                                                <value name="value">
                                                                                  <block type="expression" id="FVK`XAxm};@16F@1kP/#xh">
                                                                                    <field name="value">sh[i]</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </statement>
                                                                            <statement name="else">
                                                                              <block type="do_while_block" id="U5fV[0#t2Ge-%Y~mHMoo">
                                                                                <statement name="do">
                                                                                  <block type="variable" id=".Ei%gJ^3$JUC@2mDMC:5Q">
                                                                                    <field name="name">answer[i]</field>
                                                                                    <value name="value">
                                                                                      <block type="random_one" id="Y%H7Ik#_-p6X]hhDPJ.6">
                                                                                        <value name="items">
                                                                                          <block type="expression" id="L--enm%4$W?^[HS$HE5V">
                                                                                            <field name="value">sh</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </statement>
                                                                                <value name="while">
                                                                                  <block type="expression" id="Yz(aa:84rDa{Nk!AYQ{z">
                                                                                    <field name="value">count_x(answer[i], answer, i+1) &gt; max</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="variable" id="|#LNSwn[K3PEugnx|,2y">
                                                                                <field name="name">i</field>
                                                                                <value name="value">
                                                                                  <block type="expression" id="w,#Kny4)KLehYcwIRn-e">
                                                                                    <field name="value">i+1</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="variable" id="9-aXomPa{C9]8oQ+Op;/">
                                                                            <field name="name">drop</field>
                                                                            <value name="value">
                                                                              <block type="expression" id="YWV]Z^-x3hgN:H?I`y?{">
                                                                                <field name="value">[]</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="variable" id="PZ`TjM(NG9b1/y4jC`7)">
                                                                                <field name="name">type</field>
                                                                                <value name="value">
                                                                                  <block type="expression" id="!YM6u2_m#@1d@14BU^syfn">
                                                                                    <field name="value">[]</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="variable" id="dxs;^ah0?@2m@2:R%/G:r{">
                                                                                    <field name="name">i</field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="Lce-?X@1)P3b_WKc#stUv">
                                                                                        <field name="value">0</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="while_do_block" id="-Ipxp#I7w^;ToNw`ydB}">
                                                                                        <value name="while">
                                                                                          <block type="expression" id="W/3~8~xDpka~hAO?It8x">
                                                                                            <field name="value">i &lt; 3</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <statement name="do">
                                                                                          <block type="variable" id="3@1@1w@2Uuikw.(+H[7bU.s">
                                                                                            <field name="name">type[i]</field>
                                                                                            <value name="value">
                                                                                              <block type="random_many" id="B9b3Ha-xc9zO-advOM=W">
                                                                                                <value name="count">
                                                                                                  <block type="expression" id="RtnEl/b|7=8H.=r/=2KW">
                                                                                                    <field name="value">count_x(sh[i], answer, choise)</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="items">
                                                                                                  <block type="expression" id="32VVVqOe[!u3@2ke9_NWp">
                                                                                                    <field name="value">[0, 1, 2, 3, 4, 5]</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="variable" id="P.gJLd+2p4o]7qy~gN,I">
                                                                                                <field name="name">count</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="Ry/+kN7EjjDt|[GCC`S`">
                                                                                                    <field name="value">0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="variable" id="LOlDV{Wx5PWlQQ4nL7Fo">
                                                                                                    <field name="name">drop[i]</field>
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="bunM7)=r,]xs;iI8{1;|">
                                                                                                        <field name="value">[]</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="variable" id="^|0.A{:pMIMsS)jdnk|8">
                                                                                                        <field name="name">j</field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id="ft!aJ3K/d]FJVG1;SR8]">
                                                                                                            <field name="value">0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="while_do_block" id="/ge(4v#3~SjV?ofgmcIV">
                                                                                                            <value name="while">
                                                                                                              <block type="expression" id="18$}+b=rSXtsWxScjYBT">
                                                                                                                <field name="value">j &lt; choise</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <statement name="do">
                                                                                                              <block type="if_then_block" id="5T$PtjJ74FJhf,$^s:,A">
                                                                                                                <value name="if">
                                                                                                                  <block type="expression" id="lWqGGV=F9O(bwk$BsoMg">
                                                                                                                    <field name="value">answer[j] == sh[i]</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <statement name="then">
                                                                                                                  <block type="variable" id="G23O$;r4Bjw/|!}M-)m#">
                                                                                                                    <field name="name">answer[j]</field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="~tvLk!!Pt7UX,LESYT.e">
                                                                                                                        <field name="value">answer[j] + '_' + type[i][count]</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="variable" id="Ite{K{AGP[FC4koYh/j6">
                                                                                                                        <field name="name">drop[i][count]</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id="hVvzs1x%#pfEJP9hn!RM">
                                                                                                                            <field name="value">answer[j]</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="variable" id="d$qaxip4hoWfy@101f5@2f">
                                                                                                                            <field name="name">count</field>
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="Dbd,xu8)$So^UUCb@1:qO">
                                                                                                                                <field name="value">count +1</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="variable" id="Wcpl`yovsjVY{Bq`fBi3">
                                                                                                                    <field name="name">j</field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="Q`AXkblK]zj^=6w^=F#x">
                                                                                                                        <field name="value">j+1</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="variable" id="$5J1aFv]%V---pEuf5xX">
                                                                                                                <field name="name">i</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="PVRfQ{[vanY~YoD%kEw%">
                                                                                                                    <field name="value">i+1</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="text_shape" id="dzMs42$!ig7e%U^/-J5q">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_position" id="7]l`?^$0hKd1DmDDw~DC">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="$@2(ElBo-8jZ%D%7Yy9}Y">
                                                                                                    <field name="value">400</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="FMT6@1WF()ExXJsHzV94K">
                                                                                                    <field name="value">0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_anchor" id="l,ZLZl|+kdoa1=P]GOA`">
                                                                                                    <field name="#prop">anchor</field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="04i.@2qn1:LJ]DV8dX;Af">
                                                                                                        <field name="value">0.5</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="xIq|C5i0^TaB}INC%|/V">
                                                                                                        <field name="value">0.2</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_contents" id="Sh%/wfR@1zz%#O7Mt2V:p">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="contents">
                                                                                                          <block type="string_value" id="8=nV.Sru+46ju/jMbEuq">
                                                                                                            <field name="value">Drag and drop the objects to the 3D shapes.</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style" id="R@2qZn[�TnjSg%yc4UQ">
                                                                                                            <field name="base">text</field>
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_text_style_font_size" id="tKe=)0:lI/pN)Xv_i7-E">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="fontSize">
                                                                                                                  <block type="expression" id="%_@2hQ%F~:(1-V`{yWF@23">
                                                                                                                    <field name="value">32</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_fill" id="Te|{`gb?UxPe3XjU?uL]">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="fill">
                                                                                                                      <block type="string_value" id="g@1c7oufU_j$w4;E7WrhY">
                                                                                                                        <field name="value">black</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_stroke" id="y(kPvJiHCkI80ikWBdYJ">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="stroke">
                                                                                                                          <block type="string_value" id="7,^c@1WD-A04,+NqV^gIT">
                                                                                                                            <field name="value">white</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_stroke_thickness" id="5GBh?#_zw)@1GS577Lb0/">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="strokeThickness">
                                                                                                                              <block type="expression" id="u,?KHrZdG];wMZ)EQ2BI">
                                                                                                                                <field name="value">2</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="grid_shape" id="aSP#76)2?in;g(BeMXHm">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_grid_dimension" id="t_5X2BYQhjrsanhg.0GF">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="rows">
                                                                                                      <block type="expression" id="cdu5?W=]g@1J9ewy|eHsd">
                                                                                                        <field name="value">2</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="cols">
                                                                                                      <block type="expression" id="f7sU]DJz/J{h13JG.}tH">
                                                                                                        <field name="value">2</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_position" id="w^Z-6|wZ]^8W]DqtnI,@2">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="x">
                                                                                                          <block type="expression" id="PVzo:u@23kw-at#pR^3R=">
                                                                                                            <field name="value">400</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id="[12PO,^EivxP%^s_i/7s">
                                                                                                            <field name="value">240</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_size" id="{C=neXb-=H-nvbKU.ox`">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="width">
                                                                                                              <block type="expression" id="GSIM-Wg-tXj4R|qqXRkF">
                                                                                                                <field name="value">780</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="height">
                                                                                                              <block type="expression" id="?tT[,KXS`4K5}PeOL.jv">
                                                                                                                <field name="value">420</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_anchor" id="q).PI!?R+gT7v9/qCZKW">
                                                                                                                <field name="#prop">anchor</field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="sBp5j)Ag6ZjvF]_a@2M2]">
                                                                                                                    <field name="value">0.5</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="l%4K^s.Cv^(L4[$]6e-`">
                                                                                                                    <field name="value">0.5</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_grid_cell_source" id="k1)f,%%I2mZ^lD%.,/3}">
                                                                                                                    <field name="#prop">cell.source</field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="func_array_of_number" id="XIOOfxn)%Vlf:|h$my!n" inline="true">
                                                                                                                        <value name="items">
                                                                                                                          <block type="expression" id="lncBx?HRgL~ldW7aB)Nu">
                                                                                                                            <field name="value">4</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="from">
                                                                                                                          <block type="expression" id="}nYAr5XhBYcgqW?RPw:n">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_grid_random" id="wAyfg+IZ.^ZDj$.@1sm,P">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <field name="random">FALSE</field>
                                                                                                                        <next>
                                                                                                                          <block type="prop_grid_show_borders" id="^RK,):h!}LYQl8[wZsvF">
                                                                                                                            <field name="#prop">#showBorders</field>
                                                                                                                            <field name="value">FALSE</field>
                                                                                                                            <next>
                                                                                                                              <block type="prop_grid_cell_template_for" id="lJJKzL9VtME3$T6dHZ_v">
                                                                                                                                <field name="variable">$cell</field>
                                                                                                                                <field name="condition">$cell.data &lt; 3</field>
                                                                                                                                <field name="#prop">cell.templates[]</field>
                                                                                                                                <field name="#callback">$cell</field>
                                                                                                                                <statement name="body">
                                                                                                                                  <block type="image_shape" id="8csER/3}6$icK(m:j]iV">
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_position" id="F9CPsdM`%@2Bbe+m;U=}%">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="expression" id="1hV.a.7P^g+@1})a.Xb7q">
                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id="]80`n@2IU7+YKI6U^,2u;">
                                                                                                                                            <field name="value">$cell.bottom</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_size" id="W0F~:v_)xWj[u|w%#RCu">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="width">
                                                                                                                                              <block type="expression" id="mZ4Mu5T@2(wZg,cow`Np;">
                                                                                                                                                <field name="value">$cell.width - 10</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="height">
                                                                                                                                              <block type="expression" id="ucOn5_iVM,:Fww8H@2H9?">
                                                                                                                                                <field name="value">$cell.height - 10</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_anchor" id="v@17,zNB2]#XE,8DZbx)U">
                                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                                <value name="x">
                                                                                                                                                  <block type="expression" id="4^/FRItc`w1wp/iMAztB">
                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <value name="y">
                                                                                                                                                  <block type="expression" id="$Eeu~OGc(|0W7f9}|.Ya">
                                                                                                                                                    <field name="value">1</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_image_key" id="rY`npo0rNbKWJXGPEM}f">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="key">
                                                                                                                                                      <block type="string_value" id="^?fzv@2b[:uH-ISAPR@25Y">
                                                                                                                                                        <field name="value">shape.png</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_image_src" id="]=R%6W!s6ml`120IZIN]">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="src">
                                                                                                                                                          <block type="string_value" id="iqEc[SXUQqnSyztNnS$K">
                                                                                                                                                            <field name="value">${image_path}shape.png</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="text_shape" id="Xb%q+YI%Qip~|EmT%~c8">
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_position" id="wNm/:q=6+GN4`Eut{QGt">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id="DbX@2;[oRL,XT7I_LwaC|">
                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="%_R80ZluYo(vS0`9$D$-">
                                                                                                                                                <field name="value">$cell.centerY - 105</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_anchor" id="k},RT22z3D7v.?,g.HhJ">
                                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                                <value name="x">
                                                                                                                                                  <block type="expression" id="?.YV+_Dwf1968}f|v!9#">
                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <value name="y">
                                                                                                                                                  <block type="expression" id="rpCN2tb@1t6C|[^DNrZ($">
                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_text_contents" id="/1?~@10ifHYD99+0XQ9HF">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="contents">
                                                                                                                                                      <block type="string_value" id=")?uNKR1o||P;_#d`S~+E">
                                                                                                                                                        <field name="value">${shapes[sh[$cell.data]]}</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_text_style" id=")[P@1J-hXEx`%;LBOhw]D">
                                                                                                                                                        <field name="base">text</field>
                                                                                                                                                        <statement name="#props">
                                                                                                                                                          <block type="prop_text_style_font_size" id="|P`MhtaF,d5br|z}E9C,">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="fontSize">
                                                                                                                                                              <block type="expression" id="^n~?Eb;4r]$O{c[Vm_ZD">
                                                                                                                                                                <field name="value">28</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_text_style_fill" id="j1j8_YE1!hfzCFc[I?0A">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="fill">
                                                                                                                                                                  <block type="string_value" id="]nI#ZrL[[(%1QRG(v?V5">
                                                                                                                                                                    <field name="value">black</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_text_style_stroke" id="qLBp)ss4@1m=mmBr1jX+[">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="stroke">
                                                                                                                                                                      <block type="string_value" id=",qzW8%hN(CjAL#{L+FUP">
                                                                                                                                                                        <field name="value">white</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="z?E$BVRSW@1G1josA-emt">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="strokeThickness">
                                                                                                                                                                          <block type="expression" id=":^t}2QmVS5W2,G_{u9!c">
                                                                                                                                                                            <field name="value">2</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <next>
                                                                                                                                          <block type="drop_shape" id="w)j?(/V(p4%{9:?$8$4,">
                                                                                                                                            <field name="multiple">TRUE</field>
                                                                                                                                            <field name="resultMode">default</field>
                                                                                                                                            <field name="groupName"></field>
                                                                                                                                            <statement name="#props">
                                                                                                                                              <block type="prop_value" id="qb_M)];%e`olXAF4c)P+">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="expression" id="?kZ`cqiaeq3zZ5pG65)J">
                                                                                                                                                    <field name="value">drop[$cell.data]</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_position" id="g1@11nY@1ubyo!.P09^KF#">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="x">
                                                                                                                                                      <block type="expression" id="D~M?_o=+:r114R;KzdlT">
                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="y">
                                                                                                                                                      <block type="expression" id="z-U~X{~?yLuOv,hB{:G?">
                                                                                                                                                        <field name="value">$cell.bottom - 10</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_size" id="8G@1%gNa:|KRL~R^?un5-">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="width">
                                                                                                                                                          <block type="expression" id="5ZlwRiP{c2k[A2qo2,}x">
                                                                                                                                                            <field name="value">$cell.width - 30</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="height">
                                                                                                                                                          <block type="expression" id="s0mZ9Weel[9AI4]z4HB)">
                                                                                                                                                            <field name="value">$cell.height- 40</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_anchor" id="QTh]7@2kC63hD9FgLJebd">
                                                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                                                            <value name="x">
                                                                                                                                                              <block type="expression" id="M~P0pp-|x4k6N-Z73$q;">
                                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="y">
                                                                                                                                                              <block type="expression" id="m5)6F4@2M|_dD9BlvS@23)">
                                                                                                                                                                <field name="value">1</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_spacing" id="Pc,0~HkCf?QVyfe:YjpW">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="spacing">
                                                                                                                                                                  <block type="expression" id="]s3?XZuU(E00T3T/4%Wz">
                                                                                                                                                                    <field name="value">15</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_scale" id="A$B(EhI-RN4/ee1fHAPM">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="scale">
                                                                                                                                                                      <block type="expression" id=";@1tLU-G-(j-RTu[Jx4-p">
                                                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_grid_cell_template_for" id="3u/9NRYEHNYe~`$Qa$dv">
                                                                                                                                    <field name="variable">$cell</field>
                                                                                                                                    <field name="condition">$cell.data == 3</field>
                                                                                                                                    <field name="#prop">cell.templates[]</field>
                                                                                                                                    <field name="#callback">$cell</field>
                                                                                                                                    <statement name="body">
                                                                                                                                      <block type="image_shape" id="lD0b2V.qzd[)sZFvgakt">
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_position" id="8PZd![h/4_-_@2VGOl.rm">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id="/-Jq!#ZPliwcZM,tyEYH">
                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="?tS:Ig}F6^febY4%@2Vn~">
                                                                                                                                                <field name="value">$cell.bottom</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_size" id="k0hoa+`4;IVfb8d3}9:{">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="width">
                                                                                                                                                  <block type="expression" id="URvlB~~F(B0A[Y~]+Ya/">
                                                                                                                                                    <field name="value">$cell.width - 10</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <value name="height">
                                                                                                                                                  <block type="expression" id="ofYQ813.F4Z]Z3La)sY`">
                                                                                                                                                    <field name="value">$cell.height- 10</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_anchor" id="U?MZ1xYX[]cb!vX?8DP-">
                                                                                                                                                    <field name="#prop">anchor</field>
                                                                                                                                                    <value name="x">
                                                                                                                                                      <block type="expression" id="RsdiPalAO$L7u}8_n|^L">
                                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="y">
                                                                                                                                                      <block type="expression" id="kU3h?1sFJ]wq6,EXlWKb">
                                                                                                                                                        <field name="value">1</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_image_key" id=";+;V$;Dwg7N{9$rNG@11B">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="key">
                                                                                                                                                          <block type="string_value" id="x+}9zH)cP%)$E^tP;-uB">
                                                                                                                                                            <field name="value">shape.png</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_image_src" id="B;CDVu_Umap-hapqbH/2">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="src">
                                                                                                                                                              <block type="string_value" id="db`e9_@1j8|7`5sNZSKiB">
                                                                                                                                                                <field name="value">${image_path}shape.png</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <next>
                                                                                                                                          <block type="grid_shape" id="~P4_#?@2!x#pBZu;O/Nn,">
                                                                                                                                            <statement name="#props">
                                                                                                                                              <block type="prop_grid_dimension" id="j:-1N|Y(4{t(Vd7hSu!K">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="rows">
                                                                                                                                                  <block type="expression" id="X=ld4~0x,?WdX1QB/sz3">
                                                                                                                                                    <field name="value">3</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <value name="cols">
                                                                                                                                                  <block type="expression" id="]6C;WzPboCdP-@1nqRC2L">
                                                                                                                                                    <field name="value">5</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_position" id="[ywgfE{{#W$9v=erL,:Z">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="x">
                                                                                                                                                      <block type="expression" id="]`OA9.0@2`lut|czM+[P7">
                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="y">
                                                                                                                                                      <block type="expression" id="3l,Dj2qLwd%ZNV-+A)J?">
                                                                                                                                                        <field name="value">$cell.bottom-10</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_size" id="B;/4tVGimf%m6HPV-]|4">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="width">
                                                                                                                                                          <block type="expression" id="+IHUjSejPK`[?5i9;D,+">
                                                                                                                                                            <field name="value">$cell.width - 30</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="height">
                                                                                                                                                          <block type="expression" id="QhWbm^.r]gfFh1PaqAl}">
                                                                                                                                                            <field name="value">$cell.height- 30</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_anchor" id="(m1D9,3_VNl0HQ8@1@2GaX">
                                                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                                                            <value name="x">
                                                                                                                                                              <block type="expression" id="0`rg{e3cITR%PsKcE}+Z">
                                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="y">
                                                                                                                                                              <block type="expression" id="4JSt8}.G75;;34BxA!%N">
                                                                                                                                                                <field name="value">1</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_grid_cell_source" id="FH$R.NjEO]gGyopn+DOB">
                                                                                                                                                                <field name="#prop">cell.source</field>
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="expression" id="Y=aE47+A{Xot~w#{U9?e">
                                                                                                                                                                    <field name="value">answer</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_grid_random" id="!:b+w5bsZz@2.xDuhFzi0">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <field name="random">FALSE</field>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_grid_show_borders" id="_dz?#[{wtHmUf|ZyvS{N">
                                                                                                                                                                        <field name="#prop">#showBorders</field>
                                                                                                                                                                        <field name="value">FALSE</field>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_grid_cell_template" id="WeK2V2QLpnT{Kp)Lm:6`">
                                                                                                                                                                            <field name="variable">$cell</field>
                                                                                                                                                                            <field name="#prop">cell.template</field>
                                                                                                                                                                            <field name="#callback">$cell</field>
                                                                                                                                                                            <statement name="body">
                                                                                                                                                                              <block type="choice_custom_shape" id="h@1:OKVxl/G{A)`,?N8Cu">
                                                                                                                                                                                <field name="action">drag</field>
                                                                                                                                                                                <value name="value">
                                                                                                                                                                                  <block type="expression" id=",P2G]DnL=}y@2II$@2ETmu">
                                                                                                                                                                                    <field name="value">$cell.data</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <statement name="template">
                                                                                                                                                                                  <block type="image_shape" id="+Yxk{OKsxvh6|~@2d,}|H">
                                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                                      <block type="prop_position" id="YFe8yVHeeONDh2x?1yz0">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="x">
                                                                                                                                                                                          <block type="expression" id="1:Kqod%q(T@2Q9I}f=ZVo">
                                                                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <value name="y">
                                                                                                                                                                                          <block type="expression" id="d-{Bje!~tSTxn];p]B2U">
                                                                                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_max_size" id="z^va8cT)C~]Y!7X;+|r`">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <value name="maxWidth">
                                                                                                                                                                                              <block type="expression" id="gB!j`sM}a{ys!el-ilG]">
                                                                                                                                                                                                <field name="value">45</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <value name="maxHeight">
                                                                                                                                                                                              <block type="expression" id="3+hR@2^_+ARP6])iT)UOq">
                                                                                                                                                                                                <field name="value">45</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_image_key" id="kr7xGNxkWD!(@1)2|XLwW">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <value name="key">
                                                                                                                                                                                                  <block type="string_value" id="[AtTgG^ZvL`8nR+;$)">
                                                                                                                                                                                                    <field name="value">${$cell.data}.png</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="prop_image_src" id="ja#YrD7!?_Ed==i:O;}p">
                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                    <value name="src">
                                                                                                                                                                                                      <block type="string_value" id=":th3MeqS0Td$~GA$2ex,">
                                                                                                                                                                                                        <field name="value">${image_path}${$cell.data}.png</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </statement>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="image_shape" id="c`~V,Jz(=Ci$[@1!KLe5Z">
                                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                                          <block type="prop_position" id="it(iZ){!^{eb8|UPTMLM">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <value name="x">
                                                                                                                                                                                              <block type="expression" id="(2I-;QY9Q@2ApKcj(A,23">
                                                                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <value name="y">
                                                                                                                                                                                              <block type="expression" id="S_J@18QdP4``@20=VXLAF)">
                                                                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_image_key" id="8|d/1avcc6o]YtH@2Rg,M">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <value name="key">
                                                                                                                                                                                                  <block type="string_value" id="OCmSS=6}W#C?xoBO3C1d">
                                                                                                                                                                                                    <field name="value">boxans.png</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="prop_image_src" id="8TL{=%.J7f5/GbCAVyv9">
                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                    <value name="src">
                                                                                                                                                                                                      <block type="string_value" id="!WRihbby:xbeP@2T?eR?h">
                                                                                                                                                                                                        <field name="value">${image_path}boxans.png</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </statement>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </statement>
                                                                                                                                                                              </block>
                                                                                                                                                                            </statement>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </statement>
                                                        <next>
                                                          <block type="if_then_block" id="LJ@1_x^VPE^w]$q)(L82|">
                                                            <value name="if">
                                                              <block type="expression" id="]rk7Hd4$hA-xK,71kjWO">
                                                                <field name="value">choise &lt; 11</field>
                                                              </block>
                                                            </value>
                                                            <statement name="then">
                                                              <block type="variable" id="0$H/!zo7,;{Ad#m)Q_~C">
                                                                <field name="name">answer</field>
                                                                <value name="value">
                                                                  <block type="expression" id="Sa3+-)QqpBy$-f)^:8h:">
                                                                    <field name="value">[]</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="variable" id=":+8K@2tA|}qqHE?DWh7pI">
                                                                    <field name="name">i</field>
                                                                    <value name="value">
                                                                      <block type="expression" id="@209Y[+62^}zk]#}=5o,2">
                                                                        <field name="value">0</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="while_do_block" id="OzP}QHkyyXQOgEeNTr}I">
                                                                        <value name="while">
                                                                          <block type="expression" id="[RiP1_~4{jhqyof86(bH">
                                                                            <field name="value">i &lt; choise</field>
                                                                          </block>
                                                                        </value>
                                                                        <statement name="do">
                                                                          <block type="if_then_else_block" id="OBu(oms@1ZTSW0=aife`x">
                                                                            <value name="if">
                                                                              <block type="expression" id="@1Q44w-hQ]_.scUyqpU{O">
                                                                                <field name="value">i &lt; 4</field>
                                                                              </block>
                                                                            </value>
                                                                            <statement name="then">
                                                                              <block type="variable" id="?}=e$.[-8_=z3enzYFyJ">
                                                                                <field name="name">answer[i]</field>
                                                                                <value name="value">
                                                                                  <block type="expression" id="1#v(UUv:IJ%dvH-i{AKh">
                                                                                    <field name="value">sh[i]</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </statement>
                                                                            <statement name="else">
                                                                              <block type="do_while_block" id="y6tn)x+v):|@1~#@1ELdQH">
                                                                                <statement name="do">
                                                                                  <block type="variable" id="s?cT6A#`+Wr]4)}bS?Y]">
                                                                                    <field name="name">answer[i]</field>
                                                                                    <value name="value">
                                                                                      <block type="random_one" id="J}%@1G;dkIX]0[4hX4-Ne">
                                                                                        <value name="items">
                                                                                          <block type="expression" id="-NYT)#]XLi(9VaLh1@29,">
                                                                                            <field name="value">sh</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </statement>
                                                                                <value name="while">
                                                                                  <block type="expression" id="~H3ccE`xbJP5/H2OH,6@2">
                                                                                    <field name="value">count_x(answer[i], answer, i+1) &gt; max</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="variable" id="H#hzIQmQxPini-Y^+0TQ">
                                                                                <field name="name">i</field>
                                                                                <value name="value">
                                                                                  <block type="expression" id="CncW2n_%F;qP|wCw%lyT">
                                                                                    <field name="value">i+1</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="variable" id=")H}XwGK=A6W{mM6q3%#d">
                                                                            <field name="name">drop</field>
                                                                            <value name="value">
                                                                              <block type="expression" id="v#[i#vO=jCcuNDB;mfxR">
                                                                                <field name="value">[]</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="variable" id="M@2Hahh8F2Ris,Oixyqwe">
                                                                                <field name="name">type</field>
                                                                                <value name="value">
                                                                                  <block type="expression" id="dgIBeiAv3avZ=G!pL:kJ">
                                                                                    <field name="value">[]</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="variable" id="[uq[1(v7SXu$%=(--|!?">
                                                                                    <field name="name">i</field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="4%hRZsJ4b6`?%_)@1H6J)">
                                                                                        <field name="value">0</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="while_do_block" id="pL~W)ew)Wcvzzmx8hS9E">
                                                                                        <value name="while">
                                                                                          <block type="expression" id="FP!H]l%,pw=qzr)24tJx">
                                                                                            <field name="value">i &lt; 4</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <statement name="do">
                                                                                          <block type="variable" id="bpv.mN%!MJ|@2|~hc=iH4">
                                                                                            <field name="name">type[i]</field>
                                                                                            <value name="value">
                                                                                              <block type="random_many" id="H:}T`G@2pnF.j@1xX.,7+u">
                                                                                                <value name="count">
                                                                                                  <block type="expression" id="x-h+Zr=xHDnOb_V6t5VQ">
                                                                                                    <field name="value">count_x(sh[i], answer, choise)</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="items">
                                                                                                  <block type="expression" id="zN6@1autf_u8j!8^q3I3)">
                                                                                                    <field name="value">[0, 1, 2, 3, 4, 5]</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="variable" id="%=F=kki:[Qf@1?k=e4$Jh">
                                                                                                <field name="name">count</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="k^UhIrT8!+[KsX@1{ig@10">
                                                                                                    <field name="value">0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="variable" id="`![}O]dmn5hLRIdlb(EM">
                                                                                                    <field name="name">drop[i]</field>
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="()asy1D8Z]7~T2{;xCb#">
                                                                                                        <field name="value">[]</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="variable" id="me{:GbDm?Z_13m?@2nZPf">
                                                                                                        <field name="name">j</field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id="|,F;Hkn6@2|bCKjqu?jC}">
                                                                                                            <field name="value">0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="while_do_block" id="I~t`mIeXlzyJ^`ca]Zbo">
                                                                                                            <value name="while">
                                                                                                              <block type="expression" id="?HUNcyOfn3S_Q%0uZIK,">
                                                                                                                <field name="value">j &lt; choise</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <statement name="do">
                                                                                                              <block type="if_then_block" id="ZOV]4P!d2%IOQD(w%YhY">
                                                                                                                <value name="if">
                                                                                                                  <block type="expression" id="NUIE^Y_m!X@1_CPZK`jd+">
                                                                                                                    <field name="value">answer[j] == sh[i]</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <statement name="then">
                                                                                                                  <block type="variable" id="voF#]+(^~-){Y{RN4}_E">
                                                                                                                    <field name="name">answer[j]</field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="Pas7-c5}X/A`L!bHSvwN">
                                                                                                                        <field name="value">answer[j] + '_' + type[i][count]</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="variable" id="(6N1|Ck`7[u}eoN,Gw^4">
                                                                                                                        <field name="name">drop[i][count]</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id="Ry4Omw5(Yr{qoJHS~zoz">
                                                                                                                            <field name="value">answer[j]</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="variable" id="+4y?uJawv[GVr2m,@2Jz8">
                                                                                                                            <field name="name">count</field>
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="F8bp29mP6e5whwgGQz4$">
                                                                                                                                <field name="value">count +1</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="variable" id="1%#@2(oUcz2j%@2~R~^/vU">
                                                                                                                    <field name="name">j</field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="oh684:~,BpDR/g4yd@1`H">
                                                                                                                        <field name="value">j+1</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="variable" id="sk5vNl=}Av2x,#/}J~Ot">
                                                                                                                <field name="name">i</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="@1d,noD}+Oqv[p2]??M(2">
                                                                                                                    <field name="value">i+1</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                                                                    <field name="value">400</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                                                                    <field name="value">0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_anchor" id="O.v@2mKn@1K#+tF;|}C}xI">
                                                                                                    <field name="#prop">anchor</field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="+3%GU[X3l%%X`Jv1QL7@2">
                                                                                                        <field name="value">0.5</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="Kzp23HjHF7k]A-y1BJPm">
                                                                                                        <field name="value">0</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="contents">
                                                                                                          <block type="string_value" id="#6btV9p,PGV}wMx.Uï¿½">
                                                                                                            <field name="value">Drag and drop the objects to the 3D shapes.</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                                                            <field name="base">text</field>
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="fontSize">
                                                                                                                  <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                                                                    <field name="value">36</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="fill">
                                                                                                                      <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                                                                        <field name="value">black</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="stroke">
                                                                                                                          <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                                                            <field name="value">white</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="strokeThickness">
                                                                                                                              <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                                                                <field name="value">2</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="grid_shape" id="6tU?K^N8ec~A@1@1NwQ72N">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_grid_dimension" id="X4K:|9dKK%s9l4n5v@2lG">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="rows">
                                                                                                      <block type="expression" id="[Ev!.bxnzjc|-33v.%7=">
                                                                                                        <field name="value">2</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="cols">
                                                                                                      <block type="expression" id="u$7]/V/!ZWM4(Va??@2({">
                                                                                                        <field name="value">2</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_position" id="zLld84TfBg#!88]1,JD-">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="x">
                                                                                                          <block type="expression" id="Cd6g{Y_P?:GyVyvQi1wJ">
                                                                                                            <field name="value">400</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id=".F}E71+@1i)77!yQ`hmfu">
                                                                                                            <field name="value">190</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_size" id="+o2CWr^.ohx)_bcIG53]">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="width">
                                                                                                              <block type="expression" id="Z[x5iHG%KWC%KWQb}c-G">
                                                                                                                <field name="value">750</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="height">
                                                                                                              <block type="expression" id="@2E|Ob@1jF#gYtX8-%Iui@1">
                                                                                                                <field name="value">300</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_anchor" id="1WJjXY^.h,t]eNgxaJ}q">
                                                                                                                <field name="#prop">anchor</field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="DHx[vqs?.?nUD:AYPJWd">
                                                                                                                    <field name="value">0.5</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="e3D|RR7w!=Sufu):zV`c">
                                                                                                                    <field name="value">0.5</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_grid_cell_source" id="v8+Yu211Qg8fX9Mu4th`">
                                                                                                                    <field name="#prop">cell.source</field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="func_array_of_number" id="^XB[}b.Ox6X8TMRv8ghR" inline="true">
                                                                                                                        <value name="items">
                                                                                                                          <block type="expression" id="Bc`u(?9R[kvzPop7J,Dl">
                                                                                                                            <field name="value">4</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="from">
                                                                                                                          <block type="expression" id="}I?IE6:k6-emBO}X?VH6">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_grid_random" id="}?kS#x[b)9OGb@1`UdJGV">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <field name="random">FALSE</field>
                                                                                                                        <next>
                                                                                                                          <block type="prop_grid_show_borders" id=";y^wk4y%[VWjXOj~vrQZ">
                                                                                                                            <field name="#prop">#showBorders</field>
                                                                                                                            <field name="value">FALSE</field>
                                                                                                                            <next>
                                                                                                                              <block type="prop_grid_cell_template" id="`1%t.v[lS(UYT]|cGyMh">
                                                                                                                                <field name="variable">$cell</field>
                                                                                                                                <field name="#prop">cell.template</field>
                                                                                                                                <field name="#callback">$cell</field>
                                                                                                                                <statement name="body">
                                                                                                                                  <block type="image_shape" id="`L=F^y4dvOw]ZYUHO^/H">
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_position" id="Ik#Fmc0i!:|ctqw5Ode#">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="expression" id="qy1H;7kAXRc`b8$6jpg~">
                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id="F@2G?b1wpy@199VaEkESG5">
                                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_size" id="oyF:%u6UR7S:MB!s3/e%">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="width">
                                                                                                                                              <block type="expression" id="Ou9sExQ{3d).tz(GNC">
                                                                                                                                                <field name="value">0</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="height">
                                                                                                                                              <block type="expression" id="%NppLyKVe.ve`a,$cJko">
                                                                                                                                                <field name="value">150</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_key" id=";F?!IbNCyYuW;p)!4A1f">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="key">
                                                                                                                                                  <block type="string_value" id="(ij)KDR}MBLYd)hTpY^o">
                                                                                                                                                    <field name="value">shape.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_image_src" id="`AKOL|z?O]Jzv~G$H/%4">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="src">
                                                                                                                                                      <block type="string_value" id="z_BW~+Op^.+`#Kjz6nQ{">
                                                                                                                                                        <field name="value">${image_path}shape.png</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="text_shape" id="_[Gx-3`WZ-#uq;flPM/i">
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_position" id="08{b17LM3]5c_hY#NzM1">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id="aGyBD2-nuO=n!{fLWlw(">
                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="iu+6Kp}j9PP(dmkyj]!a">
                                                                                                                                                <field name="value">$cell.centerY - 68</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_anchor" id="Xl!|a.RZOkU%|q7A[Ru4">
                                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                                <value name="x">
                                                                                                                                                  <block type="expression" id="?eQ=L=PjK!(j%^j9+V89">
                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <value name="y">
                                                                                                                                                  <block type="expression" id="(t{dHM@1^q^+cttkVoeg?">
                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_text_contents" id="UTLg7r#clm%]GZu^UfZn">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="contents">
                                                                                                                                                      <block type="string_value" id="5XlC?y@2%`5=+R(@1X}">
                                                                                                                                                        <field name="value">${shapes[sh[$cell.data]]}</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_text_style" id=".;]L:Gn6oZn-~k@2k,r0o">
                                                                                                                                                        <field name="base">text</field>
                                                                                                                                                        <statement name="#props">
                                                                                                                                                          <block type="prop_text_style_font_size" id="$~+0805}#X]@1jg3Op^Ik">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="fontSize">
                                                                                                                                                              <block type="expression" id="z7s$qlKl3_)Lp#@22If2?">
                                                                                                                                                                <field name="value">28</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_text_style_fill" id="5Kn3v5Z7mI(DMZax8_fn">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="fill">
                                                                                                                                                                  <block type="string_value" id=",@2D44ygZxiXfwTc1)82r">
                                                                                                                                                                    <field name="value">black</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_text_style_stroke" id="]NO{.(W13KB9t9w5fHq^">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="stroke">
                                                                                                                                                                      <block type="string_value" id="q_~i;]3o.}v|8Z`Lp#5-">
                                                                                                                                                                        <field name="value">white</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="__ift#kzEYZE7!)ZOQZf">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="strokeThickness">
                                                                                                                                                                          <block type="expression" id="hW_Bz3g(r2Eh?J574#DE">
                                                                                                                                                                            <field name="value">2</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <next>
                                                                                                                                          <block type="drop_shape" id="UV:Y$6}DVR5gn5G|xK-/">
                                                                                                                                            <field name="multiple">TRUE</field>
                                                                                                                                            <field name="resultMode">default</field>
                                                                                                                                            <field name="groupName"></field>
                                                                                                                                            <statement name="#props">
                                                                                                                                              <block type="prop_value" id="Ci)=}G]/$4HxdCeG#3b4">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="expression" id="#t6g^i]yjXqi%m|Lq#,x">
                                                                                                                                                    <field name="value">drop[$cell.data]</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_position" id="G64EU8,O1gkJdu0|2(Gs">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="x">
                                                                                                                                                      <block type="expression" id=":hHR:@2eIp[LX,hNaIBx%">
                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="y">
                                                                                                                                                      <block type="expression" id="j!i!vtbMM{@2t8T92j1Ty">
                                                                                                                                                        <field name="value">$cell.centerY + 13</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_size" id="19A^5c65IM=}MShFjoWy">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="width">
                                                                                                                                                          <block type="expression" id="J0bNPmRM1CYiJli]/Sd]">
                                                                                                                                                            <field name="value">330</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="height">
                                                                                                                                                          <block type="expression" id="ih1q1=8!px@2MEpwt_WR7">
                                                                                                                                                            <field name="value">100</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_spacing" id="zB5$PSl.b:Y{ls42^X}$">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="spacing">
                                                                                                                                                              <block type="expression" id="|zUB?`xS-/$UZ3gEsRDE">
                                                                                                                                                                <field name="value">15</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_scale" id=".hWcFKX�qMJy+SuA@1(">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="scale">
                                                                                                                                                                  <block type="expression" id="Z4@1E9{Yqj[y@1?5hY$nVj">
                                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="image_shape" id="J1b!EAx]JOrggnjOl(xK">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_position" id="=j.zP/A:z1aB#i6w]2Y5">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="x">
                                                                                                          <block type="expression" id="E4~ETz0SE17UvvfLctSW">
                                                                                                            <field name="value">400</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id="n?aGAg@1]wZ[^uknl6-6!">
                                                                                                            <field name="value">390</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_size" id="iPz}@1vnGBLN-#h9hUNw#">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="width">
                                                                                                              <block type="expression" id="Sa#2k^bLz{ng3n9DXYD/">
                                                                                                                <field name="value">0</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="height">
                                                                                                              <block type="expression" id="}jm6fL4BH4#p8.xbt7[M">
                                                                                                                <field name="value">90</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_image_key" id="Tnt5$]}u)gr1R,1?!gWn">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="key">
                                                                                                                  <block type="string_value" id="{L@2iJV$Nu+4v_NG:Y~T9">
                                                                                                                    <field name="value">box.png</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_image_src" id="%(c`k(AU?9.,Orme2S~B">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="src">
                                                                                                                      <block type="string_value" id="FrSX}4#Vic1$Q03|JTI,">
                                                                                                                        <field name="value">${image_path}box.png</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="grid_shape" id="hmA4nVf9~H^QAWO_62D#">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_grid_dimension" id="1CueVu54I~$!yQoY5ME7">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="rows">
                                                                                                              <block type="expression" id=":PbtB;Dqq)[k+!?7SL9L">
                                                                                                                <field name="value">1</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="cols">
                                                                                                              <block type="expression" id="=v?TzD@2#dJ{U;]Ur0h#$">
                                                                                                                <field name="value">choise</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_position" id="x^:K#Batw7XJ:t,98G6?">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="{9$?v|0bKe_vkkhgR0c)">
                                                                                                                    <field name="value">400</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="ZrYbx^_,Eh(LF;[t!RpN">
                                                                                                                    <field name="value">390</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_size" id="NdMv@2qT:0(t~2,(@2ke0J">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="width">
                                                                                                                      <block type="expression" id=",N#TJ(~u2Q0u65s`OGH4">
                                                                                                                        <field name="value">720</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="height">
                                                                                                                      <block type="expression" id="NKraEGAjbxy+%7rk7`2K">
                                                                                                                        <field name="value">90</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_anchor" id="QoEMA(MHGSU/CaMr!s,x">
                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="xZ_(n-V8@2`=:,T:G$@24x">
                                                                                                                            <field name="value">0.5</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="Yi[)puUC:.7N{b[drK0l">
                                                                                                                            <field name="value">0.5</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_grid_cell_source" id="5)jDP):/IjGNLeJH[WXr">
                                                                                                                            <field name="#prop">cell.source</field>
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="BkdC[%lA],@2%/N4jMS_o">
                                                                                                                                <field name="value">answer</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_grid_random" id="]L~^V~l2@10ik+RE3iv.6">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <field name="random">FALSE</field>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_grid_show_borders" id="W?+PR-Z0+Qtx`+);G4si">
                                                                                                                                    <field name="#prop">#showBorders</field>
                                                                                                                                    <field name="value">FALSE</field>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_grid_cell_template" id="odfS@1Z_R859~mhlZ8ay6">
                                                                                                                                        <field name="variable">$cell</field>
                                                                                                                                        <field name="#prop">cell.template</field>
                                                                                                                                        <field name="#callback">$cell</field>
                                                                                                                                        <statement name="body">
                                                                                                                                          <block type="choice_custom_shape" id="_5?G;GCX#N0Zo5G;fxPM">
                                                                                                                                            <field name="action">drag</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id="u7.;_mqiR9FP5|hdR=pX">
                                                                                                                                                <field name="value">$cell.data</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <statement name="template">
                                                                                                                                              <block type="image_shape" id="Yri?@1rqC#sjJ|.z6d,{d">
                                                                                                                                                <statement name="#props">
                                                                                                                                                  <block type="prop_position" id="F4(5SUbdIZwZt+BzSO~F">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="x">
                                                                                                                                                      <block type="expression" id="eP=9Xk}]288JC~P!0]x5">
                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="y">
                                                                                                                                                      <block type="expression" id="HSCFU@2`:^]k3XP(I,=:,">
                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_max_size" id="z]?KtzJj6!Q-t?9M;yF4">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="maxWidth">
                                                                                                                                                          <block type="expression" id="%7yVQY:_tg}8-OB3v3ee">
                                                                                                                                                            <field name="value">45</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="maxHeight">
                                                                                                                                                          <block type="expression" id="yizdrf5~yO=$IJqqP_m3">
                                                                                                                                                            <field name="value">45</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_image_key" id="B=s3B@1miFcU$oOA4mcj1">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="key">
                                                                                                                                                              <block type="string_value" id="KReIZw$8ZKOPzxjOe3h(">
                                                                                                                                                                <field name="value">${$cell.data}.png</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_image_src" id="f~[bKcshz[$HtJorFesf">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="src">
                                                                                                                                                                  <block type="string_value" id="`Hlwn1Ft%__+N4#sy4G?">
                                                                                                                                                                    <field name="value">${image_path}${$cell.data}.png</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                                <next>
                                                                                                                                                  <block type="image_shape" id="M}!6en-0oZ%^~E6J:T4]">
                                                                                                                                                    <statement name="#props">
                                                                                                                                                      <block type="prop_position" id="-+;M@1=d$1RiI.1FQR{@1B">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="x">
                                                                                                                                                          <block type="expression" id="UU7xZ`-UPMzJ%.J/q^2!">
                                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="y">
                                                                                                                                                          <block type="expression" id="%ZS.=c=]b86XKI=Y%0x`">
                                                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_image_key" id="xEM]2^~53VZ[k?n:v|Yd">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="key">
                                                                                                                                                              <block type="string_value" id="Jv;Xv}UEYad(;G{$Mth_">
                                                                                                                                                                <field name="value">boxans.png</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_image_src" id="G||+(/h8ts%p7I5=BmSU">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="src">
                                                                                                                                                                  <block type="string_value" id="z#Y-ƝUlJc@1%h`9nKp">
                                                                                                                                                                    <field name="value">${image_path}boxans.png</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </statement>
                                                            <next>
                                                              <block type="statement" id="y!M(@2At$GL@2)Zcq??O%]">
                                                                <field name="value">function check(x, A, num) {
    for(var m=0; m&lt;num; m++){
        if(x == A[m]){
            return 1;
        }
    }
    return 0;
}
                                                                </field>
                                                                <next>
                                                                  <block type="variable" id="weElLx3.8SB.)STr377[">
                                                                    <field name="name">loadAssets</field>
                                                                    <value name="value">
                                                                      <block type="custom_image_list" id="[iQG~r:b@1r?SU7Kr3cr@2">
                                                                        <field name="link">${image_path}</field>
                                                                        <field name="images">0_0|0_1|0_2|0_3|0_4|0_5|0_d|1_0|1_1|1_2|1_3|1_4|1_5|1_d|2_0|2_1|2_2|2_3|2_4|2_5|2_d|3_0|3_1|3_2|3_3|3_4|3_5|3_d|4_0|4_1|4_2|4_3|4_4|4_5|4_d|5_0|5_1|5_2|5_3|5_4|5_5|5_d</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="partial_explanation" id="xNg8r`1PQW(1`dS+3[Nm" inline="true">
                                                                        <value name="value">
                                                                          <block type="string_value" id="yaPagY.iV/$/oJ/qiVxA">
                                                                            <field name="value">&lt;style&gt;
  table{background: rgb(150, 200, 250, 0.5); width: 900px}
td{padding: 10px; text-align: center; border: 1px solid black}
img{padding: 0 5px; margin: 0 10px; max-width: 80px; max-height: 80px; }
  &lt;/style&gt;
&lt;center&gt;
  &lt;table&gt;
  &lt;tr&gt;&lt;td&gt;3D Type&lt;/td&gt;&lt;td&gt;Looks Like&lt;/td&gt;&lt;/tr&gt;
                                                                            </field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="bs2u|)J7}nAJ~jL_d3sI">
                                                                            <field name="name">i</field>
                                                                            <value name="value">
                                                                              <block type="expression" id="3D0N%AUQ%xf$Fl9A@2u6M">
                                                                                <field name="value">0</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="while_do_block" id="Jjkc#F/g8g!mn5S0pfZl">
                                                                                <value name="while">
                                                                                  <block type="expression" id="ep@1jn0j=:1u15$keH5cb">
                                                                                    <field name="value">i &lt; num</field>
                                                                                  </block>
                                                                                </value>
                                                                                <statement name="do">
                                                                                  <block type="partial_explanation" id="O(ON+]#y|7YHVi2!N8y`" inline="true">
                                                                                    <value name="value">
                                                                                      <block type="string_value" id="h,gJMpnD7;`_%PLh$^]]">
                                                                                        <field name="value">&lt;tr&gt;&lt;td&gt;&lt;img src='@1sprite.src(${sh[i]}_d)'/&gt;&lt;/br&gt;${shapes[sh[i]]}&lt;/td&gt;&lt;td&gt;</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="variable" id="4kxx@2F0yPs+g;ydt{5ij">
                                                                                        <field name="name">j</field>
                                                                                        <value name="value">
                                                                                          <block type="expression" id="=Vb^iA?k}UW-4obzhV]p">
                                                                                            <field name="value">0</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="while_do_block" id="s8%c]`sEgY}4Ti!#BbH)">
                                                                                            <value name="while">
                                                                                              <block type="expression" id="Xv`S.Kw+rt))4KTkLG+|">
                                                                                                <field name="value">j &lt; 6</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <statement name="do">
                                                                                              <block type="if_then_block" id="PBKAe]vS9JEx:mw{fJ~L">
                                                                                                <value name="if">
                                                                                                  <block type="expression" id="r)aR2pRxL,+Q:Om/z+h3">
                                                                                                    <field name="value">check(j, type[i], type[i].length) == 0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <statement name="then">
                                                                                                  <block type="partial_explanation" id="VZxQev$43!f4@248UEqC)" inline="true">
                                                                                                    <value name="value">
                                                                                                      <block type="string_value" id="9huC@2H).7LugLUcPAmhK">
                                                                                                        <field name="value">&lt;img src='@1sprite.src(${sh[i]}_${j})'/&gt;</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="variable" id="F`ukkd9B]s1Yz/{b6R$F">
                                                                                                    <field name="name">j</field>
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="26,|Gy+u?@26L/dvNrT}E">
                                                                                                        <field name="value">j+1</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="variable" id="wZG~B.a$6x/)6~hac;Vi">
                                                                                                <field name="name">j</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="w~{)m}Zpr]q]EX|IpY$^">
                                                                                                    <field name="value">0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="while_do_block" id="W=)m(vBF{~.u}.yJ=wh2">
                                                                                                    <value name="while">
                                                                                                      <block type="expression" id="I)dXrYhbh8!n,Q=6MMN]">
                                                                                                        <field name="value">j &lt; 6</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <statement name="do">
                                                                                                      <block type="if_then_block" id="0Dxt_cVl{YKt@2`Lz{yi7">
                                                                                                        <value name="if">
                                                                                                          <block type="expression" id="hd!};p^Yy!IWt{mp+.th">
                                                                                                            <field name="value">check(j, type[i], type[i].length) == 1</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <statement name="then">
                                                                                                          <block type="partial_explanation" id="EgY~KVvwT5rsh^;UOiWm" inline="true">
                                                                                                            <value name="value">
                                                                                                              <block type="string_value" id="omyLv@2S3#,0wahP[58!$">
                                                                                                                <field name="value">&lt;img src='@1sprite.src(${sh[i]}_${j})' style='border: 3px solid green; padding: 5px'/&gt;</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="variable" id=",9`,0XQoRS^sy3Y]|pt+">
                                                                                                            <field name="name">j</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="t8zl6MW[[@1.B0ejNx5Ht">
                                                                                                                <field name="value">j+1</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="partial_explanation" id="GW/=7#,v!ycRnO9JNQk+" inline="true">
                                                                                                        <value name="value">
                                                                                                          <block type="string_value" id="VU^ImPlb?I6{Spz$Q6uj">
                                                                                                            <field name="value">&lt;/td&gt;&lt;/tr&gt;</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="variable" id="Q/g=+KtaR+PyXTAt|:S;">
                                                                                                            <field name="name">i</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="C+`Yf+.o4MZgyHQ5d8%;">
                                                                                                                <field name="value">i+1</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="partial_explanation" id="(gIR2^x}4@23Zq_tV[.Bf" inline="true">
                                                                                    <value name="value">
                                                                                      <block type="string_value" id="[AcrRB?nTp/mAmcV3Ey}">
                                                                                        <field name="value">&lt;/table&gt;</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="end_partial_explanation" id="[zJq@1BKyr@1Q;jvYg9~ad"></block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
  <block type="variable" id="X|3j4;)]/:rMm~Fxd.~P" x="561" y="1893">
    <field name="name">answer</field>
    <value name="value">
      <block type="func_shuffle" id="L(-xQN5v`}2;NehmQAL}" inline="true">
        <value name="value">
          <block type="expression" id="lCM-!IL9.SwDnT^L(T61">
            <field name="value">answer</field>
          </block>
        </value>
      </block>
    </value>
  </block>
  <block type="variable" id="H:woGM@1@1(sa.17)(p+FX" x="569" y="5363">
    <field name="name">answer</field>
    <value name="value">
      <block type="func_shuffle" id="kN3Q@2fq==tB%H(wR!L44" inline="true">
        <value name="value">
          <block type="expression" id="gf:%WjX|q^ZLv(j1x+jo">
            <field name="value">answer</field>
          </block>
        </value>
      </block>
    </value>
  </block>
</xml>
END_XML]] */