
module.exports = [
  {
    "#type": "question",
    "name": "Y1.MG.AR.OOA.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y1.MG.AR.OOA.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "statement",
        "value": "function jsUcfirst(string) \n{\n    return string.charAt(0).toUpperCase() + string.slice(1);\n}"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "variable",
        "name": "type",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "2"
          }
        }
      },
      {
        "#type": "variable",
        "name": "condition",
        "value": {
          "#type": "random_one",
          "items": {
            "#type": "expression",
            "value": "['smallest to largest', 'largest to smallest']"
          }
        }
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "20"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "Order the shapes from the ${condition} area."
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "36"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "range < 4"
            },
            "then": [
              {
                "#type": "variable",
                "name": "number",
                "value": {
                  "#type": "random_many",
                  "count": {
                    "#type": "expression",
                    "value": "3"
                  },
                  "items": {
                    "#type": "func",
                    "name": "arrayOfNumber",
                    "args": [
                      {
                        "#type": "expression",
                        "value": "10"
                      },
                      {
                        "#type": "expression",
                        "value": "1"
                      }
                    ]
                  }
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "number",
                "value": {
                  "#type": "random_many",
                  "count": {
                    "#type": "expression",
                    "value": "3"
                  },
                  "items": {
                    "#type": "func",
                    "name": "arrayOfNumber",
                    "args": [
                      {
                        "#type": "expression",
                        "value": "20"
                      },
                      {
                        "#type": "expression",
                        "value": "11"
                      }
                    ]
                  }
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "color",
            "value": {
              "#type": "random_many",
              "count": {
                "#type": "expression",
                "value": "3"
              },
              "items": {
                "#type": "expression",
                "value": "[1, 2, 3, 4, 5, 6]"
              }
            }
          }
        ],
        "else": [
          {
            "#type": "variable",
            "name": "number",
            "value": {
              "#type": "func",
              "name": "shuffle",
              "args": [
                {
                  "#type": "expression",
                  "value": "[1, 2, 3]"
                }
              ]
            }
          },
          {
            "#type": "variable",
            "name": "color",
            "value": {
              "#type": "random_many",
              "count": {
                "#type": "expression",
                "value": "3"
              },
              "items": {
                "#type": "expression",
                "value": "[1, 2, 3, 4]"
              }
            }
          }
        ]
      },
      {
        "#type": "statement",
        "value": "var asc = [];\n\nfor(var i =0; i<3; i++){\n  asc[i] = number[i];\n}\n\nvar coco = [];\n\nfor(var i =0; i<3; i++){\n  coco[i] = color[i];\n}\n\nif(condition == 'smallest to largest'){\n  for(var i =0; i<3; i++){\n      for(var j =i; j< 3; j++){\n      if(asc[j]<asc[i]){\n        var temp = asc[i];\n        asc[i] = asc[j];\n        asc[j] = temp;\n\n        var temp = coco[i];\n        coco[i] = coco[j];\n        coco[j] = temp;\n      }\n    }\n  }\n}\nelse{\n  for(var i =0; i<3; i++){\n    for(var j =i; j< 3; j++){\n      if(asc[j]>asc[i]){\n        var temp = asc[i];\n        asc[i] = asc[j];\n        asc[j] = temp;\n\n        var temp = coco[i];\n        coco[i] = coco[j];\n        coco[j] = temp;\n      }\n    }\n  }\n}"
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "3"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "150"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "780"
                },
                "height": {
                  "#type": "expression",
                  "value": "130"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "[0, 1, 2]"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "shape.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}shape.png"
                      }
                    ]
                  },
                  {
                    "#type": "drop_shape",
                    "multiple": false,
                    "resultMode": "default",
                    "groupName": "",
                    "#props": [
                      {
                        "#type": "prop_value",
                        "#prop": "",
                        "value": {
                          "#type": "expression",
                          "value": "asc[$cell.data]"
                        }
                      },
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "200"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "118"
                        }
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "list_shape",
            "#props": [
              {
                "#type": "prop_list_direction",
                "#prop": "",
                "dir": "horizontal"
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "240"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              }
            ],
            "items": [
              {
                "#type": "json",
                "#props": [
                  {
                    "#type": "prop_list_align",
                    "#prop": "",
                    "align": "middle"
                  }
                ],
                "template": {
                  "#callback": "$item",
                  "variable": "$item",
                  "body": [
                    {
                      "#type": "image_shape",
                      "#props": [
                        {
                          "#type": "prop_image_key",
                          "#prop": "",
                          "key": "${color[0]}.png"
                        },
                        {
                          "#type": "prop_image_src",
                          "#prop": "",
                          "src": "${image_path}${color[0]}.png"
                        }
                      ]
                    }
                  ]
                }
              },
              {
                "#type": "json",
                "#props": [
                  {
                    "#type": "prop_list_align",
                    "#prop": "",
                    "align": "middle"
                  }
                ],
                "template": {
                  "#callback": "$item",
                  "variable": "$item",
                  "body": [
                    {
                      "#type": "text_shape",
                      "#props": [
                        {
                          "#type": "prop_text_contents",
                          "#prop": "",
                          "contents": {
                            "#type": "expression",
                            "value": "' = 1 cm'"
                          }
                        },
                        {
                          "#prop": "",
                          "style": {
                            "#type": "json",
                            "base": "text",
                            "#props": [
                              {
                                "#type": "prop_text_style_font_size",
                                "#prop": "",
                                "fontSize": {
                                  "#type": "expression",
                                  "value": "36"
                                }
                              },
                              {
                                "#type": "prop_text_style_fill",
                                "#prop": "",
                                "fill": "black"
                              },
                              {
                                "#type": "prop_text_style_stroke",
                                "#prop": "",
                                "stroke": "white"
                              },
                              {
                                "#type": "prop_text_style_stroke_thickness",
                                "#prop": "",
                                "strokeThickness": {
                                  "#type": "expression",
                                  "value": "2"
                                }
                              }
                            ]
                          }
                        }
                      ]
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "465"
                },
                "y": {
                  "#type": "expression",
                  "value": "235"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "2"
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "26"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "350"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "bigshape.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}bigshape.png"
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "3"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "370"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "470"
                },
                "height": {
                  "#type": "expression",
                  "value": "120"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "[0, 1, 2]"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "variable",
                    "name": "data",
                    "value": {
                      "#type": "expression",
                      "value": "number[$cell.data]"
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "co",
                    "value": {
                      "#type": "expression",
                      "value": "color[$cell.data]"
                    }
                  },
                  {
                    "#type": "choice_custom_shape",
                    "action": "drag",
                    "value": {
                      "#type": "expression",
                      "value": "data"
                    },
                    "template": {
                      "#callback": "$choice",
                      "variable": "$choice",
                      "body": [
                        {
                          "#type": "grid_shape",
                          "#props": [
                            {
                              "#type": "prop_grid_dimension",
                              "#prop": "",
                              "rows": {
                                "#type": "expression",
                                "value": "Math.floor(data/6) + (data % 6 > 0 ? 1:0)"
                              },
                              "cols": {
                                "#type": "expression",
                                "value": "data > 5 ? 6:data"
                              }
                            },
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "$cell.centerX"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "$cell.centerY"
                              }
                            },
                            {
                              "#type": "prop_grid_cell_size",
                              "#prop": "cell.size",
                              "width": {
                                "#type": "expression",
                                "value": "20"
                              },
                              "height": {
                                "#type": "expression",
                                "value": "20"
                              },
                              "spacing": {
                                "#type": "expression",
                                "value": "0"
                              },
                              "radius": {
                                "#type": "expression",
                                "value": "0"
                              }
                            },
                            {
                              "#type": "prop_anchor",
                              "#prop": "anchor",
                              "x": {
                                "#type": "expression",
                                "value": "0.5"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "0.5"
                              }
                            },
                            {
                              "#type": "prop_grid_cell_source",
                              "#prop": "cell.source",
                              "value": {
                                "#type": "expression",
                                "value": "data"
                              }
                            },
                            {
                              "#type": "prop_grid_random",
                              "#prop": "",
                              "random": false
                            },
                            {
                              "#type": "prop_grid_show_borders",
                              "#prop": "#showBorders",
                              "value": false
                            },
                            {
                              "#type": "prop_grid_cell_template",
                              "variable": "$cell",
                              "#prop": "cell.template",
                              "#callback": "$cell",
                              "body": [
                                {
                                  "#type": "image_shape",
                                  "#props": [
                                    {
                                      "#type": "prop_position",
                                      "#prop": "",
                                      "x": {
                                        "#type": "expression",
                                        "value": "$cell.centerX"
                                      },
                                      "y": {
                                        "#type": "expression",
                                        "value": "$cell.centerY"
                                      }
                                    },
                                    {
                                      "#type": "prop_size",
                                      "#prop": "",
                                      "width": {
                                        "#type": "expression",
                                        "value": "$cell.width"
                                      },
                                      "height": {
                                        "#type": "expression",
                                        "value": "$cell.height"
                                      }
                                    },
                                    {
                                      "#type": "prop_image_key",
                                      "#prop": "",
                                      "key": "${co}.png"
                                    },
                                    {
                                      "#type": "prop_image_src",
                                      "#prop": "",
                                      "src": "${image_path}${co}.png"
                                    }
                                  ]
                                }
                              ]
                            }
                          ]
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 1: First, count the number of squares each shape has.</u></br></br>\n\n<style>\n  .td{width: 30px; height: 30px; border-collapse: collapse;}\n.tablex {display: inline-block; margin: 40px}\ntd{text-align: center}\n\n.stroke {\n  color: black;\n  border-collapse: collapse;\n  font-size: 20px;\n  line-box: 20px;\n}\nb{background: rgb(250, 250, 250, 0.5)}\n  </style>\n\n\n<center>\n    <table style=''>\n    <tr>"
            ]
          },
          {
            "#type": "variable",
            "name": "index",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "index < 3"
            },
            "do": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<td><table class='tablex'>"
                ]
              },
              {
                "#type": "variable",
                "name": "data",
                "value": {
                  "#type": "expression",
                  "value": "number[index]"
                }
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "i < Math.floor(data/6) + (data % 6 > 0 ? 1:0)"
                },
                "do": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<tr>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "j",
                    "value": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "while_do_block",
                    "while": {
                      "#type": "expression",
                      "value": "j < (data > 5 ? 6:data)"
                    },
                    "do": [
                      {
                        "#type": "if_then_else_block",
                        "if": {
                          "#type": "expression",
                          "value": "i*6 + j < data"
                        },
                        "then": [
                          {
                            "#type": "func",
                            "name": "addPartialExplanation",
                            "args": [
                              "<td class='td' style='background-image: url(\"@sprite.src(${color[index]}.png)\"); background-position: center; background-size: 100% 100%'>\n<div class='stroke' style='margin: 0px; padding: 0px; height: 20px; width: 20px;'>\n${i*6 + j + 1}\n</div></td>"
                            ]
                          }
                        ],
                        "else": [
                          {
                            "#type": "func",
                            "name": "addPartialExplanation",
                            "args": [
                              "<td class='td'></td>"
                            ]
                          }
                        ]
                      },
                      {
                        "#type": "variable",
                        "name": "j",
                        "value": {
                          "#type": "expression",
                          "value": "j+1"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "</tr>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "i",
                    "value": {
                      "#type": "expression",
                      "value": "i+1"
                    }
                  }
                ]
              },
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</table></td>"
                ]
              },
              {
                "#type": "variable",
                "name": "index",
                "value": {
                  "#type": "expression",
                  "value": "index+1"
                }
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</tr><tr>\n  <td><b>${number[0]}</b></td><td><b>${number[1]}</b></td><td><b>${number[2]}</b></td>\n</tr></table></center>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 2: Get to know the ordering of areas.</u></br></br>\n\n<style>\n  .td{width: 30px; height: 30px; border-collapse: collapse;}\n.table {display: inline-block; margin: 40px;}\ntd{text-align: center}\n\n.stroke {\n  color: black;\n  border-collapse: collapse;\n  font-size: 20px;\n  line-box: 20px;\n}\n\n.bg {background: rgb(250, 250, 250, 0.5)}\n  </style>\n\n\n<center>\n    \n    <span class='bg'>${jsUcfirst(condition)}</br></span>\n    <table>\n    <tr>"
            ]
          },
          {
            "#type": "variable",
            "name": "index",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "index < 3"
            },
            "do": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<td><table class='table'>"
                ]
              },
              {
                "#type": "variable",
                "name": "data",
                "value": {
                  "#type": "expression",
                  "value": "asc[index]"
                }
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "i < Math.floor(data/6) + (data % 6 > 0 ? 1:0)"
                },
                "do": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<tr>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "j",
                    "value": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "while_do_block",
                    "while": {
                      "#type": "expression",
                      "value": "j < (data > 5 ? 6:data)"
                    },
                    "do": [
                      {
                        "#type": "if_then_else_block",
                        "if": {
                          "#type": "expression",
                          "value": "i*6 + j < data"
                        },
                        "then": [
                          {
                            "#type": "func",
                            "name": "addPartialExplanation",
                            "args": [
                              "<td class='td' style='background-image: url(\"@sprite.src(${coco[index]}.png)\"); background-position: center; background-size: 100% 100%'>\n</td>"
                            ]
                          }
                        ],
                        "else": [
                          {
                            "#type": "func",
                            "name": "addPartialExplanation",
                            "args": [
                              "<td class='td'></td>"
                            ]
                          }
                        ]
                      },
                      {
                        "#type": "variable",
                        "name": "j",
                        "value": {
                          "#type": "expression",
                          "value": "j+1"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "</tr>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "i",
                    "value": {
                      "#type": "expression",
                      "value": "i+1"
                    }
                  }
                ]
              },
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</table></td>"
                ]
              },
              {
                "#type": "variable",
                "name": "index",
                "value": {
                  "#type": "expression",
                  "value": "index+1"
                }
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</tr><tr>\n  <td><span class='bg'>${condition == 'smallest to largest' ? 'Smallest':'Largest'}</span></td>\n    <td></td>\n      <td><span class='bg'>${condition == 'smallest to largest' ? 'Largest':'Smallest'}</span></td>\n        \n</tr></table></center>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 3: From the question, look for the ordering focus.</u></br></br>\n\n<style>\n  .td{width: 30px; height: 30px; border-collapse: collapse;}\n.table {display: inline-block; margin: 40px;}\ntd{text-align: center}\n\n.stroke {\n  color: black;\n  border-collapse: collapse;\n  font-size: 20px;\n  line-box: 20px;\n}\n\n.bg {background: rgb(250, 250, 250, 0.5)}\n  </style>\n\n\n    <span class='bg'>Order the shapes below from <b>${condition}</b> area.</br>\nThe correct order of shapes is:</br></span>\n<center>\n    \n    <table>\n    <tr>"
            ]
          },
          {
            "#type": "variable",
            "name": "index",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "index < 3"
            },
            "do": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<td><table class='table'>"
                ]
              },
              {
                "#type": "variable",
                "name": "data",
                "value": {
                  "#type": "expression",
                  "value": "asc[index]"
                }
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "i < Math.floor(data/6) + (data % 6 > 0 ? 1:0)"
                },
                "do": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<tr>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "j",
                    "value": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "while_do_block",
                    "while": {
                      "#type": "expression",
                      "value": "j < (data > 5 ? 6:data)"
                    },
                    "do": [
                      {
                        "#type": "if_then_else_block",
                        "if": {
                          "#type": "expression",
                          "value": "i*6 + j < data"
                        },
                        "then": [
                          {
                            "#type": "func",
                            "name": "addPartialExplanation",
                            "args": [
                              "<td class='td' style='background-image: url(\"@sprite.src(${coco[index]}.png)\"); background-position: center; background-size: 100% 100%'>\n</td>"
                            ]
                          }
                        ],
                        "else": [
                          {
                            "#type": "func",
                            "name": "addPartialExplanation",
                            "args": [
                              "<td class='td'></td>"
                            ]
                          }
                        ]
                      },
                      {
                        "#type": "variable",
                        "name": "j",
                        "value": {
                          "#type": "expression",
                          "value": "j+1"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "</tr>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "i",
                    "value": {
                      "#type": "expression",
                      "value": "i+1"
                    }
                  }
                ]
              },
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</table></td>"
                ]
              },
              {
                "#type": "variable",
                "name": "index",
                "value": {
                  "#type": "expression",
                  "value": "index+1"
                }
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</tr></table></center>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 2"
        },
        "then": [
          {
            "#type": "variable",
            "name": "itype",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "1"
              },
              "max": {
                "#type": "expression",
                "value": "7"
              }
            }
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "3"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "150"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "780"
                },
                "height": {
                  "#type": "expression",
                  "value": "130"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "asc"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "shape.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}shape.png"
                      }
                    ]
                  },
                  {
                    "#type": "drop_shape",
                    "multiple": false,
                    "resultMode": "default",
                    "groupName": "",
                    "#props": [
                      {
                        "#type": "prop_value",
                        "#prop": "",
                        "value": {
                          "#type": "expression",
                          "value": "$cell.data"
                        }
                      },
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "200"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "118"
                        }
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "350"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "bigshape.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}bigshape.png"
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "3"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "370"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "470"
                },
                "height": {
                  "#type": "expression",
                  "value": "120"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "[0, 1, 2]"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "choice_custom_shape",
                    "action": "drag",
                    "value": {
                      "#type": "expression",
                      "value": "number[$cell.data]"
                    },
                    "template": {
                      "#callback": "$choice",
                      "variable": "$choice",
                      "body": [
                        {
                          "#type": "image_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "$cell.centerX"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "$cell.centerY"
                              }
                            },
                            {
                              "#type": "prop_image_key",
                              "#prop": "",
                              "key": "${number[$cell.data]}_${itype}_${color[$cell.data]}.png"
                            },
                            {
                              "#type": "prop_image_src",
                              "#prop": "",
                              "src": "${image_path}${number[$cell.data]}_${itype}_${color[$cell.data]}.png"
                            }
                          ]
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 1: First, get to know the ordering of areas</u></br></br>\n\n<style>\n  .td{width: 30px; height: 30px; border-collapse: collapse;}\n.tablex {display: inline-block; margin: 40px;}\ntd{text-align: center; vertical-align: bottom; width: 200px}\n\n.stroke {\n  color: black;\n  border-collapse: collapse;\n  font-size: 20px;\n  line-box: 20px;\n}\n\n.bg {background: rgb(250, 250, 250, 0.5)}\n  </style>\n\n\n<center>\n    \n    <span class='bg'>${jsUcfirst(condition)}</br></span>\n    <table class='tablex'>\n    <tr>"
            ]
          },
          {
            "#type": "variable",
            "name": "index",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "index < 3"
            },
            "do": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<td><img src='@sprite.src(${asc[index]}_${itype}_${coco[index]}.png)'/></td>"
                ]
              },
              {
                "#type": "variable",
                "name": "index",
                "value": {
                  "#type": "expression",
                  "value": "index+1"
                }
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</tr><tr>\n  <td><span class='bg'>${condition == 'smallest to largest' ? 'Smallest':'Largest'}</span></td>\n    <td></td>\n      <td><span class='bg'>${condition == 'smallest to largest' ? 'Largest':'Smallest'}</span></td>\n        \n</tr></table></center>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 2: From the question, look for the ordering focus.</u></br></br>\n\n<style>\n  .td{width: 30px; height: 30px; border-collapse: collapse;}\n.tablex {display: inline-block; margin: 40px;}\ntd{text-align: center; vertical-align: bottom; width: 200px}\n\n.stroke {\n  color: black;\n  border-collapse: collapse;\n  font-size: 20px;\n  line-box: 20px;\n}\n\n.bg {background: rgb(250, 250, 250, 0.5)}\n  </style>\n\n\n    <span class='bg'>Order the shapes below from <b>${condition}</b> area.</br>\nThe correct order of shapes is:</br></br></span>\n<center>\n    \n    <table class='tablex'>\n    <tr>"
            ]
          },
          {
            "#type": "variable",
            "name": "index",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "index < 3"
            },
            "do": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<td><img src='@sprite.src(${asc[index]}_${itype}_${coco[index]}.png)'/></td>"
                ]
              },
              {
                "#type": "variable",
                "name": "index",
                "value": {
                  "#type": "expression",
                  "value": "index+1"
                }
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</tr></table></center>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          }
        ]
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="0" y="0">
    <field name="name">Y1.MG.AR.OOA.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y1.MG.AR.OOA.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="statement" id="4]sChEH772IQC=VS4k{3">
                            <field name="value">function jsUcfirst(string) 
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}
                            </field>
                            <next>
                              <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                                <field name="name">numberOfCorrect</field>
                                <value name="value">
                                  <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                    <field name="name">numberOfIncorrect</field>
                                    <value name="value">
                                      <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                        <field name="value">0</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="variable" id="YepMQT@2uwp2rRPvmnRg1">
                                        <field name="name">range</field>
                                        <value name="value">
                                          <block type="expression" id="UxgQ^Y5]rb}vpw[D=l-!">
                                            <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                          </block>
                                        </value>
                                        <next>
                                          <block type="variable" id="0!Q:JhwO-%,O48(/+iOU">
                                            <field name="name">type</field>
                                            <value name="value">
                                              <block type="random_number" id="7B3Ak?=P6Ez-uX~@2ka3%">
                                                <value name="min">
                                                  <block type="expression" id="czvQ@2WCX6jwe]q$dBCH1">
                                                    <field name="value">1</field>
                                                  </block>
                                                </value>
                                                <value name="max">
                                                  <block type="expression" id=".]scI]XJ|rm6sTB0fmmJ">
                                                    <field name="value">2</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id="|JGldcvv,D#s1D^]qqwP">
                                                <field name="name">condition</field>
                                                <value name="value">
                                                  <block type="random_one" id="L$C!@1sCFL@2vV0w@1gvl6F">
                                                    <value name="items">
                                                      <block type="expression" id="p7@2I=]@1w@25xl.@2$.{@2CO">
                                                        <field name="value">['smallest to largest', 'largest to smallest']</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                    <statement name="#props">
                                                      <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                        <field name="#prop"></field>
                                                        <value name="x">
                                                          <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                            <field name="value">400</field>
                                                          </block>
                                                        </value>
                                                        <value name="y">
                                                          <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                            <field name="value">20</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="prop_anchor" id="O.v@2mKn@1K#+tF;|}C}xI">
                                                            <field name="#prop">anchor</field>
                                                            <value name="x">
                                                              <block type="expression" id="+3%GU[X3l%%X`Jv1QL7@2">
                                                                <field name="value">0.5</field>
                                                              </block>
                                                            </value>
                                                            <value name="y">
                                                              <block type="expression" id="Kzp23HjHF7k]A-y1BJPm">
                                                                <field name="value">0</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                                <field name="#prop"></field>
                                                                <value name="contents">
                                                                  <block type="string_value" id="#6btV9p,PGV}wMx.U�">
                                                                    <field name="value">Order the shapes from the ${condition} area.</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                    <field name="base">text</field>
                                                                    <statement name="#props">
                                                                      <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                        <field name="#prop"></field>
                                                                        <value name="fontSize">
                                                                          <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                            <field name="value">36</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                            <field name="#prop"></field>
                                                                            <value name="fill">
                                                                              <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                                <field name="value">black</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                <field name="#prop"></field>
                                                                                <value name="stroke">
                                                                                  <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                    <field name="value">white</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="strokeThickness">
                                                                                      <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                        <field name="value">2</field>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </statement>
                                                    <next>
                                                      <block type="if_then_else_block" id="`3^c[(P0NIMHlyh0O{@25">
                                                        <value name="if">
                                                          <block type="expression" id="3A%o]CJ2@2tF[c]W.a:O6">
                                                            <field name="value">type == 1</field>
                                                          </block>
                                                        </value>
                                                        <statement name="then">
                                                          <block type="if_then_else_block" id="G@19Tl!;kBIg_8~Zdnr_!">
                                                            <value name="if">
                                                              <block type="expression" id="6G]~I%8[E^]~pdokepya">
                                                                <field name="value">range &lt; 4</field>
                                                              </block>
                                                            </value>
                                                            <statement name="then">
                                                              <block type="variable" id="St{};Bpm=mgwjlnE4g}r">
                                                                <field name="name">number</field>
                                                                <value name="value">
                                                                  <block type="random_many" id="bHZYcY@2}s4sR%H0f?#-:">
                                                                    <value name="count">
                                                                      <block type="expression" id="r)77V2l.!{B5wqbv9:/.">
                                                                        <field name="value">3</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="items">
                                                                      <block type="func_array_of_number" id="QyhaudbG}1N74/;w?Ogs" inline="true">
                                                                        <value name="items">
                                                                          <block type="expression" id="]T}vB.6rcY{eEJ7g?5HY">
                                                                            <field name="value">10</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="from">
                                                                          <block type="expression" id="8IGf,9/9!P(5].[gaG6d">
                                                                            <field name="value">1</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </statement>
                                                            <statement name="else">
                                                              <block type="variable" id="/AbO9z{CuK@29):.FAEYQ">
                                                                <field name="name">number</field>
                                                                <value name="value">
                                                                  <block type="random_many" id="7Aa}oLZT$|0Yn@2o^DbE6">
                                                                    <value name="count">
                                                                      <block type="expression" id="3i/i_4(ATY1A6i6)m@2ds">
                                                                        <field name="value">3</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="items">
                                                                      <block type="func_array_of_number" id="a6@2@2xbQ-/{|hB;CG_f95" inline="true">
                                                                        <value name="items">
                                                                          <block type="expression" id="SB@1d]vpIy5=j2!;HI2_j">
                                                                            <field name="value">20</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="from">
                                                                          <block type="expression" id="e_}Gcv352!|Z_Fl)~Re$">
                                                                            <field name="value">11</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </statement>
                                                            <next>
                                                              <block type="variable" id="E5Zrj9u-RdbB.ipJw-NZ">
                                                                <field name="name">color</field>
                                                                <value name="value">
                                                                  <block type="random_many" id="BODfn@2he8IgVC3r`~!m;">
                                                                    <value name="count">
                                                                      <block type="expression" id="6)98!nCql_yHa:cJxlCD">
                                                                        <field name="value">3</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="items">
                                                                      <block type="expression" id="(|{Q.-s:um:TINgH-X%S">
                                                                        <field name="value">[1, 2, 3, 4, 5, 6]</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </statement>
                                                        <statement name="else">
                                                          <block type="variable" id="RB:[.B`K_o0FiE{hQ:@2$">
                                                            <field name="name">number</field>
                                                            <value name="value">
                                                              <block type="func_shuffle" id="y(X`CSIbH`LGZ;Mcf|FU" inline="true">
                                                                <value name="value">
                                                                  <block type="expression" id="Vb+pzq/g|6;roh`09QkB">
                                                                    <field name="value">[1, 2, 3]</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="variable" id="$2OF]OMysRL5/E;wV%y(">
                                                                <field name="name">color</field>
                                                                <value name="value">
                                                                  <block type="random_many" id="AxRT.F;YrcTW@1YslzQN@1">
                                                                    <value name="count">
                                                                      <block type="expression" id="AZ)S|shT/kY|kSQ{9|Q}">
                                                                        <field name="value">3</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="items">
                                                                      <block type="expression" id="Mn}C[}flZ?P0N(t)5(C+">
                                                                        <field name="value">[1, 2, 3, 4]</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </statement>
                                                        <next>
                                                          <block type="statement" id="Gp|@1KJl/!?~NCN@1/Nb@2C">
                                                            <field name="value">var asc = [];

for(var i =0; i&lt;3; i++){
  asc[i] = number[i];
}

var coco = [];

for(var i =0; i&lt;3; i++){
  coco[i] = color[i];
}

if(condition == 'smallest to largest'){
  for(var i =0; i&lt;3; i++){
      for(var j =i; j&lt; 3; j++){
      if(asc[j]&lt;asc[i]){
        var temp = asc[i];
        asc[i] = asc[j];
        asc[j] = temp;

        var temp = coco[i];
        coco[i] = coco[j];
        coco[j] = temp;
      }
    }
  }
}
else{
  for(var i =0; i&lt;3; i++){
    for(var j =i; j&lt; 3; j++){
      if(asc[j]&gt;asc[i]){
        var temp = asc[i];
        asc[i] = asc[j];
        asc[j] = temp;

        var temp = coco[i];
        coco[i] = coco[j];
        coco[j] = temp;
      }
    }
  }
}
                                                            </field>
                                                            <next>
                                                              <block type="if_then_block" id="aGQFnw?QW(T{$@1@2;~lX7">
                                                                <value name="if">
                                                                  <block type="expression" id="|n@1Rh=){VfsrlEHF/kk?">
                                                                    <field name="value">type == 1</field>
                                                                  </block>
                                                                </value>
                                                                <statement name="then">
                                                                  <block type="grid_shape" id="6tU?K^N8ec~A@1@1NwQ72N">
                                                                    <statement name="#props">
                                                                      <block type="prop_grid_dimension" id="X4K:|9dKK%s9l4n5v@2lG">
                                                                        <field name="#prop"></field>
                                                                        <value name="rows">
                                                                          <block type="expression" id="[Ev!.bxnzjc|-33v.%7=">
                                                                            <field name="value">1</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="cols">
                                                                          <block type="expression" id="u$7]/V/!ZWM4(Va??@2({">
                                                                            <field name="value">3</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_position" id="zLld84TfBg#!88]1,JD-">
                                                                            <field name="#prop"></field>
                                                                            <value name="x">
                                                                              <block type="expression" id="Cd6g{Y_P?:GyVyvQi1wJ">
                                                                                <field name="value">400</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="y">
                                                                              <block type="expression" id=".F}E71+@1i)77!yQ`hmfu">
                                                                                <field name="value">150</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_size" id="+o2CWr^.ohx)_bcIG53]">
                                                                                <field name="#prop"></field>
                                                                                <value name="width">
                                                                                  <block type="expression" id="Z[x5iHG%KWC%KWQb}c-G">
                                                                                    <field name="value">780</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="height">
                                                                                  <block type="expression" id="@2E|Ob@1jF#gYtX8-%Iui@1">
                                                                                    <field name="value">130</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_anchor" id="1WJjXY^.h,t]eNgxaJ}q">
                                                                                    <field name="#prop">anchor</field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="DHx[vqs?.?nUD:AYPJWd">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="e3D|RR7w!=Sufu):zV`c">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_grid_cell_source" id="v8+Yu211Qg8fX9Mu4th`">
                                                                                        <field name="#prop">cell.source</field>
                                                                                        <value name="value">
                                                                                          <block type="expression" id="~j@1,^liddf}oDg}E91^i">
                                                                                            <field name="value">[0, 1, 2]</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_grid_random" id="}?kS#x[b)9OGb@1`UdJGV">
                                                                                            <field name="#prop"></field>
                                                                                            <field name="random">FALSE</field>
                                                                                            <next>
                                                                                              <block type="prop_grid_show_borders" id=";y^wk4y%[VWjXOj~vrQZ">
                                                                                                <field name="#prop">#showBorders</field>
                                                                                                <field name="value">FALSE</field>
                                                                                                <next>
                                                                                                  <block type="prop_grid_cell_template" id="7^6]Kv:2yrZi`cooAW2@2">
                                                                                                    <field name="variable">$cell</field>
                                                                                                    <field name="#prop">cell.template</field>
                                                                                                    <field name="#callback">$cell</field>
                                                                                                    <statement name="body">
                                                                                                      <block type="image_shape" id="2BVh#CTTV~=2-R/kGf8f">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_position" id="(Dq9sKU3I!c9r9zhPh#v">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="_$1IUM(N|J_+x`1HE6oI">
                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="}E)h7=[,/!@2AncaHuB=Q">
                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_image_key" id="hRhA{IW$?6v!(bQaIQ!V">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="key">
                                                                                                                  <block type="string_value" id="K8(OcS9PgShV1OODeeb9">
                                                                                                                    <field name="value">shape.png</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_image_src" id="2n?z~g#-@1fS4au5WLq%^">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="src">
                                                                                                                      <block type="string_value" id="mJZ%txRN$zgx}/-bw@2L,">
                                                                                                                        <field name="value">${image_path}shape.png</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="drop_shape" id="_4hK}pbC%wxj#W`rmgeW">
                                                                                                            <field name="multiple">FALSE</field>
                                                                                                            <field name="resultMode">default</field>
                                                                                                            <field name="groupName"></field>
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_value" id="SurV^`j{|PdM@1WVRg!cn">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="U|Etw#HTBY~f[A~DwTgu">
                                                                                                                    <field name="value">asc[$cell.data]</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_position" id="4!;=cRQS$v01;dGlR57l">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="LAKu@1PSAH{0l#zGe|F4D">
                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="S^C)HM.~kIRF5_$gdt`k">
                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_size" id="F$kVR2xvrYDcN@1-z.P_P">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="width">
                                                                                                                          <block type="expression" id="wdp1Kst_wjTYK+OiR,2]">
                                                                                                                            <field name="value">200</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="height">
                                                                                                                          <block type="expression" id="F%]t^Qg2)0lX:Kx#!%%y">
                                                                                                                            <field name="value">118</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="list_shape" id="[6q~/OKKBA:z.I?^a2Ql">
                                                                        <statement name="#props">
                                                                          <block type="prop_list_direction" id="_DO3aceOWU$;nVXr]VZ{">
                                                                            <field name="#prop"></field>
                                                                            <field name="dir">horizontal</field>
                                                                            <next>
                                                                              <block type="prop_position" id="q6tUguj/q/WcKrk}i^@1W">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="aODgzQ4qMlg67+v61,0B">
                                                                                    <field name="value">400</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="T8`Ok@2=@1E})_I-g-[sm^">
                                                                                    <field name="value">240</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_anchor" id="lNdM`Qm?!8Jmp},N,^cP">
                                                                                    <field name="#prop">anchor</field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="BV3gzPmXxfI@1?dZg-Jg7">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="YcEbnIJ}Zq+KFn4,Y$l.">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <statement name="items">
                                                                          <block type="list_item_shape" id="s5R2)Rl+5Ah8!2s{(gA@2">
                                                                            <statement name="#props">
                                                                              <block type="prop_list_align" id="qlCQ[:{1}lA7q`pzcwQW">
                                                                                <field name="#prop"></field>
                                                                                <field name="align">middle</field>
                                                                              </block>
                                                                            </statement>
                                                                            <statement name="template">
                                                                              <block type="image_shape" id="Gu+@1beE+]{,;MYY(80Tw">
                                                                                <statement name="#props">
                                                                                  <block type="prop_image_key" id="ypm5}hKiGca70%xQ^yyF">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="key">
                                                                                      <block type="string_value" id="oUqi^Z{R0U#tEApjQA2H">
                                                                                        <field name="value">${color[0]}.png</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_image_src" id="CkC_qMF(c6OiAQPysj}a">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="src">
                                                                                          <block type="string_value" id="%tU91y,h8#Y4=f=O#m)t">
                                                                                            <field name="value">${image_path}${color[0]}.png</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="list_item_shape" id="A_+3IJ|3xvB6#iCe}?V}">
                                                                                <statement name="#props">
                                                                                  <block type="prop_list_align" id="$toQeNyK:aeq[n;_y[Vn">
                                                                                    <field name="#prop"></field>
                                                                                    <field name="align">middle</field>
                                                                                  </block>
                                                                                </statement>
                                                                                <statement name="template">
                                                                                  <block type="text_shape" id=";i`40i@2Mf]O.@2i)zf?1y">
                                                                                    <statement name="#props">
                                                                                      <block type="prop_text_contents" id=".@16Y/qSU-u%lj;tpKcP(">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="contents">
                                                                                          <block type="expression" id="%,W7l{q#zoNG6U?B!]u@2">
                                                                                            <field name="value">' = 1 cm'</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_style" id="vx1Yom1bpAy2Zux+$q.e">
                                                                                            <field name="base">text</field>
                                                                                            <statement name="#props">
                                                                                              <block type="prop_text_style_font_size" id="Y%rEfT`=yQ@1jLh8B]2@29">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="fontSize">
                                                                                                  <block type="expression" id="AH(Hk#2CpFuJ1A@17J]_C">
                                                                                                    <field name="value">36</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style_fill" id="WihDF]wkB[ol5@1PhTsH!">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="fill">
                                                                                                      <block type="string_value" id="u;bp-r$)KGGcZ`;GZZL{">
                                                                                                        <field name="value">black</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style_stroke" id="3Q~e/Knn+:`:y-CWbha+">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="stroke">
                                                                                                          <block type="string_value" id="{(T$:G^PALg.puX~sX4K">
                                                                                                            <field name="value">white</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style_stroke_thickness" id="dJC$/Ll-_NWXnvMi[uVT">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="strokeThickness">
                                                                                                              <block type="expression" id="?Hc%@2NfQ,63P2PFO.K3T">
                                                                                                                <field name="value">2</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                  </block>
                                                                                </statement>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="text_shape" id="ky@1r7F-7}NRW/396gm+e">
                                                                            <statement name="#props">
                                                                              <block type="prop_position" id="iQ#@2vJL+=`DjX8(Xd!xt">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="VwKrzk[Y,/tj9O5e9II|">
                                                                                    <field name="value">465</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="m+WN}HM=m]_(v9{K=?,g">
                                                                                    <field name="value">235</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_anchor" id="e8)Jh?UqLr].%_T7pyA6">
                                                                                    <field name="#prop">anchor</field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="tr1;o~/OQBrLeV){7[;Z">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="0/7:V3wvMC:29AX/-W34">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_text_contents" id="rrVD1?:5vcO__^UFJUmW">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="contents">
                                                                                          <block type="string_value" id="^~NzBW=@1xE397NF-KXRV">
                                                                                            <field name="value">2</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_style" id="hlFt:HjCWO@2DSN#7=Ugj">
                                                                                            <field name="base">text</field>
                                                                                            <statement name="#props">
                                                                                              <block type="prop_text_style_font_size" id="9XNtDMDJI:H(3z%.S^#@2">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="fontSize">
                                                                                                  <block type="expression" id=".q|NLmOq_Fb#V#)r~r[Q">
                                                                                                    <field name="value">26</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style_fill" id="SZ0c}Wv@28B79F/bt#h@20">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="fill">
                                                                                                      <block type="string_value" id="^j)7$~D$:J++uXSnf41h">
                                                                                                        <field name="value">black</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style_stroke" id=",9#~wn^Bf0p2|W_=yEzn">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="stroke">
                                                                                                          <block type="string_value" id="jYVi=!w0RC~+UDhHa4f_">
                                                                                                            <field name="value">white</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style_stroke_thickness" id="lpxQ;lc?7h9ohEA`=?;l">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="strokeThickness">
                                                                                                              <block type="expression" id="5sKqChL3DAde@1^r0ZhR8">
                                                                                                                <field name="value">2</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="image_shape" id="cd|Hmd)g@2=.zIx#1u_fM">
                                                                                <statement name="#props">
                                                                                  <block type="prop_position" id="x^vQx]5pbE0ad=D1%E%p">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="vQNie!3$KX((Eva(@2Ouz">
                                                                                        <field name="value">400</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="jU6fF5m{-ot|TSn$QqFK">
                                                                                        <field name="value">350</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_image_key" id="@2_pJgI?:gdxA^6s!BHE6">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="key">
                                                                                          <block type="string_value" id="}=A|Z:t@1Y{v2//^Q5iBA">
                                                                                            <field name="value">bigshape.png</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_image_src" id="y?C}Vps~{o`tQU=~~)0I">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="src">
                                                                                              <block type="string_value" id="@2|fg;(hSvs7C]_EkWBFc">
                                                                                                <field name="value">${image_path}bigshape.png</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="grid_shape" id="c?A%#_2E:Y6A|S@1UK_#J">
                                                                                    <statement name="#props">
                                                                                      <block type="prop_grid_dimension" id="PvLqz$.;.q_y.$iZxPgu">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="rows">
                                                                                          <block type="expression" id="Zo+o;QcCNZr@1zJQ#p(q[">
                                                                                            <field name="value">1</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="cols">
                                                                                          <block type="expression" id="px1-BPy=I5mV@1pi$Lbn=">
                                                                                            <field name="value">3</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_position" id="ESpXB`gg{rP+~,zl{$gH">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="~LgI/d{G;p{7NQFy!FB:">
                                                                                                <field name="value">400</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="Gj@2?)}!;O}lm9Y?Y@2#@1!">
                                                                                                <field name="value">370</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_size" id="br+X=z.Q9:f}X-t5OnWN">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="width">
                                                                                                  <block type="expression" id="guHhj}%B@2?b(bbUjkEnH">
                                                                                                    <field name="value">470</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="height">
                                                                                                  <block type="expression" id="nK86^Ke[RpJ1cKweO-j;">
                                                                                                    <field name="value">120</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_anchor" id="VDq+:Vf?;v$?@1^Vo-zD,">
                                                                                                    <field name="#prop">anchor</field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="fiMlo~j^}zwq=!ihmCCQ">
                                                                                                        <field name="value">0.5</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="9^FIW%wm2Bz)%oWDb7Sz">
                                                                                                        <field name="value">0.5</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_cell_source" id="Z/WLj8BA@1N$T8Kr3hc_i">
                                                                                                        <field name="#prop">cell.source</field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id="F+Vi#x-%$1vMp/~/3o)p">
                                                                                                            <field name="value">[0, 1, 2]</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_grid_random" id="7(9`.QhfFVIDd;Z=Q0`y">
                                                                                                            <field name="#prop"></field>
                                                                                                            <field name="random">FALSE</field>
                                                                                                            <next>
                                                                                                              <block type="prop_grid_show_borders" id="7-yY5,@1T7={O=-G$Y3m^">
                                                                                                                <field name="#prop">#showBorders</field>
                                                                                                                <field name="value">FALSE</field>
                                                                                                                <next>
                                                                                                                  <block type="prop_grid_cell_template" id="2ZUvv/rEx/j@1[!jB0Fub">
                                                                                                                    <field name="variable">$cell</field>
                                                                                                                    <field name="#prop">cell.template</field>
                                                                                                                    <field name="#callback">$cell</field>
                                                                                                                    <statement name="body">
                                                                                                                      <block type="variable" id="{vg@2}$oT9hmA[=MZPy+r">
                                                                                                                        <field name="name">data</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id="tB8d,F(wLqN9Sa5ET(|c">
                                                                                                                            <field name="value">number[$cell.data]</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="variable" id="}nly!YMT?j@1.5Ym;xuBt">
                                                                                                                            <field name="name">co</field>
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="u$`W/@1{61(E;fTCyCHOX">
                                                                                                                                <field name="value">color[$cell.data]</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="choice_custom_shape" id="g_Mn~}`10QT|MJZI=9.r">
                                                                                                                                <field name="action">drag</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="Q;8Jp!0KUozLK4DltX+G">
                                                                                                                                    <field name="value">data</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <statement name="template">
                                                                                                                                  <block type="grid_shape" id="(cBJy6%{}|-/C0sp%:S[">
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_grid_dimension" id="|@1t],jpo..pd~^neJ%tV">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="rows">
                                                                                                                                          <block type="expression" id="}B|pcOQ`Hc[s^?@2PTyQS">
                                                                                                                                            <field name="value">Math.floor(data/6) + (data % 6 &gt; 0 ? 1:0)</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="cols">
                                                                                                                                          <block type="expression" id="%YWxKgAs4QjauicT~~%;">
                                                                                                                                            <field name="value">data &gt; 5 ? 6:data</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_position" id="WFu,=x.$a3A~t`pdZ2{u">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id="O_oN!id8|kq!gmNPv?)c">
                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="3I0Gv=~?(aATtL7?d0bo">
                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_grid_cell_size" id="|cS`If==#2=|jO?o%o,5">
                                                                                                                                                <field name="#prop">cell.size</field>
                                                                                                                                                <value name="width">
                                                                                                                                                  <block type="expression" id="l,g!|=c.)m/7G].K@1ea$">
                                                                                                                                                    <field name="value">20</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <value name="height">
                                                                                                                                                  <block type="expression" id="V+aOjIIjXJo#d{%;d(JJ">
                                                                                                                                                    <field name="value">20</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <value name="spacing">
                                                                                                                                                  <block type="expression" id="J4O0e2~8#o~9M?$.zWmp">
                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <value name="radius">
                                                                                                                                                  <block type="expression" id="`SeJ.ZNE2k+fb.)~grEe">
                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_anchor" id="G7#]vuR{bM3[RHz|AsgH">
                                                                                                                                                    <field name="#prop">anchor</field>
                                                                                                                                                    <value name="x">
                                                                                                                                                      <block type="expression" id="KPc3N/OPO!Dfj}KeNb9?">
                                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="y">
                                                                                                                                                      <block type="expression" id="l)p4ottA:T[V#-@2)4mIm">
                                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_grid_cell_source" id="e{G4U=dzzg;_QE8DH#%3">
                                                                                                                                                        <field name="#prop">cell.source</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="C55:[gl=Fi+ZaNA%,!3?">
                                                                                                                                                            <field name="value">data</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_grid_random" id="K:;%e@1%)u!Ayc=TB@1%a%">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <field name="random">FALSE</field>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_grid_show_borders" id="zLd^lyodCoFKXe(/)we[">
                                                                                                                                                                <field name="#prop">#showBorders</field>
                                                                                                                                                                <field name="value">FALSE</field>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_grid_cell_template" id="h/qy:SqsRiiu3L!7H~s2">
                                                                                                                                                                    <field name="variable">$cell</field>
                                                                                                                                                                    <field name="#prop">cell.template</field>
                                                                                                                                                                    <field name="#callback">$cell</field>
                                                                                                                                                                    <statement name="body">
                                                                                                                                                                      <block type="image_shape" id="TnI{k0F$JCPXNCdY@2z#0">
                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                          <block type="prop_position" id="]2jx_?MV5,T@1IUGZ`{}2">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="x">
                                                                                                                                                                              <block type="expression" id="8a3h7?+52fSv}k72a%HQ">
                                                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="y">
                                                                                                                                                                              <block type="expression" id="c~^6Sg)#d%l527Krd?DK">
                                                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_size" id="Nt$,dYiJ~bsLy[+f5(7P">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="width">
                                                                                                                                                                                  <block type="expression" id="UIGARAOs2$wc4[68hUI-">
                                                                                                                                                                                    <field name="value">$cell.width</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <value name="height">
                                                                                                                                                                                  <block type="expression" id="uZIz1H=xa@21J5`bnnxt:">
                                                                                                                                                                                    <field name="value">$cell.height</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_image_key" id="In~m)]Lv`cmt.=Bo@1|+c">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="key">
                                                                                                                                                                                      <block type="string_value" id="Y;l)g-o)J]#@1#wg1+038">
                                                                                                                                                                                        <field name="value">${co}.png</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_image_src" id="jB/g.KM!KvEn1aIrAu?h">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="src">
                                                                                                                                                                                          <block type="string_value" id="g[Flp^Y79+bc6JF%oIY7">
                                                                                                                                                                                            <field name="value">${image_path}${co}.png</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <next>
                                                                                      <block type="partial_explanation" id="Ym,h20bB@1XAnb]H75cGl" inline="true">
                                                                                        <value name="value">
                                                                                          <block type="string_value" id="TIjp:0s|c{%:bC^{,7+^">
                                                                                            <field name="value">&lt;u&gt;Step 1: First, count the number of squares each shape has.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
  .td{width: 30px; height: 30px; border-collapse: collapse;}
.tablex {display: inline-block; margin: 40px}
td{text-align: center}

.stroke {
  color: black;
  border-collapse: collapse;
  font-size: 20px;
  line-box: 20px;
}
b{background: rgb(250, 250, 250, 0.5)}
  &lt;/style&gt;


&lt;center&gt;
    &lt;table style=''&gt;
    &lt;tr&gt;
                                                                                            </field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="variable" id=":jU#`St7!(@1pv73S14V^">
                                                                                            <field name="name">index</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="TJmQv]!P-wN$43U#rA.c">
                                                                                                <field name="value">0</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="while_do_block" id="Pp1yW7l5.9^0u~V#p}Jf">
                                                                                                <value name="while">
                                                                                                  <block type="expression" id="^Gj)Ho4ppIede=DV!jHo">
                                                                                                    <field name="value">index &lt; 3</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <statement name="do">
                                                                                                  <block type="partial_explanation" id="=8RSIVPYb+qA!b2F)@2{J" inline="true">
                                                                                                    <value name="value">
                                                                                                      <block type="string_value" id=":kmAX@13AIkPgpyO|]v-n">
                                                                                                        <field name="value">&lt;td&gt;&lt;table class='tablex'&gt;</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="variable" id="xfPumhG9AsbTCr$xDxz4">
                                                                                                        <field name="name">data</field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id="cb,x7bt2Oc4/u2^66XRw">
                                                                                                            <field name="value">number[index]</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="variable" id="/!g+{wdUO9/S2kubI`^T">
                                                                                                            <field name="name">i</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="]ws:mSPYu?!lOj$MBm,(">
                                                                                                                <field name="value">0</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="while_do_block" id="EZGWOX@1|pND$i!96BMze">
                                                                                                                <value name="while">
                                                                                                                  <block type="expression" id="4+EH[2+KRV#wPYte2E1n">
                                                                                                                    <field name="value">i &lt; Math.floor(data/6) + (data % 6 &gt; 0 ? 1:0)</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <statement name="do">
                                                                                                                  <block type="partial_explanation" id="2EN@2RNRCkJa1IiQxv}N!" inline="true">
                                                                                                                    <value name="value">
                                                                                                                      <block type="string_value" id="E2s47R]Uwm`Mj?oE=lX@2">
                                                                                                                        <field name="value">&lt;tr&gt;</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="variable" id="6tard=EsqShH6Foqp_Ao">
                                                                                                                        <field name="name">j</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id=")Vu?rcV|0!Uh+C!|d~B4">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="while_do_block" id="UMsd-S3OikmDqjq{Y}r=">
                                                                                                                            <value name="while">
                                                                                                                              <block type="expression" id="e%i@1U5@2~eymjgnX~-fT_">
                                                                                                                                <field name="value">j &lt; (data &gt; 5 ? 6:data)</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <statement name="do">
                                                                                                                              <block type="if_then_else_block" id="d2p+@2y42x;k_08gSW/nT">
                                                                                                                                <value name="if">
                                                                                                                                  <block type="expression" id="gj)B[Dk|j3[SMx!IEgHg">
                                                                                                                                    <field name="value">i@26 + j &lt; data</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <statement name="then">
                                                                                                                                  <block type="partial_explanation" id=":i-TwuYmG`urE!6I0H:r" inline="true">
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="string_value" id="}K`d{#REkov?g7ypPBZv">
                                                                                                                                        <field name="value">&lt;td class='td' style='background-image: url("@1sprite.src(${color[index]}.png)"); background-position: center; background-size: 100% 100%'&gt;
&lt;div class='stroke' style='margin: 0px; padding: 0px; height: 20px; width: 20px;'&gt;
${i@26 + j + 1}
&lt;/div&gt;&lt;/td&gt;
                                                                                                                                        </field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <statement name="else">
                                                                                                                                  <block type="partial_explanation" id="yaE4Kx.J/|{JU5t?ZYeF" inline="true">
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="string_value" id="b?yzC++Ya}-A]g_JeMrQ">
                                                                                                                                        <field name="value">&lt;td class='td'&gt;&lt;/td&gt;</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <next>
                                                                                                                                  <block type="variable" id="uIggZa.{{Pq#WReVz./a">
                                                                                                                                    <field name="name">j</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="(}.X^h~614,dmo2eYe-d">
                                                                                                                                        <field name="value">j+1</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="partial_explanation" id="@2jpkB8_(e-?vMn[e%HKX" inline="true">
                                                                                                                                <value name="value">
                                                                                                                                  <block type="string_value" id="+^Bt(_@1)f~u|yDpCc+8W">
                                                                                                                                    <field name="value">&lt;/tr&gt;</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="variable" id=")?gUDE[,I7$!z,7TS#[2">
                                                                                                                                    <field name="name">i</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="n4z+T2YB1},KYbJVOfyU">
                                                                                                                                        <field name="value">i+1</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="partial_explanation" id="j#uZiXPEF^V2@1X.B,SKP" inline="true">
                                                                                                                    <value name="value">
                                                                                                                      <block type="string_value" id="MO8RRYwG,UOi|42rP2.o">
                                                                                                                        <field name="value">&lt;/table&gt;&lt;/td&gt;</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="variable" id="$X-Zjk#~lY{)tJEa+f2_">
                                                                                                                        <field name="name">index</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id="P.vb!MMGL/vvM+y?o}dk">
                                                                                                                            <field name="value">index+1</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="partial_explanation" id="2n]6xj~`p0[~|{@2;eyxU" inline="true">
                                                                                                    <value name="value">
                                                                                                      <block type="string_value" id="e�6g]-t~$21sjK^T;_">
                                                                                                        <field name="value">&lt;/tr&gt;&lt;tr&gt;
  &lt;td&gt;&lt;b&gt;${number[0]}&lt;/b&gt;&lt;/td&gt;&lt;td&gt;&lt;b&gt;${number[1]}&lt;/b&gt;&lt;/td&gt;&lt;td&gt;&lt;b&gt;${number[2]}&lt;/b&gt;&lt;/td&gt;
&lt;/tr&gt;&lt;/table&gt;&lt;/center&gt;
                                                                                                        </field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="end_partial_explanation" id="OT0X1_3WQC$iO_F!{PV{">
                                                                                                        <next>
                                                                                                          <block type="partial_explanation" id="{L/CMSGVeL5k!GI$:g]]" inline="true">
                                                                                                            <value name="value">
                                                                                                              <block type="string_value" id="0UA)0]ug{`(k{d5uuvm1">
                                                                                                                <field name="value">&lt;u&gt;Step 2: Get to know the ordering of areas.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
  .td{width: 30px; height: 30px; border-collapse: collapse;}
.table {display: inline-block; margin: 40px;}
td{text-align: center}

.stroke {
  color: black;
  border-collapse: collapse;
  font-size: 20px;
  line-box: 20px;
}

.bg {background: rgb(250, 250, 250, 0.5)}
  &lt;/style&gt;


&lt;center&gt;
    
    &lt;span class='bg'&gt;${jsUcfirst(condition)}&lt;/br&gt;&lt;/span&gt;
    &lt;table&gt;
    &lt;tr&gt;
                                                                                                                </field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="variable" id="?jQPpGoIs0)d,Z=.%p:N">
                                                                                                                <field name="name">index</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="@1G$Z8k~sSi]mO~!e7,di">
                                                                                                                    <field name="value">0</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="while_do_block" id="VFl]4yFKk~Ls).hqaO3k">
                                                                                                                    <value name="while">
                                                                                                                      <block type="expression" id="jZ`!L?WWQyQqq~vYazE.">
                                                                                                                        <field name="value">index &lt; 3</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <statement name="do">
                                                                                                                      <block type="partial_explanation" id="#UF|YW+t1)n.2j]z0dyk" inline="true">
                                                                                                                        <value name="value">
                                                                                                                          <block type="string_value" id="I+Eb#lSBR$vRXKHyk%7$">
                                                                                                                            <field name="value">&lt;td&gt;&lt;table class='table'&gt;</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="variable" id="t60i!Y`_S@22jK`_fnm}5">
                                                                                                                            <field name="name">data</field>
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="LW|RAda6@2CA!W.tZ`fse">
                                                                                                                                <field name="value">asc[index]</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="variable" id="acTIC}X@1,jhCwTl2dlh.">
                                                                                                                                <field name="name">i</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="O/cLX4Q7SdEgFk/`5fy^">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="while_do_block" id=")K]iz0mdSG@2xp=!C]JtZ">
                                                                                                                                    <value name="while">
                                                                                                                                      <block type="expression" id=";]o2@1XJGS,){2OHirWbJ">
                                                                                                                                        <field name="value">i &lt; Math.floor(data/6) + (data % 6 &gt; 0 ? 1:0)</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <statement name="do">
                                                                                                                                      <block type="partial_explanation" id="Si|xw8];%S1RHi.mQyZr" inline="true">
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="string_value" id="V`c:{vk(d[15ArYyH]R#">
                                                                                                                                            <field name="value">&lt;tr&gt;</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="variable" id="~!V3E/P`%Ay:Y3!z7p7$">
                                                                                                                                            <field name="name">j</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id="Q#4ygNBqTNNeXF$Mk9R7">
                                                                                                                                                <field name="value">0</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="while_do_block" id="eum_MYepEK/H/[qxEpY|">
                                                                                                                                                <value name="while">
                                                                                                                                                  <block type="expression" id="5~Drkv]vCm_@1OX`T!r1Y">
                                                                                                                                                    <field name="value">j &lt; (data &gt; 5 ? 6:data)</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <statement name="do">
                                                                                                                                                  <block type="if_then_else_block" id="6aqjvqy]VZSwDY,+W9^c">
                                                                                                                                                    <value name="if">
                                                                                                                                                      <block type="expression" id="I5@2EQ)C]%?bGE^$d-.A|">
                                                                                                                                                        <field name="value">i@26 + j &lt; data</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <statement name="then">
                                                                                                                                                      <block type="partial_explanation" id="l$$X+/MQ$hVarKpzPLY|" inline="true">
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="string_value" id="HSe4CvS[rv(%+}zEh0vQ">
                                                                                                                                                            <field name="value">&lt;td class='td' style='background-image: url("@1sprite.src(${coco[index]}.png)"); background-position: center; background-size: 100% 100%'&gt;
&lt;/td&gt;
                                                                                                                                                            </field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <statement name="else">
                                                                                                                                                      <block type="partial_explanation" id="qaZ%.Zkji!6CMe,S(JI;" inline="true">
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="string_value" id="HuH#1j|LVg~@2z7@2EIZ[[">
                                                                                                                                                            <field name="value">&lt;td class='td'&gt;&lt;/td&gt;</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="variable" id="_aI#eyL2TzT,X}SU_{9B">
                                                                                                                                                        <field name="name">j</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="/PUm,G}bCk~2{kt:[@1kI">
                                                                                                                                                            <field name="value">j+1</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                                <next>
                                                                                                                                                  <block type="partial_explanation" id="i3wHDQxAm7H+O/vr{EH3" inline="true">
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="string_value" id="JfiPov?-kw@21aQ}Zdbe.">
                                                                                                                                                        <field name="value">&lt;/tr&gt;</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="variable" id="4Q9Dy6Vpu8uc#chSXlEN">
                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="5tCK{i%|TbEN/oY$5@2Oe">
                                                                                                                                                            <field name="value">i+1</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="partial_explanation" id="T}u]ay%h2buN9W^asTq=" inline="true">
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="string_value" id="iMNmB@2zPopcxYG0m^Hjq">
                                                                                                                                            <field name="value">&lt;/table&gt;&lt;/td&gt;</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="variable" id="_:RAF1a^`.Rhw;$p?%_h">
                                                                                                                                            <field name="name">index</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id="5]SVop@2C7/!X]m^#xMSV">
                                                                                                                                                <field name="value">index+1</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="partial_explanation" id="5Yz!8-VCo_?PglV[r]xT" inline="true">
                                                                                                                        <value name="value">
                                                                                                                          <block type="string_value" id="=a091~G.f`F^KTKJa!?!">
                                                                                                                            <field name="value">&lt;/tr&gt;&lt;tr&gt;
  &lt;td&gt;&lt;span class='bg'&gt;${condition == 'smallest to largest' ? 'Smallest':'Largest'}&lt;/span&gt;&lt;/td&gt;
    &lt;td&gt;&lt;/td&gt;
      &lt;td&gt;&lt;span class='bg'&gt;${condition == 'smallest to largest' ? 'Largest':'Smallest'}&lt;/span&gt;&lt;/td&gt;
        
&lt;/tr&gt;&lt;/table&gt;&lt;/center&gt;
                                                                                                                            </field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="end_partial_explanation" id="8^uMTx)]6oY=v,JT`eP(">
                                                                                                                            <next>
                                                                                                                              <block type="partial_explanation" id="_I:RC8kz+|@2|a[naQ44." inline="true">
                                                                                                                                <value name="value">
                                                                                                                                  <block type="string_value" id="@2dRP{+O?oSVY.LjJf@2=)">
                                                                                                                                    <field name="value">&lt;u&gt;Step 3: From the question, look for the ordering focus.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
  .td{width: 30px; height: 30px; border-collapse: collapse;}
.table {display: inline-block; margin: 40px;}
td{text-align: center}

.stroke {
  color: black;
  border-collapse: collapse;
  font-size: 20px;
  line-box: 20px;
}

.bg {background: rgb(250, 250, 250, 0.5)}
  &lt;/style&gt;


    &lt;span class='bg'&gt;Order the shapes below from &lt;b&gt;${condition}&lt;/b&gt; area.&lt;/br&gt;
The correct order of shapes is:&lt;/br&gt;&lt;/span&gt;
&lt;center&gt;
    
    &lt;table&gt;
    &lt;tr&gt;
                                                                                                                                    </field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="variable" id="=wN~!`iAil~{6QmNg]6j">
                                                                                                                                    <field name="name">index</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="QWE!A7n:`gcFMq[3kdyH">
                                                                                                                                        <field name="value">0</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="while_do_block" id="jUQP3K`5aAi8S.eC,8xn">
                                                                                                                                        <value name="while">
                                                                                                                                          <block type="expression" id="JO-GE/tmg[mr!ct]R)1Z">
                                                                                                                                            <field name="value">index &lt; 3</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <statement name="do">
                                                                                                                                          <block type="partial_explanation" id="7!yIL9@1?K;$75xhfR{~V" inline="true">
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="string_value" id="e3b.f~dOawS||f7(jS$1">
                                                                                                                                                <field name="value">&lt;td&gt;&lt;table class='table'&gt;</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="variable" id="fjXeRwqN9XFg25P700ua">
                                                                                                                                                <field name="name">data</field>
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="expression" id="~yrh=.$]aE8^).!X[hiT">
                                                                                                                                                    <field name="value">asc[index]</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="variable" id="5l~oTp$=@1]$K4T^E:n3J">
                                                                                                                                                    <field name="name">i</field>
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="expression" id="~ISX+9M_I!(3:vYQ]!Sm">
                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="while_do_block" id="iUXXNEp![(!oIUOzWgx_">
                                                                                                                                                        <value name="while">
                                                                                                                                                          <block type="expression" id="hW%Wo~x{tbmpf2QP%;.U">
                                                                                                                                                            <field name="value">i &lt; Math.floor(data/6) + (data % 6 &gt; 0 ? 1:0)</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <statement name="do">
                                                                                                                                                          <block type="partial_explanation" id="RV~Qq0F0X8IK+{[I)Ax;" inline="true">
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="string_value" id="MlFFnv:4ixKyY@1@2AmMgo">
                                                                                                                                                                <field name="value">&lt;tr&gt;</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="variable" id="MA-mo{u61NJXR!@1E~g%1">
                                                                                                                                                                <field name="name">j</field>
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="expression" id="k66._rmxeBj#j{_[o8gp">
                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="while_do_block" id="vWo@2%4uE%i`)S8o++OPN">
                                                                                                                                                                    <value name="while">
                                                                                                                                                                      <block type="expression" id="OTU0yRq6}%@2^xO{jQ%Rp">
                                                                                                                                                                        <field name="value">j &lt; (data &gt; 5 ? 6:data)</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <statement name="do">
                                                                                                                                                                      <block type="if_then_else_block" id="L{=@2eweD$6}zNh6vz.#l">
                                                                                                                                                                        <value name="if">
                                                                                                                                                                          <block type="expression" id="z[kAZpBn?(mbr`n=Izuo">
                                                                                                                                                                            <field name="value">i@26 + j &lt; data</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <statement name="then">
                                                                                                                                                                          <block type="partial_explanation" id="+d=MsD`Sovd7@2nn|#qS0" inline="true">
                                                                                                                                                                            <value name="value">
                                                                                                                                                                              <block type="string_value" id="o6`K]mcOv}TRS)8sF-+t">
                                                                                                                                                                                <field name="value">&lt;td class='td' style='background-image: url("@1sprite.src(${coco[index]}.png)"); background-position: center; background-size: 100% 100%'&gt;
&lt;/td&gt;
                                                                                                                                                                                </field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                        <statement name="else">
                                                                                                                                                                          <block type="partial_explanation" id="m1puKb7U-kR(Q`CIe3HI" inline="true">
                                                                                                                                                                            <value name="value">
                                                                                                                                                                              <block type="string_value" id="@2Uf;{D_T}Z?b`EoGUK2K">
                                                                                                                                                                                <field name="value">&lt;td class='td'&gt;&lt;/td&gt;</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="variable" id="!~[(;wBr3-nL[PFo3#wI">
                                                                                                                                                                            <field name="name">j</field>
                                                                                                                                                                            <value name="value">
                                                                                                                                                                              <block type="expression" id="M!P=7AE8o-l4Rz~tmhMZ">
                                                                                                                                                                                <field name="value">j+1</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="partial_explanation" id="M)Yw^`=5Vqaqve=#9sdz" inline="true">
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="string_value" id="R$Gr:c=ZNZ@2G146Z:p5r">
                                                                                                                                                                            <field name="value">&lt;/tr&gt;</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="variable" id="6}kCma?$ftchMOD:Ab8.">
                                                                                                                                                                            <field name="name">i</field>
                                                                                                                                                                            <value name="value">
                                                                                                                                                                              <block type="expression" id="nb?StO%v;k2FX`Gq/9X+">
                                                                                                                                                                                <field name="value">i+1</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="partial_explanation" id="SvGMY}RFKZSP}^ZH@281F" inline="true">
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="string_value" id="X^xf|ULx|`}379_r,Usl">
                                                                                                                                                                <field name="value">&lt;/table&gt;&lt;/td&gt;</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="variable" id="2e{{Ulp37(Io@2zy;0DuB">
                                                                                                                                                                <field name="name">index</field>
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="expression" id="+T{H`!Yj6FG9!oYpsgej">
                                                                                                                                                                    <field name="value">index+1</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <next>
                                                                                                                                          <block type="partial_explanation" id="$2Od2nPH+T9jf:;,79RD" inline="true">
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="string_value" id="`(p6Pf|9bbInK9bTkJjp">
                                                                                                                                                <field name="value">&lt;/tr&gt;&lt;/table&gt;&lt;/center&gt;</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="end_partial_explanation" id="]Urn_]?rbA}n5OY(b[H7"></block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <next>
                                                                  <block type="if_then_block" id="`.ZEHLT6sEGlK?pThfVn">
                                                                    <value name="if">
                                                                      <block type="expression" id="[CKJFjP_xNm}Er?Ioa.R">
                                                                        <field name="value">type == 2</field>
                                                                      </block>
                                                                    </value>
                                                                    <statement name="then">
                                                                      <block type="variable" id="6YBU0!yV^]S;6Z!@1g8D/">
                                                                        <field name="name">itype</field>
                                                                        <value name="value">
                                                                          <block type="random_number" id="3z`k^f{aJI_E|,$?8C)V">
                                                                            <value name="min">
                                                                              <block type="expression" id="S7e5zGvh,c=G1z|Vf(8%">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="max">
                                                                              <block type="expression" id="VC+X53,`.[,({{1w93Wx">
                                                                                <field name="value">7</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="grid_shape" id="|Z7x4-OT4R/;IF{fUODJ">
                                                                            <statement name="#props">
                                                                              <block type="prop_grid_dimension" id="Q{DRxg-SvV@1_P44Z^;1g">
                                                                                <field name="#prop"></field>
                                                                                <value name="rows">
                                                                                  <block type="expression" id=".]fY}+pZXW^B?^rpEkUd">
                                                                                    <field name="value">1</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="cols">
                                                                                  <block type="expression" id="oIH%3IOP_%)992#W}m7a">
                                                                                    <field name="value">3</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_position" id="Ug7M84t7pq7fyD:8?iT6">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="|5Q|wLuIYj@13h7338z_!">
                                                                                        <field name="value">400</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="VIQ6,dLZPF/0Iebs/(SC">
                                                                                        <field name="value">150</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_size" id="9#I3)d!cyW/.:T0ZIqE6">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="width">
                                                                                          <block type="expression" id="m1,wkS,%$:bWHvHBH}u]">
                                                                                            <field name="value">780</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="height">
                                                                                          <block type="expression" id=")v[0HMZ.3=3`-#S|R_+F">
                                                                                            <field name="value">130</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_anchor" id="ZsscU460,%L$_8phE1Jo">
                                                                                            <field name="#prop">anchor</field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="Wb^P7/jZN=-;$D+9AuL=">
                                                                                                <field name="value">0.5</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="OY#W+TN@1g)UCJU@2,?LG#">
                                                                                                <field name="value">0.5</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_grid_cell_source" id="{n|kLs8@2l0@2YR[cuibm,">
                                                                                                <field name="#prop">cell.source</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="k/6m)AK$.:VohNxbtKS)">
                                                                                                    <field name="value">asc</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_grid_random" id="UUUUatZkluQ}1)!dzbP/">
                                                                                                    <field name="#prop"></field>
                                                                                                    <field name="random">FALSE</field>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_show_borders" id="XNw+$K5a2}sAIA0br!S?">
                                                                                                        <field name="#prop">#showBorders</field>
                                                                                                        <field name="value">FALSE</field>
                                                                                                        <next>
                                                                                                          <block type="prop_grid_cell_template" id="|}`2R_C!IadjeuzR=F#@1">
                                                                                                            <field name="variable">$cell</field>
                                                                                                            <field name="#prop">cell.template</field>
                                                                                                            <field name="#callback">$cell</field>
                                                                                                            <statement name="body">
                                                                                                              <block type="image_shape" id="ZKmk3_CM8zfIT[;fd~{Y">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_position" id="M-yzYqeCCm0K9R[$xu0m">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="/XC5FXZ{mE2FF=!De[12">
                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="#LaVnmoG46[,[~P#b~r[">
                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_key" id="zBs-40[?$9:/@1f?o[xH{">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="key">
                                                                                                                          <block type="string_value" id=":I.96o7]AJh:I_};;QaM">
                                                                                                                            <field name="value">shape.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_src" id="!k#3|+Xun/4H,=R-;wb,">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="src">
                                                                                                                              <block type="string_value" id=",@1s;.,#7#QY0}lFT,Kj`">
                                                                                                                                <field name="value">${image_path}shape.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="drop_shape" id="zr{0[a$d/u[o(5S%t2]C">
                                                                                                                    <field name="multiple">FALSE</field>
                                                                                                                    <field name="resultMode">default</field>
                                                                                                                    <field name="groupName"></field>
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_value" id="b`twd^ILWBONE/2Dc@1~0">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id="i0a]b?Hzz2Xu9(j`Y+nw">
                                                                                                                            <field name="value">$cell.data</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_position" id="?ASI7i2Dq@2sQb[%Lrqib">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="m$^aV[ml{C-Y}MY2?Zf4">
                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="p#d2a@2rk,g7dW]?#1ehn">
                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_size" id="O-`YT@1!x=[iU(bA=$1]h">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="width">
                                                                                                                                  <block type="expression" id="+-NBl~9o$_^@1Et0qN2C;">
                                                                                                                                    <field name="value">200</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="height">
                                                                                                                                  <block type="expression" id="b8A1x=wM:G.kYc-KMvkR">
                                                                                                                                    <field name="value">118</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="image_shape" id="rT;R0-b9=T3mXj}1|WX(">
                                                                                <statement name="#props">
                                                                                  <block type="prop_position" id="`M;-2oldA96kk|Z8VSu(">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="Vl;RGM!clk[gt!mNz|st">
                                                                                        <field name="value">400</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="^):Qn]4Q|A#b:UOtJ~6@1">
                                                                                        <field name="value">350</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_image_key" id="vSRQK#rKAB+_IT`J_Xpu">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="key">
                                                                                          <block type="string_value" id="=HaPY;~@2S~[krT7l#2x/">
                                                                                            <field name="value">bigshape.png</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_image_src" id="O0t){@2d6bm~ebM2,usfN">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="src">
                                                                                              <block type="string_value" id="nz,hOfttC8f?Z9k/~7y:">
                                                                                                <field name="value">${image_path}bigshape.png</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="grid_shape" id="lME~Qq[0@2p?4@1[raF$OH">
                                                                                    <statement name="#props">
                                                                                      <block type="prop_grid_dimension" id="_~f-m}Wcz]6kl87n?VDG">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="rows">
                                                                                          <block type="expression" id="ot(9`Tsm5oPAhY}wU.SE">
                                                                                            <field name="value">1</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="cols">
                                                                                          <block type="expression" id="a3EgMsR}[5AMqi@2.U_X?">
                                                                                            <field name="value">3</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_position" id="USwzkIV{GRzX^Zv:TQD;">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="xp-?Us.z87]((a}gx9-L">
                                                                                                <field name="value">400</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="I3#8+6E@2$L`n)]q+.)zz">
                                                                                                <field name="value">370</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_size" id="W4:UK^uUBv-qKY@2TQL`p">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="width">
                                                                                                  <block type="expression" id="aMveC:#hILtLbNvkwenx">
                                                                                                    <field name="value">470</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="height">
                                                                                                  <block type="expression" id="6$.bQbLo3W2#Fb;fL@1-D">
                                                                                                    <field name="value">120</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_anchor" id="SvoIijjNtZ,6j/?DDcsZ">
                                                                                                    <field name="#prop">anchor</field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="crbu8GVxk9@2sEDII5BY[">
                                                                                                        <field name="value">0.5</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="l5+QH{j@2ks7siR(qQ1Os">
                                                                                                        <field name="value">0.5</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_cell_source" id="tn#h`lQbI?_8IUm!qACA">
                                                                                                        <field name="#prop">cell.source</field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id="-aC/dM17FALpJ7`QhZ{q">
                                                                                                            <field name="value">[0, 1, 2]</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_grid_random" id="AYeAv9]2aR?1,cd$#5h1">
                                                                                                            <field name="#prop"></field>
                                                                                                            <field name="random">FALSE</field>
                                                                                                            <next>
                                                                                                              <block type="prop_grid_show_borders" id="t[a2rlvsCSaXjyIFFb=k">
                                                                                                                <field name="#prop">#showBorders</field>
                                                                                                                <field name="value">FALSE</field>
                                                                                                                <next>
                                                                                                                  <block type="prop_grid_cell_template" id="kN~;uEI{(ryC8nlr`ZrY">
                                                                                                                    <field name="variable">$cell</field>
                                                                                                                    <field name="#prop">cell.template</field>
                                                                                                                    <field name="#callback">$cell</field>
                                                                                                                    <statement name="body">
                                                                                                                      <block type="choice_custom_shape" id="^krN7or%UjtH1|9?q?a$">
                                                                                                                        <field name="action">drag</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id="B),q|}cOW{I@12yuqJm:/">
                                                                                                                            <field name="value">number[$cell.data]</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <statement name="template">
                                                                                                                          <block type="image_shape" id=".h4o$W_g@2_Zmm}5D,}4m">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="1PrHwS+[RLs7F@2pSP?R}">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="ocCS:zhYepzq3tP{3FfY">
                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="y2Z(Y[:]h%C`5pvb8o8@2">
                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_key" id="mD`HOyM#baq)vJyY(.%1">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="key">
                                                                                                                                      <block type="string_value" id="9;F2cj=O:LDuc`x.o=ZG">
                                                                                                                                        <field name="value">${number[$cell.data]}_${itype}_${color[$cell.data]}.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_src" id="`[3cdSLbpV{T5jRXg@2%+">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="src">
                                                                                                                                          <block type="string_value" id="Y8-JfYwN#WjO20d2ky%x">
                                                                                                                                            <field name="value">${image_path}${number[$cell.data]}_${itype}_${color[$cell.data]}.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <next>
                                                                                      <block type="partial_explanation" id="V0{4lXGc:x^YSp)b0{d5" inline="true">
                                                                                        <value name="value">
                                                                                          <block type="string_value" id="S8s6rHG;1~-3)%N{/Lbu">
                                                                                            <field name="value">&lt;u&gt;Step 1: First, get to know the ordering of areas&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
  .td{width: 30px; height: 30px; border-collapse: collapse;}
.tablex {display: inline-block; margin: 40px;}
td{text-align: center; vertical-align: bottom; width: 200px}

.stroke {
  color: black;
  border-collapse: collapse;
  font-size: 20px;
  line-box: 20px;
}

.bg {background: rgb(250, 250, 250, 0.5)}
  &lt;/style&gt;


&lt;center&gt;
    
    &lt;span class='bg'&gt;${jsUcfirst(condition)}&lt;/br&gt;&lt;/span&gt;
    &lt;table class='tablex'&gt;
    &lt;tr&gt;
                                                                                            </field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="variable" id="xl-8zUT3PPn1cIbeP{Lj">
                                                                                            <field name="name">index</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="h4}GJcmU^sL{6}lf.F5o">
                                                                                                <field name="value">0</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="while_do_block" id="%!.vZ%iD3Gsrc|;V#mQ{">
                                                                                                <value name="while">
                                                                                                  <block type="expression" id="NGqf@1S4`QIM,@2;^ns9$l">
                                                                                                    <field name="value">index &lt; 3</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <statement name="do">
                                                                                                  <block type="partial_explanation" id="66~Sq@2w6n~tyOVCF4A{v" inline="true">
                                                                                                    <value name="value">
                                                                                                      <block type="string_value" id="sc7K#gup},l:;ZbQLq@22">
                                                                                                        <field name="value">&lt;td&gt;&lt;img src='@1sprite.src(${asc[index]}_${itype}_${coco[index]}.png)'/&gt;&lt;/td&gt;</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="variable" id="9l/r^z$2MuHtTfazxJ9c">
                                                                                                        <field name="name">index</field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id="$wAP7.6GV6`!dJt7upcm">
                                                                                                            <field name="value">index+1</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="partial_explanation" id="LU-+Iu`{Tj0%n`@2Pj4$A" inline="true">
                                                                                                    <value name="value">
                                                                                                      <block type="string_value" id="$P1,wP(2k1YSmBcOs]D^">
                                                                                                        <field name="value">&lt;/tr&gt;&lt;tr&gt;
  &lt;td&gt;&lt;span class='bg'&gt;${condition == 'smallest to largest' ? 'Smallest':'Largest'}&lt;/span&gt;&lt;/td&gt;
    &lt;td&gt;&lt;/td&gt;
      &lt;td&gt;&lt;span class='bg'&gt;${condition == 'smallest to largest' ? 'Largest':'Smallest'}&lt;/span&gt;&lt;/td&gt;
        
&lt;/tr&gt;&lt;/table&gt;&lt;/center&gt;
                                                                                                        </field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="end_partial_explanation" id="cZWGi@1C%X![YL$~A%J}S">
                                                                                                        <next>
                                                                                                          <block type="partial_explanation" id="7FBz.:}KWvZm:ja,dyK@1" inline="true">
                                                                                                            <value name="value">
                                                                                                              <block type="string_value" id="M@1NS4;mQUAAn#_dRup)7">
                                                                                                                <field name="value">&lt;u&gt;Step 2: From the question, look for the ordering focus.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
  .td{width: 30px; height: 30px; border-collapse: collapse;}
.tablex {display: inline-block; margin: 40px;}
td{text-align: center; vertical-align: bottom; width: 200px}

.stroke {
  color: black;
  border-collapse: collapse;
  font-size: 20px;
  line-box: 20px;
}

.bg {background: rgb(250, 250, 250, 0.5)}
  &lt;/style&gt;


    &lt;span class='bg'&gt;Order the shapes below from &lt;b&gt;${condition}&lt;/b&gt; area.&lt;/br&gt;
The correct order of shapes is:&lt;/br&gt;&lt;/br&gt;&lt;/span&gt;
&lt;center&gt;
    
    &lt;table class='tablex'&gt;
    &lt;tr&gt;
                                                                                                                </field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="variable" id="EIS`1.3iIVc]mO+[vLO4">
                                                                                                                <field name="name">index</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="$!L4vO6uisJ8MA9Z~/al">
                                                                                                                    <field name="value">0</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="while_do_block" id="Hv9%]%[^yo%.pNoCE1{E">
                                                                                                                    <value name="while">
                                                                                                                      <block type="expression" id="hENsp{J=tg9^uliWZ/fc">
                                                                                                                        <field name="value">index &lt; 3</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <statement name="do">
                                                                                                                      <block type="partial_explanation" id="BZ};[0+elInu[%ZkRhIU" inline="true">
                                                                                                                        <value name="value">
                                                                                                                          <block type="string_value" id="K|JkM7{Hd1YLGia,UJ:E">
                                                                                                                            <field name="value">&lt;td&gt;&lt;img src='@1sprite.src(${asc[index]}_${itype}_${coco[index]}.png)'/&gt;&lt;/td&gt;</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="variable" id="(),,X#H_Pu!ehvH|fX,3">
                                                                                                                            <field name="name">index</field>
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="-$`]9Zn`kYQ7u}Hj(lgN">
                                                                                                                                <field name="value">index+1</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="partial_explanation" id="+uo~kyVf,2,@1{dp19hA5" inline="true">
                                                                                                                        <value name="value">
                                                                                                                          <block type="string_value" id="S49o%$w,JRI%}..Vr}pA">
                                                                                                                            <field name="value">&lt;/tr&gt;&lt;/table&gt;&lt;/center&gt;</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="end_partial_explanation" id="+�cL,EZ@1|VZs)j9uF7"></block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
</xml>
END_XML]] */