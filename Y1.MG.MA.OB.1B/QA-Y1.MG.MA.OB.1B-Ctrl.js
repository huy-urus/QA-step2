
module.exports = [
  {
    "#type": "question",
    "name": "Y1.MG.MA.OB.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y1.MG.MA.OB.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "range < 0"
        },
        "then": [
          {
            "#type": "variable",
            "name": "num",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "1"
              },
              "max": {
                "#type": "expression",
                "value": "7"
              }
            }
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "range < 4"
            },
            "then": [
              {
                "#type": "variable",
                "name": "num",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "5"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "15"
                  }
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "num",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "10"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "20"
                  }
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "list_object",
        "value": {
          "#type": "string_array",
          "items": "rubber duckie|slippers|beach ball|cup|bowl|school bag|guitar|Teddy bear"
        }
      },
      {
        "#type": "variable",
        "name": "o",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "0"
          },
          "max": {
            "#type": "expression",
            "value": "7"
          }
        }
      },
      {
        "#type": "variable",
        "name": "b",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "4"
          }
        }
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "20"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "How many cubes are used to measure the ${list_object[o]}?"
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "36"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "image_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "240"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "0"
            },
            "height": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "scale.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}scale.png"
          }
        ]
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "1"
            },
            "cols": {
              "#type": "expression",
              "value": "3"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "200"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "430"
            },
            "height": {
              "#type": "expression",
              "value": "50"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "1"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "expression",
              "value": "[0, 1, 2]"
            }
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_cell_template_for",
            "variable": "$cell",
            "condition": "$cell.col == 0",
            "#prop": "cell.templates[]",
            "#callback": "$cell",
            "body": [
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.bottom-3"
                    }
                  },
                  {
                    "#type": "prop_anchor",
                    "#prop": "anchor",
                    "x": {
                      "#type": "expression",
                      "value": "0.5"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "1"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "${o}.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}${o}.png"
                  }
                ]
              }
            ]
          },
          {
            "#type": "prop_grid_cell_template_for",
            "variable": "$cell",
            "condition": "$cell.col == 2",
            "#prop": "cell.templates[]",
            "#callback": "$cell",
            "body": [
              {
                "#type": "grid_shape",
                "#props": [
                  {
                    "#type": "prop_grid_dimension",
                    "#prop": "",
                    "rows": {
                      "#type": "expression",
                      "value": "1"
                    },
                    "cols": {
                      "#type": "expression",
                      "value": "1"
                    }
                  },
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.bottom-3"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "num > 4 ? 150 : 30*num"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "20"
                    }
                  },
                  {
                    "#type": "prop_anchor",
                    "#prop": "anchor",
                    "x": {
                      "#type": "expression",
                      "value": "0.5"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "1"
                    }
                  },
                  {
                    "#type": "prop_grid_cell_source",
                    "#prop": "cell.source",
                    "value": {
                      "#type": "expression",
                      "value": "1"
                    }
                  },
                  {
                    "#type": "prop_grid_random",
                    "#prop": "",
                    "random": false
                  },
                  {
                    "#type": "prop_grid_show_borders",
                    "#prop": "#showBorders",
                    "value": false
                  },
                  {
                    "#type": "prop_grid_cell_template",
                    "variable": "$cell",
                    "#prop": "cell.template",
                    "#callback": "$cell",
                    "body": [
                      {
                        "#type": "variable",
                        "name": "ox",
                        "value": {
                          "#type": "expression",
                          "value": "$cell.centerX - $cell.width/2"
                        }
                      },
                      {
                        "#type": "variable",
                        "name": "oy",
                        "value": {
                          "#type": "expression",
                          "value": "$cell.bottom"
                        }
                      },
                      {
                        "#type": "variable",
                        "name": "x",
                        "value": {
                          "#type": "expression",
                          "value": "ox"
                        }
                      },
                      {
                        "#type": "variable",
                        "name": "y",
                        "value": {
                          "#type": "expression",
                          "value": "oy"
                        }
                      },
                      {
                        "#type": "variable",
                        "name": "i",
                        "value": {
                          "#type": "expression",
                          "value": "0"
                        }
                      },
                      {
                        "#type": "while_do_block",
                        "while": {
                          "#type": "expression",
                          "value": "i < num"
                        },
                        "do": [
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "x"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "y"
                                }
                              },
                              {
                                "#type": "prop_anchor",
                                "#prop": "anchor",
                                "x": {
                                  "#type": "expression",
                                  "value": "0"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "1"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "b${b}.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}b${b}.png"
                              }
                            ]
                          },
                          {
                            "#type": "if_then_else_block",
                            "if": {
                              "#type": "expression",
                              "value": "i % 5 == 4"
                            },
                            "then": [
                              {
                                "#type": "variable",
                                "name": "x",
                                "value": {
                                  "#type": "expression",
                                  "value": "ox"
                                }
                              },
                              {
                                "#type": "variable",
                                "name": "y",
                                "value": {
                                  "#type": "expression",
                                  "value": "y-30"
                                }
                              }
                            ],
                            "else": [
                              {
                                "#type": "variable",
                                "name": "x",
                                "value": {
                                  "#type": "expression",
                                  "value": "x+30"
                                }
                              }
                            ]
                          },
                          {
                            "#type": "variable",
                            "name": "i",
                            "value": {
                              "#type": "expression",
                              "value": "i+1"
                            }
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "list_shape",
        "#props": [
          {
            "#type": "prop_list_direction",
            "#prop": "",
            "dir": "horizontal"
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "350"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          }
        ],
        "items": [
          {
            "#type": "json",
            "#props": [
              {
                "#type": "prop_list_align",
                "#prop": "",
                "align": "middle"
              }
            ],
            "template": {
              "#callback": "$item",
              "variable": "$item",
              "body": [
                {
                  "#type": "text_shape",
                  "#props": [
                    {
                      "#type": "prop_text_contents",
                      "#prop": "",
                      "contents": "Answer:${' '}"
                    },
                    {
                      "#prop": "",
                      "style": {
                        "#type": "json",
                        "base": "text",
                        "#props": [
                          {
                            "#type": "prop_text_style_font_size",
                            "#prop": "",
                            "fontSize": {
                              "#type": "expression",
                              "value": "36"
                            }
                          },
                          {
                            "#type": "prop_text_style_fill",
                            "#prop": "",
                            "fill": "black"
                          },
                          {
                            "#type": "prop_text_style_stroke",
                            "#prop": "",
                            "stroke": "white"
                          },
                          {
                            "#type": "prop_text_style_stroke_thickness",
                            "#prop": "",
                            "strokeThickness": {
                              "#type": "expression",
                              "value": "2"
                            }
                          }
                        ]
                      }
                    }
                  ]
                }
              ]
            }
          },
          {
            "#type": "json",
            "#props": [
              {
                "#type": "prop_list_align",
                "#prop": "",
                "align": "middle"
              }
            ],
            "template": {
              "#callback": "$item",
              "variable": "$item",
              "body": [
                {
                  "#type": "image_shape",
                  "#props": [
                    {
                      "#type": "prop_image_key",
                      "#prop": "",
                      "key": "shape.png"
                    },
                    {
                      "#type": "prop_image_src",
                      "#prop": "",
                      "src": "${image_path}shape.png"
                    }
                  ]
                },
                {
                  "#type": "choice_input_shape",
                  "#props": [
                    {
                      "#type": "prop_value",
                      "#prop": "",
                      "value": {
                        "#type": "expression",
                        "value": "num"
                      }
                    },
                    {
                      "#type": "prop_size",
                      "#prop": "",
                      "width": {
                        "#type": "expression",
                        "value": "115"
                      },
                      "height": {
                        "#type": "expression",
                        "value": "65"
                      }
                    },
                    {
                      "#type": "prop_input_keyboard",
                      "#prop": "",
                      "keyboard": "numbers1"
                    },
                    {
                      "#type": "prop_input_max_length",
                      "#prop": "",
                      "maxLength": {
                        "#type": "expression",
                        "value": "num.toString().length"
                      }
                    },
                    {
                      "#type": "prop_input_result_position",
                      "#prop": "",
                      "resultPosition": "bottom"
                    },
                    {
                      "#type": "prop_tab_order",
                      "#prop": "",
                      "tabOrder": {
                        "#type": "expression",
                        "value": "0"
                      }
                    },
                    {
                      "#type": "prop_stroke",
                      "#prop": "stroke"
                    },
                    {
                      "#type": "prop_fill",
                      "#prop": "fill"
                    },
                    {
                      "#prop": "",
                      "style": {
                        "#type": "json",
                        "base": "text",
                        "#props": [
                          {
                            "#type": "prop_text_style_font_size",
                            "#prop": "",
                            "fontSize": {
                              "#type": "expression",
                              "value": "32"
                            }
                          },
                          {
                            "#type": "prop_text_style_fill",
                            "#prop": "",
                            "fill": "black"
                          },
                          {
                            "#type": "prop_text_style_stroke",
                            "#prop": "",
                            "stroke": "white"
                          },
                          {
                            "#type": "prop_text_style_stroke_thickness",
                            "#prop": "",
                            "strokeThickness": {
                              "#type": "expression",
                              "value": "2"
                            }
                          }
                        ]
                      }
                    }
                  ],
                  "#init": "algorithmic_input"
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "loaf",
        "value": {
          "#type": "custom_image_list",
          "link": "${image_path}",
          "images": "scale|${o}|b${b}"
        }
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<center>\n  <u>Count the number of blocks on the scale.</u>\n</br></br>\n  \n  <style>\n  table{width: 430px }\ntd{text-align: center; vertical-align: bottom;}\n#img{max-width: 140px}\n\n#stroke {\ncolor: ${b == 4 ? 'black':'white'};\n   -webkit-text-stroke-width: 0.2px;\n   -webkit-text-stroke-color: white;\n}\n  </style>\n\n<table>\n  <tr>\n  <td style='width: 155px'><img id='img' src='@sprite.src(${o})'></td>\n<td></td>\n  <td style='width: 160px'>\n    <div style='position: relative; width: ${num > 4 ? 160:(num*30+10)}px; height: ${36 + (num % 5 != 0 ? (num / 5)*30 : (num / 5 - 1)*30)}px;  margin: auto;'>\n      <div style='position: absolute;'>"
        ]
      },
      {
        "#type": "variable",
        "name": "ox",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "variable",
        "name": "oy",
        "value": {
          "#type": "expression",
          "value": "36 + (num % 5 != 0 ? (num / 5)*30 : (num / 5 - 1)*30) - 40"
        }
      },
      {
        "#type": "variable",
        "name": "x",
        "value": {
          "#type": "expression",
          "value": "ox"
        }
      },
      {
        "#type": "variable",
        "name": "y",
        "value": {
          "#type": "expression",
          "value": "oy"
        }
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < num"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<img src='@sprite.src(b${b})' style='position: absolute; top: ${y}px; left: ${x}px;'/>"
            ]
          },
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "i % 5 == 4"
            },
            "then": [
              {
                "#type": "variable",
                "name": "x",
                "value": {
                  "#type": "expression",
                  "value": "ox"
                }
              },
              {
                "#type": "variable",
                "name": "y",
                "value": {
                  "#type": "expression",
                  "value": "y-30"
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "x",
                "value": {
                  "#type": "expression",
                  "value": "x+30"
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</div>"
        ]
      },
      {
        "#type": "variable",
        "name": "ox",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "variable",
        "name": "oy",
        "value": {
          "#type": "expression",
          "value": "36 + (num % 5 != 0 ? (num / 5)*30 : (num / 5 - 1)*30) - 40"
        }
      },
      {
        "#type": "variable",
        "name": "x",
        "value": {
          "#type": "expression",
          "value": "ox"
        }
      },
      {
        "#type": "variable",
        "name": "y",
        "value": {
          "#type": "expression",
          "value": "oy"
        }
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < num"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<span id='stroke' style='position: absolute; top: ${y+5}px; left: ${x+3}px; font-size: 24px'>${i+1}</span>"
            ]
          },
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "i % 5 == 4"
            },
            "then": [
              {
                "#type": "variable",
                "name": "x",
                "value": {
                  "#type": "expression",
                  "value": "ox"
                }
              },
              {
                "#type": "variable",
                "name": "y",
                "value": {
                  "#type": "expression",
                  "value": "y-30"
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "x",
                "value": {
                  "#type": "expression",
                  "value": "x+30"
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</div>\n  </div>\n  </td>\n</tr>\n<tr>\n    <td colspan='3'><img src='@sprite.src(scale)'/></td>\n</tr>\n</table>\n</br>\n<span style='background: rgb(250, 250, 250, 0.4)'>\n  ${num} cube${num > 1 ? 's': ''} are needed to balance the scale.</span>\n</center>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="0" y="0">
    <field name="name">Y1.MG.MA.OB.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y1.MG.MA.OB.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="YepMQT@2uwp2rRPvmnRg1">
                                    <field name="name">range</field>
                                    <value name="value">
                                      <block type="expression" id="UxgQ^Y5]rb}vpw[D=l-!">
                                        <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="if_then_else_block" id="[xE^Qkc7Wdhw7s_wb|7Q">
                                        <value name="if">
                                          <block type="expression" id="U=SiO)vj.nP6x5~2/$zQ">
                                            <field name="value">range &lt; 0</field>
                                          </block>
                                        </value>
                                        <statement name="then">
                                          <block type="variable" id="X2D{Ih@2:BzZUbxTHMXv@2">
                                            <field name="name">num</field>
                                            <value name="value">
                                              <block type="random_number" id="7Gx7gIEv)I5Yv.s%1){)">
                                                <value name="min">
                                                  <block type="expression" id="^E`!)i]]%f(^5ANIHlAO">
                                                    <field name="value">1</field>
                                                  </block>
                                                </value>
                                                <value name="max">
                                                  <block type="expression" id="qDPIf@1779_.gA,H!euxR">
                                                    <field name="value">7</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </value>
                                          </block>
                                        </statement>
                                        <statement name="else">
                                          <block type="if_then_else_block" id="UpbNjHyu}/JzsC2cv|H;">
                                            <value name="if">
                                              <block type="expression" id=";D8HF[m,x%NY^wGL1+Zt">
                                                <field name="value">range &lt; 4</field>
                                              </block>
                                            </value>
                                            <statement name="then">
                                              <block type="variable" id="34lVe-y9|@2DD|J3AhUAD">
                                                <field name="name">num</field>
                                                <value name="value">
                                                  <block type="random_number" id="GQ(7o[HKJ5nKmQ:bQm[8">
                                                    <value name="min">
                                                      <block type="expression" id="ZL5{Xz`^49cU7CEhS@2=R">
                                                        <field name="value">5</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="PX#,ov0%u!E]$F:dch3P">
                                                        <field name="value">15</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                              </block>
                                            </statement>
                                            <statement name="else">
                                              <block type="variable" id="?ooZFxGy0_`W(:B6B;j,">
                                                <field name="name">num</field>
                                                <value name="value">
                                                  <block type="random_number" id="a:DXGjEN40{NCs}dUYLb">
                                                    <value name="min">
                                                      <block type="expression" id="-mGS[97k)#[kydb1?wj^">
                                                        <field name="value">10</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="z9|/JUiSCYY6$Ku]N4`t">
                                                        <field name="value">20</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                              </block>
                                            </statement>
                                          </block>
                                        </statement>
                                        <next>
                                          <block type="variable" id="jKT_A.U{1(H8,6fssa@2l">
                                            <field name="name">list_object</field>
                                            <value name="value">
                                              <block type="string_array" id="!#UkMhzRF-:NS.9u([x2">
                                                <field name="items">rubber duckie|slippers|beach ball|cup|bowl|school bag|guitar|Teddy bear</field>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id="70G2]S_o?^[/MPAT=cWB">
                                                <field name="name">o</field>
                                                <value name="value">
                                                  <block type="random_number" id="QVS]@1$?Y@2H/VVLKP%{!j">
                                                    <value name="min">
                                                      <block type="expression" id="/tS!@1E3mJ++u$;yY8K@23">
                                                        <field name="value">0</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id=")fnokzFYwYWJ#{9B=9Ct">
                                                        <field name="value">7</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="JxvY-M]k@1ulsTp3MwKi?">
                                                    <field name="name">b</field>
                                                    <value name="value">
                                                      <block type="random_number" id="`cOp_B/56|O`@165GDRdy">
                                                        <value name="min">
                                                          <block type="expression" id="VbEgqDVW$z;0nY+mxtcO">
                                                            <field name="value">1</field>
                                                          </block>
                                                        </value>
                                                        <value name="max">
                                                          <block type="expression" id="Qirc%ZB!5-s/^Pp-K8?H">
                                                            <field name="value">4</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                        <statement name="#props">
                                                          <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                            <field name="#prop"></field>
                                                            <value name="x">
                                                              <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                                <field name="value">400</field>
                                                              </block>
                                                            </value>
                                                            <value name="y">
                                                              <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                                <field name="value">20</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="prop_anchor" id="O.v@2mKn@1K#+tF;|}C}xI">
                                                                <field name="#prop">anchor</field>
                                                                <value name="x">
                                                                  <block type="expression" id="+3%GU[X3l%%X`Jv1QL7@2">
                                                                    <field name="value">0.5</field>
                                                                  </block>
                                                                </value>
                                                                <value name="y">
                                                                  <block type="expression" id="Kzp23HjHF7k]A-y1BJPm">
                                                                    <field name="value">0</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                                    <field name="#prop"></field>
                                                                    <value name="contents">
                                                                      <block type="string_value" id="#6btV9p,PGV}wMx.U�">
                                                                        <field name="value">How many cubes are used to measure the ${list_object[o]}?</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                        <field name="base">text</field>
                                                                        <statement name="#props">
                                                                          <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                            <field name="#prop"></field>
                                                                            <value name="fontSize">
                                                                              <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                                <field name="value">36</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                                <field name="#prop"></field>
                                                                                <value name="fill">
                                                                                  <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                                    <field name="value">black</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="stroke">
                                                                                      <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                        <field name="value">white</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="strokeThickness">
                                                                                          <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                            <field name="value">2</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </statement>
                                                        <next>
                                                          <block type="image_shape" id="]}Oj0r|4y@12C42|aFaBl">
                                                            <statement name="#props">
                                                              <block type="prop_position" id="#K(l56tb0;dT`v=-#ici">
                                                                <field name="#prop"></field>
                                                                <value name="x">
                                                                  <block type="expression" id="b:^dF,T8vQ.=$Ef0#[TH">
                                                                    <field name="value">400</field>
                                                                  </block>
                                                                </value>
                                                                <value name="y">
                                                                  <block type="expression" id="T5k;0Zl(/;ZZe#tyXJOy">
                                                                    <field name="value">240</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_size" id="}o87Cp4~Es7,k$gAHJ_A">
                                                                    <field name="#prop"></field>
                                                                    <value name="width">
                                                                      <block type="expression" id="UMD?QO5~3Kx+Y#.2sWgk">
                                                                        <field name="value">0</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="height">
                                                                      <block type="expression" id="(:WE$K3Fj?xR81r=s)@1|">
                                                                        <field name="value">0</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_image_key" id="C@28tM!m.a(deNAwP=/:j">
                                                                        <field name="#prop"></field>
                                                                        <value name="key">
                                                                          <block type="string_value" id="0U(!S.U1@1k+r(Zv^;8;z">
                                                                            <field name="value">scale.png</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_image_src" id="O,a%x8(CkqANJ.-@1/9I/">
                                                                            <field name="#prop"></field>
                                                                            <value name="src">
                                                                              <block type="string_value" id="nky@2i}N.~a;DnikVw2wL">
                                                                                <field name="value">${image_path}scale.png</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </statement>
                                                            <next>
                                                              <block type="grid_shape" id="6tU?K^N8ec~A@1@1NwQ72N">
                                                                <statement name="#props">
                                                                  <block type="prop_grid_dimension" id="X4K:|9dKK%s9l4n5v@2lG">
                                                                    <field name="#prop"></field>
                                                                    <value name="rows">
                                                                      <block type="expression" id="[Ev!.bxnzjc|-33v.%7=">
                                                                        <field name="value">1</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="cols">
                                                                      <block type="expression" id="u$7]/V/!ZWM4(Va??@2({">
                                                                        <field name="value">3</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_position" id="zLld84TfBg#!88]1,JD-">
                                                                        <field name="#prop"></field>
                                                                        <value name="x">
                                                                          <block type="expression" id="Cd6g{Y_P?:GyVyvQi1wJ">
                                                                            <field name="value">400</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id=".F}E71+@1i)77!yQ`hmfu">
                                                                            <field name="value">200</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_size" id="+o2CWr^.ohx)_bcIG53]">
                                                                            <field name="#prop"></field>
                                                                            <value name="width">
                                                                              <block type="expression" id="Z[x5iHG%KWC%KWQb}c-G">
                                                                                <field name="value">430</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="height">
                                                                              <block type="expression" id="@2E|Ob@1jF#gYtX8-%Iui@1">
                                                                                <field name="value">50</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_anchor" id="1WJjXY^.h,t]eNgxaJ}q">
                                                                                <field name="#prop">anchor</field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="DHx[vqs?.?nUD:AYPJWd">
                                                                                    <field name="value">0.5</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="e3D|RR7w!=Sufu):zV`c">
                                                                                    <field name="value">1</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_grid_cell_source" id="v8+Yu211Qg8fX9Mu4th`">
                                                                                    <field name="#prop">cell.source</field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="~j@1,^liddf}oDg}E91^i">
                                                                                        <field name="value">[0, 1, 2]</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_grid_random" id="}?kS#x[b)9OGb@1`UdJGV">
                                                                                        <field name="#prop"></field>
                                                                                        <field name="random">FALSE</field>
                                                                                        <next>
                                                                                          <block type="prop_grid_show_borders" id=";y^wk4y%[VWjXOj~vrQZ">
                                                                                            <field name="#prop">#showBorders</field>
                                                                                            <field name="value">FALSE</field>
                                                                                            <next>
                                                                                              <block type="prop_grid_cell_template_for" id=".fzhp@1L+GRe_E/q}1T!E">
                                                                                                <field name="variable">$cell</field>
                                                                                                <field name="condition">$cell.col == 0</field>
                                                                                                <field name="#prop">cell.templates[]</field>
                                                                                                <field name="#callback">$cell</field>
                                                                                                <statement name="body">
                                                                                                  <block type="image_shape" id="?|OJ,/C}hye@1^Rh%AMRE">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_position" id="0uU2BVOY1Mm-ea5FJ-U#">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="x">
                                                                                                          <block type="expression" id="K-Ka);G=aGn5~3Ba]6U#">
                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id="Ypwfx{OPDR96[6b90_0Q">
                                                                                                            <field name="value">$cell.bottom-3</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_anchor" id="[vv0M:x^)j`(27lbznNq">
                                                                                                            <field name="#prop">anchor</field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="iU{.8|3(/,^|B@1xOSRc{">
                                                                                                                <field name="value">0.5</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="S|Zce.P%[DFH|cS]55dc">
                                                                                                                <field name="value">1</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_image_key" id="kZGuMIAD^]gu@2xGf=ZwY">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="key">
                                                                                                                  <block type="string_value" id="oKE#?M_zE.Zd;o5{2!?a">
                                                                                                                    <field name="value">${o}.png</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_image_src" id="M~uEyKFgF,6`SuWziuvZ">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="src">
                                                                                                                      <block type="string_value" id="eUrrH+q3LQCxG7lNVFn4">
                                                                                                                        <field name="value">${image_path}${o}.png</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="prop_grid_cell_template_for" id="ROYUAd;]acFS#^dHzPir">
                                                                                                    <field name="variable">$cell</field>
                                                                                                    <field name="condition">$cell.col == 2</field>
                                                                                                    <field name="#prop">cell.templates[]</field>
                                                                                                    <field name="#callback">$cell</field>
                                                                                                    <statement name="body">
                                                                                                      <block type="grid_shape" id="cI|6s`ZU|Gvf{q,8oN-a">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_grid_dimension" id="qXZa#kEXSMKYPJgx9AA1">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="rows">
                                                                                                              <block type="expression" id="U7A]13hm4eR|dFmU!/CO">
                                                                                                                <field name="value">1</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="cols">
                                                                                                              <block type="expression" id="7]f@2wYu?{H^c%ApLCvTN">
                                                                                                                <field name="value">1</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_position" id="7qm|uD[;+1|Q^)g2w/g0">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="i}`3%qBDHhw`R%w;1UvL">
                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="C-@1l@21SilMv0XD~ab83i">
                                                                                                                    <field name="value">$cell.bottom-3</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_size" id="yy^4bmuphK;{kBWGo+;v">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="width">
                                                                                                                      <block type="expression" id="LP~{w@29g6:HT(kcl-BSh">
                                                                                                                        <field name="value">num &gt; 4 ? 150 : 30@2num</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="height">
                                                                                                                      <block type="expression" id="ivmXb?q)pyASc|T@2_sS:">
                                                                                                                        <field name="value">20</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_anchor" id="iv@1zt_p@2vyM8)AXI-w/j">
                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="LWUjg|/%S5;J-zGVo@2Lj">
                                                                                                                            <field name="value">0.5</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="3zdUx3.0J86}CEBvpLwv">
                                                                                                                            <field name="value">1</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_grid_cell_source" id="J8P8-)#od?ngmp=5?Wew">
                                                                                                                            <field name="#prop">cell.source</field>
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="yO}8[kF0w@1O|_]PfpMk(">
                                                                                                                                <field name="value">1</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_grid_random" id="^iGP`NV^X+U9JT9@29E%9">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <field name="random">FALSE</field>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_grid_show_borders" id="IemXYc|`o@1y/I@1Fp8@1?#">
                                                                                                                                    <field name="#prop">#showBorders</field>
                                                                                                                                    <field name="value">FALSE</field>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_grid_cell_template" id="7^6]Kv:2yrZi`cooAW2@2">
                                                                                                                                        <field name="variable">$cell</field>
                                                                                                                                        <field name="#prop">cell.template</field>
                                                                                                                                        <field name="#callback">$cell</field>
                                                                                                                                        <statement name="body">
                                                                                                                                          <block type="variable" id=".XK53G%$L/LHkCW{ao4S">
                                                                                                                                            <field name="name">ox</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id="f%@1JFNO9uj#iw@2da@2T=t">
                                                                                                                                                <field name="value">$cell.centerX - $cell.width/2</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="variable" id="`@2-{MP;oX7.:umw;eI-L">
                                                                                                                                                <field name="name">oy</field>
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="expression" id="u8qTJ@13jbQPUGj5Hg$]{">
                                                                                                                                                    <field name="value">$cell.bottom</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="variable" id="i(61poaAqo(pu#9Y+9IO">
                                                                                                                                                    <field name="name">x</field>
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="expression" id="6=:YS(tt3,)c3iYS}bH7">
                                                                                                                                                        <field name="value">ox</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="variable" id="kApp=Nnrc%R,98v(]Z7^">
                                                                                                                                                        <field name="name">y</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="KdX.pII;8!2Y[FhQ@1%nz">
                                                                                                                                                            <field name="value">oy</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="variable" id="47Ok`F=9)SoW1=H[pS,U">
                                                                                                                                                            <field name="name">i</field>
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id="R4^Kw1RD.N1eqcU4SmYU">
                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="while_do_block" id="9y@2%uK]MUg$fA130bYGz">
                                                                                                                                                                <value name="while">
                                                                                                                                                                  <block type="expression" id="^?hD}#?�.Q]!=JyC@2i">
                                                                                                                                                                    <field name="value">i &lt; num</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <statement name="do">
                                                                                                                                                                  <block type="image_shape" id="@1r0[pVB2mTkkqA8(TvgL">
                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                      <block type="prop_position" id="rK5i46~Us?C;-1+)?q`2">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="x">
                                                                                                                                                                          <block type="expression" id="{9NTP$RlJNr7OeKQH)(g">
                                                                                                                                                                            <field name="value">x</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <value name="y">
                                                                                                                                                                          <block type="expression" id="}zWC4aZ0Xe`p1[h(^2/e">
                                                                                                                                                                            <field name="value">y</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_anchor" id="5{x41!s?n,k6E.1AS@1vd">
                                                                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                                                                            <value name="x">
                                                                                                                                                                              <block type="expression" id="7FItbi]8iOv,H#p[E?!)">
                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="y">
                                                                                                                                                                              <block type="expression" id="h1%4X{;Z8n9}5RU/d4e1">
                                                                                                                                                                                <field name="value">1</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_image_key" id="4Jn{m,~~wRBxgbzMX5uD">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="key">
                                                                                                                                                                                  <block type="string_value" id=")hNuD`tA!7nc]!,ajcLx">
                                                                                                                                                                                    <field name="value">b${b}.png</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_image_src" id="}?tX7+l5Q5l$]_K!-=!$">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="src">
                                                                                                                                                                                      <block type="string_value" id="yY6z!QW)!:G,DiKMW^R(">
                                                                                                                                                                                        <field name="value">${image_path}b${b}.png</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="if_then_else_block" id="SEl1Yzk2][LPZxI80YjP">
                                                                                                                                                                        <value name="if">
                                                                                                                                                                          <block type="expression" id="YRTb_0jCki[dGy(yoc^r">
                                                                                                                                                                            <field name="value">i % 5 == 4</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <statement name="then">
                                                                                                                                                                          <block type="variable" id="kU|Sk62zoXYHEHw5f!7F">
                                                                                                                                                                            <field name="name">x</field>
                                                                                                                                                                            <value name="value">
                                                                                                                                                                              <block type="expression" id="K;}2k=#Np)#KtFL{55+%">
                                                                                                                                                                                <field name="value">ox</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="variable" id="O)xpDd7jKiD+mP%.U55m">
                                                                                                                                                                                <field name="name">y</field>
                                                                                                                                                                                <value name="value">
                                                                                                                                                                                  <block type="expression" id="^$frar53E}rhyZ}/9H9@2">
                                                                                                                                                                                    <field name="value">y-30</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                        <statement name="else">
                                                                                                                                                                          <block type="variable" id="$)8?BYaM`hDYI0N_ey7o">
                                                                                                                                                                            <field name="name">x</field>
                                                                                                                                                                            <value name="value">
                                                                                                                                                                              <block type="expression" id="?F%arX?NOpXTASWDnW1A">
                                                                                                                                                                                <field name="value">x+30</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="variable" id="AA(F,[vAZX?W]Jt/fnFM">
                                                                                                                                                                            <field name="name">i</field>
                                                                                                                                                                            <value name="value">
                                                                                                                                                                              <block type="expression" id="UK}a})fbV;nr]@25k1Yy%">
                                                                                                                                                                                <field name="value">i+1</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <next>
                                                                  <block type="list_shape" id="JRoc{%zIRir(@2sJETus)">
                                                                    <statement name="#props">
                                                                      <block type="prop_list_direction" id="aYF}5X4dIN-meZ#jlEni">
                                                                        <field name="#prop"></field>
                                                                        <field name="dir">horizontal</field>
                                                                        <next>
                                                                          <block type="prop_position" id="z#4Q;j~yPga.y58g%%~R">
                                                                            <field name="#prop"></field>
                                                                            <value name="x">
                                                                              <block type="expression" id="|GFMaEVYLAR@2@2_9GON=x">
                                                                                <field name="value">400</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="y">
                                                                              <block type="expression" id="f^jpVu59l|as@1Qt;cl;,">
                                                                                <field name="value">350</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_anchor" id="25H|SXMTgb_Zu0;?!Ucp">
                                                                                <field name="#prop">anchor</field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="mXerl{Bz-8bm3r2dtB#:">
                                                                                    <field name="value">0.5</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="_Z{D),Qcs)eKZkF2h-5O">
                                                                                    <field name="value">0.5</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <statement name="items">
                                                                      <block type="list_item_shape" id="(xf1D/bWpworVctjd5_a">
                                                                        <statement name="#props">
                                                                          <block type="prop_list_align" id="`,A:g^izGVyjKvQ=y3YV">
                                                                            <field name="#prop"></field>
                                                                            <field name="align">middle</field>
                                                                          </block>
                                                                        </statement>
                                                                        <statement name="template">
                                                                          <block type="text_shape" id="8FOWf/5{x|l45JPQ[tG1">
                                                                            <statement name="#props">
                                                                              <block type="prop_text_contents" id="pQ!ysK$_-I)j`%3])^j(">
                                                                                <field name="#prop"></field>
                                                                                <value name="contents">
                                                                                  <block type="string_value" id="dTK`0x5]kieaXFb1HB{x">
                                                                                    <field name="value">Answer:${' '}</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_text_style" id="hGl`dwcMCKX+K]`rD_g5">
                                                                                    <field name="base">text</field>
                                                                                    <statement name="#props">
                                                                                      <block type="prop_text_style_font_size" id="2j+Tzr],@17@2BddU!c(xJ">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="fontSize">
                                                                                          <block type="expression" id="I26hb@2FD)pHa7+6;H#7V">
                                                                                            <field name="value">36</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_style_fill" id="5qMPNsv8C]cze;)IBxms">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="fill">
                                                                                              <block type="string_value" id="DL2C9#s+8@2T7gZD~-4uQ">
                                                                                                <field name="value">black</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_style_stroke" id="sxMr6doIU7jpJB[:Bg3e">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="stroke">
                                                                                                  <block type="string_value" id="@1[7yN1`6Jk/R!xsS2c{h">
                                                                                                    <field name="value">white</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style_stroke_thickness" id="oZrE^Ya)w1Ss~4AltPf-">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="strokeThickness">
                                                                                                      <block type="expression" id="Ds}XIOsq?XnJHw_H5[1J">
                                                                                                        <field name="value">2</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="list_item_shape" id="(pn1r5bz]V`5_a~$,A:p">
                                                                            <statement name="#props">
                                                                              <block type="prop_list_align" id="!wzS^;Dk{tbpg82(.m(:">
                                                                                <field name="#prop"></field>
                                                                                <field name="align">middle</field>
                                                                              </block>
                                                                            </statement>
                                                                            <statement name="template">
                                                                              <block type="image_shape" id="YV?wVQD5syyU4qIg%yX~">
                                                                                <statement name="#props">
                                                                                  <block type="prop_image_key" id="WF.U^y{dnu}RjbW/#p@2n">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="key">
                                                                                      <block type="string_value" id=":psqpBZWy9~/$~mR8+)Z">
                                                                                        <field name="value">shape.png</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_image_src" id="9h[%s{NZNT|6vp:;~S}A">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="src">
                                                                                          <block type="string_value" id="ERP@2?{00(H-u:?8nkUAs">
                                                                                            <field name="value">${image_path}shape.png</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="algorithmic_input_shape" id="[e}LcmqF@19.%T?]@1jB-Z">
                                                                                    <statement name="#props">
                                                                                      <block type="prop_value" id="TN6clfe}:4H;@1k0w%+`S">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="value">
                                                                                          <block type="expression" id="@1Y0=VVDuz^gR8?]A.GgV">
                                                                                            <field name="value">num</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_size" id="~)S$^+Glx|;{-)@1m?MCt">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="width">
                                                                                              <block type="expression" id="wdRRw?lccA]3huvks^0:">
                                                                                                <field name="value">115</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="height">
                                                                                              <block type="expression" id="FlQ@1R-hpt5|W3YEsjl5h">
                                                                                                <field name="value">65</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_input_keyboard" id="pX[+1]_P03Uw?r6RhxOm">
                                                                                                <field name="#prop"></field>
                                                                                                <field name="keyboard">numbers1</field>
                                                                                                <next>
                                                                                                  <block type="prop_input_max_length" id="e^196@2S|6ukB7~2Bxi9C">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="maxLength">
                                                                                                      <block type="expression" id="/_E;-7``ONG+]nS_NxH+">
                                                                                                        <field name="value">num.toString().length</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_input_result_position" id="#LH4fkex}OZhG#[@2d;3P">
                                                                                                        <field name="#prop"></field>
                                                                                                        <field name="resultPosition">bottom</field>
                                                                                                        <next>
                                                                                                          <block type="prop_tab_order" id="s|F@1Izb{|@2lRY,asD!hu">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="tabOrder">
                                                                                                              <block type="expression" id="2[il+_dgVQKQm?SvtFDj">
                                                                                                                <field name="value">0</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_stroke" id="D`nCliS/s1emx9euC0}t">
                                                                                                                <field name="#prop">stroke</field>
                                                                                                                <next>
                                                                                                                  <block type="prop_fill" id="Dz-AaH/!wx?BO[MH9dIz">
                                                                                                                    <field name="#prop">fill</field>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style" id="!1Qr3{IxD@1PMJ]G^LlcN">
                                                                                                                        <field name="base">text</field>
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_text_style_font_size" id="!(nlSpda9T+o/iXBj}YF">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="fontSize">
                                                                                                                              <block type="expression" id="FQTZZB_+$@1-hM?K.!NSO">
                                                                                                                                <field name="value">32</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_fill" id="w-4`M-zsemYrO[}g`_|q">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="fill">
                                                                                                                                  <block type="string_value" id="G;VmNGg}u8OZR_#3M_Oe">
                                                                                                                                    <field name="value">black</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_stroke" id="ahppz0%Y{eD@2C$!vyJ4m">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="stroke">
                                                                                                                                      <block type="string_value" id="}KNcD$1%M)+@1a=7A%7,,">
                                                                                                                                        <field name="value">white</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="TBWlBG{V@2q@2LOjXi7|@16">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="strokeThickness">
                                                                                                                                          <block type="expression" id="bs_awM@2W;X@1OG#fQ+@2no">
                                                                                                                                            <field name="value">2</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="variable" id="~vG^v20aJq9Kv(`y)R:E">
                                                                        <field name="name">loaf</field>
                                                                        <value name="value">
                                                                          <block type="custom_image_list" id="%9KixYhb)mxSyN%Kn=o1">
                                                                            <field name="link">${image_path}</field>
                                                                            <field name="images">scale|${o}|b${b}</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="partial_explanation" id="Mvk086wjZ.U62$QDgUH+" inline="true">
                                                                            <value name="value">
                                                                              <block type="string_value" id="V:o`|J~^bdQ24$!ZRc8e">
                                                                                <field name="value">&lt;center&gt;
  &lt;u&gt;Count the number of blocks on the scale.&lt;/u&gt;
&lt;/br&gt;&lt;/br&gt;
  
  &lt;style&gt;
  table{width: 430px }
td{text-align: center; vertical-align: bottom;}
#img{max-width: 140px}

#stroke {
color: ${b == 4 ? 'black':'white'};
   -webkit-text-stroke-width: 0.2px;
   -webkit-text-stroke-color: white;
}
  &lt;/style&gt;

&lt;table&gt;
  &lt;tr&gt;
  &lt;td style='width: 155px'&gt;&lt;img id='img' src='@1sprite.src(${o})'&gt;&lt;/td&gt;
&lt;td&gt;&lt;/td&gt;
  &lt;td style='width: 160px'&gt;
    &lt;div style='position: relative; width: ${num &gt; 4 ? 160:(num@230+10)}px; height: ${36 + (num % 5 != 0 ? (num / 5)@230 : (num / 5 - 1)@230)}px;  margin: auto;'&gt;
      &lt;div style='position: absolute;'&gt;
                                                                                </field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="variable" id="tkuA4YbX01TzjO7(-C-m">
                                                                                <field name="name">ox</field>
                                                                                <value name="value">
                                                                                  <block type="expression" id="-zYmp)[a~e?!~g$aUHkg">
                                                                                    <field name="value">0</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="variable" id="^Cb.lC{lv~,{@2qY-qF@2t">
                                                                                    <field name="name">oy</field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="=AKf(|$woz$~@1=z@1{{2R">
                                                                                        <field name="value">36 + (num % 5 != 0 ? (num / 5)@230 : (num / 5 - 1)@230) - 40</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="variable" id="qcGC]:BrfcVp0^-rG.Bv">
                                                                                        <field name="name">x</field>
                                                                                        <value name="value">
                                                                                          <block type="expression" id="O9Z7L![.)D`VsN0bMORG">
                                                                                            <field name="value">ox</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="variable" id="dA`4H7,=AaXs.jk_(?XO">
                                                                                            <field name="name">y</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="zoQ%zNO}5_b8d_u#J(z{">
                                                                                                <field name="value">oy</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="variable" id="Gk[na0,i8XePPqx.@1_s#">
                                                                                                <field name="name">i</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="1x_0T/bTKh!c}@1m(Y]v=">
                                                                                                    <field name="value">0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="while_do_block" id="4Yz|RVbsT%.w8LK$Iz#M">
                                                                                                    <value name="while">
                                                                                                      <block type="expression" id="TM,T2~9;Pz:tPg|K@1gCq">
                                                                                                        <field name="value">i &lt; num</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <statement name="do">
                                                                                                      <block type="partial_explanation" id="oM(b:7$(HJ-l4[FqXslQ" inline="true">
                                                                                                        <value name="value">
                                                                                                          <block type="string_value" id="/dEeaO-+9^W)!WavHIGb">
                                                                                                            <field name="value">&lt;img src='@1sprite.src(b${b})' style='position: absolute; top: ${y}px; left: ${x}px;'/&gt;</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="if_then_else_block" id="0cww6Xh}3%QSGK}U3[OQ">
                                                                                                            <value name="if">
                                                                                                              <block type="expression" id="ex)xbu;aLi~MTn/2D1pU">
                                                                                                                <field name="value">i % 5 == 4</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <statement name="then">
                                                                                                              <block type="variable" id="fh)jO|kv1_Ur=qFJ_C24">
                                                                                                                <field name="name">x</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="5VRs|AzCO1(H~JUu}}zN">
                                                                                                                    <field name="value">ox</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="variable" id="u27A#-.19k-wcV@11~cz[">
                                                                                                                    <field name="name">y</field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="%o3#bi?Xdw$@26@2}jaG^p">
                                                                                                                        <field name="value">y-30</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <statement name="else">
                                                                                                              <block type="variable" id="xXUi]Zg3+?WkJs2s}2OE">
                                                                                                                <field name="name">x</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="JwVqv{mi5OvyRMb)h9g`">
                                                                                                                    <field name="value">x+30</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="variable" id="nJJQ5A#JbBS_9]v1))W?">
                                                                                                                <field name="name">i</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="WZ7R(emAqxMHOTsg[pYN">
                                                                                                                    <field name="value">i+1</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="partial_explanation" id="@2U4Y$?(;C01!mgC#qx1?" inline="true">
                                                                                                        <value name="value">
                                                                                                          <block type="string_value" id="dgjeGoJf_Bj0d_43|O97">
                                                                                                            <field name="value">&lt;/div&gt;</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="variable" id=",I=Ekr+@1W.cYf@1||7+pf">
                                                                                                            <field name="name">ox</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="Hh^kI!DI8JyAMCshOwUv">
                                                                                                                <field name="value">0</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="variable" id="RJe)AdK02-F)?=wZwd+f">
                                                                                                                <field name="name">oy</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="pU$r)2Q1:pXsgClF:~nE">
                                                                                                                    <field name="value">36 + (num % 5 != 0 ? (num / 5)@230 : (num / 5 - 1)@230) - 40</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="variable" id="dGBuT}3ICr}f[{.Z-q)o">
                                                                                                                    <field name="name">x</field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="CV2]PGbV,[sqW[j`ir8D">
                                                                                                                        <field name="value">ox</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="variable" id="0/UQ/JVS`KPI/_QL?6;0">
                                                                                                                        <field name="name">y</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id="X-;y(`jw|TvX$_^.sQ06">
                                                                                                                            <field name="value">oy</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="variable" id="_LtrpQPzXjR_7bV-W6]D">
                                                                                                                            <field name="name">i</field>
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="XXi]18bT?J;$hPh(7:is">
                                                                                                                                <field name="value">0</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="while_do_block" id="%OH-L#z5E_EoU3IRnN3T">
                                                                                                                                <value name="while">
                                                                                                                                  <block type="expression" id="w=){ISx^Q8E@1Tn`2ZyXY">
                                                                                                                                    <field name="value">i &lt; num</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <statement name="do">
                                                                                                                                  <block type="partial_explanation" id="9ebYHk7_0_:G;ACkk/%{" inline="true">
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="string_value" id="DG_vmVf@1^m^8Y;ca!A:M">
                                                                                                                                        <field name="value">&lt;span id='stroke' style='position: absolute; top: ${y+5}px; left: ${x+3}px; font-size: 24px'&gt;${i+1}&lt;/span&gt;</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="if_then_else_block" id="m?+g7yS_qqDZ_}-H3[v.">
                                                                                                                                        <value name="if">
                                                                                                                                          <block type="expression" id="(5LgZ=8m:!4Dl}PUFPA|">
                                                                                                                                            <field name="value">i % 5 == 4</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <statement name="then">
                                                                                                                                          <block type="variable" id="wP@2cQM`ks/VuSo38CKmx">
                                                                                                                                            <field name="name">x</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id="ocbNH!$0#=CfNO?lxIiS">
                                                                                                                                                <field name="value">ox</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="variable" id="HHA0K5g{y.dyhcB|1@1+d">
                                                                                                                                                <field name="name">y</field>
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="expression" id="_n4BxZ~5T9a+AH2lO7t/">
                                                                                                                                                    <field name="value">y-30</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <statement name="else">
                                                                                                                                          <block type="variable" id="tEzuyarwG]xNYl^@2,^/G">
                                                                                                                                            <field name="name">x</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id=")be}PpvH?+{bD7}A_gu.">
                                                                                                                                                <field name="value">x+30</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <next>
                                                                                                                                          <block type="variable" id="WMt2B=,gulK8!jUf0CZS">
                                                                                                                                            <field name="name">i</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id="XEk{(n^F~+Z14!5/G!R5">
                                                                                                                                                <field name="value">i+1</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <next>
                                                                                                                                  <block type="partial_explanation" id="mhPQUgJfAjVP){4KEnh~" inline="true">
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="string_value" id="5@1-9mo36KpQM0bn;Zfq8">
                                                                                                                                        <field name="value">&lt;/div&gt;
  &lt;/div&gt;
  &lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
    &lt;td colspan='3'&gt;&lt;img src='@1sprite.src(scale)'/&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;/br&gt;
&lt;span style='background: rgb(250, 250, 250, 0.4)'&gt;
  ${num} cube${num &gt; 1 ? 's': ''} are needed to balance the scale.&lt;/span&gt;
&lt;/center&gt;
                                                                                                                                        </field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="end_partial_explanation" id="?h@1KKTO6ciuwCOFtnb/X"></block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
</xml>
END_XML]] */