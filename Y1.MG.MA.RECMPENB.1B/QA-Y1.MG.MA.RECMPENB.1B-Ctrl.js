
module.exports = [
  {
    "#type": "question",
    "name": "Y1.MG.MA.RECMPENB.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y1.MG.MA.RECMPENB.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "variable",
        "name": "type",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "type[0]",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "13"
          }
        }
      },
      {
        "#type": "variable",
        "name": "type[1]",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "13"
          }
        }
      },
      {
        "#type": "variable",
        "name": "h_item",
        "value": {
          "#type": "string_array",
          "items": "emtri|Chair|Electronic keyboard|Fish tank|Flower in a pot|Flower in a pot|Guitar|Hammer|Laptop|Radio|School bag|Shovel|Table|Television"
        }
      },
      {
        "#type": "variable",
        "name": "l_item",
        "value": {
          "#type": "string_array",
          "items": "emtri|Beach ball|Bowl|Coin|Cup|Doll|Feather|Leaf|Paper clip|Rubber duckie|Screw|Slipper|T-short|Teddy bear"
        }
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "20"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "Drag and drop the items to show the heavier objects."
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "36"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "image_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "250"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "0"
            },
            "height": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "scale.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}scale.png"
          }
        ]
      },
      {
        "#type": "image_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "290"
            },
            "y": {
              "#type": "expression",
              "value": "170"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "0"
            },
            "height": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "big.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}big.png"
          }
        ]
      },
      {
        "#type": "drop_shape",
        "multiple": false,
        "resultMode": "default",
        "groupName": "",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "290"
            },
            "y": {
              "#type": "expression",
              "value": "160"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "150"
            },
            "height": {
              "#type": "expression",
              "value": "90"
            }
          },
          {
            "#type": "prop_value",
            "#prop": "",
            "value": {
              "#type": "expression",
              "value": "1"
            }
          }
        ]
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "290"
            },
            "y": {
              "#type": "expression",
              "value": "220"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "Heavier"
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "24"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "image_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "515"
            },
            "y": {
              "#type": "expression",
              "value": "140"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "0"
            },
            "height": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "big.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}big.png"
          }
        ]
      },
      {
        "#type": "drop_shape",
        "multiple": false,
        "resultMode": "default",
        "groupName": "",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "515"
            },
            "y": {
              "#type": "expression",
              "value": "130"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "150"
            },
            "height": {
              "#type": "expression",
              "value": "90"
            }
          },
          {
            "#type": "prop_value",
            "#prop": "",
            "value": {
              "#type": "expression",
              "value": "10"
            }
          }
        ]
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "515"
            },
            "y": {
              "#type": "expression",
              "value": "190"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "Lighter"
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "24"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "image_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "370"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "0"
            },
            "height": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "shape.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}shape.png"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "arr",
        "value": {
          "#type": "func",
          "name": "shuffle",
          "args": [
            {
              "#type": "expression",
              "value": "[0, 1]"
            }
          ]
        }
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "1"
            },
            "cols": {
              "#type": "expression",
              "value": "2"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "370"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "370"
            },
            "height": {
              "#type": "expression",
              "value": "120"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "expression",
              "value": "arr"
            }
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_cell_template_for",
            "variable": "$cell",
            "condition": "$cell.data == 1",
            "#prop": "cell.templates[]",
            "#callback": "$cell",
            "body": [
              {
                "#type": "choice_custom_shape",
                "action": "drag",
                "value": {
                  "#type": "expression",
                  "value": "1"
                },
                "template": {
                  "#callback": "$choice",
                  "variable": "$choice",
                  "body": [
                    {
                      "#type": "image_shape",
                      "#props": [
                        {
                          "#type": "prop_position",
                          "#prop": "",
                          "x": {
                            "#type": "expression",
                            "value": "$cell.centerX"
                          },
                          "y": {
                            "#type": "expression",
                            "value": "$cell.centerY"
                          }
                        },
                        {
                          "#type": "prop_max_size",
                          "#prop": "",
                          "maxWidth": {
                            "#type": "expression",
                            "value": "$cell.width -30"
                          },
                          "maxHeight": {
                            "#type": "expression",
                            "value": "90"
                          }
                        },
                        {
                          "#type": "prop_image_key",
                          "#prop": "",
                          "key": "1_${type[1]}.png"
                        },
                        {
                          "#type": "prop_image_src",
                          "#prop": "",
                          "src": "${image_path}1_${type[1]}.png"
                        }
                      ]
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "prop_grid_cell_template_for",
            "variable": "$cell",
            "condition": "$cell.data == 0",
            "#prop": "cell.templates[]",
            "#callback": "$cell",
            "body": [
              {
                "#type": "choice_custom_shape",
                "action": "drag",
                "value": {
                  "#type": "expression",
                  "value": "10"
                },
                "template": {
                  "#callback": "$choice",
                  "variable": "$choice",
                  "body": [
                    {
                      "#type": "image_shape",
                      "#props": [
                        {
                          "#type": "prop_position",
                          "#prop": "",
                          "x": {
                            "#type": "expression",
                            "value": "$cell.centerX"
                          },
                          "y": {
                            "#type": "expression",
                            "value": "$cell.centerY"
                          }
                        },
                        {
                          "#type": "prop_max_size",
                          "#prop": "",
                          "maxWidth": {
                            "#type": "expression",
                            "value": "$cell.width -30"
                          },
                          "maxHeight": {
                            "#type": "expression",
                            "value": "90"
                          }
                        },
                        {
                          "#type": "prop_image_key",
                          "#prop": "",
                          "key": "0_${type[0]}.png"
                        },
                        {
                          "#type": "prop_image_src",
                          "#prop": "",
                          "src": "${image_path}0_${type[0]}.png"
                        }
                      ]
                    }
                  ]
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "loadAssets",
        "value": {
          "#type": "custom_image_list",
          "link": "${image_path}",
          "images": "scale|1_${type[1]}|0_${type[0]}|ex1|ex2|ex3|ex4"
        }
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 1: First, look for the heavier item.</u></br></br>\n\n<style>\n  td{width: 215px; text-align: center;}\n#item{max-height: 100px}\n  </style>\n\n<center>\n<div style='position: relative; width: 400px; height: 430px;'>\n  <img src='@sprite.src(scale)' style='position: absolute; left: 25px; top: 100px'/>\n    \n  <table style='position: absolute; top: 250px; left: -15px; width: 430px; height: 140px;'>\n    <tr><td><img id='item' src='@sprite.src(${arr[0]}_${type[arr[0]]})'/></td>\n<td><img id='item' src='@sprite.src(${arr[1]}_${type[arr[1]]})'/></td></tr>\n    </table>\n\n<table style='position: absolute; top: 0px; left: -15px; height: 150px;'>\n    <tr><td><img id='item' src='@sprite.src(1_${type[1]})'/></td>\n    </table>"
        ]
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "arr[0] == 1"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<img src='@sprite.src(ex1)' style='position: absolute; left: 0px; top: 120px'/>"
            ]
          }
        ],
        "else": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<img src='@sprite.src(ex2)' style='position: absolute; left: 115px; top: 140px'/>"
            ]
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</div>\n<span style='background: rgb(250, 250, 250, 0.5)'>\n${h_item[type[1]]} ---> Heavier item.</br>\nDrag the heavier item into the heavier side of the pan balance.</span>\n</center>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 2: Next, look for the lighter item.</u></br></br>\n\n<style>\n  td{width: 215px; text-align: center;}\n#item{max-height: 100px}\n  </style>\n\n<center>\n<div style='position: relative; width: 400px; height: 430px;'>\n  <img src='@sprite.src(scale)' style='position: absolute; left: 25px; top: 100px'/>\n    \n  <table style='position: absolute; top: 0px; left: -15px; height: 150px;'>\n    <tr><td><img id='item' src='@sprite.src(1_${type[1]})'/></td>\n    </table>\n\n<table style='position: absolute; top: -30px; left: 210px; height: 150px;'>\n    <tr><td><img id='item' src='@sprite.src(0_${type[0]})'/></td>\n    </table>\n\n  <table style='position: absolute; top: 250px; left: -15px; width: 430px; height: 140px;'>"
        ]
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "arr[0] == 1"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<td></td><td><img id='item' src='@sprite.src(${arr[1]}_${type[arr[1]]})'/></td>"
            ]
          }
        ],
        "else": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<td><img id='item' src='@sprite.src(${arr[0]}_${type[arr[0]]})'/></td><td></td>"
            ]
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</tr></table>"
        ]
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "arr[0] == 1"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<img src='@sprite.src(ex4)' style='position: absolute; left: 230px; top: 120px'/>"
            ]
          }
        ],
        "else": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<img src='@sprite.src(ex3)' style='position: absolute; left: 15px; top: 140px'/>"
            ]
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</div>\n<span style='background: rgb(250, 250, 250, 0.5)'>\n${l_item[type[0]]} ---> Lighter item.</br>\nDrag and drop the lighter item into the lighter side of the pan balance.</span>\n</center>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="0" y="0">
    <field name="name">Y1.MG.MA.RECMPENB.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y1.MG.MA.RECMPENB.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="YepMQT@2uwp2rRPvmnRg1">
                                    <field name="name">range</field>
                                    <value name="value">
                                      <block type="expression" id="UxgQ^Y5]rb}vpw[D=l-!">
                                        <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="variable" id="yiLv@1@2B:]-c4Y0)ZOVQr">
                                        <field name="name">type</field>
                                        <value name="value">
                                          <block type="expression" id="XQUDQ-)RkE,lDFE#sf(7">
                                            <field name="value">[]</field>
                                          </block>
                                        </value>
                                        <next>
                                          <block type="variable" id="QGn;Sny}@25A}Hn5,~+@2J">
                                            <field name="name">type[0]</field>
                                            <value name="value">
                                              <block type="random_number" id="8}cL(MBv$-OpkSB3jPxt">
                                                <value name="min">
                                                  <block type="expression" id="~eBD2#Xuc}PcDn:BVT?C">
                                                    <field name="value">1</field>
                                                  </block>
                                                </value>
                                                <value name="max">
                                                  <block type="expression" id="%I-J5?wD53?K8NJqc3@2?">
                                                    <field name="value">13</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id="4gDqU`]Y$|FFbXE[O:]C">
                                                <field name="name">type[1]</field>
                                                <value name="value">
                                                  <block type="random_number" id="sP@2O/v2H4lCqXD1$/EK?">
                                                    <value name="min">
                                                      <block type="expression" id="F9MXPnobD{71Y{v~@1@2/H">
                                                        <field name="value">1</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="M3|t-:)95b#V0AYUK0F4">
                                                        <field name="value">13</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="bG.Q/z33a(#dGi8askKM">
                                                    <field name="name">h_item</field>
                                                    <value name="value">
                                                      <block type="string_array" id="R<v]@2HD-@1dU[M/n19y">
                                                        <field name="items">emtri|Chair|Electronic keyboard|Fish tank|Flower in a pot|Flower in a pot|Guitar|Hammer|Laptop|Radio|School bag|Shovel|Table|Television</field>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id="]p8_er!gFz[{1b3f78oC">
                                                        <field name="name">l_item</field>
                                                        <value name="value">
                                                          <block type="string_array" id="w,2Y5J=f:Pl#{.P|$[pd">
                                                            <field name="items">emtri|Beach ball|Bowl|Coin|Cup|Doll|Feather|Leaf|Paper clip|Rubber duckie|Screw|Slipper|T-short|Teddy bear</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                            <statement name="#props">
                                                              <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                                <field name="#prop"></field>
                                                                <value name="x">
                                                                  <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                                    <field name="value">400</field>
                                                                  </block>
                                                                </value>
                                                                <value name="y">
                                                                  <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                                    <field name="value">20</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_anchor" id="O.v@2mKn@1K#+tF;|}C}xI">
                                                                    <field name="#prop">anchor</field>
                                                                    <value name="x">
                                                                      <block type="expression" id="+3%GU[X3l%%X`Jv1QL7@2">
                                                                        <field name="value">0.5</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="y">
                                                                      <block type="expression" id="Kzp23HjHF7k]A-y1BJPm">
                                                                        <field name="value">0</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                                        <field name="#prop"></field>
                                                                        <value name="contents">
                                                                          <block type="string_value" id="#6btV9p,PGV}wMx.U�">
                                                                            <field name="value">Drag and drop the items to show the heavier objects.</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                            <field name="base">text</field>
                                                                            <statement name="#props">
                                                                              <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                                <field name="#prop"></field>
                                                                                <value name="fontSize">
                                                                                  <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                                    <field name="value">36</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="fill">
                                                                                      <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                                        <field name="value">black</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="stroke">
                                                                                          <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                            <field name="value">white</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="strokeThickness">
                                                                                              <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                                <field name="value">2</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </statement>
                                                            <next>
                                                              <block type="image_shape" id="IZXNbN$@2@2JLSo6a(0j]:">
                                                                <statement name="#props">
                                                                  <block type="prop_position" id="!3V3B]S)@1xueHHjcHsZ.">
                                                                    <field name="#prop"></field>
                                                                    <value name="x">
                                                                      <block type="expression" id="4#KlbS}PP?eSm!,pivF;">
                                                                        <field name="value">400</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="y">
                                                                      <block type="expression" id="L(Zy^C3KjL8JvJ(t$6-}">
                                                                        <field name="value">250</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_size" id="(sRorHAmiINsW[3d6Pp2">
                                                                        <field name="#prop"></field>
                                                                        <value name="width">
                                                                          <block type="expression" id="LPIs=Ihi-vFIfpoQmZ^o">
                                                                            <field name="value">0</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="height">
                                                                          <block type="expression" id="VrQRtHQt_C|.g[Hzlk#{">
                                                                            <field name="value">0</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_image_key" id="Vr.S)D0o]#b�jW)ir[">
                                                                            <field name="#prop"></field>
                                                                            <value name="key">
                                                                              <block type="string_value" id="5^]xWg{QdbaW~L:ph{6[">
                                                                                <field name="value">scale.png</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_image_src" id="i8T-IEe?YY`~GRQCA,MI">
                                                                                <field name="#prop"></field>
                                                                                <value name="src">
                                                                                  <block type="string_value" id="c)372%!Z):+I.vYPC,nr">
                                                                                    <field name="value">${image_path}scale.png</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <next>
                                                                  <block type="image_shape" id=")Z-VeQHH=q%%`CWqw|iG">
                                                                    <statement name="#props">
                                                                      <block type="prop_position" id="8taNGXL|4ngb1ukIaGSX">
                                                                        <field name="#prop"></field>
                                                                        <value name="x">
                                                                          <block type="expression" id="PpcCj1p)kd3Lhx+3VKav">
                                                                            <field name="value">290</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id="W;mq_/CFq@1X{%J=@1m=Pn">
                                                                            <field name="value">170</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_size" id="J7K?o2b9Bg7y+X(XUiJc">
                                                                            <field name="#prop"></field>
                                                                            <value name="width">
                                                                              <block type="expression" id=".~XL@1Lc~_WDJy-~`[{Yw">
                                                                                <field name="value">0</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="height">
                                                                              <block type="expression" id="Oy#yj|.7W=:SX:$?@2h53">
                                                                                <field name="value">0</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_image_key" id="@1Rx29N~=3pWDbMkPnD?a">
                                                                                <field name="#prop"></field>
                                                                                <value name="key">
                                                                                  <block type="string_value" id="/-BAd)vVPkwydsWRvk4W">
                                                                                    <field name="value">big.png</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_image_src" id=":vW{%:Y(@1|]X%e#;y)Gj">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="src">
                                                                                      <block type="string_value" id="b2(E/Yt`y_0zE4HUD~tn">
                                                                                        <field name="value">${image_path}big.png</field>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="drop_shape" id="8P)]hDd(pU^(nL2gQY1X">
                                                                        <field name="multiple">FALSE</field>
                                                                        <field name="resultMode">default</field>
                                                                        <field name="groupName"></field>
                                                                        <statement name="#props">
                                                                          <block type="prop_position" id="QD?MuZ9@2$V9n8c;7Ms:}">
                                                                            <field name="#prop"></field>
                                                                            <value name="x">
                                                                              <block type="expression" id="etm)e+jo2Vip{JXJU`8S">
                                                                                <field name="value">290</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="y">
                                                                              <block type="expression" id="O$${xfoasZ$1mU_!|QSM">
                                                                                <field name="value">160</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_size" id="#mv]OOD8Jn|S40PDmD,j">
                                                                                <field name="#prop"></field>
                                                                                <value name="width">
                                                                                  <block type="expression" id="7d(VL`uZ]|$q@2Ux.OsjW">
                                                                                    <field name="value">150</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="height">
                                                                                  <block type="expression" id="n(@1MHk~2udar,l~f{9hw">
                                                                                    <field name="value">90</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_value" id="XiN__i0H|-44x~7ILRnh">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="R;d=PKuqMLsAu+N~@1;cS">
                                                                                        <field name="value">1</field>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="text_shape" id="d@1E~G07=sy%1vpS(Z@1}n">
                                                                            <statement name="#props">
                                                                              <block type="prop_position" id=".uUr34wl@2ct9r59f}aS6">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="Fovo,BfPFD286Iid!cjs">
                                                                                    <field name="value">290</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="`~$SYrpIbdf|$Dp)nY{@1">
                                                                                    <field name="value">220</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_anchor" id=",|(b=%xR@1nA(R~]~uP;E">
                                                                                    <field name="#prop">anchor</field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="@146.ztVBgPZe+7a:OT.S">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="$-H1DVSNz8[LGW3tAc},">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_text_contents" id="Q$IVM-l=~Ejz@1tW+A5,G">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="contents">
                                                                                          <block type="string_value" id="PL=6d|m_@1In#rp=mQ$2/">
                                                                                            <field name="value">Heavier</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_style" id="@2WJ@1v1a;-jC]{LLwGMYz">
                                                                                            <field name="base">text</field>
                                                                                            <statement name="#props">
                                                                                              <block type="prop_text_style_font_size" id="3F|:fRJ1Z;aK8%[d+sb:">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="fontSize">
                                                                                                  <block type="expression" id="2B@1Y~`57f:(l4?CbR}N|">
                                                                                                    <field name="value">24</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style_fill" id="$?7{hP4aU+O-7RcI@2vS4">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="fill">
                                                                                                      <block type="string_value" id="BK/7ZGll?(WM{yerxWW4">
                                                                                                        <field name="value">black</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style_stroke" id="EUC_P^%9.L7m!,qidGgC">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="stroke">
                                                                                                          <block type="string_value" id="[x.4@2jbU#0erzaJs;p@2X">
                                                                                                            <field name="value">white</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style_stroke_thickness" id="q`70%-doGVVSd|~l5E=j">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="strokeThickness">
                                                                                                              <block type="expression" id="tgngu`P8cf2?5l4!gl56">
                                                                                                                <field name="value">2</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="image_shape" id="/eisx3NB;f#`}W%PhhYe">
                                                                                <statement name="#props">
                                                                                  <block type="prop_position" id=":!qJk%7Ow%v[N}.7J6cd">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="MVKZzbLfJT4HSXBMw5k:">
                                                                                        <field name="value">515</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="`/wHNl[JqvzwL()V9Cp0">
                                                                                        <field name="value">140</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_size" id="lQy-m#=WpEzIrVU?d7|_">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="width">
                                                                                          <block type="expression" id="LWxv57-JYISYw|z-:T%R">
                                                                                            <field name="value">0</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="height">
                                                                                          <block type="expression" id="tj;F]_RlzEbYBvEH+:]~">
                                                                                            <field name="value">0</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_image_key" id="??@1E_DtCvNwB#uI6Q2Ie">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="key">
                                                                                              <block type="string_value" id="f0yTt5pG2H8uh.Hz4iQZ">
                                                                                                <field name="value">big.png</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_image_src" id="!FwA4C#/99;i#Wn1V/p@1">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="src">
                                                                                                  <block type="string_value" id="=OiF.%R-5N}jFJ%2J;CS">
                                                                                                    <field name="value">${image_path}big.png</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="drop_shape" id="X_[3-l4wvHS@2l~lK,x?q">
                                                                                    <field name="multiple">FALSE</field>
                                                                                    <field name="resultMode">default</field>
                                                                                    <field name="groupName"></field>
                                                                                    <statement name="#props">
                                                                                      <block type="prop_position" id="7G.E,Cz+++TehoXj=jX2">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="Osm4kvwa%FG5ZVXIwEWX">
                                                                                            <field name="value">515</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="w7NOeO=l@2hCp:ARVz)?B">
                                                                                            <field name="value">130</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_size" id="Y4{b5YMrIFd{V5j{B|4E">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="width">
                                                                                              <block type="expression" id="p?C$B63|c8hRmL@2:]XN!">
                                                                                                <field name="value">150</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="height">
                                                                                              <block type="expression" id="O,~YmGHBjwi16o%^3X6h">
                                                                                                <field name="value">90</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_value" id="TYB/jPsE?ai0Om0b$|`;">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="EW�#Vm.kT3e^kCwFg(">
                                                                                                    <field name="value">10</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <next>
                                                                                      <block type="text_shape" id="@1+w0m=xE|46GgrRNG-Y:">
                                                                                        <statement name="#props">
                                                                                          <block type="prop_position" id="ga?u~fmzl_mAMft9/=Z+">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="8~L5_@2J{VaI8ZXe`[!r(">
                                                                                                <field name="value">515</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="As^p-t0jiy.Vf@17H3Rz5">
                                                                                                <field name="value">190</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_anchor" id="4p{9c|0$=xDE8jR]3@19U">
                                                                                                <field name="#prop">anchor</field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="54Casa6.qomvg@1`8F[q(">
                                                                                                    <field name="value">0.5</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="e.K/-q_^Hb!#wADMtt+9">
                                                                                                    <field name="value">0.5</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_contents" id="V1Ck|Wb{OCKQe%y|hjd|">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="contents">
                                                                                                      <block type="string_value" id=":v#bP~iTp_t([^(:mQSm">
                                                                                                        <field name="value">Lighter</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style" id="DEq%O/xlI/i|aLo-|Ca,">
                                                                                                        <field name="base">text</field>
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_text_style_font_size" id="Q`p=#H/0-5mHX||e`~tR">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="fontSize">
                                                                                                              <block type="expression" id="3HD24^!o|,e!:+4XN$#?">
                                                                                                                <field name="value">24</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_fill" id="SKAEeF+$2js@1ezVgJoTU">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="fill">
                                                                                                                  <block type="string_value" id="4vlzC%Wjq9eZ#A|xU:mq">
                                                                                                                    <field name="value">black</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_stroke" id="iGT46S8?;W`4XkfIvNs)">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="stroke">
                                                                                                                      <block type="string_value" id="p67j^251.sRBZp9P8gYH">
                                                                                                                        <field name="value">white</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_stroke_thickness" id="VblcM4W0QzI8G90Qe%|e">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="strokeThickness">
                                                                                                                          <block type="expression" id="3UY8E!{_3AYg{1@1|vAms">
                                                                                                                            <field name="value">2</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="image_shape" id="#t[JROI]V%=851a25Nx`">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_position" id="UM,{7=MIdC3n:xmq)]J#">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="G0k}{Z=J154-?C28P#)f">
                                                                                                    <field name="value">400</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="uF,m1VEYuUI./)m,9K_s">
                                                                                                    <field name="value">370</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_size" id="6)W#+6{u^Z=$WY@1yFk}h">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="width">
                                                                                                      <block type="expression" id="(F|xT-%N0{GmLKBTVRpI">
                                                                                                        <field name="value">0</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="height">
                                                                                                      <block type="expression" id="-JjX-}D$K7n0,zpNW^#P">
                                                                                                        <field name="value">0</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_image_key" id="a#[YDb;aHf4@1j_jR^EOQ">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="key">
                                                                                                          <block type="string_value" id="=e`6Y^MJ!HsVaRaXK@2x,">
                                                                                                            <field name="value">shape.png</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_image_src" id="8;p`zaCB[7gyXvK[2V|X">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="src">
                                                                                                              <block type="string_value" id="/aU^{V4N/e`$_z)6O-1t">
                                                                                                                <field name="value">${image_path}shape.png</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="variable" id="XK?AC@1DX|]vZgPF_-V}9">
                                                                                                <field name="name">arr</field>
                                                                                                <value name="value">
                                                                                                  <block type="func_shuffle" id=":[E(kRG/P[2kJo_k}z^x" inline="true">
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="{y/t?$}U,_/Q580g-0l)">
                                                                                                        <field name="value">[0, 1]</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="grid_shape" id="6tU?K^N8ec~A@1@1NwQ72N">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_grid_dimension" id="X4K:|9dKK%s9l4n5v@2lG">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="rows">
                                                                                                          <block type="expression" id="[Ev!.bxnzjc|-33v.%7=">
                                                                                                            <field name="value">1</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="cols">
                                                                                                          <block type="expression" id="u$7]/V/!ZWM4(Va??@2({">
                                                                                                            <field name="value">2</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_position" id="zLld84TfBg#!88]1,JD-">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="Cd6g{Y_P?:GyVyvQi1wJ">
                                                                                                                <field name="value">400</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id=".F}E71+@1i)77!yQ`hmfu">
                                                                                                                <field name="value">370</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_size" id="+o2CWr^.ohx)_bcIG53]">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="width">
                                                                                                                  <block type="expression" id="Z[x5iHG%KWC%KWQb}c-G">
                                                                                                                    <field name="value">370</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="height">
                                                                                                                  <block type="expression" id="@2E|Ob@1jF#gYtX8-%Iui@1">
                                                                                                                    <field name="value">120</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_anchor" id="1WJjXY^.h,t]eNgxaJ}q">
                                                                                                                    <field name="#prop">anchor</field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="DHx[vqs?.?nUD:AYPJWd">
                                                                                                                        <field name="value">0.5</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="e3D|RR7w!=Sufu):zV`c">
                                                                                                                        <field name="value">0.5</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_grid_cell_source" id="v8+Yu211Qg8fX9Mu4th`">
                                                                                                                        <field name="#prop">cell.source</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id="~j@1,^liddf}oDg}E91^i">
                                                                                                                            <field name="value">arr</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_grid_random" id="}?kS#x[b)9OGb@1`UdJGV">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <field name="random">FALSE</field>
                                                                                                                            <next>
                                                                                                                              <block type="prop_grid_show_borders" id=";y^wk4y%[VWjXOj~vrQZ">
                                                                                                                                <field name="#prop">#showBorders</field>
                                                                                                                                <field name="value">FALSE</field>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_grid_cell_template_for" id="PJ%@2V2_NCMToUY;X[=q;">
                                                                                                                                    <field name="variable">$cell</field>
                                                                                                                                    <field name="condition">$cell.data == 1</field>
                                                                                                                                    <field name="#prop">cell.templates[]</field>
                                                                                                                                    <field name="#callback">$cell</field>
                                                                                                                                    <statement name="body">
                                                                                                                                      <block type="choice_custom_shape" id="SSvBAHlH%EyHN$n|jwUe">
                                                                                                                                        <field name="action">drag</field>
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="expression" id=")F=Hin{gDYj$nUA.Y[f^">
                                                                                                                                            <field name="value">1</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <statement name="template">
                                                                                                                                          <block type="image_shape" id="COhY8pu@1@1!#naHse(@1);">
                                                                                                                                            <statement name="#props">
                                                                                                                                              <block type="prop_position" id="yjz0NT33yFA(Ty:iRU(P">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="x">
                                                                                                                                                  <block type="expression" id="R02Go@1?NJ=IO:u]rz-)d">
                                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <value name="y">
                                                                                                                                                  <block type="expression" id="@1?3_)[QnY4GFC$I{xsai">
                                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_max_size" id="mG8SZ5DcZt+N!RCyOKm[">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="maxWidth">
                                                                                                                                                      <block type="expression" id="EPvBpLnf0v}dI7eG6Bng">
                                                                                                                                                        <field name="value">$cell.width -30</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="maxHeight">
                                                                                                                                                      <block type="expression" id=")3UwJ!7FCm3OFUkU@2$(d">
                                                                                                                                                        <field name="value">90</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_image_key" id="IhKj=oK5:qHAKz/R]qfL">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="key">
                                                                                                                                                          <block type="string_value" id="yDQrB)t(AMSa~khLlteJ">
                                                                                                                                                            <field name="value">1_${type[1]}.png</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_image_src" id="dS7@1@2|:0[C3}-9~~c/dr">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="src">
                                                                                                                                                              <block type="string_value" id="_zvKdeUF3)@1$@1@2D]aAZf">
                                                                                                                                                                <field name="value">${image_path}1_${type[1]}.png</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_grid_cell_template_for" id="{Rx~5G8[n#yF^{ATu@1#/">
                                                                                                                                        <field name="variable">$cell</field>
                                                                                                                                        <field name="condition">$cell.data == 0</field>
                                                                                                                                        <field name="#prop">cell.templates[]</field>
                                                                                                                                        <field name="#callback">$cell</field>
                                                                                                                                        <statement name="body">
                                                                                                                                          <block type="choice_custom_shape" id="H~o(=o?1M;Qbt4%VYR2C">
                                                                                                                                            <field name="action">drag</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id="-;O^U)qGhl8s@2w9ori%R">
                                                                                                                                                <field name="value">10</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <statement name="template">
                                                                                                                                              <block type="image_shape" id="@19r;nrZ62)w2bGi0q(-f">
                                                                                                                                                <statement name="#props">
                                                                                                                                                  <block type="prop_position" id="7Jh1N;n$;|9U/c$-$n;L">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="x">
                                                                                                                                                      <block type="expression" id="~HOU^{FraA@1mWQs?|s@1O">
                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="y">
                                                                                                                                                      <block type="expression" id="@1$9%|#G={)p,/tsKP+w%">
                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_max_size" id="2uFzUn[_`?Ej1dl9c8FP">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="maxWidth">
                                                                                                                                                          <block type="expression" id="Hx1~V|;$c`:@15rmUEw]Q">
                                                                                                                                                            <field name="value">$cell.width -30</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="maxHeight">
                                                                                                                                                          <block type="expression" id="@1RcQ=g}l9?uOwjTrz@1E3">
                                                                                                                                                            <field name="value">90</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_image_key" id="gRTqP�E_1tHY[KhB@1%">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="key">
                                                                                                                                                              <block type="string_value" id="s@2M}Yq!sJOyAqTNQ;6">
                                                                                                                                                                <field name="value">0_${type[0]}.png</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_image_src" id="t)%5@1mA76O1|hw-zjHm@1">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="src">
                                                                                                                                                                  <block type="string_value" id="jN7J`eg_Qo[ox{rqXO6/">
                                                                                                                                                                    <field name="value">${image_path}0_${type[0]}.png</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="variable" id="weElLx3.8SB.)STr377[">
                                                                                                        <field name="name">loadAssets</field>
                                                                                                        <value name="value">
                                                                                                          <block type="custom_image_list" id="[iQG~r:b@1r?SU7Kr3cr@2">
                                                                                                            <field name="link">${image_path}</field>
                                                                                                            <field name="images">scale|1_${type[1]}|0_${type[0]}|ex1|ex2|ex3|ex4</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="partial_explanation" id="r6qe;QZ}8vH.#$9cVo+m" inline="true">
                                                                                                            <value name="value">
                                                                                                              <block type="string_value" id="N3t/M%^L0gF55i3wsXg7">
                                                                                                                <field name="value">&lt;u&gt;Step 1: First, look for the heavier item.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
  td{width: 215px; text-align: center;}
#item{max-height: 100px}
  &lt;/style&gt;

&lt;center&gt;
&lt;div style='position: relative; width: 400px; height: 430px;'&gt;
  &lt;img src='@1sprite.src(scale)' style='position: absolute; left: 25px; top: 100px'/&gt;
    
  &lt;table style='position: absolute; top: 250px; left: -15px; width: 430px; height: 140px;'&gt;
    &lt;tr&gt;&lt;td&gt;&lt;img id='item' src='@1sprite.src(${arr[0]}_${type[arr[0]]})'/&gt;&lt;/td&gt;
&lt;td&gt;&lt;img id='item' src='@1sprite.src(${arr[1]}_${type[arr[1]]})'/&gt;&lt;/td&gt;&lt;/tr&gt;
    &lt;/table&gt;

&lt;table style='position: absolute; top: 0px; left: -15px; height: 150px;'&gt;
    &lt;tr&gt;&lt;td&gt;&lt;img id='item' src='@1sprite.src(1_${type[1]})'/&gt;&lt;/td&gt;
    &lt;/table&gt;
                                                                                                                </field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="if_then_else_block" id="BQ7@1gfnjY$7-f%f)xG8,">
                                                                                                                <value name="if">
                                                                                                                  <block type="expression" id=",D;Z:6T?spT8j%|B~HW_">
                                                                                                                    <field name="value">arr[0] == 1</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <statement name="then">
                                                                                                                  <block type="partial_explanation" id="^#a[+e_o,GnE2wv,3.VQ" inline="true">
                                                                                                                    <value name="value">
                                                                                                                      <block type="string_value" id="uJganX-Jq7$?3M|x=U^r">
                                                                                                                        <field name="value">&lt;img src='@1sprite.src(ex1)' style='position: absolute; left: 0px; top: 120px'/&gt;</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <statement name="else">
                                                                                                                  <block type="partial_explanation" id="z,{$AdmSj5e%6iMQ1ZU#" inline="true">
                                                                                                                    <value name="value">
                                                                                                                      <block type="string_value" id="X5~d3hI5Dyu`zq}RfSPX">
                                                                                                                        <field name="value">&lt;img src='@1sprite.src(ex2)' style='position: absolute; left: 115px; top: 140px'/&gt;</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="partial_explanation" id="0k4Amolp#r~HYRCTg5_m" inline="true">
                                                                                                                    <value name="value">
                                                                                                                      <block type="string_value" id="cGRQ~EOV,mQ$UdlZ,DY`">
                                                                                                                        <field name="value">&lt;/div&gt;
&lt;span style='background: rgb(250, 250, 250, 0.5)'&gt;
${h_item[type[1]]} ---&gt; Heavier item.&lt;/br&gt;
Drag the heavier item into the heavier side of the pan balance.&lt;/span&gt;
&lt;/center&gt;
                                                                                                                        </field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="end_partial_explanation" id="P]:Fe]T8R%3]^;0OgY%^">
                                                                                                                        <next>
                                                                                                                          <block type="partial_explanation" id="+Y7I89N]ib=Bv+N[)h`2" inline="true">
                                                                                                                            <value name="value">
                                                                                                                              <block type="string_value" id="a[J@1SVqZkZ5}Mk?IEi:X">
                                                                                                                                <field name="value">&lt;u&gt;Step 2: Next, look for the lighter item.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
  td{width: 215px; text-align: center;}
#item{max-height: 100px}
  &lt;/style&gt;

&lt;center&gt;
&lt;div style='position: relative; width: 400px; height: 430px;'&gt;
  &lt;img src='@1sprite.src(scale)' style='position: absolute; left: 25px; top: 100px'/&gt;
    
  &lt;table style='position: absolute; top: 0px; left: -15px; height: 150px;'&gt;
    &lt;tr&gt;&lt;td&gt;&lt;img id='item' src='@1sprite.src(1_${type[1]})'/&gt;&lt;/td&gt;
    &lt;/table&gt;

&lt;table style='position: absolute; top: -30px; left: 210px; height: 150px;'&gt;
    &lt;tr&gt;&lt;td&gt;&lt;img id='item' src='@1sprite.src(0_${type[0]})'/&gt;&lt;/td&gt;
    &lt;/table&gt;

  &lt;table style='position: absolute; top: 250px; left: -15px; width: 430px; height: 140px;'&gt;
                                                                                                                                </field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="if_then_else_block" id="FWtj/PO%XTohS2yG39,C">
                                                                                                                                <value name="if">
                                                                                                                                  <block type="expression" id="Q7BSMGEp!DoldQkb@2{sA">
                                                                                                                                    <field name="value">arr[0] == 1</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <statement name="then">
                                                                                                                                  <block type="partial_explanation" id="Q_858Z[?a=,%G.;dIR{b" inline="true">
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="string_value" id="sY_Zro(paK7|:5}5x%Qb">
                                                                                                                                        <field name="value">&lt;td&gt;&lt;/td&gt;&lt;td&gt;&lt;img id='item' src='@1sprite.src(${arr[1]}_${type[arr[1]]})'/&gt;&lt;/td&gt;</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <statement name="else">
                                                                                                                                  <block type="partial_explanation" id="}5}pV[5or2qtF(TTb7d^" inline="true">
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="string_value" id="v+9MqZ,`mxa~~W%=Qbj1">
                                                                                                                                        <field name="value">&lt;td&gt;&lt;img id='item' src='@1sprite.src(${arr[0]}_${type[arr[0]]})'/&gt;&lt;/td&gt;&lt;td&gt;&lt;/td&gt;</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <next>
                                                                                                                                  <block type="partial_explanation" id="2XWO~k0+~;M0vYeb}DK1" inline="true">
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="string_value" id="yqWDH0~Z3~RJaufx;NbI">
                                                                                                                                        <field name="value">&lt;/tr&gt;&lt;/table&gt;</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="if_then_else_block" id="3V#uoC4x^Z@2b!c(S52!,">
                                                                                                                                        <value name="if">
                                                                                                                                          <block type="expression" id="yqpXy[aL202Sd(he#JE[">
                                                                                                                                            <field name="value">arr[0] == 1</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <statement name="then">
                                                                                                                                          <block type="partial_explanation" id="5y#Nl~[WjXQD0R1(MsnU" inline="true">
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="string_value" id="{/BXEvRg?R~!mmv7aU1#">
                                                                                                                                                <field name="value">&lt;img src='@1sprite.src(ex4)' style='position: absolute; left: 230px; top: 120px'/&gt;</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <statement name="else">
                                                                                                                                          <block type="partial_explanation" id="xlJD+?;n=VsM-[U,u|x4" inline="true">
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="string_value" id="vw~G=v@2LCs8Hqf1]w2IW">
                                                                                                                                                <field name="value">&lt;img src='@1sprite.src(ex3)' style='position: absolute; left: 15px; top: 140px'/&gt;</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <next>
                                                                                                                                          <block type="partial_explanation" id="{[VTEX/hcorTRhVJKvO]" inline="true">
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="string_value" id="Of3D!(pcu@16O=Cn:$aWY">
                                                                                                                                                <field name="value">&lt;/div&gt;
&lt;span style='background: rgb(250, 250, 250, 0.5)'&gt;
${l_item[type[0]]} ---&gt; Lighter item.&lt;/br&gt;
Drag and drop the lighter item into the lighter side of the pan balance.&lt;/span&gt;
&lt;/center&gt;
                                                                                                                                                </field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="end_partial_explanation" id="muYJ`CK.X4m_/L7M#|=5"></block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
</xml>
END_XML]] */