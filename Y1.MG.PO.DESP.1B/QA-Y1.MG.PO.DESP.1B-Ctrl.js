
module.exports = [
  {
    "#type": "question",
    "name": "Y1.MG.PO.DESP.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y1.MG.PO.DESP.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "variable",
        "name": "color_name",
        "value": {
          "#type": "string_array",
          "items": "blue|orange|purple|green|pink|light blue|light green|red"
        }
      },
      {
        "#type": "variable",
        "name": "number_color",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "6"
          },
          "max": {
            "#type": "expression",
            "value": "6"
          }
        }
      },
      {
        "#type": "variable",
        "name": "color",
        "value": {
          "#type": "random_many",
          "count": {
            "#type": "expression",
            "value": "number_color"
          },
          "items": {
            "#type": "func",
            "name": "arrayOfNumber",
            "args": [
              {
                "#type": "expression",
                "value": "8"
              },
              {
                "#type": "expression",
                "value": "0"
              }
            ]
          }
        }
      },
      {
        "#type": "variable",
        "name": "color_pick",
        "value": {
          "#type": "random_one",
          "items": {
            "#type": "expression",
            "value": "color"
          }
        }
      },
      {
        "#type": "variable",
        "name": "total",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "40"
          },
          "max": {
            "#type": "expression",
            "value": "50"
          }
        }
      },
      {
        "#type": "variable",
        "name": "array_number",
        "value": {
          "#type": "random_many",
          "count": {
            "#type": "expression",
            "value": "total"
          },
          "items": {
            "#type": "func",
            "name": "arrayOfNumber",
            "args": [
              {
                "#type": "expression",
                "value": "100"
              },
              {
                "#type": "expression",
                "value": "1"
              }
            ]
          }
        }
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "total"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < 50"
        },
        "do": [
          {
            "#type": "variable",
            "name": "array_number[i]",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "array_number",
        "value": {
          "#type": "func",
          "name": "shuffle",
          "args": [
            {
              "#type": "expression",
              "value": "array_number"
            }
          ]
        }
      },
      {
        "#type": "variable",
        "name": "action",
        "value": {
          "#type": "string_array",
          "items": "a dragon to the gold|pirate to the treasure|boy to school|girl to the doll"
        }
      },
      {
        "#type": "variable",
        "name": "ac",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "0"
          },
          "max": {
            "#type": "expression",
            "value": "3"
          }
        }
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "40"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "Select the set of numbers that show the ${color_name[color_pick]} path from the ${action[ac]}."
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "36"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "image_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "200"
            },
            "y": {
              "#type": "expression",
              "value": "100"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "0"
            },
            "height": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "s${ac}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}s${ac}.png"
          }
        ]
      },
      {
        "#type": "statement",
        "value": "function re_Y(c){\n    if(c == 0) return 5;\n    else if(c == 1) return 5;\n    else if(c == 2) return 12;\n  else if(c == 3) return 3;\n   else if(c == 4) return 5;\n    else return 4;\n}"
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < number_color"
        },
        "do": [
          {
            "#type": "variable",
            "name": "ox",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "-2"
              },
              "max": {
                "#type": "expression",
                "value": "2"
              }
            }
          },
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "275+ox"
                },
                "y": {
                  "#type": "expression",
                  "value": "100 + re_Y(color[i])"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0"
                },
                "y": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "0"
                },
                "height": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "${color[i]}.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}${color[i]}.png"
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "10"
            },
            "cols": {
              "#type": "expression",
              "value": "5"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "100"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "174"
            },
            "height": {
              "#type": "expression",
              "value": "201"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "expression",
              "value": "array_number"
            }
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": true
          },
          {
            "#type": "prop_stroke",
            "#prop": "stroke",
            "width": {
              "#type": "expression",
              "value": "1"
            },
            "color": {
              "#type": "expression",
              "value": "0x000000"
            }
          },
          {
            "#type": "prop_grid_cell_template",
            "variable": "$cell",
            "#prop": "cell.template",
            "#callback": "$cell",
            "body": [
              {
                "#type": "if_then_block",
                "if": {
                  "#type": "expression",
                  "value": "$cell.data != 0"
                },
                "then": [
                  {
                    "#type": "text_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 4"
                        }
                      },
                      {
                        "#type": "prop_anchor",
                        "#prop": "anchor",
                        "x": {
                          "#type": "expression",
                          "value": "0.5"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "0.5"
                        }
                      },
                      {
                        "#type": "prop_text_contents",
                        "#prop": "",
                        "contents": "${$cell.data}"
                      },
                      {
                        "#prop": "",
                        "style": {
                          "#type": "json",
                          "base": "text",
                          "#props": [
                            {
                              "#type": "prop_text_style_font_size",
                              "#prop": "",
                              "fontSize": {
                                "#type": "expression",
                                "value": "22"
                              }
                            },
                            {
                              "#type": "prop_text_style_fill",
                              "#prop": "",
                              "fill": "black"
                            }
                          ]
                        }
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "image_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "600"
            },
            "y": {
              "#type": "expression",
              "value": "300"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "1"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "0"
            },
            "height": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "e${ac}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}e${ac}.png"
          }
        ]
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="-4" y="-55">
    <field name="name">Y1.MG.PO.DESP.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y1.MG.PO.DESP.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="YepMQT@2uwp2rRPvmnRg1">
                                    <field name="name">range</field>
                                    <value name="value">
                                      <block type="expression" id="UxgQ^Y5]rb}vpw[D=l-!">
                                        <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="variable" id="c}{ORYx~)|#qP^DMs3$|">
                                        <field name="name">color_name</field>
                                        <value name="value">
                                          <block type="string_array" id="[qxkkJUvQn}Nn|F2cZxu">
                                            <field name="items">blue|orange|purple|green|pink|light blue|light green|red</field>
                                          </block>
                                        </value>
                                        <next>
                                          <block type="variable" id="A?ndD9jM7cebI;)4!i0~">
                                            <field name="name">number_color</field>
                                            <value name="value">
                                              <block type="random_number" id="CdA-M^Njcgk9IXZVsg9n">
                                                <value name="min">
                                                  <block type="expression" id="eQqxmHKc(@2jis;kbOqTS">
                                                    <field name="value">6</field>
                                                  </block>
                                                </value>
                                                <value name="max">
                                                  <block type="expression" id=":e!2{Kn3^iswk@2%q2OaB">
                                                    <field name="value">6</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id="}X`)nmAfM($la6_iM!zA">
                                                <field name="name">color</field>
                                                <value name="value">
                                                  <block type="random_many" id="/[KZqIhsSe`@2Xb$v.Y|$">
                                                    <value name="count">
                                                      <block type="expression" id="ymAT4x@1zhW.o=/e.8@1+j">
                                                        <field name="value">number_color</field>
                                                      </block>
                                                    </value>
                                                    <value name="items">
                                                      <block type="func_array_of_number" id="|w]`m/tJ]goqUmS0tIDy" inline="true">
                                                        <value name="items">
                                                          <block type="expression" id="Kh$?9BH(c6eag@1L%Xj=L">
                                                            <field name="value">8</field>
                                                          </block>
                                                        </value>
                                                        <value name="from">
                                                          <block type="expression" id="wtU#7eG0d+A$xD5Rqd@18">
                                                            <field name="value">0</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="y[;@1;P|!_bt3bwU}5At)">
                                                    <field name="name">color_pick</field>
                                                    <value name="value">
                                                      <block type="random_one" id="Bpt$@2}ime{{6WIIYg:gR">
                                                        <value name="items">
                                                          <block type="expression" id="IXSTShYKKhkdZsQ3Qs[P">
                                                            <field name="value">color</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id="Vz4nX(;bEHRFY@1x#Iiqh">
                                                        <field name="name">total</field>
                                                        <value name="value">
                                                          <block type="random_number" id="7h(FE!(+JCIkvOT4uW7g">
                                                            <value name="min">
                                                              <block type="expression" id="}$/x=u{SzOccA!an0=#T">
                                                                <field name="value">40</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="XLxzDJW+5MwLr!+=KLMN">
                                                                <field name="value">50</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="XW^{SJ{;74lU+c3X{NUR">
                                                            <field name="name">array_number</field>
                                                            <value name="value">
                                                              <block type="random_many" id="}pIDcFvZ[vmAxHS#+ueu">
                                                                <value name="count">
                                                                  <block type="expression" id="|cIzb87K$4Jl()!)|~Sf">
                                                                    <field name="value">total</field>
                                                                  </block>
                                                                </value>
                                                                <value name="items">
                                                                  <block type="func_array_of_number" id="sfY21Nx@2#N+vEV+H=kgH" inline="true">
                                                                    <value name="items">
                                                                      <block type="expression" id="7sb[WYq)a?(Oi7[y/cFf">
                                                                        <field name="value">100</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="from">
                                                                      <block type="expression" id="f23U?9=DF)|LUC`b~c8I">
                                                                        <field name="value">1</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="variable" id="Fk;7Ytw=P$,0asuM#?R^">
                                                                <field name="name">i</field>
                                                                <value name="value">
                                                                  <block type="expression" id="^G/$09lt@2FLMRK`CnUZE">
                                                                    <field name="value">total</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="while_do_block" id="4Z7H,zHP@1(r2XBplE5xM">
                                                                    <value name="while">
                                                                      <block type="expression" id="k~pI+-xVpzy/+kMlcy,e">
                                                                        <field name="value">i &lt; 50</field>
                                                                      </block>
                                                                    </value>
                                                                    <statement name="do">
                                                                      <block type="variable" id="ACXl(pXrCme/r2FVRHx6">
                                                                        <field name="name">array_number[i]</field>
                                                                        <value name="value">
                                                                          <block type="expression" id="|9E,G08vzmgCGrUQJsRc">
                                                                            <field name="value">0</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="/ur%+`!G)xt{+0zz~?)m">
                                                                            <field name="name">i</field>
                                                                            <value name="value">
                                                                              <block type="expression" id="=iadxvObC~SnCov_|(H.">
                                                                                <field name="value">i+1</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="variable" id="pQ5MHm?icR}TjY0T)u[i">
                                                                        <field name="name">array_number</field>
                                                                        <value name="value">
                                                                          <block type="func_shuffle" id="DASc%Oj1c)%nC:.iY-C." inline="true">
                                                                            <value name="value">
                                                                              <block type="expression" id="u4kvnxqor6z!utcSZX(8">
                                                                                <field name="value">array_number</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="`xkFVCTP64[W26Pw9=6v">
                                                                            <field name="name">action</field>
                                                                            <value name="value">
                                                                              <block type="string_array" id="ZxL1X+0J=D[jA@216kQyM">
                                                                                <field name="items">a dragon to the gold|pirate to the treasure|boy to school|girl to the doll</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="variable" id="{:liewYkaeKzRRsrOB.W">
                                                                                <field name="name">ac</field>
                                                                                <value name="value">
                                                                                  <block type="random_number" id="-ZF#x(W#8:n0`0zRRe.u">
                                                                                    <value name="min">
                                                                                      <block type="expression" id="`_++z5^@2kV[CB=[%~O_P">
                                                                                        <field name="value">0</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="max">
                                                                                      <block type="expression" id="$bBl?^[DJsF/D+sLmTJE">
                                                                                        <field name="value">3</field>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                                                    <statement name="#props">
                                                                                      <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                                                            <field name="value">400</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                                                            <field name="value">40</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="contents">
                                                                                              <block type="string_value" id="#6btV9p,PGV}wMx.U�">
                                                                                                <field name="value">Select the set of numbers that show the ${color_name[color_pick]} path from the ${action[ac]}.</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                                                <field name="base">text</field>
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="fontSize">
                                                                                                      <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                                                        <field name="value">36</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="fill">
                                                                                                          <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                                                            <field name="value">black</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="stroke">
                                                                                                              <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                                                <field name="value">white</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="strokeThickness">
                                                                                                                  <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                                                    <field name="value">2</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <next>
                                                                                      <block type="image_shape" id="Y;bJAr1e1h6U},%s.uQ)">
                                                                                        <statement name="#props">
                                                                                          <block type="prop_position" id="!^3u%zZ#F#n39O3Re}2B">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="+t+|r4.;Pz!gD3fnnh~V">
                                                                                                <field name="value">200</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="Qyy!�?Fer#IRvj%rka">
                                                                                                <field name="value">100</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_anchor" id="a;47+Q@1al=$[=IDaauSa">
                                                                                                <field name="#prop">anchor</field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="$,ee(n(g`e^qx5dsS{s8">
                                                                                                    <field name="value">0.5</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="=6f)(n(wET!mKr[R7nLY">
                                                                                                    <field name="value">0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_size" id="8b]@1X8X)E%Rm9Eqb7uBI">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="width">
                                                                                                      <block type="expression" id="%[e{n9a@2W/{pXFA[u_Zb">
                                                                                                        <field name="value">0</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="height">
                                                                                                      <block type="expression" id="wQ_n@2I^$N1Jw9}X[BVDJ">
                                                                                                        <field name="value">0</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_image_key" id="Xs%9h)q.+.(HI!|_Sk^3">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="key">
                                                                                                          <block type="string_value" id="0U(!S.U1@1k+r(Zv^;8;z">
                                                                                                            <field name="value">s${ac}.png</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_image_src" id=",Z$4+iMkh1XguUZ`0v+2">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="src">
                                                                                                              <block type="string_value" id="nky@2i}N.~a;DnikVw2wL">
                                                                                                                <field name="value">${image_path}s${ac}.png</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="statement" id="K01zc|yk/zm1N!}g{e=B">
                                                                                            <field name="value">function re_Y(c){
    if(c == 0) return 5;
    else if(c == 1) return 5;
    else if(c == 2) return 12;
  else if(c == 3) return 3;
   else if(c == 4) return 5;
    else return 4;
}
                                                                                            </field>
                                                                                            <next>
                                                                                              <block type="variable" id="S;PM_z7MW-EjF#jZEvl)">
                                                                                                <field name="name">i</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="tH7Ov$Hh[d0):oXg{yFQ">
                                                                                                    <field name="value">0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="while_do_block" id="4wK$$UgBn%^jA[?x`f2.">
                                                                                                    <value name="while">
                                                                                                      <block type="expression" id="B`,j!gwAPG,V]nw8I_$i">
                                                                                                        <field name="value">i &lt; number_color</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <statement name="do">
                                                                                                      <block type="variable" id="cz+{ldj2K(d:LuN)b!W1">
                                                                                                        <field name="name">ox</field>
                                                                                                        <value name="value">
                                                                                                          <block type="random_number" id="#$0dE1J{kxXs3wK6%@1C,">
                                                                                                            <value name="min">
                                                                                                              <block type="expression" id="5t:{w/u#kL4EG(5Wq|fL">
                                                                                                                <field name="value">-2</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="max">
                                                                                                              <block type="expression" id="7qX`CO!%Ji]C`:~X@17uB">
                                                                                                                <field name="value">2</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="image_shape" id="_CZOVgR0xP-Wb]%l:j(`">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_position" id="-#f-u($)9^9SzV+p27@2K">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="{n=!75`QE~C#LXsp_2wu">
                                                                                                                    <field name="value">275+ox</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="}/Onz^Tf,}4JOzux_JS6">
                                                                                                                    <field name="value">100 + re_Y(color[i])</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_anchor" id="B4U@2i$!oF|56�@2L{NW">
                                                                                                                    <field name="#prop">anchor</field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id=")kjy``7nS@1gDs7F+UNil">
                                                                                                                        <field name="value">0</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="^Nzli2nuY[?V93vOv.8M">
                                                                                                                        <field name="value">0</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_size" id="o-|{t((e3nSq=YFhZlSQ">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="width">
                                                                                                                          <block type="expression" id="}k|0{]^2}A5I7@22aBsJP">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="height">
                                                                                                                          <block type="expression" id=",^p;#a5410XBoOob@2pnm">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_key" id="as1bU$Sqq;.Lx|y1pPQy">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="key">
                                                                                                                              <block type="string_value" id="GHNhs.t02at5c(MR@1R{V">
                                                                                                                                <field name="value">${color[i]}.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_src" id="iyz608JDV_r(/Y:qEI#3">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="src">
                                                                                                                                  <block type="string_value" id="9?wAnqT[x8a@2q^;|9To?">
                                                                                                                                    <field name="value">${image_path}${color[i]}.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="variable" id="2v6vSH#%_S#]@22LSs4/b">
                                                                                                                <field name="name">i</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="gbJ8O)J{YIbRS]F@1G_%`">
                                                                                                                    <field name="value">i+1</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="grid_shape" id="6tU?K^N8ec~A@1@1NwQ72N">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_grid_dimension" id="X4K:|9dKK%s9l4n5v@2lG">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="rows">
                                                                                                              <block type="expression" id="[Ev!.bxnzjc|-33v.%7=">
                                                                                                                <field name="value">10</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="cols">
                                                                                                              <block type="expression" id="u$7]/V/!ZWM4(Va??@2({">
                                                                                                                <field name="value">5</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_position" id="zLld84TfBg#!88]1,JD-">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="Cd6g{Y_P?:GyVyvQi1wJ">
                                                                                                                    <field name="value">400</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id=".F}E71+@1i)77!yQ`hmfu">
                                                                                                                    <field name="value">100</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_size" id="+o2CWr^.ohx)_bcIG53]">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="width">
                                                                                                                      <block type="expression" id="Z[x5iHG%KWC%KWQb}c-G">
                                                                                                                        <field name="value">174</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="height">
                                                                                                                      <block type="expression" id="@2E|Ob@1jF#gYtX8-%Iui@1">
                                                                                                                        <field name="value">201</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_anchor" id="1WJjXY^.h,t]eNgxaJ}q">
                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="DHx[vqs?.?nUD:AYPJWd">
                                                                                                                            <field name="value">0.5</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="e3D|RR7w!=Sufu):zV`c">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_grid_cell_source" id="v8+Yu211Qg8fX9Mu4th`">
                                                                                                                            <field name="#prop">cell.source</field>
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="~j@1,^liddf}oDg}E91^i">
                                                                                                                                <field name="value">array_number</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_grid_random" id="}?kS#x[b)9OGb@1`UdJGV">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <field name="random">FALSE</field>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_grid_show_borders" id=";y^wk4y%[VWjXOj~vrQZ">
                                                                                                                                    <field name="#prop">#showBorders</field>
                                                                                                                                    <field name="value">TRUE</field>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_stroke" id="+}]j9v]Q5eCixD42/AQ6">
                                                                                                                                        <field name="#prop">stroke</field>
                                                                                                                                        <value name="width">
                                                                                                                                          <block type="expression" id="]CIV8YA-`iZXM.LRQi_8">
                                                                                                                                            <field name="value">1</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="color">
                                                                                                                                          <block type="expression" id="{!S(z~]:{Er%d)zgRL">
                                                                                                                                            <field name="value">0x000000</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_grid_cell_template" id="7^6]Kv:2yrZi`cooAW2@2">
                                                                                                                                            <field name="variable">$cell</field>
                                                                                                                                            <field name="#prop">cell.template</field>
                                                                                                                                            <field name="#callback">$cell</field>
                                                                                                                                            <statement name="body">
                                                                                                                                              <block type="if_then_block" id="GQ%yw@1Gd;Pat2L=?~J%a">
                                                                                                                                                <value name="if">
                                                                                                                                                  <block type="expression" id=":p?t?l3//UV$p!=qq;st">
                                                                                                                                                    <field name="value">$cell.data != 0</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <statement name="then">
                                                                                                                                                  <block type="text_shape" id="1.QF{QcAiv8:i.S5?WYa">
                                                                                                                                                    <statement name="#props">
                                                                                                                                                      <block type="prop_position" id="mi+_xO%C,%,PR+S4l{.m">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="x">
                                                                                                                                                          <block type="expression" id="`s3=J8y1?/2BfNW}fX_a">
                                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="y">
                                                                                                                                                          <block type="expression" id="1]0WU}@14sS(sC0@1wr(i?">
                                                                                                                                                            <field name="value">$cell.centerY + 4</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_anchor" id="O.v@2mKn@1K#+tF;|}C}xI">
                                                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                                                            <value name="x">
                                                                                                                                                              <block type="expression" id="+3%GU[X3l%%X`Jv1QL7@2">
                                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="y">
                                                                                                                                                              <block type="expression" id="Kzp23HjHF7k]A-y1BJPm">
                                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_text_contents" id=";;Cid;Q)zQg.@1Tn}O`,J">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="contents">
                                                                                                                                                                  <block type="string_value" id="]WB8NQRDu7#(E2R)ebJm">
                                                                                                                                                                    <field name="value">${$cell.data}</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_text_style" id="0+JLbMt/#_Jk@1hO)({tM">
                                                                                                                                                                    <field name="base">text</field>
                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                      <block type="prop_text_style_font_size" id="w#~Z`7]cZuJT4U.{YGqh">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="fontSize">
                                                                                                                                                                          <block type="expression" id="D_ia+ldaSr;C_?[jg1=b">
                                                                                                                                                                            <field name="value">22</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_text_style_fill" id="F/opAR5_DdOv!z,y/M@1O">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="fill">
                                                                                                                                                                              <block type="string_value" id="0,,vl:D3IhXf;F`9#kb:">
                                                                                                                                                                                <field name="value">black</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="image_shape" id="QBXlPC;k7yug_zy~tR2%">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_position" id="p@2q4@16,1PI2FAdxs:1$i">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="i0:e!rf2Dt`Kf#DU~6_1">
                                                                                                                    <field name="value">600</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id=".kU(0T9(J|5B{jsLU~">
                                                                                                                    <field name="value">300</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_anchor" id="llb[ctlYsQ/NS7qUisO#">
                                                                                                                    <field name="#prop">anchor</field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="B/tMl})sL]FK=XkZ,%Rs">
                                                                                                                        <field name="value">0.5</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="@1@1(f5_ke:5)Zc7p}I={4">
                                                                                                                        <field name="value">1</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_size" id="!:62LsOaqX`+9w))j~K(">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="width">
                                                                                                                          <block type="expression" id="3~?or1,Z5z@2$2%p+m=A7">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="height">
                                                                                                                          <block type="expression" id="gXe/|1@1LPxF1l]ybPf~?">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_key" id="i6Y6av22E/@1XP7;W-5!:">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="key">
                                                                                                                              <block type="string_value" id="-C~nO{r}g^BI@2(@2Ubyqc">
                                                                                                                                <field name="value">e${ac}.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_src" id="/:3gy]SLn~4164fOT{Mi">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="src">
                                                                                                                                  <block type="string_value" id="}39;zWrwBJTdlQ=F@2#dE">
                                                                                                                                    <field name="value">${image_path}e${ac}.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
</xml>
END_XML]] */