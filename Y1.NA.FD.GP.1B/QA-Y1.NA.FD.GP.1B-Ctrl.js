
module.exports = [
  {
    "#type": "question",
    "name": "Y1.NA.FD.GP.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y1.NA.FD.GP.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "range < 4"
        },
        "then": [
          {
            "#type": "variable",
            "name": "x",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "2"
              },
              "max": {
                "#type": "expression",
                "value": "5"
              }
            }
          },
          {
            "#type": "variable",
            "name": "y",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "1"
              },
              "max": {
                "#type": "expression",
                "value": "3"
              }
            }
          }
        ],
        "else": [
          {
            "#type": "variable",
            "name": "x",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "2"
              },
              "max": {
                "#type": "expression",
                "value": "5"
              }
            }
          },
          {
            "#type": "variable",
            "name": "y",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "3"
              },
              "max": {
                "#type": "expression",
                "value": "6"
              }
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "list_shape",
        "value": {
          "#type": "string_array",
          "items": "space|triangle|heart|square|star|octagon|diamond"
        }
      },
      {
        "#type": "variable",
        "name": "lst",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "6"
          }
        }
      },
      {
        "#type": "variable",
        "name": "it",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "8"
          }
        }
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "40"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "Drag the ${list_shape[lst]} into equal groups.\nThen complete the sentence."
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "36"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "statement",
        "value": "var col, row;\nif(x == 2) { row = 2; col =1}\nif(x == 3) { row = 2; col =2}\nif(x == 4) { row = 2; col =2}\nif(x == 5) { row = 3; col =2}\n\nvar array = [it, it, it, it, it, it, it, it, it, it, it, it, it, it, it, it, it, it, it];"
      },
      {
        "#type": "variable",
        "name": "X",
        "value": {
          "#type": "random_many",
          "count": {
            "#type": "expression",
            "value": "y"
          },
          "items": {
            "#type": "expression",
            "value": "array"
          }
        }
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "row"
            },
            "cols": {
              "#type": "expression",
              "value": "col"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "200"
            },
            "y": {
              "#type": "expression",
              "value": "230"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "380"
            },
            "height": {
              "#type": "expression",
              "value": "300"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "func",
              "name": "arrayOfNumber",
              "args": [
                {
                  "#type": "expression",
                  "value": "x"
                },
                {
                  "#type": "expression",
                  "value": "1"
                }
              ]
            }
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_cell_template",
            "variable": "$cell",
            "#prop": "cell.template",
            "#callback": "$cell",
            "body": [
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "180"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "100"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "drop.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}drop.png"
                  }
                ]
              },
              {
                "#type": "drop_shape",
                "multiple": true,
                "resultMode": "none",
                "groupName": "",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "143"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "86"
                    }
                  },
                  {
                    "#type": "prop_spacing",
                    "#prop": "",
                    "spacing": {
                      "#type": "expression",
                      "value": "10"
                    }
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "image_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "590"
            },
            "y": {
              "#type": "expression",
              "value": "190"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "0"
            },
            "height": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "380"
            },
            "height": {
              "#type": "expression",
              "value": "150"
            }
          },
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "box.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}box.png"
          }
        ]
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "590"
            },
            "y": {
              "#type": "expression",
              "value": "140"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "(Drag into the box below)"
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "24"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "statement",
        "value": "var col2, row2;\nvar num = x*y;\nif(num < 11) { row2 = 1; col2 = num;}\nelse if(num < 21) { row2 = 2; col2 = 10;}\nelse { row2 = 3; col2 = 10;}"
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "row2"
            },
            "cols": {
              "#type": "expression",
              "value": "col2"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "590"
            },
            "y": {
              "#type": "expression",
              "value": "200"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "359"
            },
            "height": {
              "#type": "expression",
              "value": "90"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "func",
              "name": "arrayOfNumber",
              "args": [
                {
                  "#type": "expression",
                  "value": "num"
                },
                {
                  "#type": "expression",
                  "value": "1"
                }
              ]
            }
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_cell_template",
            "variable": "$cell",
            "#prop": "cell.template",
            "#callback": "$cell",
            "body": [
              {
                "#type": "choice_image_shape",
                "action": "drag",
                "hideBox": false,
                "resultMode": "default",
                "#props": [
                  {
                    "#type": "prop_value",
                    "#prop": "",
                    "value": {
                      "#type": "expression",
                      "value": "it"
                    }
                  },
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "0"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "${lst}_${it}.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}${lst}_${it}.png"
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "list_shape",
        "#props": [
          {
            "#type": "prop_list_direction",
            "#prop": "",
            "dir": "horizontal"
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "580"
            },
            "y": {
              "#type": "expression",
              "value": "310"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          }
        ],
        "items": [
          {
            "#type": "json",
            "#props": [
              {
                "#type": "prop_list_align",
                "#prop": "",
                "align": "middle"
              }
            ],
            "template": {
              "#callback": "$item",
              "variable": "$item",
              "body": [
                {
                  "#type": "text_shape",
                  "#props": [
                    {
                      "#type": "prop_text_contents",
                      "#prop": "",
                      "contents": {
                        "#type": "expression",
                        "value": "'There are '"
                      }
                    },
                    {
                      "#prop": "",
                      "style": {
                        "#type": "json",
                        "base": "text",
                        "#props": [
                          {
                            "#type": "prop_text_style_font_size",
                            "#prop": "",
                            "fontSize": {
                              "#type": "expression",
                              "value": "36"
                            }
                          },
                          {
                            "#type": "prop_text_style_fill",
                            "#prop": "",
                            "fill": "black"
                          },
                          {
                            "#type": "prop_text_style_stroke",
                            "#prop": "",
                            "stroke": "white"
                          },
                          {
                            "#type": "prop_text_style_stroke_thickness",
                            "#prop": "",
                            "strokeThickness": {
                              "#type": "expression",
                              "value": "2"
                            }
                          }
                        ]
                      }
                    }
                  ]
                }
              ]
            }
          },
          {
            "#type": "json",
            "#props": [
              {
                "#type": "prop_list_align",
                "#prop": "",
                "align": "middle"
              }
            ],
            "template": {
              "#callback": "$item",
              "variable": "$item",
              "body": [
                {
                  "#type": "image_shape",
                  "#props": [
                    {
                      "#type": "prop_image_key",
                      "#prop": "",
                      "key": "shape.png"
                    },
                    {
                      "#type": "prop_image_src",
                      "#prop": "",
                      "src": "${image_path}shape.png"
                    }
                  ]
                },
                {
                  "#type": "choice_input_shape",
                  "#props": [
                    {
                      "#type": "prop_value",
                      "#prop": "",
                      "value": {
                        "#type": "expression",
                        "value": "y"
                      }
                    },
                    {
                      "#type": "prop_size",
                      "#prop": "",
                      "width": {
                        "#type": "expression",
                        "value": "115"
                      },
                      "height": {
                        "#type": "expression",
                        "value": "64"
                      }
                    },
                    {
                      "#type": "prop_input_keyboard",
                      "#prop": "",
                      "keyboard": "numbers1"
                    },
                    {
                      "#type": "prop_input_max_length",
                      "#prop": "",
                      "maxLength": {
                        "#type": "expression",
                        "value": "y.toString().length"
                      }
                    },
                    {
                      "#type": "prop_input_result_position",
                      "#prop": "",
                      "resultPosition": "top"
                    },
                    {
                      "#type": "prop_tab_order",
                      "#prop": "",
                      "tabOrder": {
                        "#type": "expression",
                        "value": "0"
                      }
                    },
                    {
                      "#type": "prop_stroke",
                      "#prop": "stroke"
                    },
                    {
                      "#type": "prop_fill",
                      "#prop": "fill"
                    },
                    {
                      "#prop": "",
                      "style": {
                        "#type": "json",
                        "base": "text",
                        "#props": [
                          {
                            "#type": "prop_text_style_font_size",
                            "#prop": "",
                            "fontSize": {
                              "#type": "expression",
                              "value": "36"
                            }
                          },
                          {
                            "#type": "prop_text_style_fill",
                            "#prop": "",
                            "fill": "black"
                          },
                          {
                            "#type": "prop_text_style_stroke",
                            "#prop": "",
                            "stroke": "white"
                          },
                          {
                            "#type": "prop_text_style_stroke_thickness",
                            "#prop": "",
                            "strokeThickness": {
                              "#type": "expression",
                              "value": "2"
                            }
                          }
                        ]
                      }
                    }
                  ],
                  "#init": "algorithmic_input"
                }
              ]
            }
          },
          {
            "#type": "json",
            "#props": [
              {
                "#type": "prop_list_align",
                "#prop": "",
                "align": "middle"
              }
            ],
            "template": {
              "#callback": "$item",
              "variable": "$item",
              "body": [
                {
                  "#type": "text_shape",
                  "#props": [
                    {
                      "#type": "prop_text_contents",
                      "#prop": "",
                      "contents": {
                        "#type": "expression",
                        "value": "' ' + list_shape[lst] + (y > 1 ? 's' : '')"
                      }
                    },
                    {
                      "#prop": "",
                      "style": {
                        "#type": "json",
                        "base": "text",
                        "#props": [
                          {
                            "#type": "prop_text_style_font_size",
                            "#prop": "",
                            "fontSize": {
                              "#type": "expression",
                              "value": "36"
                            }
                          },
                          {
                            "#type": "prop_text_style_fill",
                            "#prop": "",
                            "fill": "black"
                          },
                          {
                            "#type": "prop_text_style_stroke",
                            "#prop": "",
                            "stroke": "white"
                          },
                          {
                            "#type": "prop_text_style_stroke_thickness",
                            "#prop": "",
                            "strokeThickness": {
                              "#type": "expression",
                              "value": "2"
                            }
                          }
                        ]
                      }
                    }
                  ]
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "670"
            },
            "y": {
              "#type": "expression",
              "value": "345"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "in each group."
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "36"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "loadAssets",
        "value": {
          "#type": "custom_image_list",
          "link": "${image_path}",
          "images": "${lst}_${it}|drop"
        }
      },
      {
        "#type": "statement",
        "value": "var color = ['#e60000', '#e68a00', '#009933', '#0073e6', '#6600cc'];"
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 1:</u></br></br>\n\nThere are ${x} groups.</br>\nName the shapes into"
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          {
            "#type": "expression",
            "value": "' '"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < x"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<p style='color: ${color[i]}; display: inline-block'>Group ${i+1}</p>"
            ]
          },
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "i != x-1"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  {
                    "#type": "expression",
                    "value": "', '"
                  }
                ]
              }
            ],
            "else": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  {
                    "#type": "expression",
                    "value": "'.'"
                  }
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</br></br><style>\n  table{width: 950px;}\ntd{width: 200px; text-align: center}\n  </style>\n\n<center>\n  <table style='background: rgb(250,250,250, 0.6)'>\n  <tr><td colspan=${x}>\n    <table>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "variable",
        "name": "count",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < row2"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<tr>"
            ]
          },
          {
            "#type": "variable",
            "name": "j",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "j <col2"
            },
            "do": [
              {
                "#type": "if_then_block",
                "if": {
                  "#type": "expression",
                  "value": "count < num"
                },
                "then": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<td style='height: 80px'><img src='@sprite.src(${lst}_${it})'/><sup style='color: ${color[count % x]}'>${count % x + 1}</sup></td>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "count",
                    "value": {
                      "#type": "expression",
                      "value": "count +1"
                    }
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "j",
                "value": {
                  "#type": "expression",
                  "value": "j+1"
                }
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</tr>"
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</table>\n</td>\n</tr>\n<tr>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < x"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<td style='height: 110px; background-image: url(\"@sprite.src(drop)\"); background-repeat: no-repeat; background-position: center; background-size: 150px 110px'></td>"
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</tr><tr>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < x"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<td style='color: ${color[i]}'>Group ${i+1}</td>"
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</tr></table>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 2: Now, let's drag shapes into the assigned Group.</u></br></br>"
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<style>\n  table{width: 950px;}\ntd{width: 200px; text-align: center}\nimg{padding: 2px}\n  </style>\n\n<center>\n  <table style='background: rgb(250,250,250, 0.6)'>"
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<tr>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < x"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<td style='height: 120px; background-image: url(\"@sprite.src(drop)\"); background-repeat: no-repeat; background-position: center; background-size: 170px 120px'>"
            ]
          },
          {
            "#type": "variable",
            "name": "j",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "j < y"
            },
            "do": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<img src='@sprite.src(${lst}_${it})'/><sup style='color: ${color[i]}'>${i+1}</sup>"
                ]
              },
              {
                "#type": "if_then_block",
                "if": {
                  "#type": "expression",
                  "value": "j % 3 == 2"
                },
                "then": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "</br>"
                    ]
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "j",
                "value": {
                  "#type": "expression",
                  "value": "j+1"
                }
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</td>"
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</tr><tr>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < x"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<td style='color: ${color[i]}'>Group ${i+1}</td>"
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</tr></table>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 3: Finally, count the number of shapes in one group.</u></br></br>\n\n<style>\n  td{text-align: center}\n  img {padding: 2px}\n  </style>\n<center>\n  <table>\n  <tr>\n  <td style='background-image: url(\"@sprite.src(drop)\"); background-size: 200px 150px ; width: 200px; height: 150px'>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < y"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<img src='@sprite.src(${lst}_${it})'/><sup style=''>${i+1}</sup>"
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "i % 3 == 2"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</br>"
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</td></tr></table>\n</center>\n</br>\nThere are <b>${y}</b> ${list_shape[lst]}${y > 1 ? 's':''} in each group."
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="0" y="0">
    <field name="name">Y1.NA.FD.GP.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y1.NA.FD.GP.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="YepMQT@2uwp2rRPvmnRg1">
                                    <field name="name">range</field>
                                    <value name="value">
                                      <block type="expression" id="UxgQ^Y5]rb}vpw[D=l-!">
                                        <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="if_then_else_block" id="$ylCar3Tgt%RlI|Rrb@1g">
                                        <value name="if">
                                          <block type="expression" id="?_r4h[;jvsPNMUX,EDe5">
                                            <field name="value">range &lt; 4</field>
                                          </block>
                                        </value>
                                        <statement name="then">
                                          <block type="variable" id="==ua`_hY{`l5!wpe(DxN">
                                            <field name="name">x</field>
                                            <value name="value">
                                              <block type="random_number" id="`=v3o^lV}4,,4rO:}~3E">
                                                <value name="min">
                                                  <block type="expression" id="]$c/az~{jEtob8-yIMQ;">
                                                    <field name="value">2</field>
                                                  </block>
                                                </value>
                                                <value name="max">
                                                  <block type="expression" id="XYM7FU==PzewuAZZHv;V">
                                                    <field name="value">5</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id="|,%+Frq2@2./;ZW:TR~GP">
                                                <field name="name">y</field>
                                                <value name="value">
                                                  <block type="random_number" id="{?.FazlyOO2s+3gb]q9W">
                                                    <value name="min">
                                                      <block type="expression" id="|4ExRO^Hy:tk#h}K@29SX">
                                                        <field name="value">1</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="{SvQFh!.+tC=`LW`Z2+u">
                                                        <field name="value">3</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                              </block>
                                            </next>
                                          </block>
                                        </statement>
                                        <statement name="else">
                                          <block type="variable" id="kv_wVZI.;.QHWs2b;g{U">
                                            <field name="name">x</field>
                                            <value name="value">
                                              <block type="random_number" id="VQFs,}JFRu$sA1,,yJw!">
                                                <value name="min">
                                                  <block type="expression" id="MKLY@1(U+EG!%[s:6#L$l">
                                                    <field name="value">2</field>
                                                  </block>
                                                </value>
                                                <value name="max">
                                                  <block type="expression" id="HS5R/?W$0q2vBcy1W/D6">
                                                    <field name="value">5</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id="a[!GVkOB|5vN+.2E{vXw">
                                                <field name="name">y</field>
                                                <value name="value">
                                                  <block type="random_number" id="-Q+5/Nr0.bI$sqzn]vah">
                                                    <value name="min">
                                                      <block type="expression" id="6FZbSgRyh1`d+AOnm|qg">
                                                        <field name="value">3</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="R85Rm1Ib]2Y];:LRXX0|">
                                                        <field name="value">6</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                              </block>
                                            </next>
                                          </block>
                                        </statement>
                                        <next>
                                          <block type="variable" id="NZ6^:7Qt(|cI0RdoW5(u">
                                            <field name="name">list_shape</field>
                                            <value name="value">
                                              <block type="string_array" id="~r%bJfvSICaK/:tW+6J-">
                                                <field name="items">space|triangle|heart|square|star|octagon|diamond</field>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id="2z#T;C8p52Euy!6DIk=h">
                                                <field name="name">lst</field>
                                                <value name="value">
                                                  <block type="random_number" id="Es24m5qG[_~6FwWh?|kQ">
                                                    <value name="min">
                                                      <block type="expression" id="JqD?VP:ZJW6BA?66!%`k">
                                                        <field name="value">1</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="C}c2QWy^}Gaw;;ekvu5K">
                                                        <field name="value">6</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="SdQzvz.^hYfuhNcs=M/;">
                                                    <field name="name">it</field>
                                                    <value name="value">
                                                      <block type="random_number" id="7.f^3@1mWmN]Q;{YSCB;G">
                                                        <value name="min">
                                                          <block type="expression" id="cY#Q1[W=SqT{^-1w@1]IA">
                                                            <field name="value">1</field>
                                                          </block>
                                                        </value>
                                                        <value name="max">
                                                          <block type="expression" id="Dwrw!Qc.l;X%fXsbXpzW">
                                                            <field name="value">8</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="text_shape" id="o~0R)}@2==Eb@2/n=bO+9e">
                                                        <statement name="#props">
                                                          <block type="prop_position" id="3],pryQ=+8j}Ry}|LM6D">
                                                            <field name="#prop"></field>
                                                            <value name="x">
                                                              <block type="expression" id="RfiTGj+CVEF?|evt`r:i">
                                                                <field name="value">400</field>
                                                              </block>
                                                            </value>
                                                            <value name="y">
                                                              <block type="expression" id="v;{{43QPASWV.SJ_y},L">
                                                                <field name="value">40</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="prop_text_contents" id="^4=0_qGU3L|h@18^d2(L9">
                                                                <field name="#prop"></field>
                                                                <value name="contents">
                                                                  <block type="string_value" id="vWo8T^H6p^boHpt|EbRB">
                                                                    <field name="value">Drag the ${list_shape[lst]} into equal groups.
Then complete the sentence.
                                                                    </field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_text_style" id=",C.Dwl4w~{q%!`6]H(zN">
                                                                    <field name="base">text</field>
                                                                    <statement name="#props">
                                                                      <block type="prop_text_style_font_size" id="Z2A!EAjOwA:PgAj`36;a">
                                                                        <field name="#prop"></field>
                                                                        <value name="fontSize">
                                                                          <block type="expression" id="=t.L|HE;Y[3|YG8@1_s1a">
                                                                            <field name="value">36</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_text_style_fill" id="cVQz#5F-FO$1|dXZfWA?">
                                                                            <field name="#prop"></field>
                                                                            <value name="fill">
                                                                              <block type="string_value" id="`FGThR60e]T(o6=~zVe@1">
                                                                                <field name="value">black</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                <field name="#prop"></field>
                                                                                <value name="stroke">
                                                                                  <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                    <field name="value">white</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="strokeThickness">
                                                                                      <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                        <field name="value">2</field>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </statement>
                                                        <next>
                                                          <block type="statement" id="PSJsIgB:v^(j5cDbC:gX">
                                                            <field name="value">var col, row;
if(x == 2) { row = 2; col =1}
if(x == 3) { row = 2; col =2}
if(x == 4) { row = 2; col =2}
if(x == 5) { row = 3; col =2}

var array = [it, it, it, it, it, it, it, it, it, it, it, it, it, it, it, it, it, it, it];
                                                            </field>
                                                            <next>
                                                              <block type="variable" id="4@2Stse}8-?cr{LtTu`]%">
                                                                <field name="name">X</field>
                                                                <value name="value">
                                                                  <block type="random_many" id="]SZ3YdVO!JSXKBt;eS~%">
                                                                    <value name="count">
                                                                      <block type="expression" id="PFV=09yUTD092Srb.@1~6">
                                                                        <field name="value">y</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="items">
                                                                      <block type="expression" id="c5k)McBqjdU#g$ujerLf">
                                                                        <field name="value">array</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="grid_shape" id="6tU?K^N8ec~A@1@1NwQ72N">
                                                                    <statement name="#props">
                                                                      <block type="prop_grid_dimension" id="X4K:|9dKK%s9l4n5v@2lG">
                                                                        <field name="#prop"></field>
                                                                        <value name="rows">
                                                                          <block type="expression" id="[Ev!.bxnzjc|-33v.%7=">
                                                                            <field name="value">row</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="cols">
                                                                          <block type="expression" id="u$7]/V/!ZWM4(Va??@2({">
                                                                            <field name="value">col</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_position" id="zLld84TfBg#!88]1,JD-">
                                                                            <field name="#prop"></field>
                                                                            <value name="x">
                                                                              <block type="expression" id="Cd6g{Y_P?:GyVyvQi1wJ">
                                                                                <field name="value">200</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="y">
                                                                              <block type="expression" id=".F}E71+@1i)77!yQ`hmfu">
                                                                                <field name="value">230</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_size" id="+o2CWr^.ohx)_bcIG53]">
                                                                                <field name="#prop"></field>
                                                                                <value name="width">
                                                                                  <block type="expression" id="Z[x5iHG%KWC%KWQb}c-G">
                                                                                    <field name="value">380</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="height">
                                                                                  <block type="expression" id="@2E|Ob@1jF#gYtX8-%Iui@1">
                                                                                    <field name="value">300</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_anchor" id="1WJjXY^.h,t]eNgxaJ}q">
                                                                                    <field name="#prop">anchor</field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="DHx[vqs?.?nUD:AYPJWd">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="e3D|RR7w!=Sufu):zV`c">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_grid_cell_source" id="v8+Yu211Qg8fX9Mu4th`">
                                                                                        <field name="#prop">cell.source</field>
                                                                                        <value name="value">
                                                                                          <block type="func_array_of_number" id="Ya@18O9n2n[N=)|[JfYdP" inline="true">
                                                                                            <value name="items">
                                                                                              <block type="expression" id="$V#CjK,H]m}fNsLdao=y">
                                                                                                <field name="value">x</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="from">
                                                                                              <block type="expression" id="3iPG{)MGy8%^S_o;oPt-">
                                                                                                <field name="value">1</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_grid_random" id="}?kS#x[b)9OGb@1`UdJGV">
                                                                                            <field name="#prop"></field>
                                                                                            <field name="random">FALSE</field>
                                                                                            <next>
                                                                                              <block type="prop_grid_show_borders" id=";y^wk4y%[VWjXOj~vrQZ">
                                                                                                <field name="#prop">#showBorders</field>
                                                                                                <field name="value">FALSE</field>
                                                                                                <next>
                                                                                                  <block type="prop_grid_cell_template" id="7^6]Kv:2yrZi`cooAW2@2">
                                                                                                    <field name="variable">$cell</field>
                                                                                                    <field name="#prop">cell.template</field>
                                                                                                    <field name="#callback">$cell</field>
                                                                                                    <statement name="body">
                                                                                                      <block type="image_shape" id=":(ny)4okP;p?;8tsK}Zp">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_position" id="BrGvf@2rpK.b4+@1Paw;%w">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="n9Ayv!=Q@1T9ZR@1f_KQ^d">
                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="TUn@1yE0^Ou:c{kO}}E$!">
                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_size" id="ZVVvq_,uIBB/eH+{~T}-">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="width">
                                                                                                                  <block type="expression" id="bOcqTZZkNNld^2~D;ZM5">
                                                                                                                    <field name="value">180</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="height">
                                                                                                                  <block type="expression" id="a98brgBtt%s_mF|[N;nn">
                                                                                                                    <field name="value">100</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_image_key" id="}E3jPy:oP-10H61elbop">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="key">
                                                                                                                      <block type="string_value" id="7w25DXb{}V9I/$p]!D(i">
                                                                                                                        <field name="value">drop.png</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_src" id="NL[052tO8I@1(cwr?m6-#">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="src">
                                                                                                                          <block type="string_value" id="qD~C1R+D/fG2~Il@1(Q{v">
                                                                                                                            <field name="value">${image_path}drop.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="drop_shape" id="nc~Uz3^ju#kyIzQMcR/{">
                                                                                                            <field name="multiple">TRUE</field>
                                                                                                            <field name="resultMode">none</field>
                                                                                                            <field name="groupName"></field>
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_position" id="..0;9/ox+hgyxneXtAyE">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="rZSyhE;E|#U|do~Q$cns">
                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="?PecpBd.=265:aS7hf2U">
                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_size" id="{m}[_U,FVEn;-K4#%qUt">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="width">
                                                                                                                      <block type="expression" id="Z@2@11,$Q}=J=l!L#+N-oU">
                                                                                                                        <field name="value">143</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="height">
                                                                                                                      <block type="expression" id="/tI1uEJox0#6,_G?7Ug4">
                                                                                                                        <field name="value">86</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_spacing" id="S-0Z~GLb_bqqG:npgyVz">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="spacing">
                                                                                                                          <block type="expression" id="^7lZSf@2vrxPBFU0gOab]">
                                                                                                                            <field name="value">10</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="image_shape" id="0MP45k3~^q2MO{g]+iWt">
                                                                        <statement name="#props">
                                                                          <block type="prop_position" id="ofQ(L`:sZleSV-{Ul_$s">
                                                                            <field name="#prop"></field>
                                                                            <value name="x">
                                                                              <block type="expression" id="@2V?jV;V3+Wqaw{qkopQ%">
                                                                                <field name="value">590</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="y">
                                                                              <block type="expression" id="$|vstdi~@13lGq;#@1EpOh">
                                                                                <field name="value">190</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_size" id="[eL(r:8X8EY/tTI#EJv6">
                                                                                <field name="#prop"></field>
                                                                                <value name="width">
                                                                                  <block type="expression" id="EJ)UoW/H9M_}w]v$3+$T">
                                                                                    <field name="value">0</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="height">
                                                                                  <block type="expression" id="k6?ofue`E9ib2OO2fK6|">
                                                                                    <field name="value">0</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_size" id="Btcy{dm/RCVTQp_~S+p)">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="width">
                                                                                      <block type="expression" id="NM7|.zs@13C6yDbpGCyz@2">
                                                                                        <field name="value">380</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="height">
                                                                                      <block type="expression" id=",63:+gvYzdq9D4b6Hl5j">
                                                                                        <field name="value">150</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_image_key" id="8tK+Bwnzaoo{0ucl8}9N">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="key">
                                                                                          <block type="string_value" id="w9k~KB6q@2|R62?pt4Y#,">
                                                                                            <field name="value">box.png</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_image_src" id="M00Fe4f4(}:DOo[@1u(_x">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="src">
                                                                                              <block type="string_value" id="okn=4~ZTA/)TCZb~k_,1">
                                                                                                <field name="value">${image_path}box.png</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="text_shape" id="H,n10k5+tqrONNXSNfRt">
                                                                            <statement name="#props">
                                                                              <block type="prop_position" id="#E{`/.Hl5xM#inYH]vU2">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="F-Tdf}J42r~:_zwlRV@18">
                                                                                    <field name="value">590</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="mZgF8/#8QoLMI.|P?9Vz">
                                                                                    <field name="value">140</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_text_contents" id="xX-pY}rhB~@2]M_lS=/Eg">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="contents">
                                                                                      <block type="string_value" id="mZo8tHzw)dY@23U[-I:Lh">
                                                                                        <field name="value">(Drag into the box below)</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_text_style" id="cvrwdY$A!t:f2Ll:`^)#">
                                                                                        <field name="base">text</field>
                                                                                        <statement name="#props">
                                                                                          <block type="prop_text_style_font_size" id="%A/@1d6LA(mr5sMsVNvf9">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="fontSize">
                                                                                              <block type="expression" id="S%I,-^3Z(RPG%KJVtA:I">
                                                                                                <field name="value">24</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_style_fill" id="8h+FFUb70tn-$3~|c^Vc">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="fill">
                                                                                                  <block type="string_value" id="xI;cOeN]-K%(k?l-J(pi">
                                                                                                    <field name="value">black</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style_stroke" id="h_!-j1@1xRfDrbyg5;-Hb">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="stroke">
                                                                                                      <block type="string_value" id="GZR2mT@1hf@1x/N4~s;@1#P">
                                                                                                        <field name="value">white</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style_stroke_thickness" id="Rb|]pWM.q@2@15Hm/~u#35">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="strokeThickness">
                                                                                                          <block type="expression" id="!VoJp:QcYtPWSdC4XC;[">
                                                                                                            <field name="value">2</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="statement" id="jKBVSj,am@1+-|RmwymS7">
                                                                                <field name="value">var col2, row2;
var num = x@2y;
if(num &lt; 11) { row2 = 1; col2 = num;}
else if(num &lt; 21) { row2 = 2; col2 = 10;}
else { row2 = 3; col2 = 10;}
                                                                                </field>
                                                                                <next>
                                                                                  <block type="grid_shape" id="1|`fC[+$Yy6NR{$9E0I|">
                                                                                    <statement name="#props">
                                                                                      <block type="prop_grid_dimension" id="|,4./1@2n_(zrnLUyA_s/">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="rows">
                                                                                          <block type="expression" id="bN|GY;Xdf[J!.dVh.-Iq">
                                                                                            <field name="value">row2</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="cols">
                                                                                          <block type="expression" id="RH15v|0|C`[2^Puo6mHL">
                                                                                            <field name="value">col2</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_position" id="?M`}PQI=$c`LZCkwG$jN">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="pQcA/r`Pr1eDh5I%ZSm3">
                                                                                                <field name="value">590</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="(H+{Hj.OZ9-bXend~SDi">
                                                                                                <field name="value">200</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_size" id="N!%;W%`w0Km:XVQpOht{">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="width">
                                                                                                  <block type="expression" id="{(W6;W=+Y,-5p(`@25g+@2">
                                                                                                    <field name="value">359</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="height">
                                                                                                  <block type="expression" id="}0_RB!V~dHJ:y@1=#A@2k[">
                                                                                                    <field name="value">90</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_anchor" id="jeB8nOBtVn8DsxbkiR70">
                                                                                                    <field name="#prop">anchor</field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="awb`c=2D|`D~,2I^bh?,">
                                                                                                        <field name="value">0.5</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="iJN}cG$g8Iyo-L5!LJLT">
                                                                                                        <field name="value">0.5</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_cell_source" id="GF58Cz9-vA5LS!oYKnqr">
                                                                                                        <field name="#prop">cell.source</field>
                                                                                                        <value name="value">
                                                                                                          <block type="func_array_of_number" id="Q`;31puH|9y0#!?@13jA]" inline="true">
                                                                                                            <value name="items">
                                                                                                              <block type="expression" id="#gG`z@2754+S]0^%Z5T+C">
                                                                                                                <field name="value">num</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="from">
                                                                                                              <block type="expression" id="SkrX^/X_1#H~akAB5K`v">
                                                                                                                <field name="value">1</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_grid_random" id="#PAp^B2QM`$q[v^Ny5~V">
                                                                                                            <field name="#prop"></field>
                                                                                                            <field name="random">FALSE</field>
                                                                                                            <next>
                                                                                                              <block type="prop_grid_show_borders" id="z+G07F!3^_}d,!bd!/kM">
                                                                                                                <field name="#prop">#showBorders</field>
                                                                                                                <field name="value">FALSE</field>
                                                                                                                <next>
                                                                                                                  <block type="prop_grid_cell_template" id="hf%lv[!oK|@2A-puLGB.g">
                                                                                                                    <field name="variable">$cell</field>
                                                                                                                    <field name="#prop">cell.template</field>
                                                                                                                    <field name="#callback">$cell</field>
                                                                                                                    <statement name="body">
                                                                                                                      <block type="choice_image_shape" id="tf=@1zyXh;WNhKEm;ZK7P">
                                                                                                                        <field name="action">drag</field>
                                                                                                                        <field name="hideBox">FALSE</field>
                                                                                                                        <field name="resultMode">default</field>
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_value" id="b=n/Z`rlf}e0|#n(#0JT">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id=",ei)SRN09BE$Jt,U{9vj">
                                                                                                                                <field name="value">it</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_position" id="KJYl5EP=W|737?9zY0q[">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="p^Tnylf`^9T3A$QXLO!b">
                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="%qRa/DxqD4Ef;Dw@2i!d!">
                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_size" id="Q%i@1s[.]f?B],1r}bWv7">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="width">
                                                                                                                                      <block type="expression" id="n!GlgxLYOT1+zQbM8{%L">
                                                                                                                                        <field name="value">0</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="height">
                                                                                                                                      <block type="expression" id="oORep:l-@2/##K,docCbn">
                                                                                                                                        <field name="value">0</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_key" id="$;rd$RRkGYr!8f=jH7L2">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="key">
                                                                                                                                          <block type="string_value" id="8ngs{@2KdBZ?wyb/9Y=9L">
                                                                                                                                            <field name="value">${lst}_${it}.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_src" id=":%Bi4Ibj8X:M|vOZ{|1,">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="src">
                                                                                                                                              <block type="string_value" id="tN9HAeFpMg0$yK/sQRz/">
                                                                                                                                                <field name="value">${image_path}${lst}_${it}.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <next>
                                                                                      <block type="list_shape" id="l4W#EYH!$f#ri}J:xu;H">
                                                                                        <statement name="#props">
                                                                                          <block type="prop_list_direction" id="/yV:.EJT=#2@1ma]45{N4">
                                                                                            <field name="#prop"></field>
                                                                                            <field name="dir">horizontal</field>
                                                                                            <next>
                                                                                              <block type="prop_position" id="MYAhTDYAvaO-^N/P@1kB-">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="mwui(a@24K%9(S8/I{M1;">
                                                                                                    <field name="value">580</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id=")=bV,tHV?C_[Dv2n|G2t">
                                                                                                    <field name="value">310</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_anchor" id="0$MX]q#o(Gt!X56`;e]@1">
                                                                                                    <field name="#prop">anchor</field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id=";aT@1gOM[hN`Ki5G/vdec">
                                                                                                        <field name="value">0.5</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="O^|r%D-D_/xh;{zv4aP!">
                                                                                                        <field name="value">0.5</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <statement name="items">
                                                                                          <block type="list_item_shape" id="`Q:u6r9[J5n@1}g~m[tm0">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_list_align" id="y1DagYQ!`2x~zBz.Lek}">
                                                                                                <field name="#prop"></field>
                                                                                                <field name="align">middle</field>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <statement name="template">
                                                                                              <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="contents">
                                                                                                      <block type="expression" id="k((cKrzgk($RK;v-hPC3">
                                                                                                        <field name="value">'There are '</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                                                        <field name="base">text</field>
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="fontSize">
                                                                                                              <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                                                                <field name="value">36</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="fill">
                                                                                                                  <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                                                                    <field name="value">black</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_stroke" id="7hIK/:%YHy$./Z8i5h-U">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="stroke">
                                                                                                                      <block type="string_value" id="s}vUTGbnzzwUMoyx3sj9">
                                                                                                                        <field name="value">white</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_stroke_thickness" id="46C4.%l(:]tyuN8F6.Xj">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="strokeThickness">
                                                                                                                          <block type="expression" id="sF{N:lx`m])dspg/7nXO">
                                                                                                                            <field name="value">2</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="list_item_shape" id="XZw4h|_}D{+pqqOsNp2?">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_list_align" id="=.TsM-gnlXs{C3ZPPy:j">
                                                                                                    <field name="#prop"></field>
                                                                                                    <field name="align">middle</field>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <statement name="template">
                                                                                                  <block type="image_shape" id=":yNx;S^90|S3WLb:@2qLj">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_image_key" id="8Yd5a=l;$PUFc;NUeiZo">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="key">
                                                                                                          <block type="string_value" id="~r]Wq./ZAIHEL`wC_.R;">
                                                                                                            <field name="value">shape.png</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_image_src" id="P]8@1F,`EDIa:2RbmXXnL">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="src">
                                                                                                              <block type="string_value" id="1(PLP#a/#{Bfn$HvsMu%">
                                                                                                                <field name="value">${image_path}shape.png</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="algorithmic_input_shape" id="Nc%$1u4v_1V~C)3~pk+,">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_value" id="Pyj2!WoRm@1,%w8h/if@1L">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="@2Z7-PsfPQDn}#wiNi[X6">
                                                                                                                <field name="value">y</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_size" id="F1KOq0K`8Q_0b55`cZ?v">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="width">
                                                                                                                  <block type="expression" id="R5dW=K~MhRA$fa4TAV^4">
                                                                                                                    <field name="value">115</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="height">
                                                                                                                  <block type="expression" id="{h?lha$9|vJV/:طas">
                                                                                                                    <field name="value">64</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_input_keyboard" id="i|9+GjUT2Hu|)9kG:SB5">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <field name="keyboard">numbers1</field>
                                                                                                                    <next>
                                                                                                                      <block type="prop_input_max_length" id="pJ%|i$)W5=twKqBOq)zm">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="maxLength">
                                                                                                                          <block type="expression" id="Jj1`RNkLEq;~m`@2J/ktn">
                                                                                                                            <field name="value">y.toString().length</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_input_result_position" id="eQ(1I`p9Fbp|Wl1j6A~i">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <field name="resultPosition">top</field>
                                                                                                                            <next>
                                                                                                                              <block type="prop_tab_order" id="wbPUzT,s8fBZHg:c1:3m">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="tabOrder">
                                                                                                                                  <block type="expression" id="8hqU-Id/o@1x;jZgD-X4v">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_stroke" id="ChVpQN#R41E_/f3G@1e$X">
                                                                                                                                    <field name="#prop">stroke</field>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_fill" id="1x`cOnA%k{3=jKuRrD-~">
                                                                                                                                        <field name="#prop">fill</field>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style" id="%-0E;)AU3NAz;XI114SP">
                                                                                                                                            <field name="base">text</field>
                                                                                                                                            <statement name="#props">
                                                                                                                                              <block type="prop_text_style_font_size" id="HL]qwn!7?N5DPA.Zl9Q8">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="fontSize">
                                                                                                                                                  <block type="expression" id="eI`Tf6^Xx[Q^FVxhmJ$;">
                                                                                                                                                    <field name="value">36</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_text_style_fill" id="=EiA-7wA2}As|,4^G_GO">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="fill">
                                                                                                                                                      <block type="string_value" id="i{$D-{)JwU/xU_RQY5K_">
                                                                                                                                                        <field name="value">black</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_text_style_stroke" id="$qKi}[vbWzMu4FxD!pxI">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="stroke">
                                                                                                                                                          <block type="string_value" id="2pbNG@2J9,-D{wOP@21vIl">
                                                                                                                                                            <field name="value">white</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_text_style_stroke_thickness" id="GA4E#G$aEEee%@1A@2I~$u">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="strokeThickness">
                                                                                                                                                              <block type="expression" id="1(Jq+P2~}:{FPm44A~yH">
                                                                                                                                                                <field name="value">2</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="list_item_shape" id="PQ=j|VB?HGu-Gc1gma|H">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_list_align" id="gks%L.?yxriv9V@2@1ghxs">
                                                                                                        <field name="#prop"></field>
                                                                                                        <field name="align">middle</field>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <statement name="template">
                                                                                                      <block type="text_shape" id="oMVnQJcza;mNi-93.z5N">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_text_contents" id="T69M2#hx!{EJfi;F=wxh">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="contents">
                                                                                                              <block type="expression" id="w}X?{@1q=+jzM7HF3=~`S">
                                                                                                                <field name="value">' ' + list_shape[lst] + (y &gt; 1 ? 's' : '')</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style" id="/)B[o^T:|gD+;F$CM!:U">
                                                                                                                <field name="base">text</field>
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_text_style_font_size" id="x^r4(~y:ZWe_OrP|}?ta">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="fontSize">
                                                                                                                      <block type="expression" id="HS|rHjlyR{[asm5=P8;0">
                                                                                                                        <field name="value">36</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_fill" id="7ts`%Hv=7NetEqEbQGNl">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="fill">
                                                                                                                          <block type="string_value" id="Oj8Ss+[fgg[_QF{K0Yez">
                                                                                                                            <field name="value">black</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_stroke" id="q/M4=j=vZoom8ijNtmCa">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="stroke">
                                                                                                                              <block type="string_value" id="mO$+F-pnBZM8VRL#gAk4">
                                                                                                                                <field name="value">white</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_stroke_thickness" id="5l#-jGN`Cd;Qk+sM]dV/">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="strokeThickness">
                                                                                                                                  <block type="expression" id="zB=ybY-p/j(!Rp~ygoP)">
                                                                                                                                    <field name="value">2</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="text_shape" id="Y,@2[Inc:(~uLkdt$],p@1">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                                                                    <field name="value">670</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                                                                    <field name="value">345</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_anchor" id="O.v@2mKn@1K#+tF;|}C}xI">
                                                                                                    <field name="#prop">anchor</field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="+3%GU[X3l%%X`Jv1QL7@2">
                                                                                                        <field name="value">0.5</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="Kzp23HjHF7k]A-y1BJPm">
                                                                                                        <field name="value">0</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_contents" id="2+A!Rv3:3YwX+?UI8dPE">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="contents">
                                                                                                          <block type="string_value" id="|n(55mzD66@2T-Qln~2c$">
                                                                                                            <field name="value">in each group.</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style" id="{4yV#XqC:);1j:O2_oC,">
                                                                                                            <field name="base">text</field>
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_text_style_font_size" id="0!TQe])vaeLFP#%2W].z">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="fontSize">
                                                                                                                  <block type="expression" id="Y#8Th20eFkr%7nJgHyGp">
                                                                                                                    <field name="value">36</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_fill" id="rZ]a{CsgOJ73zrALnYW$">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="fill">
                                                                                                                      <block type="string_value" id="74hbueI;h)h/C@2A;@14mQ">
                                                                                                                        <field name="value">black</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_stroke" id="L$N@1{9NV8-UgD]%2zivG">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="stroke">
                                                                                                                          <block type="string_value" id="$Wt?/U!hrPeh.Lwwq:WQ">
                                                                                                                            <field name="value">white</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_stroke_thickness" id="Oe737wdI6L$Aac~@2~(z{">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="strokeThickness">
                                                                                                                              <block type="expression" id="Zy{Qz.rsZ,zFBm2FN]@2t">
                                                                                                                                <field name="value">2</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="variable" id="weElLx3.8SB.)STr377[">
                                                                                                <field name="name">loadAssets</field>
                                                                                                <value name="value">
                                                                                                  <block type="custom_image_list" id="[iQG~r:b@1r?SU7Kr3cr@2">
                                                                                                    <field name="link">${image_path}</field>
                                                                                                    <field name="images">${lst}_${it}|drop</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="statement" id="X@2bmbZYP3!^}%Lt6pPjj">
                                                                                                    <field name="value">var color = ['#e60000', '#e68a00', '#009933', '#0073e6', '#6600cc'];</field>
                                                                                                    <next>
                                                                                                      <block type="partial_explanation" id="qa[soWcmV(~JuZUE;@1$k" inline="true">
                                                                                                        <value name="value">
                                                                                                          <block type="string_value" id=":+^;U3[oydpnz1[`b;XC">
                                                                                                            <field name="value">&lt;u&gt;Step 1:&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

There are ${x} groups.&lt;/br&gt;
Name the shapes into
                                                                                                            </field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="partial_explanation" id="+4(Y73U{OqHP.y:?`=rK" inline="true">
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="NxL8g/EsjhI9v5R_c)M^">
                                                                                                                <field name="value">' '</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="variable" id="5F@1sFl~s_NZ]B1mLKZJ3">
                                                                                                                <field name="name">i</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="I#?a~?mug6w9D;g-]~pC">
                                                                                                                    <field name="value">0</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="while_do_block" id=":!Q`W([idK4Qw-K8LE^p">
                                                                                                                    <value name="while">
                                                                                                                      <block type="expression" id="8rGK$^lGFmPw]T(x9j]%">
                                                                                                                        <field name="value">i &lt; x</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <statement name="do">
                                                                                                                      <block type="partial_explanation" id="/?:8]ux^lG{NHU}+6):#" inline="true">
                                                                                                                        <value name="value">
                                                                                                                          <block type="string_value" id="nFEvkP,b;vf@2Hzz!k/vJ">
                                                                                                                            <field name="value">&lt;p style='color: ${color[i]}; display: inline-block'&gt;Group ${i+1}&lt;/p&gt;</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="if_then_else_block" id="NA��Nlco~8v-dZ@2i">
                                                                                                                            <value name="if">
                                                                                                                              <block type="expression" id="USO2PJUF_7QM5hrmI4C/">
                                                                                                                                <field name="value">i != x-1</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <statement name="then">
                                                                                                                              <block type="partial_explanation" id="W!ccC6dk@1G%.yNOqn{H@1" inline="true">
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="bGKG?#|oc@1NqG7!j#fV?">
                                                                                                                                    <field name="value">', '</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <statement name="else">
                                                                                                                              <block type="partial_explanation" id="xE!E^.z_{K=0D!G#uDqb" inline="true">
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="k9u.4d:^qFX4Lx0}u`Qj">
                                                                                                                                    <field name="value">'.'</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="variable" id="o@2Ktt8/;C^~%1!0n^@2U|">
                                                                                                                                <field name="name">i</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="]y}f7{ws9$u5p61Y_B2!">
                                                                                                                                    <field name="value">i+1</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="partial_explanation" id="Oskn%Tv~q^:_a,d$.%ZK" inline="true">
                                                                                                                        <value name="value">
                                                                                                                          <block type="string_value" id="74#t.[QS2)$QCXvr7xiU">
                                                                                                                            <field name="value">&lt;/br&gt;&lt;/br&gt;&lt;style&gt;
  table{width: 950px;}
td{width: 200px; text-align: center}
  &lt;/style&gt;

&lt;center&gt;
  &lt;table style='background: rgb(250,250,250, 0.6)'&gt;
  &lt;tr&gt;&lt;td colspan=${x}&gt;
    &lt;table&gt;
                                                                                                                            </field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="variable" id="T53n6rQm;jEDX:n7iq(S">
                                                                                                                            <field name="name">i</field>
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="bzr^3}7I=Uh1xZo[s2W^">
                                                                                                                                <field name="value">0</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="variable" id="hD?!=MGqs;$(MQ@1wj,iW">
                                                                                                                                <field name="name">count</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="FPb}0:nGg!iK?q]3AnwK">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="while_do_block" id="]/jYO(Jiey5|qjFjCZ|T">
                                                                                                                                    <value name="while">
                                                                                                                                      <block type="expression" id="ggO@2skexwMKe)0ktT[/K">
                                                                                                                                        <field name="value">i &lt; row2</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <statement name="do">
                                                                                                                                      <block type="partial_explanation" id="CLu@2TMPt.;%]1BR5,^jp" inline="true">
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="string_value" id="pC8zmG0;OCe%Qc~PE.tq">
                                                                                                                                            <field name="value">&lt;tr&gt;</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="variable" id="rxmT_CUtI.O(8c8M]}!@1">
                                                                                                                                            <field name="name">j</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id="@2H2aZ6PvY4iG}3q[P3i3">
                                                                                                                                                <field name="value">0</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="while_do_block" id="FZR?5Q!-IqB#-RpR|M;Q">
                                                                                                                                                <value name="while">
                                                                                                                                                  <block type="expression" id="O^/xC)+L11mX~B6a;(+d">
                                                                                                                                                    <field name="value">j &lt;col2</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <statement name="do">
                                                                                                                                                  <block type="if_then_block" id="g~tEs7.0rz`y%[g/Fq!#">
                                                                                                                                                    <value name="if">
                                                                                                                                                      <block type="expression" id="aumwxbd(T4ap;_E_V:LY">
                                                                                                                                                        <field name="value">count &lt; num</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <statement name="then">
                                                                                                                                                      <block type="partial_explanation" id="Q[]30beU7)U7!MQ@2v;pR" inline="true">
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="string_value" id="d;7(I|S{%Zs}OHF7TbsE">
                                                                                                                                                            <field name="value">&lt;td style='height: 80px'&gt;&lt;img src='@1sprite.src(${lst}_${it})'/&gt;&lt;sup style='color: ${color[count % x]}'&gt;${count % x + 1}&lt;/sup&gt;&lt;/td&gt;</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="variable" id="#GEwS$S@2p_r;zKa#uC^1">
                                                                                                                                                            <field name="name">count</field>
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id="2v|S)cd0)%$!zomzCP7r">
                                                                                                                                                                <field name="value">count +1</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="variable" id="T7;E#-0}UmuJnW;E7/yX">
                                                                                                                                                        <field name="name">j</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="/LGpFWUFzWOUMKY2g5=?">
                                                                                                                                                            <field name="value">j+1</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                                <next>
                                                                                                                                                  <block type="partial_explanation" id="6mMytWD0?_f{#(_[6C+1" inline="true">
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="string_value" id="}J@1:78i`$V6s}c9?B+VR">
                                                                                                                                                        <field name="value">&lt;/tr&gt;</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="variable" id="2`B?J@1zE]~@13w9:ea@27W">
                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="@2]xPqKd`XJqc+A]AhB`8">
                                                                                                                                                            <field name="value">i+1</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="partial_explanation" id="!yix#p]vqGKq@15jxorE0" inline="true">
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="string_value" id="TyrFVQl;lG(@2T;O@2fA|w">
                                                                                                                                            <field name="value">&lt;/table&gt;
&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
                                                                                                                                            </field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="variable" id="rxBdgwOQ:{PdO,8,k/Pp">
                                                                                                                                            <field name="name">i</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id="OIHGHG_)B|G0+VJ#v5Xo">
                                                                                                                                                <field name="value">0</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="while_do_block" id="-xsDN2igGO@2Aq^xgyBwU">
                                                                                                                                                <value name="while">
                                                                                                                                                  <block type="expression" id="e{d1Gkp){PQL~r(x0kwA">
                                                                                                                                                    <field name="value">i &lt; x</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <statement name="do">
                                                                                                                                                  <block type="partial_explanation" id="9xT^cXLl%av@2wsZn[Tdl" inline="true">
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="string_value" id="sEAX4,FFp+E7EWh0101c">
                                                                                                                                                        <field name="value">&lt;td style='height: 110px; background-image: url("@1sprite.src(drop)"); background-repeat: no-repeat; background-position: center; background-size: 150px 110px'&gt;&lt;/td&gt;</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="variable" id="8$Tzo`JiJQ7sYQ}a5Aiv">
                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="Av5i25B!#/@1%Zd@1Gk@2ie">
                                                                                                                                                            <field name="value">i+1</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                                <next>
                                                                                                                                                  <block type="partial_explanation" id="tC(_C8%W#H.SHV[}-" inline="true">
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="string_value" id="oXy=vh6s=%![qkn?K3N_">
                                                                                                                                                        <field name="value">&lt;/tr&gt;&lt;tr&gt;</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="variable" id="(nKE^KAZe][Z!O0=4[6d">
                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="[7R9me+ua$vc^T7+hCpe">
                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="while_do_block" id="$Bo#VQn8$k8~M_%@1:0^T">
                                                                                                                                                            <value name="while">
                                                                                                                                                              <block type="expression" id="3_Z[Y-)-7[z~9}E@1[Dm-">
                                                                                                                                                                <field name="value">i &lt; x</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <statement name="do">
                                                                                                                                                              <block type="partial_explanation" id="W=nqyP;UPA^e|5{J36Lf" inline="true">
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="string_value" id="JI0!L@2eoaK$.I|7,p/xN">
                                                                                                                                                                    <field name="value">&lt;td style='color: ${color[i]}'&gt;Group ${i+1}&lt;/td&gt;</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="variable" id="^jj`PS3_4(sv+-0!BuT/">
                                                                                                                                                                    <field name="name">i</field>
                                                                                                                                                                    <value name="value">
                                                                                                                                                                      <block type="expression" id="9Jqw#kS6:^%hcc@1lv(~p">
                                                                                                                                                                        <field name="value">i+1</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="partial_explanation" id="_RkjpLYHG.+f$T;HIi~:" inline="true">
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="string_value" id="H2^JA/U8JxLH:brIJxz8">
                                                                                                                                                                    <field name="value">&lt;/tr&gt;&lt;/table&gt;</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="end_partial_explanation" id="~jx?q|c?]W/iPsKEl3Y|">
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="partial_explanation" id="3XsV,t_v?lp@1)uU`u+AT" inline="true">
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="string_value" id="U/ju9;@1m7]na`z)X;DLk">
                                                                                                                                                                            <field name="value">&lt;u&gt;Step 2: Now, let's drag shapes into the assigned Group.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="partial_explanation" id="tw,%jehz,JiPKwWF;@2Eq" inline="true">
                                                                                                                                                                            <value name="value">
                                                                                                                                                                              <block type="string_value" id="}T+=dzjdDYknX]=;0nil">
                                                                                                                                                                                <field name="value">&lt;style&gt;
  table{width: 950px;}
td{width: 200px; text-align: center}
img{padding: 2px}
  &lt;/style&gt;

&lt;center&gt;
  &lt;table style='background: rgb(250,250,250, 0.6)'&gt;
                                                                                                                                                                                </field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="partial_explanation" id="|qgXAwor?6Qv%T}{-%~-" inline="true">
                                                                                                                                                                                <value name="value">
                                                                                                                                                                                  <block type="string_value" id=")RUlSa0Cc+@1_Z--^14}_">
                                                                                                                                                                                    <field name="value">&lt;tr&gt;</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="variable" id="cswn-io}/@1^$qwE;+;XD">
                                                                                                                                                                                    <field name="name">i</field>
                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                      <block type="expression" id="+d..s)w8.IZ-(])5C~=T">
                                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="while_do_block" id="rOgDjM6lGp:hCsdTwA=b">
                                                                                                                                                                                        <value name="while">
                                                                                                                                                                                          <block type="expression" id="-muVAk3bZ65+aBQ.6+Z9">
                                                                                                                                                                                            <field name="value">i &lt; x</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <statement name="do">
                                                                                                                                                                                          <block type="partial_explanation" id="Uh1O@1x-9V);j_kUb@2h(Z" inline="true">
                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                              <block type="string_value" id="a|h-Gks#+;+/op7INrM~">
                                                                                                                                                                                                <field name="value">&lt;td style='height: 120px; background-image: url("@1sprite.src(drop)"); background-repeat: no-repeat; background-position: center; background-size: 170px 120px'&gt;</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="variable" id="%EIN~0;x93gr5Y-xY.tf">
                                                                                                                                                                                                <field name="name">j</field>
                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                  <block type="expression" id="Lsdv1oi=xFN(`$I0wcle">
                                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="while_do_block" id="9VKcHI@2D%=7QiI;sjofv">
                                                                                                                                                                                                    <value name="while">
                                                                                                                                                                                                      <block type="expression" id="-;|aQ/k@1$HUVd@2pGO]`6">
                                                                                                                                                                                                        <field name="value">j &lt; y</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <statement name="do">
                                                                                                                                                                                                      <block type="partial_explanation" id="U..e)?W)6n5uc46uP_GV" inline="true">
                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                          <block type="string_value" id="k8TS(qU+;)XTa.-E(KqH">
                                                                                                                                                                                                            <field name="value">&lt;img src='@1sprite.src(${lst}_${it})'/&gt;&lt;sup style='color: ${color[i]}'&gt;${i+1}&lt;/sup&gt;</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="if_then_block" id="/==|!HzmW%=9R87/evUT">
                                                                                                                                                                                                            <value name="if">
                                                                                                                                                                                                              <block type="expression" id="C[#IuZ~YXIPC}aDWH~Gi">
                                                                                                                                                                                                                <field name="value">j % 3 == 2</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                            <statement name="then">
                                                                                                                                                                                                              <block type="partial_explanation" id="U#lrzTr,^Z;H/.4Xn}81" inline="true">
                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                  <block type="string_value" id="Ki2[dR={}TrZIvp%MRtl">
                                                                                                                                                                                                                    <field name="value">&lt;/br&gt;</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                            <next>
                                                                                                                                                                                                              <block type="variable" id="@2P#62N7`g6y3@2s$g[.is">
                                                                                                                                                                                                                <field name="name">j</field>
                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                  <block type="expression" id="4g]+)daW-O;`NQO,XMjr">
                                                                                                                                                                                                                    <field name="value">j+1</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </next>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="partial_explanation" id="@2!s44Zs9cOp,qwsV3;dk" inline="true">
                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                          <block type="string_value" id="AChM]KGdPQn])E5!b:{C">
                                                                                                                                                                                                            <field name="value">&lt;/td&gt;</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="variable" id="F/cPL^a4;Md3IMh/Fi)P">
                                                                                                                                                                                                            <field name="name">i</field>
                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                              <block type="expression" id="hFJxmOqb-#8EuF7F?f8s">
                                                                                                                                                                                                                <field name="value">i+1</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </statement>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="partial_explanation" id="6cIb|.@1^gS5XCMg򤋢 inline=">
                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                              <block type="string_value" id="4Ca7Jy4#qKRcC7Bn4Cev">
                                                                                                                                                                                                <field name="value">&lt;/tr&gt;&lt;tr&gt;</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="variable" id="ld?(20[haObY6E?@2asl@1">
                                                                                                                                                                                                <field name="name">i</field>
                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                  <block type="expression" id="w+ve?8C/~VP4^EW2j(!k">
                                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="while_do_block" id="H(@2Sp5DsHROiBusRcqOj">
                                                                                                                                                                                                    <value name="while">
                                                                                                                                                                                                      <block type="expression" id="DogOW$MbvhP/4y.^Jel^">
                                                                                                                                                                                                        <field name="value">i &lt; x</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <statement name="do">
                                                                                                                                                                                                      <block type="partial_explanation" id="|}03m4J%@2v:J34;}Wf))" inline="true">
                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                          <block type="string_value" id="yS!J#^#[u0zj+@2O@2c45Y">
                                                                                                                                                                                                            <field name="value">&lt;td style='color: ${color[i]}'&gt;Group ${i+1}&lt;/td&gt;</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="variable" id="rQ+vQi^L_:wgq2INs{W)">
                                                                                                                                                                                                            <field name="name">i</field>
                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                              <block type="expression" id="|+C69)^`T]Z0,#XZWT3Z">
                                                                                                                                                                                                                <field name="value">i+1</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="partial_explanation" id="8YHl1iDMhr_q)kJ4{Q`N" inline="true">
                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                          <block type="string_value" id="!,c_L0@1AO[m84SBOGQGt">
                                                                                                                                                                                                            <field name="value">&lt;/tr&gt;&lt;/table&gt;</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="end_partial_explanation" id="qOB]^TQ^O7C@1wG%^WlZe">
                                                                                                                                                                                                            <next>
                                                                                                                                                                                                              <block type="partial_explanation" id="O.=[QlnDw3Yl^(%~^01s" inline="true">
                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                  <block type="string_value" id="(:whN6=RCh9vO3FVE,iO">
                                                                                                                                                                                                                    <field name="value">&lt;u&gt;Step 3: Finally, count the number of shapes in one group.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
  td{text-align: center}
  img {padding: 2px}
  &lt;/style&gt;
&lt;center&gt;
  &lt;table&gt;
  &lt;tr&gt;
  &lt;td style='background-image: url("@1sprite.src(drop)"); background-size: 200px 150px ; width: 200px; height: 150px'&gt;
                                                                                                                                                                                                                    </field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                  <block type="variable" id=":7A-M]g@1fJq7%M7qNeE5">
                                                                                                                                                                                                                    <field name="name">i</field>
                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                      <block type="expression" id="}@1IVFu#E:c|pBd|I?,)G">
                                                                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                      <block type="while_do_block" id="u]cH;B{Oy%?_f:f}M}Q!">
                                                                                                                                                                                                                        <value name="while">
                                                                                                                                                                                                                          <block type="expression" id="|YhSvSnIdo?g)A,@2Y(6?">
                                                                                                                                                                                                                            <field name="value">i &lt; y</field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                        <statement name="do">
                                                                                                                                                                                                                          <block type="partial_explanation" id="#9Z}C=6:UNi{peMn]B#S" inline="true">
                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                              <block type="string_value" id="FTx4OoTwd=`=|8b_b(J2">
                                                                                                                                                                                                                                <field name="value">&lt;img src='@1sprite.src(${lst}_${it})'/&gt;&lt;sup style=''&gt;${i+1}&lt;/sup&gt;</field>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                              <block type="if_then_block" id="^Z9W.Gl^h``49(GJb,vn">
                                                                                                                                                                                                                                <value name="if">
                                                                                                                                                                                                                                  <block type="expression" id="Y@2]@1=g]tk+}dP}9ELr#7">
                                                                                                                                                                                                                                    <field name="value">i % 3 == 2</field>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                <statement name="then">
                                                                                                                                                                                                                                  <block type="partial_explanation" id="H,VY3^TV;;}ht)@1+b|f)" inline="true">
                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                      <block type="string_value" id="K!0f@1l-{F.BIAJj:/`+F">
                                                                                                                                                                                                                                        <field name="value">&lt;/br&gt;</field>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                  <block type="variable" id="fll6Y]=nO`x+gnNWX=LN">
                                                                                                                                                                                                                                    <field name="name">i</field>
                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                      <block type="expression" id="GxUQ+@2km:?@1S1^5]mz?,">
                                                                                                                                                                                                                                        <field name="value">i+1</field>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                          <block type="partial_explanation" id="[r=3!M@1hK+{i$[dQ{%py" inline="true">
                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                              <block type="string_value" id="o@2~O4-#.%2X|lAzo1uqc">
                                                                                                                                                                                                                                <field name="value">&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/center&gt;
&lt;/br&gt;
There are &lt;b&gt;${y}&lt;/b&gt; ${list_shape[lst]}${y &gt; 1 ? 's':''} in each group.
                                                                                                                                                                                                                                </field>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                              <block type="end_partial_explanation" id="|_|qJBWz()k+Kh$6)zfw"></block>
                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </next>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </next>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
  <block type="partial_explanation" id="_945f=drVf@29UMn.vp,$" inline="true" x="676" y="6710">
    <value name="value">
      <block type="string_value" id="fYkLodWg=PZBx85=DLv}">
        <field name="value">&lt;td style='height: 90px; background-image: url("@1sprite.src(drop)"); background-repeat: no-repeat; background-position: center;'&gt;&lt;/td&gt;</field>
      </block>
    </value>
  </block>
</xml>
END_XML]] */
