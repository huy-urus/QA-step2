
module.exports = [
  {
    "#type": "question",
    "name": "Y1.NA.FD.RECHALFCOL.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y1.NA.FD.RECHALFCOL.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "variable",
        "name": "list_item",
        "value": {
          "#type": "string_array",
          "items": "space|cups|planes|trains|shoes|bowls|dolls"
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "range < 4"
        },
        "then": [
          {
            "#type": "variable",
            "name": "num",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "2"
              },
              "max": {
                "#type": "expression",
                "value": "8"
              }
            }
          }
        ],
        "else": [
          {
            "#type": "variable",
            "name": "num",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "9"
              },
              "max": {
                "#type": "expression",
                "value": "20"
              }
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "total",
        "value": {
          "#type": "expression",
          "value": "num*2"
        }
      },
      {
        "#type": "statement",
        "value": "function check(x, A) {\n    for(var m=0; m<A.length; m++){\n        if(x == A[m]){\n            return 1;\n        }\n    }\n    return 0;\n}\n\nvar row, col;\nif(total < 6){row = 1; col = total;\n             if(total == 4){row = 2; col = 2;}}\nelse if(total < 11) {row = 2; col = 5;\n                    if(total == 6){row = 2; col = 3;}\n                    if(total == 8){row = 2; col = 4;}}\nelse if(total < 16) {row = 3; col = 5;\n                    if(total == 12){row = 3; col = 4;}\n                    if(total == 14){row = 2; col = 7;}\n                    if(total == 15){row = 3; col = 5;}}\nelse if(total < 21) {row = 4; col = 5;\n                    if(total == 18){row = 3; col = 6;}\n                    if(total == 16){row = 2; col = 8;}}\nelse if(total < 31) {row = 3; col = 10;\n                    if(total == 24){row = 4; col = 6;}\n                    if(total == 28){row = 4; col = 7;}}\nelse {row = 4; col = 10;}"
      },
      {
        "#type": "do_while_block",
        "do": [
          {
            "#type": "variable",
            "name": "pick",
            "value": {
              "#type": "random_many",
              "count": {
                "#type": "expression",
                "value": "3"
              },
              "items": {
                "#type": "func",
                "name": "arrayOfNumber",
                "args": [
                  {
                    "#type": "expression",
                    "value": "total"
                  },
                  {
                    "#type": "expression",
                    "value": "-num"
                  }
                ]
              }
            }
          }
        ],
        "while": {
          "#type": "expression",
          "value": "check(0, pick) == 1"
        }
      },
      {
        "#type": "variable",
        "name": "number",
        "value": {
          "#type": "expression",
          "value": "[num, num+pick[0], num+pick[1], num+pick[2]]"
        }
      },
      {
        "#type": "variable",
        "name": "number",
        "value": {
          "#type": "func",
          "name": "shuffle",
          "args": [
            {
              "#type": "expression",
              "value": "number"
            }
          ]
        }
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "10"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "Which one is half coloured?"
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "36"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "item",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "6"
          }
        }
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "2"
            },
            "cols": {
              "#type": "expression",
              "value": "2"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "250"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "750"
            },
            "height": {
              "#type": "expression",
              "value": "380"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "expression",
              "value": "[0, 1, 2, 3]"
            }
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_cell_template",
            "variable": "$cell",
            "#prop": "cell.template",
            "#callback": "$cell",
            "body": [
              {
                "#type": "variable",
                "name": "data",
                "value": {
                  "#type": "expression",
                  "value": "$cell.data"
                }
              },
              {
                "#type": "choice_custom_shape",
                "action": "click-one",
                "value": {
                  "#type": "expression",
                  "value": "number[data]"
                },
                "template": {
                  "#callback": "$choice",
                  "variable": "$choice",
                  "body": [
                    {
                      "#type": "image_shape",
                      "#props": [
                        {
                          "#type": "prop_position",
                          "#prop": "",
                          "x": {
                            "#type": "expression",
                            "value": "$cell.centerX"
                          },
                          "y": {
                            "#type": "expression",
                            "value": "$cell.centerY"
                          }
                        },
                        {
                          "#type": "prop_size",
                          "#prop": "",
                          "width": {
                            "#type": "expression",
                            "value": "350"
                          },
                          "height": {
                            "#type": "expression",
                            "value": "170"
                          }
                        },
                        {
                          "#type": "prop_image_key",
                          "#prop": "",
                          "key": "big.png"
                        },
                        {
                          "#type": "prop_image_src",
                          "#prop": "",
                          "src": "${image_path}big.png"
                        }
                      ]
                    },
                    {
                      "#type": "grid_shape",
                      "#props": [
                        {
                          "#type": "prop_grid_dimension",
                          "#prop": "",
                          "rows": {
                            "#type": "expression",
                            "value": "row"
                          },
                          "cols": {
                            "#type": "expression",
                            "value": "col"
                          }
                        },
                        {
                          "#type": "prop_position",
                          "#prop": "",
                          "x": {
                            "#type": "expression",
                            "value": "$cell.centerX"
                          },
                          "y": {
                            "#type": "expression",
                            "value": "$cell.centerY"
                          }
                        },
                        {
                          "#type": "prop_size",
                          "#prop": "",
                          "width": {
                            "#type": "expression",
                            "value": "320"
                          },
                          "height": {
                            "#type": "expression",
                            "value": "150"
                          }
                        },
                        {
                          "#type": "prop_anchor",
                          "#prop": "anchor",
                          "x": {
                            "#type": "expression",
                            "value": "0.5"
                          },
                          "y": {
                            "#type": "expression",
                            "value": "0.5"
                          }
                        },
                        {
                          "#type": "prop_grid_cell_source",
                          "#prop": "cell.source",
                          "value": {
                            "#type": "func",
                            "name": "arrayOfNumber",
                            "args": [
                              {
                                "#type": "expression",
                                "value": "total"
                              },
                              {
                                "#type": "expression",
                                "value": "0"
                              }
                            ]
                          }
                        },
                        {
                          "#type": "prop_grid_random",
                          "#prop": "",
                          "random": false
                        },
                        {
                          "#type": "prop_grid_show_borders",
                          "#prop": "#showBorders",
                          "value": false
                        },
                        {
                          "#type": "prop_grid_cell_template_for",
                          "variable": "$cell",
                          "condition": "$cell.data < number[data]",
                          "#prop": "cell.templates[]",
                          "#callback": "$cell",
                          "body": [
                            {
                              "#type": "image_shape",
                              "#props": [
                                {
                                  "#type": "prop_position",
                                  "#prop": "",
                                  "x": {
                                    "#type": "expression",
                                    "value": "$cell.centerX"
                                  },
                                  "y": {
                                    "#type": "expression",
                                    "value": "$cell.centerY"
                                  }
                                },
                                {
                                  "#type": "prop_max_size",
                                  "#prop": "",
                                  "maxWidth": {
                                    "#type": "expression",
                                    "value": "$cell.width - 5"
                                  },
                                  "maxHeight": {
                                    "#type": "expression",
                                    "value": "$cell.height - 5"
                                  }
                                },
                                {
                                  "#type": "prop_image_key",
                                  "#prop": "",
                                  "key": "${item}_1.png"
                                },
                                {
                                  "#type": "prop_image_src",
                                  "#prop": "",
                                  "src": "${image_path}${item}_1.png"
                                }
                              ]
                            }
                          ]
                        },
                        {
                          "#type": "prop_grid_cell_template_for",
                          "variable": "$cell",
                          "condition": "$cell.data >= number[data]",
                          "#prop": "cell.templates[]",
                          "#callback": "$cell",
                          "body": [
                            {
                              "#type": "image_shape",
                              "#props": [
                                {
                                  "#type": "prop_position",
                                  "#prop": "",
                                  "x": {
                                    "#type": "expression",
                                    "value": "$cell.centerX"
                                  },
                                  "y": {
                                    "#type": "expression",
                                    "value": "$cell.centerY"
                                  }
                                },
                                {
                                  "#type": "prop_max_size",
                                  "#prop": "",
                                  "maxWidth": {
                                    "#type": "expression",
                                    "value": "$cell.width - 5"
                                  },
                                  "maxHeight": {
                                    "#type": "expression",
                                    "value": "$cell.height - 5"
                                  }
                                },
                                {
                                  "#type": "prop_image_key",
                                  "#prop": "",
                                  "key": "${item}_0.png"
                                },
                                {
                                  "#type": "prop_image_src",
                                  "#prop": "",
                                  "src": "${image_path}${item}_0.png"
                                }
                              ]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "func",
        "name": "addAnswers",
        "args": [
          {
            "#type": "expression",
            "value": "num"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "loadAssets",
        "value": {
          "#type": "custom_image_list",
          "link": "${image_path}",
          "images": "${item}_0|${item}_1|big"
        }
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 1: Count the number of ${list_item[item]}.</u>\n</br></br>\n<style>\n   table{border: 1px dashed red; border-radius: 20px; background: rgb(150, 200, 250, 0.3); margin: 20px; display: inline-block}\n  td{text-align: center; padding: 5px;}\nimg{max-width: 60px; max-height: 60px}\n  </style>\n<center>\n<table>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "variable",
        "name": "count",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < row"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<tr>"
            ]
          },
          {
            "#type": "variable",
            "name": "j",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "j < col"
            },
            "do": [
              {
                "#type": "if_then_else_block",
                "if": {
                  "#type": "expression",
                  "value": "count < total"
                },
                "then": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<td><img src='@sprite.src(${item}_0)'/><sup style='font-size: 24px; color: blue'>${count+1}</sup></td>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "count",
                    "value": {
                      "#type": "expression",
                      "value": "count+1"
                    }
                  }
                ],
                "else": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<td></td>"
                    ]
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "j",
                "value": {
                  "#type": "expression",
                  "value": "j+1"
                }
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</tr>"
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</table></center></br>\n<span style='background: rgb(250, 250, 250, 0.5)'>There are <b>${total}</b> ${list_item[item]} in total.</span>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 2: Divide the number of total parts into 2.</u>\n</br>\n  </br>\n${total} &#247; ${2} = ${num}</br>\nShade ${num} ${list_item[item]}.\n</br></br>\n\n<style>\n   table{border: 1px dashed red; border-radius: 20px; background: rgb(150, 200, 250, 0.3); margin: 20px; display: inline-block}\n  td{text-align: center; padding: 5px;}\nimg{max-width: 60px; max-height: 60px}\n  </style>\n<center>\n<table>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "variable",
        "name": "count",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < row"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<tr>"
            ]
          },
          {
            "#type": "variable",
            "name": "j",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "j < col"
            },
            "do": [
              {
                "#type": "if_then_else_block",
                "if": {
                  "#type": "expression",
                  "value": "count < total"
                },
                "then": [
                  {
                    "#type": "if_then_else_block",
                    "if": {
                      "#type": "expression",
                      "value": "count < num"
                    },
                    "then": [
                      {
                        "#type": "func",
                        "name": "addPartialExplanation",
                        "args": [
                          "<td><img src='@sprite.src(${item}_1)'/><sup style='font-size: 24px; color: blue'>${count+1}</sup></td>"
                        ]
                      }
                    ],
                    "else": [
                      {
                        "#type": "func",
                        "name": "addPartialExplanation",
                        "args": [
                          "<td><img src='@sprite.src(${item}_0)'/></td>"
                        ]
                      }
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "count",
                    "value": {
                      "#type": "expression",
                      "value": "count+1"
                    }
                  }
                ],
                "else": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<td></td>"
                    ]
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "j",
                "value": {
                  "#type": "expression",
                  "value": "j+1"
                }
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</tr>"
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</table></center>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      },
      {
        "#type": "variable",
        "name": "border",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < 4"
        },
        "do": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "number[i] == num"
            },
            "then": [
              {
                "#type": "variable",
                "name": "border[i]",
                "value": {
                  "#type": "expression",
                  "value": "'5px solid green'"
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "border[i]",
                "value": {
                  "#type": "expression",
                  "value": "'1px dashed red'"
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 3: Which one looks like it?</u>\n</br></br>"
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<style>\n  table{border-radius: 20px; background: rgb(150, 200, 250, 0.3); margin: 10px; display: inline-block}\n  td{text-align: center; padding: 5px;}\nimg{max-width: 60px; max-height: 60px; padding: 5px}\n  </style>\n<center>"
        ]
      },
      {
        "#type": "variable",
        "name": "z",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "z < 4"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<table style='border: ${border[z]}'>"
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "variable",
            "name": "count",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < row"
            },
            "do": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<tr>"
                ]
              },
              {
                "#type": "variable",
                "name": "j",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "j < col"
                },
                "do": [
                  {
                    "#type": "if_then_else_block",
                    "if": {
                      "#type": "expression",
                      "value": "count < total"
                    },
                    "then": [
                      {
                        "#type": "if_then_else_block",
                        "if": {
                          "#type": "expression",
                          "value": "count < number[z]"
                        },
                        "then": [
                          {
                            "#type": "func",
                            "name": "addPartialExplanation",
                            "args": [
                              "<td><img src='@sprite.src(${item}_1)'/></td>"
                            ]
                          }
                        ],
                        "else": [
                          {
                            "#type": "func",
                            "name": "addPartialExplanation",
                            "args": [
                              "<td><img src='@sprite.src(${item}_0)'/></td>"
                            ]
                          }
                        ]
                      },
                      {
                        "#type": "variable",
                        "name": "count",
                        "value": {
                          "#type": "expression",
                          "value": "count+1"
                        }
                      }
                    ],
                    "else": [
                      {
                        "#type": "func",
                        "name": "addPartialExplanation",
                        "args": [
                          "<td></td>"
                        ]
                      }
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "j",
                    "value": {
                      "#type": "expression",
                      "value": "j+1"
                    }
                  }
                ]
              },
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</tr>"
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</table>"
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "z == 1"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</br>"
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "z",
            "value": {
              "#type": "expression",
              "value": "z+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</table></center>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="0" y="0">
    <field name="name">Y1.NA.FD.RECHALFCOL.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y1.NA.FD.RECHALFCOL.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="YepMQT@2uwp2rRPvmnRg1">
                                    <field name="name">range</field>
                                    <value name="value">
                                      <block type="expression" id="UxgQ^Y5]rb}vpw[D=l-!">
                                        <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="variable" id="k@1L@2Z=Y@2n:SJB:^4h4jX">
                                        <field name="name">list_item</field>
                                        <value name="value">
                                          <block type="string_array" id="Y{@16nEt7zL[u}u74cz$c">
                                            <field name="items">space|cups|planes|trains|shoes|bowls|dolls</field>
                                          </block>
                                        </value>
                                        <next>
                                          <block type="if_then_else_block" id="@1O9A@2Cu)~.)brD-@2,3!g">
                                            <value name="if">
                                              <block type="expression" id="m)sT3Lk)~-PT_n-)lCUd">
                                                <field name="value">range &lt; 4</field>
                                              </block>
                                            </value>
                                            <statement name="then">
                                              <block type="variable" id=";G:%)Q|)x$)^43+77gg6">
                                                <field name="name">num</field>
                                                <value name="value">
                                                  <block type="random_number" id="Z#y1aGq3aWVW?TxZNX{N">
                                                    <value name="min">
                                                      <block type="expression" id=",1CZ=0ACf5/%lpm~68%2">
                                                        <field name="value">2</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="Cd[pZb;:E-/itJR:W7]Z">
                                                        <field name="value">8</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                              </block>
                                            </statement>
                                            <statement name="else">
                                              <block type="variable" id="vn$UY)Cr39REBUfeYv^s">
                                                <field name="name">num</field>
                                                <value name="value">
                                                  <block type="random_number" id="uI{:#C0cKp2{0-W0#mA~">
                                                    <value name="min">
                                                      <block type="expression" id="6l_N$Z-2vt[pvh+?Ia`8">
                                                        <field name="value">9</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="H]/!,pm3.2Zs-g|;A`I{">
                                                        <field name="value">20</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                              </block>
                                            </statement>
                                            <next>
                                              <block type="variable" id="P-6oQC9YThO6oub~Zs7H">
                                                <field name="name">total</field>
                                                <value name="value">
                                                  <block type="expression" id="ie-6N34u~zp:4q:T9^[E">
                                                    <field name="value">num@22</field>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="statement" id="n}8{-rSm;e%0:e~}bLU2">
                                                    <field name="value">function check(x, A) {
    for(var m=0; m&lt;A.length; m++){
        if(x == A[m]){
            return 1;
        }
    }
    return 0;
}

var row, col;
if(total &lt; 6){row = 1; col = total;
             if(total == 4){row = 2; col = 2;}}
else if(total &lt; 11) {row = 2; col = 5;
                    if(total == 6){row = 2; col = 3;}
                    if(total == 8){row = 2; col = 4;}}
else if(total &lt; 16) {row = 3; col = 5;
                    if(total == 12){row = 3; col = 4;}
                    if(total == 14){row = 2; col = 7;}
                    if(total == 15){row = 3; col = 5;}}
else if(total &lt; 21) {row = 4; col = 5;
                    if(total == 18){row = 3; col = 6;}
                    if(total == 16){row = 2; col = 8;}}
else if(total &lt; 31) {row = 3; col = 10;
                    if(total == 24){row = 4; col = 6;}
                    if(total == 28){row = 4; col = 7;}}
else {row = 4; col = 10;}
                                                    </field>
                                                    <next>
                                                      <block type="do_while_block" id="rOuyx4xZOW2p(k}~fzG_">
                                                        <statement name="do">
                                                          <block type="variable" id="Ri@2:F=/7;Z=L!7xT@1pPA">
                                                            <field name="name">pick</field>
                                                            <value name="value">
                                                              <block type="random_many" id="Y.|(:cT4$ez#;3zxkgG`">
                                                                <value name="count">
                                                                  <block type="expression" id="cpX@1D8.:.$~N[U#S~Q4+">
                                                                    <field name="value">3</field>
                                                                  </block>
                                                                </value>
                                                                <value name="items">
                                                                  <block type="func_array_of_number" id="o00oJe;xp}|IdoSHZTH(" inline="true">
                                                                    <value name="items">
                                                                      <block type="expression" id="E^l?S15|cOS!~|!y3o+:">
                                                                        <field name="value">total</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="from">
                                                                      <block type="expression" id="7QH)1n(ypUl3z@1IlkY|`">
                                                                        <field name="value">-num</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </statement>
                                                        <value name="while">
                                                          <block type="expression" id=")(RM(H:LXT7]`dFePtmu">
                                                            <field name="value">check(0, pick) == 1</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="]Q@16I|LbjUgf/w_5ml,U">
                                                            <field name="name">number</field>
                                                            <value name="value">
                                                              <block type="expression" id="m,v0]jG^t]rr}]am(a?G">
                                                                <field name="value">[num, num+pick[0], num+pick[1], num+pick[2]]</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="variable" id="tqS0We_kbWk)8E!0{2SA">
                                                                <field name="name">number</field>
                                                                <value name="value">
                                                                  <block type="func_shuffle" id="_S4l4(UPbk4$Hhm01FC;" inline="true">
                                                                    <value name="value">
                                                                      <block type="expression" id="B7+v^u{WC|~PC_Ul+tRD">
                                                                        <field name="value">number</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                                    <statement name="#props">
                                                                      <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                                        <field name="#prop"></field>
                                                                        <value name="x">
                                                                          <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                                            <field name="value">400</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                                            <field name="value">10</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_anchor" id="O.v@2mKn@1K#+tF;|}C}xI">
                                                                            <field name="#prop">anchor</field>
                                                                            <value name="x">
                                                                              <block type="expression" id="+3%GU[X3l%%X`Jv1QL7@2">
                                                                                <field name="value">0.5</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="y">
                                                                              <block type="expression" id="Kzp23HjHF7k]A-y1BJPm">
                                                                                <field name="value">0</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                                                <field name="#prop"></field>
                                                                                <value name="contents">
                                                                                  <block type="string_value" id="#6btV9p,PGV}wMx.U�">
                                                                                    <field name="value">Which one is half coloured?</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                                    <field name="base">text</field>
                                                                                    <statement name="#props">
                                                                                      <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="fontSize">
                                                                                          <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                                            <field name="value">36</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="fill">
                                                                                              <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                                                <field name="value">black</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="stroke">
                                                                                                  <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                                    <field name="value">white</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="strokeThickness">
                                                                                                      <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                                        <field name="value">2</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="variable" id="y{MIb(Clym2HbZF-})CA">
                                                                        <field name="name">item</field>
                                                                        <value name="value">
                                                                          <block type="random_number" id="E1qX!8MWU:.Yej-}]dBR">
                                                                            <value name="min">
                                                                              <block type="expression" id="b_,NHv)gA@2zbL@2)7DStk">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="max">
                                                                              <block type="expression" id="G7Z`F$D;WB`I2e2(HKBS">
                                                                                <field name="value">6</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="grid_shape" id="H_F,K72P,I:]fm6JKg0y">
                                                                            <statement name="#props">
                                                                              <block type="prop_grid_dimension" id="j4t7t;Wp0/al51}_iXjU">
                                                                                <field name="#prop"></field>
                                                                                <value name="rows">
                                                                                  <block type="expression" id="E=5ijJF?B6uQ@1lKkDY:B">
                                                                                    <field name="value">2</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="cols">
                                                                                  <block type="expression" id="IDvcs3G{]~u.Yh+~pyZ3">
                                                                                    <field name="value">2</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_position" id="i5Cv5f~_][FvRh!BD+s:">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="nfi_3cH=p96`gW0v;?">
                                                                                        <field name="value">400</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="|PM?3L?b~Gr`tZ`fvbbK">
                                                                                        <field name="value">250</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_size" id="%UF`yP%}@1=l}+=^3(]vV">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="width">
                                                                                          <block type="expression" id="B@2lAR2Twj-_lHI,`|0Tu">
                                                                                            <field name="value">750</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="height">
                                                                                          <block type="expression" id="GW3sd5BQTV1Y3p2i:%ks">
                                                                                            <field name="value">380</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_anchor" id="27JKa/C3+%nhb]}c(z7{">
                                                                                            <field name="#prop">anchor</field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="/@1p.745UsOmYXyI1^NQ+">
                                                                                                <field name="value">0.5</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="5dNia5t_D)cY@16wzW@2YV">
                                                                                                <field name="value">0.5</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_grid_cell_source" id="=g!#a93Ns2yHu~J$3uII">
                                                                                                <field name="#prop">cell.source</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="~j@1,^liddf}oDg}E91^i">
                                                                                                    <field name="value">[0, 1, 2, 3]</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_grid_random" id="UC^@1B%oh^wzu$qM_1~2{">
                                                                                                    <field name="#prop"></field>
                                                                                                    <field name="random">FALSE</field>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_show_borders" id="WBiHb8m@124`Bdk8xdiZn">
                                                                                                        <field name="#prop">#showBorders</field>
                                                                                                        <field name="value">FALSE</field>
                                                                                                        <next>
                                                                                                          <block type="prop_grid_cell_template" id="7^6]Kv:2yrZi`cooAW2@2">
                                                                                                            <field name="variable">$cell</field>
                                                                                                            <field name="#prop">cell.template</field>
                                                                                                            <field name="#callback">$cell</field>
                                                                                                            <statement name="body">
                                                                                                              <block type="variable" id="mj@2YpM;:)|Kq8:362.Hf">
                                                                                                                <field name="name">data</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="Ghs])+fF8`zo+70ztTwG">
                                                                                                                    <field name="value">$cell.data</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="choice_custom_shape" id="BNSDrePw~y6p|]Fm=F75">
                                                                                                                    <field name="action">click-one</field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="Yzq)g@1gr3ULJM{^4[{jg">
                                                                                                                        <field name="value">number[data]</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <statement name="template">
                                                                                                                      <block type="image_shape" id="s-R.QkWb?{,(Nvf6CXyE">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="8bpC8-.]BW[.XF1n.Sm#">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="v^~jO+Z6Dp$|kfI_9D43">
                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="M+5VTYd`!Io6WbIf|;qX">
                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_size" id="RDQVkfYR`h^_-@1I6AZQ_">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="width">
                                                                                                                                  <block type="expression" id="v$XN5;;8:lFSN@2yPN$e)">
                                                                                                                                    <field name="value">350</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="height">
                                                                                                                                  <block type="expression" id="1qLR{Cn~OY9_vWO_X|hs">
                                                                                                                                    <field name="value">170</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_key" id="Y@1!-ZBlQ|K[-;DBbwEX3">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="key">
                                                                                                                                      <block type="string_value" id="CmswmV!i$0@1;GYBPD}tP">
                                                                                                                                        <field name="value">big.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_src" id="t%hxh?L3H6N#v%8.M_hz">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="src">
                                                                                                                                          <block type="string_value" id=")_Z@1wo:~KZ-^|J]iiD3.">
                                                                                                                                            <field name="value">${image_path}big.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="grid_shape" id="6tU?K^N8ec~A@1@1NwQ72N">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_grid_dimension" id="X4K:|9dKK%s9l4n5v@2lG">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="rows">
                                                                                                                                  <block type="expression" id="[Ev!.bxnzjc|-33v.%7=">
                                                                                                                                    <field name="value">row</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="cols">
                                                                                                                                  <block type="expression" id="u$7]/V/!ZWM4(Va??@2({">
                                                                                                                                    <field name="value">col</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_position" id="zLld84TfBg#!88]1,JD-">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="x">
                                                                                                                                      <block type="expression" id="Cd6g{Y_P?:GyVyvQi1wJ">
                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="y">
                                                                                                                                      <block type="expression" id=".F}E71+@1i)77!yQ`hmfu">
                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_size" id="+o2CWr^.ohx)_bcIG53]">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="width">
                                                                                                                                          <block type="expression" id="Z[x5iHG%KWC%KWQb}c-G">
                                                                                                                                            <field name="value">320</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="height">
                                                                                                                                          <block type="expression" id="@2E|Ob@1jF#gYtX8-%Iui@1">
                                                                                                                                            <field name="value">150</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_anchor" id="1WJjXY^.h,t]eNgxaJ}q">
                                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id="DHx[vqs?.?nUD:AYPJWd">
                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="e3D|RR7w!=Sufu):zV`c">
                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_grid_cell_source" id="v8+Yu211Qg8fX9Mu4th`">
                                                                                                                                                <field name="#prop">cell.source</field>
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="func_array_of_number" id="fitxQX.Tk2Q@2byr-v6==" inline="true">
                                                                                                                                                    <value name="items">
                                                                                                                                                      <block type="expression" id="Kc{lJ}ok79qAhq{@2SS$)">
                                                                                                                                                        <field name="value">total</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="from">
                                                                                                                                                      <block type="expression" id="9=JV]}11ZhNt!9ki+k">
                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_grid_random" id="}?kS#x[b)9OGb@1`UdJGV">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <field name="random">FALSE</field>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_grid_show_borders" id=";y^wk4y%[VWjXOj~vrQZ">
                                                                                                                                                        <field name="#prop">#showBorders</field>
                                                                                                                                                        <field name="value">FALSE</field>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_grid_cell_template_for" id="a$#JSe6h@1;=@2L=ViQTi:">
                                                                                                                                                            <field name="variable">$cell</field>
                                                                                                                                                            <field name="condition">$cell.data &lt; number[data]</field>
                                                                                                                                                            <field name="#prop">cell.templates[]</field>
                                                                                                                                                            <field name="#callback">$cell</field>
                                                                                                                                                            <statement name="body">
                                                                                                                                                              <block type="image_shape" id="(.[$Y.?HQu22$#%,(+h7">
                                                                                                                                                                <statement name="#props">
                                                                                                                                                                  <block type="prop_position" id="3HlAtlql1weve,ElpzQa">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="x">
                                                                                                                                                                      <block type="expression" id="0_yk7Vci9,}g;rk;.zq;">
                                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <value name="y">
                                                                                                                                                                      <block type="expression" id="@1Y++CD.s+K,}z%5=~m8`">
                                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_max_size" id="|IUltK0K83fo~I8x52Tr">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="maxWidth">
                                                                                                                                                                          <block type="expression" id="fQ!J-@1lUa13@2ZmvT50KX">
                                                                                                                                                                            <field name="value">$cell.width - 5</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <value name="maxHeight">
                                                                                                                                                                          <block type="expression" id="@2KZ]iOdGW=q%X}_EhIi5">
                                                                                                                                                                            <field name="value">$cell.height - 5</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_image_key" id="m1M[7/-Y:-I)at95)GC9">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="key">
                                                                                                                                                                              <block type="string_value" id="sG{/qj!ssU=0]IY_fx:!">
                                                                                                                                                                                <field name="value">${item}_1.png</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_image_src" id="ysp-ur#8;^wlr.)6qirC">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="src">
                                                                                                                                                                                  <block type="string_value" id="hfjWBbrCO].F(HWIM=yl">
                                                                                                                                                                                    <field name="value">${image_path}${item}_1.png</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_grid_cell_template_for" id="JD5B0KjbjWI2ZURV8G|^">
                                                                                                                                                                <field name="variable">$cell</field>
                                                                                                                                                                <field name="condition">$cell.data &gt;= number[data]</field>
                                                                                                                                                                <field name="#prop">cell.templates[]</field>
                                                                                                                                                                <field name="#callback">$cell</field>
                                                                                                                                                                <statement name="body">
                                                                                                                                                                  <block type="image_shape" id="r]C[Ki!@2(wTw953=M7n{">
                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                      <block type="prop_position" id="[@1ZhZo-Q|G2,G[AbPBY0">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="x">
                                                                                                                                                                          <block type="expression" id="`O0lq2BOW29DT3+wy;6l">
                                                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <value name="y">
                                                                                                                                                                          <block type="expression" id="v2w}Uh,Ndk}ny:y;B%kt">
                                                                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_max_size" id="w(N4/{pAD_fisp2%Lag-">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="maxWidth">
                                                                                                                                                                              <block type="expression" id="%BNl@2;b^dgciRT{B#F^G">
                                                                                                                                                                                <field name="value">$cell.width - 5</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="maxHeight">
                                                                                                                                                                              <block type="expression" id="sp5LxXD-FVET9Nk6?+c9">
                                                                                                                                                                                <field name="value">$cell.height - 5</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_image_key" id="R:#Z)~yVx%;#`gGLlMd6">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="key">
                                                                                                                                                                                  <block type="string_value" id="}^Y?Xb/S@15yiqA){Y[)x">
                                                                                                                                                                                    <field name="value">${item}_0.png</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_image_src" id=";{M}#Rf,fik|TgmcQW,e">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="src">
                                                                                                                                                                                      <block type="string_value" id="tvg{yx}KKP08u,no2uBI">
                                                                                                                                                                                        <field name="value">${image_path}${item}_0.png</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="func_add_answer" id="|:brd;{%ZUqGryepXafC">
                                                                                <value name="value">
                                                                                  <block type="expression" id="+}(bgSeXcXiSD2cHpq}2">
                                                                                    <field name="value">num</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="variable" id="weElLx3.8SB.)STr377[">
                                                                                    <field name="name">loadAssets</field>
                                                                                    <value name="value">
                                                                                      <block type="custom_image_list" id="[iQG~r:b@1r?SU7Kr3cr@2">
                                                                                        <field name="link">${image_path}</field>
                                                                                        <field name="images">${item}_0|${item}_1|big</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="partial_explanation" id="UEQOGA!8XMIc=Id(TUBr" inline="true">
                                                                                        <value name="value">
                                                                                          <block type="string_value" id="l4+i1`09?t#UO2i+{}.8">
                                                                                            <field name="value">&lt;u&gt;Step 1: Count the number of ${list_item[item]}.&lt;/u&gt;
&lt;/br&gt;&lt;/br&gt;
&lt;style&gt;
   table{border: 1px dashed red; border-radius: 20px; background: rgb(150, 200, 250, 0.3); margin: 20px; display: inline-block}
  td{text-align: center; padding: 5px;}
img{max-width: 60px; max-height: 60px}
  &lt;/style&gt;
&lt;center&gt;
&lt;table&gt;
                                                                                            </field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="variable" id="(L`)3:fm0pdCs7vV@2,+|">
                                                                                            <field name="name">i</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="30@2KZ(oVB1cfD%Sm}zEA">
                                                                                                <field name="value">0</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="variable" id="[Wc@1yR@2ym_3GiNH2zv|d">
                                                                                                <field name="name">count</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="$+3re@2RDLhvxII[:r?9Q">
                                                                                                    <field name="value">0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="while_do_block" id="5OD@2~[p;kG_l7$W367M|">
                                                                                                    <value name="while">
                                                                                                      <block type="expression" id="1u|0r]KFM[tE14j]0nNz">
                                                                                                        <field name="value">i &lt; row</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <statement name="do">
                                                                                                      <block type="partial_explanation" id="a[[uAV7y`SXpP%]0o!]k" inline="true">
                                                                                                        <value name="value">
                                                                                                          <block type="string_value" id="/Zf)st:G(!gI_,8t$by_">
                                                                                                            <field name="value">&lt;tr&gt;</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="variable" id="L:A-/1Y:^)J.2}WH(Ux@1">
                                                                                                            <field name="name">j</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="+kk_:NF]r_(19dXv]oJO">
                                                                                                                <field name="value">0</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="while_do_block" id="BX$cY?l#Q#wJ1GZG%=mQ">
                                                                                                                <value name="while">
                                                                                                                  <block type="expression" id="xJe=86Z[OFL#,C%k8W@1R">
                                                                                                                    <field name="value">j &lt; col</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <statement name="do">
                                                                                                                  <block type="if_then_else_block" id="M/~}Z-8{6)0q,E{Cab$k">
                                                                                                                    <value name="if">
                                                                                                                      <block type="expression" id=")/uNGpqVw37~Mq,jw0aQ">
                                                                                                                        <field name="value">count &lt; total</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <statement name="then">
                                                                                                                      <block type="partial_explanation" id="jy8c(p{cu@1gr4n%lk[R{" inline="true">
                                                                                                                        <value name="value">
                                                                                                                          <block type="string_value" id="4A$Dj5F!DLNN/misows5">
                                                                                                                            <field name="value">&lt;td&gt;&lt;img src='@1sprite.src(${item}_0)'/&gt;&lt;sup style='font-size: 24px; color: blue'&gt;${count+1}&lt;/sup&gt;&lt;/td&gt;</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="variable" id="!D^2[9LPCWk?2%oJmlV}">
                                                                                                                            <field name="name">count</field>
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="CVn,4ppCT?e@1DlCWF`o~">
                                                                                                                                <field name="value">count+1</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <statement name="else">
                                                                                                                      <block type="partial_explanation" id="HT9vGpANT(1eK[tAm!6!" inline="true">
                                                                                                                        <value name="value">
                                                                                                                          <block type="string_value" id="3Pn7Twzc,EgY(,^6-njN">
                                                                                                                            <field name="value">&lt;td&gt;&lt;/td&gt;</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="variable" id="zIeHuBN_7NH|F4~7f?`4">
                                                                                                                        <field name="name">j</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id="wRD#P)ku02kM@25u[Kky9">
                                                                                                                            <field name="value">j+1</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="partial_explanation" id="JI|Q/VAUlx{a^vFx),RD" inline="true">
                                                                                                                    <value name="value">
                                                                                                                      <block type="string_value" id="HD[)@2X1Q2ogafAy%,;u/">
                                                                                                                        <field name="value">&lt;/tr&gt;</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="variable" id=".Z1@2WH)2O=qcMIeHn;1|">
                                                                                                                        <field name="name">i</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id="l4%D+bHeI.0/8Rm,T,IO">
                                                                                                                            <field name="value">i+1</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="partial_explanation" id="tS3#~?93.;{T4d(U0poI" inline="true">
                                                                                                        <value name="value">
                                                                                                          <block type="string_value" id="jfdpG%+ZphD;FokDR~yd">
                                                                                                            <field name="value">&lt;/table&gt;&lt;/center&gt;&lt;/br&gt;
&lt;span style='background: rgb(250, 250, 250, 0.5)'&gt;There are &lt;b&gt;${total}&lt;/b&gt; ${list_item[item]} in total.&lt;/span&gt;
                                                                                                            </field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="end_partial_explanation" id="+.c]]Js]m$!#SX{#I#]Z">
                                                                                                            <next>
                                                                                                              <block type="partial_explanation" id="7rTe_;OSW@2p9vo0[eHm:" inline="true">
                                                                                                                <value name="value">
                                                                                                                  <block type="string_value" id="NuD46j~D;j=ve#]|l!Mx">
                                                                                                                    <field name="value">&lt;u&gt;Step 2: Divide the number of total parts into 2.&lt;/u&gt;
&lt;/br&gt;
  &lt;/br&gt;
${total} &amp;#247; ${2} = ${num}&lt;/br&gt;
Shade ${num} ${list_item[item]}.
&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
   table{border: 1px dashed red; border-radius: 20px; background: rgb(150, 200, 250, 0.3); margin: 20px; display: inline-block}
  td{text-align: center; padding: 5px;}
img{max-width: 60px; max-height: 60px}
  &lt;/style&gt;
&lt;center&gt;
&lt;table&gt;
                                                                                                                    </field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="variable" id="1lgE#GHh#OdL.0=VYWg%">
                                                                                                                    <field name="name">i</field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="DkT%0(}[T-sTx~xeMDlK">
                                                                                                                        <field name="value">0</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="variable" id="U/C$kz|-zFSPv=0P.$XM">
                                                                                                                        <field name="name">count</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id="y20/dA=z|+P^O_f},z@1_">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="while_do_block" id="1#KSb7dadm-p$S+7uwTB">
                                                                                                                            <value name="while">
                                                                                                                              <block type="expression" id="KMoHLvSc#wK@1W.wzDB9J">
                                                                                                                                <field name="value">i &lt; row</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <statement name="do">
                                                                                                                              <block type="partial_explanation" id="on8ru3ve@1QQ!:3Lyoh_P" inline="true">
                                                                                                                                <value name="value">
                                                                                                                                  <block type="string_value" id="{V:TWW],D^!f9J=d,@2,F">
                                                                                                                                    <field name="value">&lt;tr&gt;</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="variable" id="/01!s{axrDvhT3oUg-u`">
                                                                                                                                    <field name="name">j</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="oiJ?H88MGt45^;TF9WT:">
                                                                                                                                        <field name="value">0</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="while_do_block" id="t.7e!{Rz`}{}hlQ7P7s$">
                                                                                                                                        <value name="while">
                                                                                                                                          <block type="expression" id="2-5.g{Df`gRArpcu0s{,">
                                                                                                                                            <field name="value">j &lt; col</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <statement name="do">
                                                                                                                                          <block type="if_then_else_block" id="j/l$Hs08VMd_(iHk~6EY">
                                                                                                                                            <value name="if">
                                                                                                                                              <block type="expression" id=",9r_2[R===X=YAT@2bpo%">
                                                                                                                                                <field name="value">count &lt; total</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <statement name="then">
                                                                                                                                              <block type="if_then_else_block" id="@1rMxUO]M@2VEX2xvt)h^{">
                                                                                                                                                <value name="if">
                                                                                                                                                  <block type="expression" id="`Z{7l2Z@1hftOwYQq[/Pi">
                                                                                                                                                    <field name="value">count &lt; num</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <statement name="then">
                                                                                                                                                  <block type="partial_explanation" id="VMDoS%XI{Z7I}6YT2P3D" inline="true">
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="string_value" id="q!M8^P0b6oug4TjwehKA">
                                                                                                                                                        <field name="value">&lt;td&gt;&lt;img src='@1sprite.src(${item}_1)'/&gt;&lt;sup style='font-size: 24px; color: blue'&gt;${count+1}&lt;/sup&gt;&lt;/td&gt;</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                                <statement name="else">
                                                                                                                                                  <block type="partial_explanation" id="r/[0L/OBsn;r//]!W[lv" inline="true">
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="string_value" id="T@2%-HCYH8H0j18aY:iu^">
                                                                                                                                                        <field name="value">&lt;td&gt;&lt;img src='@1sprite.src(${item}_0)'/&gt;&lt;/td&gt;</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                                <next>
                                                                                                                                                  <block type="variable" id="Owb5/SSJ0l@1d)n@2v7ocK">
                                                                                                                                                    <field name="name">count</field>
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="expression" id="lN_Z.k`4h;BP;t@2KlK)A">
                                                                                                                                                        <field name="value">count+1</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                            <statement name="else">
                                                                                                                                              <block type="partial_explanation" id="lzZS|0C%_6hNw8!~C/7x" inline="true">
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="string_value" id="lujf)qD4$./@1wCE+}8Tt">
                                                                                                                                                    <field name="value">&lt;td&gt;&lt;/td&gt;</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                            <next>
                                                                                                                                              <block type="variable" id="V|OyYv/cSR~Q{@1Pg~TbR">
                                                                                                                                                <field name="name">j</field>
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="expression" id="^l0=c0.NZs_Gjv-,+K/9">
                                                                                                                                                    <field name="value">j+1</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <next>
                                                                                                                                          <block type="partial_explanation" id="sif-MUcJ4u15}U,.r/Y-" inline="true">
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="string_value" id="G1[PQ~W@11I2R={,~-1if">
                                                                                                                                                <field name="value">&lt;/tr&gt;</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="variable" id="{;iaO;nmgX;H8E33zX:o">
                                                                                                                                                <field name="name">i</field>
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="expression" id="mKN9{;JLb@2K,C|{0ccfy">
                                                                                                                                                    <field name="value">i+1</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="partial_explanation" id=":EQGkb:}OrL:f:?VZ6()" inline="true">
                                                                                                                                <value name="value">
                                                                                                                                  <block type="string_value" id="rP558:lxmc3yE{:s$37L">
                                                                                                                                    <field name="value">&lt;/table&gt;&lt;/center&gt;</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="end_partial_explanation" id="a:;{pG(wypS{G1kllma3">
                                                                                                                                    <next>
                                                                                                                                      <block type="variable" id="wa0KBaIPd#lz:p67^x|y">
                                                                                                                                        <field name="name">border</field>
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="expression" id="/hn[5e7R$3U!(u`|k)qh">
                                                                                                                                            <field name="value">[]</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="variable" id="a#oxGeO?@2`1~s:^y$@1ji">
                                                                                                                                            <field name="name">i</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id="|b1MtKZ)+B:GdE?c2qc6">
                                                                                                                                                <field name="value">0</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="while_do_block" id="1M[]4M@1$`k7(VDC(9I![">
                                                                                                                                                <value name="while">
                                                                                                                                                  <block type="expression" id="Z7w4t]/mq%RqZJ?y?(?G">
                                                                                                                                                    <field name="value">i &lt; 4</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <statement name="do">
                                                                                                                                                  <block type="if_then_else_block" id="2+QK//|g3Pywl.fN`O6r">
                                                                                                                                                    <value name="if">
                                                                                                                                                      <block type="expression" id="mz:RgAN1xx|705P6c1{l">
                                                                                                                                                        <field name="value">number[i] == num</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <statement name="then">
                                                                                                                                                      <block type="variable" id="=9Cj!8gPm/:w|XB7%Fg4">
                                                                                                                                                        <field name="name">border[i]</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="OKI-Yzaodl@2a4!lAm2X+">
                                                                                                                                                            <field name="value">'5px solid green'</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <statement name="else">
                                                                                                                                                      <block type="variable" id="l~24d5_e(a5H@2}9I,7;)">
                                                                                                                                                        <field name="name">border[i]</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="h-U0[{0%CHv?@2]oZk%ey">
                                                                                                                                                            <field name="value">'1px dashed red'</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="variable" id="hkH|SY})HhHHq,X5[Ru|">
                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="cT55bi_ziS?No:3[i=$y">
                                                                                                                                                            <field name="value">i+1</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                                <next>
                                                                                                                                                  <block type="partial_explanation" id="zi.EijVwpi,6U{:};=@2;" inline="true">
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="string_value" id="uhU?+w15^Jd3|mIK`0Wu">
                                                                                                                                                        <field name="value">&lt;u&gt;Step 3: Which one looks like it?&lt;/u&gt;
&lt;/br&gt;&lt;/br&gt;
                                                                                                                                                        </field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="partial_explanation" id="_rCeOsVtJm,RnEdm(8,b" inline="true">
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="string_value" id="Z6,1Lz@2,@1]]^1)8s7=QE">
                                                                                                                                                            <field name="value">&lt;style&gt;
  table{border-radius: 20px; background: rgb(150, 200, 250, 0.3); margin: 10px; display: inline-block}
  td{text-align: center; padding: 5px;}
img{max-width: 60px; max-height: 60px; padding: 5px}
  &lt;/style&gt;
&lt;center&gt;
                                                                                                                                                            </field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="variable" id="e5`V]bt50JzA4{2k/Ze1">
                                                                                                                                                            <field name="name">z</field>
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id="cKzlI]XZe{y[U)G!bx(9">
                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="while_do_block" id="d2(LU[X,,L3g3nUw@2;tQ">
                                                                                                                                                                <value name="while">
                                                                                                                                                                  <block type="expression" id="JvkTE8[yI(2UChZzTvUo">
                                                                                                                                                                    <field name="value">z &lt; 4</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <statement name="do">
                                                                                                                                                                  <block type="partial_explanation" id="MjjNwN:bD0[5@2+V_6-zq" inline="true">
                                                                                                                                                                    <value name="value">
                                                                                                                                                                      <block type="string_value" id="t1i(t0wxyNh@12vwwW_;q">
                                                                                                                                                                        <field name="value">&lt;table style='border: ${border[z]}'&gt;</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="variable" id="3NGdIS`A!jBOA0=BI+xK">
                                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="expression" id="_@2h(X:C`EJ/PT{{2QQv-">
                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="variable" id="h`1R,T_O7S2;JxntzDoU">
                                                                                                                                                                            <field name="name">count</field>
                                                                                                                                                                            <value name="value">
                                                                                                                                                                              <block type="expression" id="JlG!!l3$VWpxyn%B-55r">
                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="while_do_block" id="X?-^p=M~cVL8f3MBIvAW">
                                                                                                                                                                                <value name="while">
                                                                                                                                                                                  <block type="expression" id="B$8o%ur6QjIsb-BX_lgP">
                                                                                                                                                                                    <field name="value">i &lt; row</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <statement name="do">
                                                                                                                                                                                  <block type="partial_explanation" id="}P/N+FVlSbyi3o%jK0ng" inline="true">
                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                      <block type="string_value" id="F15Gfx%t7+$OP+TawnMs">
                                                                                                                                                                                        <field name="value">&lt;tr&gt;</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="variable" id="/ili%j(Wy-YVhR7$gia;">
                                                                                                                                                                                        <field name="name">j</field>
                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                          <block type="expression" id="#z=pLaiLK(0+.HZn@1XQJ">
                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="while_do_block" id=":fM?s#.F[!xUjUSYi]yY">
                                                                                                                                                                                            <value name="while">
                                                                                                                                                                                              <block type="expression" id="u/-;a_$p@1^eht}[!Is@19">
                                                                                                                                                                                                <field name="value">j &lt; col</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <statement name="do">
                                                                                                                                                                                              <block type="if_then_else_block" id="8z.I|y@2m@1]8M7hNUJPmx">
                                                                                                                                                                                                <value name="if">
                                                                                                                                                                                                  <block type="expression" id="+`c/Gz7Q^{.fQ4ZjfhF$">
                                                                                                                                                                                                    <field name="value">count &lt; total</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <statement name="then">
                                                                                                                                                                                                  <block type="if_then_else_block" id="7oxzm9Kzwzz,%}El1tUv">
                                                                                                                                                                                                    <value name="if">
                                                                                                                                                                                                      <block type="expression" id=".C{lU0=|40[{u,g9atxG">
                                                                                                                                                                                                        <field name="value">count &lt; number[z]</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <statement name="then">
                                                                                                                                                                                                      <block type="partial_explanation" id="Q`^_)^dI4R,`qh8=?+Yb" inline="true">
                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                          <block type="string_value" id="/X(wR;zi;|u=TSE7Ik8n">
                                                                                                                                                                                                            <field name="value">&lt;td&gt;&lt;img src='@1sprite.src(${item}_1)'/&gt;&lt;/td&gt;</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                    <statement name="else">
                                                                                                                                                                                                      <block type="partial_explanation" id="PqAGvG.C3Ln3U;gvDQpq" inline="true">
                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                          <block type="string_value" id="Bv?1Bg%K#;HzuGI[U^:w">
                                                                                                                                                                                                            <field name="value">&lt;td&gt;&lt;img src='@1sprite.src(${item}_0)'/&gt;&lt;/td&gt;</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="variable" id="UoZO6R]9P2XT}vM107X-">
                                                                                                                                                                                                        <field name="name">count</field>
                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                          <block type="expression" id="+[/Csk@2q`KZ{@1;!tQ]F6">
                                                                                                                                                                                                            <field name="value">count+1</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </statement>
                                                                                                                                                                                                <statement name="else">
                                                                                                                                                                                                  <block type="partial_explanation" id="hVbyZgDrOyagz@1,2i!^u" inline="true">
                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                      <block type="string_value" id="}/rT}T2e3%cMfcqTS:qH">
                                                                                                                                                                                                        <field name="value">&lt;td&gt;&lt;/td&gt;</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </statement>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="variable" id="fVrWw2eR~KCTZv2zyG$S">
                                                                                                                                                                                                    <field name="name">j</field>
                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                      <block type="expression" id="Ap#Lph;1fT!q$1)zSJ?F">
                                                                                                                                                                                                        <field name="value">j+1</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </statement>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="partial_explanation" id="H:b5)seWWWgb7vQT!F-]" inline="true">
                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                  <block type="string_value" id="f|#Um_okVTFsTs)slCx9">
                                                                                                                                                                                                    <field name="value">&lt;/tr&gt;</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="variable" id="WG6u`}cP`3[RAqvs6Q;9">
                                                                                                                                                                                                    <field name="name">i</field>
                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                      <block type="expression" id="p$k4@1NQ2QQ+.Tw3~cNo`">
                                                                                                                                                                                                        <field name="value">i+1</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </statement>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="partial_explanation" id="Dn8E^1=^iUI+3m[jN/7H" inline="true">
                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                      <block type="string_value" id="(~(IMhe1,v,SesP)bfuB">
                                                                                                                                                                                        <field name="value">&lt;/table&gt;</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="if_then_block" id=".k^Sq66w([~niwBI^hdf">
                                                                                                                                                                                        <value name="if">
                                                                                                                                                                                          <block type="expression" id="Vu{1}PGJArr}m20uy/K8">
                                                                                                                                                                                            <field name="value">z == 1</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <statement name="then">
                                                                                                                                                                                          <block type="partial_explanation" id="fVFZ=4fRSgr:|!}dAoW9" inline="true">
                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                              <block type="string_value" id="7(1i!qUDOggZX=Z2|]Ug">
                                                                                                                                                                                                <field name="value">&lt;/br&gt;</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </statement>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="variable" id="{|bV9C}|5z$/`h~OjXgn">
                                                                                                                                                                                            <field name="name">z</field>
                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                              <block type="expression" id="Vt;M1G=vVxit}@1k?8uBA">
                                                                                                                                                                                                <field name="value">z+1</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="partial_explanation" id="Fohz6H`/M18wOU+@1L0r7" inline="true">
                                                                                                                                                                    <value name="value">
                                                                                                                                                                      <block type="string_value" id="v-q5VWcVe4I#TOM?+@2g%">
                                                                                                                                                                        <field name="value">&lt;/table&gt;&lt;/center&gt;</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="end_partial_explanation" id="tJsPt@1#DA09|BGz}ghH5"></block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
  <block type="partial_explanation" id="k[kB!F]qLgPK`TOQ@2`8a" inline="true" x="181" y="5133">
    <value name="value">
      <block type="string_value" id="f@1#K4vgua]-E1-/G801S">
        <field name="value">&lt;/br&gt;&lt;/br&gt;&lt;span style='background: rgb(150, 200, 250, 0.3)'&gt;The answer is&lt;/span&gt;&lt;/br&gt;&lt;/br&gt;
&lt;table&gt;
        </field>
      </block>
    </value>
    <next>
      <block type="variable" id="VSQRPYq7c}|r(X@1vjS/j">
        <field name="name">i</field>
        <value name="value">
          <block type="expression" id="qY-q[@2@1Ynt2cAlqf3l{^">
            <field name="value">0</field>
          </block>
        </value>
        <next>
          <block type="variable" id="O+(QA(@1DX{1+L!97F/u^">
            <field name="name">count</field>
            <value name="value">
              <block type="expression" id="sFqi?Ove%#Mg(4oRH%U6">
                <field name="value">0</field>
              </block>
            </value>
            <next>
              <block type="while_do_block" id="@1C8(R{.d8u:j9VzF^u$:">
                <value name="while">
                  <block type="expression" id="9@2!C6j15$F0}(`K8W!Ya">
                    <field name="value">i &lt; row</field>
                  </block>
                </value>
                <statement name="do">
                  <block type="partial_explanation" id="99X%O%xA!wpl}fgW=TWz" inline="true">
                    <value name="value">
                      <block type="string_value" id="1NYT{9;8z1qp`[U95E3Y">
                        <field name="value">&lt;tr&gt;</field>
                      </block>
                    </value>
                    <next>
                      <block type="variable" id="12n2F_y3IK6@2v@1yrYL2X">
                        <field name="name">j</field>
                        <value name="value">
                          <block type="expression" id="#HauITjhZXy!%jA9p0d#">
                            <field name="value">0</field>
                          </block>
                        </value>
                        <next>
                          <block type="while_do_block" id="_D[`@27m0p]^$LJ}an@1jl">
                            <value name="while">
                              <block type="expression" id=":MPXC[]flFL@1bTZxn_K~">
                                <field name="value">j &lt; col</field>
                              </block>
                            </value>
                            <statement name="do">
                              <block type="if_then_else_block" id="kzP8wHOOc:$1ypG8([0C">
                                <value name="if">
                                  <block type="expression" id="IUq+,A+(AQ$D-YhdXVp6">
                                    <field name="value">count &lt; total</field>
                                  </block>
                                </value>
                                <statement name="then">
                                  <block type="if_then_else_block" id="/+rqjI)Y;QYz[tAx~_%E">
                                    <value name="if">
                                      <block type="expression" id="k1ygx{X.c~9nt|:=awO5">
                                        <field name="value">count &lt; num</field>
                                      </block>
                                    </value>
                                    <statement name="then">
                                      <block type="partial_explanation" id="(MRTV7(fR12)NA41XRa/" inline="true">
                                        <value name="value">
                                          <block type="string_value" id="T;/Kp+yfU#8XnMt^=#j%">
                                            <field name="value">&lt;td&gt;&lt;img src='@1sprite.src(${item}_1)'/&gt;&lt;/td&gt;</field>
                                          </block>
                                        </value>
                                      </block>
                                    </statement>
                                    <statement name="else">
                                      <block type="partial_explanation" id="IZRp@1~j4dfHCNA~n8cS?" inline="true">
                                        <value name="value">
                                          <block type="string_value" id="%g]/72k5H_2Cuv~eAACz">
                                            <field name="value">&lt;td&gt;&lt;img src='@1sprite.src(${item}_0)'/&gt;&lt;/td&gt;</field>
                                          </block>
                                        </value>
                                      </block>
                                    </statement>
                                    <next>
                                      <block type="variable" id="E8z(srjF@1H}@2iAqHfGkT">
                                        <field name="name">count</field>
                                        <value name="value">
                                          <block type="expression" id="DA4;fiST6C/8S!?8M#S-">
                                            <field name="value">count+1</field>
                                          </block>
                                        </value>
                                      </block>
                                    </next>
                                  </block>
                                </statement>
                                <statement name="else">
                                  <block type="partial_explanation" id="cV`P|8v#B=TNjiNP2m7E" inline="true">
                                    <value name="value">
                                      <block type="string_value" id=";4.$lVAzX=Dmutr2kgBt">
                                        <field name="value">&lt;td&gt;&lt;/td&gt;</field>
                                      </block>
                                    </value>
                                  </block>
                                </statement>
                                <next>
                                  <block type="variable" id=")]U#`q|2H~,b?7TKY.@2]">
                                    <field name="name">j</field>
                                    <value name="value">
                                      <block type="expression" id="IHC$UrJbRe{;4z.A@2Lhi">
                                        <field name="value">j+1</field>
                                      </block>
                                    </value>
                                  </block>
                                </next>
                              </block>
                            </statement>
                            <next>
                              <block type="partial_explanation" id="CeBa@1gu+u4;uwvKzsG(!" inline="true">
                                <value name="value">
                                  <block type="string_value" id="Bn(CC9!p-{lMf$IEVVuz">
                                    <field name="value">&lt;/tr&gt;</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id=",;]bk,0#3qBneB@1,c{S.">
                                    <field name="name">i</field>
                                    <value name="value">
                                      <block type="expression" id="2Pte$_3~gG1BY@2u%)$74">
                                        <field name="value">i+1</field>
                                      </block>
                                    </value>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </statement>
              </block>
            </next>
          </block>
        </next>
      </block>
    </next>
  </block>
</xml>
END_XML]] */