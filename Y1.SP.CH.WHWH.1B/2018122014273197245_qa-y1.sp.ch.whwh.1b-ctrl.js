
module.exports = [
  {
    "#type": "question",
    "name": "Y1.SP.CH.WHWH.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/Y1.SP.CH.WHWH.1B/"
      },
      {
        "#type": "variable",
        "name": "background",
        "value": {
          "#type": "string_array",
          "items": "bg1|bg2|bg3|bg4"
        }
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_one",
          "items": {
            "#type": "expression",
            "value": "background"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "variable",
        "name": "type",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "2"
          }
        }
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "statement",
        "value": "function re_color(color) {\n  if(color == 'g'){return 'green';}\n  if(color == 'r'){return 'red';}\n  if(color == 'b'){return 'blue';}\n  if(color == 'y'){return 'yellow';}\n}"
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "variable",
            "name": "tittle",
            "value": {
              "#type": "random_one",
              "items": {
                "#type": "expression",
                "value": "['How likely are you going to', 'What are the chances you will', 'What is the probability that you will']"
              }
            }
          },
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "range < 4"
            },
            "then": [
              {
                "#type": "variable",
                "name": "total",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "3"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "10"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "num",
                "value": {
                  "#type": "expression",
                  "value": "2"
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "total",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "10"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "16"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "num",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "3"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "4"
                  }
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "an",
            "value": {
              "#type": "string_array",
              "items": "a|a|a|a|an|a"
            }
          },
          {
            "#type": "variable",
            "name": "list_item",
            "value": {
              "#type": "string_array",
              "items": "heart|star|triangle|square|octagon|diamond"
            }
          },
          {
            "#type": "variable",
            "name": "list_items",
            "value": {
              "#type": "string_array",
              "items": "hearts|stars|triangles|squares|octagons|diamonds"
            }
          },
          {
            "#type": "variable",
            "name": "color",
            "value": {
              "#type": "string_array",
              "items": "ligth blue|purple|pink|orange|blue|green|yellow|red"
            }
          },
          {
            "#type": "variable",
            "name": "item",
            "value": {
              "#type": "random_many",
              "count": {
                "#type": "expression",
                "value": "num"
              },
              "items": {
                "#type": "expression",
                "value": "[0, 1, 2, 3, 4, 5]"
              }
            }
          },
          {
            "#type": "variable",
            "name": "num_item",
            "value": {
              "#type": "expression",
              "value": "[]"
            }
          },
          {
            "#type": "variable",
            "name": "answer",
            "value": {
              "#type": "random_one",
              "items": {
                "#type": "expression",
                "value": "[\"will happen\", \"probably\", \"might happen\", \"won't happen\"]"
              }
            }
          },
          {
            "#type": "variable",
            "name": "num_item",
            "value": {
              "#type": "expression",
              "value": "[]"
            }
          },
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "answer == \"won't happen\""
            },
            "then": [
              {
                "#type": "variable",
                "name": "num_item[0]",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "variable",
                "name": "min",
                "value": {
                  "#type": "expression",
                  "value": "1"
                }
              }
            ],
            "else": [
              {
                "#type": "if_then_else_block",
                "if": {
                  "#type": "expression",
                  "value": "answer == \"might happen\""
                },
                "then": [
                  {
                    "#type": "variable",
                    "name": "num_item[0]",
                    "value": {
                      "#type": "random_number",
                      "min": {
                        "#type": "expression",
                        "value": "1"
                      },
                      "max": {
                        "#type": "expression",
                        "value": "Math.floor(total/2)-1"
                      }
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "min",
                    "value": {
                      "#type": "expression",
                      "value": "1"
                    }
                  }
                ],
                "else": [
                  {
                    "#type": "if_then_else_block",
                    "if": {
                      "#type": "expression",
                      "value": "answer == \"probably\""
                    },
                    "then": [
                      {
                        "#type": "variable",
                        "name": "num_item[0]",
                        "value": {
                          "#type": "random_number",
                          "min": {
                            "#type": "expression",
                            "value": "Math.floor(total/2)+1"
                          },
                          "max": {
                            "#type": "expression",
                            "value": "total-1"
                          }
                        }
                      },
                      {
                        "#type": "variable",
                        "name": "min",
                        "value": {
                          "#type": "expression",
                          "value": "1"
                        }
                      }
                    ],
                    "else": [
                      {
                        "#type": "variable",
                        "name": "num_item[0]",
                        "value": {
                          "#type": "expression",
                          "value": "total"
                        }
                      },
                      {
                        "#type": "variable",
                        "name": "min",
                        "value": {
                          "#type": "expression",
                          "value": "0"
                        }
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "total <= 10"
            },
            "then": [
              {
                "#type": "variable",
                "name": "grid",
                "value": {
                  "#type": "expression",
                  "value": "4"
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "grid",
                "value": {
                  "#type": "expression",
                  "value": "5"
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "1"
            }
          },
          {
            "#type": "variable",
            "name": "temp",
            "value": {
              "#type": "expression",
              "value": "total - num_item[0]"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < num - 1"
            },
            "do": [
              {
                "#type": "variable",
                "name": "num_item[i]",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "min"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "temp - num + i + 1"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "temp",
                "value": {
                  "#type": "expression",
                  "value": "temp - num_item[i]"
                }
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "num_item[i]",
            "value": {
              "#type": "expression",
              "value": "temp"
            }
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "variable",
            "name": "list",
            "value": {
              "#type": "expression",
              "value": "[]"
            }
          },
          {
            "#type": "variable",
            "name": "co",
            "value": {
              "#type": "expression",
              "value": "[]"
            }
          },
          {
            "#type": "variable",
            "name": "n",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < num"
            },
            "do": [
              {
                "#type": "variable",
                "name": "j",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "j < num_item[i]"
                },
                "do": [
                  {
                    "#type": "variable",
                    "name": "list[n]",
                    "value": {
                      "#type": "expression",
                      "value": "item[i]"
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "co[n]",
                    "value": {
                      "#type": "random_number",
                      "min": {
                        "#type": "expression",
                        "value": "0"
                      },
                      "max": {
                        "#type": "expression",
                        "value": "7"
                      }
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "n",
                    "value": {
                      "#type": "expression",
                      "value": "n+1"
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "j",
                    "value": {
                      "#type": "expression",
                      "value": "j+1"
                    }
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "total"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < grid*grid"
            },
            "do": [
              {
                "#type": "variable",
                "name": "list[i]",
                "value": {
                  "#type": "expression",
                  "value": "-1"
                }
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "list",
            "value": {
              "#type": "func",
              "name": "shuffle",
              "args": [
                {
                  "#type": "expression",
                  "value": "list"
                }
              ]
            }
          },
          {
            "#type": "variable",
            "name": "select",
            "value": {
              "#type": "random_one",
              "items": {
                "#type": "expression",
                "value": "['grab', 'choose', 'select']"
              }
            }
          },
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "select == 'choose'"
            },
            "then": [
              {
                "#type": "variable",
                "name": "selecting",
                "value": {
                  "#type": "expression",
                  "value": "'choosing'"
                }
              }
            ],
            "else": [
              {
                "#type": "if_then_else_block",
                "if": {
                  "#type": "expression",
                  "value": "select == 'grab'"
                },
                "then": [
                  {
                    "#type": "variable",
                    "name": "selecting",
                    "value": {
                      "#type": "expression",
                      "value": "'grabbing'"
                    }
                  }
                ],
                "else": [
                  {
                    "#type": "variable",
                    "name": "selecting",
                    "value": {
                      "#type": "expression",
                      "value": "'selecting'"
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "40"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "${tittle} ${select} ${an[item[0]]} ${list_item[item[0]]} at random?"
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "35"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "220"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "0"
                },
                "height": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "bottle.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}bottle.png"
              }
            ]
          },
          {
            "#type": "variable",
            "name": "count",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "grid"
                },
                "cols": {
                  "#type": "expression",
                  "value": "grid"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "250"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "160"
                },
                "height": {
                  "#type": "expression",
                  "value": "160"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "func",
                  "name": "arrayOfNumber",
                  "args": [
                    {
                      "#type": "expression",
                      "value": "grid*grid"
                    },
                    {
                      "#type": "expression",
                      "value": "0"
                    }
                  ]
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_cell_template_for",
                "variable": "$cell",
                "condition": "list[$cell.data] >= 0",
                "#prop": "cell.templates[]",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "variable",
                    "name": "ox",
                    "value": {
                      "#type": "random_number",
                      "min": {
                        "#type": "expression",
                        "value": "-5"
                      },
                      "max": {
                        "#type": "expression",
                        "value": "5"
                      }
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "oy",
                    "value": {
                      "#type": "random_number",
                      "min": {
                        "#type": "expression",
                        "value": "-5"
                      },
                      "max": {
                        "#type": "expression",
                        "value": "5"
                      }
                    }
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX+ox"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY+oy"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "0"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "0"
                        }
                      },
                      {
                        "#type": "prop_max_size",
                        "#prop": "",
                        "maxWidth": {
                          "#type": "expression",
                          "value": "$cell.width"
                        },
                        "maxHeight": {
                          "#type": "expression",
                          "value": "$cell.height"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "${list[$cell.data]}_${co[count]}.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}${list[$cell.data]}_${co[count]}.png"
                      },
                      {
                        "#type": "prop_scale",
                        "#prop": "",
                        "scale": {
                          "#type": "expression",
                          "value": "0.95"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "count",
                    "value": {
                      "#type": "expression",
                      "value": "count +1"
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "4"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "400"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "750"
                },
                "height": {
                  "#type": "expression",
                  "value": "50"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "[\"will happen\", \"probably\", \"might happen\", \"won't happen\"]"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": true
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "choice_custom_shape",
                    "action": "click-one",
                    "value": {
                      "#type": "expression",
                      "value": "$cell.data"
                    },
                    "template": {
                      "#callback": "$choice",
                      "variable": "$choice",
                      "body": [
                        {
                          "#type": "image_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "$cell.centerX"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "$cell.centerY"
                              }
                            },
                            {
                              "#type": "prop_size",
                              "#prop": "",
                              "width": {
                                "#type": "expression",
                                "value": "0"
                              },
                              "height": {
                                "#type": "expression",
                                "value": "0"
                              }
                            },
                            {
                              "#type": "prop_image_key",
                              "#prop": "",
                              "key": "${$cell.data[2]}.png"
                            },
                            {
                              "#type": "prop_image_src",
                              "#prop": "",
                              "src": "${image_path}${$cell.data[2]}.png"
                            }
                          ]
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "func",
            "name": "addAnswers",
            "args": [
              {
                "#type": "expression",
                "value": "answer"
              }
            ]
          },
          {
            "#type": "variable",
            "name": "link",
            "value": {
              "#type": "expression",
              "value": "'http://starmathsonline.s3.amazonaws.com/' + image_path"
            }
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 1: Familiarise yourself with the meaning of \"will happen\", \"probably\", \"might happen\", \"won't happen\".</u></br></br>\n<style>\n  td{border: 1px solid black; text-align: center}\ntable{background: rgb(179, 218, 255, 0.5)}\n</style>\n<center>\n  <table>\n    <tr>\n      <td style='text-align: center; width: 250px'><b style=''>Probability Term</b></td>\n      <td style='text-align: center; width: 750px'><b style=''>Meaning</b></td>\n    </tr>\n    <tr>\n      <td style='text-align: center; width: 250px'><span style=''>Will happen</span></td>\n      <td><span style=''>Event will surely happen, certain to take place.</span></td>\n    </tr>\n    <tr>\n      <td style='text-align: center; width: 250px'><span style=''>Probably</span></td>\n      <td><span style=''>Event has high chance of happening, very likely to take place.</span></td>\n    </tr>\n    <tr>\n      <td style='text-align: center; width: 250px'><span style=''>Might happen</span></td>\n      <td><span style=''>Event has high chance of happening, very likely to take place.</span></td>\n    </tr>\n    <tr>\n      <td style='text-align: center; width: 250px'><span style=''>Won't happen</span></td>\n      <td><span style=''>Event has no chance of happening, impossible to take place.</span></td>\n    </tr>\n   </table>\n</center>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 2: From the question, identify the item to focus on in the question.</u></br></br>\n<span style='background: rgb(250, 250, 250, 0.5)'>\n\"${tittle} ${select} ${an[item[0]]} <b>${list_item[item[0]]}?</b>\"</br></br>\n\nThe item to focus on is the <b>${list_item[item[0]]}</b>.</span>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 3: Identify the chances of ${selecting} ${an[item[0]]} ${list_item[item[0]]}.</u>\n</br></br>\n<span style='background: rgb(250, 250, 250, 0.5)'>\n  \nNumber of ${list_item[item[0]]}${num_item[0] < 2 ? '':'s'} : <b>${num_item[0]}</b></br>\n\n<style>\n  #stroke{\n   -webkit-text-stroke-width: 1px;\n   -webkit-text-stroke-color: white;\n}\n  table {overflow-x:auto;}\n  td{text-align: center; width: 50px; height: ${160/grid}px;}\n#img {max-height: auto; max-width: ${160/grid}px; padding: 0; margin: 0; border: 0}\n  </style>\n<center>\n    <div style='position: relative; width: 252px; height: 285px'>\n  <img style='position: absolute; height: ${total > 10 ? 140:120}%; left: 0px; top: 0px' src='${link}bottle.png'/>\n<table style='position: absolute; top: ${115}px; left: ${50}px'>"
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "variable",
            "name": "count",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < grid"
            },
            "do": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<tr>"
                ]
              },
              {
                "#type": "variable",
                "name": "j",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "j < grid"
                },
                "do": [
                  {
                    "#type": "if_then_else_block",
                    "if": {
                      "#type": "expression",
                      "value": "list[i*grid+j] >= 0"
                    },
                    "then": [
                      {
                        "#type": "func",
                        "name": "addPartialExplanation",
                        "args": [
                          "<td><img id='img' src='${link}${list[i*grid+j]}_${co[count]}.png'/></td>"
                        ]
                      },
                      {
                        "#type": "variable",
                        "name": "count",
                        "value": {
                          "#type": "expression",
                          "value": "count + 1"
                        }
                      }
                    ],
                    "else": [
                      {
                        "#type": "func",
                        "name": "addPartialExplanation",
                        "args": [
                          "<td></td>"
                        ]
                      }
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "j",
                    "value": {
                      "#type": "expression",
                      "value": "j+1"
                    }
                  }
                ]
              },
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</tr>"
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</table>\n<table style='position: absolute; top: ${115}px; left: ${60}px'>"
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "variable",
            "name": "count",
            "value": {
              "#type": "expression",
              "value": "1"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < grid"
            },
            "do": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<tr>"
                ]
              },
              {
                "#type": "variable",
                "name": "j",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "j < grid"
                },
                "do": [
                  {
                    "#type": "if_then_else_block",
                    "if": {
                      "#type": "expression",
                      "value": "list[i*grid+j] == item[0]"
                    },
                    "then": [
                      {
                        "#type": "func",
                        "name": "addPartialExplanation",
                        "args": [
                          "<td><span id='stroke' style='color: black'>${count}</span></td>"
                        ]
                      },
                      {
                        "#type": "variable",
                        "name": "count",
                        "value": {
                          "#type": "expression",
                          "value": "count + 1"
                        }
                      }
                    ],
                    "else": [
                      {
                        "#type": "func",
                        "name": "addPartialExplanation",
                        "args": [
                          "<td></td>"
                        ]
                      }
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "j",
                    "value": {
                      "#type": "expression",
                      "value": "j+1"
                    }
                  }
                ]
              },
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</tr>"
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</table>\n</div>\n</center>\n</br>\nTotal items: <b>${total}</b></br></br>\n\n<b>${num_item[0]}</b> out of <b>${total}</b> chances.</br></br>\n\nThe chances of ${selecting} a shape is \"<b>${answer}</b>\".</span>"
            ]
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 2"
        },
        "then": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "range < 4"
            },
            "then": [
              {
                "#type": "variable",
                "name": "part",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "4"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "4"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "num",
                "value": {
                  "#type": "expression",
                  "value": "2"
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "part",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "8"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "16"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "num",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "3"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "4"
                  }
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "answer",
            "value": {
              "#type": "random_one",
              "items": {
                "#type": "expression",
                "value": "[\"will happen\", \"probably\", \"might happen\", \"won't happen\"]"
              }
            }
          },
          {
            "#type": "variable",
            "name": "num_color",
            "value": {
              "#type": "expression",
              "value": "[]"
            }
          },
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "answer == \"won't happen\""
            },
            "then": [
              {
                "#type": "variable",
                "name": "num_color[0]",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "variable",
                "name": "min",
                "value": {
                  "#type": "expression",
                  "value": "1"
                }
              }
            ],
            "else": [
              {
                "#type": "if_then_else_block",
                "if": {
                  "#type": "expression",
                  "value": "answer == \"might happen\""
                },
                "then": [
                  {
                    "#type": "variable",
                    "name": "num_color[0]",
                    "value": {
                      "#type": "random_number",
                      "min": {
                        "#type": "expression",
                        "value": "1"
                      },
                      "max": {
                        "#type": "expression",
                        "value": "Math.floor(part/2)-1"
                      }
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "min",
                    "value": {
                      "#type": "expression",
                      "value": "1"
                    }
                  }
                ],
                "else": [
                  {
                    "#type": "if_then_else_block",
                    "if": {
                      "#type": "expression",
                      "value": "answer == \"probably\""
                    },
                    "then": [
                      {
                        "#type": "variable",
                        "name": "num_color[0]",
                        "value": {
                          "#type": "random_number",
                          "min": {
                            "#type": "expression",
                            "value": "Math.floor(part/2)+1"
                          },
                          "max": {
                            "#type": "expression",
                            "value": "part-1"
                          }
                        }
                      },
                      {
                        "#type": "variable",
                        "name": "min",
                        "value": {
                          "#type": "expression",
                          "value": "1"
                        }
                      }
                    ],
                    "else": [
                      {
                        "#type": "variable",
                        "name": "num_color[0]",
                        "value": {
                          "#type": "expression",
                          "value": "part"
                        }
                      },
                      {
                        "#type": "variable",
                        "name": "min",
                        "value": {
                          "#type": "expression",
                          "value": "0"
                        }
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "1"
            }
          },
          {
            "#type": "variable",
            "name": "temp",
            "value": {
              "#type": "expression",
              "value": "part- num_color[0]"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < num - 1"
            },
            "do": [
              {
                "#type": "variable",
                "name": "num_color[i]",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "min"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "temp - num + i + 1"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "temp",
                "value": {
                  "#type": "expression",
                  "value": "temp - num_color[i]"
                }
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "num_color[i]",
            "value": {
              "#type": "expression",
              "value": "temp"
            }
          },
          {
            "#type": "variable",
            "name": "color",
            "value": {
              "#type": "random_many",
              "count": {
                "#type": "expression",
                "value": "num"
              },
              "items": {
                "#type": "expression",
                "value": "['b', 'g', 'r', 'y']"
              }
            }
          },
          {
            "#type": "variable",
            "name": "qes_color",
            "value": {
              "#type": "expression",
              "value": "color[0]"
            }
          },
          {
            "#type": "variable",
            "name": "spiner",
            "value": {
              "#type": "expression",
              "value": "[]"
            }
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "variable",
            "name": "count",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < num"
            },
            "do": [
              {
                "#type": "variable",
                "name": "j",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "j < num_color[i]"
                },
                "do": [
                  {
                    "#type": "variable",
                    "name": "spiner[count]",
                    "value": {
                      "#type": "expression",
                      "value": "color[i]"
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "j",
                    "value": {
                      "#type": "expression",
                      "value": "j+1"
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "count",
                    "value": {
                      "#type": "expression",
                      "value": "count + 1"
                    }
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "spiner",
            "value": {
              "#type": "func",
              "name": "shuffle",
              "args": [
                {
                  "#type": "expression",
                  "value": "spiner"
                }
              ]
            }
          },
          {
            "#type": "variable",
            "name": "tittle",
            "value": {
              "#type": "random_one",
              "items": {
                "#type": "expression",
                "value": "['How likely will', 'What is the chance', 'What is the probability of']"
              }
            }
          },
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "20"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "If we spin the spinner, ${tittle.toLowerCase()} the spinner lands on the ${re_color(qes_color)} colour?"
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "36"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "320"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "spin.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}spin.png"
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "variable",
            "name": "angle",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < part"
            },
            "do": [
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "400"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "200"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "1-${part}${spiner[i]}.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}1-${part}${spiner[i]}.png"
                  },
                  {
                    "#type": "prop_angle",
                    "#prop": "",
                    "angle": {
                      "#type": "expression",
                      "value": "angle"
                    }
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "angle",
                "value": {
                  "#type": "expression",
                  "value": "angle + 360/part"
                }
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "190"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "point.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}point.png"
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "4"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "400"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "750"
                },
                "height": {
                  "#type": "expression",
                  "value": "50"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "[\"will happen\", \"probably\", \"might happen\", \"won't happen\"]"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": true
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "choice_custom_shape",
                    "action": "click-one",
                    "value": {
                      "#type": "expression",
                      "value": "$cell.data"
                    },
                    "template": {
                      "#callback": "$choice",
                      "variable": "$choice",
                      "body": [
                        {
                          "#type": "image_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "$cell.centerX"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "$cell.centerY"
                              }
                            },
                            {
                              "#type": "prop_size",
                              "#prop": "",
                              "width": {
                                "#type": "expression",
                                "value": "0"
                              },
                              "height": {
                                "#type": "expression",
                                "value": "0"
                              }
                            },
                            {
                              "#type": "prop_image_key",
                              "#prop": "",
                              "key": "${$cell.data[2]}.png"
                            },
                            {
                              "#type": "prop_image_src",
                              "#prop": "",
                              "src": "${image_path}${$cell.data[2]}.png"
                            }
                          ]
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "func",
            "name": "addAnswers",
            "args": [
              {
                "#type": "expression",
                "value": "answer"
              }
            ]
          },
          {
            "#type": "variable",
            "name": "link",
            "value": {
              "#type": "expression",
              "value": "'http://starmathsonline.s3.amazonaws.com/' + image_path"
            }
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 1: Familiarise yourself with the meaning of \"will happen\", \"probably\", \"might happen\", \"won't happen\".</u></br></br>\n<style>\n  td{border: 1px solid black; text-align: center}\ntable{background: rgb(179, 218, 255, 0.5)}\n</style>\n<center>\n  <table>\n    <tr>\n      <td style='text-align: center; width: 250px'><b>Probability Term</b></td>\n      <td style='text-align: center; width: 750px'><b>Meaning</b></td>\n    </tr>\n    <tr>\n      <td style='text-align: center; width: 250px'><span style=''>Will happen</span></td>\n      <td><span style=''>Event will surely happen, certain to take place.</span></td>\n    </tr>\n    <tr>\n      <td style='text-align: center; width: 250px'><span style=''>Probably</span></td>\n      <td><span style=''>Event has high chance of happening, very likely to take place.</span></td>\n    </tr>\n    <tr>\n      <td style='text-align: center; width: 250px'><span style=''>Might happen</span></td>\n      <td><span style=''>Event has high chance of happening, very likely to take place.</span></td>\n    </tr>\n    <tr>\n      <td style='text-align: center; width: 250px'><span style=''>Won't happen</span></td>\n      <td><span style=''>Event has no chance of happening, impossible to take place.</span></td>\n    </tr>\n   </table>\n</center>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 2: From the question, identify the item to focus on in the question.</u></br></br>\n<span style='background: rgb(250, 250, 250, 0.5)'>\n\"If we spin the spinner, ${tittle.toLowerCase()} the spinner lands on the <b>${re_color(qes_color)}</b> colour?\"</br></br>\n\nThe colour to focus is <b>${re_color(qes_color)}</b> colour.\n  \n</span>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 3: Identify the chances of the spinner landing on the ${re_color(qes_color)} colour.</u>\n</br></br>\n  <center>\n<div style='position: relative; width:187px; height: 187px;'>\n  <img src='@sprite.src(spin.png)' style='position: absolute; left: 12px; top: 160px'/>"
            ]
          },
          {
            "#type": "variable",
            "name": "class_name",
            "value": {
              "#type": "expression",
              "value": "[]"
            }
          },
          {
            "#type": "variable",
            "name": "angle",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < part"
            },
            "do": [
              {
                "#type": "variable",
                "name": "class_name[i]",
                "value": {
                  "#type": "expression",
                  "value": "'angle' + i"
                }
              },
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<style>\n   .${class_name[i]} {\n     position: absolute;\n        -ms-transform: rotate(${angle}deg);\n        /* IE 9 */\n        -webkit-transform: rotate(${angle}deg);\n        /* Safari */\n        transform: rotate(${angle}deg);\n        /* Standard syntax */\n    }\n</style>\n<div class=${class_name[i]}>\n<img src='@sprite.src(1-${part}${spiner[i]}.png)'/>\n  </div>"
                ]
              },
              {
                "#type": "variable",
                "name": "angle",
                "value": {
                  "#type": "expression",
                  "value": "angle + 360/part"
                }
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<img src='@sprite.src(point.png)' style='position: absolute; left: 62px; top: 40px'/>\n  \n  </div></center>"
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</br></br> <span style='background: rgb(250, 250, 250, 0.3)'>\n  \nNumber of <b>${re_color(qes_color)}</b> part${num_color[0] <2 ? '':'s'}: <b>${num_color[0]}</b>.</b></br>\n\nTotal equal parts: <b>${part}</b></br>\n\n<b>${num_color[0]}</b> out of <b>${part}</b> chances.</br>\n\nThe chances of the spinner landing on ${re_color(qes_color)} colour is \"<b>${answer}</b>\".</span>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          }
        ]
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="h9eu_v7yd(YUK8|N_57y" x="0" y="0">
    <field name="name">Y1.SP.CH.WHWH.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="]X^0%s$4TN}}a63Cc`:^">
        <field name="name">image_path</field>
        <value name="value">
          <block type="string_value" id="0]N!Kcvmv78-1z,K0~KB">
            <field name="value">Develop/ImageQAs/Y1.SP.CH.WHWH.1B/</field>
          </block>
        </value>
        <next>
          <block type="variable" id="8#D=YeFOq=CS+6vBkYxQ">
            <field name="name">background</field>
            <value name="value">
              <block type="string_array" id="rOI5/2vK`1U@23rh`Ek+s">
                <field name="items">bg1|bg2|bg3|bg4</field>
              </block>
            </value>
            <next>
              <block type="variable" id="e`;LG$E;Ul8.q^TrUt#E">
                <field name="name">drop_background</field>
                <value name="value">
                  <block type="random_one" id=")ONXBQ@2)M1f9|;cnO~u7">
                    <value name="items">
                      <block type="expression" id="vH0s~U:53qH.oPBX$m9|">
                        <field name="value">background</field>
                      </block>
                    </value>
                  </block>
                </value>
                <next>
                  <block type="background_shape" id="MWn3EV0_f%GfT;^gqu!)">
                    <statement name="#props">
                      <block type="prop_image_key" id="].L@1cD=FY4MYn52y3e[~">
                        <field name="#prop"></field>
                        <value name="key">
                          <block type="string_value" id="OFDR|n.N793c8XFc=qD-">
                            <field name="value">${drop_background}.png</field>
                          </block>
                        </value>
                        <next>
                          <block type="prop_image_src" id="]gRYqF=bWmZ).N8dJ@1Z$">
                            <field name="#prop"></field>
                            <value name="src">
                              <block type="string_value" id="aP/JE=GV:4l(_E9vtk{f">
                                <field name="value">${image_path}${drop_background}.png</field>
                              </block>
                            </value>
                          </block>
                        </next>
                      </block>
                    </statement>
                    <next>
                      <block type="variable" id="sKjBDwP+gk)}@1wsj@2r|.">
                        <field name="name">type</field>
                        <value name="value">
                          <block type="random_number" id="c7kfANhh;Q7qo,@19-CD#">
                            <value name="min">
                              <block type="expression" id="5?B#F..^@2Y@2lPaVKdK|@2">
                                <field name="value">1</field>
                              </block>
                            </value>
                            <value name="max">
                              <block type="expression" id="q[z(F)AS)|hqe[{bÊ{">
                                <field name="value">2</field>
                              </block>
                            </value>
                          </block>
                        </value>
                        <next>
                          <block type="input_param" id="xAl{^THC7-9|RHW:v!u~" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="xePe.o?Srstp2[(:-K8a">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="gLs`q:AE|q=i/QE27AXK" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="`IrPg,OjgQE{J[m~#{,7">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="Q6#2-|SwVM:VZAoKIvoT">
                                    <field name="name">range</field>
                                    <value name="value">
                                      <block type="expression" id="_mOr`{^q1jdI=@2R{S5$4">
                                        <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="statement" id="?1z?z6TM@2+L|:z8BF+v#">
                                        <field name="value">function re_color(color) {
  if(color == 'g'){return 'green';}
  if(color == 'r'){return 'red';}
  if(color == 'b'){return 'blue';}
  if(color == 'y'){return 'yellow';}
}
                                        </field>
                                        <next>
                                          <block type="if_then_block" id="XD]k5j@2eCP9rYf$AzHEX">
                                            <value name="if">
                                              <block type="expression" id="GcI0wTK8]P8g?e/L8E;g">
                                                <field name="value">type == 1</field>
                                              </block>
                                            </value>
                                            <statement name="then">
                                              <block type="variable" id="bq61{z5gdz(/:bkG_}-D">
                                                <field name="name">tittle</field>
                                                <value name="value">
                                                  <block type="random_one" id="n_P?q:}?u)v(|u:H-EHH">
                                                    <value name="items">
                                                      <block type="expression" id="Es9b0/O[kB@2pJ8@2pr/vz">
                                                        <field name="value">['How likely are you going to', 'What are the chances you will', 'What is the probability that you will']</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="if_then_else_block" id="M^;~ioODJVw]I@2nfYjt-">
                                                    <value name="if">
                                                      <block type="expression" id="qF/.:zT6s1wJ+!KP,0uV">
                                                        <field name="value">range &lt; 4</field>
                                                      </block>
                                                    </value>
                                                    <statement name="then">
                                                      <block type="variable" id="bHE=^r5HSia@2%@2FQl2@23">
                                                        <field name="name">total</field>
                                                        <value name="value">
                                                          <block type="random_number" id="J(L8,CC0o)6QWm:2-B8=">
                                                            <value name="min">
                                                              <block type="expression" id="q|O?%V]}9QWpjPiDz?d,">
                                                                <field name="value">3</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="%zJ!AwI:eKn@2R45RSaM;">
                                                                <field name="value">10</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="ArE+8zBjI+-587,o_@2$m">
                                                            <field name="name">num</field>
                                                            <value name="value">
                                                              <block type="expression" id="itHL8C(S4cl2g=-17C5=">
                                                                <field name="value">2</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </statement>
                                                    <statement name="else">
                                                      <block type="variable" id="guf9wjx}SU97@1~U_Zq7p">
                                                        <field name="name">total</field>
                                                        <value name="value">
                                                          <block type="random_number" id="flxS3Uk#Ej+m1,i`OW_E">
                                                            <value name="min">
                                                              <block type="expression" id="Y{m%8|Eb#soFJ?Dl/5At">
                                                                <field name="value">10</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="Es/`gdIGEh-/m|N:Tj?]">
                                                                <field name="value">16</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="A~cvU$Pq(`bR{#VHBOy9">
                                                            <field name="name">num</field>
                                                            <value name="value">
                                                              <block type="random_number" id="Q2y‘OH+$Brx{ZXa=f.">
                                                                <value name="min">
                                                                  <block type="expression" id="90#,70Pxn5vc,gQdM@1!K">
                                                                    <field name="value">3</field>
                                                                  </block>
                                                                </value>
                                                                <value name="max">
                                                                  <block type="expression" id="6F{X,qY6ad`a,?Lqn}C6">
                                                                    <field name="value">4</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </statement>
                                                    <next>
                                                      <block type="variable" id="d4Jq1zRn@1tSw?_(}]{{O">
                                                        <field name="name">an</field>
                                                        <value name="value">
                                                          <block type="string_array" id="i;+8P{~AdRY5(;DBBok{">
                                                            <field name="items">a|a|a|a|an|a</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id=";;t,IhB@1[DgrtwYa-O_@1">
                                                            <field name="name">list_item</field>
                                                            <value name="value">
                                                              <block type="string_array" id="h=f9f1H8@2GxRHx?oS+S]">
                                                                <field name="items">heart|star|triangle|square|octagon|diamond</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="variable" id="rozn|14vrh9}hxntutnJ">
                                                                <field name="name">list_items</field>
                                                                <value name="value">
                                                                  <block type="string_array" id="mpFkf)2H4@2~$}Y_76Iva">
                                                                    <field name="items">hearts|stars|triangles|squares|octagons|diamonds</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="variable" id="?W@1q)mNuZboH5Yfl]wy|">
                                                                    <field name="name">color</field>
                                                                    <value name="value">
                                                                      <block type="string_array" id="MUF6UoPgEaXoW(uP04mR">
                                                                        <field name="items">ligth blue|purple|pink|orange|blue|green|yellow|red</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="variable" id="^byjFkxHbP`_p[X%LlM@1">
                                                                        <field name="name">item</field>
                                                                        <value name="value">
                                                                          <block type="random_many" id="cheM^`B(c}^]lRp[/}[@2">
                                                                            <value name="count">
                                                                              <block type="expression" id="2-$@1YHj_-I3jt$4%5@1oX">
                                                                                <field name="value">num</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="items">
                                                                              <block type="expression" id="tv8}2p9SGVCK1E/qkK@2J">
                                                                                <field name="value">[0, 1, 2, 3, 4, 5]</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="YY)?-MKUQnfPv+ZnZesS">
                                                                            <field name="name">num_item</field>
                                                                            <value name="value">
                                                                              <block type="expression" id=".h;pVFOkx9DbfZ.=ts+O">
                                                                                <field name="value">[]</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="variable" id="kCi1;vJ$AZk1B2/1_61B">
                                                                                <field name="name">answer</field>
                                                                                <value name="value">
                                                                                  <block type="random_one" id="oIdM+{9r8/@1V@2w#{aoK5">
                                                                                    <value name="items">
                                                                                      <block type="expression" id="|oAI0(.@1lcvC9ARPsfa2">
                                                                                        <field name="value">["will happen", "probably", "might happen", "won't happen"]</field>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="variable" id="lO%I@1%Mr,`:dHI(Rip~]">
                                                                                    <field name="name">num_item</field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id=",;Z0|LDlhkX}R8-rQ}mk">
                                                                                        <field name="value">[]</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="if_then_else_block" id="W5hf|,)skAcC%KM^Bc,8">
                                                                                        <value name="if">
                                                                                          <block type="expression" id="Eym,Y3t/1uPCrUAf_!ur">
                                                                                            <field name="value">answer == "won't happen"</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <statement name="then">
                                                                                          <block type="variable" id="bHkw[t@1Q=BaL%Mi8EI^w">
                                                                                            <field name="name">num_item[0]</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="1Nk!lML|:Xa%lS{}M;`{">
                                                                                                <field name="value">0</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="variable" id=";d%zWUi`Dvv)=4u@22k1O">
                                                                                                <field name="name">min</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="H9!@2,|Cf,Gf8d@1#%mHN$">
                                                                                                    <field name="value">1</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <statement name="else">
                                                                                          <block type="if_then_else_block" id="oC{nI{QOYf|::Hdy`lLN">
                                                                                            <value name="if">
                                                                                              <block type="expression" id="P2f~LLg:9VqDTf,oH[2S">
                                                                                                <field name="value">answer == "might happen"</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <statement name="then">
                                                                                              <block type="variable" id="8Uo7Y8ptVL.ll(Q=CU1~">
                                                                                                <field name="name">num_item[0]</field>
                                                                                                <value name="value">
                                                                                                  <block type="random_number" id="gTr`f$)#@1tnGqVJF@2E|J">
                                                                                                    <value name="min">
                                                                                                      <block type="expression" id="L!09LG#Ao,_g~]AjdX;m">
                                                                                                        <field name="value">1</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="max">
                                                                                                      <block type="expression" id="S]vue.b5EA93c@1$NvRWC">
                                                                                                        <field name="value">Math.floor(total/2)-1</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="variable" id=";%LGzP1L$EsUITpxj^Kc">
                                                                                                    <field name="name">min</field>
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="IZ@2$sd,Wy!yyHVLJOMl@2">
                                                                                                        <field name="value">1</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <statement name="else">
                                                                                              <block type="if_then_else_block" id="x}FTefEughkpf7m{81qz">
                                                                                                <value name="if">
                                                                                                  <block type="expression" id="KL@1$i`]vC@2Cy{HUr75=H">
                                                                                                    <field name="value">answer == "probably"</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <statement name="then">
                                                                                                  <block type="variable" id=".LNtVZf~)](vaLz31S3;">
                                                                                                    <field name="name">num_item[0]</field>
                                                                                                    <value name="value">
                                                                                                      <block type="random_number" id="_xK4A+K3LNh#6N4}yeoH">
                                                                                                        <value name="min">
                                                                                                          <block type="expression" id="s(us`Q_GfM9fUi,W!{5r">
                                                                                                            <field name="value">Math.floor(total/2)+1</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="max">
                                                                                                          <block type="expression" id="|vu03^))2Y6Z:@2A%iMA]">
                                                                                                            <field name="value">total-1</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="variable" id="21}k7},dZV{qUg+#SIJh">
                                                                                                        <field name="name">min</field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id="Jb;Iwyl0mm7d0hsnyeb|">
                                                                                                            <field name="value">1</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <statement name="else">
                                                                                                  <block type="variable" id="X:PMQ3^?Tz`tAKle-2,6">
                                                                                                    <field name="name">num_item[0]</field>
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="/l/tf~rEM_NLZ2:Qj|^X">
                                                                                                        <field name="value">total</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="variable" id=";KU4|%x3]-JV3?ajg}]n">
                                                                                                        <field name="name">min</field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id=")cZiinh+Tg~+$)#1@1.4S">
                                                                                                            <field name="value">0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </statement>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="if_then_else_block" id="~-${t]h6nYovp7!t^mnw">
                                                                                            <value name="if">
                                                                                              <block type="expression" id="Sz2(^8H)wcqLhrV:JC-5">
                                                                                                <field name="value">total &lt;= 10</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <statement name="then">
                                                                                              <block type="variable" id="I_;@2jA,_2r@2z7lN4]5dI">
                                                                                                <field name="name">grid</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="a2fZi^e]AF[@19iqG--u@2">
                                                                                                    <field name="value">4</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <statement name="else">
                                                                                              <block type="variable" id="L-v|ATgF+|afgDEgPl?J">
                                                                                                <field name="name">grid</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="w@1S+8yf.%u#t#W2?(KzZ">
                                                                                                    <field name="value">5</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="variable" id="(MO7F@2]F(PwI{y33Z;7e">
                                                                                                <field name="name">i</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="BU5OjnLML6@2V+}$XVtU!">
                                                                                                    <field name="value">1</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="variable" id="4)^%xU5F_PK[Ew`^nN}m">
                                                                                                    <field name="name">temp</field>
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="DpdrvZtV)~KD+9,QAlF!">
                                                                                                        <field name="value">total - num_item[0]</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="while_do_block" id="|@1Z|@28/pafKchI]aBZs-">
                                                                                                        <value name="while">
                                                                                                          <block type="expression" id="UfZcWN$newiEgRt}U1gj">
                                                                                                            <field name="value">i &lt; num - 1</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <statement name="do">
                                                                                                          <block type="variable" id="-OsCTXo9.(Bb.Een/uKG">
                                                                                                            <field name="name">num_item[i]</field>
                                                                                                            <value name="value">
                                                                                                              <block type="random_number" id=".1788K$ELlPpbtHgaQtf">
                                                                                                                <value name="min">
                                                                                                                  <block type="expression" id="`pA8Z@1w$51_U(p0sq)[7">
                                                                                                                    <field name="value">min</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="max">
                                                                                                                  <block type="expression" id="37$2o[]bGK,y75ze6em~">
                                                                                                                    <field name="value">temp - num + i + 1</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="variable" id="YI=.(a4Z)TfVf9-}sI0;">
                                                                                                                <field name="name">temp</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="HfYex0ALw|hU,#Gx.kB6">
                                                                                                                    <field name="value">temp - num_item[i]</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="variable" id="ny=HvzT~;#D,v@24+neW4">
                                                                                                                    <field name="name">i</field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="rb64:DVB]:RO?j0wNwHh">
                                                                                                                        <field name="value">i+1</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="variable" id="#pi=R5`y/Gsem.[Z]KW{">
                                                                                                            <field name="name">num_item[i]</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="uwE25l|eMc,z|-Hq(uHb">
                                                                                                                <field name="value">temp</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="variable" id="!mB]/i0@28]T)Zg^c}fM0">
                                                                                                                <field name="name">i</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="s@1!]7}+VAwCEx=We-PvL">
                                                                                                                    <field name="value">0</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="variable" id="ka-$xS%+XD!Hq`RFB6X_">
                                                                                                                    <field name="name">list</field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="IgdUZ;}f%gcrIr[Pe.jA">
                                                                                                                        <field name="value">[]</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="variable" id=")BFdaJ=6WA~gQee=[4JL">
                                                                                                                        <field name="name">co</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id="TV3l4_V]yW`a4@1|dqs8q">
                                                                                                                            <field name="value">[]</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="variable" id="LfZF3BCyvNQJ8EA^FJ;@1">
                                                                                                                            <field name="name">n</field>
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="jk#2G!`-YVkqek6L[nq`">
                                                                                                                                <field name="value">0</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="while_do_block" id=":a!!sv0v(@2Jd(QuTT1Wh">
                                                                                                                                <value name="while">
                                                                                                                                  <block type="expression" id="4osxP}iIY,aE~!u!$u79">
                                                                                                                                    <field name="value">i &lt; num</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <statement name="do">
                                                                                                                                  <block type="variable" id="bHJ5nRVp1t}?G`MOl?P#">
                                                                                                                                    <field name="name">j</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="=n%xvo@1$lYDv`FsIB|yO">
                                                                                                                                        <field name="value">0</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="while_do_block" id="u;1W!9K?f5M#ZEW-V;NJ">
                                                                                                                                        <value name="while">
                                                                                                                                          <block type="expression" id="YF?Xq9i$tlPr#FJ`5N~#">
                                                                                                                                            <field name="value">j &lt; num_item[i]</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <statement name="do">
                                                                                                                                          <block type="variable" id="06+TDYcFzH^}B@1vZb9@2m">
                                                                                                                                            <field name="name">list[n]</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id="_@2u|O1jmfERR)mei7),k">
                                                                                                                                                <field name="value">item[i]</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="variable" id="|JDHjt%xcCAN2rm?ak]e">
                                                                                                                                                <field name="name">co[n]</field>
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="random_number" id="PEYDTzEWxD1xLg@1CoFNm">
                                                                                                                                                    <value name="min">
                                                                                                                                                      <block type="expression" id="84zT@2z3![T|o`]r~BgRc">
                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="max">
                                                                                                                                                      <block type="expression" id=".=1N1cCaSNL+A/[r)Akv">
                                                                                                                                                        <field name="value">7</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="variable" id="P[dV,/xe$nO:Xz[D$BDR">
                                                                                                                                                    <field name="name">n</field>
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="expression" id="xi[Emoew’st1-VJ_).">
                                                                                                                                                        <field name="value">n+1</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="variable" id="ml,cIYOlv5?CR;VYb=K(">
                                                                                                                                                        <field name="name">j</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="(_2,RisSf8c~%6L;zlC6">
                                                                                                                                                            <field name="value">j+1</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <next>
                                                                                                                                          <block type="variable" id="j:s^1_`,Sl;.2o@2y/F!%">
                                                                                                                                            <field name="name">i</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id=":}gPyzL!]6Ge8cj@1Vr9.">
                                                                                                                                                <field name="value">i+1</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <next>
                                                                                                                                  <block type="variable" id="DM`]1VL~{SC78DkZp{3+">
                                                                                                                                    <field name="name">i</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="6$O3hBcK-qpn@1v:l/g?8">
                                                                                                                                        <field name="value">total</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="while_do_block" id="iMz0:1N}=5hH.-J2TA@1b">
                                                                                                                                        <value name="while">
                                                                                                                                          <block type="expression" id="+@1BOiS}^?BrAn1SRDw,Q">
                                                                                                                                            <field name="value">i &lt; grid@2grid</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <statement name="do">
                                                                                                                                          <block type="variable" id="iYpA^f)gHmf#=)0N0X0R">
                                                                                                                                            <field name="name">list[i]</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id="D6^ssWf?4edaZ}/t;hvI">
                                                                                                                                                <field name="value">-1</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="variable" id="VgRQF4j4k=mIFs1vl.+i">
                                                                                                                                                <field name="name">i</field>
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="expression" id="cgXD)msJS`Aa8}JZ=52-">
                                                                                                                                                    <field name="value">i+1</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <next>
                                                                                                                                          <block type="variable" id="aMMC=gO7KZ-x,++T)DhT">
                                                                                                                                            <field name="name">list</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="func_shuffle" id="2LD]EA^occIfV)$oJ,!R" inline="true">
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="expression" id="iEo]^XE0|/p%_?9vF+kJ">
                                                                                                                                                    <field name="value">list</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="variable" id="i`7suY+]Z)=iNA9u@2ub~">
                                                                                                                                                <field name="name">select</field>
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="random_one" id="@1n,rMZ8hPj;+2/`_P~2$">
                                                                                                                                                    <value name="items">
                                                                                                                                                      <block type="expression" id="Hrbw1#vdG73JDA_$[64B">
                                                                                                                                                        <field name="value">['grab', 'choose', 'select']</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="if_then_else_block" id="ZjRPfNGDe@2H3`IulVAT}">
                                                                                                                                                    <value name="if">
                                                                                                                                                      <block type="expression" id="m(Anj2LSQGM;CSU6xmmk">
                                                                                                                                                        <field name="value">select == 'choose'</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <statement name="then">
                                                                                                                                                      <block type="variable" id="Z2omyn/^_-hzTvpNy3!{">
                                                                                                                                                        <field name="name">selecting</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="OSXBT?-Zno~bQWgmFX`^">
                                                                                                                                                            <field name="value">'choosing'</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <statement name="else">
                                                                                                                                                      <block type="if_then_else_block" id="$IHhWI7(:47+P8^Ft$hV">
                                                                                                                                                        <value name="if">
                                                                                                                                                          <block type="expression" id="%pkUy+Pwbm5.oSQU1EQ~">
                                                                                                                                                            <field name="value">select == 'grab'</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <statement name="then">
                                                                                                                                                          <block type="variable" id="-:dma+fjTy8ww[wy@2H1)">
                                                                                                                                                            <field name="name">selecting</field>
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id="NWnezr$[Ii/Lz1C=Ik+/">
                                                                                                                                                                <field name="value">'grabbing'</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                        <statement name="else">
                                                                                                                                                          <block type="variable" id="fg#nU3D.HBELq@29A0HD^">
                                                                                                                                                            <field name="name">selecting</field>
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id="IKt5,CC43M;DFl5.[oRn">
                                                                                                                                                                <field name="value">'selecting'</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="text_shape" id="yMo8AD^jK@1T6K]Dh-sul">
                                                                                                                                                        <statement name="#props">
                                                                                                                                                          <block type="prop_position" id="e{Hr5%cP(nwIxCeBal)F">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="x">
                                                                                                                                                              <block type="expression" id="+J7B@1|VgX0g/jP}E%bsX">
                                                                                                                                                                <field name="value">400</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="y">
                                                                                                                                                              <block type="expression" id="UVs+)T@28c#ab8td7%1m@1">
                                                                                                                                                                <field name="value">40</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_text_contents" id="fkl{-C4j:8aW180hX+}z">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="contents">
                                                                                                                                                                  <block type="string_value" id="W(H9[80V`.L#VUk]oi]p">
                                                                                                                                                                    <field name="value">${tittle} ${select} ${an[item[0]]} ${list_item[item[0]]} at random?</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_text_style" id="wR3,3_~isO1bDx=06X{4">
                                                                                                                                                                    <field name="base">text</field>
                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                      <block type="prop_text_style_font_size" id="JOÃ‘Q:a=f!4mHB(UiNb">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="fontSize">
                                                                                                                                                                          <block type="expression" id="3jZj0%YP3V/2qd`X!-?M">
                                                                                                                                                                            <field name="value">35</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_text_style_fill" id="^H{c[20-O[!FD+=WnmIt">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="fill">
                                                                                                                                                                              <block type="string_value" id="agODYXn7|-9IHCI!?y0!">
                                                                                                                                                                                <field name="value">black</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_text_style_stroke" id="/Bq0z}wLZQ96xP4M`l?r">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="stroke">
                                                                                                                                                                                  <block type="string_value" id=";nd+0sBOX3Q+V!i|Czk3">
                                                                                                                                                                                    <field name="value">white</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_text_style_stroke_thickness" id="-rv3pC9dx10=#HE5cfZ|">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="strokeThickness">
                                                                                                                                                                                      <block type="expression" id="A,Jup=2y+yv523,G6/|1">
                                                                                                                                                                                        <field name="value">2</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="image_shape" id="V3+BfAlDvn{3QdC9Bt#%">
                                                                                                                                                            <statement name="#props">
                                                                                                                                                              <block type="prop_position" id="N6Tx/6Al+;1Rn_`0zUnI">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="x">
                                                                                                                                                                  <block type="expression" id="^0KYha!_+hvE,DA[}Wqw">
                                                                                                                                                                    <field name="value">400</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <value name="y">
                                                                                                                                                                  <block type="expression" id="}sgD,Xda-2}SgYjx{R%;">
                                                                                                                                                                    <field name="value">220</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_size" id="fdmeY-}MY-`P~+wEVm{G">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="width">
                                                                                                                                                                      <block type="expression" id=";)$;uU)ddVZ?$b+.ZfqH">
                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <value name="height">
                                                                                                                                                                      <block type="expression" id="{Q_X[}l[=_AZ1UV2{9UN">
                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_image_key" id="gXHiY%#1)E]c|NEqm@1r3">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="key">
                                                                                                                                                                          <block type="string_value" id=";M9r)s65c]AGm%nS{}HM">
                                                                                                                                                                            <field name="value">bottle.png</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_image_src" id="G2DWEMC4Hrr|xed%#aS/">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="src">
                                                                                                                                                                              <block type="string_value" id="1{G]]yHQPu48QrJK~k|`">
                                                                                                                                                                                <field name="value">${image_path}bottle.png</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="variable" id="AL~pk:D)Em3hl:%5+O};">
                                                                                                                                                                <field name="name">count</field>
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="expression" id="NI7G$KF0_a5w#I@2uT7xu">
                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="grid_shape" id="RE4mR^@2}s/xKi-;T.2#=">
                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                      <block type="prop_grid_dimension" id=";@1kSm;CG^{$K5-H;01;^">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="rows">
                                                                                                                                                                          <block type="expression" id=".RJs]MpS8+D^@2o(f!pz(">
                                                                                                                                                                            <field name="value">grid</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <value name="cols">
                                                                                                                                                                          <block type="expression" id="do,0F.Ku0=m8O|BvbhI5">
                                                                                                                                                                            <field name="value">grid</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_position" id="%G/Y0cTo@1_3RMKBlav[[">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="x">
                                                                                                                                                                              <block type="expression" id=";lP$bpiE~4CXxi_JujX/">
                                                                                                                                                                                <field name="value">400</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="y">
                                                                                                                                                                              <block type="expression" id="G7^|9L{qhA@2Hze^X{Wj#">
                                                                                                                                                                                <field name="value">250</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_size" id="8Dj(R{t0ezG2mG$L#V{A">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="width">
                                                                                                                                                                                  <block type="expression" id="(%LT6(7q@1Ekl.O1xL_i/">
                                                                                                                                                                                    <field name="value">160</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <value name="height">
                                                                                                                                                                                  <block type="expression" id=")c3R$yz[tmT_=%c)xrnD">
                                                                                                                                                                                    <field name="value">160</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_grid_cell_source" id="brI@2@2mKg]3#LUVJmQ-I8">
                                                                                                                                                                                    <field name="#prop">cell.source</field>
                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                      <block type="func_array_of_number" id="^X)@2kKC?;5?]Di.#esfS" inline="true">
                                                                                                                                                                                        <value name="items">
                                                                                                                                                                                          <block type="expression" id="y%9sl!AZA?h2lsX:xYcf">
                                                                                                                                                                                            <field name="value">grid@2grid</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <value name="from">
                                                                                                                                                                                          <block type="expression" id="e1Mx~poC_;WtN0Mu/@2-g">
                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_anchor" id="[C?Moj[J)GwEsv]pX+{)">
                                                                                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                                                                                        <value name="x">
                                                                                                                                                                                          <block type="expression" id="zu,K0P67%^@2$43TX`-#_">
                                                                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <value name="y">
                                                                                                                                                                                          <block type="expression" id="wDP+ny;vyNqEYe5Yk~%6">
                                                                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_grid_show_borders" id="Aj4!Fad,DPZQb;k@2Lg)]">
                                                                                                                                                                                            <field name="#prop">#showBorders</field>
                                                                                                                                                                                            <field name="value">FALSE</field>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_grid_random" id="V]WaCoi`L5hdAzDrYkkh">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <field name="random">FALSE</field>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="prop_grid_cell_template_for" id="-BSylpWxCiD?$B]uR7h^">
                                                                                                                                                                                                    <field name="variable">$cell</field>
                                                                                                                                                                                                    <field name="condition">list[$cell.data] &gt;= 0</field>
                                                                                                                                                                                                    <field name="#prop">cell.templates[]</field>
                                                                                                                                                                                                    <field name="#callback">$cell</field>
                                                                                                                                                                                                    <statement name="body">
                                                                                                                                                                                                      <block type="variable" id="=Pix[dh1sTvklMpc:x5m">
                                                                                                                                                                                                        <field name="name">ox</field>
                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                          <block type="random_number" id="rQHH$lJ5SKKqVuzh4p(h">
                                                                                                                                                                                                            <value name="min">
                                                                                                                                                                                                              <block type="expression" id="|gF^dOQ1=$VX|OuNR3xo">
                                                                                                                                                                                                                <field name="value">-5</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                            <value name="max">
                                                                                                                                                                                                              <block type="expression" id="fMgo,XeXEq-np@1R8bos;">
                                                                                                                                                                                                                <field name="value">5</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="variable" id="UwYV~Bh)VI1b2X%]NP{[">
                                                                                                                                                                                                            <field name="name">oy</field>
                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                              <block type="random_number" id="oQ6U3K_k3_WrUr6k;Z@1v">
                                                                                                                                                                                                                <value name="min">
                                                                                                                                                                                                                  <block type="expression" id=".4SkA3@1{Z0ld%lXG1?b+">
                                                                                                                                                                                                                    <field name="value">-5</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                <value name="max">
                                                                                                                                                                                                                  <block type="expression" id="|tpW=m{tqnDt:M^YZ4Kq">
                                                                                                                                                                                                                    <field name="value">5</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                            <next>
                                                                                                                                                                                                              <block type="image_shape" id=",QXG11i1.(h,w7KtSZ^l">
                                                                                                                                                                                                                <statement name="#props">
                                                                                                                                                                                                                  <block type="prop_position" id="X4@1jM[2Q/LlU|FGW1Hf[">
                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                    <value name="x">
                                                                                                                                                                                                                      <block type="expression" id="#h1+LZA5_kCie|UOx$pl">
                                                                                                                                                                                                                        <field name="value">$cell.centerX+ox</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                    <value name="y">
                                                                                                                                                                                                                      <block type="expression" id="E-A`uOVK![;D^3h4ILwM">
                                                                                                                                                                                                                        <field name="value">$cell.centerY+oy</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                      <block type="prop_size" id="cKA3,sxnjFi93uM?8!;7">
                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                        <value name="width">
                                                                                                                                                                                                                          <block type="expression" id=")!|Y}t{htRivGyQ_BG8a">
                                                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                        <value name="height">
                                                                                                                                                                                                                          <block type="expression" id="dJ$MJrV|.hHy_Qm~970,">
                                                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                          <block type="prop_max_size" id="-,9c94pLy_1~YS|TkZC|">
                                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                                            <value name="maxWidth">
                                                                                                                                                                                                                              <block type="expression" id="~qJw8xK3)hH8_nwK-le2">
                                                                                                                                                                                                                                <field name="value">$cell.width</field>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                            <value name="maxHeight">
                                                                                                                                                                                                                              <block type="expression" id="_u.t~kdYxcN)MM!~gdb]">
                                                                                                                                                                                                                                <field name="value">$cell.height</field>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                              <block type="prop_image_key" id="3NGXd([9UC=`lx30Rd%`">
                                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                                <value name="key">
                                                                                                                                                                                                                                  <block type="string_value" id="k02Q(O@1X;J^5UJZ/ztn+">
                                                                                                                                                                                                                                    <field name="value">${list[$cell.data]}_${co[count]}.png</field>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                  <block type="prop_image_src" id="nHY@1z@1ixv~u)(z|XIVuD">
                                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                                    <value name="src">
                                                                                                                                                                                                                                      <block type="string_value" id="yegpgM,pOWZfV/?xi)3+">
                                                                                                                                                                                                                                        <field name="value">${image_path}${list[$cell.data]}_${co[count]}.png</field>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                      <block type="prop_scale" id="[1SfM@1Mz6|}o{v7_n(D?">
                                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                                        <value name="scale">
                                                                                                                                                                                                                                          <block type="expression" id="X_z0jXLldKL{vY.wd;,:">
                                                                                                                                                                                                                                            <field name="value">0.95</field>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                  <block type="variable" id="Sjwh@2Gp7t:Tb{2dO35AE">
                                                                                                                                                                                                                    <field name="name">count</field>
                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                      <block type="expression" id="w@21i9KvjT#+y9[=,-+2V">
                                                                                                                                                                                                                        <field name="value">count +1</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </next>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </next>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="grid_shape" id="]MUfeqa9L@2sK)05w,sC8">
                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                          <block type="prop_grid_dimension" id="7sU.bZ9Mii[z64[EP4tM">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="rows">
                                                                                                                                                                              <block type="expression" id="Elzq/GHTtJaxMNz~TdpX">
                                                                                                                                                                                <field name="value">1</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="cols">
                                                                                                                                                                              <block type="expression" id="5c-ig_:U2+5jtVm)k88q">
                                                                                                                                                                                <field name="value">4</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_position" id="(O::Jq?wSXwPdR)l0jLG">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="x">
                                                                                                                                                                                  <block type="expression" id="I$@1^FzJIig,m)sUu_.9a">
                                                                                                                                                                                    <field name="value">400</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <value name="y">
                                                                                                                                                                                  <block type="expression" id="`R)@2BlzYyyEn]Fy[@1lm:">
                                                                                                                                                                                    <field name="value">400</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_size" id="o8D#-_zTs=gnlo(4cOE[">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="width">
                                                                                                                                                                                      <block type="expression" id="6EHv#D/KW!?![~#S@2w#u">
                                                                                                                                                                                        <field name="value">750</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <value name="height">
                                                                                                                                                                                      <block type="expression" id="I5xz.AYp:l}V![y{:/+b">
                                                                                                                                                                                        <field name="value">50</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_grid_cell_source" id="a@1Z0+Sw$ci7(oUDqA0Ek">
                                                                                                                                                                                        <field name="#prop">cell.source</field>
                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                          <block type="expression" id="|a#:zGT9??2fr4ea4lP_">
                                                                                                                                                                                            <field name="value">["will happen", "probably", "might happen", "won't happen"]</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_anchor" id="2p35{NtD{.0lr-FXK%/x">
                                                                                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                                                                                            <value name="x">
                                                                                                                                                                                              <block type="expression" id="a4%-4p`(11/JF8vsC7n.">
                                                                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <value name="y">
                                                                                                                                                                                              <block type="expression" id="k2DEQ-!ybgq{P.[@2wMFU">
                                                                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_grid_show_borders" id="3yeifrB;pNa`MXM:|v1U">
                                                                                                                                                                                                <field name="#prop">#showBorders</field>
                                                                                                                                                                                                <field name="value">FALSE</field>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="prop_grid_random" id="=q;oA2Tls]~9;IX|A_8+">
                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                    <field name="random">TRUE</field>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="prop_grid_cell_template" id="@2Yl9o5CcinVoF]tN%rU^">
                                                                                                                                                                                                        <field name="variable">$cell</field>
                                                                                                                                                                                                        <field name="#prop">cell.template</field>
                                                                                                                                                                                                        <field name="#callback">$cell</field>
                                                                                                                                                                                                        <statement name="body">
                                                                                                                                                                                                          <block type="choice_custom_shape" id="}?rFTfV!G@2ti)OKIbwU7">
                                                                                                                                                                                                            <field name="action">click-one</field>
                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                              <block type="expression" id="Ebeo5_G-wm7I^~3.AoAn">
                                                                                                                                                                                                                <field name="value">$cell.data</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                            <statement name="template">
                                                                                                                                                                                                              <block type="image_shape" id="W9H(v0L~Rj},DZL?gFzM">
                                                                                                                                                                                                                <statement name="#props">
                                                                                                                                                                                                                  <block type="prop_position" id="PEok49pit|-qn.PDeJX?">
                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                    <value name="x">
                                                                                                                                                                                                                      <block type="expression" id="Dw2OCet$78?QRV0bo)g:">
                                                                                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                    <value name="y">
                                                                                                                                                                                                                      <block type="expression" id="}_djv}}Ax@2[46AY0ZQC[">
                                                                                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                      <block type="prop_size" id="mv+xd#_r(C/yO4Y6RF}c">
                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                        <value name="width">
                                                                                                                                                                                                                          <block type="expression" id="@1{gA,z@2xx/@2P[vaX1)^4">
                                                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                        <value name="height">
                                                                                                                                                                                                                          <block type="expression" id="Hdb7okz1BpXDfQjqDLWQ">
                                                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                          <block type="prop_image_key" id="zY4|1(|^-j%x6GIxYw%o">
                                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                                            <value name="key">
                                                                                                                                                                                                                              <block type="string_value" id="_R?H)_yYf?S$1W;66l!`">
                                                                                                                                                                                                                                <field name="value">${$cell.data[2]}.png</field>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                              <block type="prop_image_src" id="Maq0,[=8|Il4%?p7P{[U">
                                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                                <value name="src">
                                                                                                                                                                                                                                  <block type="string_value" id="5SxqhI+,:Y|-M/$F.pHh">
                                                                                                                                                                                                                                    <field name="value">${image_path}${$cell.data[2]}.png</field>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="func_add_answer" id="f2v3HoE,PHe?x;Ewg[%-" inline="true">
                                                                                                                                                                            <value name="value">
                                                                                                                                                                              <block type="expression" id="E@2Pxc3M0/Gz@1hwEw%H#M">
                                                                                                                                                                                <field name="value">answer</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="variable" id="jYJUy=8zp]J{!P,1SiGG">
                                                                                                                                                                                <field name="name">link</field>
                                                                                                                                                                                <value name="value">
                                                                                                                                                                                  <block type="expression" id="OR.;o@1?~y:MChR~j258c">
                                                                                                                                                                                    <field name="value">'http://starmathsonline.s3.amazonaws.com/' + image_path</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="partial_explanation" id=";hq1K[i8QGNa=T~|x0;v" inline="true">
                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                      <block type="string_value" id="9K52h%9MNGu4Sy$4c2zH">
                                                                                                                                                                                        <field name="value">&lt;u&gt;Step 1: Familiarise yourself with the meaning of "will happen", "probably", "might happen", "won't happen".&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;
&lt;style&gt;
  td{border: 1px solid black; text-align: center}
table{background: rgb(179, 218, 255, 0.5)}
&lt;/style&gt;
&lt;center&gt;
  &lt;table&gt;
    &lt;tr&gt;
      &lt;td style='text-align: center; width: 250px'&gt;&lt;b style=''&gt;Probability Term&lt;/b&gt;&lt;/td&gt;
      &lt;td style='text-align: center; width: 750px'&gt;&lt;b style=''&gt;Meaning&lt;/b&gt;&lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
      &lt;td style='text-align: center; width: 250px'&gt;&lt;span style=''&gt;Will happen&lt;/span&gt;&lt;/td&gt;
      &lt;td&gt;&lt;span style=''&gt;Event will surely happen, certain to take place.&lt;/span&gt;&lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
      &lt;td style='text-align: center; width: 250px'&gt;&lt;span style=''&gt;Probably&lt;/span&gt;&lt;/td&gt;
      &lt;td&gt;&lt;span style=''&gt;Event has high chance of happening, very likely to take place.&lt;/span&gt;&lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
      &lt;td style='text-align: center; width: 250px'&gt;&lt;span style=''&gt;Might happen&lt;/span&gt;&lt;/td&gt;
      &lt;td&gt;&lt;span style=''&gt;Event has high chance of happening, very likely to take place.&lt;/span&gt;&lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
      &lt;td style='text-align: center; width: 250px'&gt;&lt;span style=''&gt;Won't happen&lt;/span&gt;&lt;/td&gt;
      &lt;td&gt;&lt;span style=''&gt;Event has no chance of happening, impossible to take place.&lt;/span&gt;&lt;/td&gt;
    &lt;/tr&gt;
   &lt;/table&gt;
&lt;/center&gt;
                                                                                                                                                                                        </field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="end_partial_explanation" id="UE@2Mb@1S@2blcz)K1iLYgp">
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="partial_explanation" id="iFG%RGF?sN7~i:%6MQfL" inline="true">
                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                              <block type="string_value" id="VY5Mbshj_d%F=~{S5Ev0">
                                                                                                                                                                                                <field name="value">&lt;u&gt;Step 2: From the question, identify the item to focus on in the question.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;
&lt;span style='background: rgb(250, 250, 250, 0.5)'&gt;
"${tittle} ${select} ${an[item[0]]} &lt;b&gt;${list_item[item[0]]}?&lt;/b&gt;"&lt;/br&gt;&lt;/br&gt;

The item to focus on is the &lt;b&gt;${list_item[item[0]]}&lt;/b&gt;.&lt;/span&gt;
                                                                                                                                                                                                </field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="end_partial_explanation" id="4Hkw!+:jeZ1:B}E6rlQK">
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="partial_explanation" id="@1|OF^/cpKTMlchM36.QY" inline="true">
                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                      <block type="string_value" id="0b2!xhjRI@1Q9Xg+uJh0J">
                                                                                                                                                                                                        <field name="value">&lt;u&gt;Step 3: Identify the chances of ${selecting} ${an[item[0]]} ${list_item[item[0]]}.&lt;/u&gt;
&lt;/br&gt;&lt;/br&gt;
&lt;span style='background: rgb(250, 250, 250, 0.5)'&gt;
  
Number of ${list_item[item[0]]}${num_item[0] &lt; 2 ? '':'s'} : &lt;b&gt;${num_item[0]}&lt;/b&gt;&lt;/br&gt;

&lt;style&gt;
  #stroke{
   -webkit-text-stroke-width: 1px;
   -webkit-text-stroke-color: white;
}
  table {overflow-x:auto;}
  td{text-align: center; width: 50px; height: ${160/grid}px;}
#img {max-height: auto; max-width: ${160/grid}px; padding: 0; margin: 0; border: 0}
  &lt;/style&gt;
&lt;center&gt;
    &lt;div style='position: relative; width: 252px; height: 285px'&gt;
  &lt;img style='position: absolute; height: ${total &gt; 10 ? 140:120}%; left: 0px; top: 0px' src='${link}bottle.png'/&gt;
&lt;table style='position: absolute; top: ${115}px; left: ${50}px'&gt;
                                                                                                                                                                                                        </field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="variable" id="q}a4^,G,5Lb9^4qP]CQS">
                                                                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                          <block type="expression" id="s3^W@1_3^CGe_gJ]0XaEP">
                                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="variable" id="N?sdyhAvR7,`=qf@2d$IT">
                                                                                                                                                                                                            <field name="name">count</field>
                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                              <block type="expression" id="G|;(sXdq5F:!Zu@1ifx`W">
                                                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                            <next>
                                                                                                                                                                                                              <block type="while_do_block" id="v//=i@1=ReRJ?Gf8/YdD}">
                                                                                                                                                                                                                <value name="while">
                                                                                                                                                                                                                  <block type="expression" id=".mB2xczT6Fo)dG2T?J1~">
                                                                                                                                                                                                                    <field name="value">i &lt; grid</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                <statement name="do">
                                                                                                                                                                                                                  <block type="partial_explanation" id="U;/}eR7Rl!KRE9(JT8Wv" inline="true">
                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                      <block type="string_value" id="4:{aV(bv[`(sqK(P8Paw">
                                                                                                                                                                                                                        <field name="value">&lt;tr&gt;</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                      <block type="variable" id="ME7?)P~2d]5yV7yvh4t=">
                                                                                                                                                                                                                        <field name="name">j</field>
                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                          <block type="expression" id="i;$4SVg#d5TzrYwfaSF/">
                                                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                          <block type="while_do_block" id="[a~xBGOSKUB(QuP%:E[f">
                                                                                                                                                                                                                            <value name="while">
                                                                                                                                                                                                                              <block type="expression" id="d!?(^rr0;L,f~(e7qz@2O">
                                                                                                                                                                                                                                <field name="value">j &lt; grid</field>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                            <statement name="do">
                                                                                                                                                                                                                              <block type="if_then_else_block" id="X`iY_EM91UqsFYd$]sG5">
                                                                                                                                                                                                                                <value name="if">
                                                                                                                                                                                                                                  <block type="expression" id="[yQSfeqZ0|y,[GtK@1Gb?">
                                                                                                                                                                                                                                    <field name="value">list[i@2grid+j] &gt;= 0</field>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                <statement name="then">
                                                                                                                                                                                                                                  <block type="partial_explanation" id="qySEgc2_IOByu=7?9Mv0" inline="true">
                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                      <block type="string_value" id="(GS2[3{)/e-;VE4B0F+;">
                                                                                                                                                                                                                                        <field name="value">&lt;td&gt;&lt;img id='img' src='${link}${list[i@2grid+j]}_${co[count]}.png'/&gt;&lt;/td&gt;</field>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                      <block type="variable" id="yt,YctWWBTD;@1%ws$-7~">
                                                                                                                                                                                                                                        <field name="name">count</field>
                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                          <block type="expression" id="On@2Y7uMHSl$VSR}c1Rc|">
                                                                                                                                                                                                                                            <field name="value">count + 1</field>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                                                <statement name="else">
                                                                                                                                                                                                                                  <block type="partial_explanation" id="U94hXFpMG]h6rDJ-@1@2=T" inline="true">
                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                      <block type="string_value" id="Kl/bG:z^~~6ov@2Xw|QI(">
                                                                                                                                                                                                                                        <field name="value">&lt;td&gt;&lt;/td&gt;</field>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                  <block type="variable" id="me!(h5fX-R5WJ8BxdG-E">
                                                                                                                                                                                                                                    <field name="name">j</field>
                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                      <block type="expression" id="x1+nBE@1Sua^{^0T=oNCS">
                                                                                                                                                                                                                                        <field name="value">j+1</field>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                              <block type="partial_explanation" id="y3U#zJ`-n=2JCW],gE$m" inline="true">
                                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                                  <block type="string_value" id=".cq0V#g]A?^1^^WNsJqX">
                                                                                                                                                                                                                                    <field name="value">&lt;/tr&gt;</field>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                  <block type="variable" id="7/Z^.L0iqAU,iq{]GI1j">
                                                                                                                                                                                                                                    <field name="name">i</field>
                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                      <block type="expression" id="LWvaI75x~MXQq0,;79Hz">
                                                                                                                                                                                                                                        <field name="value">i+1</field>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                  <block type="partial_explanation" id="z$lA#wX0EY,$-0$B;Lvu" inline="true">
                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                      <block type="string_value" id=".gIf#yP8oy$/1PUe.U">
                                                                                                                                                                                                                        <field name="value">&lt;/table&gt;
&lt;table style='position: absolute; top: ${115}px; left: ${60}px'&gt;
                                                                                                                                                                                                                        </field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                      <block type="variable" id="}LU+m?LufbNz:j29UWHK">
                                                                                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                          <block type="expression" id=";U=KpmQi0XDY@2UjH,@1Un">
                                                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                          <block type="variable" id="T,Gzu8e``o:_Kq1Ml1Tb">
                                                                                                                                                                                                                            <field name="name">count</field>
                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                              <block type="expression" id="OBbe+a:npC6#m$N,_]6D">
                                                                                                                                                                                                                                <field name="value">1</field>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                              <block type="while_do_block" id="dv03}4rir(2z|@2_yppbl">
                                                                                                                                                                                                                                <value name="while">
                                                                                                                                                                                                                                  <block type="expression" id="@1}@2#jR({8Ix(f0nK-?Iw">
                                                                                                                                                                                                                                    <field name="value">i &lt; grid</field>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                <statement name="do">
                                                                                                                                                                                                                                  <block type="partial_explanation" id="pS#[aSAbRG[3)xk|~n0e" inline="true">
                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                      <block type="string_value" id="]B{U%R[S:%oe+eM%1.m/">
                                                                                                                                                                                                                                        <field name="value">&lt;tr&gt;</field>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                      <block type="variable" id="s9]KtLL-^8[L3HCn^TI,">
                                                                                                                                                                                                                                        <field name="name">j</field>
                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                          <block type="expression" id="h+Z9,Yzqo~rZ2}pZrQ/M">
                                                                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                          <block type="while_do_block" id="o5h/(WNIJR}~-xVMe/1y">
                                                                                                                                                                                                                                            <value name="while">
                                                                                                                                                                                                                                              <block type="expression" id="]5Vy_0!$@2ZC8aHJAJKl0">
                                                                                                                                                                                                                                                <field name="value">j &lt; grid</field>
                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                            <statement name="do">
                                                                                                                                                                                                                                              <block type="if_then_else_block" id="}v2P.hg+m0@1;Lz7(y?1z">
                                                                                                                                                                                                                                                <value name="if">
                                                                                                                                                                                                                                                  <block type="expression" id="Scg3#AuA_he!o1|z40yU">
                                                                                                                                                                                                                                                    <field name="value">list[i@2grid+j] == item[0]</field>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                <statement name="then">
                                                                                                                                                                                                                                                  <block type="partial_explanation" id="k)sUc)RQqj4KA#iHre(G" inline="true">
                                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                                      <block type="string_value" id="Y-fKj%,+]B-G06+m%t=l">
                                                                                                                                                                                                                                                        <field name="value">&lt;td&gt;&lt;span id='stroke' style='color: black'&gt;${count}&lt;/span&gt;&lt;/td&gt;</field>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                                      <block type="variable" id="w-kfZ`#E#k1XVORJ%7N$">
                                                                                                                                                                                                                                                        <field name="name">count</field>
                                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                                          <block type="expression" id="A@1P,i@1jrVJ{yR_T6Hs6:">
                                                                                                                                                                                                                                                            <field name="value">count + 1</field>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                                                                <statement name="else">
                                                                                                                                                                                                                                                  <block type="partial_explanation" id="tG5Rv:^z96kD=Ak(_Cc+" inline="true">
                                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                                      <block type="string_value" id="_gl58?[VC7cJ=-Df|t1r">
                                                                                                                                                                                                                                                        <field name="value">&lt;td&gt;&lt;/td&gt;</field>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                                  <block type="variable" id="h@2tJzbSbERzeucI,8e2g">
                                                                                                                                                                                                                                                    <field name="name">j</field>
                                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                                      <block type="expression" id="dm^UKH9i9CDTAQmna@2vk">
                                                                                                                                                                                                                                                        <field name="value">j+1</field>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                              <block type="partial_explanation" id="kV@1Kx{m~Nt2JUjt}B@1lK" inline="true">
                                                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                                                  <block type="string_value" id="Z}8D[W,Y%Nn^jvWA^y]F">
                                                                                                                                                                                                                                                    <field name="value">&lt;/tr&gt;</field>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                                  <block type="variable" id="X@17[%Bhr%JI}k)84nkSz">
                                                                                                                                                                                                                                                    <field name="name">i</field>
                                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                                      <block type="expression" id="(6-HuKMXID|@1CqLl0~Tt">
                                                                                                                                                                                                                                                        <field name="value">i+1</field>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                  <block type="partial_explanation" id="KdMBFDUihDXqM6|3pZDH" inline="true">
                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                      <block type="string_value" id="r(/LS47-^-W@1gJ=IH^5C">
                                                                                                                                                                                                                                        <field name="value">&lt;/table&gt;
&lt;/div&gt;
&lt;/center&gt;
&lt;/br&gt;
Total items: &lt;b&gt;${total}&lt;/b&gt;&lt;/br&gt;&lt;/br&gt;

&lt;b&gt;${num_item[0]}&lt;/b&gt; out of &lt;b&gt;${total}&lt;/b&gt; chances.&lt;/br&gt;&lt;/br&gt;

The chances of ${selecting} a shape is "&lt;b&gt;${answer}&lt;/b&gt;".&lt;/span&gt;
                                                                                                                                                                                                                                        </field>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </next>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </next>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </statement>
                                            <next>
                                              <block type="if_then_block" id="Mi(f[g^QJYMy)+55w[:}">
                                                <value name="if">
                                                  <block type="expression" id="vD~HZ4DS5o01gLS=_8gq">
                                                    <field name="value">type == 2</field>
                                                  </block>
                                                </value>
                                                <statement name="then">
                                                  <block type="if_then_else_block" id="^RDO;@2kUVp`jK@1K?J}R#">
                                                    <value name="if">
                                                      <block type="expression" id="7_V%^y~!08^IN%QG~_#k">
                                                        <field name="value">range &lt; 4</field>
                                                      </block>
                                                    </value>
                                                    <statement name="then">
                                                      <block type="variable" id="iR0u:=n1:]`z?i/aPY[s">
                                                        <field name="name">part</field>
                                                        <value name="value">
                                                          <block type="random_number" id="iYe0aT!dRenzIBZjM4LL">
                                                            <value name="min">
                                                              <block type="expression" id="r=E${a#^^VVtEz?y$rql">
                                                                <field name="value">4</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="HjY{fI0|7JLDx?rnYe?c">
                                                                <field name="value">4</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="DdJ_h:apdx1R9=]~HunO">
                                                            <field name="name">num</field>
                                                            <value name="value">
                                                              <block type="expression" id="NS#a{eHs`T]Lbc@1~0#W{">
                                                                <field name="value">2</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </statement>
                                                    <statement name="else">
                                                      <block type="variable" id="w`Mer~^PF4#8c_ysEZ+:">
                                                        <field name="name">part</field>
                                                        <value name="value">
                                                          <block type="random_number" id="l?UgX@2Jxe%uTmGSwz(nC">
                                                            <value name="min">
                                                              <block type="expression" id="eX/g^2hF8{sQ%qr?-6|2">
                                                                <field name="value">8</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="?;6Y46`{wb[e):gKGc=r">
                                                                <field name="value">16</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="Q-bFFuz)[+qFKI+;(%E(">
                                                            <field name="name">num</field>
                                                            <value name="value">
                                                              <block type="random_number" id="[SqDGxOTZqz)@1HP_$n99">
                                                                <value name="min">
                                                                  <block type="expression" id="qu!6FRUla~#ft9hE-L,P">
                                                                    <field name="value">3</field>
                                                                  </block>
                                                                </value>
                                                                <value name="max">
                                                                  <block type="expression" id="KX(s8DEK5K!X{RE?s{M%">
                                                                    <field name="value">4</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </statement>
                                                    <next>
                                                      <block type="variable" id="I@1Q35Dq98_JEIarEaJV,">
                                                        <field name="name">answer</field>
                                                        <value name="value">
                                                          <block type="random_one" id="v0hr3#h2`P^=j1oNP}j2">
                                                            <value name="items">
                                                              <block type="expression" id="]|m-)xko?0b@1J+p1Jt4/">
                                                                <field name="value">["will happen", "probably", "might happen", "won't happen"]</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="h!m;=`Qn3aD7pn1pF1f/">
                                                            <field name="name">num_color</field>
                                                            <value name="value">
                                                              <block type="expression" id="+|)2u%sOs@2@17lA9)Ua7b">
                                                                <field name="value">[]</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="if_then_else_block" id="qA5@2FQ$KM(]XTo2E;KlC">
                                                                <value name="if">
                                                                  <block type="expression" id="Td;t_oL|I$@2Xw|:R9FW(">
                                                                    <field name="value">answer == "won't happen"</field>
                                                                  </block>
                                                                </value>
                                                                <statement name="then">
                                                                  <block type="variable" id="8Y$Zq~~XN@2KCtM~X-oFJ">
                                                                    <field name="name">num_color[0]</field>
                                                                    <value name="value">
                                                                      <block type="expression" id="||s:aS]9kwL|^swf1Yd]">
                                                                        <field name="value">0</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="variable" id="@2zOv=y+I^T83WS5SKB2l">
                                                                        <field name="name">min</field>
                                                                        <value name="value">
                                                                          <block type="expression" id="(r.nlSLfDfq(!D9424U9">
                                                                            <field name="value">1</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <statement name="else">
                                                                  <block type="if_then_else_block" id="%b)iaUHvL~)V=_!/i,[T">
                                                                    <value name="if">
                                                                      <block type="expression" id="mriU7(zp/h$4=`9Xe+f3">
                                                                        <field name="value">answer == "might happen"</field>
                                                                      </block>
                                                                    </value>
                                                                    <statement name="then">
                                                                      <block type="variable" id="gVRJF:[3Oc|=dXt`-Eo,">
                                                                        <field name="name">num_color[0]</field>
                                                                        <value name="value">
                                                                          <block type="random_number" id="%MvwB]6-{shwy/7aR+zZ">
                                                                            <value name="min">
                                                                              <block type="expression" id="-@1{8s#ttM8,z7%^Zh1j_">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="max">
                                                                              <block type="expression" id=";9^@2u;g(;DmRUGAbF(uU">
                                                                                <field name="value">Math.floor(part/2)-1</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="W8Z;{sJY9D5t6+hhl_dI">
                                                                            <field name="name">min</field>
                                                                            <value name="value">
                                                                              <block type="expression" id="6xI9C9_e$TELuX]yNtBK">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <statement name="else">
                                                                      <block type="if_then_else_block" id="FF=HHI]NND2N:m|FMdat">
                                                                        <value name="if">
                                                                          <block type="expression" id="Pu@2C7KaPR}H%WTft$#n(">
                                                                            <field name="value">answer == "probably"</field>
                                                                          </block>
                                                                        </value>
                                                                        <statement name="then">
                                                                          <block type="variable" id="E]^!fCq?eqkcEr|,?eIo">
                                                                            <field name="name">num_color[0]</field>
                                                                            <value name="value">
                                                                              <block type="random_number" id="R#lmXU`J|i#=IS_}lZ=]">
                                                                                <value name="min">
                                                                                  <block type="expression" id="Z^l4F[9J(PCU@2#ix`@2,2">
                                                                                    <field name="value">Math.floor(part/2)+1</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="max">
                                                                                  <block type="expression" id="=E}UIcnuI2mG@2^{D@2Iay">
                                                                                    <field name="value">part-1</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="variable" id="1|UVLh`}^1mHo~LPE0R?">
                                                                                <field name="name">min</field>
                                                                                <value name="value">
                                                                                  <block type="expression" id="4Dt^FIP_0,s=$B|UL0Zs">
                                                                                    <field name="value">1</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <statement name="else">
                                                                          <block type="variable" id=",=_@1^No^}az$-896mRTk">
                                                                            <field name="name">num_color[0]</field>
                                                                            <value name="value">
                                                                              <block type="expression" id="9JrmR$;|tf_)mMJ(Oe9h">
                                                                                <field name="value">part</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="variable" id="@17e%$uf]$-Uimx}m,Pyx">
                                                                                <field name="name">min</field>
                                                                                <value name="value">
                                                                                  <block type="expression" id="UnyVYIq]N}#S6U97#k.d">
                                                                                    <field name="value">0</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                      </block>
                                                                    </statement>
                                                                  </block>
                                                                </statement>
                                                                <next>
                                                                  <block type="variable" id="FSJv]h}J5jHVL@2y6zuj/">
                                                                    <field name="name">i</field>
                                                                    <value name="value">
                                                                      <block type="expression" id="X^.$.CDhWZoLd1V_Wm+6">
                                                                        <field name="value">1</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="variable" id="|xUiS[b)1@1j=:=_E;SL}">
                                                                        <field name="name">temp</field>
                                                                        <value name="value">
                                                                          <block type="expression" id="=HH^%shuICw(wICe@2KNS">
                                                                            <field name="value">part- num_color[0]</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="while_do_block" id="udv:xyIVYmEoT/!2L;2=">
                                                                            <value name="while">
                                                                              <block type="expression" id=".@1[K$(!7@1JB%Ji@1ByWZT">
                                                                                <field name="value">i &lt; num - 1</field>
                                                                              </block>
                                                                            </value>
                                                                            <statement name="do">
                                                                              <block type="variable" id="i0-t#C!6GIE:qP9,~0Tp">
                                                                                <field name="name">num_color[i]</field>
                                                                                <value name="value">
                                                                                  <block type="random_number" id="A|SFW;a-fhb57`~,(=:e">
                                                                                    <value name="min">
                                                                                      <block type="expression" id="$Q-C/VYb!F,6sPbhlBde">
                                                                                        <field name="value">min</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="max">
                                                                                      <block type="expression" id="t3!m8)pwZTiw#IU@1z3l4">
                                                                                        <field name="value">temp - num + i + 1</field>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="variable" id="67}q6z?1tqKeO?-v;c--">
                                                                                    <field name="name">temp</field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="uZuV,gH_euc([|}(fkR+">
                                                                                        <field name="value">temp - num_color[i]</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="variable" id="Sb9J(nNu~Jl/FCeaP%QN">
                                                                                        <field name="name">i</field>
                                                                                        <value name="value">
                                                                                          <block type="expression" id="[1[^[T+)T+4RL)J:tUir">
                                                                                            <field name="value">i+1</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="variable" id="2rIxciilj@25hj.vfg0nI">
                                                                                <field name="name">num_color[i]</field>
                                                                                <value name="value">
                                                                                  <block type="expression" id="UkpPO-_25B}~v!:L7kqn">
                                                                                    <field name="value">temp</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="variable" id="uq:LfoBJgnw0P^j@1CH(l">
                                                                                    <field name="name">color</field>
                                                                                    <value name="value">
                                                                                      <block type="random_many" id="xriTUM8Lz6%H56eVJUp+">
                                                                                        <value name="count">
                                                                                          <block type="expression" id="UUeD(d_ZD5oBtVcWI?+-">
                                                                                            <field name="value">num</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="items">
                                                                                          <block type="expression" id="xJh)Vhf`YN!Z8O]mgNA1">
                                                                                            <field name="value">['b', 'g', 'r', 'y']</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="variable" id="qa0tpg3[ad5^$$F0Qu]{">
                                                                                        <field name="name">qes_color</field>
                                                                                        <value name="value">
                                                                                          <block type="expression" id="B@17;+KjDC|OT^`hx6a6;">
                                                                                            <field name="value">color[0]</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="variable" id="1+lHlf!kHAuL4Vv:?Ewf">
                                                                                            <field name="name">spiner</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id=",?+[e41qFl%@2oXQw;rOg">
                                                                                                <field name="value">[]</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="variable" id="NPkN9P7AN2u0x!HRbI)`">
                                                                                                <field name="name">i</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="2Z71gT!CyJQ=-@2@1g(r[_">
                                                                                                    <field name="value">0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="variable" id="uo6SKuoVjzqj35S+~X88">
                                                                                                    <field name="name">count</field>
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="[rTP6WA|#4d{u_)K{/Tr">
                                                                                                        <field name="value">0</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="while_do_block" id=":/RZ.3-is$hZ{@1_9cIZp">
                                                                                                        <value name="while">
                                                                                                          <block type="expression" id="IW+kD0#^%$)5,FkYRqPI">
                                                                                                            <field name="value">i &lt; num</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <statement name="do">
                                                                                                          <block type="variable" id="59wb:a7NxQFL-r7%|zcR">
                                                                                                            <field name="name">j</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="_fEg^`%Z[LjtwDtG?Xmr">
                                                                                                                <field name="value">0</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="while_do_block" id="5aqdQG]w:%Mi^bBfoc1w">
                                                                                                                <value name="while">
                                                                                                                  <block type="expression" id="3AH2x.me,8i/DniXLQbt">
                                                                                                                    <field name="value">j &lt; num_color[i]</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <statement name="do">
                                                                                                                  <block type="variable" id="7lip;lU`69u0`(fH,a#v">
                                                                                                                    <field name="name">spiner[count]</field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="ZO;bcS{lc~MOwnW@1}+8A">
                                                                                                                        <field name="value">color[i]</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="variable" id="eOn,hApnH%2PJ$wL@1SlG">
                                                                                                                        <field name="name">j</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id="T^1O[]+JK/(c]tvibJZ8">
                                                                                                                            <field name="value">j+1</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="variable" id="QdiLynMtpU!sm)!3bH[=">
                                                                                                                            <field name="name">count</field>
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="MSJG3c~Ez0#}(gxVE%NT">
                                                                                                                                <field name="value">count + 1</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="variable" id="?8mcr:o4iy]T5h26u[50">
                                                                                                                    <field name="name">i</field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="F{Gp{XtIIwhmRV9(]xje">
                                                                                                                        <field name="value">i+1</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="variable" id="]_DbE:2bwm?-~jp{.XE4">
                                                                                                            <field name="name">spiner</field>
                                                                                                            <value name="value">
                                                                                                              <block type="func_shuffle" id="g,R;@2B@1q@2D+f_yxwQXRN" inline="true">
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="y3P@2OP1Mu1fXA9mHFi?t">
                                                                                                                    <field name="value">spiner</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="variable" id="n}]v:P#Qd|EBnXHS@1.4)">
                                                                                                                <field name="name">tittle</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="random_one" id="wz20{heCdgkBr;N=!4Ik">
                                                                                                                    <value name="items">
                                                                                                                      <block type="expression" id="yt!ogZ[m1H/D3[OV|dD5">
                                                                                                                        <field name="value">['How likely will', 'What is the chance', 'What is the probability of']</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                                                                                            <field name="value">400</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                                                                                            <field name="value">20</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_anchor" id="O.v@2mKn@1K#+tF;|}C}xI">
                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="+3%GU[X3l%%X`Jv1QL7@2">
                                                                                                                                <field name="value">0.5</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="Kzp23HjHF7k]A-y1BJPm">
                                                                                                                                <field name="value">0</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="contents">
                                                                                                                                  <block type="string_value" id="#6btV9p,PGV}wMx.U�">
                                                                                                                                    <field name="value">If we spin the spinner, ${tittle.toLowerCase()} the spinner lands on the ${re_color(qes_color)} colour?</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                                                                                    <field name="base">text</field>
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="fontSize">
                                                                                                                                          <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                                                                                            <field name="value">36</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="fill">
                                                                                                                                              <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                                                                                                <field name="value">black</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="stroke">
                                                                                                                                                  <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                                                                                    <field name="value">white</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="strokeThickness">
                                                                                                                                                      <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                                                                                        <field name="value">2</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="image_shape" id="stnYv6tF0TZWG,%G.9|p">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id=",^c8(QC4F|4~3.QB@2=Vu">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="CE1+C2oNd:kHg0kH$Gh1">
                                                                                                                                <field name="value">400</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="7љ!PtEb]dtCb:G/Fl">
                                                                                                                                <field name="value">320</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_key" id="9;rtu5n-/l83HF96KaV+">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="key">
                                                                                                                                  <block type="string_value" id="f.w?Ga%LlwPU(~p-;0H4">
                                                                                                                                    <field name="value">spin.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_src" id="ZBey}(HSc^??M9~oZpN{">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="src">
                                                                                                                                      <block type="string_value" id="pFQ|o@1]Jqb/Q1:V)tOke">
                                                                                                                                        <field name="value">${image_path}spin.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="variable" id="ZiMU.6/J:z[7iqB)eYr2">
                                                                                                                            <field name="name">i</field>
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="mKSt_{O{B;RceDv@1thXL">
                                                                                                                                <field name="value">0</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="variable" id=",p2xT#6n3b%{2xo+0iww">
                                                                                                                                <field name="name">angle</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="-syTZzyNYfMmE|=A6Ewh">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="while_do_block" id="K}wG5;(koYvOG$$iq.-G">
                                                                                                                                    <value name="while">
                                                                                                                                      <block type="expression" id="kiqPCCV~mr+3I_6j=Pq{">
                                                                                                                                        <field name="value">i &lt; part</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <statement name="do">
                                                                                                                                      <block type="image_shape" id="B^c3a+e%!FB6?H9]q=$h">
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_position" id="e5a=}@1n1sExE;w9?OCdZ">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id="-ow}`u4yLk#a!oZB1DQv">
                                                                                                                                                <field name="value">400</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="I5Mx4g0gWX7as8is4?H{">
                                                                                                                                                <field name="value">200</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_key" id="8((h:PxpCq]R_lN~/zT-">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="key">
                                                                                                                                                  <block type="string_value" id="0U(!S.U1@1k+r(Zv^;8;z">
                                                                                                                                                    <field name="value">1-${part}${spiner[i]}.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_image_src" id="TypF1@2v]c_{%d;}PiukD">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="src">
                                                                                                                                                      <block type="string_value" id="nky@2i}N.~a;DnikVw2wL">
                                                                                                                                                        <field name="value">${image_path}1-${part}${spiner[i]}.png</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_angle" id="1v4$;GZyOa==B(E}{m:=">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="angle">
                                                                                                                                                          <block type="expression" id="g/HB@2J_XKaQk0/qFnb~v">
                                                                                                                                                            <field name="value">angle</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <next>
                                                                                                                                          <block type="variable" id="?488E%Xo#i1fZ+O-kG|w">
                                                                                                                                            <field name="name">angle</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id="@1B/Aai4fPtX{wuGRLh=y">
                                                                                                                                                <field name="value">angle + 360/part</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="variable" id="TiZwhByNm_HmbLl-hTmK">
                                                                                                                                                <field name="name">i</field>
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="expression" id="zYd_HSBg%PPrbzS(_u_p">
                                                                                                                                                    <field name="value">i+1</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="image_shape" id="aeXIbpFRM}7~QMEqQ?6P">
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_position" id="oJ[VS18lNuMXsn#+@1.$g">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id="-!s(zkaq+(IvZOE$7RVn">
                                                                                                                                                <field name="value">400</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="z@2SC6N0gxa.fcANjy!vd">
                                                                                                                                                <field name="value">190</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_key" id="~J^8+#V;ax:?OV6jh)gv">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="key">
                                                                                                                                                  <block type="string_value" id="]_c~Lx:Qq2H))gO0dh#J">
                                                                                                                                                    <field name="value">point.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_image_src" id="lPOnero_qxG(R[kSU;!?">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="src">
                                                                                                                                                      <block type="string_value" id="=`GrP,Zgh$A-Q+rW0kSD">
                                                                                                                                                        <field name="value">${image_path}point.png</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <next>
                                                                                                                                          <block type="grid_shape" id="^7ZZyIN@2dC:S}WhR5fqX">
                                                                                                                                            <statement name="#props">
                                                                                                                                              <block type="prop_grid_dimension" id=";%L3`U`X1:}y4T^QD(^y">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="rows">
                                                                                                                                                  <block type="expression" id="y^s5/g$vw#Qkh|y.ty,m">
                                                                                                                                                    <field name="value">1</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <value name="cols">
                                                                                                                                                  <block type="expression" id="cmiCohrouf{JmE+|@1,~U">
                                                                                                                                                    <field name="value">4</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_position" id="O@1J%yb3]4DxX:;S4V^">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="x">
                                                                                                                                                      <block type="expression" id="9I,0%JIvA8n~hgCF^f.{">
                                                                                                                                                        <field name="value">400</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="y">
                                                                                                                                                      <block type="expression" id="@1fQS|_:]lmq{lHlymHO%">
                                                                                                                                                        <field name="value">400</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_size" id="a7^Y3/][3v,88PZ3{Q?h">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="width">
                                                                                                                                                          <block type="expression" id="~w#~~}@1;Kn8z$Jj8O1NC">
                                                                                                                                                            <field name="value">750</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="height">
                                                                                                                                                          <block type="expression" id="%W#Z}sXD9gWbaffzuGzB">
                                                                                                                                                            <field name="value">50</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_grid_cell_source" id="`qTEmm~2vwbXHh@2N!]K)">
                                                                                                                                                            <field name="#prop">cell.source</field>
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id="8swjd_|B4@1^)p,LM6/v9">
                                                                                                                                                                <field name="value">["will happen", "probably", "might happen", "won't happen"]</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_anchor" id="H?#(V1IKb+WpoHVOA~)M">
                                                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                                                <value name="x">
                                                                                                                                                                  <block type="expression" id=":vce#5@2r9R/aB9g]rQ]F">
                                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <value name="y">
                                                                                                                                                                  <block type="expression" id="2n$R(EF3P2_|pad%hxod">
                                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_grid_show_borders" id="5G9}kV{K11tYA_-jL/Ut">
                                                                                                                                                                    <field name="#prop">#showBorders</field>
                                                                                                                                                                    <field name="value">FALSE</field>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_grid_random" id="?Jm@2QJVSp^q|1@2k=@1|ze">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <field name="random">TRUE</field>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_grid_cell_template" id="QICq/h`A+A[r45Vk`U,)">
                                                                                                                                                                            <field name="variable">$cell</field>
                                                                                                                                                                            <field name="#prop">cell.template</field>
                                                                                                                                                                            <field name="#callback">$cell</field>
                                                                                                                                                                            <statement name="body">
                                                                                                                                                                              <block type="choice_custom_shape" id=",og#MhHa+fU}RS2_(,F!">
                                                                                                                                                                                <field name="action">click-one</field>
                                                                                                                                                                                <value name="value">
                                                                                                                                                                                  <block type="expression" id="7!;O!=?%x:SXuTBHq8Wz">
                                                                                                                                                                                    <field name="value">$cell.data</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <statement name="template">
                                                                                                                                                                                  <block type="image_shape" id=":W]q1.w#N{`lTEfqUhnr">
                                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                                      <block type="prop_position" id="mR-ViOi(q#:-ySRF;6g#">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="x">
                                                                                                                                                                                          <block type="expression" id="grSP/10[^Fuqe6?!DYd[">
                                                                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <value name="y">
                                                                                                                                                                                          <block type="expression" id="%v$Pr/3`yt^l7O}lDpfk">
                                                                                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_size" id="{V]rP@1W2Vj;;]Ont!Y)$">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <value name="width">
                                                                                                                                                                                              <block type="expression" id="}dcCqp;[ITh:.+FeJnFI">
                                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <value name="height">
                                                                                                                                                                                              <block type="expression" id="9_bhAV7;o]gdQ0uY027d">
                                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_image_key" id="[Tcg_t2yU7A1![LVa}B?">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <value name="key">
                                                                                                                                                                                                  <block type="string_value" id="aqcp$X#-a~ZcH^sSW9lq">
                                                                                                                                                                                                    <field name="value">${$cell.data[2]}.png</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="prop_image_src" id="bDH;-9x_TlZYz#A@1N#[N">
                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                    <value name="src">
                                                                                                                                                                                                      <block type="string_value" id="$G@2C^;U@2Y8vj,B@2%Xot0">
                                                                                                                                                                                                        <field name="value">${image_path}${$cell.data[2]}.png</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </statement>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </statement>
                                                                                                                                                                              </block>
                                                                                                                                                                            </statement>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                            <next>
                                                                                                                                              <block type="func_add_answer" id="MXv[#_!gx2hoYsPlli6U" inline="true">
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="expression" id="34:3nzK4]:zGN=/3HMLe">
                                                                                                                                                    <field name="value">answer</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="variable" id="P+w+(t0.AS{xAe]oTnl(">
                                                                                                                                                    <field name="name">link</field>
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="expression" id="N%wWv708|O)^76gok0ro">
                                                                                                                                                        <field name="value">'http://starmathsonline.s3.amazonaws.com/' + image_path</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="partial_explanation" id="zrqhGkidjctg:9_?4d9]" inline="true">
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="string_value" id="ECY@2}:K)l@1;_wsq-Moyf">
                                                                                                                                                            <field name="value">&lt;u&gt;Step 1: Familiarise yourself with the meaning of "will happen", "probably", "might happen", "won't happen".&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;
&lt;style&gt;
  td{border: 1px solid black; text-align: center}
table{background: rgb(179, 218, 255, 0.5)}
&lt;/style&gt;
&lt;center&gt;
  &lt;table&gt;
    &lt;tr&gt;
      &lt;td style='text-align: center; width: 250px'&gt;&lt;b&gt;Probability Term&lt;/b&gt;&lt;/td&gt;
      &lt;td style='text-align: center; width: 750px'&gt;&lt;b&gt;Meaning&lt;/b&gt;&lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
      &lt;td style='text-align: center; width: 250px'&gt;&lt;span style=''&gt;Will happen&lt;/span&gt;&lt;/td&gt;
      &lt;td&gt;&lt;span style=''&gt;Event will surely happen, certain to take place.&lt;/span&gt;&lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
      &lt;td style='text-align: center; width: 250px'&gt;&lt;span style=''&gt;Probably&lt;/span&gt;&lt;/td&gt;
      &lt;td&gt;&lt;span style=''&gt;Event has high chance of happening, very likely to take place.&lt;/span&gt;&lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
      &lt;td style='text-align: center; width: 250px'&gt;&lt;span style=''&gt;Might happen&lt;/span&gt;&lt;/td&gt;
      &lt;td&gt;&lt;span style=''&gt;Event has high chance of happening, very likely to take place.&lt;/span&gt;&lt;/td&gt;
    &lt;/tr&gt;
    &lt;tr&gt;
      &lt;td style='text-align: center; width: 250px'&gt;&lt;span style=''&gt;Won't happen&lt;/span&gt;&lt;/td&gt;
      &lt;td&gt;&lt;span style=''&gt;Event has no chance of happening, impossible to take place.&lt;/span&gt;&lt;/td&gt;
    &lt;/tr&gt;
   &lt;/table&gt;
&lt;/center&gt;
                                                                                                                                                            </field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="end_partial_explanation" id="=6p[{+ywNy9qVg_S@2BB@1">
                                                                                                                                                            <next>
                                                                                                                                                              <block type="partial_explanation" id="8mU]f%I9!.kV@2wlfNj80" inline="true">
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="string_value" id="@1fJ2ou6?jlZ2e9qz}_=m">
                                                                                                                                                                    <field name="value">&lt;u&gt;Step 2: From the question, identify the item to focus on in the question.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;
&lt;span style='background: rgb(250, 250, 250, 0.5)'&gt;
"If we spin the spinner, ${tittle.toLowerCase()} the spinner lands on the &lt;b&gt;${re_color(qes_color)}&lt;/b&gt; colour?"&lt;/br&gt;&lt;/br&gt;

The colour to focus is &lt;b&gt;${re_color(qes_color)}&lt;/b&gt; colour.
  
&lt;/span&gt;
                                                                                                                                                                    </field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="end_partial_explanation" id="_Ajx3{ONICIaEXa%oygc">
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="partial_explanation" id="a6mKYl0AUF@1lfF]4}@2QP" inline="true">
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="string_value" id="ki$2RCuL8+sygO-coiG7">
                                                                                                                                                                            <field name="value">&lt;u&gt;Step 3: Identify the chances of the spinner landing on the ${re_color(qes_color)} colour.&lt;/u&gt;
&lt;/br&gt;&lt;/br&gt;
  &lt;center&gt;
&lt;div style='position: relative; width:187px; height: 187px;'&gt;
  &lt;img src='@1sprite.src(spin.png)' style='position: absolute; left: 12px; top: 160px'/&gt;
                                                                                                                                                                            </field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="variable" id="T6CR{EhCT?^FR^C-W#(T">
                                                                                                                                                                            <field name="name">class_name</field>
                                                                                                                                                                            <value name="value">
                                                                                                                                                                              <block type="expression" id=")PGJMN}Ks0ZH169doRn+">
                                                                                                                                                                                <field name="value">[]</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="variable" id="BNeq@2k/W[XC9lH+)uE8F">
                                                                                                                                                                                <field name="name">angle</field>
                                                                                                                                                                                <value name="value">
                                                                                                                                                                                  <block type="expression" id="@1s#iT]1WvzXs#}d74ej%">
                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="variable" id="mQrG/$6lLVUOPEr3sF1z">
                                                                                                                                                                                    <field name="name">i</field>
                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                      <block type="expression" id="$0Lp3yl6Dc8SXUMhC8JT">
                                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="while_do_block" id="6HT+}RYt7wzto=[zE3Lp">
                                                                                                                                                                                        <value name="while">
                                                                                                                                                                                          <block type="expression" id="=USbr_|U;wW]Y~t#KU,4">
                                                                                                                                                                                            <field name="value">i &lt; part</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <statement name="do">
                                                                                                                                                                                          <block type="variable" id="oJ:%j@1%kj=9Y5WcaD^~K">
                                                                                                                                                                                            <field name="name">class_name[i]</field>
                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                              <block type="expression" id="N:DfDWqN_0w)N]A+RKuL">
                                                                                                                                                                                                <field name="value">'angle' + i</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="partial_explanation" id="~@2oSKGa?lFE3-Tk][PUQ" inline="true">
                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                  <block type="string_value" id="Sz1rwA[,[,F8Vjp[$|3M">
                                                                                                                                                                                                    <field name="value">&lt;style&gt;
   .${class_name[i]} {
     position: absolute;
        -ms-transform: rotate(${angle}deg);
        /@2 IE 9 @2/
        -webkit-transform: rotate(${angle}deg);
        /@2 Safari @2/
        transform: rotate(${angle}deg);
        /@2 Standard syntax @2/
    }
&lt;/style&gt;
&lt;div class=${class_name[i]}&gt;
&lt;img src='@1sprite.src(1-${part}${spiner[i]}.png)'/&gt;
  &lt;/div&gt;
                                                                                                                                                                                                    </field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="variable" id="i^b{t!^g`LHdz2qa@1~qS">
                                                                                                                                                                                                    <field name="name">angle</field>
                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                      <block type="expression" id="x@2}{[?Mbq~V49~/j@12!+">
                                                                                                                                                                                                        <field name="value">angle + 360/part</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="variable" id="(Kq@1USUBzqcGt$_AVx{#">
                                                                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                          <block type="expression" id="Yr8g)FFh(?CWk:ZY~DQ:">
                                                                                                                                                                                                            <field name="value">i+1</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </statement>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="partial_explanation" id="0,9Mhgtso%~O^@2@26al}Y" inline="true">
                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                              <block type="string_value" id="VC[CAmy}V@2E]jtdJUOF2">
                                                                                                                                                                                                <field name="value">&lt;img src='@1sprite.src(point.png)' style='position: absolute; left: 62px; top: 40px'/&gt;
  
  &lt;/div&gt;&lt;/center&gt;
                                                                                                                                                                                                </field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="partial_explanation" id="[[$yUTF:Tw{Rx9q{~K%Q" inline="true">
                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                  <block type="string_value" id="oBr,ae|HP|jRLsTkAPvc">
                                                                                                                                                                                                    <field name="value">&lt;/br&gt;&lt;/br&gt; &lt;span style='background: rgb(250, 250, 250, 0.3)'&gt;
  
Number of &lt;b&gt;${re_color(qes_color)}&lt;/b&gt; part${num_color[0] &lt;2 ? '':'s'}: &lt;b&gt;${num_color[0]}&lt;/b&gt;.&lt;/b&gt;&lt;/br&gt;

Total equal parts: &lt;b&gt;${part}&lt;/b&gt;&lt;/br&gt;

&lt;b&gt;${num_color[0]}&lt;/b&gt; out of &lt;b&gt;${part}&lt;/b&gt; chances.&lt;/br&gt;

The chances of the spinner landing on ${re_color(qes_color)} colour is "&lt;b&gt;${answer}&lt;/b&gt;".&lt;/span&gt;
                                                                                                                                                                                                    </field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="end_partial_explanation" id="FVxYwA}7t117f}d1!Tnz"></block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </statement>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
</xml>
END_XML]] */