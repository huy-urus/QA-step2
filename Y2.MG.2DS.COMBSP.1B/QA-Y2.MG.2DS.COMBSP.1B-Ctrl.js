
module.exports = [
  {
    "#type": "question",
    "name": "Y2.MG.2DS.COMBSP.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y2.MG.2DS.COMBSP.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "rank",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "20"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "The red line cuts the picture into 2.\nIdentify and select the new shapes that you see below."
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "36"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "name_shape",
        "value": {
          "#type": "string_array",
          "items": "Kite|Pentagon|Hexangon|Circle|Rectangle|Square|Diamond|Parallelogram|Trapezium|Semi-Circle|Triangle"
        }
      },
      {
        "#type": "variable",
        "name": "shape",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "0"
          },
          "max": {
            "#type": "expression",
            "value": "11"
          }
        }
      },
      {
        "#type": "statement",
        "value": "var ans_shape = [\n  10,\n  4,\n  10,\n  4,\n  10,\n  10,\n  10,\n  4,\n  10,\n  [8, 10],\n  8,\n  9\n];\n\nfunction check(x, A, num) {\n    for(var m=0; m<num; m++){\n        if(x == A[m]){\n            return 1;\n        }\n    }\n    return 0;\n}"
      },
      {
        "#type": "variable",
        "name": "answer",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "shape != 9"
        },
        "then": [
          {
            "#type": "variable",
            "name": "answer[0]",
            "value": {
              "#type": "expression",
              "value": "ans_shape[shape]"
            }
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "1"
            }
          }
        ],
        "else": [
          {
            "#type": "variable",
            "name": "answer[0]",
            "value": {
              "#type": "expression",
              "value": "ans_shape[shape][0]"
            }
          },
          {
            "#type": "variable",
            "name": "answer[1]",
            "value": {
              "#type": "expression",
              "value": "ans_shape[shape][1]"
            }
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "2"
            }
          }
        ]
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < 6"
        },
        "do": [
          {
            "#type": "do_while_block",
            "do": [
              {
                "#type": "variable",
                "name": "answer[i]",
                "value": {
                  "#type": "random_one",
                  "items": {
                    "#type": "func",
                    "name": "arrayOfNumber",
                    "args": [
                      {
                        "#type": "expression",
                        "value": "11"
                      },
                      {
                        "#type": "expression",
                        "value": "0"
                      }
                    ]
                  }
                }
              }
            ],
            "while": {
              "#type": "expression",
              "value": "check(answer[i], answer, i) == 1"
            }
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "answer",
        "value": {
          "#type": "func",
          "name": "shuffle",
          "args": [
            {
              "#type": "expression",
              "value": "answer"
            }
          ]
        }
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "1"
            },
            "cols": {
              "#type": "expression",
              "value": "1"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "200"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "750"
            },
            "height": {
              "#type": "expression",
              "value": "190"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "expression",
              "value": "1"
            }
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_cell_template",
            "variable": "$cell",
            "#prop": "cell.template",
            "#callback": "$cell",
            "body": [
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY"
                    }
                  },
                  {
                    "#type": "prop_max_size",
                    "#prop": "",
                    "maxWidth": {
                      "#type": "expression",
                      "value": "0"
                    },
                    "maxHeight": {
                      "#type": "expression",
                      "value": "$cell.height"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "${shape}.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}${shape}.png"
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "2"
            },
            "cols": {
              "#type": "expression",
              "value": "3"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "370"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "750"
            },
            "height": {
              "#type": "expression",
              "value": "130"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "expression",
              "value": "answer"
            }
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_cell_template",
            "variable": "$cell",
            "#prop": "cell.template",
            "#callback": "$cell",
            "body": [
              {
                "#type": "if_then_else_block",
                "if": {
                  "#type": "expression",
                  "value": "shape == 9"
                },
                "then": [
                  {
                    "#type": "choice_custom_shape",
                    "action": "click-many",
                    "value": {
                      "#type": "expression",
                      "value": "$cell.data"
                    },
                    "template": {
                      "#callback": "$choice",
                      "variable": "$choice",
                      "body": [
                        {
                          "#type": "image_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "$cell.centerX"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "$cell.centerY"
                              }
                            },
                            {
                              "#type": "prop_max_size",
                              "#prop": "",
                              "maxWidth": {
                                "#type": "expression",
                                "value": "0"
                              },
                              "maxHeight": {
                                "#type": "expression",
                                "value": "$cell.height"
                              }
                            },
                            {
                              "#type": "prop_image_key",
                              "#prop": "",
                              "key": "a${$cell.data}.png"
                            },
                            {
                              "#type": "prop_image_src",
                              "#prop": "",
                              "src": "${image_path}a${$cell.data}.png"
                            }
                          ]
                        }
                      ]
                    }
                  }
                ],
                "else": [
                  {
                    "#type": "choice_custom_shape",
                    "action": "click-one",
                    "value": {
                      "#type": "expression",
                      "value": "$cell.data"
                    },
                    "template": {
                      "#callback": "$choice",
                      "variable": "$choice",
                      "body": [
                        {
                          "#type": "image_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "$cell.centerX"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "$cell.centerY"
                              }
                            },
                            {
                              "#type": "prop_max_size",
                              "#prop": "",
                              "maxWidth": {
                                "#type": "expression",
                                "value": "0"
                              },
                              "maxHeight": {
                                "#type": "expression",
                                "value": "$cell.height"
                              }
                            },
                            {
                              "#type": "prop_image_key",
                              "#prop": "",
                              "key": "a${$cell.data}.png"
                            },
                            {
                              "#type": "prop_image_src",
                              "#prop": "",
                              "src": "${image_path}a${$cell.data}.png"
                            }
                          ]
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "func",
        "name": "addAnswers",
        "args": [
          {
            "#type": "expression",
            "value": "ans_shape[shape]"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "load",
        "value": {
          "#type": "custom_image_list",
          "link": "${image_path}",
          "images": "${shape}|${shape}_1|${shape}_2|arrow"
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "shape == 9"
        },
        "then": [
          {
            "#type": "variable",
            "name": "a1",
            "value": {
              "#type": "expression",
              "value": "ans_shape[9][0]"
            }
          },
          {
            "#type": "variable",
            "name": "a2",
            "value": {
              "#type": "expression",
              "value": "ans_shape[9][1]"
            }
          }
        ],
        "else": [
          {
            "#type": "variable",
            "name": "a1",
            "value": {
              "#type": "expression",
              "value": "ans_shape[shape]"
            }
          },
          {
            "#type": "variable",
            "name": "a2",
            "value": {
              "#type": "expression",
              "value": "ans_shape[shape]"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<center>\n  <u>Identify the shapes that appear when the picture is cut into 2.</u>\n  \n \n <style>\n  table {background: rgb(150, 200, 250, 0.3); width: 90%}\n  td{text-align: center;}\n  img{padding: 10px; max-height: 200px}\n  </style>\n\n<table>\n  <tr>\n  <td rowspan='2'><img src='@sprite.src(${shape})'/><img src='@sprite.src(arrow)'/></td>\n<td><img src='@sprite.src(${shape}_1)'/>${name_shape[a1]}</td>\n</tr><tr><td><img src='@sprite.src(${shape}_2)'/>${name_shape[a2]}</td>\n</tr>\n</table>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="105" y="8467">
    <field name="name">Y2.MG.2DS.COMBSP.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y2.MG.2DS.COMBSP.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="YepMQT@2uwp2rRPvmnRg1">
                                    <field name="name">rank</field>
                                    <value name="value">
                                      <block type="expression" id="UxgQ^Y5]rb}vpw[D=l-!">
                                        <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                        <statement name="#props">
                                          <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                            <field name="#prop"></field>
                                            <value name="x">
                                              <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                <field name="value">400</field>
                                              </block>
                                            </value>
                                            <value name="y">
                                              <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                <field name="value">20</field>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="prop_anchor" id="O.v@2mKn@1K#+tF;|}C}xI">
                                                <field name="#prop">anchor</field>
                                                <value name="x">
                                                  <block type="expression" id="+3%GU[X3l%%X`Jv1QL7@2">
                                                    <field name="value">0.5</field>
                                                  </block>
                                                </value>
                                                <value name="y">
                                                  <block type="expression" id="Kzp23HjHF7k]A-y1BJPm">
                                                    <field name="value">0</field>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                    <field name="#prop"></field>
                                                    <value name="contents">
                                                      <block type="string_value" id="#6btV9p,PGV}wMx.Uï¿½">
                                                        <field name="value">The red line cuts the picture into 2.
Identify and select the new shapes that you see below.
                                                        </field>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                        <field name="base">text</field>
                                                        <statement name="#props">
                                                          <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                            <field name="#prop"></field>
                                                            <value name="fontSize">
                                                              <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                <field name="value">36</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                <field name="#prop"></field>
                                                                <value name="fill">
                                                                  <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                    <field name="value">black</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                    <field name="#prop"></field>
                                                                    <value name="stroke">
                                                                      <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                        <field name="value">white</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                        <field name="#prop"></field>
                                                                        <value name="strokeThickness">
                                                                          <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                            <field name="value">2</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </statement>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </statement>
                                        <next>
                                          <block type="variable" id="d_[8_Zr_R(C@1uDZ$N:70">
                                            <field name="name">name_shape</field>
                                            <value name="value">
                                              <block type="string_array" id="dZK1k:BMdmZ;]77@2AzHE">
                                                <field name="items">Kite|Pentagon|Hexangon|Circle|Rectangle|Square|Diamond|Parallelogram|Trapezium|Semi-Circle|Triangle</field>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id=")]hh(Fpau]Gav0-?l=wI">
                                                <field name="name">shape</field>
                                                <value name="value">
                                                  <block type="random_number" id="d+v2].ylxA28:)!lILJN">
                                                    <value name="min">
                                                      <block type="expression" id="vks2`@2Pr9pSZ=Wqqpl[9">
                                                        <field name="value">0</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="X;FVXyLQO6[2bw+Y,Eah">
                                                        <field name="value">11</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="statement" id="b:JK$HBAFHr9krt0gxQj">
                                                    <field name="value">var ans_shape = [
  10,
  4,
  10,
  4,
  10,
  10,
  10,
  4,
  10,
  [8, 10],
  8,
  9
];

function check(x, A, num) {
    for(var m=0; m&lt;num; m++){
        if(x == A[m]){
            return 1;
        }
    }
    return 0;
}
                                                    </field>
                                                    <next>
                                                      <block type="variable" id="=!Gn#-s4VlhNL~]?d[hX">
                                                        <field name="name">answer</field>
                                                        <value name="value">
                                                          <block type="expression" id="S(nv3swW.|:-Kig,ExEK">
                                                            <field name="value">[]</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="if_then_else_block" id="/tZXyZ~1j/}[fc9JdT2(">
                                                            <value name="if">
                                                              <block type="expression" id="$sEOM/VTnP%@1Uj@1.5`A2">
                                                                <field name="value">shape != 9</field>
                                                              </block>
                                                            </value>
                                                            <statement name="then">
                                                              <block type="variable" id="i!dP58UuDTMUv)[prP~:">
                                                                <field name="name">answer[0]</field>
                                                                <value name="value">
                                                                  <block type="expression" id="1Jj.nI{`9:dJcMShZ=Di">
                                                                    <field name="value">ans_shape[shape]</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="variable" id="e!zFeC2ej5)90t0x!$V]">
                                                                    <field name="name">i</field>
                                                                    <value name="value">
                                                                      <block type="expression" id="Z%jp/MI[bBJX1:GniPfi">
                                                                        <field name="value">1</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </statement>
                                                            <statement name="else">
                                                              <block type="variable" id=":0@1uJA52HW?(Lt)4t@2^X">
                                                                <field name="name">answer[0]</field>
                                                                <value name="value">
                                                                  <block type="expression" id="c_KlvBOb)Y[U?^]7M@2]z">
                                                                    <field name="value">ans_shape[shape][0]</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="variable" id="tykrJ^-P?EmAMxr1J,73">
                                                                    <field name="name">answer[1]</field>
                                                                    <value name="value">
                                                                      <block type="expression" id="5_S(P]4:Ph3Y+I0T4zQK">
                                                                        <field name="value">ans_shape[shape][1]</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="variable" id="o7Z/qS(4[KEsvb]([LG[">
                                                                        <field name="name">i</field>
                                                                        <value name="value">
                                                                          <block type="expression" id=":E|:R9!1nl)h}mMF.@24Z">
                                                                            <field name="value">2</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </statement>
                                                            <next>
                                                              <block type="while_do_block" id="4F,@2@1tTLB??9ywKM5#Vl">
                                                                <value name="while">
                                                                  <block type="expression" id="JKm_JB$_pJ#be/x1q]5B">
                                                                    <field name="value">i &lt; 6</field>
                                                                  </block>
                                                                </value>
                                                                <statement name="do">
                                                                  <block type="do_while_block" id="TGuF]pX}YXuMLO@2kt=O.">
                                                                    <statement name="do">
                                                                      <block type="variable" id="+Xy-x3GB9r2z@1y`D]p([">
                                                                        <field name="name">answer[i]</field>
                                                                        <value name="value">
                                                                          <block type="random_one" id="|FuBl.}vo@2N3KAbOwLjZ">
                                                                            <value name="items">
                                                                              <block type="func_array_of_number" id="Ds?R}jy}liG2nSu!yy.2" inline="true">
                                                                                <value name="items">
                                                                                  <block type="expression" id="#}x2ES8~;hg`HdQ)SKLW">
                                                                                    <field name="value">11</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="from">
                                                                                  <block type="expression" id="fw`2zv[o@2Gd$}l=HsO4-">
                                                                                    <field name="value">0</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </statement>
                                                                    <value name="while">
                                                                      <block type="expression" id="Sb8pL#R9u)q:b,TcPgeC">
                                                                        <field name="value">check(answer[i], answer, i) == 1</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="variable" id="nqfGQ@277Nif^|uP]:agN">
                                                                        <field name="name">i</field>
                                                                        <value name="value">
                                                                          <block type="expression" id="Ku)]EbqBM7;u;][1pi=.">
                                                                            <field name="value">i+1</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <next>
                                                                  <block type="variable" id="bwvVd[wNc}2A)~0Gbwo.">
                                                                    <field name="name">answer</field>
                                                                    <value name="value">
                                                                      <block type="func_shuffle" id="m14XXoT8KI5I0@14iU)}X" inline="true">
                                                                        <value name="value">
                                                                          <block type="expression" id="oP9{eomvo1B2D=,wfos5">
                                                                            <field name="value">answer</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="grid_shape" id="1C_Rk973M$xY!;+kawGC">
                                                                        <statement name="#props">
                                                                          <block type="prop_grid_dimension" id="hc0qCaC[9QfZIIIt`a@1^">
                                                                            <field name="#prop"></field>
                                                                            <value name="rows">
                                                                              <block type="expression" id="rqzQvst,6$YJZv8(.R?x">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="cols">
                                                                              <block type="expression" id="D|.9OP|pz}f^$,#Xk(JI">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_position" id="uv7JOcV:6Ct9Bs#6YEV/">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="WKY6yuHo6,bDTpEcnF}[">
                                                                                    <field name="value">400</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id=";?5wB^J1p/p8Uq;U33l@1">
                                                                                    <field name="value">200</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_size" id="zDK[ka}bZy_S:ETJ.pDO">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="width">
                                                                                      <block type="expression" id="xP/3.wh|As`Ae$L$xrY~">
                                                                                        <field name="value">750</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="height">
                                                                                      <block type="expression" id="~TC[-U);-,1A#|m[!i~G">
                                                                                        <field name="value">190</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_anchor" id="p5L.B@2bMO|X6{!6UN]q5">
                                                                                        <field name="#prop">anchor</field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id=".4GO8H()%||nvE5kb/t4">
                                                                                            <field name="value">0.5</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="gHe@1,,SY}tO=d%!liz;0">
                                                                                            <field name="value">0.5</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_grid_cell_source" id=":gX(Vg+9w)!T]gWZ!UX5">
                                                                                            <field name="#prop">cell.source</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="6:Vf5g,BQ~EP:s}%B`9%">
                                                                                                <field name="value">1</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_grid_random" id="6g|?$]v9k=!QlP9F2pl:">
                                                                                                <field name="#prop"></field>
                                                                                                <field name="random">FALSE</field>
                                                                                                <next>
                                                                                                  <block type="prop_grid_show_borders" id="-t4rS8~xsUV,`l5Z1lX(">
                                                                                                    <field name="#prop">#showBorders</field>
                                                                                                    <field name="value">FALSE</field>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_cell_template" id="B`Uvs-.4KWVyUov!mhN)">
                                                                                                        <field name="variable">$cell</field>
                                                                                                        <field name="#prop">cell.template</field>
                                                                                                        <field name="#callback">$cell</field>
                                                                                                        <statement name="body">
                                                                                                          <block type="image_shape" id="](D?np-^}AEYbshva1VS">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_position" id="vw(l@1Hp]6_6[|a}+]BzW">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="k|1BesPo#zCBCyOXNp">
                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="U2[==S)ahFs4_rj#C;A+">
                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_max_size" id="DegKTBB19G:hT.f4=n|3">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="maxWidth">
                                                                                                                      <block type="expression" id="0`552zRB6zw8i+T81A4q">
                                                                                                                        <field name="value">0</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="maxHeight">
                                                                                                                      <block type="expression" id="rn|XiB(PjVfxJB9u3mvc">
                                                                                                                        <field name="value">$cell.height</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_key" id="/Y|u4TFMoY0I2iD^yA%g">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="key">
                                                                                                                          <block type="string_value" id="0U(!S.U1@1k+r(Zv^;8;z">
                                                                                                                            <field name="value">${shape}.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_src" id="G#xbJ?B_4a-oMQA1+tL!">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="src">
                                                                                                                              <block type="string_value" id="nky@2i}N.~a;DnikVw2wL">
                                                                                                                                <field name="value">${image_path}${shape}.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="grid_shape" id="6tU?K^N8ec~A@1@1NwQ72N">
                                                                            <statement name="#props">
                                                                              <block type="prop_grid_dimension" id="X4K:|9dKK%s9l4n5v@2lG">
                                                                                <field name="#prop"></field>
                                                                                <value name="rows">
                                                                                  <block type="expression" id="[Ev!.bxnzjc|-33v.%7=">
                                                                                    <field name="value">2</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="cols">
                                                                                  <block type="expression" id="u$7]/V/!ZWM4(Va??@2({">
                                                                                    <field name="value">3</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_position" id="zLld84TfBg#!88]1,JD-">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="Cd6g{Y_P?:GyVyvQi1wJ">
                                                                                        <field name="value">400</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id=".F}E71+@1i)77!yQ`hmfu">
                                                                                        <field name="value">370</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_size" id="+o2CWr^.ohx)_bcIG53]">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="width">
                                                                                          <block type="expression" id="Z[x5iHG%KWC%KWQb}c-G">
                                                                                            <field name="value">750</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="height">
                                                                                          <block type="expression" id="@2E|Ob@1jF#gYtX8-%Iui@1">
                                                                                            <field name="value">130</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_anchor" id="1WJjXY^.h,t]eNgxaJ}q">
                                                                                            <field name="#prop">anchor</field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="DHx[vqs?.?nUD:AYPJWd">
                                                                                                <field name="value">0.5</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="e3D|RR7w!=Sufu):zV`c">
                                                                                                <field name="value">0.5</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_grid_cell_source" id="v8+Yu211Qg8fX9Mu4th`">
                                                                                                <field name="#prop">cell.source</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="~j@1,^liddf}oDg}E91^i">
                                                                                                    <field name="value">answer</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_grid_random" id="}?kS#x[b)9OGb@1`UdJGV">
                                                                                                    <field name="#prop"></field>
                                                                                                    <field name="random">FALSE</field>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_show_borders" id=";y^wk4y%[VWjXOj~vrQZ">
                                                                                                        <field name="#prop">#showBorders</field>
                                                                                                        <field name="value">FALSE</field>
                                                                                                        <next>
                                                                                                          <block type="prop_grid_cell_template" id="7^6]Kv:2yrZi`cooAW2@2">
                                                                                                            <field name="variable">$cell</field>
                                                                                                            <field name="#prop">cell.template</field>
                                                                                                            <field name="#callback">$cell</field>
                                                                                                            <statement name="body">
                                                                                                              <block type="if_then_else_block" id="~/{NC;N(M_ou,P06J00a">
                                                                                                                <value name="if">
                                                                                                                  <block type="expression" id="tUA6iUX|qd!pZ~Y1:|Ey">
                                                                                                                    <field name="value">shape == 9</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <statement name="then">
                                                                                                                  <block type="choice_custom_shape" id="E=U+(fR]T#57x!USG(MA">
                                                                                                                    <field name="action">click-many</field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="=B=TJ;(HwMb{Z]H@1lL(f">
                                                                                                                        <field name="value">$cell.data</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <statement name="template">
                                                                                                                      <block type="image_shape" id="A{@12#yroMtw75X)f-2@1K">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="pKfB[xWQTI))=i:Wg6ML">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="UOF-QyWZ,-PR81,-(O+e">
                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="Uj+PFGTucsX;-G6+L-/P">
                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_max_size" id="6hVb4lEuz/e[nBDt5@2%@1">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="maxWidth">
                                                                                                                                  <block type="expression" id="sVrcvf6;(513zmY7fy~q">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="maxHeight">
                                                                                                                                  <block type="expression" id="G7_Y1tfa@2^+N?ud8l?=_">
                                                                                                                                    <field name="value">$cell.height</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_key" id="~l|frk03)@26y?a|a|c20">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="key">
                                                                                                                                      <block type="string_value" id="rk0DeA`P{Ak8~_#~$MJJ">
                                                                                                                                        <field name="value">a${$cell.data}.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_src" id="w!oYdXmHVhmKb~5mW/Eg">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="src">
                                                                                                                                          <block type="string_value" id="190D6eUF[u3pn_hHRlIH">
                                                                                                                                            <field name="value">${image_path}a${$cell.data}.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <statement name="else">
                                                                                                                  <block type="choice_custom_shape" id="@1/eF9V2@2xXo!%Ve0D:x-">
                                                                                                                    <field name="action">click-one</field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="!AMxUkKZ`T{mJ4$xo?X)">
                                                                                                                        <field name="value">$cell.data</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <statement name="template">
                                                                                                                      <block type="image_shape" id=",#?MVp5n2~5_U^s:m,FM">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="!{3!Xop~G;QjC@2qoM)K%">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="Zf!p-EP+}!jz[H,(20?8">
                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="J2R85UsbS917cr9k#fwQ">
                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_max_size" id="8AK71OiYn/CC$?s|kARz">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="maxWidth">
                                                                                                                                  <block type="expression" id="8::QXBiX5ip8UP7-#Ü">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="maxHeight">
                                                                                                                                  <block type="expression" id="w!|`|)V|v)1T2IpGLjFt">
                                                                                                                                    <field name="value">$cell.height</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_key" id="|$K:RHg;hAht}1xA9GVG">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="key">
                                                                                                                                      <block type="string_value" id="WKxotHU?7e@2_,}=WKQM6">
                                                                                                                                        <field name="value">a${$cell.data}.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_src" id="o3t[#sHxTA::C/Vbvie8">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="src">
                                                                                                                                          <block type="string_value" id="Mr$P2g1=MuKU.CmT:fFz">
                                                                                                                                            <field name="value">${image_path}a${$cell.data}.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="func_add_answer" id="-5b!~`qt@1hH60@2$#yx_I" inline="true">
                                                                                <value name="value">
                                                                                  <block type="expression" id="B43y=oo5k6VvQD.6XaJ9">
                                                                                    <field name="value">ans_shape[shape]</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="variable" id="`Svq:[QSU;Z$U31?{rG0">
                                                                                    <field name="name">load</field>
                                                                                    <value name="value">
                                                                                      <block type="custom_image_list" id="egs8?6141HX@2_}9Be|?K">
                                                                                        <field name="link">${image_path}</field>
                                                                                        <field name="images">${shape}|${shape}_1|${shape}_2|arrow</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="if_then_else_block" id="i^KU(94`48+0i1C0Tig?">
                                                                                        <value name="if">
                                                                                          <block type="expression" id="Cs)XK`]v7=5u4yR$d)vW">
                                                                                            <field name="value">shape == 9</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <statement name="then">
                                                                                          <block type="variable" id="VTif)fV9dE2panIM@2S!%">
                                                                                            <field name="name">a1</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="uXv]!Kn;|HzXZX9|q_pB">
                                                                                                <field name="value">ans_shape[9][0]</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="variable" id="FTn@1/;y$|)1_)iqGYqt=">
                                                                                                <field name="name">a2</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="Ij.HS.0}X}CG{#/:(eTx">
                                                                                                    <field name="value">ans_shape[9][1]</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <statement name="else">
                                                                                          <block type="variable" id="P7~=Q@1;16D,O-:^vYsWK">
                                                                                            <field name="name">a1</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="xA@2mnG7LRFLcOB.g^MTV">
                                                                                                <field name="value">ans_shape[shape]</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="variable" id="Y=].NIUqYAIJI(Y#lH$A">
                                                                                                <field name="name">a2</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="YF.d(lOO4M1JNXI[y|IV">
                                                                                                    <field name="value">ans_shape[shape]</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="partial_explanation" id="ePDzD`e[u@2P%2PX~_97g" inline="true">
                                                                                            <value name="value">
                                                                                              <block type="string_value" id="8?ATbejy@1-RF1;##i(ec">
                                                                                                <field name="value">&lt;center&gt;
  &lt;u&gt;Identify the shapes that appear when the picture is cut into 2.&lt;/u&gt;
  
 
 &lt;style&gt;
  table {background: rgb(150, 200, 250, 0.3); width: 90%}
  td{text-align: center;}
  img{padding: 10px; max-height: 200px}
  &lt;/style&gt;

&lt;table&gt;
  &lt;tr&gt;
  &lt;td rowspan='2'&gt;&lt;img src='@1sprite.src(${shape})'/&gt;&lt;img src='@1sprite.src(arrow)'/&gt;&lt;/td&gt;
&lt;td&gt;&lt;img src='@1sprite.src(${shape}_1)'/&gt;${name_shape[a1]}&lt;/td&gt;
&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;img src='@1sprite.src(${shape}_2)'/&gt;${name_shape[a2]}&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
                                                                                                </field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="end_partial_explanation" id="5}nZ.`.v$/{-8@27jy9[B"></block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
  <block type="expression" id="L$3|wA4fkk7~,RX{NuEn" x="503" y="9767">
    <field name="value">collection</field>
  </block>
</xml>
END_XML]] */