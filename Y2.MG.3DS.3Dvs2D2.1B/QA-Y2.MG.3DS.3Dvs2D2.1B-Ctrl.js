
module.exports = [
  {
    "#type": "question",
    "name": "Y2.MG.3DS.3Dvs2D2.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y2.MG.3DS.3Dvs2D2.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "range < 5"
        },
        "then": [
          {
            "#type": "variable",
            "name": "type",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "1"
              },
              "max": {
                "#type": "expression",
                "value": "1"
              }
            }
          }
        ],
        "else": [
          {
            "#type": "variable",
            "name": "type",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "1"
              },
              "max": {
                "#type": "expression",
                "value": "2"
              }
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "shape",
        "value": {
          "#type": "string_array",
          "items": "x|triangular prism|rectangular prism|rectangular pyramid|cylinder|cone|cube"
        }
      },
      {
        "#type": "variable",
        "name": "sh",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "6"
          }
        }
      },
      {
        "#type": "statement",
        "value": "if(type == 1){\nvar Ans = [\n    [],\n    ['Triangle', 'Rectangle'],\n    ['Rectangle'],\n    ['Triangle', 'Square'],\n    ['Circle', 'Curved Surface'],\n    ['Circle', 'Curved Surface'],\n    ['Square'],\n  ];\n}\nelse {\nvar Ans = [\n    1,\n    'Triangle',\n    'Rectangle',\n    'Triangle',\n    'Circle',\n    'Circle',\n    'Square',\n];\n}"
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "type == 2"
        },
        "then": [
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "Name the shaded side on the 3D object below."
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "36"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "130"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "400"
                },
                "height": {
                  "#type": "expression",
                  "value": "150"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_max_size",
                        "#prop": "",
                        "maxWidth": {
                          "#type": "expression",
                          "value": "$cell.width"
                        },
                        "maxHeight": {
                          "#type": "expression",
                          "value": "$cell.height"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "2_${sh}.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}2_${sh}.png"
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "265"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "shape.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}shape.png"
              }
            ]
          },
          {
            "#type": "choice_input_shape",
            "#props": [
              {
                "#type": "prop_value",
                "#prop": "",
                "value": {
                  "#type": "expression",
                  "value": "Ans[sh]"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "265"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "530"
                },
                "height": {
                  "#type": "expression",
                  "value": "60"
                }
              },
              {
                "#type": "prop_input_keyboard",
                "#prop": "",
                "keyboard": "full_letters"
              },
              {
                "#type": "prop_input_max_length",
                "#prop": "",
                "maxLength": {
                  "#type": "expression",
                  "value": "Ans[sh].toString().length"
                }
              },
              {
                "#type": "prop_input_result_position",
                "#prop": "",
                "resultPosition": "bottom"
              },
              {
                "#type": "prop_input_type",
                "#prop": "",
                "dataType": "text-insensitive"
              },
              {
                "#type": "prop_tab_order",
                "#prop": "",
                "tabOrder": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_stroke",
                "#prop": "stroke"
              },
              {
                "#type": "prop_fill",
                "#prop": "fill"
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "32"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ],
            "#init": "algorithmic_input"
          },
          {
            "#type": "func",
            "name": "configureKeyboard",
            "args": [
              "full_letters",
              {
                "#type": "expression",
                "value": "0.5"
              },
              {
                "#type": "expression",
                "value": "1"
              },
              {
                "#type": "expression",
                "value": "400"
              },
              {
                "#type": "expression",
                "value": "450"
              },
              {
                "#type": "expression",
                "value": "0.7"
              }
            ]
          }
        ],
        "else": [
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "20"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "Identify the 2D shapes found in the 3D object below."
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "36"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "200"
                },
                "y": {
                  "#type": "expression",
                  "value": "250"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "400"
                },
                "height": {
                  "#type": "expression",
                  "value": "350"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_max_size",
                        "#prop": "",
                        "maxWidth": {
                          "#type": "expression",
                          "value": "$cell.width"
                        },
                        "maxHeight": {
                          "#type": "expression",
                          "value": "$cell.height"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "${sh}.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}${sh}.png"
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "5"
                },
                "cols": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "580"
                },
                "y": {
                  "#type": "expression",
                  "value": "250"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "400"
                },
                "height": {
                  "#type": "expression",
                  "value": "380"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "func",
                  "name": "shuffle",
                  "args": [
                    {
                      "#type": "expression",
                      "value": "['Circle', 'Square', 'Triangle', 'Rectangle', 'Curved Surface']"
                    }
                  ]
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "choice_custom_shape",
                    "action": "click-many",
                    "value": {
                      "#type": "expression",
                      "value": "$cell.data"
                    },
                    "template": {
                      "#callback": "$choice",
                      "variable": "$choice",
                      "body": [
                        {
                          "#type": "image_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "$cell.centerX"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "$cell.centerY"
                              }
                            },
                            {
                              "#type": "prop_max_size",
                              "#prop": "",
                              "maxWidth": {
                                "#type": "expression",
                                "value": "$cell.width"
                              },
                              "maxHeight": {
                                "#type": "expression",
                                "value": "$cell.height"
                              }
                            },
                            {
                              "#type": "prop_image_key",
                              "#prop": "",
                              "key": "${$cell.data.toLowerCase()}.png"
                            },
                            {
                              "#type": "prop_image_src",
                              "#prop": "",
                              "src": "${image_path}${$cell.data.toLowerCase()}.png"
                            }
                          ]
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "func",
            "name": "addAnswers",
            "args": [
              {
                "#type": "expression",
                "value": "Ans[sh]"
              }
            ]
          }
        ]
      },
      {
        "#type": "statement",
        "value": "function jsUcfirst(string) \n{\n    return string;\n}"
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "variable",
            "name": "Variable",
            "value": {
              "#type": "custom_image_list",
              "link": "${image_path}",
              "images": "1|2|3|4|5|6|arow|ex1|ex2|ex3|ex4|ex5|ex6"
            }
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 1: Get to know the different 3D objects and their faces or curve surfaces.</u></br></br>\n\n<style>\n  img{max-height: 150px; padding: 15px}\n      table{width: 100%; background: rgb(150, 200, 250, 0.4)}\n      td{text-align: center; border: 1px solid black;}\n    </style>\n    <center>\n      <table>\n        <tr>\n          <td>3D object</td>\n          <td><img src='@sprite.src(1)'/></td>\n          <td><img src='@sprite.src(2)'/></td>\n          <td><img src='@sprite.src(3)'/></td>\n          \n        </tr>\n        <tr>\n            <td>2D object</td>\n            <td>${Ans[1][0]}</br>${Ans[1][1]}</td>\n            <td>${Ans[2]}</td>\n            <td>${Ans[3][0]}</br>${Ans[3][1]}</td>\n            \n          </tr>\n\n          <tr>\n              <td>3D object</td>\n\t\t\t  <td><img src='@sprite.src(4)'/></td>\n              <td><img src='@sprite.src(5)'/></td>\n              <td><img src='@sprite.src(6)'/></td>\n            </tr>\n            <tr>\n                <td>2D object</td>\n                <td>${Ans[4][0]}</br>${Ans[4][1]}</td>\n                <td>${Ans[5][0]}</br>${Ans[5][1]}</td>\n                <td>${Ans[6]}</td>\n              </tr>\n      </table>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 2: Now let's look out for the 2D shapes in the ${shape[sh]}.</u></br></br>\n\n<style>\n  img {padding: 10px; max-height: 290px}\n  </style>\n<center>\n  <img style='max-height: 150px' src='@sprite.src(${sh})'/><img src='@sprite.src(arow)'/><img src='@sprite.src(ex${sh})'/>\n</br></br>\n<span style='background: rgb(250, 250, 250, 0.5)'>The ${shape[sh].toLowerCase()} has${' '}"
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < Ans[sh].length"
            },
            "do": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<b>${Ans[sh][i].toLowerCase()}</b>"
                ]
              },
              {
                "#type": "if_then_block",
                "if": {
                  "#type": "expression",
                  "value": "i < Ans[sh].length - 1"
                },
                "then": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "${', '}"
                    ]
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "${' '}in it.</span>\n</center>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          }
        ],
        "else": [
          {
            "#type": "variable",
            "name": "Variable",
            "value": {
              "#type": "custom_image_list",
              "link": "${image_path}",
              "images": "2_1|2_2|2_3|2_4|2_5|2_6|arow|2_ex1|2_ex2|2_ex3|2_ex4|2_ex5|2_ex6"
            }
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 1: Get to know the different 3D objects and their faces or curve surfaces.</u></br></br>\n\n<style>\n  img{max-height: 150px; padding: 15px}\n      table{width: 100%; background: rgb(150, 200, 250, 0.4)}\n      td{text-align: center; border: 1px solid black;}\n    </style>\n    <center>\n      <table>\n        <tr>\n          <td>3D object</td>\n          <td><img src='@sprite.src(2_1)'/></td>\n          <td><img src='@sprite.src(2_2)'/></td>\n          <td><img src='@sprite.src(2_3)'/></td>\n          \n        </tr>\n        <tr>\n            <td>2D object</td>\n            <td>${jsUcfirst(Ans[1])}</td>\n            <td>${jsUcfirst(Ans[2])}</td>\n            <td>${jsUcfirst(Ans[3])}</td>\n            \n          </tr>\n\n          <tr>\n              <td>3D object</td>\n\t\t\t  <td><img src='@sprite.src(2_4)'/></td>\n              <td><img src='@sprite.src(2_5)'/></td>\n              <td><img src='@sprite.src(2_6)'/></td>\n            </tr>\n            <tr>\n                <td>2D object</td>\n                <td>${jsUcfirst(Ans[4])}</td>\n            <td>${jsUcfirst(Ans[5])}</td>\n            <td>${jsUcfirst(Ans[6])}</td>\n              </tr>\n      </table>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 2: Now let's look out for the shaded 2D shape in the ${shape[sh]}.</u></br></br>\n\n<style>\n  img {padding: 10px; max-height: 290px}\n  </style>\n<center>\n  <img style='max-height: 150px' src='@sprite.src(2_${sh})'/><img src='@sprite.src(arow)'/><img src='@sprite.src(2_ex${sh})'/>\n</br></br>\n<span style='background: rgb(250, 250, 250, 0.5)'>The shaded 2D shape is a <b>${Ans[sh].toLowerCase()}</b>."
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          }
        ]
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="0" y="0">
    <field name="name">Y2.MG.3DS.3Dvs2D2.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y2.MG.3DS.3Dvs2D2.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="YepMQT@2uwp2rRPvmnRg1">
                                    <field name="name">range</field>
                                    <value name="value">
                                      <block type="expression" id="UxgQ^Y5]rb}vpw[D=l-!">
                                        <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="if_then_else_block" id="bOGamwi%G=@2L3e}vpx3^">
                                        <value name="if">
                                          <block type="expression" id="`fGQ8Sn:Ec.^rdxS8g:9">
                                            <field name="value">range &lt; 5</field>
                                          </block>
                                        </value>
                                        <statement name="then">
                                          <block type="variable" id="$fd-E!YfGvVjk}uo@1rwY">
                                            <field name="name">type</field>
                                            <value name="value">
                                              <block type="random_number" id="I|grK5q5y_-mthwy~qyr">
                                                <value name="min">
                                                  <block type="expression" id="jRHZYta=aKX#..RUNTzS">
                                                    <field name="value">1</field>
                                                  </block>
                                                </value>
                                                <value name="max">
                                                  <block type="expression" id="J^h?@1SWBc:GCBzh:pHq_">
                                                    <field name="value">1</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </value>
                                          </block>
                                        </statement>
                                        <statement name="else">
                                          <block type="variable" id="7DK^OFZ%[o7kLmRwTOcm">
                                            <field name="name">type</field>
                                            <value name="value">
                                              <block type="random_number" id="roh(y#+lr43se7$SL@1qz">
                                                <value name="min">
                                                  <block type="expression" id="hU5BbsooU}|sED@14(i[e">
                                                    <field name="value">1</field>
                                                  </block>
                                                </value>
                                                <value name="max">
                                                  <block type="expression" id="By,?2=O05+d;y@1OFCM6Q">
                                                    <field name="value">2</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </value>
                                          </block>
                                        </statement>
                                        <next>
                                          <block type="variable" id="z)BQ23I`Y:tL1XLh=@2Dz">
                                            <field name="name">shape</field>
                                            <value name="value">
                                              <block type="string_array" id="n{2@18EeRVXFv^(7P:e1v">
                                                <field name="items">x|triangular prism|rectangular prism|rectangular pyramid|cylinder|cone|cube</field>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id="@2g`/E%YH^Ig+AqK#~FC]">
                                                <field name="name">sh</field>
                                                <value name="value">
                                                  <block type="random_number" id="b6w},-B)Gz-.4f#7]@1.F">
                                                    <value name="min">
                                                      <block type="expression" id="T18Af|b:G_CMv_^@1x;?z">
                                                        <field name="value">1</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="k@2yHJgK([i::@2kJg7Otm">
                                                        <field name="value">6</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="statement" id="=[e_Vvps.q-{oO9wbYgr">
                                                    <field name="value">if(type == 1){
var Ans = [
    [],
    ['Triangle', 'Rectangle'],
    ['Rectangle'],
    ['Triangle', 'Square'],
    ['Circle', 'Curved Surface'],
    ['Circle', 'Curved Surface'],
    ['Square'],
  ];
}
else {
var Ans = [
    1,
    'Triangle',
    'Rectangle',
    'Triangle',
    'Circle',
    'Circle',
    'Square',
];
}
                                                    </field>
                                                    <next>
                                                      <block type="if_then_else_block" id="ibdsZN{MaGMEC%m#a)r|">
                                                        <value name="if">
                                                          <block type="expression" id="hd}JxnkfDUg|9Q_;.pTb">
                                                            <field name="value">type == 2</field>
                                                          </block>
                                                        </value>
                                                        <statement name="then">
                                                          <block type="text_shape" id="B?-s{RGv39P2bDQh1on;">
                                                            <statement name="#props">
                                                              <block type="prop_position" id="e~tY/-r.L^uZa0Z(pN[(">
                                                                <field name="#prop"></field>
                                                                <value name="x">
                                                                  <block type="expression" id="#p[[Qz0_h/VjEcdXP2~7">
                                                                    <field name="value">400</field>
                                                                  </block>
                                                                </value>
                                                                <value name="y">
                                                                  <block type="expression" id="EZZCXMy!5X5ur?cBWuhx">
                                                                    <field name="value">0</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_anchor" id="k]p.8QY[F#!MO|b8tWG@2">
                                                                    <field name="#prop">anchor</field>
                                                                    <value name="x">
                                                                      <block type="expression" id="d7~xB@1p]eqlN/13fRG5Y">
                                                                        <field name="value">0.5</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="y">
                                                                      <block type="expression" id="o8cyN?qSj1S:jDVD(sMX">
                                                                        <field name="value">0</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_text_contents" id="+HHYu27.#{j}+|%@1x96p">
                                                                        <field name="#prop"></field>
                                                                        <value name="contents">
                                                                          <block type="string_value" id="l9S+UIT=)?R${TMsL(.i">
                                                                            <field name="value">Name the shaded side on the 3D object below.</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_text_style" id="E@1GliKj@28?Y5O}1soA21">
                                                                            <field name="base">text</field>
                                                                            <statement name="#props">
                                                                              <block type="prop_text_style_font_size" id="@2YlmWJVV|/j`fiH`cvw8">
                                                                                <field name="#prop"></field>
                                                                                <value name="fontSize">
                                                                                  <block type="expression" id="_sopY0g:@11DZMM9BMKdH">
                                                                                    <field name="value">36</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_text_style_fill" id="pt3QQB7NyFqml5a5|mle">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="fill">
                                                                                      <block type="string_value" id="`7V|g@1,z/ZO|UVnJKn~E">
                                                                                        <field name="value">black</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_text_style_stroke" id=".k0n5A`Tr^g7t)l6{CNQ">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="stroke">
                                                                                          <block type="string_value" id=",4nI8KvQ^F?Q#ydG%/TA">
                                                                                            <field name="value">white</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_style_stroke_thickness" id="fZo@2eYV9j]s]S(9}:v-B">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="strokeThickness">
                                                                                              <block type="expression" id="rvSx)5ILL-j_/yoK}fG?">
                                                                                                <field name="value">2</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </statement>
                                                            <next>
                                                              <block type="grid_shape" id="wYAHY1}6D5s@1{aNN%Vhm">
                                                                <statement name="#props">
                                                                  <block type="prop_grid_dimension" id="l7zzGE_HOpdk02nUo@2$?">
                                                                    <field name="#prop"></field>
                                                                    <value name="rows">
                                                                      <block type="expression" id="|!MrA+?E44x3Q70e8FwT">
                                                                        <field name="value">1</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="cols">
                                                                      <block type="expression" id="kJ4QMaF3/{Pa`gq_pAwa">
                                                                        <field name="value">1</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_position" id="r@1BmIREwS@2zk5l;2PVMZ">
                                                                        <field name="#prop"></field>
                                                                        <value name="x">
                                                                          <block type="expression" id="F3S{utErryo{{[A6JsPU">
                                                                            <field name="value">400</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id="jv:fK5-i#Ei-bPe{fkE$">
                                                                            <field name="value">130</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_size" id="^NJkJzKTX-vp=0y?KnED">
                                                                            <field name="#prop"></field>
                                                                            <value name="width">
                                                                              <block type="expression" id="1hyTvUv;bV6r::MobIna">
                                                                                <field name="value">400</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="height">
                                                                              <block type="expression" id="@1ZvYgn:5b{@1PR!~-.U$l">
                                                                                <field name="value">150</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_anchor" id="3ggyqd$nYinV}bKVxxcs">
                                                                                <field name="#prop">anchor</field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="wEzy#@1v6OU@1O)CSK?pV6">
                                                                                    <field name="value">0.5</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="jy8H$YLT)3soPy3ZEG}%">
                                                                                    <field name="value">0.5</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_grid_cell_source" id="z5l;x||QI!IE~+}mXcvK">
                                                                                    <field name="#prop">cell.source</field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="`oZdd;eg3QtglnuK5)?}">
                                                                                        <field name="value">1</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_grid_random" id="`}}xR9-y(+s?bVI11Apf">
                                                                                        <field name="#prop"></field>
                                                                                        <field name="random">FALSE</field>
                                                                                        <next>
                                                                                          <block type="prop_grid_show_borders" id="2,+R7UO+?2YU,=)5ND)B">
                                                                                            <field name="#prop">#showBorders</field>
                                                                                            <field name="value">FALSE</field>
                                                                                            <next>
                                                                                              <block type="prop_grid_cell_template" id="??o9vH7(r|:xMJp[0N~D">
                                                                                                <field name="variable">$cell</field>
                                                                                                <field name="#prop">cell.template</field>
                                                                                                <field name="#callback">$cell</field>
                                                                                                <statement name="body">
                                                                                                  <block type="image_shape" id=":rjCm`]FLi%to]8v[!aB">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_position" id="9~?@1P{!56(La[wu[gorJ">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="x">
                                                                                                          <block type="expression" id="jn,slzpVy/oIBrhs^{uu">
                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id="Z(hCcthQL8(T/MCT@1=zK">
                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_max_size" id="u@1B@1:Jf_lkiC~(C(~|/A">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="maxWidth">
                                                                                                              <block type="expression" id="7fd=1Ne22G}37PotxRzA">
                                                                                                                <field name="value">$cell.width</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="maxHeight">
                                                                                                              <block type="expression" id="7Ni5}VIM6{DYiH3LCJ80">
                                                                                                                <field name="value">$cell.height</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_image_key" id="IbRj).?vhFCIf9=E]QU+">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="key">
                                                                                                                  <block type="string_value" id=",}nV-g2sN.aYKKAX9Xd@2">
                                                                                                                    <field name="value">2_${sh}.png</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_image_src" id="t,|^Epc@1G)$(E=S6^pf!">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="src">
                                                                                                                      <block type="string_value" id="sÛb%,E0IPmDf9Y!YEA">
                                                                                                                        <field name="value">${image_path}2_${sh}.png</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <next>
                                                                  <block type="image_shape" id="PDwO7RZoLJL$FlY%Gg#R">
                                                                    <statement name="#props">
                                                                      <block type="prop_position" id="|`.[_-Ks./yoH$1]k8l6">
                                                                        <field name="#prop"></field>
                                                                        <value name="x">
                                                                          <block type="expression" id="i|Qr4/Y`a1w@2%Mr[aBZy">
                                                                            <field name="value">400</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id="[}a59c49;=I({mdWn.3%">
                                                                            <field name="value">265</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_image_key" id="dNhGuZbQG.;en5!ypw|S">
                                                                            <field name="#prop"></field>
                                                                            <value name="key">
                                                                              <block type="string_value" id="@2-;@11}];KS4%^2hjU-2$">
                                                                                <field name="value">shape.png</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_image_src" id="0%s?`wAI8l$fRn5S7NJd">
                                                                                <field name="#prop"></field>
                                                                                <value name="src">
                                                                                  <block type="string_value" id="eePqz[}Fl$606s[x]A">
                                                                                    <field name="value">${image_path}shape.png</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="algorithmic_input_shape" id="[e}LcmqF@19.%T?]@1jB-Z">
                                                                        <statement name="#props">
                                                                          <block type="prop_value" id="TN6clfe}:4H;@1k0w%+`S">
                                                                            <field name="#prop"></field>
                                                                            <value name="value">
                                                                              <block type="expression" id="@1Y0=VVDuz^gR8?]A.GgV">
                                                                                <field name="value">Ans[sh]</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_position" id="(u^k=nloWKrc75-ihkQ6">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="+SQo+%UqGFz6@107iOi%4">
                                                                                    <field name="value">400</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="aOD#P0{Z$l:BCNh)9@2~E">
                                                                                    <field name="value">265</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_size" id="~)S$^+Glx|;{-)@1m?MCt">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="width">
                                                                                      <block type="expression" id="wdRRw?lccA]3huvks^0:">
                                                                                        <field name="value">530</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="height">
                                                                                      <block type="expression" id="FlQ@1R-hpt5|W3YEsjl5h">
                                                                                        <field name="value">60</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_input_keyboard" id="pX[+1]_P03Uw?r6RhxOm">
                                                                                        <field name="#prop"></field>
                                                                                        <field name="keyboard">full_letters</field>
                                                                                        <next>
                                                                                          <block type="prop_input_max_length" id="e^196@2S|6ukB7~2Bxi9C">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="maxLength">
                                                                                              <block type="expression" id="/_E;-7``ONG+]nS_NxH+">
                                                                                                <field name="value">Ans[sh].toString().length</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_input_result_position" id="#LH4fkex}OZhG#[@2d;3P">
                                                                                                <field name="#prop"></field>
                                                                                                <field name="resultPosition">bottom</field>
                                                                                                <next>
                                                                                                  <block type="prop_input_type" id="92s1GHyezc_DXP_P]99z">
                                                                                                    <field name="#prop"></field>
                                                                                                    <field name="dataType">text-insensitive</field>
                                                                                                    <next>
                                                                                                      <block type="prop_tab_order" id="s|F@1Izb{|@2lRY,asD!hu">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="tabOrder">
                                                                                                          <block type="expression" id="2[il+_dgVQKQm?SvtFDj">
                                                                                                            <field name="value">0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_stroke" id="D`nCliS/s1emx9euC0}t">
                                                                                                            <field name="#prop">stroke</field>
                                                                                                            <next>
                                                                                                              <block type="prop_fill" id="Dz-AaH/!wx?BO[MH9dIz">
                                                                                                                <field name="#prop">fill</field>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style" id="!1Qr3{IxD@1PMJ]G^LlcN">
                                                                                                                    <field name="base">text</field>
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_text_style_font_size" id="!(nlSpda9T+o/iXBj}YF">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="fontSize">
                                                                                                                          <block type="expression" id="FQTZZB_+$@1-hM?K.!NSO">
                                                                                                                            <field name="value">32</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_fill" id="w-4`M-zsemYrO[}g`_|q">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="fill">
                                                                                                                              <block type="string_value" id="G;VmNGg}u8OZR_#3M_Oe">
                                                                                                                                <field name="value">black</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_stroke" id="ahppz0%Y{eD@2C$!vyJ4m">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="stroke">
                                                                                                                                  <block type="string_value" id="}KNcD$1%M)+@1a=7A%7,,">
                                                                                                                                    <field name="value">white</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_stroke_thickness" id="TBWlBG{V@2q@2LOjXi7|@16">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="strokeThickness">
                                                                                                                                      <block type="expression" id="bs_awM@2W;X@1OG#fQ+@2no">
                                                                                                                                        <field name="value">2</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="configure_keyboard" id="USS4z10^fA+mox/mlc(@2">
                                                                            <field name="keyboard">full_letters</field>
                                                                            <value name="anchorX">
                                                                              <block type="expression" id="tIMzg6MFs~{CU~+QL!%F">
                                                                                <field name="value">0.5</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="anchorY">
                                                                              <block type="expression" id="whkUMUG2BUL:Rfx{ahyR">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="x">
                                                                              <block type="expression" id="6Dh8Tv(@1Q(X%%-J!.dl{">
                                                                                <field name="value">400</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="y">
                                                                              <block type="expression" id=",|wPt|;PZJRbwUU,L.AL">
                                                                                <field name="value">450</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="scale">
                                                                              <block type="expression" id="{z)=7{.Hx;H5sDCSum(}">
                                                                                <field name="value">0.7</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </statement>
                                                        <statement name="else">
                                                          <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                            <statement name="#props">
                                                              <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                                <field name="#prop"></field>
                                                                <value name="x">
                                                                  <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                                    <field name="value">400</field>
                                                                  </block>
                                                                </value>
                                                                <value name="y">
                                                                  <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                                    <field name="value">20</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_anchor" id="O.v@2mKn@1K#+tF;|}C}xI">
                                                                    <field name="#prop">anchor</field>
                                                                    <value name="x">
                                                                      <block type="expression" id="+3%GU[X3l%%X`Jv1QL7@2">
                                                                        <field name="value">0.5</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="y">
                                                                      <block type="expression" id="Kzp23HjHF7k]A-y1BJPm">
                                                                        <field name="value">0</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                                        <field name="#prop"></field>
                                                                        <value name="contents">
                                                                          <block type="string_value" id="#6btV9p,PGV}wMx.Uï¿½">
                                                                            <field name="value">Identify the 2D shapes found in the 3D object below.</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                            <field name="base">text</field>
                                                                            <statement name="#props">
                                                                              <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                                <field name="#prop"></field>
                                                                                <value name="fontSize">
                                                                                  <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                                    <field name="value">36</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="fill">
                                                                                      <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                                        <field name="value">black</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="stroke">
                                                                                          <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                            <field name="value">white</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="strokeThickness">
                                                                                              <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                                <field name="value">2</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </statement>
                                                            <next>
                                                              <block type="grid_shape" id="pZK{U1Sh.TUBksrg=,@1R">
                                                                <statement name="#props">
                                                                  <block type="prop_grid_dimension" id="U;[h%OxDljAe@2?CVCv#F">
                                                                    <field name="#prop"></field>
                                                                    <value name="rows">
                                                                      <block type="expression" id="CjgthFFe=GZ3ZbPB~$T:">
                                                                        <field name="value">1</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="cols">
                                                                      <block type="expression" id="cKoe@22n/WM`|7tP|g%pV">
                                                                        <field name="value">1</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_position" id="QukdeWcOB0FCR:l?!CQw">
                                                                        <field name="#prop"></field>
                                                                        <value name="x">
                                                                          <block type="expression" id=";Bm%/DmUp;/5^ZluLTQs">
                                                                            <field name="value">200</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id="=j`+]U#dG8cp}13MtT[}">
                                                                            <field name="value">250</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_size" id="APzVEA#e)Y78tmcpcf8M">
                                                                            <field name="#prop"></field>
                                                                            <value name="width">
                                                                              <block type="expression" id="w.Ux^_D?d3DR.@1lrSYW.">
                                                                                <field name="value">400</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="height">
                                                                              <block type="expression" id="DA+-Y5!Z(5gcUFgQTnV1">
                                                                                <field name="value">350</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_anchor" id="-+,8?_%`t|V@1]9K]VMhL">
                                                                                <field name="#prop">anchor</field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="~%.b4J{5U9)}eø){G}">
                                                                                    <field name="value">0.5</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="UDEzM8,/+SER3_SMCn=2">
                                                                                    <field name="value">0.5</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_grid_cell_source" id="!rHZiwWFBFE^!Y2h30j:">
                                                                                    <field name="#prop">cell.source</field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="Qgp,[wuk$XffmfLln%M}">
                                                                                        <field name="value">1</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_grid_random" id="gp)h.)bok!n:}gm)@2`Yo">
                                                                                        <field name="#prop"></field>
                                                                                        <field name="random">FALSE</field>
                                                                                        <next>
                                                                                          <block type="prop_grid_show_borders" id="ne:V+=y=rj[Vz+!@2R~Jg">
                                                                                            <field name="#prop">#showBorders</field>
                                                                                            <field name="value">FALSE</field>
                                                                                            <next>
                                                                                              <block type="prop_grid_cell_template" id="[Dtl7GzW93}D|j+Y+y-v">
                                                                                                <field name="variable">$cell</field>
                                                                                                <field name="#prop">cell.template</field>
                                                                                                <field name="#callback">$cell</field>
                                                                                                <statement name="body">
                                                                                                  <block type="image_shape" id="RttAj0ua_~)82;JuTWXt">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_position" id="sgYc_%$e2qCD0=Mo0GN[">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="x">
                                                                                                          <block type="expression" id="x/J79gycu54q7V2qZLUt">
                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id="-@1Q4vH^lGIrn2q-d]1|4">
                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_max_size" id="Qq$Kl6xjHZXR@14Wq^E:7">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="maxWidth">
                                                                                                              <block type="expression" id="u|j4YD@1hg@2!a5O+Pg^rU">
                                                                                                                <field name="value">$cell.width</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="maxHeight">
                                                                                                              <block type="expression" id="`a$5=IJpc`89#h$Q|s3n">
                                                                                                                <field name="value">$cell.height</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_image_key" id="}MGGY1x7m2@1TJ@2m=z=">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="key">
                                                                                                                  <block type="string_value" id="gxJF(eCeaT|OuUUnx|48">
                                                                                                                    <field name="value">${sh}.png</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_image_src" id="5zRH@22TKjy?;MnRyeXt:">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="src">
                                                                                                                      <block type="string_value" id="M)}F1-1TNt;A2g7|etii">
                                                                                                                        <field name="value">${image_path}${sh}.png</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <next>
                                                                  <block type="grid_shape" id="6tU?K^N8ec~A@1@1NwQ72N">
                                                                    <statement name="#props">
                                                                      <block type="prop_grid_dimension" id="X4K:|9dKK%s9l4n5v@2lG">
                                                                        <field name="#prop"></field>
                                                                        <value name="rows">
                                                                          <block type="expression" id="[Ev!.bxnzjc|-33v.%7=">
                                                                            <field name="value">5</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="cols">
                                                                          <block type="expression" id="u$7]/V/!ZWM4(Va??@2({">
                                                                            <field name="value">1</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_position" id="zLld84TfBg#!88]1,JD-">
                                                                            <field name="#prop"></field>
                                                                            <value name="x">
                                                                              <block type="expression" id="Cd6g{Y_P?:GyVyvQi1wJ">
                                                                                <field name="value">580</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="y">
                                                                              <block type="expression" id=".F}E71+@1i)77!yQ`hmfu">
                                                                                <field name="value">250</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_size" id="+o2CWr^.ohx)_bcIG53]">
                                                                                <field name="#prop"></field>
                                                                                <value name="width">
                                                                                  <block type="expression" id="Z[x5iHG%KWC%KWQb}c-G">
                                                                                    <field name="value">400</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="height">
                                                                                  <block type="expression" id="@2E|Ob@1jF#gYtX8-%Iui@1">
                                                                                    <field name="value">380</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_anchor" id="1WJjXY^.h,t]eNgxaJ}q">
                                                                                    <field name="#prop">anchor</field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="DHx[vqs?.?nUD:AYPJWd">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="e3D|RR7w!=Sufu):zV`c">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_grid_cell_source" id="v8+Yu211Qg8fX9Mu4th`">
                                                                                        <field name="#prop">cell.source</field>
                                                                                        <value name="value">
                                                                                          <block type="func_shuffle" id="s/RA}`Gb6}r7`;foVww_" inline="true">
                                                                                            <value name="value">
                                                                                              <block type="expression" id="sx^]Y(cEc3?SL$Q?@1yJ1">
                                                                                                <field name="value">['Circle', 'Square', 'Triangle', 'Rectangle', 'Curved Surface']</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_grid_random" id="}?kS#x[b)9OGb@1`UdJGV">
                                                                                            <field name="#prop"></field>
                                                                                            <field name="random">FALSE</field>
                                                                                            <next>
                                                                                              <block type="prop_grid_show_borders" id=";y^wk4y%[VWjXOj~vrQZ">
                                                                                                <field name="#prop">#showBorders</field>
                                                                                                <field name="value">FALSE</field>
                                                                                                <next>
                                                                                                  <block type="prop_grid_cell_template" id="7^6]Kv:2yrZi`cooAW2@2">
                                                                                                    <field name="variable">$cell</field>
                                                                                                    <field name="#prop">cell.template</field>
                                                                                                    <field name="#callback">$cell</field>
                                                                                                    <statement name="body">
                                                                                                      <block type="choice_custom_shape" id="bG^v2V9EmGigm%wwJ@2}4">
                                                                                                        <field name="action">click-many</field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id="X%^;{}}RZ,8U)u;mB4TZ">
                                                                                                            <field name="value">$cell.data</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <statement name="template">
                                                                                                          <block type="image_shape" id="aFi@2Z8-_}}V:wJZS53q8">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_position" id="MEmeqHEpHdM@1U5u /!">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="7VPb0LZ6/Ô|:W}B@1if">
                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="IQrN0}9tq6499U}+[l2(">
                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_max_size" id="rAY%WC7z{+pPb?sviqB|">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="maxWidth">
                                                                                                                      <block type="expression" id="DBee/Ol)_^9_p,K6RFuW">
                                                                                                                        <field name="value">$cell.width</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="maxHeight">
                                                                                                                      <block type="expression" id="vP.CC;O7D9m/^Q{s7C)l">
                                                                                                                        <field name="value">$cell.height</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_key" id="$?H2hCB5i;BUt-D2ZhsF">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="key">
                                                                                                                          <block type="string_value" id="0U(!S.U1@1k+r(Zv^;8;z">
                                                                                                                            <field name="value">${$cell.data.toLowerCase()}.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_src" id="C$.,?fKzGvvw#:#4ERS7">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="src">
                                                                                                                              <block type="string_value" id="nky@2i}N.~a;DnikVw2wL">
                                                                                                                                <field name="value">${image_path}${$cell.data.toLowerCase()}.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="func_add_answer" id="u@1b=t@1E(~uM-Q0SILlQ@2" inline="true">
                                                                        <value name="value">
                                                                          <block type="expression" id="A$@2F9^Cn$O~c_w2q5/+!">
                                                                            <field name="value">Ans[sh]</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </statement>
                                                        <next>
                                                          <block type="statement" id="+;|(u3G+D=mBNK5ekClg">
                                                            <field name="value">function jsUcfirst(string) 
{
    return string;
}
                                                            </field>
                                                            <next>
                                                              <block type="if_then_else_block" id="%ZL-r)|FA}:Co.~uRKqN">
                                                                <value name="if">
                                                                  <block type="expression" id="#,9!?mB@1U!%ax~t0V#C_">
                                                                    <field name="value">type == 1</field>
                                                                  </block>
                                                                </value>
                                                                <statement name="then">
                                                                  <block type="variable" id="8i=H8C.tBf[99gGqaC;#">
                                                                    <field name="name">Variable</field>
                                                                    <value name="value">
                                                                      <block type="custom_image_list" id="))svSS$ypRuz})y7@2R4F">
                                                                        <field name="link">${image_path}</field>
                                                                        <field name="images">1|2|3|4|5|6|arow|ex1|ex2|ex3|ex4|ex5|ex6</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="partial_explanation" id="we.WCP;K6C;/~k]s?ifw" inline="true">
                                                                        <value name="value">
                                                                          <block type="string_value" id="mZ{L6DY4{_L@2f9au?Y3#">
                                                                            <field name="value">&lt;u&gt;Step 1: Get to know the different 3D objects and their faces or curve surfaces.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
  img{max-height: 150px; padding: 15px}
      table{width: 100%; background: rgb(150, 200, 250, 0.4)}
      td{text-align: center; border: 1px solid black;}
    &lt;/style&gt;
    &lt;center&gt;
      &lt;table&gt;
        &lt;tr&gt;
          &lt;td&gt;3D object&lt;/td&gt;
          &lt;td&gt;&lt;img src='@1sprite.src(1)'/&gt;&lt;/td&gt;
          &lt;td&gt;&lt;img src='@1sprite.src(2)'/&gt;&lt;/td&gt;
          &lt;td&gt;&lt;img src='@1sprite.src(3)'/&gt;&lt;/td&gt;
          
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td&gt;2D object&lt;/td&gt;
            &lt;td&gt;${Ans[1][0]}&lt;/br&gt;${Ans[1][1]}&lt;/td&gt;
            &lt;td&gt;${Ans[2]}&lt;/td&gt;
            &lt;td&gt;${Ans[3][0]}&lt;/br&gt;${Ans[3][1]}&lt;/td&gt;
            
          &lt;/tr&gt;

          &lt;tr&gt;
              &lt;td&gt;3D object&lt;/td&gt;
			  &lt;td&gt;&lt;img src='@1sprite.src(4)'/&gt;&lt;/td&gt;
              &lt;td&gt;&lt;img src='@1sprite.src(5)'/&gt;&lt;/td&gt;
              &lt;td&gt;&lt;img src='@1sprite.src(6)'/&gt;&lt;/td&gt;
            &lt;/tr&gt;
            &lt;tr&gt;
                &lt;td&gt;2D object&lt;/td&gt;
                &lt;td&gt;${Ans[4][0]}&lt;/br&gt;${Ans[4][1]}&lt;/td&gt;
                &lt;td&gt;${Ans[5][0]}&lt;/br&gt;${Ans[5][1]}&lt;/td&gt;
                &lt;td&gt;${Ans[6]}&lt;/td&gt;
              &lt;/tr&gt;
      &lt;/table&gt;
                                                                            </field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="end_partial_explanation" id="8cR_;[i}9~x3ULM1~uLd">
                                                                            <next>
                                                                              <block type="partial_explanation" id="$8{.#fJfp@2k?}@2W;vtwu" inline="true">
                                                                                <value name="value">
                                                                                  <block type="string_value" id="OPm(OQFk5!5f[rT79hC7">
                                                                                    <field name="value">&lt;u&gt;Step 2: Now let's look out for the 2D shapes in the ${shape[sh]}.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
  img {padding: 10px; max-height: 290px}
  &lt;/style&gt;
&lt;center&gt;
  &lt;img style='max-height: 150px' src='@1sprite.src(${sh})'/&gt;&lt;img src='@1sprite.src(arow)'/&gt;&lt;img src='@1sprite.src(ex${sh})'/&gt;
&lt;/br&gt;&lt;/br&gt;
&lt;span style='background: rgb(250, 250, 250, 0.5)'&gt;The ${shape[sh].toLowerCase()} has${' '}
                                                                                    </field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="variable" id="vL^tCyol)QKnWdKL9j;`">
                                                                                    <field name="name">i</field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="e=vN?}WOf0Pe[LcbuG~J">
                                                                                        <field name="value">0</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="while_do_block" id="9Pu;(~N6r!Y/OcV0k@1+z">
                                                                                        <value name="while">
                                                                                          <block type="expression" id="vQ0gRJA~X:cq{8ZHDW`R">
                                                                                            <field name="value">i &lt; Ans[sh].length</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <statement name="do">
                                                                                          <block type="partial_explanation" id="?k#h#3UJL|t=+A!~$K]w" inline="true">
                                                                                            <value name="value">
                                                                                              <block type="string_value" id="K^Ny2l1L:Pr+2dif+aeq">
                                                                                                <field name="value">&lt;b&gt;${Ans[sh][i].toLowerCase()}&lt;/b&gt;</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="if_then_block" id="_M{y@2nB1Zl.xZaRjUnR9">
                                                                                                <value name="if">
                                                                                                  <block type="expression" id="=XtyuYv~8Q4~@1R^Zj#S#">
                                                                                                    <field name="value">i &lt; Ans[sh].length - 1</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <statement name="then">
                                                                                                  <block type="partial_explanation" id="4n_!t!HftD03P?Bl|.-~" inline="true">
                                                                                                    <value name="value">
                                                                                                      <block type="string_value" id="If+BwG!c|nFcU.w?r}M[">
                                                                                                        <field name="value">${', '}</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="variable" id="^`VY6oyz[.)ho-yb0btF">
                                                                                                    <field name="name">i</field>
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="jA.0i|U{MXI8nRm8;A)h">
                                                                                                        <field name="value">i+1</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="partial_explanation" id="FOJ0SaZJ:M/N6.#-ab^{" inline="true">
                                                                                            <value name="value">
                                                                                              <block type="string_value" id="h,AS`zZiS-yF)?m7~~#n">
                                                                                                <field name="value">${' '}in it.&lt;/span&gt;
&lt;/center&gt;
                                                                                                </field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="end_partial_explanation" id="M6RcsvB,.}K5:!1w`t`#"></block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <statement name="else">
                                                                  <block type="variable" id="XD6FQzqng=#cnz-QELR~">
                                                                    <field name="name">Variable</field>
                                                                    <value name="value">
                                                                      <block type="custom_image_list" id="=:vkBC{uWOQfSbvBF^xF">
                                                                        <field name="link">${image_path}</field>
                                                                        <field name="images">2_1|2_2|2_3|2_4|2_5|2_6|arow|2_ex1|2_ex2|2_ex3|2_ex4|2_ex5|2_ex6</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="partial_explanation" id="AUgbUD}@2K^Qbo^OUkR!9" inline="true">
                                                                        <value name="value">
                                                                          <block type="string_value" id="~PxO4hBSZIp[ssIS1$e7">
                                                                            <field name="value">&lt;u&gt;Step 1: Get to know the different 3D objects and their faces or curve surfaces.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
  img{max-height: 150px; padding: 15px}
      table{width: 100%; background: rgb(150, 200, 250, 0.4)}
      td{text-align: center; border: 1px solid black;}
    &lt;/style&gt;
    &lt;center&gt;
      &lt;table&gt;
        &lt;tr&gt;
          &lt;td&gt;3D object&lt;/td&gt;
          &lt;td&gt;&lt;img src='@1sprite.src(2_1)'/&gt;&lt;/td&gt;
          &lt;td&gt;&lt;img src='@1sprite.src(2_2)'/&gt;&lt;/td&gt;
          &lt;td&gt;&lt;img src='@1sprite.src(2_3)'/&gt;&lt;/td&gt;
          
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td&gt;2D object&lt;/td&gt;
            &lt;td&gt;${jsUcfirst(Ans[1])}&lt;/td&gt;
            &lt;td&gt;${jsUcfirst(Ans[2])}&lt;/td&gt;
            &lt;td&gt;${jsUcfirst(Ans[3])}&lt;/td&gt;
            
          &lt;/tr&gt;

          &lt;tr&gt;
              &lt;td&gt;3D object&lt;/td&gt;
			  &lt;td&gt;&lt;img src='@1sprite.src(2_4)'/&gt;&lt;/td&gt;
              &lt;td&gt;&lt;img src='@1sprite.src(2_5)'/&gt;&lt;/td&gt;
              &lt;td&gt;&lt;img src='@1sprite.src(2_6)'/&gt;&lt;/td&gt;
            &lt;/tr&gt;
            &lt;tr&gt;
                &lt;td&gt;2D object&lt;/td&gt;
                &lt;td&gt;${jsUcfirst(Ans[4])}&lt;/td&gt;
            &lt;td&gt;${jsUcfirst(Ans[5])}&lt;/td&gt;
            &lt;td&gt;${jsUcfirst(Ans[6])}&lt;/td&gt;
              &lt;/tr&gt;
      &lt;/table&gt;
                                                                            </field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="end_partial_explanation" id="UObu=u((LmNy3}4NPIVk">
                                                                            <next>
                                                                              <block type="partial_explanation" id="L1m`B7yD7~{(NpnyR|L4" inline="true">
                                                                                <value name="value">
                                                                                  <block type="string_value" id="jWUs;PWD58Dn:ene_dOu">
                                                                                    <field name="value">&lt;u&gt;Step 2: Now let's look out for the shaded 2D shape in the ${shape[sh]}.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
  img {padding: 10px; max-height: 290px}
  &lt;/style&gt;
&lt;center&gt;
  &lt;img style='max-height: 150px' src='@1sprite.src(2_${sh})'/&gt;&lt;img src='@1sprite.src(arow)'/&gt;&lt;img src='@1sprite.src(2_ex${sh})'/&gt;
&lt;/br&gt;&lt;/br&gt;
&lt;span style='background: rgb(250, 250, 250, 0.5)'&gt;The shaded 2D shape is a &lt;b&gt;${Ans[sh].toLowerCase()}&lt;/b&gt;.
                                                                                    </field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="end_partial_explanation" id="k7{jhxoTs0H]wTE:[+Nd"></block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
  <block type="partial_explanation" id="`?N2A^Kv[zFW$`/BmsI?" inline="true" x="120" y="5026">
    <value name="value">
      <block type="string_value" id="[TtN^No|DBf~{12~}$kk">
        <field name="value">&lt;u&gt;Step 1: Get to know the different 3D objects and their faces or curve surfaces.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
  img{max-height: 150px; padding: 15px}
      table{width: 100%; background: rgb(150, 200, 250, 0.4)}
      td{text-align: center; border: 1px solid black;}
    &lt;/style&gt;
    &lt;center&gt;
      &lt;table&gt;
        &lt;tr&gt;
          &lt;td&gt;3D object&lt;/td&gt;
          &lt;td&gt;&lt;img src='@1sprite.src(1)'/&gt;&lt;/td&gt;
          &lt;td&gt;&lt;img src='@1sprite.src(2)'/&gt;&lt;/td&gt;
          &lt;td&gt;&lt;img src='@1sprite.src(3)'/&gt;&lt;/td&gt;
          &lt;td&gt;&lt;img src='@1sprite.src(4)'/&gt;&lt;/td&gt;
        &lt;/tr&gt;
        &lt;tr&gt;
            &lt;td&gt;2D object&lt;/td&gt;
            &lt;td&gt;${jsUcfirst(Ans[1])}&lt;/td&gt;
            &lt;td&gt;${jsUcfirst(Ans[2])}&lt;/td&gt;
            &lt;td&gt;${jsUcfirst(Ans[3])}&lt;/td&gt;
            &lt;td&gt;${jsUcfirst(Ans[4])}&lt;/td&gt;
          &lt;/tr&gt;

          &lt;tr&gt;
              &lt;td&gt;3D object&lt;/td&gt;
              &lt;td&gt;&lt;img src='@1sprite.src(5)'/&gt;&lt;/td&gt;
              &lt;td&gt;&lt;img src='@1sprite.src(6)'/&gt;&lt;/td&gt;
              &lt;td&gt;&lt;img src='@1sprite.src(7)'/&gt;&lt;/td&gt;
            &lt;/tr&gt;
            &lt;tr&gt;
                &lt;td&gt;2D object&lt;/td&gt;
                &lt;td&gt;${jsUcfirst(Ans[5])}&lt;/td&gt;
                &lt;td&gt;${jsUcfirst(Ans[6])}&lt;/td&gt;
                &lt;td&gt;${jsUcfirst(Ans[7])}&lt;/td&gt;
              &lt;/tr&gt;
      &lt;/table&gt;
        </field>
      </block>
    </value>
  </block>
</xml>
END_XML]] */