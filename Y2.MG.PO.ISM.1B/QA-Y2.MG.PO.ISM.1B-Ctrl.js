
module.exports = [
  {
    "#type": "question",
    "name": "Y2.MG.PO.ISM.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y2.MG.PO.ISM.1B"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "place",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "2"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "${place}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}${place}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "range < 4"
        },
        "then": [
          {
            "#type": "variable",
            "name": "dif",
            "value": {
              "#type": "expression",
              "value": "3"
            }
          },
          {
            "#type": "variable",
            "name": "number",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "5"
              },
              "max": {
                "#type": "expression",
                "value": "10"
              }
            }
          },
          {
            "#type": "variable",
            "name": "max",
            "value": {
              "#type": "expression",
              "value": "18"
            }
          }
        ],
        "else": [
          {
            "#type": "variable",
            "name": "dif",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "4"
              },
              "max": {
                "#type": "expression",
                "value": "6"
              }
            }
          },
          {
            "#type": "variable",
            "name": "number",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "10"
              },
              "max": {
                "#type": "expression",
                "value": "20"
              }
            }
          },
          {
            "#type": "variable",
            "name": "max",
            "value": {
              "#type": "expression",
              "value": "28"
            }
          }
        ]
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "place == 1"
        },
        "then": [
          {
            "#type": "variable",
            "name": "list_object",
            "value": {
              "#type": "string_array",
              "items": "tree|trashcan|picnic table|stop sign|slide|swing|bicycle|balloon|football|basketball|skateboard"
            }
          },
          {
            "#type": "variable",
            "name": "place",
            "value": {
              "#type": "expression",
              "value": "'park'"
            }
          },
          {
            "#type": "variable",
            "name": "p",
            "value": {
              "#type": "expression",
              "value": "1"
            }
          }
        ],
        "else": [
          {
            "#type": "variable",
            "name": "list_object",
            "value": {
              "#type": "string_array",
              "items": "trashcan|picnic table|slide|swing|beach umbrella|beach towel|bicycle|balloon|football|basketball|skateboard"
            }
          },
          {
            "#type": "variable",
            "name": "place",
            "value": {
              "#type": "expression",
              "value": "'beach'"
            }
          },
          {
            "#type": "variable",
            "name": "p",
            "value": {
              "#type": "expression",
              "value": "2"
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "object",
        "value": {
          "#type": "random_many",
          "count": {
            "#type": "expression",
            "value": "dif"
          },
          "items": {
            "#type": "func",
            "name": "arrayOfNumber",
            "args": [
              {
                "#type": "expression",
                "value": "11"
              },
              {
                "#type": "expression",
                "value": "0"
              }
            ]
          }
        }
      },
      {
        "#type": "variable",
        "name": "array_ob",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "ox",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "oy",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < number"
        },
        "do": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "i < dif"
            },
            "then": [
              {
                "#type": "variable",
                "name": "array_ob[i]",
                "value": {
                  "#type": "expression",
                  "value": "object[i]"
                }
              },
              {
                "#type": "variable",
                "name": "ox[i]",
                "value": {
                  "#type": "random_one",
                  "items": {
                    "#type": "expression",
                    "value": "[-5, -4, -3, -2, -1, 1, 2,3 , 4, 5]"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "oy[i]",
                "value": {
                  "#type": "random_one",
                  "items": {
                    "#type": "expression",
                    "value": "[-5, -4, -3, -2, -1, 1, 2,3 , 4, 5]"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "loadAssets",
                "value": {
                  "#type": "custom_image_list",
                  "link": "${image_path}",
                  "images": "${p}_${object[i]}"
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "array_ob[i]",
                "value": {
                  "#type": "random_one",
                  "items": {
                    "#type": "expression",
                    "value": "object"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "ox[i]",
                "value": {
                  "#type": "random_one",
                  "items": {
                    "#type": "expression",
                    "value": "[-5, -4, -3, -2, -1, 1, 2,3 , 4, 5]"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "oy[i]",
                "value": {
                  "#type": "random_one",
                  "items": {
                    "#type": "expression",
                    "value": "[-5, -4, -3, -2, -1, 1, 2,3 , 4, 5]"
                  }
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < max"
        },
        "do": [
          {
            "#type": "variable",
            "name": "array_ob[i]",
            "value": {
              "#type": "expression",
              "value": "-1"
            }
          },
          {
            "#type": "variable",
            "name": "ox[i]",
            "value": {
              "#type": "random_one",
              "items": {
                "#type": "expression",
                "value": "[-5, -4, -3, -2, -1, 1, 2,3 , 4, 5]"
              }
            }
          },
          {
            "#type": "variable",
            "name": "oy[i]",
            "value": {
              "#type": "random_one",
              "items": {
                "#type": "expression",
                "value": "[-5, -4, -3, -2, -1, 1, 2,3 , 4, 5]"
              }
            }
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "array_ob",
        "value": {
          "#type": "func",
          "name": "shuffle",
          "args": [
            {
              "#type": "expression",
              "value": "array_ob"
            }
          ]
        }
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "10"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "Study the ${place} and answer the question below."
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "36"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "max == 18 ? 3 : 4"
            },
            "cols": {
              "#type": "expression",
              "value": "max == 18 ? 6 : 7"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "180"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "780"
            },
            "height": {
              "#type": "expression",
              "value": "230"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "func",
              "name": "arrayOfNumber",
              "args": [
                {
                  "#type": "expression",
                  "value": "max"
                },
                {
                  "#type": "expression",
                  "value": "0"
                }
              ]
            }
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_cell_template",
            "variable": "$cell",
            "#prop": "cell.template",
            "#callback": "$cell",
            "body": [
              {
                "#type": "if_then_block",
                "if": {
                  "#type": "expression",
                  "value": "array_ob[$cell.data] >= 0"
                },
                "then": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX+ox[$cell.data]"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY+oy[$cell.data]"
                        }
                      },
                      {
                        "#type": "prop_max_size",
                        "#prop": "",
                        "maxWidth": {
                          "#type": "expression",
                          "value": "$cell.width - 10"
                        },
                        "maxHeight": {
                          "#type": "expression",
                          "value": "$cell.height - 10"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "${p}_${array_ob[$cell.data]}.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}${p}_${array_ob[$cell.data]}.png"
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "type",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "3"
          }
        }
      },
      {
        "#type": "statement",
        "value": "function count_x (x, A) {\n    var count = 0;\n    for(var i = 0; i< A.length; i++){\n        if(A[i] == x){count++;}\n    }\n    return count;\n}\n\nfunction check(x, A) {\n    for(var m=0; m<A.length; m++){\n        if(x == A[m]){\n            return 1;\n        }\n    }\n    return 0;\n}"
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "list_shape",
            "#props": [
              {
                "#type": "prop_list_direction",
                "#prop": "",
                "dir": "horizontal"
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "350"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              }
            ],
            "items": [
              {
                "#type": "json",
                "#props": [
                  {
                    "#type": "prop_list_align",
                    "#prop": "",
                    "align": "middle"
                  }
                ],
                "template": {
                  "#callback": "$item",
                  "variable": "$item",
                  "body": [
                    {
                      "#type": "text_shape",
                      "#props": [
                        {
                          "#type": "prop_anchor",
                          "#prop": "anchor",
                          "x": {
                            "#type": "expression",
                            "value": "0.5"
                          },
                          "y": {
                            "#type": "expression",
                            "value": "0"
                          }
                        },
                        {
                          "#type": "prop_text_contents",
                          "#prop": "",
                          "contents": "How many ${list_object[object[0]]} are there in the ${place}?${' '}"
                        },
                        {
                          "#prop": "",
                          "style": {
                            "#type": "json",
                            "base": "text",
                            "#props": [
                              {
                                "#type": "prop_text_style_font_size",
                                "#prop": "",
                                "fontSize": {
                                  "#type": "expression",
                                  "value": "36"
                                }
                              },
                              {
                                "#type": "prop_text_style_fill",
                                "#prop": "",
                                "fill": "black"
                              },
                              {
                                "#type": "prop_text_style_stroke",
                                "#prop": "",
                                "stroke": "white"
                              },
                              {
                                "#type": "prop_text_style_stroke_thickness",
                                "#prop": "",
                                "strokeThickness": {
                                  "#type": "expression",
                                  "value": "2"
                                }
                              }
                            ]
                          }
                        }
                      ]
                    }
                  ]
                }
              },
              {
                "#type": "json",
                "#props": [
                  {
                    "#type": "prop_list_align",
                    "#prop": "",
                    "align": "middle"
                  }
                ],
                "template": {
                  "#callback": "$item",
                  "variable": "$item",
                  "body": [
                    {
                      "#type": "image_shape",
                      "#props": [
                        {
                          "#type": "prop_size",
                          "#prop": "",
                          "width": {
                            "#type": "expression",
                            "value": "90"
                          },
                          "height": {
                            "#type": "expression",
                            "value": "60"
                          }
                        },
                        {
                          "#type": "prop_image_key",
                          "#prop": "",
                          "key": "shape.png"
                        },
                        {
                          "#type": "prop_image_src",
                          "#prop": "",
                          "src": "${image_path}shape.png"
                        }
                      ]
                    },
                    {
                      "#type": "choice_input_shape",
                      "#props": [
                        {
                          "#type": "prop_value",
                          "#prop": "",
                          "value": {
                            "#type": "expression",
                            "value": "count_x(object[0], array_ob)"
                          }
                        },
                        {
                          "#type": "prop_size",
                          "#prop": "",
                          "width": {
                            "#type": "expression",
                            "value": "90"
                          },
                          "height": {
                            "#type": "expression",
                            "value": "60"
                          }
                        },
                        {
                          "#type": "prop_input_keyboard",
                          "#prop": "",
                          "keyboard": "numbers1"
                        },
                        {
                          "#type": "prop_input_max_length",
                          "#prop": "",
                          "maxLength": {
                            "#type": "expression",
                            "value": "count_x(object[0], array_ob).toString().length"
                          }
                        },
                        {
                          "#type": "prop_input_result_position",
                          "#prop": "",
                          "resultPosition": "bottom"
                        },
                        {
                          "#type": "prop_tab_order",
                          "#prop": "",
                          "tabOrder": {
                            "#type": "expression",
                            "value": "0"
                          }
                        },
                        {
                          "#type": "prop_stroke",
                          "#prop": "stroke"
                        },
                        {
                          "#type": "prop_fill",
                          "#prop": "fill"
                        },
                        {
                          "#prop": "",
                          "style": {
                            "#type": "json",
                            "base": "text",
                            "#props": [
                              {
                                "#type": "prop_text_style_font_size",
                                "#prop": "",
                                "fontSize": {
                                  "#type": "expression",
                                  "value": "36"
                                }
                              },
                              {
                                "#type": "prop_text_style_fill",
                                "#prop": "",
                                "fill": "black"
                              },
                              {
                                "#type": "prop_text_style_stroke",
                                "#prop": "",
                                "stroke": "white"
                              },
                              {
                                "#type": "prop_text_style_stroke_thickness",
                                "#prop": "",
                                "strokeThickness": {
                                  "#type": "expression",
                                  "value": "2"
                                }
                              }
                            ]
                          }
                        }
                      ],
                      "#init": "algorithmic_input"
                    }
                  ]
                }
              }
            ]
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "type == 2"
            },
            "then": [
              {
                "#type": "variable",
                "name": "qa",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "0"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "1"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "variable",
                "name": "dk",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "dk == 0"
                },
                "do": [
                  {
                    "#type": "if_then_else_block",
                    "if": {
                      "#type": "expression",
                      "value": "count_x(object[i], array_ob) != count_x(object[i+1], array_ob)"
                    },
                    "then": [
                      {
                        "#type": "variable",
                        "name": "dk",
                        "value": {
                          "#type": "expression",
                          "value": "1"
                        }
                      },
                      {
                        "#type": "if_then_else_block",
                        "if": {
                          "#type": "expression",
                          "value": "count_x(object[0], array_ob) > count_x(object[1], array_ob)"
                        },
                        "then": [
                          {
                            "#type": "variable",
                            "name": "more",
                            "value": {
                              "#type": "expression",
                              "value": "object[i]"
                            }
                          },
                          {
                            "#type": "variable",
                            "name": "less",
                            "value": {
                              "#type": "expression",
                              "value": "object[i+1]"
                            }
                          }
                        ],
                        "else": [
                          {
                            "#type": "variable",
                            "name": "more",
                            "value": {
                              "#type": "expression",
                              "value": "object[i+1]"
                            }
                          },
                          {
                            "#type": "variable",
                            "name": "less",
                            "value": {
                              "#type": "expression",
                              "value": "object[i]"
                            }
                          }
                        ]
                      }
                    ],
                    "else": [
                      {
                        "#type": "variable",
                        "name": "dk",
                        "value": {
                          "#type": "expression",
                          "value": "0"
                        }
                      },
                      {
                        "#type": "variable",
                        "name": "i",
                        "value": {
                          "#type": "expression",
                          "value": "i+1"
                        }
                      }
                    ]
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "400"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "320"
                    }
                  },
                  {
                    "#type": "prop_anchor",
                    "#prop": "anchor",
                    "x": {
                      "#type": "expression",
                      "value": "0.5"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "There are more ${qa == 1 ? list_object[more]:list_object[less]}s than ${qa == 1 ? list_object[less]:list_object[more]}s in the ${place}."
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "36"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "grid_shape",
                "#props": [
                  {
                    "#type": "prop_grid_dimension",
                    "#prop": "",
                    "rows": {
                      "#type": "expression",
                      "value": "1"
                    },
                    "cols": {
                      "#type": "expression",
                      "value": "2"
                    }
                  },
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "400"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "400"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "600"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "60"
                    }
                  },
                  {
                    "#type": "prop_anchor",
                    "#prop": "anchor",
                    "x": {
                      "#type": "expression",
                      "value": "0.5"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "0.5"
                    }
                  },
                  {
                    "#type": "prop_grid_cell_source",
                    "#prop": "cell.source",
                    "value": {
                      "#type": "expression",
                      "value": "['true', 'false']"
                    }
                  },
                  {
                    "#type": "prop_grid_random",
                    "#prop": "",
                    "random": false
                  },
                  {
                    "#type": "prop_grid_show_borders",
                    "#prop": "#showBorders",
                    "value": false
                  },
                  {
                    "#type": "prop_grid_cell_template",
                    "variable": "$cell",
                    "#prop": "cell.template",
                    "#callback": "$cell",
                    "body": [
                      {
                        "#type": "choice_custom_shape",
                        "action": "click-one",
                        "value": {
                          "#type": "expression",
                          "value": "$cell.data"
                        },
                        "template": {
                          "#callback": "$choice",
                          "variable": "$choice",
                          "body": [
                            {
                              "#type": "image_shape",
                              "#props": [
                                {
                                  "#type": "prop_position",
                                  "#prop": "",
                                  "x": {
                                    "#type": "expression",
                                    "value": "$cell.centerX"
                                  },
                                  "y": {
                                    "#type": "expression",
                                    "value": "$cell.centerY"
                                  }
                                },
                                {
                                  "#type": "prop_image_key",
                                  "#prop": "",
                                  "key": "${$cell.data}.png"
                                },
                                {
                                  "#type": "prop_image_src",
                                  "#prop": "",
                                  "src": "${image_path}${$cell.data}.png"
                                }
                              ]
                            }
                          ]
                        }
                      }
                    ]
                  }
                ]
              },
              {
                "#type": "func",
                "name": "addAnswers",
                "args": [
                  {
                    "#type": "expression",
                    "value": "qa == 1 ? 'true' : 'false'"
                  }
                ]
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "qa",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "0"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "1"
                  }
                }
              },
              {
                "#type": "if_then_else_block",
                "if": {
                  "#type": "expression",
                  "value": "qa == 0"
                },
                "then": [
                  {
                    "#type": "variable",
                    "name": "ob",
                    "value": {
                      "#type": "random_one",
                      "items": {
                        "#type": "expression",
                        "value": "object"
                      }
                    }
                  }
                ],
                "else": [
                  {
                    "#type": "do_while_block",
                    "do": [
                      {
                        "#type": "variable",
                        "name": "ob",
                        "value": {
                          "#type": "random_one",
                          "items": {
                            "#type": "func",
                            "name": "arrayOfNumber",
                            "args": [
                              {
                                "#type": "expression",
                                "value": "11"
                              },
                              {
                                "#type": "expression",
                                "value": "0"
                              }
                            ]
                          }
                        }
                      }
                    ],
                    "while": {
                      "#type": "expression",
                      "value": "check(ob, object) == 1"
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "400"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "320"
                    }
                  },
                  {
                    "#type": "prop_anchor",
                    "#prop": "anchor",
                    "x": {
                      "#type": "expression",
                      "value": "0.5"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "The ${place} does not have any ${list_object[ob]}s."
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "36"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "grid_shape",
                "#props": [
                  {
                    "#type": "prop_grid_dimension",
                    "#prop": "",
                    "rows": {
                      "#type": "expression",
                      "value": "1"
                    },
                    "cols": {
                      "#type": "expression",
                      "value": "2"
                    }
                  },
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "400"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "400"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "600"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "60"
                    }
                  },
                  {
                    "#type": "prop_anchor",
                    "#prop": "anchor",
                    "x": {
                      "#type": "expression",
                      "value": "0.5"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "0.5"
                    }
                  },
                  {
                    "#type": "prop_grid_cell_source",
                    "#prop": "cell.source",
                    "value": {
                      "#type": "expression",
                      "value": "['true', 'false']"
                    }
                  },
                  {
                    "#type": "prop_grid_random",
                    "#prop": "",
                    "random": false
                  },
                  {
                    "#type": "prop_grid_show_borders",
                    "#prop": "#showBorders",
                    "value": false
                  },
                  {
                    "#type": "prop_grid_cell_template",
                    "variable": "$cell",
                    "#prop": "cell.template",
                    "#callback": "$cell",
                    "body": [
                      {
                        "#type": "choice_custom_shape",
                        "action": "click-one",
                        "value": {
                          "#type": "expression",
                          "value": "$cell.data"
                        },
                        "template": {
                          "#callback": "$choice",
                          "variable": "$choice",
                          "body": [
                            {
                              "#type": "image_shape",
                              "#props": [
                                {
                                  "#type": "prop_position",
                                  "#prop": "",
                                  "x": {
                                    "#type": "expression",
                                    "value": "$cell.centerX"
                                  },
                                  "y": {
                                    "#type": "expression",
                                    "value": "$cell.centerY"
                                  }
                                },
                                {
                                  "#type": "prop_image_key",
                                  "#prop": "",
                                  "key": "${$cell.data}.png"
                                },
                                {
                                  "#type": "prop_image_src",
                                  "#prop": "",
                                  "src": "${image_path}${$cell.data}.png"
                                }
                              ]
                            }
                          ]
                        }
                      }
                    ]
                  }
                ]
              },
              {
                "#type": "func",
                "name": "addAnswers",
                "args": [
                  {
                    "#type": "expression",
                    "value": "qa == 1 ? 'true' : 'false'"
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "loadAssets",
        "value": {
          "#type": "custom_image_list",
          "link": "${image_path}",
          "images": "${p}"
        }
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 1: Study the ${place} carefully. Look out for the different items in the ${place}.</u></br>\n<style>\n  table{background-image: url(\"@sprite.src(${p})\"); width: 800px; height: 450px;}\ntd{text-align: center;  width: 100px}\nimg{max-width: 70px; max-height: 60px}\n\n  </style>\n<center>\n  <table>\n    <tr><td colspan=${max==18?6:7}></td></tr>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < (max==18?3:4)"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<tr>"
            ]
          },
          {
            "#type": "variable",
            "name": "j",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "j < (max==18?6:7)"
            },
            "do": [
              {
                "#type": "if_then_else_block",
                "if": {
                  "#type": "expression",
                  "value": "array_ob[(max==18?6:7)*i + j] >= 0"
                },
                "then": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<td><img src='@sprite.src(${p}_${array_ob[(max==18?6:7)*i + j]})'/></td>"
                    ]
                  }
                ],
                "else": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<td></td>"
                    ]
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "j",
                "value": {
                  "#type": "expression",
                  "value": "j+1"
                }
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</tr>"
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<tr><td colspan=${max==18?6:7}></td></tr></table></center>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 2: Now let's count the number of ${list_object[object[0]]} in the ${place}.</u></br>\n<style>\n  table{background-image: url(\"@sprite.src(${p})\"); width: 800px; height: 450px;}\ntd{text-align: center;width: 100px}\nimg{max-width: 70px; max-height: 60px}\n#choise {border: 3px solid green; border-radius: 40px; display: inline-block; width: 70px; height: 70px}\n  </style>\n<center>\n  <table>\n    <tr><td colspan=${max==18?6:7}></td></tr>"
            ]
          },
          {
            "#type": "variable",
            "name": "count",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < (max==18?3:4)"
            },
            "do": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<tr>"
                ]
              },
              {
                "#type": "variable",
                "name": "j",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "j < (max==18?6:7)"
                },
                "do": [
                  {
                    "#type": "if_then_else_block",
                    "if": {
                      "#type": "expression",
                      "value": "array_ob[(max==18?6:7)*i + j] >= 0"
                    },
                    "then": [
                      {
                        "#type": "if_then_else_block",
                        "if": {
                          "#type": "expression",
                          "value": "array_ob[(max==18?6:7)*i + j] == object[0]"
                        },
                        "then": [
                          {
                            "#type": "func",
                            "name": "addPartialExplanation",
                            "args": [
                              "<td><div id='choise'><img style='padding: 5px' src='@sprite.src(${p}_${array_ob[(max==18?6:7)*i + j]})'/></div> ${count+1}</td>"
                            ]
                          },
                          {
                            "#type": "variable",
                            "name": "count",
                            "value": {
                              "#type": "expression",
                              "value": "count +1"
                            }
                          }
                        ],
                        "else": [
                          {
                            "#type": "func",
                            "name": "addPartialExplanation",
                            "args": [
                              "<td><img src='@sprite.src(${p}_${array_ob[(max==18?6:7)*i + j]})'/></td>"
                            ]
                          }
                        ]
                      }
                    ],
                    "else": [
                      {
                        "#type": "func",
                        "name": "addPartialExplanation",
                        "args": [
                          "<td></td>"
                        ]
                      }
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "j",
                    "value": {
                      "#type": "expression",
                      "value": "j+1"
                    }
                  }
                ]
              },
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</tr>"
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<tr><td colspan=${max==18?6:7}></td></tr></table>\n  </br>\n<span style='background: rgb(250, 250, 250, 0.5)'>\nThe number of <b>${list_object[object[0]]}${count_x(object[0], array_ob) < 2 ? '':'s'}</b> in the ${place} is <b>${count_x(object[0], array_ob)}</b>.</span>\n</center>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "type == 2"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<u>Step 2: Now let's count the number of ${qa == 1 ? list_object[more]:list_object[less]} in the ${place}.</u></br>\n<style>\n  table{background-image: url(\"@sprite.src(${p})\"); width: 800px; height: 450px;}\ntd{text-align: center;width: 100px}\nimg{max-width: 70px; max-height: 60px}\n#choise {border: 3px solid green; border-radius: 40px; display: inline-block; width: 70px; height: 70px}\n  </style>\n<center>\n  <table>\n    <tr><td colspan=${max==18?6:7}></td></tr>"
                ]
              },
              {
                "#type": "variable",
                "name": "count",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "i < (max==18?3:4)"
                },
                "do": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<tr>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "j",
                    "value": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "while_do_block",
                    "while": {
                      "#type": "expression",
                      "value": "j < (max==18?6:7)"
                    },
                    "do": [
                      {
                        "#type": "if_then_else_block",
                        "if": {
                          "#type": "expression",
                          "value": "array_ob[(max==18?6:7)*i + j] >= 0"
                        },
                        "then": [
                          {
                            "#type": "if_then_else_block",
                            "if": {
                              "#type": "expression",
                              "value": "array_ob[(max==18?6:7)*i + j] == (qa == 1 ? more:less)"
                            },
                            "then": [
                              {
                                "#type": "func",
                                "name": "addPartialExplanation",
                                "args": [
                                  "<td><div id='choise'><img style='padding: 5px' src='@sprite.src(${p}_${array_ob[(max==18?6:7)*i + j]})'/></div> ${count+1}</td>"
                                ]
                              },
                              {
                                "#type": "variable",
                                "name": "count",
                                "value": {
                                  "#type": "expression",
                                  "value": "count +1"
                                }
                              }
                            ],
                            "else": [
                              {
                                "#type": "func",
                                "name": "addPartialExplanation",
                                "args": [
                                  "<td><img src='@sprite.src(${p}_${array_ob[(max==18?6:7)*i + j]})'/></td>"
                                ]
                              }
                            ]
                          }
                        ],
                        "else": [
                          {
                            "#type": "func",
                            "name": "addPartialExplanation",
                            "args": [
                              "<td></td>"
                            ]
                          }
                        ]
                      },
                      {
                        "#type": "variable",
                        "name": "j",
                        "value": {
                          "#type": "expression",
                          "value": "j+1"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "</tr>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "i",
                    "value": {
                      "#type": "expression",
                      "value": "i+1"
                    }
                  }
                ]
              },
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<tr><td colspan=${max==18?6:7}></td></tr></table>\n  </br>\n<span style='background: rgb(250, 250, 250, 0.5)'>\nThe number of ${qa == 1 ? list_object[more]:list_object[less]}${count_x((qa == 1 ? more:less), array_ob) < 2 ? '' : 's'} in the ${place} is <b style='color: green'>${count_x((qa == 1 ? more:less), array_ob)}</b>.</span>\n</center>"
                ]
              },
              {
                "#type": "func",
                "name": "endPartialExplanation",
                "args": []
              },
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<u>Step 3: Next count the number of ${qa == 1 ? list_object[less]:list_object[more]} in the ${place}.</u></br>\n<style>\n  table{background-image: url(\"@sprite.src(${p})\"); width: 800px; height: 450px;}\ntd{text-align: center;width: 100px}\nimg{max-width: 70px; max-height: 60px}\n#choise {border: 3px solid red; border-radius: 40px; display: inline-block; width: 70px; height: 70px}\n  </style>\n<center>\n  <table>\n    <tr><td colspan=${max==18?6:7}></td></tr>"
                ]
              },
              {
                "#type": "variable",
                "name": "count",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "i < (max==18?3:4)"
                },
                "do": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<tr>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "j",
                    "value": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "while_do_block",
                    "while": {
                      "#type": "expression",
                      "value": "j < (max==18?6:7)"
                    },
                    "do": [
                      {
                        "#type": "if_then_else_block",
                        "if": {
                          "#type": "expression",
                          "value": "array_ob[(max==18?6:7)*i + j] >= 0"
                        },
                        "then": [
                          {
                            "#type": "if_then_else_block",
                            "if": {
                              "#type": "expression",
                              "value": "array_ob[(max==18?6:7)*i + j] == (qa == 1 ? less:more)"
                            },
                            "then": [
                              {
                                "#type": "func",
                                "name": "addPartialExplanation",
                                "args": [
                                  "<td><div id='choise'><img style='padding: 5px' src='@sprite.src(${p}_${array_ob[(max==18?6:7)*i + j]})'/></div> ${count+1}</td>"
                                ]
                              },
                              {
                                "#type": "variable",
                                "name": "count",
                                "value": {
                                  "#type": "expression",
                                  "value": "count +1"
                                }
                              }
                            ],
                            "else": [
                              {
                                "#type": "func",
                                "name": "addPartialExplanation",
                                "args": [
                                  "<td><img src='@sprite.src(${p}_${array_ob[(max==18?6:7)*i + j]})'/></td>"
                                ]
                              }
                            ]
                          }
                        ],
                        "else": [
                          {
                            "#type": "func",
                            "name": "addPartialExplanation",
                            "args": [
                              "<td></td>"
                            ]
                          }
                        ]
                      },
                      {
                        "#type": "variable",
                        "name": "j",
                        "value": {
                          "#type": "expression",
                          "value": "j+1"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "</tr>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "i",
                    "value": {
                      "#type": "expression",
                      "value": "i+1"
                    }
                  }
                ]
              },
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<tr><td colspan=${max==18?6:7}></td></tr></table>\n  </br>\n<span style='background: rgb(250, 250, 250, 0.5)'>\nThe number of ${qa == 1 ? list_object[less]:list_object[more]}${count_x((qa == 1 ? less:more), array_ob) < 2 ? '':'s'} in the ${place} is <b style='color: red'>${count_x((qa == 1 ? less:more), array_ob)}</b>.</span>\n</center>"
                ]
              },
              {
                "#type": "func",
                "name": "endPartialExplanation",
                "args": []
              },
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<u>Step 4: Finally choose if there are more ${qa == 1 ? list_object[more]:list_object[less]} than ${qa == 1 ? list_object[less]:list_object[more]} in the ${place}.</u></br></br>\n\n<b style='color: green'>${count_x((qa == 1 ? more:less), array_ob)}</b> ${qa == 1? 'more than':'less than'} <b style='color: red'>${count_x((qa == 1 ? less:more), array_ob)}</b>.</br></br>\nIt is <b>${qa == 1? 'true':'false'}</b> that there are more <b>${qa == 1 ? list_object[more]:list_object[less]}</b> than <b>${qa == 1 ? list_object[less]:list_object[more]}</b> in the ${place}."
                ]
              }
            ],
            "else": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<u>Step 2: Now let's count the number of ${list_object[ob]} in the ${place}.</u></br>\n<style>\n  table{background-image: url(\"@sprite.src(${p})\"); width: 800px; height: 450px;}\ntd{text-align: center;width: 100px}\nimg{max-width: 70px; max-height: 60px}\n#choise {border: 3px solid green; border-radius: 40px; display: inline-block; width: 70px; height: 70px}\n  </style>\n<center>\n  <table>\n    <tr><td colspan=${max==18?6:7}></td></tr>"
                ]
              },
              {
                "#type": "variable",
                "name": "count",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "i < (max==18?3:4)"
                },
                "do": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<tr>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "j",
                    "value": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "while_do_block",
                    "while": {
                      "#type": "expression",
                      "value": "j < (max==18?6:7)"
                    },
                    "do": [
                      {
                        "#type": "if_then_else_block",
                        "if": {
                          "#type": "expression",
                          "value": "array_ob[(max==18?6:7)*i + j] >= 0"
                        },
                        "then": [
                          {
                            "#type": "if_then_else_block",
                            "if": {
                              "#type": "expression",
                              "value": "array_ob[(max==18?6:7)*i + j] == ob"
                            },
                            "then": [
                              {
                                "#type": "func",
                                "name": "addPartialExplanation",
                                "args": [
                                  "<td><div id='choise'><img style='padding: 5px' src='@sprite.src(${p}_${array_ob[(max==18?6:7)*i + j]})'/></div> ${count+1}</td>"
                                ]
                              },
                              {
                                "#type": "variable",
                                "name": "count",
                                "value": {
                                  "#type": "expression",
                                  "value": "count +1"
                                }
                              }
                            ],
                            "else": [
                              {
                                "#type": "func",
                                "name": "addPartialExplanation",
                                "args": [
                                  "<td><img src='@sprite.src(${p}_${array_ob[(max==18?6:7)*i + j]})'/></td>"
                                ]
                              }
                            ]
                          }
                        ],
                        "else": [
                          {
                            "#type": "func",
                            "name": "addPartialExplanation",
                            "args": [
                              "<td></td>"
                            ]
                          }
                        ]
                      },
                      {
                        "#type": "variable",
                        "name": "j",
                        "value": {
                          "#type": "expression",
                          "value": "j+1"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "</tr>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "i",
                    "value": {
                      "#type": "expression",
                      "value": "i+1"
                    }
                  }
                ]
              },
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<tr><td colspan=${max==18?6:7}></td></tr></table>\n  </br>\n<span style='background: rgb(250, 250, 250, 0.5)'>\nThe number of <b>${list_object[ob]}${count_x(ob, array_ob) < 2 ? '':'s'}</b> in the ${place} is <b>${count_x(ob, array_ob)}</b>.</br>\nIt is <b>${qa == 1? 'true':'false'}</b> that the ${place} does not have any <b>${list_object[ob]}s</b>.</span>\n</center>"
                ]
              },
              {
                "#type": "func",
                "name": "endPartialExplanation",
                "args": []
              }
            ]
          }
        ]
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="0" y="0">
    <field name="name">Y2.MG.PO.ISM.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y2.MG.PO.ISM.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
            <field name="name">image_path</field>
            <value name="value">
              <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                <field name="value">Develop/ImageQAs/${concept_code}/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="643xJE9DI2eZ4HrwetPT">
                <field name="name">place</field>
                <value name="value">
                  <block type="random_number" id="4Kr5o8LH3)TtAG!@2$HlM">
                    <value name="min">
                      <block type="expression" id="%g2dY80~_Te5w7opqWW.">
                        <field name="value">1</field>
                      </block>
                    </value>
                    <value name="max">
                      <block type="expression" id="9dXMXaN_g8Ql.GSw#o?Y">
                        <field name="value">2</field>
                      </block>
                    </value>
                  </block>
                </value>
                <next>
                  <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                    <statement name="#props">
                      <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                        <field name="#prop"></field>
                        <value name="key">
                          <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                            <field name="value">${place}.png</field>
                          </block>
                        </value>
                        <next>
                          <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                            <field name="#prop"></field>
                            <value name="src">
                              <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                <field name="value">${image_path}${place}.png</field>
                              </block>
                            </value>
                          </block>
                        </next>
                      </block>
                    </statement>
                    <next>
                      <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                        <field name="name">numberOfCorrect</field>
                        <value name="value">
                          <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                            <field name="value">0</field>
                          </block>
                        </value>
                        <next>
                          <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                            <field name="name">numberOfIncorrect</field>
                            <value name="value">
                              <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="variable" id="YepMQT@2uwp2rRPvmnRg1">
                                <field name="name">range</field>
                                <value name="value">
                                  <block type="expression" id="UxgQ^Y5]rb}vpw[D=l-!">
                                    <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="if_then_else_block" id="c4D5vF^3mfpozOpdUPd%">
                                    <value name="if">
                                      <block type="expression" id="K]VYY_@2BuW!Xd4XO8VEl">
                                        <field name="value">range &lt; 4</field>
                                      </block>
                                    </value>
                                    <statement name="then">
                                      <block type="variable" id="eVo#+mS(4e/6n#MP,`th">
                                        <field name="name">dif</field>
                                        <value name="value">
                                          <block type="expression" id="~reZE:n5,G^9cqhA@1ib`">
                                            <field name="value">3</field>
                                          </block>
                                        </value>
                                        <next>
                                          <block type="variable" id="Xkn)6A)^nwyRo+5F[Ogc">
                                            <field name="name">number</field>
                                            <value name="value">
                                              <block type="random_number" id="8UHKgJO3nAe:`70MaMG%">
                                                <value name="min">
                                                  <block type="expression" id="O5vr0-7m?9,qNJ2iercr">
                                                    <field name="value">5</field>
                                                  </block>
                                                </value>
                                                <value name="max">
                                                  <block type="expression" id="-72LT[M]`U!beMhYa-zQ">
                                                    <field name="value">10</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id="xj^/rF!86zCUr^?^Ew6a">
                                                <field name="name">max</field>
                                                <value name="value">
                                                  <block type="expression" id="MMreT53sdMda1kM.N9qw">
                                                    <field name="value">18</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </statement>
                                    <statement name="else">
                                      <block type="variable" id="/#hjx{0xxZMX.Z/Tob~5">
                                        <field name="name">dif</field>
                                        <value name="value">
                                          <block type="random_number" id="-TjZm7Lw6,UG%K^:VR!}">
                                            <value name="min">
                                              <block type="expression" id="]W)U;).$xerCZeTf@1LHM">
                                                <field name="value">4</field>
                                              </block>
                                            </value>
                                            <value name="max">
                                              <block type="expression" id="k!SgzirZJ9`F5)!aFUN/">
                                                <field name="value">6</field>
                                              </block>
                                            </value>
                                          </block>
                                        </value>
                                        <next>
                                          <block type="variable" id="N|Rj($ETrBndA4PK[YbZ">
                                            <field name="name">number</field>
                                            <value name="value">
                                              <block type="random_number" id="?Mn^v?/ekG.xS{@2AO@2`#">
                                                <value name="min">
                                                  <block type="expression" id="):_GO^dQ;$0:3-wa3@1,O">
                                                    <field name="value">10</field>
                                                  </block>
                                                </value>
                                                <value name="max">
                                                  <block type="expression" id="+}!kse{sZjC_7k3;@1,w+">
                                                    <field name="value">20</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id="XfH@2?EY5Mt_#b{j=tke9">
                                                <field name="name">max</field>
                                                <value name="value">
                                                  <block type="expression" id="LqS}geeNp$+QJ-nrSCV0">
                                                    <field name="value">28</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </statement>
                                    <next>
                                      <block type="if_then_else_block" id="(BJ`$a{Y52dRu,!7ThLQ">
                                        <value name="if">
                                          <block type="expression" id="LFa.b?b=a(e+,_Bmo#yj">
                                            <field name="value">place == 1</field>
                                          </block>
                                        </value>
                                        <statement name="then">
                                          <block type="variable" id="^6LSq/d^$}(~/~_!Ee?_">
                                            <field name="name">list_object</field>
                                            <value name="value">
                                              <block type="string_array" id="ISYlVzAXYhXa|)Vu~kkL">
                                                <field name="items">tree|trashcan|picnic table|stop sign|slide|swing|bicycle|balloon|football|basketball|skateboard</field>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id=",69L5dR?ugTWPCf[O6a]">
                                                <field name="name">place</field>
                                                <value name="value">
                                                  <block type="expression" id="PBqp]A@1xT@1wjVsw)~[AT">
                                                    <field name="value">'park'</field>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="H#-[f%X5+Ef43)@1(7nh@1">
                                                    <field name="name">p</field>
                                                    <value name="value">
                                                      <block type="expression" id="zJpGvbN[b2g|!?T5$Bxr">
                                                        <field name="value">1</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </statement>
                                        <statement name="else">
                                          <block type="variable" id="QS..F(`D%}IUj)BtqsZk">
                                            <field name="name">list_object</field>
                                            <value name="value">
                                              <block type="string_array" id="e4XkpM(o5W.DA/Xbj]}|">
                                                <field name="items">trashcan|picnic table|slide|swing|beach umbrella|beach towel|bicycle|balloon|football|basketball|skateboard</field>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id="3U+1X+sOPh1f2:-G$TBw">
                                                <field name="name">place</field>
                                                <value name="value">
                                                  <block type="expression" id="A!;NgqP@2Q.2v+LM6U{Y3">
                                                    <field name="value">'beach'</field>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="?p6g0S1MwspOO6%M(">
                                                    <field name="name">p</field>
                                                    <value name="value">
                                                      <block type="expression" id=",aKLSMU?~Zd877LJE2yg">
                                                        <field name="value">2</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </statement>
                                        <next>
                                          <block type="variable" id="(vjN%qvZF1}p2pR+Z~I[">
                                            <field name="name">object</field>
                                            <value name="value">
                                              <block type="random_many" id=",24TV@1:$8?n}]p0[W//=">
                                                <value name="count">
                                                  <block type="expression" id="_Wxn5Rk;/QH=_P=vN39H">
                                                    <field name="value">dif</field>
                                                  </block>
                                                </value>
                                                <value name="items">
                                                  <block type="func_array_of_number" id="cn3sr[({yrqxI!+5@1;.{" inline="true">
                                                    <value name="items">
                                                      <block type="expression" id="jvap5gY1;1{:q%e?iI@2K">
                                                        <field name="value">11</field>
                                                      </block>
                                                    </value>
                                                    <value name="from">
                                                      <block type="expression" id="zZBw-9YTmgt$[N%dnMx[">
                                                        <field name="value">0</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id="$$$1rgXW3U/Dzp}xY1+g">
                                                <field name="name">array_ob</field>
                                                <value name="value">
                                                  <block type="expression" id="=@1@1WtyMew2[e)ci+QS+]">
                                                    <field name="value">[]</field>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="{wQ7F%B(onv#|[cQ@1R|3">
                                                    <field name="name">ox</field>
                                                    <value name="value">
                                                      <block type="expression" id="u1{[X@1kdjuD}yvJ;rwi(">
                                                        <field name="value">[]</field>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id="Ll5A:Tms.JT6,li(^#G`">
                                                        <field name="name">oy</field>
                                                        <value name="value">
                                                          <block type="expression" id="b/hKmtcfZe![|kzU2+c[">
                                                            <field name="value">[]</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="@2=d=G|z5?!@2ozMRk)T`c">
                                                            <field name="name">i</field>
                                                            <value name="value">
                                                              <block type="expression" id="3e}Wt;[sJN-wN}ts%S3m">
                                                                <field name="value">0</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="while_do_block" id="%v:B@2+nr&#10;+FQ+8+1~C">
                                                                <value name="while">
                                                                  <block type="expression" id="2^py71xQj3TSG,:dPw67">
                                                                    <field name="value">i &lt; number</field>
                                                                  </block>
                                                                </value>
                                                                <statement name="do">
                                                                  <block type="if_then_else_block" id="/:Kzn.a6^@2w.KPhLyV`R">
                                                                    <value name="if">
                                                                      <block type="expression" id="{yJ^#b@1@1Rl642!y}Z]~%">
                                                                        <field name="value">i &lt; dif</field>
                                                                      </block>
                                                                    </value>
                                                                    <statement name="then">
                                                                      <block type="variable" id="+UcsX6vDf$|TruKIMfUp">
                                                                        <field name="name">array_ob[i]</field>
                                                                        <value name="value">
                                                                          <block type="expression" id="W`swmM:KYVnnKLzf@19OF">
                                                                            <field name="value">object[i]</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="JPN:IrDwl.g~UB2JUNAo">
                                                                            <field name="name">ox[i]</field>
                                                                            <value name="value">
                                                                              <block type="random_one" id="xy#+O?F.+q1|nck/UKbn">
                                                                                <value name="items">
                                                                                  <block type="expression" id="f=.hOYYjO/;thtG-njgs">
                                                                                    <field name="value">[-5, -4, -3, -2, -1, 1, 2,3 , 4, 5]</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="variable" id="@1/F||J]?iTK6%;ZnJwTK">
                                                                                <field name="name">oy[i]</field>
                                                                                <value name="value">
                                                                                  <block type="random_one" id="7+)ELK,Q3aAif|Dg`BfF">
                                                                                    <value name="items">
                                                                                      <block type="expression" id="L%7/t11GZ@2+GQKq}PRT3">
                                                                                        <field name="value">[-5, -4, -3, -2, -1, 1, 2,3 , 4, 5]</field>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="variable" id="weElLx3.8SB.)STr377[">
                                                                                    <field name="name">loadAssets</field>
                                                                                    <value name="value">
                                                                                      <block type="custom_image_list" id="[iQG~r:b@1r?SU7Kr3cr@2">
                                                                                        <field name="link">${image_path}</field>
                                                                                        <field name="images">${p}_${object[i]}</field>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <statement name="else">
                                                                      <block type="variable" id="g,~oQ{TCv!;@1Ka@2RvV0[">
                                                                        <field name="name">array_ob[i]</field>
                                                                        <value name="value">
                                                                          <block type="random_one" id="4x@1ds~7PoxxQU7]K/8=r">
                                                                            <value name="items">
                                                                              <block type="expression" id="B@1Ex!yKVfJBMxopW:hdE">
                                                                                <field name="value">object</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="1kki;nQtFeHfi?kuP{ji">
                                                                            <field name="name">ox[i]</field>
                                                                            <value name="value">
                                                                              <block type="random_one" id="=0VpB,f:vjTkm)ehr:m7">
                                                                                <value name="items">
                                                                                  <block type="expression" id="Z0D_W;.7@2rgx?R8NyGc9">
                                                                                    <field name="value">[-5, -4, -3, -2, -1, 1, 2,3 , 4, 5]</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="variable" id="741u}l9DHnG7}8l}W+(~">
                                                                                <field name="name">oy[i]</field>
                                                                                <value name="value">
                                                                                  <block type="random_one" id="JLoXY2]^l9:GgryIpf7d">
                                                                                    <value name="items">
                                                                                      <block type="expression" id="mI,2j:8`GT2o_y0SR?K)">
                                                                                        <field name="value">[-5, -4, -3, -2, -1, 1, 2,3 , 4, 5]</field>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="variable" id="Xkq0myU]R{14}[F@2C$EC">
                                                                        <field name="name">i</field>
                                                                        <value name="value">
                                                                          <block type="expression" id="wL-IMe^CMB+Wo3.]YSci">
                                                                            <field name="value">i+1</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <next>
                                                                  <block type="while_do_block" id="#@2QNk6/1$7`@2wsxK,%UE">
                                                                    <value name="while">
                                                                      <block type="expression" id="(q6CqZs1f;!xI7;H21)H">
                                                                        <field name="value">i &lt; max</field>
                                                                      </block>
                                                                    </value>
                                                                    <statement name="do">
                                                                      <block type="variable" id="|ZCn?^Y45W7`Mi@2P$l.9">
                                                                        <field name="name">array_ob[i]</field>
                                                                        <value name="value">
                                                                          <block type="expression" id="N$COB_u,(hsDi^+er8$/">
                                                                            <field name="value">-1</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="]%9@2GGh~.+v+elgFvbRS">
                                                                            <field name="name">ox[i]</field>
                                                                            <value name="value">
                                                                              <block type="random_one" id="DlF=jt^7:9XV0x1C,;wn">
                                                                                <value name="items">
                                                                                  <block type="expression" id="N}f727{rDcguAy~fqK8h">
                                                                                    <field name="value">[-5, -4, -3, -2, -1, 1, 2,3 , 4, 5]</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="variable" id="|@12-87I3Z?MDVdE6%clV">
                                                                                <field name="name">oy[i]</field>
                                                                                <value name="value">
                                                                                  <block type="random_one" id="2rYbuWG_#cri@2iQDo0yj">
                                                                                    <value name="items">
                                                                                      <block type="expression" id="!7-B8.B+K;Y/P5QVjm)W">
                                                                                        <field name="value">[-5, -4, -3, -2, -1, 1, 2,3 , 4, 5]</field>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="variable" id="iYBID;@1sV@1F]NMfeX]@23">
                                                                                    <field name="name">i</field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="~vw_m@1Y!!FQrew~+7ytl">
                                                                                        <field name="value">i+1</field>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="variable" id="T#n~FkjZ%NeH]i|I90~/">
                                                                        <field name="name">array_ob</field>
                                                                        <value name="value">
                                                                          <block type="func_shuffle" id="~:K69HqKckzFXCu3w9Y4" inline="true">
                                                                            <value name="value">
                                                                              <block type="expression" id="V=m{aAJ6ZsfLi%3z,e,A">
                                                                                <field name="value">array_ob</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                                            <statement name="#props">
                                                                              <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                                                    <field name="value">400</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                                                    <field name="value">10</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_anchor" id="O.v@2mKn@1K#+tF;|}C}xI">
                                                                                    <field name="#prop">anchor</field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="+3%GU[X3l%%X`Jv1QL7@2">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="Kzp23HjHF7k]A-y1BJPm">
                                                                                        <field name="value">0</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="contents">
                                                                                          <block type="string_value" id="#6btV9p,PGV}wMx.U�">
                                                                                            <field name="value">Study the ${place} and answer the question below.</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                                            <field name="base">text</field>
                                                                                            <statement name="#props">
                                                                                              <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="fontSize">
                                                                                                  <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                                                    <field name="value">36</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="fill">
                                                                                                      <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                                                        <field name="value">black</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="stroke">
                                                                                                          <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                                            <field name="value">white</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="strokeThickness">
                                                                                                              <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                                                <field name="value">2</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="grid_shape" id="6tU?K^N8ec~A@1@1NwQ72N">
                                                                                <statement name="#props">
                                                                                  <block type="prop_grid_dimension" id="X4K:|9dKK%s9l4n5v@2lG">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="rows">
                                                                                      <block type="expression" id="[Ev!.bxnzjc|-33v.%7=">
                                                                                        <field name="value">max == 18 ? 3 : 4</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="cols">
                                                                                      <block type="expression" id="u$7]/V/!ZWM4(Va??@2({">
                                                                                        <field name="value">max == 18 ? 6 : 7</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_position" id="zLld84TfBg#!88]1,JD-">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="Cd6g{Y_P?:GyVyvQi1wJ">
                                                                                            <field name="value">400</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id=".F}E71+@1i)77!yQ`hmfu">
                                                                                            <field name="value">180</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_size" id="+o2CWr^.ohx)_bcIG53]">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="width">
                                                                                              <block type="expression" id="Z[x5iHG%KWC%KWQb}c-G">
                                                                                                <field name="value">780</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="height">
                                                                                              <block type="expression" id="@2E|Ob@1jF#gYtX8-%Iui@1">
                                                                                                <field name="value">230</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_anchor" id="1WJjXY^.h,t]eNgxaJ}q">
                                                                                                <field name="#prop">anchor</field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="DHx[vqs?.?nUD:AYPJWd">
                                                                                                    <field name="value">0.5</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="e3D|RR7w!=Sufu):zV`c">
                                                                                                    <field name="value">0.5</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_grid_cell_source" id="v8+Yu211Qg8fX9Mu4th`">
                                                                                                    <field name="#prop">cell.source</field>
                                                                                                    <value name="value">
                                                                                                      <block type="func_array_of_number" id="U%wSzq7?as:B1/U9)w+W" inline="true">
                                                                                                        <value name="items">
                                                                                                          <block type="expression" id="]ZnI5,3=sMS.q{@2$C1~s">
                                                                                                            <field name="value">max</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="from">
                                                                                                          <block type="expression" id="V5i[v:B~a?%lQ|4_L2%q">
                                                                                                            <field name="value">0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_random" id="}?kS#x[b)9OGb@1`UdJGV">
                                                                                                        <field name="#prop"></field>
                                                                                                        <field name="random">FALSE</field>
                                                                                                        <next>
                                                                                                          <block type="prop_grid_show_borders" id=";y^wk4y%[VWjXOj~vrQZ">
                                                                                                            <field name="#prop">#showBorders</field>
                                                                                                            <field name="value">FALSE</field>
                                                                                                            <next>
                                                                                                              <block type="prop_grid_cell_template" id="7^6]Kv:2yrZi`cooAW2@2">
                                                                                                                <field name="variable">$cell</field>
                                                                                                                <field name="#prop">cell.template</field>
                                                                                                                <field name="#callback">$cell</field>
                                                                                                                <statement name="body">
                                                                                                                  <block type="if_then_block" id="?:UQ.3$$ETY~Oyn5_l|Q">
                                                                                                                    <value name="if">
                                                                                                                      <block type="expression" id="BkXs?i}e}.:Ak|U+2OAM">
                                                                                                                        <field name="value">array_ob[$cell.data] &gt;= 0</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <statement name="then">
                                                                                                                      <block type="image_shape" id="z#w@1M=((ovrG;cGq9S))">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="~eu_|(IMqGxYqF]qX)8!">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="FskR@1uw/%i[WC=HZw!ir">
                                                                                                                                <field name="value">$cell.centerX+ox[$cell.data]</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="x{Fa;|p]sSXntoNz=FOq">
                                                                                                                                <field name="value">$cell.centerY+oy[$cell.data]</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_max_size" id="Pn6hIBp5!{J$1iCWy4ZI">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="maxWidth">
                                                                                                                                  <block type="expression" id="IjSV|6BTCoNAfJ9|MoJj">
                                                                                                                                    <field name="value">$cell.width - 10</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="maxHeight">
                                                                                                                                  <block type="expression" id="kTZ2(iG@2Y@18k-do!/0KX">
                                                                                                                                    <field name="value">$cell.height - 10</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_key" id="44[8dzW%^E?0tGO_r`}K">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="key">
                                                                                                                                      <block type="string_value" id="boFhAP6[4..1|3`o}Fw1">
                                                                                                                                        <field name="value">${p}_${array_ob[$cell.data]}.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_src" id=";|(ZR^hQg3i_f@2OiD;o5">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="src">
                                                                                                                                          <block type="string_value" id="Ec0p`QhBf#KjBNWy1Z-{">
                                                                                                                                            <field name="value">${image_path}${p}_${array_ob[$cell.data]}.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="variable" id="kZN/ZP#b4ANerSHBo{U[">
                                                                                    <field name="name">type</field>
                                                                                    <value name="value">
                                                                                      <block type="random_number" id="AV4xmJ^Y_ChF+o:$Oh{y">
                                                                                        <value name="min">
                                                                                          <block type="expression" id="vtkq:e`a[GbOy1rS8.J6">
                                                                                            <field name="value">1</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="max">
                                                                                          <block type="expression" id="P`#s9_~g~#eBP[c{}`=d">
                                                                                            <field name="value">3</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="statement" id="H.Yg~#;h$@1uVw8(St.0S">
                                                                                        <field name="value">function count_x (x, A) {
    var count = 0;
    for(var i = 0; i&lt; A.length; i++){
        if(A[i] == x){count++;}
    }
    return count;
}

function check(x, A) {
    for(var m=0; m&lt;A.length; m++){
        if(x == A[m]){
            return 1;
        }
    }
    return 0;
}
                                                                                        </field>
                                                                                        <next>
                                                                                          <block type="if_then_else_block" id="i]v8.Ow2S8qJ1WvIad2)">
                                                                                            <value name="if">
                                                                                              <block type="expression" id="n%,,qs$MiEliv@12|~5LM">
                                                                                                <field name="value">type == 1</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <statement name="then">
                                                                                              <block type="list_shape" id="8zBq[jJhr8:80dxdu6mr">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_list_direction" id="5=b(3H301x8(=@2zf5;](">
                                                                                                    <field name="#prop"></field>
                                                                                                    <field name="dir">horizontal</field>
                                                                                                    <next>
                                                                                                      <block type="prop_position" id="Rz4#4;mVQd;t/O9XP|;x">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="x">
                                                                                                          <block type="expression" id="^zlq_WYUHWsa2Abf0,E6">
                                                                                                            <field name="value">400</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id="!Y,ov2b`%1jWd4Q6pw:-">
                                                                                                            <field name="value">350</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_anchor" id="u#jwsd9RPk.#,1!68g!V">
                                                                                                            <field name="#prop">anchor</field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="yYPEIPp;9T{V$SSh^G)f">
                                                                                                                <field name="value">0.5</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="hozV6AR+W]vBZwEr#lAH">
                                                                                                                <field name="value">0.5</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <statement name="items">
                                                                                                  <block type="list_item_shape" id="G6@2G73G}$ngzNUbure?c">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_list_align" id="vdVO!@2`;@1H5M.0TC$(Z3">
                                                                                                        <field name="#prop"></field>
                                                                                                        <field name="align">middle</field>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <statement name="template">
                                                                                                      <block type="text_shape" id="8Y`m0qL|u0{CE}TV~G2%">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_anchor" id="z0RlvuD[zWv^el^4F87T">
                                                                                                            <field name="#prop">anchor</field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="!Il5+l^jJ.Yx/g2n|.iQ">
                                                                                                                <field name="value">0.5</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="@1Yx-GB18[z-+(1}|L^Zx">
                                                                                                                <field name="value">0</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_contents" id="SHcm|hIi8~;_~C6#k]B(">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="contents">
                                                                                                                  <block type="string_value" id="r/:v/0t:@2ghn2#:,NJ]3">
                                                                                                                    <field name="value">How many ${list_object[object[0]]} are there in the ${place}?${' '}</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style" id="Y8jZp4{f8RH|6hc1EGL.">
                                                                                                                    <field name="base">text</field>
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_text_style_font_size" id="Ysl|$a!4OrosA{gdD~YQ">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="fontSize">
                                                                                                                          <block type="expression" id="DG|+dA#c#|:dJunU[@2Pj">
                                                                                                                            <field name="value">36</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_fill" id="Gs=dRx_FNKa#-~L%hemw">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="fill">
                                                                                                                              <block type="string_value" id="?7qah6}~am6I-z3}Wu_M">
                                                                                                                                <field name="value">black</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_stroke" id="_47nOn9jij(vlt(3ZsTe">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="stroke">
                                                                                                                                  <block type="string_value" id="[TN9.+wk=0ub46}CZfxt">
                                                                                                                                    <field name="value">white</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_stroke_thickness" id="INw_~J)Fra9^d6JCslu$">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="strokeThickness">
                                                                                                                                      <block type="expression" id="[c0[xO@1AY?G$(;SUKblo">
                                                                                                                                        <field name="value">2</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="list_item_shape" id="E:%;AhOV0;D)Q!75!(`w">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_list_align" id=")X4Tcf,!@1cj2j+PyR32K">
                                                                                                            <field name="#prop"></field>
                                                                                                            <field name="align">middle</field>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <statement name="template">
                                                                                                          <block type="image_shape" id=",U,Oc2o-z8ky[~FH|u3M">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_size" id="b(A1o%@1Soht+GmVdtWDy">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="width">
                                                                                                                  <block type="expression" id="vKvGDpuT5d@2RXLHC[K$V">
                                                                                                                    <field name="value">90</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="height">
                                                                                                                  <block type="expression" id="3+O(RO}Vo=!]XqH?/%H6">
                                                                                                                    <field name="value">60</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_image_key" id="6pml)7C9o]9WZ}`sG3J-">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="key">
                                                                                                                      <block type="string_value" id="=!C2}5LE;^ySR0@1CeqnM">
                                                                                                                        <field name="value">shape.png</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_src" id="h,|Xy?H0x]5}z,0PxQd=">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="src">
                                                                                                                          <block type="string_value" id="5QM+#=Ddjp+KdHOyhot[">
                                                                                                                            <field name="value">${image_path}shape.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="algorithmic_input_shape" id="/$N|}[EI~)U_oEGW`8}}">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_value" id="6DCwNY]{X%2UyQ5mVN~k">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="7k.:rZYs`-$SY0C#,_eK">
                                                                                                                        <field name="value">count_x(object[0], array_ob)</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_size" id=";@1{v!#=.sZ[YsnGM)">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="width">
                                                                                                                          <block type="expression" id="g=K@1B-+p-v#e22.iatnb">
                                                                                                                            <field name="value">90</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="height">
                                                                                                                          <block type="expression" id="9TO6PJKi2S@1D=ke}_AK|">
                                                                                                                            <field name="value">60</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_input_keyboard" id=",m4UFKyg1Dyxc[E]HlCt">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <field name="keyboard">numbers1</field>
                                                                                                                            <next>
                                                                                                                              <block type="prop_input_max_length" id=";1B0IPiMWtEHc{Zs]F(=">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="maxLength">
                                                                                                                                  <block type="expression" id="lF6X?$F#H^K5wv$FetEr">
                                                                                                                                    <field name="value">count_x(object[0], array_ob).toString().length</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_input_result_position" id="@1+YJxLgMYecMYT4~rJ7X">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <field name="resultPosition">bottom</field>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_tab_order" id="LS]9Y!hgd3(Zqfx5q%X-">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="tabOrder">
                                                                                                                                          <block type="expression" id="GpVJl1roKT,LJTC+9?:r">
                                                                                                                                            <field name="value">0</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_stroke" id="[y=]l-8fb23KCfX.lE/#">
                                                                                                                                            <field name="#prop">stroke</field>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_fill" id="^]E5O_J?z5mbd3tW%Vh/">
                                                                                                                                                <field name="#prop">fill</field>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_text_style" id="BjyRo^{)?]CG?kkP8e2:">
                                                                                                                                                    <field name="base">text</field>
                                                                                                                                                    <statement name="#props">
                                                                                                                                                      <block type="prop_text_style_font_size" id="Fgh%`DiH+%xxJ[6HWLde">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="fontSize">
                                                                                                                                                          <block type="expression" id=":/YN|hZucWTu!#3qHT@1t">
                                                                                                                                                            <field name="value">36</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_text_style_fill" id="HW,}dT=3C-ERvg@2S,d,Y">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="fill">
                                                                                                                                                              <block type="string_value" id="V50+CyW`MF$m!5~?o,j,">
                                                                                                                                                                <field name="value">black</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_text_style_stroke" id="#XEtMT{9@10#]8MmNUF}_">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="stroke">
                                                                                                                                                                  <block type="string_value" id="z0;gCnqAjfl[@2F#EUM5`">
                                                                                                                                                                    <field name="value">white</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_text_style_stroke_thickness" id="1qI9_RCIO0ouW;@1%jF59">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="strokeThickness">
                                                                                                                                                                      <block type="expression" id="AMF-P_~r!0~4${sJc(Zf">
                                                                                                                                                                        <field name="value">2</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <statement name="else">
                                                                                              <block type="if_then_else_block" id="L:W=-]5?XLj+{k]whW~:">
                                                                                                <value name="if">
                                                                                                  <block type="expression" id="dEEL!/BQ1kj:HyF/M:zR">
                                                                                                    <field name="value">type == 2</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <statement name="then">
                                                                                                  <block type="variable" id="Qr@1+RYj:EkzPPa)@1[ZeS">
                                                                                                    <field name="name">qa</field>
                                                                                                    <value name="value">
                                                                                                      <block type="random_number" id="jL_,Yhe4w;jE+SjwP7T0">
                                                                                                        <value name="min">
                                                                                                          <block type="expression" id="mx/0pt{)S-jeQvH,Gc?`">
                                                                                                            <field name="value">0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="max">
                                                                                                          <block type="expression" id="!@1Kk}+u;}`K8c293MO^z">
                                                                                                            <field name="value">1</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="variable" id="_?d4THRi8OZo@29[_`c}4">
                                                                                                        <field name="name">i</field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id="j!#lxZK3${{51uVVa42Q">
                                                                                                            <field name="value">0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="variable" id="UTD7yd3h|C}%f|~r:fxR">
                                                                                                            <field name="name">dk</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="6M1{E/i]^@2VU1]WB3@2=~">
                                                                                                                <field name="value">0</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="while_do_block" id=":xLI#C1g)$I{{e5bl+_W">
                                                                                                                <value name="while">
                                                                                                                  <block type="expression" id="su~B=T9%f-:nLKF~97%%">
                                                                                                                    <field name="value">dk == 0</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <statement name="do">
                                                                                                                  <block type="if_then_else_block" id="jG3)CPeX_${zS41a6`4T">
                                                                                                                    <value name="if">
                                                                                                                      <block type="expression" id="C:?k,6|Qg;w^yp[z,#HW">
                                                                                                                        <field name="value">count_x(object[i], array_ob) != count_x(object[i+1], array_ob)</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <statement name="then">
                                                                                                                      <block type="variable" id="P]UHbJApWkJOdx-E-2V~">
                                                                                                                        <field name="name">dk</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id="sa~Hq0w#p6a?l9J_!bfj">
                                                                                                                            <field name="value">1</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="if_then_else_block" id="P(Rm19,mX`u8x,cx+#:P">
                                                                                                                            <value name="if">
                                                                                                                              <block type="expression" id="JSR6C]pKOU0SdiD!#ebQ">
                                                                                                                                <field name="value">count_x(object[0], array_ob) &gt; count_x(object[1], array_ob)</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <statement name="then">
                                                                                                                              <block type="variable" id="P5+6n|yzcuN@2pnP2D~">
                                                                                                                                <field name="name">more</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="[9S^@1h9F4aYO7|C#T=sS">
                                                                                                                                    <field name="value">object[i]</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="variable" id="oZGg,Ke_A+1#S1+_(4Qk">
                                                                                                                                    <field name="name">less</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="mAMTq^!!BdW=f:!@2fq,F">
                                                                                                                                        <field name="value">object[i+1]</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <statement name="else">
                                                                                                                              <block type="variable" id="Q#GNaJZDNYp1^icWyq4A">
                                                                                                                                <field name="name">more</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="~AERJ)?#EB2[+rd79iV[">
                                                                                                                                    <field name="value">object[i+1]</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="variable" id="rbWfwdr3[g!j+m(p3~Y7">
                                                                                                                                    <field name="name">less</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="U:|[z=pjICsf!l_?e(ci">
                                                                                                                                        <field name="value">object[i]</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <statement name="else">
                                                                                                                      <block type="variable" id=":(8mOpxNxks?/xc{cBuH">
                                                                                                                        <field name="name">dk</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id="AS.C(oaLY;~He=SNtu,@2">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="variable" id="ScOII85IC=]7_Zl4O2X9">
                                                                                                                            <field name="name">i</field>
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="/Cunky@2@2,6tUgQS4E3P/">
                                                                                                                                <field name="value">i+1</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="text_shape" id=".mbPc_RX-0nqXoQQ3@2F.">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_position" id="7veT_A,`BN$U@2rMZeixf">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="O|2TSDL^(K/:yF:nTw)b">
                                                                                                                            <field name="value">400</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="?A5U3M$_vOvz~gD)$@1@1P">
                                                                                                                            <field name="value">320</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_anchor" id="(y/1w}Q2|=@20KMK{-NPk">
                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="BUygw!FG@1Ae`Nre;3gq.">
                                                                                                                                <field name="value">0.5</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="`]o%bhy1WF]|4F/^#U9l">
                                                                                                                                <field name="value">0</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_contents" id="-j$K?K@1cJxG5`zFN[c_A">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="contents">
                                                                                                                                  <block type="string_value" id="?!@2+LL~l+~yxAtp3^Er6">
                                                                                                                                    <field name="value">There are more ${qa == 1 ? list_object[more]:list_object[less]}s than ${qa == 1 ? list_object[less]:list_object[more]}s in the ${place}.</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style" id=":D@13?a+Hx^x#2;O2z)U4">
                                                                                                                                    <field name="base">text</field>
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_text_style_font_size" id="6khBb`v]JSeKw90G@2y3S">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="fontSize">
                                                                                                                                          <block type="expression" id="fk2]}oJ^ShqW%RKCRirK">
                                                                                                                                            <field name="value">36</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style_fill" id="!5=HU5|-|ov4!%[K[Fx0">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="fill">
                                                                                                                                              <block type="string_value" id="+8Z$EwD|d8_6fX_Q#aO;">
                                                                                                                                                <field name="value">black</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_text_style_stroke" id="K^7GmqSA6WfP69k%pYj|">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="stroke">
                                                                                                                                                  <block type="string_value" id=";@24=+el^(A$E#6RS.R_}">
                                                                                                                                                    <field name="value">white</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_text_style_stroke_thickness" id="[671|wj^Hf=jdET@2syk;">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="strokeThickness">
                                                                                                                                                      <block type="expression" id=".(Xdtmg}JmUJ-e!u-!L?">
                                                                                                                                                        <field name="value">2</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="grid_shape" id="1ZWl8UIT0lR{fJye5Ogv">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_grid_dimension" id="U~COg;9hC@1r|2QIl.71X">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="rows">
                                                                                                                              <block type="expression" id="`!_@2-#f:~wMLG1lXgkY}">
                                                                                                                                <field name="value">1</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="cols">
                                                                                                                              <block type="expression" id="Cb#T219?!}fr;6bZ2H;{">
                                                                                                                                <field name="value">2</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_position" id="9X/tSx?aqCEuaU9C]ioO">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="dwWl2v0?1Z9;1mL64=?p">
                                                                                                                                    <field name="value">400</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="hh5aiDYA/PHFQ{Ay3?.K">
                                                                                                                                    <field name="value">400</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_size" id="+cDo91~HpQ$z4ecoBpDV">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="width">
                                                                                                                                      <block type="expression" id="|kHfkY4t.K}R@1OqntO_,">
                                                                                                                                        <field name="value">600</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="height">
                                                                                                                                      <block type="expression" id="g5mVMEhF%;I],MLS$5cP">
                                                                                                                                        <field name="value">60</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_anchor" id="E6?Q^[s38^qxVX0#Y$-m">
                                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="expression" id="-MW2d7@1(FTl+:r^y,pY@2">
                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id="47`7Yr+bBd5nS(P?.+tA">
                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_grid_cell_source" id=";}:J)%J9u_,Epk-trCXD">
                                                                                                                                            <field name="#prop">cell.source</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id="~j@1,^liddf}oDg}E91^i">
                                                                                                                                                <field name="value">['true', 'false']</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_grid_random" id="-8Ut@1soPLZ@1Ftn:opp]K">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <field name="random">FALSE</field>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_grid_show_borders" id="vC~@2R^zBwtDud;x@2z%Zx">
                                                                                                                                                    <field name="#prop">#showBorders</field>
                                                                                                                                                    <field name="value">FALSE</field>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_grid_cell_template" id="WcLYMENf)XACPp5v-Mux">
                                                                                                                                                        <field name="variable">$cell</field>
                                                                                                                                                        <field name="#prop">cell.template</field>
                                                                                                                                                        <field name="#callback">$cell</field>
                                                                                                                                                        <statement name="body">
                                                                                                                                                          <block type="choice_custom_shape" id="oUI7p-XWO0XljuyUS=V|">
                                                                                                                                                            <field name="action">click-one</field>
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id="@2nOPhT4gw_JJjaJ}`4N@1">
                                                                                                                                                                <field name="value">$cell.data</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <statement name="template">
                                                                                                                                                              <block type="image_shape" id=":07|k=Cu4^f}3;ty51{e">
                                                                                                                                                                <statement name="#props">
                                                                                                                                                                  <block type="prop_position" id="V(Y#brPM#C74`KcV1Rr:">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="x">
                                                                                                                                                                      <block type="expression" id="5!Gajp0^Ky7gU^@1AY-:k">
                                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <value name="y">
                                                                                                                                                                      <block type="expression" id="`P4H1{qM6#XLc+GT5d2S">
                                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_image_key" id="]`NcDP{Scu@2[_lzs~b;I">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="key">
                                                                                                                                                                          <block type="string_value" id="zLu}MagH9Z;d3DG.;3^@2">
                                                                                                                                                                            <field name="value">${$cell.data}.png</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_image_src" id="7IL;/wQ?3/-)FW.B:bJm">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="src">
                                                                                                                                                                              <block type="string_value" id="@19@156KmC65.Qh(n^z87}">
                                                                                                                                                                                <field name="value">${image_path}${$cell.data}.png</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="func_add_answer" id="IFd=J#EJf.;SAno5{u=d">
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="Q;/-7U5oYFa#gwR^53H~">
                                                                                                                                <field name="value">qa == 1 ? 'true' : 'false'</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <statement name="else">
                                                                                                  <block type="variable" id="513/=N8REH1?@25Xhx[KW">
                                                                                                    <field name="name">qa</field>
                                                                                                    <value name="value">
                                                                                                      <block type="random_number" id="?35noMx``|JT#KI}zQ`z">
                                                                                                        <value name="min">
                                                                                                          <block type="expression" id="#rrt@1tz/~9ArWgI,Rw.J">
                                                                                                            <field name="value">0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="max">
                                                                                                          <block type="expression" id="2dd)qO$y72pKV%wu8Ek;">
                                                                                                            <field name="value">1</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="if_then_else_block" id="GDlg0m=cE}OTs`1)5aAS">
                                                                                                        <value name="if">
                                                                                                          <block type="expression" id="S1fa]}kA1VwxdJ$khCap">
                                                                                                            <field name="value">qa == 0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <statement name="then">
                                                                                                          <block type="variable" id="m:[J7o.R@2)^pOUk972w7">
                                                                                                            <field name="name">ob</field>
                                                                                                            <value name="value">
                                                                                                              <block type="random_one" id="WW~97ZM|UHdY-|t%b|VN">
                                                                                                                <value name="items">
                                                                                                                  <block type="expression" id="?H)rE6a=(,T1HFV|@1l3!">
                                                                                                                    <field name="value">object</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <statement name="else">
                                                                                                          <block type="do_while_block" id="VD7;OV]CsPRY@1r1XPq1e">
                                                                                                            <statement name="do">
                                                                                                              <block type="variable" id="~!$0|4A1Ot?Lg?ad{5x@2">
                                                                                                                <field name="name">ob</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="random_one" id="E0#wct=HwILm`@1ys)MC{">
                                                                                                                    <value name="items">
                                                                                                                      <block type="func_array_of_number" id="-~a4=NQM4Spe;@1.NNGRu" inline="true">
                                                                                                                        <value name="items">
                                                                                                                          <block type="expression" id="6RjKbJ0C8Yt$7z{M#?C%">
                                                                                                                            <field name="value">11</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="from">
                                                                                                                          <block type="expression" id="US~Oyn9RR@2B^Tt#A?v#p">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <value name="while">
                                                                                                              <block type="expression" id="{}(l0.v!PXF5C8|@1$(}I">
                                                                                                                <field name="value">check(ob, object) == 1</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="text_shape" id="#-V}I{{m;RC;dNK]d;v@1">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_position" id="WN[CY}eS%vMv^.g$iLQ]">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="b|%~{`cv!OiE_?lLr$+p">
                                                                                                                    <field name="value">400</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="%yr=O8@2fz#!8J_]wv4]n">
                                                                                                                    <field name="value">320</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_anchor" id="pp{/gYVYo8eZ2s%.zs@1l">
                                                                                                                    <field name="#prop">anchor</field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="]@22DDogsbGsy8uv}QHaT">
                                                                                                                        <field name="value">0.5</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="J]wypQasazYuHP8%CRq~">
                                                                                                                        <field name="value">0</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_contents" id="V~}%h$0y`hb17mYprp@1K">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="contents">
                                                                                                                          <block type="string_value" id=".LNA5lei2VD#i,mkJ`W)">
                                                                                                                            <field name="value">The ${place} does not have any ${list_object[ob]}s.</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style" id="LiWE(=+8@1^x@1@1BBV@2{Iu">
                                                                                                                            <field name="base">text</field>
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_text_style_font_size" id="0kOlXE?H^h)EGsWBY+B!">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="fontSize">
                                                                                                                                  <block type="expression" id="qUXLrZ1)@2(yOwiuU}a.L">
                                                                                                                                    <field name="value">36</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_fill" id="5Nx1V]:gkO~I([%3_J6T">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="fill">
                                                                                                                                      <block type="string_value" id=":0b/RQOa,P2Tzk+u:SXE">
                                                                                                                                        <field name="value">black</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_stroke" id="|N?OzC[$;3!woN.0|so8">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="stroke">
                                                                                                                                          <block type="string_value" id="6EFfMX,G/PBDcii@2Mh,#">
                                                                                                                                            <field name="value">white</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style_stroke_thickness" id="tr?+qq#E?54^.32iPWQ8">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="strokeThickness">
                                                                                                                                              <block type="expression" id="pgYm0~o,jLH@2;/!}RCgg">
                                                                                                                                                <field name="value">2</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="grid_shape" id="(t$[6fW;m.Im;pe6YY,W">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_grid_dimension" id="HJ$wDj(~%U3jfdNfMz%-">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="rows">
                                                                                                                      <block type="expression" id=",^?!UR[/:]Wow`?3s?,p">
                                                                                                                        <field name="value">1</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="cols">
                                                                                                                      <block type="expression" id=":zvGhIL0Vn)G|GcSuQM_">
                                                                                                                        <field name="value">2</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_position" id="A!ZIJ/;YYPVOQ):[Mo-=">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="PmPqwQlumQ;:89Aq.y%u">
                                                                                                                            <field name="value">400</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="g@2@2IgL2+hE=WViS}TVR4">
                                                                                                                            <field name="value">400</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_size" id=";Q$J5XFl{:/H@2Ni+Fg0W">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="width">
                                                                                                                              <block type="expression" id="tg|i@2|C+/eDEdK9_v`n#">
                                                                                                                                <field name="value">600</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="height">
                                                                                                                              <block type="expression" id=".@2~./mXFAA(^SIe)``c#">
                                                                                                                                <field name="value">60</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_anchor" id="A}8E[;hFPT=TH9-_s1Ig">
                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="WFTRu2!-Zc@2z%$ffMNA^">
                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="(5H4W~bNb#s.~:P@1f|9w">
                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_grid_cell_source" id="@1vES@1wT6CJtFvqu,A.8R">
                                                                                                                                    <field name="#prop">cell.source</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="rDu!1p9q:pQtBW]B=[_^">
                                                                                                                                        <field name="value">['true', 'false']</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_grid_random" id="j}9j!usB.Z.=@2;(-k?KA">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <field name="random">FALSE</field>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_grid_show_borders" id="e,E3NBNJ3l^Jfp6NfGIj">
                                                                                                                                            <field name="#prop">#showBorders</field>
                                                                                                                                            <field name="value">FALSE</field>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_grid_cell_template" id="zXrwJw.JY}J|cqt?=a2F">
                                                                                                                                                <field name="variable">$cell</field>
                                                                                                                                                <field name="#prop">cell.template</field>
                                                                                                                                                <field name="#callback">$cell</field>
                                                                                                                                                <statement name="body">
                                                                                                                                                  <block type="choice_custom_shape" id="1%?-CBzbxw2a2[MNn%ZW">
                                                                                                                                                    <field name="action">click-one</field>
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="expression" id="cI^FGzl~k#IS$~zB0-td">
                                                                                                                                                        <field name="value">$cell.data</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <statement name="template">
                                                                                                                                                      <block type="image_shape" id="-?R(Ktt|c9L;us=a|enE">
                                                                                                                                                        <statement name="#props">
                                                                                                                                                          <block type="prop_position" id=".J@1yU:,zzriKEO{/R06P">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="x">
                                                                                                                                                              <block type="expression" id="=?Rece@2{uHPJV.c@20@2e7">
                                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="y">
                                                                                                                                                              <block type="expression" id="7AZFT~=Iwbhx.8b~-WXP">
                                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_image_key" id="f$L7{Cy24~))ksO,,uzk">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="key">
                                                                                                                                                                  <block type="string_value" id="-MS4_e}EY4)8;seKX%tr">
                                                                                                                                                                    <field name="value">${$cell.data}.png</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_image_src" id="Jd#2~,3l/ab.0KTxv$3.">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="src">
                                                                                                                                                                      <block type="string_value" id="54n%OXP4}%sQiPKRo/,`">
                                                                                                                                                                        <field name="value">${image_path}${$cell.data}.png</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="func_add_answer" id=".9+PxT8T7h`^5{Vob@1=s">
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="e)x+G,59#Ju^Qub56y,u">
                                                                                                                        <field name="value">qa == 1 ? 'true' : 'false'</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="variable" id="yrq]!kFkrs?[40Qob{I=">
                                                                                                <field name="name">loadAssets</field>
                                                                                                <value name="value">
                                                                                                  <block type="custom_image_list" id="^[RD/81v[+5cq6:7LWT1">
                                                                                                    <field name="link">${image_path}</field>
                                                                                                    <field name="images">${p}</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="partial_explanation" id="KN/l]fNC$yr,?Vu4~cdR" inline="true">
                                                                                                    <value name="value">
                                                                                                      <block type="string_value" id="B~oO/~Y{rz!H,aHwXh)a">
                                                                                                        <field name="value">&lt;u&gt;Step 1: Study the ${place} carefully. Look out for the different items in the ${place}.&lt;/u&gt;&lt;/br&gt;
&lt;style&gt;
  table{background-image: url("@1sprite.src(${p})"); width: 800px; height: 450px;}
td{text-align: center;  width: 100px}
img{max-width: 70px; max-height: 60px}

  &lt;/style&gt;
&lt;center&gt;
  &lt;table&gt;
    &lt;tr&gt;&lt;td colspan=${max==18?6:7}&gt;&lt;/td&gt;&lt;/tr&gt;
                                                                                                        </field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="variable" id="98Kgv#g}k34r.CpOmltL">
                                                                                                        <field name="name">i</field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id="xrw{LwZ0s.w]h@1S[zf{z">
                                                                                                            <field name="value">0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="while_do_block" id="g%YR-mB$ICmQ?$)w9.bu">
                                                                                                            <value name="while">
                                                                                                              <block type="expression" id="~f[6umuZO@1@241-@2p@2?)X">
                                                                                                                <field name="value">i &lt; (max==18?3:4)</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <statement name="do">
                                                                                                              <block type="partial_explanation" id="cw!:Wh:YCzLvYOX4?jy@1" inline="true">
                                                                                                                <value name="value">
                                                                                                                  <block type="string_value" id="SiW@1:@2EMae?Ex0;5vhoj">
                                                                                                                    <field name="value">&lt;tr&gt;</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="variable" id="a3gl`=N47H#(u)x$6[-o">
                                                                                                                    <field name="name">j</field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="!7_k5A~;6IaX0we+q3A;">
                                                                                                                        <field name="value">0</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="while_do_block" id="tBFw:v!/{uc]@1.Gl?[_A">
                                                                                                                        <value name="while">
                                                                                                                          <block type="expression" id="G:p^DoymYw)F.+jKFpca">
                                                                                                                            <field name="value">j &lt; (max==18?6:7)</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <statement name="do">
                                                                                                                          <block type="if_then_else_block" id="#j;[y|go{TIEoNt%g6?6">
                                                                                                                            <value name="if">
                                                                                                                              <block type="expression" id="{8xH2qpQ_[hD}7i/DSJ(">
                                                                                                                                <field name="value">array_ob[(max==18?6:7)@2i + j] &gt;= 0</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <statement name="then">
                                                                                                                              <block type="partial_explanation" id="t/`(3=LUtR$8X{BO)vw8" inline="true">
                                                                                                                                <value name="value">
                                                                                                                                  <block type="string_value" id="$,m(?z_qPxtEy1VvS=qI">
                                                                                                                                    <field name="value">&lt;td&gt;&lt;img src='@1sprite.src(${p}_${array_ob[(max==18?6:7)@2i + j]})'/&gt;&lt;/td&gt;</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <statement name="else">
                                                                                                                              <block type="partial_explanation" id="9i%x5WM=(3c_aC([06]+" inline="true">
                                                                                                                                <value name="value">
                                                                                                                                  <block type="string_value" id="5}i71W=~Fv(m0ow!STL0">
                                                                                                                                    <field name="value">&lt;td&gt;&lt;/td&gt;</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="variable" id="|YrV[=K[vxA;TS81vRoa">
                                                                                                                                <field name="name">j</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="5ic`~]D@1f:J_tF;yO95F">
                                                                                                                                    <field name="value">j+1</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="partial_explanation" id="h(,QPgnycB}yjBY]ejg)" inline="true">
                                                                                                                            <value name="value">
                                                                                                                              <block type="string_value" id="[xE/#qgmsB0!XsR[O+NK">
                                                                                                                                <field name="value">&lt;/tr&gt;</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="variable" id="tJOdZ0wdFmtaR_yRs0((">
                                                                                                                                <field name="name">i</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="K,|N4qu3$/~=Y}x1?YRN">
                                                                                                                                    <field name="value">i+1</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="partial_explanation" id="!EH3}GY.B7Kvw5C7d`-(" inline="true">
                                                                                                                <value name="value">
                                                                                                                  <block type="string_value" id="#p^CrLrU}VXj0J.7Zmux">
                                                                                                                    <field name="value">&lt;tr&gt;&lt;td colspan=${max==18?6:7}&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/center&gt;</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="end_partial_explanation" id="^mt;1VE?.d!$?ndj-eox">
                                                                                                                    <next>
                                                                                                                      <block type="if_then_else_block" id=":^:H+BL{FJ2:eteFRMYT">
                                                                                                                        <value name="if">
                                                                                                                          <block type="expression" id="HWfbY9^D(sLMK|@1QA46p">
                                                                                                                            <field name="value">type == 1</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <statement name="then">
                                                                                                                          <block type="partial_explanation" id="!-+ptiA!eegOAb]$HW`z" inline="true">
                                                                                                                            <value name="value">
                                                                                                                              <block type="string_value" id="qSWlIfZX@1k5nbxbRu12.">
                                                                                                                                <field name="value">&lt;u&gt;Step 2: Now let's count the number of ${list_object[object[0]]} in the ${place}.&lt;/u&gt;&lt;/br&gt;
&lt;style&gt;
  table{background-image: url("@1sprite.src(${p})"); width: 800px; height: 450px;}
td{text-align: center;width: 100px}
img{max-width: 70px; max-height: 60px}
#choise {border: 3px solid green; border-radius: 40px; display: inline-block; width: 70px; height: 70px}
  &lt;/style&gt;
&lt;center&gt;
  &lt;table&gt;
    &lt;tr&gt;&lt;td colspan=${max==18?6:7}&gt;&lt;/td&gt;&lt;/tr&gt;
                                                                                                                                </field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="variable" id="|D|PtZ0r,.e@201T+lp,a">
                                                                                                                                <field name="name">count</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="=TGkjI+i|`ZVP2z0LCk;">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="variable" id="BmbRiW6~uswt|1p,@2;@19">
                                                                                                                                    <field name="name">i</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="b4!@2ycp{Q9.iU,YC$(D:">
                                                                                                                                        <field name="value">0</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="while_do_block" id="s[x@1}E`GCZ$?(ONDvlDm">
                                                                                                                                        <value name="while">
                                                                                                                                          <block type="expression" id="%+3fE@1Kop;J_42:(O!1|">
                                                                                                                                            <field name="value">i &lt; (max==18?3:4)</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <statement name="do">
                                                                                                                                          <block type="partial_explanation" id="hYejL_-{PHv_Q%Ctm7mD" inline="true">
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="string_value" id="vswT2,]AkHZI,EVG%(,.">
                                                                                                                                                <field name="value">&lt;tr&gt;</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="variable" id="iWea[9WaYD+_+C4grYWe">
                                                                                                                                                <field name="name">j</field>
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="expression" id="TUtXY=XzAeX5WZHA)p)v">
                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="while_do_block" id="do.T(/`0m{K5MImz8/du">
                                                                                                                                                    <value name="while">
                                                                                                                                                      <block type="expression" id="F$xLB$Y00er_R,:7z]CK">
                                                                                                                                                        <field name="value">j &lt; (max==18?6:7)</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <statement name="do">
                                                                                                                                                      <block type="if_then_else_block" id="_vC;F6P{F4x3s}ZElseL">
                                                                                                                                                        <value name="if">
                                                                                                                                                          <block type="expression" id="Nr`yf:,HDxQ[KjHg!HqN">
                                                                                                                                                            <field name="value">array_ob[(max==18?6:7)@2i + j] &gt;= 0</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <statement name="then">
                                                                                                                                                          <block type="if_then_else_block" id="kTcdy-91;y76niHWk1Rt">
                                                                                                                                                            <value name="if">
                                                                                                                                                              <block type="expression" id="=#~ZGS8u^,11_9crC%Xl">
                                                                                                                                                                <field name="value">array_ob[(max==18?6:7)@2i + j] == object[0]</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <statement name="then">
                                                                                                                                                              <block type="partial_explanation" id="$GHw,+rMSUmmD?qhA-[y" inline="true">
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="string_value" id="38=^B4|7F[c$GOKiVQV2">
                                                                                                                                                                    <field name="value">&lt;td&gt;&lt;div id='choise'&gt;&lt;img style='padding: 5px' src='@1sprite.src(${p}_${array_ob[(max==18?6:7)@2i + j]})'/&gt;&lt;/div&gt; ${count+1}&lt;/td&gt;</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="variable" id="gsS=/V2DQj2F0qGWmHAq">
                                                                                                                                                                    <field name="name">count</field>
                                                                                                                                                                    <value name="value">
                                                                                                                                                                      <block type="expression" id="zBTx93;^aiYo0(GIO4h~">
                                                                                                                                                                        <field name="value">count +1</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                            <statement name="else">
                                                                                                                                                              <block type="partial_explanation" id="DHc5zhZFVF7iRe5|Nu@1r" inline="true">
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="string_value" id="ZTKR/Y%o@2$Raf?l9E)`_">
                                                                                                                                                                    <field name="value">&lt;td&gt;&lt;img src='@1sprite.src(${p}_${array_ob[(max==18?6:7)@2i + j]})'/&gt;&lt;/td&gt;</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                        <statement name="else">
                                                                                                                                                          <block type="partial_explanation" id="{G|+4_2%Z#9OsrzHq|hb" inline="true">
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="string_value" id="7hH7.q})mhw^]Z69jJ2M">
                                                                                                                                                                <field name="value">&lt;td&gt;&lt;/td&gt;</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="variable" id="Cc|F1M#Z!y-#G%NM{gIP">
                                                                                                                                                            <field name="name">j</field>
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id="GoQgYfKL%4:%4OJ#J|)I">
                                                                                                                                                                <field name="value">j+1</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="partial_explanation" id="SG.8g(oDbbGOr|32o1`$" inline="true">
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="string_value" id="h!tnP7f(bi?NYYuxK2G/">
                                                                                                                                                            <field name="value">&lt;/tr&gt;</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="variable" id="=,]3ismk8dp?ZAf4WA}v">
                                                                                                                                                            <field name="name">i</field>
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id="o4F~MNce$J!5O9/4@14zt">
                                                                                                                                                                <field name="value">i+1</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <next>
                                                                                                                                          <block type="partial_explanation" id="R#$Zxj%;pA`J:YPdx5{f" inline="true">
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="string_value" id="2kQxrnfC;?no.GxIp%uo">
                                                                                                                                                <field name="value">&lt;tr&gt;&lt;td colspan=${max==18?6:7}&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
  &lt;/br&gt;
&lt;span style='background: rgb(250, 250, 250, 0.5)'&gt;
The number of &lt;b&gt;${list_object[object[0]]}${count_x(object[0], array_ob) &lt; 2 ? '':'s'}&lt;/b&gt; in the ${place} is &lt;b&gt;${count_x(object[0], array_ob)}&lt;/b&gt;.&lt;/span&gt;
&lt;/center&gt;
                                                                                                                                                </field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="end_partial_explanation" id="35V63YgS]N_eXW;fq).S"></block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <statement name="else">
                                                                                                                          <block type="if_then_else_block" id="}3`TK5-T)Q]uO^x[ou0j">
                                                                                                                            <value name="if">
                                                                                                                              <block type="expression" id="+DZ0bO5q,4;Q|@1Z7NS$h">
                                                                                                                                <field name="value">type == 2</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <statement name="then">
                                                                                                                              <block type="partial_explanation" id="WRZ#laJVzDmb/6L;J:%1" inline="true">
                                                                                                                                <value name="value">
                                                                                                                                  <block type="string_value" id="}TG~|(:?H_%bySPnq;1f">
                                                                                                                                    <field name="value">&lt;u&gt;Step 2: Now let's count the number of ${qa == 1 ? list_object[more]:list_object[less]} in the ${place}.&lt;/u&gt;&lt;/br&gt;
&lt;style&gt;
  table{background-image: url("@1sprite.src(${p})"); width: 800px; height: 450px;}
td{text-align: center;width: 100px}
img{max-width: 70px; max-height: 60px}
#choise {border: 3px solid green; border-radius: 40px; display: inline-block; width: 70px; height: 70px}
  &lt;/style&gt;
&lt;center&gt;
  &lt;table&gt;
    &lt;tr&gt;&lt;td colspan=${max==18?6:7}&gt;&lt;/td&gt;&lt;/tr&gt;
                                                                                                                                    </field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="variable" id="I%#F|pa}YYM,_03,]Qas">
                                                                                                                                    <field name="name">count</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="}L0WB/N/0AHDHF|BL|(k">
                                                                                                                                        <field name="value">0</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="variable" id="Dc2VxL0zF68JSJoakl`$">
                                                                                                                                        <field name="name">i</field>
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="expression" id="Brf-h{/?wE#t%EM,1h@1m">
                                                                                                                                            <field name="value">0</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="while_do_block" id="{Rv`+Hx$H.aH1kC[eS2P">
                                                                                                                                            <value name="while">
                                                                                                                                              <block type="expression" id="r]5$8/_x@2fZ-dHZG.5UV">
                                                                                                                                                <field name="value">i &lt; (max==18?3:4)</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <statement name="do">
                                                                                                                                              <block type="partial_explanation" id="EKXE5H:X|0YyLFDeL?t," inline="true">
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="string_value" id="{Wao+w5B@1K#`FUW13l@24">
                                                                                                                                                    <field name="value">&lt;tr&gt;</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="variable" id="YB=O(V[,l]_,ez?7;wDt">
                                                                                                                                                    <field name="name">j</field>
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="expression" id="Cnr)(AOU[BR(Ck:U~q@1^">
                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="while_do_block" id="^7TSgpeHwW4ECjWVpnRJ">
                                                                                                                                                        <value name="while">
                                                                                                                                                          <block type="expression" id="l.,MK/|oo#.(MDH5:">
                                                                                                                                                            <field name="value">j &lt; (max==18?6:7)</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <statement name="do">
                                                                                                                                                          <block type="if_then_else_block" id="o~WJ8u%7oeRlwR1wO@1^Z">
                                                                                                                                                            <value name="if">
                                                                                                                                                              <block type="expression" id="xvJ=GXutA@1q=U675EFTJ">
                                                                                                                                                                <field name="value">array_ob[(max==18?6:7)@2i + j] &gt;= 0</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <statement name="then">
                                                                                                                                                              <block type="if_then_else_block" id="t9NtO(5pe41^L]%{o0(`">
                                                                                                                                                                <value name="if">
                                                                                                                                                                  <block type="expression" id="UdEB=qbv/(BJN7#SE#~`">
                                                                                                                                                                    <field name="value">array_ob[(max==18?6:7)@2i + j] == (qa == 1 ? more:less)</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <statement name="then">
                                                                                                                                                                  <block type="partial_explanation" id="(@1qX2n4L!QQubyVmapW}" inline="true">
                                                                                                                                                                    <value name="value">
                                                                                                                                                                      <block type="string_value" id="An:amkO5f?!M[:@1:HX5c">
                                                                                                                                                                        <field name="value">&lt;td&gt;&lt;div id='choise'&gt;&lt;img style='padding: 5px' src='@1sprite.src(${p}_${array_ob[(max==18?6:7)@2i + j]})'/&gt;&lt;/div&gt; ${count+1}&lt;/td&gt;</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="variable" id="mg$o:E#j1P/SN@2FXdJ$7">
                                                                                                                                                                        <field name="name">count</field>
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="expression" id="yFlVki7g%%6IN5l0dC;6">
                                                                                                                                                                            <field name="value">count +1</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                                <statement name="else">
                                                                                                                                                                  <block type="partial_explanation" id="aQLHAFKK|T8CK$=ZrVt." inline="true">
                                                                                                                                                                    <value name="value">
                                                                                                                                                                      <block type="string_value" id="-:@1R}2}pi7@1GyI5TSNHe">
                                                                                                                                                                        <field name="value">&lt;td&gt;&lt;img src='@1sprite.src(${p}_${array_ob[(max==18?6:7)@2i + j]})'/&gt;&lt;/td&gt;</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                            <statement name="else">
                                                                                                                                                              <block type="partial_explanation" id="1%MeYCnJt;3$t:Zc0=@2S" inline="true">
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="string_value" id="MQ@2X)!)O??mN0I+2Maa8">
                                                                                                                                                                    <field name="value">&lt;td&gt;&lt;/td&gt;</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="variable" id="@2wC_JRqUI5_5dafgOe7.">
                                                                                                                                                                <field name="name">j</field>
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="expression" id="z,g4aywZtx^,c71i}fFs">
                                                                                                                                                                    <field name="value">j+1</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="partial_explanation" id="T^6n_`/[gjE+f0[xTFHU" inline="true">
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="string_value" id="`oJ83jls62dN]`l6A~x6">
                                                                                                                                                                <field name="value">&lt;/tr&gt;</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="variable" id="_ZyfM9MC[e(u94yxR2):">
                                                                                                                                                                <field name="name">i</field>
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="expression" id="yAK^B}R?-x5i+pKnIHAj">
                                                                                                                                                                    <field name="value">i+1</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                            <next>
                                                                                                                                              <block type="partial_explanation" id="4/7$tIsmuWL$FNf469kW" inline="true">
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="string_value" id=")1zg{;jeU4B`lcxO^qQ3">
                                                                                                                                                    <field name="value">&lt;tr&gt;&lt;td colspan=${max==18?6:7}&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
  &lt;/br&gt;
&lt;span style='background: rgb(250, 250, 250, 0.5)'&gt;
The number of ${qa == 1 ? list_object[more]:list_object[less]}${count_x((qa == 1 ? more:less), array_ob) &lt; 2 ? '' : 's'} in the ${place} is &lt;b style='color: green'&gt;${count_x((qa == 1 ? more:less), array_ob)}&lt;/b&gt;.&lt;/span&gt;
&lt;/center&gt;
                                                                                                                                                    </field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="end_partial_explanation" id="R60{VGN$DM;,1JL@1=0}|">
                                                                                                                                                    <next>
                                                                                                                                                      <block type="partial_explanation" id="}CtrJ@1k`m8Ft.x%xFz7?" inline="true">
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="string_value" id="3=Ko,0I=_AM(,fsc,UP4">
                                                                                                                                                            <field name="value">&lt;u&gt;Step 3: Next count the number of ${qa == 1 ? list_object[less]:list_object[more]} in the ${place}.&lt;/u&gt;&lt;/br&gt;
&lt;style&gt;
  table{background-image: url("@1sprite.src(${p})"); width: 800px; height: 450px;}
td{text-align: center;width: 100px}
img{max-width: 70px; max-height: 60px}
#choise {border: 3px solid red; border-radius: 40px; display: inline-block; width: 70px; height: 70px}
  &lt;/style&gt;
&lt;center&gt;
  &lt;table&gt;
    &lt;tr&gt;&lt;td colspan=${max==18?6:7}&gt;&lt;/td&gt;&lt;/tr&gt;
                                                                                                                                                            </field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="variable" id="H3,mKrmH1`kOU2@28oW5:">
                                                                                                                                                            <field name="name">count</field>
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id="pK;A4#RNYV?wX`5U}kfs">
                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="variable" id="lrjP=6|Ag9/s^@1@2Q~aSn">
                                                                                                                                                                <field name="name">i</field>
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="expression" id="D3|zSI~k(l8SJQYT]26u">
                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="while_do_block" id="YO-,/xaLIZM|vJ`X#!n)">
                                                                                                                                                                    <value name="while">
                                                                                                                                                                      <block type="expression" id="@1L-aI$ito=G|A`zV_`6s">
                                                                                                                                                                        <field name="value">i &lt; (max==18?3:4)</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <statement name="do">
                                                                                                                                                                      <block type="partial_explanation" id="$~Fj@2$PH?8DFQmqQj+8f" inline="true">
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="string_value" id="^y}eL!HSH;1RTP{Ke5l`">
                                                                                                                                                                            <field name="value">&lt;tr&gt;</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="variable" id="XMWLSa`K6@1`I-mK.yfs_">
                                                                                                                                                                            <field name="name">j</field>
                                                                                                                                                                            <value name="value">
                                                                                                                                                                              <block type="expression" id="2QKa6cLq}o]7kF3vIC%9">
                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="while_do_block" id="BqM=/am2x@1=^NDD#|E[r">
                                                                                                                                                                                <value name="while">
                                                                                                                                                                                  <block type="expression" id="Y_;m~s+]/Wda-ffIIP?d">
                                                                                                                                                                                    <field name="value">j &lt; (max==18?6:7)</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <statement name="do">
                                                                                                                                                                                  <block type="if_then_else_block" id=":~;Gz:QM:7tdg#wN.9ji">
                                                                                                                                                                                    <value name="if">
                                                                                                                                                                                      <block type="expression" id="(ME:yOa~F|Q]g66T`DYh">
                                                                                                                                                                                        <field name="value">array_ob[(max==18?6:7)@2i + j] &gt;= 0</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <statement name="then">
                                                                                                                                                                                      <block type="if_then_else_block" id="nob_]=g?:ID_2`x?7USQ">
                                                                                                                                                                                        <value name="if">
                                                                                                                                                                                          <block type="expression" id="tYae+3^l+PHyen|/n@1pK">
                                                                                                                                                                                            <field name="value">array_ob[(max==18?6:7)@2i + j] == (qa == 1 ? less:more)</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <statement name="then">
                                                                                                                                                                                          <block type="partial_explanation" id="7IOHpymsvfdo_Es{j@1x@1" inline="true">
                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                              <block type="string_value" id="#Gt2vk{`[eF!by;pmXc,">
                                                                                                                                                                                                <field name="value">&lt;td&gt;&lt;div id='choise'&gt;&lt;img style='padding: 5px' src='@1sprite.src(${p}_${array_ob[(max==18?6:7)@2i + j]})'/&gt;&lt;/div&gt; ${count+1}&lt;/td&gt;</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="variable" id="^xm{oEao{IChYbP6B/LS">
                                                                                                                                                                                                <field name="name">count</field>
                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                  <block type="expression" id="/7mRU7/]X$V@1oMkmy?j:">
                                                                                                                                                                                                    <field name="value">count +1</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </statement>
                                                                                                                                                                                        <statement name="else">
                                                                                                                                                                                          <block type="partial_explanation" id=";.qQW^z-/MABk-f=?0cH" inline="true">
                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                              <block type="string_value" id="S^OqXFhQFz_nn%XE,cs8">
                                                                                                                                                                                                <field name="value">&lt;td&gt;&lt;img src='@1sprite.src(${p}_${array_ob[(max==18?6:7)@2i + j]})'/&gt;&lt;/td&gt;</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </statement>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </statement>
                                                                                                                                                                                    <statement name="else">
                                                                                                                                                                                      <block type="partial_explanation" id="M,jDIaUsenz7E8tY!aOn" inline="true">
                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                          <block type="string_value" id="Ro|oC#@2x.%lGi{}!Ol+M">
                                                                                                                                                                                            <field name="value">&lt;td&gt;&lt;/td&gt;</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </statement>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="variable" id="{`yWcl{p`p^I-+=9whV,">
                                                                                                                                                                                        <field name="name">j</field>
                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                          <block type="expression" id="xU=j?ftM#,eJG8-H=BD8">
                                                                                                                                                                                            <field name="value">j+1</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </statement>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="partial_explanation" id="gKV1x?mwct/tsb%pOu=$" inline="true">
                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                      <block type="string_value" id="}XG2qpi3s.RFuHLXqCjZ">
                                                                                                                                                                                        <field name="value">&lt;/tr&gt;</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="variable" id="`c,lx{H~Oa2QRsnysJSS">
                                                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                          <block type="expression" id="z8H1%$#9uDi|w2n_d@1[.">
                                                                                                                                                                                            <field name="value">i+1</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="partial_explanation" id="BS5n:CVxw4}=@1e946%:T" inline="true">
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="string_value" id="6@2Y7+}6P?5[gM,Olhd}G">
                                                                                                                                                                            <field name="value">&lt;tr&gt;&lt;td colspan=${max==18?6:7}&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
  &lt;/br&gt;
&lt;span style='background: rgb(250, 250, 250, 0.5)'&gt;
The number of ${qa == 1 ? list_object[less]:list_object[more]}${count_x((qa == 1 ? less:more), array_ob) &lt; 2 ? '':'s'} in the ${place} is &lt;b style='color: red'&gt;${count_x((qa == 1 ? less:more), array_ob)}&lt;/b&gt;.&lt;/span&gt;
&lt;/center&gt;
                                                                                                                                                                            </field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="end_partial_explanation" id="U,:,A;Mr@1y(lYXp8hXW%">
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="partial_explanation" id="Y)E^sL$[ph2EDOjGMY[9" inline="true">
                                                                                                                                                                                <value name="value">
                                                                                                                                                                                  <block type="string_value" id="-)Pv8_t[GqC8D]_Gv|x?">
                                                                                                                                                                                    <field name="value">&lt;u&gt;Step 4: Finally choose if there are more ${qa == 1 ? list_object[more]:list_object[less]} than ${qa == 1 ? list_object[less]:list_object[more]} in the ${place}.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;b style='color: green'&gt;${count_x((qa == 1 ? more:less), array_ob)}&lt;/b&gt; ${qa == 1? 'more than':'less than'} &lt;b style='color: red'&gt;${count_x((qa == 1 ? less:more), array_ob)}&lt;/b&gt;.&lt;/br&gt;&lt;/br&gt;
It is &lt;b&gt;${qa == 1? 'true':'false'}&lt;/b&gt; that there are more &lt;b&gt;${qa == 1 ? list_object[more]:list_object[less]}&lt;/b&gt; than &lt;b&gt;${qa == 1 ? list_object[less]:list_object[more]}&lt;/b&gt; in the ${place}.
                                                                                                                                                                                    </field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <statement name="else">
                                                                                                                              <block type="partial_explanation" id="c,Sw34W(NmsJOaRTW01T" inline="true">
                                                                                                                                <value name="value">
                                                                                                                                  <block type="string_value" id="p=qAhKdU0H{l|0-O%oy:">
                                                                                                                                    <field name="value">&lt;u&gt;Step 2: Now let's count the number of ${list_object[ob]} in the ${place}.&lt;/u&gt;&lt;/br&gt;
&lt;style&gt;
  table{background-image: url("@1sprite.src(${p})"); width: 800px; height: 450px;}
td{text-align: center;width: 100px}
img{max-width: 70px; max-height: 60px}
#choise {border: 3px solid green; border-radius: 40px; display: inline-block; width: 70px; height: 70px}
  &lt;/style&gt;
&lt;center&gt;
  &lt;table&gt;
    &lt;tr&gt;&lt;td colspan=${max==18?6:7}&gt;&lt;/td&gt;&lt;/tr&gt;
                                                                                                                                    </field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="variable" id="@1pjR|,fNqTnX]XAm(E[_">
                                                                                                                                    <field name="name">count</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="YZuF!C)yQXn(kFk@2Oqdg">
                                                                                                                                        <field name="value">0</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="variable" id="gx@1ZEV-+Dy@1@1GOp^tT$Z">
                                                                                                                                        <field name="name">i</field>
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="expression" id="ZbOR9oKp-.9[!+mc#OL^">
                                                                                                                                            <field name="value">0</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="while_do_block" id="vLM!Y=SHuPr;G[/1E2J,">
                                                                                                                                            <value name="while">
                                                                                                                                              <block type="expression" id="y{Ba.yE@1:@2hnuT8?^{U6">
                                                                                                                                                <field name="value">i &lt; (max==18?3:4)</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <statement name="do">
                                                                                                                                              <block type="partial_explanation" id="_/CHKTT8xSQs?-zP}6YO" inline="true">
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="string_value" id="OMY9SpAP,/kWXpH0$v{P">
                                                                                                                                                    <field name="value">&lt;tr&gt;</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="variable" id="U~U/NClG@2t!(5vz%g?=2">
                                                                                                                                                    <field name="name">j</field>
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="expression" id="U6@1V$]97@2|ENzT3CnmrI">
                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="while_do_block" id="B@1Tg@2]TYen{)}A=8ms56">
                                                                                                                                                        <value name="while">
                                                                                                                                                          <block type="expression" id="_3nz!eXm]nY?)=zJ.5%a">
                                                                                                                                                            <field name="value">j &lt; (max==18?6:7)</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <statement name="do">
                                                                                                                                                          <block type="if_then_else_block" id="e?ZE/!Gje@1!0-SB:+^zz">
                                                                                                                                                            <value name="if">
                                                                                                                                                              <block type="expression" id=")_d0TsoqpptRO=m7ld5P">
                                                                                                                                                                <field name="value">array_ob[(max==18?6:7)@2i + j] &gt;= 0</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <statement name="then">
                                                                                                                                                              <block type="if_then_else_block" id="-O^XTmhMWcn,(QS.UH5[">
                                                                                                                                                                <value name="if">
                                                                                                                                                                  <block type="expression" id="3##+6EJaXy]f[klP}L3^">
                                                                                                                                                                    <field name="value">array_ob[(max==18?6:7)@2i + j] == ob</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <statement name="then">
                                                                                                                                                                  <block type="partial_explanation" id="b%Z/S`V|uLD`3(AL`1v!" inline="true">
                                                                                                                                                                    <value name="value">
                                                                                                                                                                      <block type="string_value" id="GWxLR4R@1YOHy:,di,:dL">
                                                                                                                                                                        <field name="value">&lt;td&gt;&lt;div id='choise'&gt;&lt;img style='padding: 5px' src='@1sprite.src(${p}_${array_ob[(max==18?6:7)@2i + j]})'/&gt;&lt;/div&gt; ${count+1}&lt;/td&gt;</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="variable" id="~e?PM6x-?P73?1Z6b@2ly">
                                                                                                                                                                        <field name="name">count</field>
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="expression" id="zsD5}m3Ff5^LvCO,??%^">
                                                                                                                                                                            <field name="value">count +1</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                                <statement name="else">
                                                                                                                                                                  <block type="partial_explanation" id="smky{MCNIBdPTjq1d3@2t" inline="true">
                                                                                                                                                                    <value name="value">
                                                                                                                                                                      <block type="string_value" id="{,UV)mvBV?lmA|K]sa7U">
                                                                                                                                                                        <field name="value">&lt;td&gt;&lt;img src='@1sprite.src(${p}_${array_ob[(max==18?6:7)@2i + j]})'/&gt;&lt;/td&gt;</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                            <statement name="else">
                                                                                                                                                              <block type="partial_explanation" id="LN}{av9-2:YK6H3[A)W$" inline="true">
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="string_value" id="ima5vh%+Cty5vA`-lh;}">
                                                                                                                                                                    <field name="value">&lt;td&gt;&lt;/td&gt;</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="variable" id="gO{kP3Kk;:y:a|wIq[|6">
                                                                                                                                                                <field name="name">j</field>
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="expression" id="`pG@18Ty_Ap=lq=u`i5;Y">
                                                                                                                                                                    <field name="value">j+1</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="partial_explanation" id="Vv:R!qu}@1z`LO|?87[cK" inline="true">
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="string_value" id="50/(D9nXr0EoPuX|[mEQ">
                                                                                                                                                                <field name="value">&lt;/tr&gt;</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="variable" id="pn8r?SMsg-^ZpwE+`qB:">
                                                                                                                                                                <field name="name">i</field>
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="expression" id="2utx9Yn4;7/$@1m/e!U+^">
                                                                                                                                                                    <field name="value">i+1</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                            <next>
                                                                                                                                              <block type="partial_explanation" id="uo(=T++Q.:mf;AEFt$|4" inline="true">
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="string_value" id="cA{uJkDZYTJB[g@2?h;Pq">
                                                                                                                                                    <field name="value">&lt;tr&gt;&lt;td colspan=${max==18?6:7}&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
  &lt;/br&gt;
&lt;span style='background: rgb(250, 250, 250, 0.5)'&gt;
The number of &lt;b&gt;${list_object[ob]}${count_x(ob, array_ob) &lt; 2 ? '':'s'}&lt;/b&gt; in the ${place} is &lt;b&gt;${count_x(ob, array_ob)}&lt;/b&gt;.&lt;/br&gt;
It is &lt;b&gt;${qa == 1? 'true':'false'}&lt;/b&gt; that the ${place} does not have any &lt;b&gt;${list_object[ob]}s&lt;/b&gt;.&lt;/span&gt;
&lt;/center&gt;
                                                                                                                                                    </field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="end_partial_explanation" id="tJf%o+LJR_Y0-1if9WYb"></block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
</xml>
END_XML]] */