
module.exports = [
  {
    "#type": "question",
    "name": "Y2.MG.VC.RUIU.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y2.MG.VC.RUIU.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "statement",
        "value": "function jsUcfirst(string) \n{\n    return string.charAt(0).toUpperCase() + string.slice(1);\n}\n\nfunction re_max(a, b){\n  if(a > b){return a;}\n  return b;\n}\n\nfunction re_min(a, b){\n  if(a < b){return a;}\n  return b;\n}"
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "range < 4"
        },
        "then": [
          {
            "#type": "variable",
            "name": "volume",
            "value": {
              "#type": "expression",
              "value": "10"
            }
          }
        ],
        "else": [
          {
            "#type": "variable",
            "name": "volume",
            "value": {
              "#type": "expression",
              "value": "20"
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "type",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "2"
          }
        }
      },
      {
        "#type": "variable",
        "name": "items",
        "value": {
          "#type": "string_array",
          "items": "rubber duckie|slippers|beach ball|doll|Teddy bear|T-shirt|cup|bowl|school bag|hammer|pot|guitar|shovel|keyboard|television"
        }
      },
      {
        "#type": "variable",
        "name": "unit",
        "value": {
          "#type": "string_array",
          "items": "test tube|cup|beaker"
        }
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "variable",
            "name": "it",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "0"
              },
              "max": {
                "#type": "expression",
                "value": "14"
              }
            }
          },
          {
            "#type": "variable",
            "name": "u",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "0"
              },
              "max": {
                "#type": "expression",
                "value": "2"
              }
            }
          },
          {
            "#type": "variable",
            "name": "empty",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "1"
              },
              "max": {
                "#type": "expression",
                "value": "5"
              }
            }
          },
          {
            "#type": "variable",
            "name": "level",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "empty + 1"
              },
              "max": {
                "#type": "expression",
                "value": "volume"
              }
            }
          },
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "40"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "The below objects were dipped into water.\nWhat is the volume of the ${items[it]}?"
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "36"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "2"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "200"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "700"
                },
                "height": {
                  "#type": "expression",
                  "value": "235"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "[0, 1]"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template_for",
                "variable": "$cell",
                "condition": "$cell.col == 0",
                "#prop": "cell.templates[]",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "0"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "0"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "${volume}_${empty}.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}${volume}_${empty}.png"
                      }
                    ]
                  }
                ]
              },
              {
                "#type": "prop_grid_cell_template_for",
                "variable": "$cell",
                "condition": "$cell.col == 1",
                "#prop": "cell.templates[]",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "0"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "0"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "${volume}_${level}.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}${volume}_${level}.png"
                      }
                    ]
                  },
                  {
                    "#type": "grid_shape",
                    "#props": [
                      {
                        "#type": "prop_grid_dimension",
                        "#prop": "",
                        "rows": {
                          "#type": "expression",
                          "value": "1"
                        },
                        "cols": {
                          "#type": "expression",
                          "value": "1"
                        }
                      },
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX+ 20"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY+40"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "120"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "150"
                        }
                      },
                      {
                        "#type": "prop_anchor",
                        "#prop": "anchor",
                        "x": {
                          "#type": "expression",
                          "value": "0.5"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "0.5"
                        }
                      },
                      {
                        "#type": "prop_grid_cell_source",
                        "#prop": "cell.source",
                        "value": {
                          "#type": "expression",
                          "value": "1"
                        }
                      },
                      {
                        "#type": "prop_grid_random",
                        "#prop": "",
                        "random": false
                      },
                      {
                        "#type": "prop_grid_show_borders",
                        "#prop": "#showBorders",
                        "value": false
                      },
                      {
                        "#type": "prop_grid_cell_template",
                        "variable": "$cell",
                        "#prop": "cell.template",
                        "#callback": "$cell",
                        "body": [
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.bottom - 10"
                                }
                              },
                              {
                                "#type": "prop_anchor",
                                "#prop": "anchor",
                                "x": {
                                  "#type": "expression",
                                  "value": "0.5"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "1"
                                }
                              },
                              {
                                "#type": "prop_max_size",
                                "#prop": "",
                                "maxWidth": {
                                  "#type": "expression",
                                  "value": "$cell.width"
                                },
                                "maxHeight": {
                                  "#type": "expression",
                                  "value": "0"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "${it}.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}${it}.png"
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "list_shape",
            "#props": [
              {
                "#type": "prop_list_direction",
                "#prop": "",
                "dir": "horizontal"
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "350"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              }
            ],
            "items": [
              {
                "#type": "json",
                "#props": [
                  {
                    "#type": "prop_list_align",
                    "#prop": "",
                    "align": "middle"
                  }
                ],
                "template": {
                  "#callback": "$item",
                  "variable": "$item",
                  "body": [
                    {
                      "#type": "text_shape",
                      "#props": [
                        {
                          "#type": "prop_text_contents",
                          "#prop": "",
                          "contents": "Answer:${' '}"
                        },
                        {
                          "#prop": "",
                          "style": {
                            "#type": "json",
                            "base": "text",
                            "#props": [
                              {
                                "#type": "prop_text_style_font_size",
                                "#prop": "",
                                "fontSize": {
                                  "#type": "expression",
                                  "value": "36"
                                }
                              },
                              {
                                "#type": "prop_text_style_fill",
                                "#prop": "",
                                "fill": "black"
                              },
                              {
                                "#type": "prop_text_style_stroke",
                                "#prop": "",
                                "stroke": "white"
                              },
                              {
                                "#type": "prop_text_style_stroke_thickness",
                                "#prop": "",
                                "strokeThickness": {
                                  "#type": "expression",
                                  "value": "2"
                                }
                              }
                            ]
                          }
                        }
                      ]
                    }
                  ]
                }
              },
              {
                "#type": "json",
                "#props": [
                  {
                    "#type": "prop_list_align",
                    "#prop": "",
                    "align": "middle"
                  }
                ],
                "template": {
                  "#callback": "$item",
                  "variable": "$item",
                  "body": [
                    {
                      "#type": "image_shape",
                      "#props": [
                        {
                          "#type": "prop_image_key",
                          "#prop": "",
                          "key": "shape.png"
                        },
                        {
                          "#type": "prop_image_src",
                          "#prop": "",
                          "src": "${image_path}shape.png"
                        }
                      ]
                    },
                    {
                      "#type": "choice_input_shape",
                      "#props": [
                        {
                          "#type": "prop_value",
                          "#prop": "",
                          "value": {
                            "#type": "expression",
                            "value": "level-empty"
                          }
                        },
                        {
                          "#type": "prop_size",
                          "#prop": "",
                          "width": {
                            "#type": "expression",
                            "value": "123"
                          },
                          "height": {
                            "#type": "expression",
                            "value": "69"
                          }
                        },
                        {
                          "#type": "prop_input_keyboard",
                          "#prop": "",
                          "keyboard": "numbers1"
                        },
                        {
                          "#type": "prop_input_max_length",
                          "#prop": "",
                          "maxLength": {
                            "#type": "expression",
                            "value": "(level-empty).toString().length"
                          }
                        },
                        {
                          "#type": "prop_input_result_position",
                          "#prop": "",
                          "resultPosition": "bottom"
                        },
                        {
                          "#type": "prop_tab_order",
                          "#prop": "",
                          "tabOrder": {
                            "#type": "expression",
                            "value": "0"
                          }
                        },
                        {
                          "#type": "prop_stroke",
                          "#prop": "stroke"
                        },
                        {
                          "#type": "prop_fill",
                          "#prop": "fill"
                        },
                        {
                          "#prop": "",
                          "style": {
                            "#type": "json",
                            "base": "text",
                            "#props": [
                              {
                                "#type": "prop_text_style_font_size",
                                "#prop": "",
                                "fontSize": {
                                  "#type": "expression",
                                  "value": "32"
                                }
                              },
                              {
                                "#type": "prop_text_style_fill",
                                "#prop": "",
                                "fill": "black"
                              },
                              {
                                "#type": "prop_text_style_stroke",
                                "#prop": "",
                                "stroke": "white"
                              },
                              {
                                "#type": "prop_text_style_stroke_thickness",
                                "#prop": "",
                                "strokeThickness": {
                                  "#type": "expression",
                                  "value": "2"
                                }
                              }
                            ]
                          }
                        }
                      ],
                      "#init": "algorithmic_input"
                    }
                  ]
                }
              },
              {
                "#type": "json",
                "#props": [
                  {
                    "#type": "prop_list_align",
                    "#prop": "",
                    "align": "middle"
                  }
                ],
                "template": {
                  "#callback": "$item",
                  "variable": "$item",
                  "body": [
                    {
                      "#type": "text_shape",
                      "#props": [
                        {
                          "#type": "prop_text_contents",
                          "#prop": "",
                          "contents": "${' ' + unit[u] + (level - empty == 1 ? '':'s')}"
                        },
                        {
                          "#prop": "",
                          "style": {
                            "#type": "json",
                            "base": "text",
                            "#props": [
                              {
                                "#type": "prop_text_style_font_size",
                                "#prop": "",
                                "fontSize": {
                                  "#type": "expression",
                                  "value": "36"
                                }
                              },
                              {
                                "#type": "prop_text_style_fill",
                                "#prop": "",
                                "fill": "black"
                              },
                              {
                                "#type": "prop_text_style_stroke",
                                "#prop": "",
                                "stroke": "white"
                              },
                              {
                                "#type": "prop_text_style_stroke_thickness",
                                "#prop": "",
                                "strokeThickness": {
                                  "#type": "expression",
                                  "value": "2"
                                }
                              }
                            ]
                          }
                        }
                      ]
                    }
                  ]
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type== 2"
        },
        "then": [
          {
            "#type": "variable",
            "name": "empty",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "1"
              },
              "max": {
                "#type": "expression",
                "value": "5"
              }
            }
          },
          {
            "#type": "variable",
            "name": "it",
            "value": {
              "#type": "random_many",
              "count": {
                "#type": "expression",
                "value": "2"
              },
              "items": {
                "#type": "func",
                "name": "arrayOfNumber",
                "args": [
                  {
                    "#type": "expression",
                    "value": "15"
                  },
                  {
                    "#type": "expression",
                    "value": "0"
                  }
                ]
              }
            }
          },
          {
            "#type": "variable",
            "name": "level",
            "value": {
              "#type": "random_many",
              "count": {
                "#type": "expression",
                "value": "2"
              },
              "items": {
                "#type": "func",
                "name": "arrayOfNumber",
                "args": [
                  {
                    "#type": "expression",
                    "value": "volume - 6"
                  },
                  {
                    "#type": "expression",
                    "value": "6"
                  }
                ]
              }
            }
          },
          {
            "#type": "variable",
            "name": "it",
            "value": {
              "#type": "func",
              "name": "sort",
              "args": [
                {
                  "#type": "expression",
                  "value": "it"
                },
                "asc"
              ]
            }
          },
          {
            "#type": "variable",
            "name": "level",
            "value": {
              "#type": "func",
              "name": "sort",
              "args": [
                {
                  "#type": "expression",
                  "value": "level"
                },
                "asc"
              ]
            }
          },
          {
            "#type": "variable",
            "name": "u",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "0"
              },
              "max": {
                "#type": "expression",
                "value": "2"
              }
            }
          },
          {
            "#type": "variable",
            "name": "con",
            "value": {
              "#type": "random_one",
              "items": {
                "#type": "expression",
                "value": "['larger', 'smaller']"
              }
            }
          },
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "con == 'larger'"
            },
            "then": [
              {
                "#type": "func",
                "name": "addAnswers",
                "args": [
                  {
                    "#type": "expression",
                    "value": "re_max(it[0], it[1])"
                  }
                ]
              }
            ],
            "else": [
              {
                "#type": "func",
                "name": "addAnswers",
                "args": [
                  {
                    "#type": "expression",
                    "value": "re_min(it[0], it[1])"
                  }
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "arr",
            "value": {
              "#type": "func",
              "name": "shuffle",
              "args": [
                {
                  "#type": "expression",
                  "value": "[0, 1]"
                }
              ]
            }
          },
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "40"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "The below objects were dipped into water.\nWhich item had a ${con} volume?"
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "36"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "150"
                },
                "y": {
                  "#type": "expression",
                  "value": "215"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "0"
                },
                "height": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "${volume}_${empty}.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}${volume}_${empty}.png"
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "2"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "520"
                },
                "y": {
                  "#type": "expression",
                  "value": "215"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "480"
                },
                "height": {
                  "#type": "expression",
                  "value": "235"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "arr"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "0"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "0"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "${volume}_${level[$cell.data]}.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}${volume}_${level[$cell.data]}.png"
                      }
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "data",
                    "value": {
                      "#type": "expression",
                      "value": "$cell.data"
                    }
                  },
                  {
                    "#type": "grid_shape",
                    "#props": [
                      {
                        "#type": "prop_grid_dimension",
                        "#prop": "",
                        "rows": {
                          "#type": "expression",
                          "value": "1"
                        },
                        "cols": {
                          "#type": "expression",
                          "value": "1"
                        }
                      },
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX+ 20"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY+40"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "120"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "150"
                        }
                      },
                      {
                        "#type": "prop_anchor",
                        "#prop": "anchor",
                        "x": {
                          "#type": "expression",
                          "value": "0.5"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "0.5"
                        }
                      },
                      {
                        "#type": "prop_grid_cell_source",
                        "#prop": "cell.source",
                        "value": {
                          "#type": "expression",
                          "value": "1"
                        }
                      },
                      {
                        "#type": "prop_grid_random",
                        "#prop": "",
                        "random": false
                      },
                      {
                        "#type": "prop_grid_show_borders",
                        "#prop": "#showBorders",
                        "value": false
                      },
                      {
                        "#type": "prop_grid_cell_template",
                        "variable": "$cell",
                        "#prop": "cell.template",
                        "#callback": "$cell",
                        "body": [
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.bottom - 10"
                                }
                              },
                              {
                                "#type": "prop_anchor",
                                "#prop": "anchor",
                                "x": {
                                  "#type": "expression",
                                  "value": "0.5"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "1"
                                }
                              },
                              {
                                "#type": "prop_max_size",
                                "#prop": "",
                                "maxWidth": {
                                  "#type": "expression",
                                  "value": "$cell.width"
                                },
                                "maxHeight": {
                                  "#type": "expression",
                                  "value": "0"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "${it[data]}.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}${it[data]}.png"
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "2"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "520"
                },
                "y": {
                  "#type": "expression",
                  "value": "400"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "480"
                },
                "height": {
                  "#type": "expression",
                  "value": "50"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "arr"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "choice_custom_shape",
                    "action": "click-one",
                    "value": {
                      "#type": "expression",
                      "value": "it[$cell.data]"
                    },
                    "template": {
                      "#callback": "$choice",
                      "variable": "$choice",
                      "body": [
                        {
                          "#type": "image_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "$cell.centerX"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "$cell.centerY"
                              }
                            },
                            {
                              "#type": "prop_size",
                              "#prop": "",
                              "width": {
                                "#type": "expression",
                                "value": "0"
                              },
                              "height": {
                                "#type": "expression",
                                "value": "0"
                              }
                            },
                            {
                              "#type": "prop_image_key",
                              "#prop": "",
                              "key": "box.png"
                            },
                            {
                              "#type": "prop_image_src",
                              "#prop": "",
                              "src": "${image_path}box.png"
                            }
                          ]
                        },
                        {
                          "#type": "text_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "$cell.centerX"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "$cell.centerY+3"
                              }
                            },
                            {
                              "#type": "prop_text_contents",
                              "#prop": "",
                              "contents": "${jsUcfirst(items[it[$cell.data]])}"
                            },
                            {
                              "#prop": "",
                              "style": {
                                "#type": "json",
                                "base": "text",
                                "#props": [
                                  {
                                    "#type": "prop_text_style_font_size",
                                    "#prop": "",
                                    "fontSize": {
                                      "#type": "expression",
                                      "value": "36"
                                    }
                                  },
                                  {
                                    "#type": "prop_text_style_fill",
                                    "#prop": "",
                                    "fill": "black"
                                  },
                                  {
                                    "#type": "prop_text_style_stroke",
                                    "#prop": "",
                                    "stroke": "white"
                                  },
                                  {
                                    "#type": "prop_text_style_stroke_thickness",
                                    "#prop": "",
                                    "strokeThickness": {
                                      "#type": "expression",
                                      "value": "2"
                                    }
                                  }
                                ]
                              }
                            }
                          ]
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 1: From the question, look for the item to focus on.</u></br></br>\n\n<span style='background: rgb(250, 250, 250, 0.3)'>\nWhat is the volume of the <b>${items[it]}</b>?\n</br></r>\nThe item to focus on is the <b>${items[it]}</b>.</span>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 2: Now let's calculate the volume of the ${items[it]} by subtracting the original water level from the water level with the ${items[it]}.</u></br></br>\n<span style='background: rgb(250, 250, 250, 0.3)'>\n${level} - ${empty} = <b>${level - empty}</b></br></br>\n\nThe <b>${items[it]}</b> has a volume of <b>${level - empty}</b> ${unit[u]}${level - empty > 1 ? 's': ''}.</span>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 2"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 1: From the question, look for the item to focus on.</u></br></br>\n\n<span style='background: rgb(250, 250, 250, 0.3)'>\nWhich item had a <b>${con}</b> volume?\n</br></r>\nThe condition to focus on is the item with a <b>${con}</b> volume.</span>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 2: Now let's calculate the volume of the items by subtracting the original water level from the water level with the item.\n</u></br></br>\n<span style='background: rgb(250, 250, 250, 0.3)'>\n  \n${jsUcfirst(items[it[0]])}</br>\n${level[0]} - ${empty} = <b>${level[0] - empty}</b></br></br>\n  \n${jsUcfirst(items[it[1]])}</br>\n${level[1]} - ${empty} = <b>${level[1] - empty}</b></span>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 3: Finally, choose the item with a ${con} volume.</u></br></br>\n<span style='background: rgb(250, 250, 250, 0.3)'>\nThe <b>${items[con == 'larger' ? re_max(it[0], it[1]) : re_min(it[0], it[1])]}</b> has a <b>${con}</b> volume.</span>"
            ]
          }
        ]
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="0" y="0">
    <field name="name">Y2.MG.VC.RUIU.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y2.MG.VC.RUIU.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="YepMQT@2uwp2rRPvmnRg1">
                                    <field name="name">range</field>
                                    <value name="value">
                                      <block type="expression" id="UxgQ^Y5]rb}vpw[D=l-!">
                                        <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="statement" id="I?.f@1y~?@2Ham+tnoUDD0">
                                        <field name="value">function jsUcfirst(string) 
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function re_max(a, b){
  if(a &gt; b){return a;}
  return b;
}

function re_min(a, b){
  if(a &lt; b){return a;}
  return b;
}
                                        </field>
                                        <next>
                                          <block type="if_then_else_block" id="W@1/|(ag!3g:lyj|cI9Bd">
                                            <value name="if">
                                              <block type="expression" id="sv~A+|HHZRYDb{nVSt-:">
                                                <field name="value">range &lt; 4</field>
                                              </block>
                                            </value>
                                            <statement name="then">
                                              <block type="variable" id="u)OpKP6$E(GZ=@2`.$^Jk">
                                                <field name="name">volume</field>
                                                <value name="value">
                                                  <block type="expression" id="|kD|x-TwOBOKenvrjttW">
                                                    <field name="value">10</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </statement>
                                            <statement name="else">
                                              <block type="variable" id="P-s{lF@2K%yqG$O+:MDwb">
                                                <field name="name">volume</field>
                                                <value name="value">
                                                  <block type="expression" id="S[{RhKJ8UAO3F(WW,IQT">
                                                    <field name="value">20</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </statement>
                                            <next>
                                              <block type="variable" id="i8?V7/^w2s8|ukNcQ3q|">
                                                <field name="name">type</field>
                                                <value name="value">
                                                  <block type="random_number" id="5aw#`@1g64SP?;Hfjjk!0">
                                                    <value name="min">
                                                      <block type="expression" id="Z=+_o#hFP(xd`ZlJVF1(">
                                                        <field name="value">1</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id=")g5enQx7B^dlwH22N;Z@2">
                                                        <field name="value">2</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="Mbnh-[-FM?P3?,Of%qyj">
                                                    <field name="name">items</field>
                                                    <value name="value">
                                                      <block type="string_array" id="m;?nHz%^e0NDqE@2UFpFc">
                                                        <field name="items">rubber duckie|slippers|beach ball|doll|Teddy bear|T-shirt|cup|bowl|school bag|hammer|pot|guitar|shovel|keyboard|television</field>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id="yk#!0|.7e@1n?XrWW)4/k">
                                                        <field name="name">unit</field>
                                                        <value name="value">
                                                          <block type="string_array" id="`E;_DT2)~iHWw@21H]vSS">
                                                            <field name="items">test tube|cup|beaker</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="if_then_block" id="/H$^`jCH[tMRM1YJ?RVW">
                                                            <value name="if">
                                                              <block type="expression" id="v5gY[,(}|bfvfZa)pQC/">
                                                                <field name="value">type == 1</field>
                                                              </block>
                                                            </value>
                                                            <statement name="then">
                                                              <block type="variable" id="HV!~{Ttq+%q=NhRV~?">
                                                                <field name="name">it</field>
                                                                <value name="value">
                                                                  <block type="random_number" id="!V.DFMG+0R@2#=Ie#gmEa">
                                                                    <value name="min">
                                                                      <block type="expression" id="yf7r;A{{Sac`2kZec@1(G">
                                                                        <field name="value">0</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="max">
                                                                      <block type="expression" id="Z]Yy4,/%fxWMjK0PN7Iy">
                                                                        <field name="value">14</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="variable" id="qOIJv3v@1FG?_Wv~{6I(U">
                                                                    <field name="name">u</field>
                                                                    <value name="value">
                                                                      <block type="random_number" id="XoFRfku~hFyCPF61RJ4c">
                                                                        <value name="min">
                                                                          <block type="expression" id="LPdrPv@1h3TkP3`N+3@1ho">
                                                                            <field name="value">0</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="max">
                                                                          <block type="expression" id="Y_8%@1F$3i/-e,H+]}rmn">
                                                                            <field name="value">2</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="variable" id=":#QCh:^Ii+Jn7j)Og+ZD">
                                                                        <field name="name">empty</field>
                                                                        <value name="value">
                                                                          <block type="random_number" id="tG47n^@2kYBy_NBLwStdA">
                                                                            <value name="min">
                                                                              <block type="expression" id="Gs}d[|(_Q`;I~[+.l_g`">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="max">
                                                                              <block type="expression" id="k~ZVks2$DW4%(9lmf=Yx">
                                                                                <field name="value">5</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="JPzE|hE@2o-ck0o?AQ!=m">
                                                                            <field name="name">level</field>
                                                                            <value name="value">
                                                                              <block type="random_number" id="Dd-f+w4^n`uNkk(!`9o@1">
                                                                                <value name="min">
                                                                                  <block type="expression" id="w;2K0h[)lc0I(mt`.Lkp">
                                                                                    <field name="value">empty + 1</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="max">
                                                                                  <block type="expression" id="r7MU8V!915,usR8=[.Fa">
                                                                                    <field name="value">volume</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                                                <statement name="#props">
                                                                                  <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                                                        <field name="value">400</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                                                        <field name="value">40</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_anchor" id="O.v@2mKn@1K#+tF;|}C}xI">
                                                                                        <field name="#prop">anchor</field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="+3%GU[X3l%%X`Jv1QL7@2">
                                                                                            <field name="value">0.5</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="Kzp23HjHF7k]A-y1BJPm">
                                                                                            <field name="value">0.5</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="contents">
                                                                                              <block type="string_value" id="#6btV9p,PGV}wMx.U�">
                                                                                                <field name="value">The below objects were dipped into water.
What is the volume of the ${items[it]}?
                                                                                                </field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                                                <field name="base">text</field>
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="fontSize">
                                                                                                      <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                                                        <field name="value">36</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="fill">
                                                                                                          <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                                                            <field name="value">black</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="stroke">
                                                                                                              <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                                                <field name="value">white</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="strokeThickness">
                                                                                                                  <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                                                    <field name="value">2</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="grid_shape" id="6tU?K^N8ec~A@1@1NwQ72N">
                                                                                    <statement name="#props">
                                                                                      <block type="prop_grid_dimension" id="X4K:|9dKK%s9l4n5v@2lG">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="rows">
                                                                                          <block type="expression" id="[Ev!.bxnzjc|-33v.%7=">
                                                                                            <field name="value">1</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="cols">
                                                                                          <block type="expression" id="u$7]/V/!ZWM4(Va??@2({">
                                                                                            <field name="value">2</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_position" id="zLld84TfBg#!88]1,JD-">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="Cd6g{Y_P?:GyVyvQi1wJ">
                                                                                                <field name="value">400</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id=".F}E71+@1i)77!yQ`hmfu">
                                                                                                <field name="value">200</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_size" id="+o2CWr^.ohx)_bcIG53]">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="width">
                                                                                                  <block type="expression" id="Z[x5iHG%KWC%KWQb}c-G">
                                                                                                    <field name="value">700</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="height">
                                                                                                  <block type="expression" id="@2E|Ob@1jF#gYtX8-%Iui@1">
                                                                                                    <field name="value">235</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_anchor" id="1WJjXY^.h,t]eNgxaJ}q">
                                                                                                    <field name="#prop">anchor</field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="DHx[vqs?.?nUD:AYPJWd">
                                                                                                        <field name="value">0.5</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="e3D|RR7w!=Sufu):zV`c">
                                                                                                        <field name="value">0.5</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_cell_source" id="v8+Yu211Qg8fX9Mu4th`">
                                                                                                        <field name="#prop">cell.source</field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id="~j@1,^liddf}oDg}E91^i">
                                                                                                            <field name="value">[0, 1]</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_grid_random" id="}?kS#x[b)9OGb@1`UdJGV">
                                                                                                            <field name="#prop"></field>
                                                                                                            <field name="random">FALSE</field>
                                                                                                            <next>
                                                                                                              <block type="prop_grid_show_borders" id=";y^wk4y%[VWjXOj~vrQZ">
                                                                                                                <field name="#prop">#showBorders</field>
                                                                                                                <field name="value">FALSE</field>
                                                                                                                <next>
                                                                                                                  <block type="prop_grid_cell_template_for" id="5sj03_Vo4;vh2)!EOZ;d">
                                                                                                                    <field name="variable">$cell</field>
                                                                                                                    <field name="condition">$cell.col == 0</field>
                                                                                                                    <field name="#prop">cell.templates[]</field>
                                                                                                                    <field name="#callback">$cell</field>
                                                                                                                    <statement name="body">
                                                                                                                      <block type="image_shape" id="T.$(S1e)RiG]c.#6u!`8">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id=",6J7+Gm1H_72uB?h%Mdp">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="R?(~Vsu|IR6QJC5By7V[">
                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="Dhws~Z,}@2(Yznp_@1D^-$">
                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_size" id="k~(fV;@20,+ACtA@2sejUc">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="width">
                                                                                                                                  <block type="expression" id="tnkb??XP(dau|Nds(R6y">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="height">
                                                                                                                                  <block type="expression" id="Bo}Gvo]U`QPS;]sR{MdA">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_key" id="+8tWSxjr.V0WjK$[+rjG">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="key">
                                                                                                                                      <block type="string_value" id="0U(!S.U1@1k+r(Zv^;8;z">
                                                                                                                                        <field name="value">${volume}_${empty}.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_src" id="+x:a7vNdvdK9z.TV`Ba?">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="src">
                                                                                                                                          <block type="string_value" id="nky@2i}N.~a;DnikVw2wL">
                                                                                                                                            <field name="value">${image_path}${volume}_${empty}.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="prop_grid_cell_template_for" id="Bq}_WxY#D^%^~vFx@1HaW">
                                                                                                                        <field name="variable">$cell</field>
                                                                                                                        <field name="condition">$cell.col == 1</field>
                                                                                                                        <field name="#prop">cell.templates[]</field>
                                                                                                                        <field name="#callback">$cell</field>
                                                                                                                        <statement name="body">
                                                                                                                          <block type="image_shape" id="77n85C7{UT-kRJ0NQ10-">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="_/GpR)yyzJuN=G.dPvu2">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="b5;FBmClsNvr[s5;UZnQ">
                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id=")w0ad!u`cad^[u~fCK9b">
                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_size" id="u3hv[)}A2Bf/.7Fy`rbw">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="width">
                                                                                                                                      <block type="expression" id="8iOfXa2~IXAkN8|3Az}p">
                                                                                                                                        <field name="value">0</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="height">
                                                                                                                                      <block type="expression" id="{|3OzYPO3r7_;@1=6LQRA">
                                                                                                                                        <field name="value">0</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_key" id="b-zN(f%,d%8m,h38pOFe">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="key">
                                                                                                                                          <block type="string_value" id="qR#[hc6Mw[mMm%y(w/4f">
                                                                                                                                            <field name="value">${volume}_${level}.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_src" id="{.Xt:_:8Z.]$4GakJ}.R">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="src">
                                                                                                                                              <block type="string_value" id="VC(_aV]rJV,Orht@1}%s}">
                                                                                                                                                <field name="value">${image_path}${volume}_${level}.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="grid_shape" id="}p)5nc}d=m%QAbc4@2@2d^">
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_grid_dimension" id="M3To7ZpF;8mbI=iezl9(">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="rows">
                                                                                                                                      <block type="expression" id="Rt$B7jn{t6$h|U?b{em2">
                                                                                                                                        <field name="value">1</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="cols">
                                                                                                                                      <block type="expression" id="fC`NEm(c~k+4}LW9_|S0">
                                                                                                                                        <field name="value">1</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_position" id="R-3{=+}@2+N;6K[w5@10o/">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="expression" id="L;wo-n@1#0zdZXk|y3DET">
                                                                                                                                            <field name="value">$cell.centerX+ 20</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id="m[tZWvF@2$l)1}{XEU#]z">
                                                                                                                                            <field name="value">$cell.centerY+40</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_size" id="`s@27Znc{2Hhv+I9ps8|!">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="width">
                                                                                                                                              <block type="expression" id="c?~/U=?-}8g4ORX@1G),@1">
                                                                                                                                                <field name="value">120</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="height">
                                                                                                                                              <block type="expression" id="{Cf-S@2buh@1)ej=um5_Ng">
                                                                                                                                                <field name="value">150</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_anchor" id="aZjY^nqwg@2XNR5Ey.]M$">
                                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                                <value name="x">
                                                                                                                                                  <block type="expression" id="-G~Gnjum/M9n7bzX$(r{">
                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <value name="y">
                                                                                                                                                  <block type="expression" id="AdP[PaD8:V@2/T=P.k1|v">
                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_grid_cell_source" id="~+[OYp)~o9r]F@1a^=.#B">
                                                                                                                                                    <field name="#prop">cell.source</field>
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="expression" id="g-6cZPP6rv(E:)o?M0.o">
                                                                                                                                                        <field name="value">1</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_grid_random" id="E;=LFUYBObyb{NhFz{B?">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <field name="random">FALSE</field>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_grid_show_borders" id="Z,THuwb+9W@2IV5TZW7CB">
                                                                                                                                                            <field name="#prop">#showBorders</field>
                                                                                                                                                            <field name="value">FALSE</field>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_grid_cell_template" id="7^6]Kv:2yrZi`cooAW2@2">
                                                                                                                                                                <field name="variable">$cell</field>
                                                                                                                                                                <field name="#prop">cell.template</field>
                                                                                                                                                                <field name="#callback">$cell</field>
                                                                                                                                                                <statement name="body">
                                                                                                                                                                  <block type="image_shape" id="WiFJK)sC%K:tP0hTD+?8">
                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                      <block type="prop_position" id="quBu8axE/(Sv^_~a/H,X">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="x">
                                                                                                                                                                          <block type="expression" id="L5}40okFB{w?^po5euz2">
                                                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <value name="y">
                                                                                                                                                                          <block type="expression" id="bo],9ZKV|r^gz}iW/.S:">
                                                                                                                                                                            <field name="value">$cell.bottom - 10</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_anchor" id="k2{bX5[7#MXFsHGyahr:">
                                                                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                                                                            <value name="x">
                                                                                                                                                                              <block type="expression" id="0^wf$6Ri+vx)ofYu9#@2M">
                                                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="y">
                                                                                                                                                                              <block type="expression" id="_3Hpx@2Qd{`QfV@2kzG%_b">
                                                                                                                                                                                <field name="value">1</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_max_size" id="ajWvHPgkd|-;l[ot6%rp">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="maxWidth">
                                                                                                                                                                                  <block type="expression" id=")O[e0%1MJ+P2L/r}Wi_T">
                                                                                                                                                                                    <field name="value">$cell.width</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <value name="maxHeight">
                                                                                                                                                                                  <block type="expression" id="$=}n(f@1nR@2kW@25~|zpo$">
                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_image_key" id="5#9sAg2[.A@1b6t%s~:5?">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="key">
                                                                                                                                                                                      <block type="string_value" id=";AO,$({p1o.P+|$Fl_K$">
                                                                                                                                                                                        <field name="value">${it}.png</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_image_src" id="]k1sg,muot7c8wvjf,kl">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="src">
                                                                                                                                                                                          <block type="string_value" id="L);.B}oowWmsI%l[SbJj">
                                                                                                                                                                                            <field name="value">${image_path}${it}.png</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <next>
                                                                                      <block type="list_shape" id="sY:c5(=G8O6CNdRm%e;Q">
                                                                                        <statement name="#props">
                                                                                          <block type="prop_list_direction" id="_^M,`UAeV@1eBH^k)i)/M">
                                                                                            <field name="#prop"></field>
                                                                                            <field name="dir">horizontal</field>
                                                                                            <next>
                                                                                              <block type="prop_position" id="rI1lY5Jc)e?N7RQgIWff">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="L{I%P1fo;(Uh_;_2ntiB">
                                                                                                    <field name="value">400</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="m+vuY#fn{weSb.4K$;we">
                                                                                                    <field name="value">350</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_anchor" id=")ZF7Ii@1.(6U76xN)Pi7a">
                                                                                                    <field name="#prop">anchor</field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="pPWP-ld56Srs(6!vfB}z">
                                                                                                        <field name="value">0.5</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="Tfe9U3`s%]U6y@1j.)y[D">
                                                                                                        <field name="value">0.5</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <statement name="items">
                                                                                          <block type="list_item_shape" id="H/#}g^!#|DLs%`1=uMD#">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_list_align" id="~Do|zd;nJ{(kHZaifvBn">
                                                                                                <field name="#prop"></field>
                                                                                                <field name="align">middle</field>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <statement name="template">
                                                                                              <block type="text_shape" id="#)C}:@1b@1+,w[eyay(]}P">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_text_contents" id="6!#jqZ39+!:;-6oVD{xK">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="contents">
                                                                                                      <block type="string_value" id="7}uv+FKQagdp+d=~q3Ea">
                                                                                                        <field name="value">Answer:${' '}</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style" id="5:5|}(H@2Mp5xt+xe,^X+">
                                                                                                        <field name="base">text</field>
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_text_style_font_size" id="NU:mD4sJkw{.N{mF+2J4">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="fontSize">
                                                                                                              <block type="expression" id=",Tl[:F%O|Wy:VKxQZD1$">
                                                                                                                <field name="value">36</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_fill" id="!b],0s?HT%=q4Ns$8jzr">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="fill">
                                                                                                                  <block type="string_value" id="W^~D)PBd-!!084.wC)r8">
                                                                                                                    <field name="value">black</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_stroke" id="%j5@2zQ5dO6A{RYOq^@29d">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="stroke">
                                                                                                                      <block type="string_value" id="fz92.rVqB}!evj!m{7fO">
                                                                                                                        <field name="value">white</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_stroke_thickness" id="GN50St6Jv%5o1[iP0kdF">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="strokeThickness">
                                                                                                                          <block type="expression" id="mn]+xqR~Oq?VyQ0QWAy]">
                                                                                                                            <field name="value">2</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="list_item_shape" id="la;]?@2#cHW[7qO^$ktDw">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_list_align" id="HgFl%YP;oCp=IhAVseQJ">
                                                                                                    <field name="#prop"></field>
                                                                                                    <field name="align">middle</field>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <statement name="template">
                                                                                                  <block type="image_shape" id="tL$?R97J21l9mSM6.x(H">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_image_key" id="v@1yF@1Mf7bd_xT.QSBr6o">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="key">
                                                                                                          <block type="string_value" id="}`EP5tR#cahB/}MYPT]]">
                                                                                                            <field name="value">shape.png</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_image_src" id="t6CG1Q5qa[(S99^]5Mij">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="src">
                                                                                                              <block type="string_value" id="+Cb}$bLDvJ/rRN)E)A)+">
                                                                                                                <field name="value">${image_path}shape.png</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="algorithmic_input_shape" id="[e}LcmqF@19.%T?]@1jB-Z">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_value" id="TN6clfe}:4H;@1k0w%+`S">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="@1Y0=VVDuz^gR8?]A.GgV">
                                                                                                                <field name="value">level-empty</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_size" id="~)S$^+Glx|;{-)@1m?MCt">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="width">
                                                                                                                  <block type="expression" id="wdRRw?lccA]3huvks^0:">
                                                                                                                    <field name="value">123</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="height">
                                                                                                                  <block type="expression" id="FlQ@1R-hpt5|W3YEsjl5h">
                                                                                                                    <field name="value">69</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_input_keyboard" id="pX[+1]_P03Uw?r6RhxOm">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <field name="keyboard">numbers1</field>
                                                                                                                    <next>
                                                                                                                      <block type="prop_input_max_length" id="e^196@2S|6ukB7~2Bxi9C">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="maxLength">
                                                                                                                          <block type="expression" id="/_E;-7``ONG+]nS_NxH+">
                                                                                                                            <field name="value">(level-empty).toString().length</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_input_result_position" id="#LH4fkex}OZhG#[@2d;3P">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <field name="resultPosition">bottom</field>
                                                                                                                            <next>
                                                                                                                              <block type="prop_tab_order" id="s|F@1Izb{|@2lRY,asD!hu">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="tabOrder">
                                                                                                                                  <block type="expression" id="2[il+_dgVQKQm?SvtFDj">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_stroke" id="D`nCliS/s1emx9euC0}t">
                                                                                                                                    <field name="#prop">stroke</field>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_fill" id="Dz-AaH/!wx?BO[MH9dIz">
                                                                                                                                        <field name="#prop">fill</field>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style" id="!1Qr3{IxD@1PMJ]G^LlcN">
                                                                                                                                            <field name="base">text</field>
                                                                                                                                            <statement name="#props">
                                                                                                                                              <block type="prop_text_style_font_size" id="!(nlSpda9T+o/iXBj}YF">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="fontSize">
                                                                                                                                                  <block type="expression" id="FQTZZB_+$@1-hM?K.!NSO">
                                                                                                                                                    <field name="value">32</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_text_style_fill" id="w-4`M-zsemYrO[}g`_|q">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="fill">
                                                                                                                                                      <block type="string_value" id="G;VmNGg}u8OZR_#3M_Oe">
                                                                                                                                                        <field name="value">black</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_text_style_stroke" id="ahppz0%Y{eD@2C$!vyJ4m">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="stroke">
                                                                                                                                                          <block type="string_value" id="}KNcD$1%M)+@1a=7A%7,,">
                                                                                                                                                            <field name="value">white</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_text_style_stroke_thickness" id="TBWlBG{V@2q@2LOjXi7|@16">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="strokeThickness">
                                                                                                                                                              <block type="expression" id="bs_awM@2W;X@1OG#fQ+@2no">
                                                                                                                                                                <field name="value">2</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="list_item_shape" id="_;1d_a7f9~Ni,8)%3p^8">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_list_align" id="nenBc8{=)ZwGSqBIcfyP">
                                                                                                        <field name="#prop"></field>
                                                                                                        <field name="align">middle</field>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <statement name="template">
                                                                                                      <block type="text_shape" id="]y{ko8$y!0_3OsJnpb@2[">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_text_contents" id="ZES:Pds)wW?0xC?(19">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="contents">
                                                                                                              <block type="string_value" id=",{GJa4WO45?1S(IRb%l^">
                                                                                                                <field name="value">${' ' + unit[u] + (level - empty == 1 ? '':'s')}</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style" id="7UuurzO3ivs`_vSs28~{">
                                                                                                                <field name="base">text</field>
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_text_style_font_size" id="/-@1v!X_W`el+S.VZ]m#-">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="fontSize">
                                                                                                                      <block type="expression" id="^2y{l%P.VJ$o|UpOb!E+">
                                                                                                                        <field name="value">36</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_fill" id="qe=8X`tX1k}olm9Fjb=8">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="fill">
                                                                                                                          <block type="string_value" id="7|BffKS/6UqH47(g9)[7">
                                                                                                                            <field name="value">black</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_stroke" id="/5WL_4?i-MGF-a^_Wc`z">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="stroke">
                                                                                                                              <block type="string_value" id="###Mu7Wd=Ye82hM{,(qG">
                                                                                                                                <field name="value">white</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_stroke_thickness" id="duFm~NK-+0MmwTqp@2`).">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="strokeThickness">
                                                                                                                                  <block type="expression" id="uB?-gja$xaXt@1v]iRe(d">
                                                                                                                                    <field name="value">2</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </statement>
                                                            <next>
                                                              <block type="if_then_block" id="?qc`V#caJBD?Y(1`d`BV">
                                                                <value name="if">
                                                                  <block type="expression" id="W;=)}Xd?2e0mPd%%,`_]">
                                                                    <field name="value">type== 2</field>
                                                                  </block>
                                                                </value>
                                                                <statement name="then">
                                                                  <block type="variable" id="(+H-q@2#$]7=L#p$]O4:R">
                                                                    <field name="name">empty</field>
                                                                    <value name="value">
                                                                      <block type="random_number" id="3ufaeB|A}kN4lHqK|i@1.">
                                                                        <value name="min">
                                                                          <block type="expression" id="guZ|W4iRh{fb2xQryR|0">
                                                                            <field name="value">1</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="max">
                                                                          <block type="expression" id="El2uUuC=X)w|+I)AttUh">
                                                                            <field name="value">5</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="variable" id="y3T+t)Q,B/dn3f;01?8z">
                                                                        <field name="name">it</field>
                                                                        <value name="value">
                                                                          <block type="random_many" id="b=Mb@2A@1(oZZ(Gq|h,lo8">
                                                                            <value name="count">
                                                                              <block type="expression" id="o-X7(85_OF4i7H#xY90c">
                                                                                <field name="value">2</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="items">
                                                                              <block type="func_array_of_number" id=":~t#nYB(W_PyH^/:{rB0" inline="true">
                                                                                <value name="items">
                                                                                  <block type="expression" id="4xZdnl%^up_s$Ojmtooe">
                                                                                    <field name="value">15</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="from">
                                                                                  <block type="expression" id="lr#[A9jc|z!0=[aB,vv{">
                                                                                    <field name="value">0</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="Dka4eSokvdGQOl~VvJ`O">
                                                                            <field name="name">level</field>
                                                                            <value name="value">
                                                                              <block type="random_many" id="5hSEVLcvo`xYbLJY#3uH">
                                                                                <value name="count">
                                                                                  <block type="expression" id="(`Pkf5!=lA/SNU1ytCVV">
                                                                                    <field name="value">2</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="items">
                                                                                  <block type="func_array_of_number" id="F4QtVP)4vNX2:{]TV:Kj" inline="true">
                                                                                    <value name="items">
                                                                                      <block type="expression" id="~ir4=kjXVp($PJ~WkvZ0">
                                                                                        <field name="value">volume - 6</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="from">
                                                                                      <block type="expression" id="+vfXrV+RQj{H/!V14koP">
                                                                                        <field name="value">6</field>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="variable" id="@1#~5;b}zUbpOL.yNuB2d">
                                                                                <field name="name">it</field>
                                                                                <value name="value">
                                                                                  <block type="func_sort" id="Lj3C@1YTuv]qd~:gH84LM" inline="true">
                                                                                    <value name="value">
                                                                                      <block type="expression" id="p`Lw==q9tUid#9VJs?c9">
                                                                                        <field name="value">it</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="by">
                                                                                      <block type="string_value" id="L=I_OcvJAZsX;x4OX9ri">
                                                                                        <field name="value">asc</field>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="variable" id="m:cFOQ:aM^KtDhg+_GcZ">
                                                                                    <field name="name">level</field>
                                                                                    <value name="value">
                                                                                      <block type="func_sort" id="[j3b$f3S?bbDex46Zeo1" inline="true">
                                                                                        <value name="value">
                                                                                          <block type="expression" id="Y@1a7H~+el?`I;CA/ana|">
                                                                                            <field name="value">level</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="by">
                                                                                          <block type="string_value" id="dkH=X?2E[_#jg!2p(6oF">
                                                                                            <field name="value">asc</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="variable" id="3|)k~%7z61g``M1_vG3@2">
                                                                                        <field name="name">u</field>
                                                                                        <value name="value">
                                                                                          <block type="random_number" id="`=gPE!@2EFk[C2W@1YEtid">
                                                                                            <value name="min">
                                                                                              <block type="expression" id="c?]tbx5vH,+a}-a(MvsU">
                                                                                                <field name="value">0</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="max">
                                                                                              <block type="expression" id="As$@2R^e@1kS==h@15_xp{W">
                                                                                                <field name="value">2</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="variable" id=",N_m9Io:y#@1?OZrS`rn[">
                                                                                            <field name="name">con</field>
                                                                                            <value name="value">
                                                                                              <block type="random_one" id="inUYG+KBEpWg`zp+KH$?">
                                                                                                <value name="items">
                                                                                                  <block type="expression" id="hXiw[_LR(m#eH;O(kB">
                                                                                                    <field name="value">['larger', 'smaller']</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="if_then_else_block" id="@1vpWteOAXlG;J1RGOv;$">
                                                                                                <value name="if">
                                                                                                  <block type="expression" id="J}rtI]QxYZ#W0GwZd8lh">
                                                                                                    <field name="value">con == 'larger'</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <statement name="then">
                                                                                                  <block type="func_add_answer" id="y+SjW9-/]+lpm3(Bzg$L" inline="true">
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="V?.Xs@2oH-a^(ji)u?as{">
                                                                                                        <field name="value">re_max(it[0], it[1])</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <statement name="else">
                                                                                                  <block type="func_add_answer" id="foJsd4MMk+SEBQu)a,B|" inline="true">
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="D:9@2ZA|a#,H|rwgc|E}g">
                                                                                                        <field name="value">re_min(it[0], it[1])</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="variable" id="Dfh|8]KK._uK-$JR:#?5">
                                                                                                    <field name="name">arr</field>
                                                                                                    <value name="value">
                                                                                                      <block type="func_shuffle" id="G{2dI~,z-^Dm@1:{=y}g." inline="true">
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id="HLkQ(6G(s(H,!|iyu@2Mh">
                                                                                                            <field name="value">[0, 1]</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="text_shape" id="rf^d~bzYmfID,7k3^)zT">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_position" id="`v2_S/E`a1{x{64u5nU[">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="fa?m^TUdtslwL^!]7]{D">
                                                                                                                <field name="value">400</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="2eU9(3YYU]q;j4I%=j7e">
                                                                                                                <field name="value">40</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_anchor" id="P{u3F}u}t)=uHk)A65[f">
                                                                                                                <field name="#prop">anchor</field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="LtW2dUm+$n0pbx79)EyM">
                                                                                                                    <field name="value">0.5</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="vYKlBrT$0cJXt4U[R4qx">
                                                                                                                    <field name="value">0.5</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_contents" id="]c]d0DxH!sL0CH.D-Q-7">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="contents">
                                                                                                                      <block type="string_value" id="~%`E#rPG=ezs_gT2CPc-">
                                                                                                                        <field name="value">The below objects were dipped into water.
Which item had a ${con} volume?
                                                                                                                        </field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style" id="kiLz#gl_smzk|OgZBY6O">
                                                                                                                        <field name="base">text</field>
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_text_style_font_size" id="mu=;7jhII)BwnGo~(n]/">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="fontSize">
                                                                                                                              <block type="expression" id="I{774yhqVB-@1YtwZB-;;">
                                                                                                                                <field name="value">36</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_fill" id="h{@2m]GD_pKZ/}xlNn;{I">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="fill">
                                                                                                                                  <block type="string_value" id="[7/HM1|_YpE]e0wVrku]">
                                                                                                                                    <field name="value">black</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_stroke" id="-F=?P@1s@1mIQj$L=w{#l4">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="stroke">
                                                                                                                                      <block type="string_value" id="sr{}{!J#8FK+:IZ-j�">
                                                                                                                                        <field name="value">white</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="D-;.c-Mr/?S^-M,un=^(">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="strokeThickness">
                                                                                                                                          <block type="expression" id=":jFr98P+=cExH5X.!^eD">
                                                                                                                                            <field name="value">2</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="image_shape" id="x2,ukXDX%~.]�R$/u@1">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_position" id=".0xp|mu%;%l^bxNCa4Tm">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="4YbL#j@1b`m^#I$SQk%p$">
                                                                                                                    <field name="value">150</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="P4!}fB8a`{G[};T@2#{9#">
                                                                                                                    <field name="value">215</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_size" id="r7I-wkpJnjFR!]|!g[[l">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="width">
                                                                                                                      <block type="expression" id=":g@2RTnrRtuD~EFp/xwr.">
                                                                                                                        <field name="value">0</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="height">
                                                                                                                      <block type="expression" id="sG,H@1xG?r.1Y.0sK!LSE">
                                                                                                                        <field name="value">0</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_key" id=",w/D5x`yo949s}IL(PSC">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="key">
                                                                                                                          <block type="string_value" id="XTfRZw`eh}6%3)M(=}.c">
                                                                                                                            <field name="value">${volume}_${empty}.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_src" id="i^x�XnxtEh$?{Cn;c!">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="src">
                                                                                                                              <block type="string_value" id=",Dvcvx~x2!ytmP)0IMyp">
                                                                                                                                <field name="value">${image_path}${volume}_${empty}.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="grid_shape" id="Srb.]BII|sc}{i1?.Auv">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_grid_dimension" id="SJ`w+Ak_I@1B=!1lv6{3v">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="rows">
                                                                                                                      <block type="expression" id="J^Rxwjr=7`|OmU^!bddy">
                                                                                                                        <field name="value">1</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="cols">
                                                                                                                      <block type="expression" id="i+J@2bL|Bh?WLe$8Bp@1C{">
                                                                                                                        <field name="value">2</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_position" id="`[jJ8;xgGqoCW~;SoN[^">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="6iRNg2acI]x6fz9eG[++">
                                                                                                                            <field name="value">520</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="jlQ8Uql@2v+:8rb[8E0Q9">
                                                                                                                            <field name="value">215</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_size" id="L{wV9Tr.m.%Y!;@1{.rr,">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="width">
                                                                                                                              <block type="expression" id="s@2c_Qw#W.1}W2@1Bv@1z1m">
                                                                                                                                <field name="value">480</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="height">
                                                                                                                              <block type="expression" id=":pk29EiE!dmPHt2?).zT">
                                                                                                                                <field name="value">235</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_anchor" id="Xl6@2tp~CC~]^?:SQdV@1?">
                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id=":#fuod4iQWwNpsPLQ)0s">
                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="$9Nu9.plu??g[!LO:Hk{">
                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_grid_cell_source" id="@2V#dqgf=$hf,=7[3;}Lp">
                                                                                                                                    <field name="#prop">cell.source</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="Efb~np_L6FU}oW]7b9%~">
                                                                                                                                        <field name="value">arr</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_grid_random" id="[mrNWtU5t=d2_f6[!zyZ">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <field name="random">FALSE</field>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_grid_show_borders" id="V^I;n$_eQWWOjC^._I[S">
                                                                                                                                            <field name="#prop">#showBorders</field>
                                                                                                                                            <field name="value">FALSE</field>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_grid_cell_template" id="owL@2E]B[sug_h]bWTJL@2">
                                                                                                                                                <field name="variable">$cell</field>
                                                                                                                                                <field name="#prop">cell.template</field>
                                                                                                                                                <field name="#callback">$cell</field>
                                                                                                                                                <statement name="body">
                                                                                                                                                  <block type="image_shape" id="k?Jh!B#R:}qEG|k99=d=">
                                                                                                                                                    <statement name="#props">
                                                                                                                                                      <block type="prop_position" id="J/,6r-#vQ@2m^I17Oxmze">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="x">
                                                                                                                                                          <block type="expression" id="FbF/64Rye#r)@2+6D]2uz">
                                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="y">
                                                                                                                                                          <block type="expression" id="q{2akJK=7!nqR9p^TLl%">
                                                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_size" id="`tQ;/Qf-FWC~Su~K#O0|">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="width">
                                                                                                                                                              <block type="expression" id="@2d7GA{)W2ZG@2!N@2%iFYi">
                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="height">
                                                                                                                                                              <block type="expression" id="U77i?mf}85][a?3ScIb9">
                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_image_key" id="v-ZYE9s6s2@2yI1#-6wg:">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="key">
                                                                                                                                                                  <block type="string_value" id="hPiM^zG4:xzOx%{,}WK5">
                                                                                                                                                                    <field name="value">${volume}_${level[$cell.data]}.png</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_image_src" id="T$A?45u6/t9!9WEZLdZj">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="src">
                                                                                                                                                                      <block type="string_value" id="vIg~_e|bd8%oEXoIVk_0">
                                                                                                                                                                        <field name="value">${image_path}${volume}_${level[$cell.data]}.png</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="variable" id=".z$]wEh.~H@1jigLQ:_-0">
                                                                                                                                                        <field name="name">data</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="2Hsmy#z!P|sO@2O^O5Eq5">
                                                                                                                                                            <field name="value">$cell.data</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="grid_shape" id="GwEk.8@2s5^!|jQscEsL.">
                                                                                                                                                            <statement name="#props">
                                                                                                                                                              <block type="prop_grid_dimension" id="aPxK7{`rCngkk9ofcZx`">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="rows">
                                                                                                                                                                  <block type="expression" id="o}E@2v[.Pqw9d@160OX(-V">
                                                                                                                                                                    <field name="value">1</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <value name="cols">
                                                                                                                                                                  <block type="expression" id="45kY9w6JZmArr%)tqY$u">
                                                                                                                                                                    <field name="value">1</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_position" id="dJCR_%I!Z|z#eo}-eS|!">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="x">
                                                                                                                                                                      <block type="expression" id="~HgQI{PGlRLqkBo[q7Fa">
                                                                                                                                                                        <field name="value">$cell.centerX+ 20</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <value name="y">
                                                                                                                                                                      <block type="expression" id="X0!cUUu@1T]P}J$`Rx66+">
                                                                                                                                                                        <field name="value">$cell.centerY+40</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_size" id="|YFmwPjxhmcYThfnE9~/">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="width">
                                                                                                                                                                          <block type="expression" id=":kJ!S({y?[-Ugo}l23lu">
                                                                                                                                                                            <field name="value">120</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <value name="height">
                                                                                                                                                                          <block type="expression" id="tk?.j7V}Tew_[A;).!h#">
                                                                                                                                                                            <field name="value">150</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_anchor" id="DLd=a9Bxes$T28mBp!VY">
                                                                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                                                                            <value name="x">
                                                                                                                                                                              <block type="expression" id="W%x`Wf#J{lVFsZdaUZrt">
                                                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="y">
                                                                                                                                                                              <block type="expression" id="%~;U@2Uy;BS`:f8G2E[?s">
                                                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_grid_cell_source" id="]2!K!L:Q:T-]7wKg]YMv">
                                                                                                                                                                                <field name="#prop">cell.source</field>
                                                                                                                                                                                <value name="value">
                                                                                                                                                                                  <block type="expression" id="hoZb+C]Z.5PU:-~G,@1#k">
                                                                                                                                                                                    <field name="value">1</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_grid_random" id="}|P7nJc$|r?b{W%P}k+a">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <field name="random">FALSE</field>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_grid_show_borders" id=":J@1+@2EkYBlZ+EQ2tP9Zi">
                                                                                                                                                                                        <field name="#prop">#showBorders</field>
                                                                                                                                                                                        <field name="value">FALSE</field>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_grid_cell_template" id="ftsnId6]^cXH5I!Pcdl?">
                                                                                                                                                                                            <field name="variable">$cell</field>
                                                                                                                                                                                            <field name="#prop">cell.template</field>
                                                                                                                                                                                            <field name="#callback">$cell</field>
                                                                                                                                                                                            <statement name="body">
                                                                                                                                                                                              <block type="image_shape" id="R`]M~`o!;;Zr)Ssx7j24">
                                                                                                                                                                                                <statement name="#props">
                                                                                                                                                                                                  <block type="prop_position" id="PVb{?K%HR@202SRBIpG!-">
                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                    <value name="x">
                                                                                                                                                                                                      <block type="expression" id="x~B%rA0-m:}hYK7Vo{0N">
                                                                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <value name="y">
                                                                                                                                                                                                      <block type="expression" id="vFh$OPJ/SI6]+/ZpgPD1">
                                                                                                                                                                                                        <field name="value">$cell.bottom - 10</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="prop_anchor" id="Dl8Y;fbcVU,(#Xn~g:M@1">
                                                                                                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                                                                                                        <value name="x">
                                                                                                                                                                                                          <block type="expression" id="].{i}G%{TKF2~t[JTA$]">
                                                                                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <value name="y">
                                                                                                                                                                                                          <block type="expression" id="Y%]8fH2qK@2##~m,TDG]l">
                                                                                                                                                                                                            <field name="value">1</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="prop_max_size" id="Bv-,Ce;a^F}=X_9-n)1-">
                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                            <value name="maxWidth">
                                                                                                                                                                                                              <block type="expression" id="okwIq5k^8eql;rb-(VJ8">
                                                                                                                                                                                                                <field name="value">$cell.width</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                            <value name="maxHeight">
                                                                                                                                                                                                              <block type="expression" id="iPjqRZXN8j3:Y!NxcgEE">
                                                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                            <next>
                                                                                                                                                                                                              <block type="prop_image_key" id="Zo6V`jE75unk^|gf$X;@2">
                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                <value name="key">
                                                                                                                                                                                                                  <block type="string_value" id="^O=Rb:[a$5@1S/ea|B80a">
                                                                                                                                                                                                                    <field name="value">${it[data]}.png</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                  <block type="prop_image_src" id="R~!ET{eL%C;X!@2NU3#K~">
                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                    <value name="src">
                                                                                                                                                                                                                      <block type="string_value" id=",0LaCGq3mG;i3Ad)hkG%">
                                                                                                                                                                                                                        <field name="value">${image_path}${it[data]}.png</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </next>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </next>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </statement>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </statement>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="grid_shape" id=")~vrwZO8QE=}yGqD#,D9">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_grid_dimension" id="bPQ~8a-JS7nkf~R578):">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="rows">
                                                                                                                          <block type="expression" id="`0$jP~Pkit8YEsG`n_vX">
                                                                                                                            <field name="value">1</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="cols">
                                                                                                                          <block type="expression" id="Ew|F7I8Ff?H}zCF`(teC">
                                                                                                                            <field name="value">2</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_position" id="ymi32rV6VK,Dbm%5u7W)">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="6^(Us(sVSw#KBDmSP|!7">
                                                                                                                                <field name="value">520</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id=".g,XA@1dN8ea#b3w4P(~f">
                                                                                                                                <field name="value">400</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_size" id="HjFTfMz)`jCM4)v!uw~a">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="width">
                                                                                                                                  <block type="expression" id="/.x^82NP?b)A-iR-j]pG">
                                                                                                                                    <field name="value">480</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="height">
                                                                                                                                  <block type="expression" id="n9#`S]=xNS2CVAVkM|[;">
                                                                                                                                    <field name="value">50</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_anchor" id="Vhf4c-1JK)lSwFu2i4Ck">
                                                                                                                                    <field name="#prop">anchor</field>
                                                                                                                                    <value name="x">
                                                                                                                                      <block type="expression" id="?FR2tD#=B64@2.nsHarnx">
                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="y">
                                                                                                                                      <block type="expression" id="lG$;1wf@19E3aX[uaOk07">
                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_grid_cell_source" id="4I9q|$]?(?xs55$RSq.L">
                                                                                                                                        <field name="#prop">cell.source</field>
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="expression" id="dK+{[!J$a#M,6GthkVDz">
                                                                                                                                            <field name="value">arr</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_grid_random" id="Yt-M_lm{YBK0X%m#5qQ4">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <field name="random">FALSE</field>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_grid_show_borders" id="}rn^-`~i9@2-;~)/e9:@29">
                                                                                                                                                <field name="#prop">#showBorders</field>
                                                                                                                                                <field name="value">FALSE</field>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_grid_cell_template" id="ZSKaK%fu{rnLLx!xT^VS">
                                                                                                                                                    <field name="variable">$cell</field>
                                                                                                                                                    <field name="#prop">cell.template</field>
                                                                                                                                                    <field name="#callback">$cell</field>
                                                                                                                                                    <statement name="body">
                                                                                                                                                      <block type="choice_custom_shape" id="cm$eP2orQP;%.?m3%@13%">
                                                                                                                                                        <field name="action">click-one</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="YF{mXvRQ+q`qHTZI__CY">
                                                                                                                                                            <field name="value">it[$cell.data]</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <statement name="template">
                                                                                                                                                          <block type="image_shape" id="[wqYZybwHU[uyf[T)BT{">
                                                                                                                                                            <statement name="#props">
                                                                                                                                                              <block type="prop_position" id="T5SKVW:2Jz.6IE~M!{2@1">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="x">
                                                                                                                                                                  <block type="expression" id="kYt2^8UyOKmsPc$Sy53v">
                                                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <value name="y">
                                                                                                                                                                  <block type="expression" id="c+dLNB[i_Kv[I5G/W-o4">
                                                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_size" id="Tq6W[$z-%~ykIDo~9q-O">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="width">
                                                                                                                                                                      <block type="expression" id="gRHcc,3F[[PSO:=)%.?{">
                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <value name="height">
                                                                                                                                                                      <block type="expression" id="ejF9)RqSjH2X?Z^|+fe8">
                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_image_key" id="-H_On=ZjIBP_Iu%O-`KJ">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="key">
                                                                                                                                                                          <block type="string_value" id="HN%gChSfjo.1Lw{.cmjx">
                                                                                                                                                                            <field name="value">box.png</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_image_src" id="gtpGWEW_LM~NtRA78u+q">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="src">
                                                                                                                                                                              <block type="string_value" id="+sMHLN;Y_20M=i_=^R)?">
                                                                                                                                                                                <field name="value">${image_path}box.png</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="text_shape" id="ql/srwOgyXMVIA.BvLzx">
                                                                                                                                                                <statement name="#props">
                                                                                                                                                                  <block type="prop_position" id="qbI4CO[3lR@1MAQ`XWbkD">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="x">
                                                                                                                                                                      <block type="expression" id="Bq1tGepE?;Gw:;^Lyj0d">
                                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <value name="y">
                                                                                                                                                                      <block type="expression" id="q(D1w@1YS!5;OpLhAmC@2Z">
                                                                                                                                                                        <field name="value">$cell.centerY+3</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_text_contents" id="SN]@2@2T^eHpdP,J$:tGit">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="contents">
                                                                                                                                                                          <block type="string_value" id="2Jk+T|@1x{Fj6P70FX.S+">
                                                                                                                                                                            <field name="value">${jsUcfirst(items[it[$cell.data]])}</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_text_style" id="a9Q;Ty%/l[OvG(,P=n3.">
                                                                                                                                                                            <field name="base">text</field>
                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                              <block type="prop_text_style_font_size" id="@1Y-6JY9}zqnbc,1y;Ql/">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="fontSize">
                                                                                                                                                                                  <block type="expression" id="`C[)|LNVD8!j:Af5gV)G">
                                                                                                                                                                                    <field name="value">36</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_text_style_fill" id="Balq(,8+4h.bw+S$!#v4">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="fill">
                                                                                                                                                                                      <block type="string_value" id="~~GH;RJoW8e(c.sfp/TC">
                                                                                                                                                                                        <field name="value">black</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_text_style_stroke" id="y,_~yB7IS9!}$ol@2G9#,">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="stroke">
                                                                                                                                                                                          <block type="string_value" id="6+LVr:)G!:e%YL#]h!,K">
                                                                                                                                                                                            <field name="value">white</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_text_style_stroke_thickness" id="-EC)jbx...fMRyWAze7.">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <value name="strokeThickness">
                                                                                                                                                                                              <block type="expression" id="d6,abUuU0I8Ap/`OD;eb">
                                                                                                                                                                                                <field name="value">2</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </statement>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <next>
                                                                  <block type="if_then_block" id="#oRwS@1S|_XAbL-ca_Ojf">
                                                                    <value name="if">
                                                                      <block type="expression" id="cr(H-qxiaj(kuUWs;tU!">
                                                                        <field name="value">type == 1</field>
                                                                      </block>
                                                                    </value>
                                                                    <statement name="then">
                                                                      <block type="partial_explanation" id="f!@2W+jp=CdP,?UX}4(az" inline="true">
                                                                        <value name="value">
                                                                          <block type="string_value" id="1gq[XF7odU+:WNA.i[Fw">
                                                                            <field name="value">&lt;u&gt;Step 1: From the question, look for the item to focus on.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;span style='background: rgb(250, 250, 250, 0.3)'&gt;
What is the volume of the &lt;b&gt;${items[it]}&lt;/b&gt;?
&lt;/br&gt;&lt;/r&gt;
The item to focus on is the &lt;b&gt;${items[it]}&lt;/b&gt;.&lt;/span&gt;
                                                                            </field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="end_partial_explanation" id="?S@2]8RZ}1+z|k2rc%;X5">
                                                                            <next>
                                                                              <block type="partial_explanation" id="?x@2#;6-ay]}@2si_%5W#+" inline="true">
                                                                                <value name="value">
                                                                                  <block type="string_value" id="b^5xoocNb],ZBShU8fKW">
                                                                                    <field name="value">&lt;u&gt;Step 2: Now let's calculate the volume of the ${items[it]} by subtracting the original water level from the water level with the ${items[it]}.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;
&lt;span style='background: rgb(250, 250, 250, 0.3)'&gt;
${level} - ${empty} = &lt;b&gt;${level - empty}&lt;/b&gt;&lt;/br&gt;&lt;/br&gt;

The &lt;b&gt;${items[it]}&lt;/b&gt; has a volume of &lt;b&gt;${level - empty}&lt;/b&gt; ${unit[u]}${level - empty &gt; 1 ? 's': ''}.&lt;/span&gt;
                                                                                    </field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="end_partial_explanation" id="tuo@2Act?Y=3V~im`8GIs"></block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="if_then_block" id="r{w@1l${RAV@1GSvEkm7Z[">
                                                                        <value name="if">
                                                                          <block type="expression" id="}fSHXf4ExzGNOY@2|s:c[">
                                                                            <field name="value">type == 2</field>
                                                                          </block>
                                                                        </value>
                                                                        <statement name="then">
                                                                          <block type="partial_explanation" id="Y[GZ?Z,Bg`Ic!!fV$bTI" inline="true">
                                                                            <value name="value">
                                                                              <block type="string_value" id="R%vKK.}Z@2PzYKGk}1EW0">
                                                                                <field name="value">&lt;u&gt;Step 1: From the question, look for the item to focus on.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;span style='background: rgb(250, 250, 250, 0.3)'&gt;
Which item had a &lt;b&gt;${con}&lt;/b&gt; volume?
&lt;/br&gt;&lt;/r&gt;
The condition to focus on is the item with a &lt;b&gt;${con}&lt;/b&gt; volume.&lt;/span&gt;
                                                                                </field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="end_partial_explanation" id="X^Bv~z|[vI4u_Wbx.Vgf">
                                                                                <next>
                                                                                  <block type="partial_explanation" id="e=w@2kREI6%o`_kvuJz#0" inline="true">
                                                                                    <value name="value">
                                                                                      <block type="string_value" id="4$`::Gs~Ql6L8}YS@2#,A">
                                                                                        <field name="value">&lt;u&gt;Step 2: Now let's calculate the volume of the items by subtracting the original water level from the water level with the item.
&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;
&lt;span style='background: rgb(250, 250, 250, 0.3)'&gt;
  
${jsUcfirst(items[it[0]])}&lt;/br&gt;
${level[0]} - ${empty} = &lt;b&gt;${level[0] - empty}&lt;/b&gt;&lt;/br&gt;&lt;/br&gt;
  
${jsUcfirst(items[it[1]])}&lt;/br&gt;
${level[1]} - ${empty} = &lt;b&gt;${level[1] - empty}&lt;/b&gt;&lt;/span&gt;
                                                                                        </field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="end_partial_explanation" id="Q..eN[NY|F|KS#qd[vya">
                                                                                        <next>
                                                                                          <block type="partial_explanation" id="g=tn,;Gc35mHcjLYPxI%" inline="true">
                                                                                            <value name="value">
                                                                                              <block type="string_value" id="=@2y4n8GU4Us,`I6SX}mr">
                                                                                                <field name="value">&lt;u&gt;Step 3: Finally, choose the item with a ${con} volume.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;
&lt;span style='background: rgb(250, 250, 250, 0.3)'&gt;
The &lt;b&gt;${items[con == 'larger' ? re_max(it[0], it[1]) : re_min(it[0], it[1])]}&lt;/b&gt; has a &lt;b&gt;${con}&lt;/b&gt; volume.&lt;/span&gt;
                                                                                                </field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
</xml>
END_XML]] */