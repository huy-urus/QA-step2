
module.exports = [
  {
    "#type": "question",
    "name": "Y2.NA.WN.CB100NC.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y2.NA.WN.CB100NC.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "Fill in the missing numbers!"
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "36"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "start",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "0"
          },
          "max": {
            "#type": "expression",
            "value": "9"
          }
        }
      },
      {
        "#type": "variable",
        "name": "start",
        "value": {
          "#type": "expression",
          "value": "start*100 + 1"
        }
      },
      {
        "#type": "variable",
        "name": "array",
        "value": {
          "#type": "func",
          "name": "arrayOfNumber",
          "args": [
            {
              "#type": "expression",
              "value": "100"
            },
            {
              "#type": "expression",
              "value": "start"
            }
          ]
        }
      },
      {
        "#type": "variable",
        "name": "hide",
        "value": {
          "#type": "random_many",
          "count": {
            "#type": "expression",
            "value": "5"
          },
          "items": {
            "#type": "func",
            "name": "arrayOfNumber",
            "args": [
              {
                "#type": "expression",
                "value": "100"
              },
              {
                "#type": "expression",
                "value": "0"
              }
            ]
          }
        }
      },
      {
        "#type": "variable",
        "name": "color",
        "value": {
          "#type": "expression",
          "value": "[0xff9999, 0xffbb99, 0xcc99ff, 0xb3ccff, 0xecb3ff]"
        }
      },
      {
        "#type": "variable",
        "name": "co",
        "value": {
          "#type": "func",
          "name": "pullOne",
          "args": [
            {
              "#type": "expression",
              "value": "[0, 1, 2, 3, 4]"
            }
          ]
        }
      },
      {
        "#type": "statement",
        "value": "function check(x, A) {\n    for(var m=0; m<5; m++){\n        if(x == A[m]){\n            return 1;\n        }\n    }\n    return 0;\n}\n\nfunction list_col(A){\n  var K = [];\n  for(var i = 0; i< 5; i++){\n    K.push(A[i] % 10);\n  }\n  return K;\n}\n\nfunction check_z(x, A, z) {\n  for(var m=0; m<z; m++){\n      if(x == A[m] && m != z){\n          return 1;\n      }\n  }\n  return 0;\n}\n\nfunction different(A){\n  var count = 0;\n  for(var i = 0; i<5; i++){\n    if(check_z(A[i], A, i) == 0){\n      count++;\n    }\n  }\n  return count;\n}\n\nfunction ones_hide(A){\n  var X = [];\n  for(var i = 0; i < 5; i++){\n    X[i] = A[i] % 10;\n  }\n  return X;\n}\n\none_hide = ones_hide(hide);"
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "1"
            },
            "cols": {
              "#type": "expression",
              "value": "1"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "220"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "different(one_hide)*100 + (10 - different(one_hide))*50"
            },
            "height": {
              "#type": "expression",
              "value": "350"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "expression",
              "value": "1"
            }
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": true
          },
          {
            "#type": "prop_grid_cell_template",
            "variable": "$cell",
            "#prop": "cell.template",
            "#callback": "$cell",
            "body": [
              {
                "#type": "variable",
                "name": "ox",
                "value": {
                  "#type": "expression",
                  "value": "$cell.centerX - $cell.width/2"
                }
              },
              {
                "#type": "variable",
                "name": "oy",
                "value": {
                  "#type": "expression",
                  "value": "$cell.centerY - $cell.height/2"
                }
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "i < 100"
                },
                "do": [
                  {
                    "#type": "grid_shape",
                    "#props": [
                      {
                        "#type": "prop_grid_dimension",
                        "#prop": "",
                        "rows": {
                          "#type": "expression",
                          "value": "1"
                        },
                        "cols": {
                          "#type": "expression",
                          "value": "1"
                        }
                      },
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "ox"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "oy"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "check(i % 10, list_col(hide)) == 1 ? 100: 50"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "35"
                        }
                      },
                      {
                        "#type": "prop_anchor",
                        "#prop": "anchor",
                        "x": {
                          "#type": "expression",
                          "value": "0"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "0"
                        }
                      },
                      {
                        "#type": "prop_grid_cell_source",
                        "#prop": "cell.source",
                        "value": {
                          "#type": "expression",
                          "value": "1"
                        }
                      },
                      {
                        "#type": "prop_stroke",
                        "#prop": "stroke",
                        "width": {
                          "#type": "expression",
                          "value": "2"
                        },
                        "color": {
                          "#type": "expression",
                          "value": "0x000000"
                        }
                      },
                      {
                        "#type": "prop_fill",
                        "#prop": "fill",
                        "color": {
                          "#type": "expression",
                          "value": "color[co]"
                        }
                      },
                      {
                        "#type": "prop_grid_random",
                        "#prop": "",
                        "random": false
                      },
                      {
                        "#type": "prop_grid_show_borders",
                        "#prop": "#showBorders",
                        "value": true
                      },
                      {
                        "#type": "prop_grid_cell_template",
                        "variable": "$cell",
                        "#prop": "cell.template",
                        "#callback": "$cell",
                        "body": [
                          {
                            "#type": "if_then_else_block",
                            "if": {
                              "#type": "expression",
                              "value": "check(i, hide) == 1"
                            },
                            "then": [
                              {
                                "#type": "image_shape",
                                "#props": [
                                  {
                                    "#type": "prop_position",
                                    "#prop": "",
                                    "x": {
                                      "#type": "expression",
                                      "value": "$cell.centerX - $cell.width/2"
                                    },
                                    "y": {
                                      "#type": "expression",
                                      "value": "$cell.centerY"
                                    }
                                  },
                                  {
                                    "#type": "prop_size",
                                    "#prop": "",
                                    "width": {
                                      "#type": "expression",
                                      "value": "50"
                                    },
                                    "height": {
                                      "#type": "expression",
                                      "value": "30"
                                    }
                                  },
                                  {
                                    "#type": "prop_anchor",
                                    "#prop": "anchor",
                                    "x": {
                                      "#type": "expression",
                                      "value": "0"
                                    },
                                    "y": {
                                      "#type": "expression",
                                      "value": "0.5"
                                    }
                                  },
                                  {
                                    "#type": "prop_image_key",
                                    "#prop": "",
                                    "key": "shape.png"
                                  },
                                  {
                                    "#type": "prop_image_src",
                                    "#prop": "",
                                    "src": "${image_path}shape.png"
                                  }
                                ]
                              },
                              {
                                "#type": "choice_input_shape",
                                "#props": [
                                  {
                                    "#type": "prop_value",
                                    "#prop": "",
                                    "value": {
                                      "#type": "expression",
                                      "value": "array[i]"
                                    }
                                  },
                                  {
                                    "#type": "prop_position",
                                    "#prop": "",
                                    "x": {
                                      "#type": "expression",
                                      "value": "$cell.centerX - $cell.width/4"
                                    },
                                    "y": {
                                      "#type": "expression",
                                      "value": "$cell.centerY"
                                    }
                                  },
                                  {
                                    "#type": "prop_size",
                                    "#prop": "",
                                    "width": {
                                      "#type": "expression",
                                      "value": "40"
                                    },
                                    "height": {
                                      "#type": "expression",
                                      "value": "25"
                                    }
                                  },
                                  {
                                    "#type": "prop_input_keyboard",
                                    "#prop": "",
                                    "keyboard": "numbers1"
                                  },
                                  {
                                    "#type": "prop_input_max_length",
                                    "#prop": "",
                                    "maxLength": {
                                      "#type": "expression",
                                      "value": "array[i].toString().length"
                                    }
                                  },
                                  {
                                    "#type": "prop_input_result_position",
                                    "#prop": "",
                                    "resultPosition": "right"
                                  },
                                  {
                                    "#type": "prop_tab_order",
                                    "#prop": "",
                                    "tabOrder": {
                                      "#type": "expression",
                                      "value": "0"
                                    }
                                  },
                                  {
                                    "#type": "prop_stroke",
                                    "#prop": "stroke"
                                  },
                                  {
                                    "#type": "prop_fill",
                                    "#prop": "fill"
                                  },
                                  {
                                    "#prop": "",
                                    "style": {
                                      "#type": "json",
                                      "base": "text",
                                      "#props": [
                                        {
                                          "#type": "prop_text_style_font_size",
                                          "#prop": "",
                                          "fontSize": {
                                            "#type": "expression",
                                            "value": "28"
                                          }
                                        },
                                        {
                                          "#type": "prop_text_style_fill",
                                          "#prop": "",
                                          "fill": "black"
                                        },
                                        {
                                          "#type": "prop_text_style_stroke",
                                          "#prop": "",
                                          "stroke": "white"
                                        },
                                        {
                                          "#type": "prop_text_style_stroke_thickness",
                                          "#prop": "",
                                          "strokeThickness": {
                                            "#type": "expression",
                                            "value": "2"
                                          }
                                        }
                                      ]
                                    }
                                  }
                                ],
                                "#init": "algorithmic_input"
                              }
                            ],
                            "else": [
                              {
                                "#type": "list_shape",
                                "#props": [
                                  {
                                    "#type": "prop_list_direction",
                                    "#prop": "",
                                    "dir": "horizontal"
                                  },
                                  {
                                    "#type": "prop_position",
                                    "#prop": "",
                                    "x": {
                                      "#type": "expression",
                                      "value": "$cell.centerX"
                                    },
                                    "y": {
                                      "#type": "expression",
                                      "value": "$cell.centerY"
                                    }
                                  },
                                  {
                                    "#type": "prop_anchor",
                                    "#prop": "anchor",
                                    "x": {
                                      "#type": "expression",
                                      "value": "0.5"
                                    },
                                    "y": {
                                      "#type": "expression",
                                      "value": "0.5"
                                    }
                                  },
                                  {
                                    "#type": "prop_spacing",
                                    "#prop": "",
                                    "spacing": {
                                      "#type": "expression",
                                      "value": "1"
                                    }
                                  }
                                ],
                                "items": [
                                  {
                                    "#type": "json",
                                    "#props": [
                                      {
                                        "#type": "prop_list_align",
                                        "#prop": "",
                                        "align": "middle"
                                      },
                                      {
                                        "#type": "prop_list_item_source",
                                        "#prop": "",
                                        "source": {
                                          "#type": "func",
                                          "name": "charactersOf",
                                          "args": [
                                            {
                                              "#type": "expression",
                                              "value": "array[i]"
                                            }
                                          ]
                                        }
                                      }
                                    ],
                                    "template": {
                                      "#callback": "$item",
                                      "variable": "$item",
                                      "body": [
                                        {
                                          "#type": "if_then_else_block",
                                          "if": {
                                            "#type": "expression",
                                            "value": "array[i] > 999"
                                          },
                                          "then": [
                                            {
                                              "#type": "image_shape",
                                              "#props": [
                                                {
                                                  "#type": "prop_image_key",
                                                  "#prop": "",
                                                  "key": "${$item.data}.png"
                                                },
                                                {
                                                  "#type": "prop_image_src",
                                                  "#prop": "",
                                                  "src": "${image_path}${$item.data}.png"
                                                },
                                                {
                                                  "#type": "prop_scale",
                                                  "#prop": "",
                                                  "scale": {
                                                    "#type": "expression",
                                                    "value": "0.4"
                                                  }
                                                }
                                              ]
                                            }
                                          ],
                                          "else": [
                                            {
                                              "#type": "image_shape",
                                              "#props": [
                                                {
                                                  "#type": "prop_image_key",
                                                  "#prop": "",
                                                  "key": "${$item.data}.png"
                                                },
                                                {
                                                  "#type": "prop_image_src",
                                                  "#prop": "",
                                                  "src": "${image_path}${$item.data}.png"
                                                },
                                                {
                                                  "#type": "prop_scale",
                                                  "#prop": "",
                                                  "scale": {
                                                    "#type": "expression",
                                                    "value": "0.5"
                                                  }
                                                }
                                              ]
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  }
                                ]
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  },
                  {
                    "#type": "if_then_else_block",
                    "if": {
                      "#type": "expression",
                      "value": "check(i % 10, list_col(hide)) == 1"
                    },
                    "then": [
                      {
                        "#type": "variable",
                        "name": "ox",
                        "value": {
                          "#type": "expression",
                          "value": "ox + 100"
                        }
                      }
                    ],
                    "else": [
                      {
                        "#type": "variable",
                        "name": "ox",
                        "value": {
                          "#type": "expression",
                          "value": "ox + 50"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "if_then_block",
                    "if": {
                      "#type": "expression",
                      "value": "i % 10 == 9"
                    },
                    "then": [
                      {
                        "#type": "variable",
                        "name": "ox",
                        "value": {
                          "#type": "expression",
                          "value": "$cell.centerX - $cell.width/2"
                        }
                      },
                      {
                        "#type": "variable",
                        "name": "oy",
                        "value": {
                          "#type": "expression",
                          "value": "oy + 35"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "i",
                    "value": {
                      "#type": "expression",
                      "value": "i+1"
                    }
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "func",
        "name": "configureKeyboard",
        "args": [
          "numbers1",
          {
            "#type": "expression",
            "value": "0.5"
          },
          {
            "#type": "expression",
            "value": "1"
          },
          {
            "#type": "expression",
            "value": "400"
          },
          {
            "#type": "expression",
            "value": "450"
          },
          {
            "#type": "expression",
            "value": "0.8"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "Variable",
        "value": {
          "#type": "custom_image_list",
          "link": "${image_path}",
          "images": "0|1|2|3|4|5|6|7|8|9"
        }
      },
      {
        "#type": "variable",
        "name": "color",
        "value": {
          "#type": "expression",
          "value": "['#ff9999', '#ffbb99', '#cc99ff', '#b3ccff', '#ecb3ff']"
        }
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<center><u>Count using the number chart below.</u></br></br>\n\n<style>\n  td{border: 2px solid black; text-align: center; padding: 10px; width: 90px}\nimg{}\n  </style>\n\n    <table>\n    <tr>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < 100"
        },
        "do": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "check(i, hide) == 1"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<td style='background: ${color[co]}'><b>${array[i]}</b></td>"
                ]
              }
            ],
            "else": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<td style='background: white'>${array[i]}</td>"
                ]
              }
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "i % 10 == 9 && i  != 99"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</tr><tr>"
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</tr></table></center>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="0" y="0">
    <field name="name">Y2.NA.WN.CB100NC.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y2.NA.WN.CB100NC.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                    <statement name="#props">
                                      <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                        <field name="#prop"></field>
                                        <value name="x">
                                          <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                            <field name="value">400</field>
                                          </block>
                                        </value>
                                        <value name="y">
                                          <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                            <field name="value">0</field>
                                          </block>
                                        </value>
                                        <next>
                                          <block type="prop_anchor" id="O.v@2mKn@1K#+tF;|}C}xI">
                                            <field name="#prop">anchor</field>
                                            <value name="x">
                                              <block type="expression" id="+3%GU[X3l%%X`Jv1QL7@2">
                                                <field name="value">0.5</field>
                                              </block>
                                            </value>
                                            <value name="y">
                                              <block type="expression" id="Kzp23HjHF7k]A-y1BJPm">
                                                <field name="value">0</field>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                <field name="#prop"></field>
                                                <value name="contents">
                                                  <block type="string_value" id="#6btV9p,PGV}wMx.U�">
                                                    <field name="value">Fill in the missing numbers!</field>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                    <field name="base">text</field>
                                                    <statement name="#props">
                                                      <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                        <field name="#prop"></field>
                                                        <value name="fontSize">
                                                          <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                            <field name="value">36</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                            <field name="#prop"></field>
                                                            <value name="fill">
                                                              <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                <field name="value">black</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                <field name="#prop"></field>
                                                                <value name="stroke">
                                                                  <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                    <field name="value">white</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                    <field name="#prop"></field>
                                                                    <value name="strokeThickness">
                                                                      <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                        <field name="value">2</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </statement>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </statement>
                                    <next>
                                      <block type="variable" id="=Rn+C@2TYD@12~zue5Cd,v">
                                        <field name="name">start</field>
                                        <value name="value">
                                          <block type="random_number" id="R{lcLxL(MMN$bj?f3#FJ">
                                            <value name="min">
                                              <block type="expression" id="[@2@2sH#s3S^9i1^wTwtmq">
                                                <field name="value">0</field>
                                              </block>
                                            </value>
                                            <value name="max">
                                              <block type="expression" id="2^Lz-VjU=N|Xmt)X1.L]">
                                                <field name="value">9</field>
                                              </block>
                                            </value>
                                          </block>
                                        </value>
                                        <next>
                                          <block type="variable" id="e]{Ug,w,Vik|;wy;HQ@1w">
                                            <field name="name">start</field>
                                            <value name="value">
                                              <block type="expression" id="dDN:{p.Yx$y`b!_I3~Yc">
                                                <field name="value">start@2100 + 1</field>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id="@2n5s;L5d[lewZrjLu3.:">
                                                <field name="name">array</field>
                                                <value name="value">
                                                  <block type="func_array_of_number" id="G`l:nZ@2wqPdy!I3qqW#@1" inline="true">
                                                    <value name="items">
                                                      <block type="expression" id="iSPa/fo8TRHcTiAMtq-p">
                                                        <field name="value">100</field>
                                                      </block>
                                                    </value>
                                                    <value name="from">
                                                      <block type="expression" id="qq5BwrWo+tP$|mBt%w4?">
                                                        <field name="value">start</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="r+6SI6.grVYHp[Mq/X:9">
                                                    <field name="name">hide</field>
                                                    <value name="value">
                                                      <block type="random_many" id="dsePB?KJP}njElRKcD!U">
                                                        <value name="count">
                                                          <block type="expression" id=")+DN8@2%N6~J@1T4sSGpn+">
                                                            <field name="value">5</field>
                                                          </block>
                                                        </value>
                                                        <value name="items">
                                                          <block type="func_array_of_number" id="[i%DXl1P::ThIh56yi]V" inline="true">
                                                            <value name="items">
                                                              <block type="expression" id="YK/]K6FHZBIPi2cpB7qB">
                                                                <field name="value">100</field>
                                                              </block>
                                                            </value>
                                                            <value name="from">
                                                              <block type="expression" id="l?9Cb(qhgdumNqb5@2ro_">
                                                                <field name="value">0</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id="SF[T+jyzf42`VO],{7,M">
                                                        <field name="name">color</field>
                                                        <value name="value">
                                                          <block type="expression" id="Hu?!J@1HtxY?W-0T|7h5~">
                                                            <field name="value">[0xff9999, 0xffbb99, 0xcc99ff, 0xb3ccff, 0xecb3ff]</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="[u4B+`=+7a{ILAKDC{@1Y">
                                                            <field name="name">co</field>
                                                            <value name="value">
                                                              <block type="func_pull_one" id="_p,GU}BaDDUAhgFNN6-E" inline="true">
                                                                <value name="value">
                                                                  <block type="expression" id="LK:Sgk|s[F1pjZp)Gsi=">
                                                                    <field name="value">[0, 1, 2, 3, 4]</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="statement" id="GgwP]kpUoM_)sSCU|9`F">
                                                                <field name="value">function check(x, A) {
    for(var m=0; m&lt;5; m++){
        if(x == A[m]){
            return 1;
        }
    }
    return 0;
}

function list_col(A){
  var K = [];
  for(var i = 0; i&lt; 5; i++){
    K.push(A[i] % 10);
  }
  return K;
}

function check_z(x, A, z) {
  for(var m=0; m&lt;z; m++){
      if(x == A[m] &amp;&amp; m != z){
          return 1;
      }
  }
  return 0;
}

function different(A){
  var count = 0;
  for(var i = 0; i&lt;5; i++){
    if(check_z(A[i], A, i) == 0){
      count++;
    }
  }
  return count;
}

function ones_hide(A){
  var X = [];
  for(var i = 0; i &lt; 5; i++){
    X[i] = A[i] % 10;
  }
  return X;
}

one_hide = ones_hide(hide);
                                                                </field>
                                                                <next>
                                                                  <block type="grid_shape" id="UsPU#5oY#6zcmqdZ=R7)">
                                                                    <statement name="#props">
                                                                      <block type="prop_grid_dimension" id="Z!t%Ysa@2=q4Z8_X^RR3r">
                                                                        <field name="#prop"></field>
                                                                        <value name="rows">
                                                                          <block type="expression" id="CUXQ}Wy~r$%vd_$$R}[!">
                                                                            <field name="value">1</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="cols">
                                                                          <block type="expression" id="/Sf@2|/Cbij;!tWhPfm(:">
                                                                            <field name="value">1</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_position" id="%};=aax3Z9xC-zU$TMA@2">
                                                                            <field name="#prop"></field>
                                                                            <value name="x">
                                                                              <block type="expression" id="v{Mc(~jM6=+Et?DyI~qR">
                                                                                <field name="value">400</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="y">
                                                                              <block type="expression" id="47}h_)@2jV7V]iH=esG:p">
                                                                                <field name="value">220</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_size" id="3jGj#m-;2#Zs#id%?7MS">
                                                                                <field name="#prop"></field>
                                                                                <value name="width">
                                                                                  <block type="expression" id="w@1AVX|^6]C?p:9[%|Y1W">
                                                                                    <field name="value">different(one_hide)@2100 + (10 - different(one_hide))@250</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="height">
                                                                                  <block type="expression" id="m-?LZZ(rH.U|ng,`VoCt">
                                                                                    <field name="value">350</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_anchor" id="QImfq!{te5oApCRq0U1(">
                                                                                    <field name="#prop">anchor</field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="u;Qr=u|7{8cE8J%G:KIj">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="n.iZ@2l.1wPoLK954SS{-">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_grid_cell_source" id="AD$8Rs%,p}]+bK~!oU!:">
                                                                                        <field name="#prop">cell.source</field>
                                                                                        <value name="value">
                                                                                          <block type="expression" id="lIXa_/f)ExII)Va@2Ih[t">
                                                                                            <field name="value">1</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_grid_random" id="Ssbx12`:AzOFQPwCFZ/.">
                                                                                            <field name="#prop"></field>
                                                                                            <field name="random">FALSE</field>
                                                                                            <next>
                                                                                              <block type="prop_grid_show_borders" id=":OU-v@23?BZT4B73{NRSZ">
                                                                                                <field name="#prop">#showBorders</field>
                                                                                                <field name="value">TRUE</field>
                                                                                                <next>
                                                                                                  <block type="prop_grid_cell_template" id="~5f@2A7k)$q1ozg{(U;JS">
                                                                                                    <field name="variable">$cell</field>
                                                                                                    <field name="#prop">cell.template</field>
                                                                                                    <field name="#callback">$cell</field>
                                                                                                    <statement name="body">
                                                                                                      <block type="variable" id="?KaJ^iT^pDVT0~VB=3{L">
                                                                                                        <field name="name">ox</field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id="/0^ufOizWmz)=F.0FS4x">
                                                                                                            <field name="value">$cell.centerX - $cell.width/2</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="variable" id="g.l$i#ao^WFanLNGJ/vd">
                                                                                                            <field name="name">oy</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="XrIE.^vBNaQVqZ.Xd)VQ">
                                                                                                                <field name="value">$cell.centerY - $cell.height/2</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="variable" id=":V0F^.9dwV9+RoqBMTC@1">
                                                                                                                <field name="name">i</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="@2$.[Y@23.n_d-^uk;XREJ">
                                                                                                                    <field name="value">0</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="while_do_block" id=",Q2;R(dI!B}iI[8=;%BG">
                                                                                                                    <value name="while">
                                                                                                                      <block type="expression" id="CCo2rOZf#@2:aS58xIRSf">
                                                                                                                        <field name="value">i &lt; 100</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <statement name="do">
                                                                                                                      <block type="grid_shape" id="g5,O[V$!Y.:V?Tx2]$Q-">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_grid_dimension" id="GocPLut6DdIO5I8Y(OtU">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="rows">
                                                                                                                              <block type="expression" id="!z|6c(NM#+ruj!|UU@1/;">
                                                                                                                                <field name="value">1</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="cols">
                                                                                                                              <block type="expression" id="0(l0`qD%yOQgmq6OA/uG">
                                                                                                                                <field name="value">1</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_position" id="2GE]/nub%kn@2{#~6vW{y">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="z=cQ36UiLoB}-k%%-+/]">
                                                                                                                                    <field name="value">ox</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="%=[;3S_)Qmt4O45{(Hk5">
                                                                                                                                    <field name="value">oy</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_size" id="Sh@2j[V^ciXY{.2lc3hs)">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="width">
                                                                                                                                      <block type="expression" id="k8r.O`CLCnkm$9NV=eW{">
                                                                                                                                        <field name="value">check(i % 10, list_col(hide)) == 1 ? 100: 50</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="height">
                                                                                                                                      <block type="expression" id=":SgD(yH3/%GPI}G;h)c;">
                                                                                                                                        <field name="value">35</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_anchor" id="Ink{Nf(f9I)]9Dn[WE4l">
                                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="expression" id="?I7^_v.]Wzv+0~e1dV|V">
                                                                                                                                            <field name="value">0</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id="i59g8ig_GDtc+eT2,@1nI">
                                                                                                                                            <field name="value">0</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_grid_cell_source" id="?%y8:`U@2e{~C8L/X4?vl">
                                                                                                                                            <field name="#prop">cell.source</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id="~j@1,^liddf}oDg}E91^i">
                                                                                                                                                <field name="value">1</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_stroke" id="+gUkQrXDYdMCFFJnd@1bO">
                                                                                                                                                <field name="#prop">stroke</field>
                                                                                                                                                <value name="width">
                                                                                                                                                  <block type="expression" id="w==VhJWPny)SeVyJ49@2M">
                                                                                                                                                    <field name="value">2</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <value name="color">
                                                                                                                                                  <block type="expression" id="[APt]!e]m)dciFJa:d/l">
                                                                                                                                                    <field name="value">0x000000</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_fill" id="oefEFk_W$2=Epjqjn,FX">
                                                                                                                                                    <field name="#prop">fill</field>
                                                                                                                                                    <value name="color">
                                                                                                                                                      <block type="expression" id="D@26VrM5P%@242?mw.n:bP">
                                                                                                                                                        <field name="value">color[co]</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_grid_random" id="S;E:zL/;#ucH`;sP5$Pa">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <field name="random">FALSE</field>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_grid_show_borders" id="nCy;_7$d$f/BseZz5).}">
                                                                                                                                                            <field name="#prop">#showBorders</field>
                                                                                                                                                            <field name="value">TRUE</field>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_grid_cell_template" id="H/mY/D7v0(ZJG{qx|f@2l">
                                                                                                                                                                <field name="variable">$cell</field>
                                                                                                                                                                <field name="#prop">cell.template</field>
                                                                                                                                                                <field name="#callback">$cell</field>
                                                                                                                                                                <statement name="body">
                                                                                                                                                                  <block type="if_then_else_block" id="Xst;+0gqGM_8JlfoFu({">
                                                                                                                                                                    <value name="if">
                                                                                                                                                                      <block type="expression" id="ESv7pLP^Q~dp=qCQI(5(">
                                                                                                                                                                        <field name="value">check(i, hide) == 1</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <statement name="then">
                                                                                                                                                                      <block type="image_shape" id=",bnPTppm$ywdNGxWvH%X">
                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                          <block type="prop_position" id="EoZW3np{]G%~a`0@1J`G)">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="x">
                                                                                                                                                                              <block type="expression" id="0N)D.e5z@1X-fTZP$rB}E">
                                                                                                                                                                                <field name="value">$cell.centerX - $cell.width/2</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="y">
                                                                                                                                                                              <block type="expression" id="Qtu+?QwsZuNSn=T+u66j">
                                                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_size" id="rFhupwW`Fe5S`P8Xk-TI">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="width">
                                                                                                                                                                                  <block type="expression" id="?;Svewf5D?;iwj,E(.1A">
                                                                                                                                                                                    <field name="value">50</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <value name="height">
                                                                                                                                                                                  <block type="expression" id="?]VDlJ0m|0u-`CPKeV,c">
                                                                                                                                                                                    <field name="value">30</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_anchor" id="qBYEn;NkJ-91.!#=ZR@1%">
                                                                                                                                                                                    <field name="#prop">anchor</field>
                                                                                                                                                                                    <value name="x">
                                                                                                                                                                                      <block type="expression" id="Zb)lS`ks@2oC2~Me])qF8">
                                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <value name="y">
                                                                                                                                                                                      <block type="expression" id="Fc~mj=k2l(v4o@1:Ew}V)">
                                                                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_image_key" id="GfIH3VbK(c9:$QT;8kNY">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="key">
                                                                                                                                                                                          <block type="string_value" id="+g@2{DW,1j3xH~PcZ(5_.">
                                                                                                                                                                                            <field name="value">shape.png</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_image_src" id="}/hM0o!K,tvjN/Tr@2xgH">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <value name="src">
                                                                                                                                                                                              <block type="string_value" id=":3mJD6ArtIwAso5ECeOo">
                                                                                                                                                                                                <field name="value">${image_path}shape.png</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="algorithmic_input_shape" id="[e}LcmqF@19.%T?]@1jB-Z">
                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                              <block type="prop_value" id="TN6clfe}:4H;@1k0w%+`S">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="value">
                                                                                                                                                                                  <block type="expression" id="@1Y0=VVDuz^gR8?]A.GgV">
                                                                                                                                                                                    <field name="value">array[i]</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_position" id="(u^k=nloWKrc75-ihkQ6">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="x">
                                                                                                                                                                                      <block type="expression" id="+SQo+%UqGFz6@107iOi%4">
                                                                                                                                                                                        <field name="value">$cell.centerX - $cell.width/4</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <value name="y">
                                                                                                                                                                                      <block type="expression" id="aOD#P0{Z$l:BCNh)9@2~E">
                                                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_size" id="~)S$^+Glx|;{-)@1m?MCt">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="width">
                                                                                                                                                                                          <block type="expression" id="wdRRw?lccA]3huvks^0:">
                                                                                                                                                                                            <field name="value">40</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <value name="height">
                                                                                                                                                                                          <block type="expression" id="FlQ@1R-hpt5|W3YEsjl5h">
                                                                                                                                                                                            <field name="value">25</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_input_keyboard" id="pX[+1]_P03Uw?r6RhxOm">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <field name="keyboard">numbers1</field>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_input_max_length" id="e^196@2S|6ukB7~2Bxi9C">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <value name="maxLength">
                                                                                                                                                                                                  <block type="expression" id="/_E;-7``ONG+]nS_NxH+">
                                                                                                                                                                                                    <field name="value">array[i].toString().length</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="prop_input_result_position" id="#LH4fkex}OZhG#[@2d;3P">
                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                    <field name="resultPosition">right</field>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="prop_tab_order" id="s|F@1Izb{|@2lRY,asD!hu">
                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                        <value name="tabOrder">
                                                                                                                                                                                                          <block type="expression" id="2[il+_dgVQKQm?SvtFDj">
                                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="prop_stroke" id="D`nCliS/s1emx9euC0}t">
                                                                                                                                                                                                            <field name="#prop">stroke</field>
                                                                                                                                                                                                            <next>
                                                                                                                                                                                                              <block type="prop_fill" id="Dz-AaH/!wx?BO[MH9dIz">
                                                                                                                                                                                                                <field name="#prop">fill</field>
                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                  <block type="prop_text_style" id="!1Qr3{IxD@1PMJ]G^LlcN">
                                                                                                                                                                                                                    <field name="base">text</field>
                                                                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                                                                      <block type="prop_text_style_font_size" id="!(nlSpda9T+o/iXBj}YF">
                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                        <value name="fontSize">
                                                                                                                                                                                                                          <block type="expression" id="FQTZZB_+$@1-hM?K.!NSO">
                                                                                                                                                                                                                            <field name="value">28</field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                          <block type="prop_text_style_fill" id="w-4`M-zsemYrO[}g`_|q">
                                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                                            <value name="fill">
                                                                                                                                                                                                                              <block type="string_value" id="G;VmNGg}u8OZR_#3M_Oe">
                                                                                                                                                                                                                                <field name="value">black</field>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                              <block type="prop_text_style_stroke" id="ahppz0%Y{eD@2C$!vyJ4m">
                                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                                <value name="stroke">
                                                                                                                                                                                                                                  <block type="string_value" id="}KNcD$1%M)+@1a=7A%7,,">
                                                                                                                                                                                                                                    <field name="value">white</field>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                  <block type="prop_text_style_stroke_thickness" id="TBWlBG{V@2q@2LOjXi7|@16">
                                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                                    <value name="strokeThickness">
                                                                                                                                                                                                                                      <block type="expression" id="bs_awM@2W;X@1OG#fQ+@2no">
                                                                                                                                                                                                                                        <field name="value">2</field>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </next>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </next>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </statement>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                    <statement name="else">
                                                                                                                                                                      <block type="list_shape" id="Vq?wn_!:Keunmz@1?(oGc">
                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                          <block type="prop_list_direction" id="TxrH-pVfps@2UPo~,#Xd:">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <field name="dir">horizontal</field>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_position" id="+`eN+!K1=]uKm.tK#t`{">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="x">
                                                                                                                                                                                  <block type="expression" id="OYp7,)rp3k@2}fT/IYtR#">
                                                                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <value name="y">
                                                                                                                                                                                  <block type="expression" id="DX,YeA|8|qMHjj`tQb9-">
                                                                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_anchor" id="U7hY1{`jw0Wk|o^3fgxl">
                                                                                                                                                                                    <field name="#prop">anchor</field>
                                                                                                                                                                                    <value name="x">
                                                                                                                                                                                      <block type="expression" id="wS[?N+?~i_X!=pX~p2Ek">
                                                                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <value name="y">
                                                                                                                                                                                      <block type="expression" id="/zp~9bd4n7:iGd[2iMy`">
                                                                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_spacing" id="ho46Hc}?`2z.Oh:X:@1Q8">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="spacing">
                                                                                                                                                                                          <block type="expression" id="U=@2_WQRHf([FKM^nszJ@1">
                                                                                                                                                                                            <field name="value">1</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                        <statement name="items">
                                                                                                                                                                          <block type="list_item_shape" id="vIE/k|v{E]0^@17$.YJVY">
                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                              <block type="prop_list_align" id="KMAU4F(N@2qu`en#1G~^G">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <field name="align">middle</field>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_list_item_source" id="5T6r~([`(5@2EynD8`[du">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="source">
                                                                                                                                                                                      <block type="func_characters_of" id="z5h.Gc7RGMkst=R6oMK1" inline="true">
                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                          <block type="expression" id=".bDb@2JB-@1@1BF$A4d8eHr">
                                                                                                                                                                                            <field name="value">array[i]</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </statement>
                                                                                                                                                                            <statement name="template">
                                                                                                                                                                              <block type="if_then_else_block" id="]1H~FjD-LnSW2kWT,qqs">
                                                                                                                                                                                <value name="if">
                                                                                                                                                                                  <block type="expression" id="i|lyV]X?0s/-bp$p3}Ha">
                                                                                                                                                                                    <field name="value">array[i] &gt; 999</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <statement name="then">
                                                                                                                                                                                  <block type="image_shape" id="JSjt((sX4sLqVvI!v5Yg">
                                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                                      <block type="prop_image_key" id="@1X}J=z`UfSEk;JzM@2#Yw">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="key">
                                                                                                                                                                                          <block type="string_value" id="GXlUo,Y+NjMz^^b1_7@2.">
                                                                                                                                                                                            <field name="value">${$item.data}.png</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_image_src" id="1O%tIBk{d`Q5B6?MPj0Q">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <value name="src">
                                                                                                                                                                                              <block type="string_value" id=";12IQ^X$wnO%Hu])sh/S">
                                                                                                                                                                                                <field name="value">${image_path}${$item.data}.png</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_scale" id="y}z4xvSHcpO^`}X1[i8l">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <value name="scale">
                                                                                                                                                                                                  <block type="expression" id="Lg(9reyv{)!?i5SqRO0r">
                                                                                                                                                                                                    <field name="value">0.4</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </statement>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </statement>
                                                                                                                                                                                <statement name="else">
                                                                                                                                                                                  <block type="image_shape" id="cWmiNGqA6cTDlB]|l|KZ">
                                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                                      <block type="prop_image_key" id="Od/81F0N-gM~nNXGqzAU">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="key">
                                                                                                                                                                                          <block type="string_value" id="Esm#lfe]:u0@1HnTl5mKb">
                                                                                                                                                                                            <field name="value">${$item.data}.png</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_image_src" id="g=K(,;w2N9J]F8+#J=eT">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <value name="src">
                                                                                                                                                                                              <block type="string_value" id="IXE4d$]-Aag}8LQATeVC">
                                                                                                                                                                                                <field name="value">${image_path}${$item.data}.png</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_scale" id="|xiBX^Rx@1-t}AqCA@2kHz">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <value name="scale">
                                                                                                                                                                                                  <block type="expression" id="z_;Q+^e+|3%HSUgf4qO0">
                                                                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </statement>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </statement>
                                                                                                                                                                              </block>
                                                                                                                                                                            </statement>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="if_then_else_block" id="f;fbW`}3/p.2NaQ{Y_oI">
                                                                                                                            <value name="if">
                                                                                                                              <block type="expression" id=":$?U)0$!(l-b(soDxW?4">
                                                                                                                                <field name="value">check(i % 10, list_col(hide)) == 1</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <statement name="then">
                                                                                                                              <block type="variable" id="-aYN|]uO)64/Y@26Vu}M6">
                                                                                                                                <field name="name">ox</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="d,Hl2Fh)dDfL2l3h`8P6">
                                                                                                                                    <field name="value">ox + 100</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <statement name="else">
                                                                                                                              <block type="variable" id="-a50|B{geD3?-0qxY^7U">
                                                                                                                                <field name="name">ox</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="70jz%Alv~i-b1O,XQx/e">
                                                                                                                                    <field name="value">ox + 50</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="if_then_block" id="|K7?b@2cVLAXZGxE%IxSu">
                                                                                                                                <value name="if">
                                                                                                                                  <block type="expression" id="5t|8E4p[N~WZP60Zzy~;">
                                                                                                                                    <field name="value">i % 10 == 9</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <statement name="then">
                                                                                                                                  <block type="variable" id="$poIaPG}R3R?WFb{G_W1">
                                                                                                                                    <field name="name">ox</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="qdhAEjDtf}gOa=(KOD)f">
                                                                                                                                        <field name="value">$cell.centerX - $cell.width/2</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="variable" id=";0:`$ked||g4ydcBY13z">
                                                                                                                                        <field name="name">oy</field>
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="expression" id="_)x/p~hi.i@28=6EDJKBI">
                                                                                                                                            <field name="value">oy + 35</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <next>
                                                                                                                                  <block type="variable" id="Bav`Hv]iiJBXa=^@2AfMY">
                                                                                                                                    <field name="name">i</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="T9ay.1n}n%R]4BXXYye-">
                                                                                                                                        <field name="value">i+1</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="configure_keyboard" id="V`L27h;W:~R{a{P;OJnO">
                                                                        <field name="keyboard">numbers1</field>
                                                                        <value name="anchorX">
                                                                          <block type="expression" id="3:|p6K[wkdaJ,Lg]l+1@2">
                                                                            <field name="value">0.5</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="anchorY">
                                                                          <block type="expression" id="6_jX,W^XGmj|w5;u-eG|">
                                                                            <field name="value">1</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="x">
                                                                          <block type="expression" id="k653)|Crq=kpFG~L[;jx">
                                                                            <field name="value">400</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id="_@2|seGiI7`g+|:{qW-H@2">
                                                                            <field name="value">450</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="scale">
                                                                          <block type="expression" id="GhQqKgV,g1wndAIh.}-Z">
                                                                            <field name="value">0.8</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="?-;9.(.T85UwpM2p4RS-">
                                                                            <field name="name">Variable</field>
                                                                            <value name="value">
                                                                              <block type="custom_image_list" id="iU|EK#9K44IK%KY?K{VR">
                                                                                <field name="link">${image_path}</field>
                                                                                <field name="images">0|1|2|3|4|5|6|7|8|9</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="variable" id=";|)W27xe0yc24d~C;8YW">
                                                                                <field name="name">color</field>
                                                                                <value name="value">
                                                                                  <block type="expression" id="{]jF`M}-|yN1RdxvE(ut">
                                                                                    <field name="value">['#ff9999', '#ffbb99', '#cc99ff', '#b3ccff', '#ecb3ff']</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="partial_explanation" id="hS(sN93w[72]Fz@2T^/I}" inline="true">
                                                                                    <value name="value">
                                                                                      <block type="string_value" id="Nj3YpHMKE}0+S?~#SX2#">
                                                                                        <field name="value">&lt;center&gt;&lt;u&gt;Count using the number chart below.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
  td{border: 2px solid black; text-align: center; padding: 10px; width: 90px}
img{}
  &lt;/style&gt;

    &lt;table&gt;
    &lt;tr&gt;
                                                                                        </field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="variable" id="E4):k[JmLaj:Yij~xi)W">
                                                                                        <field name="name">i</field>
                                                                                        <value name="value">
                                                                                          <block type="expression" id="R3xLRy.f%aRQ6XBM(/WV">
                                                                                            <field name="value">0</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="while_do_block" id="6zhX[(yRmu`IvqMyW]#u">
                                                                                            <value name="while">
                                                                                              <block type="expression" id="Bdm,o]bBxhYng?QfS1Aw">
                                                                                                <field name="value">i &lt; 100</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <statement name="do">
                                                                                              <block type="if_then_else_block" id="9K$([t4gbV-5AF-NXz-c">
                                                                                                <value name="if">
                                                                                                  <block type="expression" id=",NLD}@1thU~L^]bQ)mS)$">
                                                                                                    <field name="value">check(i, hide) == 1</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <statement name="then">
                                                                                                  <block type="partial_explanation" id="4N1r9wk4j}-3%k~D@279h" inline="true">
                                                                                                    <value name="value">
                                                                                                      <block type="string_value" id="T1Pan{2f@2G)V3W=Oiv/P">
                                                                                                        <field name="value">&lt;td style='background: ${color[co]}'&gt;&lt;b&gt;${array[i]}&lt;/b&gt;&lt;/td&gt;</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <statement name="else">
                                                                                                  <block type="partial_explanation" id="}APIB+%7z:chEnkeNq8X" inline="true">
                                                                                                    <value name="value">
                                                                                                      <block type="string_value" id="R9t?~Z)dK?y0|]OhnPf}">
                                                                                                        <field name="value">&lt;td style='background: white'&gt;${array[i]}&lt;/td&gt;</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="if_then_block" id="PY+0_0yXB$M%:uVQs]Gu">
                                                                                                    <value name="if">
                                                                                                      <block type="expression" id="-3!?^Gm];a0.W.q)Mtdn">
                                                                                                        <field name="value">i % 10 == 9 &amp;&amp; i  != 99</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <statement name="then">
                                                                                                      <block type="partial_explanation" id="qtGneYpV1moE@22Ns3o3v" inline="true">
                                                                                                        <value name="value">
                                                                                                          <block type="string_value" id="u74-l,Sc1wLg1ENH%,P+">
                                                                                                            <field name="value">&lt;/tr&gt;&lt;tr&gt;</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="variable" id="s|FB@2}1198!W$KD;;@1Cq">
                                                                                                        <field name="name">i</field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id="qG_=h[XSW9?pF~2[)3?t">
                                                                                                            <field name="value">i+1</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="partial_explanation" id="u:j7q24rLndi2`;YA..e" inline="true">
                                                                                                <value name="value">
                                                                                                  <block type="string_value" id="1.kFON;hrVUrZi{Ba$eO">
                                                                                                    <field name="value">&lt;/tr&gt;&lt;/table&gt;&lt;/center&gt;</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="end_partial_explanation" id="8]#^YeokTnZn5S`Q;:/T"></block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
  <block type="grid_shape" id="6tU?K^N8ec~A@1@1NwQ72N" x="859" y="662">
    <statement name="#props">
      <block type="prop_grid_dimension" id="X4K:|9dKK%s9l4n5v@2lG">
        <field name="#prop"></field>
        <value name="rows">
          <block type="expression" id="[Ev!.bxnzjc|-33v.%7=">
            <field name="value">10</field>
          </block>
        </value>
        <value name="cols">
          <block type="expression" id="u$7]/V/!ZWM4(Va??@2({">
            <field name="value">10</field>
          </block>
        </value>
        <next>
          <block type="prop_position" id="zLld84TfBg#!88]1,JD-">
            <field name="#prop"></field>
            <value name="x">
              <block type="expression" id="Cd6g{Y_P?:GyVyvQi1wJ">
                <field name="value">400</field>
              </block>
            </value>
            <value name="y">
              <block type="expression" id=".F}E71+@1i)77!yQ`hmfu">
                <field name="value">220</field>
              </block>
            </value>
            <next>
              <block type="prop_size" id="+o2CWr^.ohx)_bcIG53]">
                <field name="#prop"></field>
                <value name="width">
                  <block type="expression" id="Z[x5iHG%KWC%KWQb}c-G">
                    <field name="value">600</field>
                  </block>
                </value>
                <value name="height">
                  <block type="expression" id="@2E|Ob@1jF#gYtX8-%Iui@1">
                    <field name="value">350</field>
                  </block>
                </value>
                <next>
                  <block type="prop_anchor" id="1WJjXY^.h,t]eNgxaJ}q">
                    <field name="#prop">anchor</field>
                    <value name="x">
                      <block type="expression" id="DHx[vqs?.?nUD:AYPJWd">
                        <field name="value">0.5</field>
                      </block>
                    </value>
                    <value name="y">
                      <block type="expression" id="e3D|RR7w!=Sufu):zV`c">
                        <field name="value">0.5</field>
                      </block>
                    </value>
                    <next>
                      <block type="prop_stroke" id="GR31Y7DeZ$zSeq+5SpZ]">
                        <field name="#prop">stroke</field>
                        <value name="width">
                          <block type="expression" id="Nz.MU$09n.Y2_c!=e#ab">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="color">
                          <block type="expression" id="x6]:zw$XOqx!^]|l/9/6">
                            <field name="value">0x000000</field>
                          </block>
                        </value>
                        <next>
                          <block type="prop_fill" id="vyutdU}v4wrmBX`8Z9}V">
                            <field name="#prop">fill</field>
                            <value name="color">
                              <block type="expression" id="`D8#%G]z}+@25n3{ThbY(">
                                <field name="value">color[co]</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_grid_cell_source" id="v8+Yu211Qg8fX9Mu4th`">
                                <field name="#prop">cell.source</field>
                                <value name="value">
                                  <block type="func_array_of_number" id="73qbC-9P1rGbQoku3.O=" inline="true">
                                    <value name="items">
                                      <block type="expression" id="pS}nii@1`%{C[V{c#-RPJ">
                                        <field name="value">100</field>
                                      </block>
                                    </value>
                                    <value name="from">
                                      <block type="expression" id="Epaf8z+8A`;k_oT@28w.a">
                                        <field name="value">0</field>
                                      </block>
                                    </value>
                                  </block>
                                </value>
                                <next>
                                  <block type="prop_grid_random" id="}?kS#x[b)9OGb@1`UdJGV">
                                    <field name="#prop"></field>
                                    <field name="random">FALSE</field>
                                    <next>
                                      <block type="prop_grid_show_borders" id=";y^wk4y%[VWjXOj~vrQZ">
                                        <field name="#prop">#showBorders</field>
                                        <field name="value">TRUE</field>
                                        <next>
                                          <block type="prop_grid_cell_template" id="7^6]Kv:2yrZi`cooAW2@2">
                                            <field name="variable">$cell</field>
                                            <field name="#prop">cell.template</field>
                                            <field name="#callback">$cell</field>
                                            <statement name="body">
                                              <block type="if_then_else_block" id="0a/vewU.M.wCHo/CG[XT">
                                                <value name="if">
                                                  <block type="expression" id="LhkTRPWoLxMqYu!C)OQ}">
                                                    <field name="value">check($cell.data, hide) == 1</field>
                                                  </block>
                                                </value>
                                                <statement name="then">
                                                  <block type="if_then_else_block" id="2Zh~I#;Qs7-KxHDT?U-r">
                                                    <value name="if">
                                                      <block type="expression" id="7_BEf5zTd1$:j}Dt-$MC">
                                                        <field name="value">check($cell.data+10, hide) == 0</field>
                                                      </block>
                                                    </value>
                                                    <statement name="then">
                                                      <block type="algorithmic_input_shape" id="URlE;C2E39G-}f$;EJT[">
                                                        <statement name="#props">
                                                          <block type="prop_value" id="m-fMu0CF=hjXNEQ$-8p`">
                                                            <field name="#prop"></field>
                                                            <value name="value">
                                                              <block type="expression" id="ROB3f!kX|}+zGlZaXKtn">
                                                                <field name="value">array[$cell.data]</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="prop_position" id="qRKSKV.UAF=NV^l[mpMC">
                                                                <field name="#prop"></field>
                                                                <value name="x">
                                                                  <block type="expression" id="Q8UDSzjw$[DcNbA8Byy]">
                                                                    <field name="value">$cell.centerX</field>
                                                                  </block>
                                                                </value>
                                                                <value name="y">
                                                                  <block type="expression" id=":.Q|-Z7uSNGWHASG~I`l">
                                                                    <field name="value">$cell.centerY</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_size" id="FTs[yEP)?J+S_+EM`vS-">
                                                                    <field name="#prop"></field>
                                                                    <value name="width">
                                                                      <block type="expression" id=":cRlk:s4w7eb1KR8nwbq">
                                                                        <field name="value">40</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="height">
                                                                      <block type="expression" id="Q_}|+.)R:xKeUYR4Pv.Y">
                                                                        <field name="value">10</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_input_keyboard" id="d@2-yCrj{/v9MHX[SWLGE">
                                                                        <field name="#prop"></field>
                                                                        <field name="keyboard">numbers1</field>
                                                                        <next>
                                                                          <block type="prop_input_max_length" id="=ETtNK7yo)A4$NSO1KfI">
                                                                            <field name="#prop"></field>
                                                                            <value name="maxLength">
                                                                              <block type="expression" id="m}U@2$JIHv{$zUQYBk2$F">
                                                                                <field name="value">array[$cell.data].toString().length</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_input_result_position" id="bcpxeM,}Pf![D,5/+[TQ">
                                                                                <field name="#prop"></field>
                                                                                <field name="resultPosition">bottom</field>
                                                                                <next>
                                                                                  <block type="prop_tab_order" id="%/@1Etzx(p15s%+SGXug#">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="tabOrder">
                                                                                      <block type="expression" id="OlytK!d8i6ytiM,.Zce:">
                                                                                        <field name="value">0</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_stroke" id="%/Gt7@1ZqKe:F2);Kn!,D">
                                                                                        <field name="#prop">stroke</field>
                                                                                        <next>
                                                                                          <block type="prop_fill" id="5G+6_NU#7Asd_@2hdzE_)">
                                                                                            <field name="#prop">fill</field>
                                                                                            <next>
                                                                                              <block type="prop_text_style" id="zGP/J.:Uht[Pw|4,xi(.">
                                                                                                <field name="base">text</field>
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_text_style_font_size" id="PdDui=![kgJUT8sfFq;@2">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="fontSize">
                                                                                                      <block type="expression" id="%kIL#NAtfnCDx{+/CL1a">
                                                                                                        <field name="value">28</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style_fill" id="W;.?jd~0m$iSnAvxo5ML">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="fill">
                                                                                                          <block type="string_value" id="kYa8RSsjBQE5Y0}M[3#F">
                                                                                                            <field name="value">black</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style_stroke" id="lxmn/Z}9#r({fu,OkD5.">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="stroke">
                                                                                                              <block type="string_value" id="!Jdp}VB3;f,sTVqd;Rl?">
                                                                                                                <field name="value">white</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_stroke_thickness" id="TaUNTah]_w8l[}cwA/Kk">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="strokeThickness">
                                                                                                                  <block type="expression" id="47?+G)~QsX6W+c.)U;m~">
                                                                                                                    <field name="value">2</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </statement>
                                                      </block>
                                                    </statement>
                                                    <statement name="else">
                                                      <block type="if_then_else_block" id="7~_Y75cI#d{Yho@2p~@2Q3">
                                                        <value name="if">
                                                          <block type="expression" id="IE?rsHY7Cts]:He%@27]L">
                                                            <field name="value">check($cell.data-10, hide) == 0</field>
                                                          </block>
                                                        </value>
                                                        <statement name="then">
                                                          <block type="algorithmic_input_shape" id="}HxiT9,6]YMd6-J1U#fv">
                                                            <statement name="#props">
                                                              <block type="prop_value" id="{7}fw1Sme+m5R0@2UJS=Q">
                                                                <field name="#prop"></field>
                                                                <value name="value">
                                                                  <block type="expression" id="ZR7#H[rINsfj.#MP%CVv">
                                                                    <field name="value">array[$cell.data]</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_position" id="E^(I7rw!5gj;|,A1d@1gz">
                                                                    <field name="#prop"></field>
                                                                    <value name="x">
                                                                      <block type="expression" id="]2F)VOJX^Bx.X-4C}jg~">
                                                                        <field name="value">$cell.centerX</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="y">
                                                                      <block type="expression" id="akRv|TS(SV-5?g${sNCr">
                                                                        <field name="value">$cell.centerY</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_size" id="I-x~8@2+y9Yja3;M6y_GO">
                                                                        <field name="#prop"></field>
                                                                        <value name="width">
                                                                          <block type="expression" id="LQV{~2a`GoZX|]qp%%zH">
                                                                            <field name="value">40</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="height">
                                                                          <block type="expression" id="Kog)][:DTyB,gqoZWs32">
                                                                            <field name="value">10</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_input_keyboard" id="b18Uu07@1DSbaFoeTV6?n">
                                                                            <field name="#prop"></field>
                                                                            <field name="keyboard">numbers1</field>
                                                                            <next>
                                                                              <block type="prop_input_max_length" id=".c{]/oKX+WfpH6?Oi@15o">
                                                                                <field name="#prop"></field>
                                                                                <value name="maxLength">
                                                                                  <block type="expression" id="^3Kru/,W1ZCl+iuvW7M7">
                                                                                    <field name="value">array[$cell.data].toString().length</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_input_result_position" id="8op_YS|yB_nnfTlQrlPH">
                                                                                    <field name="#prop"></field>
                                                                                    <field name="resultPosition">top</field>
                                                                                    <next>
                                                                                      <block type="prop_tab_order" id="]P{@1PLQ)@2W,aYgQ.UXHT">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="tabOrder">
                                                                                          <block type="expression" id="Y)RJ(:[LE]=u54npjjIm">
                                                                                            <field name="value">0</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_stroke" id="_sH9cXU|$1ku[`Xw_@2;e">
                                                                                            <field name="#prop">stroke</field>
                                                                                            <next>
                                                                                              <block type="prop_fill" id="k,wN,vKZ}dq@18noKU3gN">
                                                                                                <field name="#prop">fill</field>
                                                                                                <next>
                                                                                                  <block type="prop_text_style" id="L~]q[},Q#Hykf=,jyP5?">
                                                                                                    <field name="base">text</field>
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_text_style_font_size" id="vONR|^449}h%X|F3.@1vz">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="fontSize">
                                                                                                          <block type="expression" id="Q(rP)hX$%6M1EBsMZ:aN">
                                                                                                            <field name="value">28</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style_fill" id="SyLOp?:g%CJAeFxHj@10c">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="fill">
                                                                                                              <block type="string_value" id="5GIK;PZEYDF=|YX{15`O">
                                                                                                                <field name="value">black</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_stroke" id="m+x]2r]B4Ye@2LYyf?Aw:">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="stroke">
                                                                                                                  <block type="string_value" id="THXn;(Q8gMuf@2ld?JUbG">
                                                                                                                    <field name="value">white</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_stroke_thickness" id="HFa[)XYZ(;;_|9tjX8)6">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="strokeThickness">
                                                                                                                      <block type="expression" id="EuE8r!IUqkz4C(.~-Q)h">
                                                                                                                        <field name="value">2</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </statement>
                                                          </block>
                                                        </statement>
                                                        <statement name="else">
                                                          <block type="if_then_else_block" id="1mW4/:DjqLZfldv/UA}b">
                                                            <value name="if">
                                                              <block type="expression" id=";fuYbor2A-;Tcg3!Ne^l">
                                                                <field name="value">check($cell.data+1, hide) == 0</field>
                                                              </block>
                                                            </value>
                                                            <statement name="then">
                                                              <block type="algorithmic_input_shape" id="PdKjRS+PW@1qvvUNBiB=w">
                                                                <statement name="#props">
                                                                  <block type="prop_value" id="c2xEs/Zuu8![:RgiS#JV">
                                                                    <field name="#prop"></field>
                                                                    <value name="value">
                                                                      <block type="expression" id=",mkj4xO`m31lMH!hypsA">
                                                                        <field name="value">array[$cell.data]</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_position" id="z#WqEwTyUbSgil,%vCQ)">
                                                                        <field name="#prop"></field>
                                                                        <value name="x">
                                                                          <block type="expression" id="1l=Ig^YNUwbE9crsx9VE">
                                                                            <field name="value">$cell.centerX</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id="748.E+hI@1o(z}?hApp=9">
                                                                            <field name="value">$cell.centerY</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_size" id="xqsV@2:1@2y~`__{bAf~(c">
                                                                            <field name="#prop"></field>
                                                                            <value name="width">
                                                                              <block type="expression" id="#k7r7moku{q58WJ$ZY#t">
                                                                                <field name="value">40</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="height">
                                                                              <block type="expression" id="`r~eyZ.9_]yolEQE2|iy">
                                                                                <field name="value">10</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_input_keyboard" id="!O?dbmC$@2YNWE.$k@1_If">
                                                                                <field name="#prop"></field>
                                                                                <field name="keyboard">numbers1</field>
                                                                                <next>
                                                                                  <block type="prop_input_max_length" id="Gi5=}SfNxtxZA)(g%e~9">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="maxLength">
                                                                                      <block type="expression" id="%tcJ!8)%-.go0yt^0#}Z">
                                                                                        <field name="value">array[$cell.data].toString().length</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_input_result_position" id="`RDOQl|RRQ{Qv$w+Ojkt">
                                                                                        <field name="#prop"></field>
                                                                                        <field name="resultPosition">right</field>
                                                                                        <next>
                                                                                          <block type="prop_tab_order" id="%_~dQI:7jJ-r/KZWA[}8">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="tabOrder">
                                                                                              <block type="expression" id="P=N{61}U:6/rJZxM^6Uq">
                                                                                                <field name="value">0</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_stroke" id="s#x+rD1+l@2cs^.hw0yZh">
                                                                                                <field name="#prop">stroke</field>
                                                                                                <next>
                                                                                                  <block type="prop_fill" id="EU#GhS+G_{(Czp7=H0mr">
                                                                                                    <field name="#prop">fill</field>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style" id="GMC[/6aGuxkzC%8nfx_W">
                                                                                                        <field name="base">text</field>
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_text_style_font_size" id="yZI9Q~$tL!ScQdIt7^p?">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="fontSize">
                                                                                                              <block type="expression" id="[+-`:ZSP{8Gqm3?+|euK">
                                                                                                                <field name="value">28</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_fill" id="tSB#9`HQji2lbJVFKWkM">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="fill">
                                                                                                                  <block type="string_value" id="o.eYS@2Y7]K-${Yj,Su/N">
                                                                                                                    <field name="value">black</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_stroke" id=".NOX`pbPx#^;x@14dm)|I">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="stroke">
                                                                                                                      <block type="string_value" id="WwBDq.H[RDi;:B1|S%mU">
                                                                                                                        <field name="value">white</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_stroke_thickness" id="g79sU@1|U7zIv45f%Q:mk">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="strokeThickness">
                                                                                                                          <block type="expression" id="OG@1seZcQR5|r3K10^H|/">
                                                                                                                            <field name="value">2</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                              </block>
                                                            </statement>
                                                            <statement name="else">
                                                              <block type="algorithmic_input_shape" id="n@1r:@1OF@2xe.6^xHsx+3~">
                                                                <statement name="#props">
                                                                  <block type="prop_value" id="dZL_7LDL)dAk!8YU=9(L">
                                                                    <field name="#prop"></field>
                                                                    <value name="value">
                                                                      <block type="expression" id="LW_IWWIOD7^2=OQL2(9`">
                                                                        <field name="value">array[$cell.data]</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_position" id="6UTFHSU7?_EpEevIdh/f">
                                                                        <field name="#prop"></field>
                                                                        <value name="x">
                                                                          <block type="expression" id="eeTM:d)~kFocgA(fVGmE">
                                                                            <field name="value">$cell.centerX</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id="ej@1KWIiS;sO]Awd~oKNy">
                                                                            <field name="value">$cell.centerY</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_size" id="u2WnZolvyB#`tFhV~a4T">
                                                                            <field name="#prop"></field>
                                                                            <value name="width">
                                                                              <block type="expression" id="cSju^QJf3=2Z]MS0?FW~">
                                                                                <field name="value">40</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="height">
                                                                              <block type="expression" id="Q~D@17tC^hb-7,/y@2{Jw:">
                                                                                <field name="value">10</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_input_keyboard" id="K$J=6,m+.454/Y0~K_r5">
                                                                                <field name="#prop"></field>
                                                                                <field name="keyboard">numbers1</field>
                                                                                <next>
                                                                                  <block type="prop_input_max_length" id=":ndlPj0aG:5+cwPTvXus">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="maxLength">
                                                                                      <block type="expression" id="eDuu]edSIJ;fS}irh85B">
                                                                                        <field name="value">array[$cell.data].toString().length</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_input_result_position" id="VQMP|w.f0yZ89_(4xA?(">
                                                                                        <field name="#prop"></field>
                                                                                        <field name="resultPosition">left</field>
                                                                                        <next>
                                                                                          <block type="prop_tab_order" id="y[6#QS)TyfaJtu.D/D@2F">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="tabOrder">
                                                                                              <block type="expression" id="|DK@2XPApycI9(@2wlgwoe">
                                                                                                <field name="value">0</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_stroke" id="a!}Vf`/Br4fKFpX+?,~h">
                                                                                                <field name="#prop">stroke</field>
                                                                                                <next>
                                                                                                  <block type="prop_fill" id=",LXLVr1u(;gR6C3S/hH,">
                                                                                                    <field name="#prop">fill</field>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style" id="$GwhE/#C+:2)yE,x=SHZ">
                                                                                                        <field name="base">text</field>
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_text_style_font_size" id="ph`fWYts1^y4@25gF!s%_">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="fontSize">
                                                                                                              <block type="expression" id="fhszjT|dij/VcV#1Y;1]">
                                                                                                                <field name="value">28</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_fill" id="t%nG4B$DN?y;ll2SFyU#">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="fill">
                                                                                                                  <block type="string_value" id="pSlSKhy2J@2|]/8i!TD~g">
                                                                                                                    <field name="value">black</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_stroke" id="EU~Rf:X~x~N}zWK!ilsZ">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="stroke">
                                                                                                                      <block type="string_value" id="SCkNMK)XzlW0ecq/tB{F">
                                                                                                                        <field name="value">white</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_stroke_thickness" id="~pm(dF:13?||@2gVa$;|9">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="strokeThickness">
                                                                                                                          <block type="expression" id="rwu)$Kla3k~A@2dV[Z$3m">
                                                                                                                            <field name="value">2</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                              </block>
                                                            </statement>
                                                          </block>
                                                        </statement>
                                                      </block>
                                                    </statement>
                                                  </block>
                                                </statement>
                                              </block>
                                            </statement>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
</xml>
END_XML]] */