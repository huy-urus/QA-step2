
module.exports = [
  {
    "#type": "question",
    "name": "Y2.SP.DA.GD.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y2.SP.DA.GD.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "rank",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "variable",
        "name": "sex",
        "value": {
          "#type": "random_one",
          "items": {
            "#type": "expression",
            "value": "['his', 'her']"
          }
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "sex == 'his'"
        },
        "then": [
          {
            "#type": "variable",
            "name": "list_name",
            "value": {
              "#type": "string_array",
              "items": "David|Johnny|Peter|Paul|Michael|Bob|Stephen|Angelo|Boon|Kavin|Angus|Luke|Jason|Mitch|Jeremy|John"
            }
          }
        ],
        "else": [
          {
            "#type": "variable",
            "name": "list_name",
            "value": {
              "#type": "string_array",
              "items": "Mary|Jane|Lisa|Rose|Olivia|Nina|Sara|Janette|Isabelle|Angela|Bessie|Susan|Francine|Brigitta|Julie"
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "item",
        "value": {
          "#type": "string_array",
          "items": "pet|sport|colour|mode of transport|food|book|lolly|shape"
        }
      },
      {
        "#type": "variable",
        "name": "group",
        "value": {
          "#type": "string_array",
          "items": "schoolmates|classmates|neighbours|swim team friends|soccer teammates|art club friends|playgroup friends"
        }
      },
      {
        "#type": "variable",
        "name": "place",
        "value": {
          "#type": "string_array",
          "items": "school|class|neighbourhood|swim team|soccer team|art club|playgroup"
        }
      },
      {
        "#type": "variable",
        "name": "name",
        "value": {
          "#type": "random_one",
          "items": {
            "#type": "expression",
            "value": "list_name"
          }
        }
      },
      {
        "#type": "variable",
        "name": "value",
        "value": {
          "#type": "random_many",
          "count": {
            "#type": "expression",
            "value": "3"
          },
          "items": {
            "#type": "func",
            "name": "arrayOfNumber",
            "args": [
              {
                "#type": "expression",
                "value": "9"
              },
              {
                "#type": "expression",
                "value": "1"
              }
            ]
          }
        }
      },
      {
        "#type": "variable",
        "name": "it1",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "0"
          },
          "max": {
            "#type": "expression",
            "value": "7"
          }
        }
      },
      {
        "#type": "variable",
        "name": "g",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "0"
          },
          "max": {
            "#type": "expression",
            "value": "6"
          }
        }
      },
      {
        "#type": "variable",
        "name": "answer",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "0"
          },
          "max": {
            "#type": "expression",
            "value": "1"
          }
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "answer == 1"
        },
        "then": [
          {
            "#type": "variable",
            "name": "p",
            "value": {
              "#type": "expression",
              "value": "g"
            }
          },
          {
            "#type": "variable",
            "name": "it",
            "value": {
              "#type": "expression",
              "value": "it1"
            }
          },
          {
            "#type": "func",
            "name": "addAnswers",
            "args": [
              {
                "#type": "expression",
                "value": "11"
              }
            ]
          }
        ],
        "else": [
          {
            "#type": "do_while_block",
            "do": [
              {
                "#type": "variable",
                "name": "p",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "0"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "6"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "it",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "0"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "7"
                  }
                }
              }
            ],
            "while": {
              "#type": "expression",
              "value": "it == it1 || p == g"
            }
          },
          {
            "#type": "func",
            "name": "addAnswers",
            "args": [
              {
                "#type": "expression",
                "value": "10"
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "type",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "3"
          }
        }
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "60"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "${name} asked ${sex} ${group[g]} what were their favourite ${item[it1]}.\nDid his question help to gather the data below?"
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "32"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "1"
            },
            "cols": {
              "#type": "expression",
              "value": "2"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "type == 2 ? 410 : 390"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "600"
            },
            "height": {
              "#type": "expression",
              "value": "50"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "expression",
              "value": "[0, 1]"
            }
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_cell_template_for",
            "variable": "$cell",
            "condition": "$cell.data == 0",
            "#prop": "cell.templates[]",
            "#callback": "$cell",
            "body": [
              {
                "#type": "choice_custom_shape",
                "action": "click-one",
                "value": {
                  "#type": "expression",
                  "value": "10"
                },
                "template": {
                  "#callback": "$choice",
                  "variable": "$choice",
                  "body": [
                    {
                      "#type": "image_shape",
                      "#props": [
                        {
                          "#type": "prop_position",
                          "#prop": "",
                          "x": {
                            "#type": "expression",
                            "value": "$cell.centerX"
                          },
                          "y": {
                            "#type": "expression",
                            "value": "$cell.centerY"
                          }
                        },
                        {
                          "#type": "prop_image_key",
                          "#prop": "",
                          "key": "no.png"
                        },
                        {
                          "#type": "prop_image_src",
                          "#prop": "",
                          "src": "${image_path}no.png"
                        }
                      ]
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "prop_grid_cell_template_for",
            "variable": "$cell",
            "condition": "$cell.data == 1",
            "#prop": "cell.templates[]",
            "#callback": "$cell",
            "body": [
              {
                "#type": "choice_custom_shape",
                "action": "click-one",
                "value": {
                  "#type": "expression",
                  "value": "11"
                },
                "template": {
                  "#callback": "$choice",
                  "variable": "$choice",
                  "body": [
                    {
                      "#type": "image_shape",
                      "#props": [
                        {
                          "#type": "prop_position",
                          "#prop": "",
                          "x": {
                            "#type": "expression",
                            "value": "$cell.centerX"
                          },
                          "y": {
                            "#type": "expression",
                            "value": "$cell.centerY"
                          }
                        },
                        {
                          "#type": "prop_image_key",
                          "#prop": "",
                          "key": "yes.png"
                        },
                        {
                          "#type": "prop_image_src",
                          "#prop": "",
                          "src": "${image_path}yes.png"
                        }
                      ]
                    }
                  ]
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "130"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "Most favourite ${item[it]} in ${place[p]}"
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "32"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "image_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "240"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "0"
            },
            "height": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "table${type}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}table${type}.png"
          }
        ]
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "type == 2"
        },
        "then": [
          {
            "#type": "variable",
            "name": "color",
            "value": {
              "#type": "random_many",
              "count": {
                "#type": "expression",
                "value": "3"
              },
              "items": {
                "#type": "func",
                "name": "arrayOfNumber",
                "args": [
                  {
                    "#type": "expression",
                    "value": "5"
                  },
                  {
                    "#type": "expression",
                    "value": "1"
                  }
                ]
              }
            }
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "35",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "3"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "355"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "500"
                },
                "height": {
                  "#type": "expression",
                  "value": "30"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "[0, 1, 2]"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_max_size",
                        "#prop": "",
                        "maxWidth": {
                          "#type": "expression",
                          "value": "0"
                        },
                        "maxHeight": {
                          "#type": "expression",
                          "value": "$cell.height - 10"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "${it}_${$cell.data}.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}${it}_${$cell.data}.png"
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "35",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "3"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "240"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "500"
                },
                "height": {
                  "#type": "expression",
                  "value": "195"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "[0, 1, 2]"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.bottom"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "0"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "195/10*value[$cell.data]"
                        }
                      },
                      {
                        "#type": "prop_anchor",
                        "#prop": "anchor",
                        "x": {
                          "#type": "expression",
                          "value": "0.5"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "1"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "ver${color[$cell.data]}.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}ver${color[$cell.data]}.png"
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ],
        "else": [
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "3"
                },
                "cols": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "145"
                },
                "y": {
                  "#type": "expression",
                  "value": "240"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "180"
                },
                "height": {
                  "#type": "expression",
                  "value": "160"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "[0, 1, 2]"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_max_size",
                        "#prop": "",
                        "maxWidth": {
                          "#type": "expression",
                          "value": "50"
                        },
                        "maxHeight": {
                          "#type": "expression",
                          "value": "$cell.height - 10"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "${it}_${$cell.data}.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}${it}_${$cell.data}.png"
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "type == 1"
            },
            "then": [
              {
                "#type": "grid_shape",
                "#props": [
                  {
                    "#type": "prop_grid_dimension",
                    "#prop": "",
                    "rows": {
                      "#type": "expression",
                      "value": "3"
                    },
                    "cols": {
                      "#type": "expression",
                      "value": "1"
                    }
                  },
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "490"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "240"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "516"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "160"
                    }
                  },
                  {
                    "#type": "prop_anchor",
                    "#prop": "anchor",
                    "x": {
                      "#type": "expression",
                      "value": "0.5"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "0.5"
                    }
                  },
                  {
                    "#type": "prop_grid_cell_source",
                    "#prop": "cell.source",
                    "value": {
                      "#type": "expression",
                      "value": "[0, 1, 2]"
                    }
                  },
                  {
                    "#type": "prop_grid_random",
                    "#prop": "",
                    "random": false
                  },
                  {
                    "#type": "prop_grid_show_borders",
                    "#prop": "#showBorders",
                    "value": false
                  },
                  {
                    "#type": "prop_grid_cell_template",
                    "variable": "$cell",
                    "#prop": "cell.template",
                    "#callback": "$cell",
                    "body": [
                      {
                        "#type": "variable",
                        "name": "val",
                        "value": {
                          "#type": "expression",
                          "value": "value[$cell.data]"
                        }
                      },
                      {
                        "#type": "variable",
                        "name": "ox",
                        "value": {
                          "#type": "expression",
                          "value": "$cell.centerX - $cell.width/2 + 50"
                        }
                      },
                      {
                        "#type": "while_do_block",
                        "while": {
                          "#type": "expression",
                          "value": "val >= 5"
                        },
                        "do": [
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "ox"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_max_size",
                                "#prop": "",
                                "maxWidth": {
                                  "#type": "expression",
                                  "value": "0"
                                },
                                "maxHeight": {
                                  "#type": "expression",
                                  "value": "$cell.height - 10"
                                }
                              },
                              {
                                "#type": "prop_anchor",
                                "#prop": "anchor",
                                "x": {
                                  "#type": "expression",
                                  "value": "0"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "0.5"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "5.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}5.png"
                              }
                            ]
                          },
                          {
                            "#type": "variable",
                            "name": "val",
                            "value": {
                              "#type": "expression",
                              "value": "val - 5"
                            }
                          },
                          {
                            "#type": "variable",
                            "name": "ox",
                            "value": {
                              "#type": "expression",
                              "value": "ox + 57"
                            }
                          }
                        ]
                      },
                      {
                        "#type": "while_do_block",
                        "while": {
                          "#type": "expression",
                          "value": "val >= 1"
                        },
                        "do": [
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "ox"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_max_size",
                                "#prop": "",
                                "maxWidth": {
                                  "#type": "expression",
                                  "value": "0"
                                },
                                "maxHeight": {
                                  "#type": "expression",
                                  "value": "$cell.height - 10"
                                }
                              },
                              {
                                "#type": "prop_anchor",
                                "#prop": "anchor",
                                "x": {
                                  "#type": "expression",
                                  "value": "0"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "0.5"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "1.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}1.png"
                              }
                            ]
                          },
                          {
                            "#type": "variable",
                            "name": "val",
                            "value": {
                              "#type": "expression",
                              "value": "val -  1"
                            }
                          },
                          {
                            "#type": "variable",
                            "name": "ox",
                            "value": {
                              "#type": "expression",
                              "value": "ox +  9"
                            }
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ],
            "else": [
              {
                "#type": "grid_shape",
                "#props": [
                  {
                    "#type": "prop_grid_dimension",
                    "#prop": "",
                    "rows": {
                      "#type": "expression",
                      "value": "3"
                    },
                    "cols": {
                      "#type": "expression",
                      "value": "1"
                    }
                  },
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "490"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "240"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "516"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "160"
                    }
                  },
                  {
                    "#type": "prop_anchor",
                    "#prop": "anchor",
                    "x": {
                      "#type": "expression",
                      "value": "0.5"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "0.5"
                    }
                  },
                  {
                    "#type": "prop_grid_cell_source",
                    "#prop": "cell.source",
                    "value": {
                      "#type": "expression",
                      "value": "[0, 1, 2]"
                    }
                  },
                  {
                    "#type": "prop_grid_random",
                    "#prop": "",
                    "random": false
                  },
                  {
                    "#type": "prop_grid_show_borders",
                    "#prop": "#showBorders",
                    "value": false
                  },
                  {
                    "#type": "prop_grid_cell_template",
                    "variable": "$cell",
                    "#prop": "cell.template",
                    "#callback": "$cell",
                    "body": [
                      {
                        "#type": "variable",
                        "name": "i",
                        "value": {
                          "#type": "expression",
                          "value": "0"
                        }
                      },
                      {
                        "#type": "variable",
                        "name": "str",
                        "value": {
                          "#type": "expression",
                          "value": "''"
                        }
                      },
                      {
                        "#type": "while_do_block",
                        "while": {
                          "#type": "expression",
                          "value": "i < value[$cell.data]"
                        },
                        "do": [
                          {
                            "#type": "variable",
                            "name": "str",
                            "value": {
                              "#type": "expression",
                              "value": "str + '*'"
                            }
                          },
                          {
                            "#type": "variable",
                            "name": "i",
                            "value": {
                              "#type": "expression",
                              "value": "i+1"
                            }
                          }
                        ]
                      },
                      {
                        "#type": "list_shape",
                        "#props": [
                          {
                            "#type": "prop_list_direction",
                            "#prop": "",
                            "dir": "horizontal"
                          },
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_anchor",
                            "#prop": "anchor",
                            "x": {
                              "#type": "expression",
                              "value": "0.5"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "0.5"
                            }
                          }
                        ],
                        "items": [
                          {
                            "#type": "json",
                            "#props": [
                              {
                                "#type": "prop_list_align",
                                "#prop": "",
                                "align": "middle"
                              },
                              {
                                "#type": "prop_list_item_source",
                                "#prop": "",
                                "source": {
                                  "#type": "expression",
                                  "value": "str"
                                }
                              }
                            ],
                            "template": {
                              "#callback": "$item",
                              "variable": "$item",
                              "body": [
                                {
                                  "#type": "image_shape",
                                  "#props": [
                                    {
                                      "#type": "prop_max_size",
                                      "#prop": "",
                                      "maxWidth": {
                                        "#type": "expression",
                                        "value": "$cell.width/10 -5"
                                      },
                                      "maxHeight": {
                                        "#type": "expression",
                                        "value": "$cell.height - 10"
                                      }
                                    },
                                    {
                                      "#type": "prop_image_key",
                                      "#prop": "",
                                      "key": "${it}_${$cell.data}.png"
                                    },
                                    {
                                      "#type": "prop_image_src",
                                      "#prop": "",
                                      "src": "${image_path}${it}_${$cell.data}.png"
                                    }
                                  ]
                                }
                              ]
                            }
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "load",
        "value": {
          "#type": "custom_image_list",
          "link": "${image_path}",
          "images": "table${type}|${it}_0|${it}_1|${it}_2|ver1|ver2|ver3|ver4|ver5|1|5"
        }
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 1: Study the graph given. Pay attention to the data and title.</u></br></br>\n<center>"
        ]
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "type == 2"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<style>\n  td{text-align: center; vertical-align: bottom; width: 300px; height: 210px}\nimg{margin: 5px;}\n  </style>\n\n<table style='background-image: url(\"@sprite.src(table2)\"); width: 580px; height: 210px;'>\n  <tr>\n  <td><img src='@sprite.src(ver${color[0]})' style='height: ${value[0]*19.5}px; max-width: 39px;'/></td>\n  <td><img src='@sprite.src(ver${color[1]})' style='height: ${value[1]*19.5}px; max-width: 39px;'/></td>\n  <td><img src='@sprite.src(ver${color[2]})' style='height: ${value[2]*19.5}px; max-width: 39px;'/></td>\n</tr>\n<tr>\n    </table>\n\n\n<table style='width: 580px; height: 50px;'>\n  <tr>\n    <td style='height: 50px'><img src='@sprite.src(${it}_0)' style='max-height: 50px;'/></td>\n<td style='height: 50px'><img src='@sprite.src(${it}_1)' style='max-height: 50px;'/></td>\n<td style='height: 50px'><img src='@sprite.src(${it}_2)' style='max-height: 50px;'/></td>\n</tr>\n</table>"
            ]
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "type == 1"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<style>\n  td{text-align: center; vertical-align: bottom;}\nimg{margin: 5px; max-height: 40px}\n  </style>\n\n<table style='background-image: url(\"@sprite.src(table1)\"); width: 696px; height: 169px;'>"
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "i < 3"
                },
                "do": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<tr><td style='width: 180px'><img src='@sprite.src(${it}_${i})'/></td>\n<td>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "val",
                    "value": {
                      "#type": "expression",
                      "value": "value[i]"
                    }
                  },
                  {
                    "#type": "while_do_block",
                    "while": {
                      "#type": "expression",
                      "value": "val >= 5"
                    },
                    "do": [
                      {
                        "#type": "func",
                        "name": "addPartialExplanation",
                        "args": [
                          "<img src='@sprite.src(5)'/>"
                        ]
                      },
                      {
                        "#type": "variable",
                        "name": "val",
                        "value": {
                          "#type": "expression",
                          "value": "val - 5"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "while_do_block",
                    "while": {
                      "#type": "expression",
                      "value": "val  >= 1"
                    },
                    "do": [
                      {
                        "#type": "func",
                        "name": "addPartialExplanation",
                        "args": [
                          "<img src='@sprite.src(1)'/>"
                        ]
                      },
                      {
                        "#type": "variable",
                        "name": "val",
                        "value": {
                          "#type": "expression",
                          "value": "val - 1"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "</td></tr>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "i",
                    "value": {
                      "#type": "expression",
                      "value": "i+1"
                    }
                  }
                ]
              },
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</table>"
                ]
              }
            ],
            "else": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<style>\n  td{text-align: center; vertical-align: bottom;}\nimg{margin: 5px; max-height: 40px}\n  </style>\n\n<table style='background-image: url(\"@sprite.src(table3)\"); width: 696px; height: 169px;'>"
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "i < 3"
                },
                "do": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<tr><td style='width: 180px'><img src='@sprite.src(${it}_${i})'/></td>\n<td>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "j",
                    "value": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "while_do_block",
                    "while": {
                      "#type": "expression",
                      "value": "j < value[i]"
                    },
                    "do": [
                      {
                        "#type": "func",
                        "name": "addPartialExplanation",
                        "args": [
                          "<img style='max-width: 50px' src='@sprite.src(${it}_${i})'/>"
                        ]
                      },
                      {
                        "#type": "variable",
                        "name": "j",
                        "value": {
                          "#type": "expression",
                          "value": "j+1"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "</td></tr>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "i",
                    "value": {
                      "#type": "expression",
                      "value": "i+1"
                    }
                  }
                ]
              },
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</table>"
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</center>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 2: Now let's compare between the question and the graph.</u></br></br>\n<center>"
        ]
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "type == 2"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<style>\n  td{text-align: center; vertical-align: bottom; width: 300px; height: 210px}\nimg{margin: 5px;}\n  </style>\n\n<table style='background-image: url(\"@sprite.src(table2)\"); width: 580px; height: 210px;'>\n  <tr>\n  <td><img src='@sprite.src(ver${color[0]})' style='height: ${value[0]*19.5}px; max-width: 39px;'/></td>\n  <td><img src='@sprite.src(ver${color[1]})' style='height: ${value[1]*19.5}px; max-width: 39px;'/></td>\n  <td><img src='@sprite.src(ver${color[2]})' style='height: ${value[2]*19.5}px; max-width: 39px;'/></td>\n</tr>\n<tr>\n    </table>\n\n\n<table style='width: 580px; height: 50px;'>\n  <tr>\n    <td style='height: 50px'><img src='@sprite.src(${it}_0)' style='max-height: 50px;'/></td>\n<td style='height: 50px'><img src='@sprite.src(${it}_1)' style='max-height: 50px;'/></td>\n<td style='height: 50px'><img src='@sprite.src(${it}_2)' style='max-height: 50px;'/></td>\n</tr>\n</table>"
            ]
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "type == 1"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<style>\n  td{text-align: center; vertical-align: bottom;}\nimg{margin: 5px; max-height: 40px}\n  </style>\n\n<table style='background-image: url(\"@sprite.src(table1)\"); width: 696px; height: 169px;'>"
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "i < 3"
                },
                "do": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<tr><td style='width: 180px'><img src='@sprite.src(${it}_${i})'/></td>\n<td>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "val",
                    "value": {
                      "#type": "expression",
                      "value": "value[i]"
                    }
                  },
                  {
                    "#type": "while_do_block",
                    "while": {
                      "#type": "expression",
                      "value": "val >= 5"
                    },
                    "do": [
                      {
                        "#type": "func",
                        "name": "addPartialExplanation",
                        "args": [
                          "<img src='@sprite.src(5)'/>"
                        ]
                      },
                      {
                        "#type": "variable",
                        "name": "val",
                        "value": {
                          "#type": "expression",
                          "value": "val - 5"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "while_do_block",
                    "while": {
                      "#type": "expression",
                      "value": "val  >= 1"
                    },
                    "do": [
                      {
                        "#type": "func",
                        "name": "addPartialExplanation",
                        "args": [
                          "<img src='@sprite.src(1)'/>"
                        ]
                      },
                      {
                        "#type": "variable",
                        "name": "val",
                        "value": {
                          "#type": "expression",
                          "value": "val - 1"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "</td></tr>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "i",
                    "value": {
                      "#type": "expression",
                      "value": "i+1"
                    }
                  }
                ]
              },
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</table>"
                ]
              }
            ],
            "else": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<style>\n  td{text-align: center; vertical-align: bottom;}\nimg{margin: 5px; max-height: 40px}\n  </style>\n\n<table style='background-image: url(\"@sprite.src(table3)\"); width: 696px; height: 169px;'>"
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "i < 3"
                },
                "do": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<tr><td style='width: 180px'><img src='@sprite.src(${it}_${i})'/></td>\n<td>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "j",
                    "value": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "while_do_block",
                    "while": {
                      "#type": "expression",
                      "value": "j < value[i]"
                    },
                    "do": [
                      {
                        "#type": "func",
                        "name": "addPartialExplanation",
                        "args": [
                          "<img style='max-width: 50px' src='@sprite.src(${it}_${i})'/>"
                        ]
                      },
                      {
                        "#type": "variable",
                        "name": "j",
                        "value": {
                          "#type": "expression",
                          "value": "j+1"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "</td></tr>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "i",
                    "value": {
                      "#type": "expression",
                      "value": "i+1"
                    }
                  }
                ]
              },
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</table>"
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</center>\n</br>\n<span style='background: rgb(250, 250, 250, 0.5)'>\n  Question focus on <b>${group[g]}</b> and favourite <b>${item[it1]}s</b>.\n</br>\nGraph focus on <b>${place[p]}</b> and favourite <b>${item[it]}</b>.\n</span>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 3: Finally select if the question can gather the data.</u></br></br>\n<center>"
        ]
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "type == 2"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<style>\n  td{text-align: center; vertical-align: bottom; width: 300px; height: 210px}\nimg{margin: 5px;}\n  </style>\n\n<table style='background-image: url(\"@sprite.src(table2)\"); width: 580px; height: 210px;'>\n  <tr>\n  <td><img src='@sprite.src(ver${color[0]})' style='height: ${value[0]*19.5}px; max-width: 39px;'/></td>\n  <td><img src='@sprite.src(ver${color[1]})' style='height: ${value[1]*19.5}px; max-width: 39px;'/></td>\n  <td><img src='@sprite.src(ver${color[2]})' style='height: ${value[2]*19.5}px; max-width: 39px;'/></td>\n</tr>\n<tr>\n    </table>\n\n\n<table style='width: 580px; height: 50px;'>\n  <tr>\n    <td style='height: 50px'><img src='@sprite.src(${it}_0)' style='max-height: 50px;'/></td>\n<td style='height: 50px'><img src='@sprite.src(${it}_1)' style='max-height: 50px;'/></td>\n<td style='height: 50px'><img src='@sprite.src(${it}_2)' style='max-height: 50px;'/></td>\n</tr>\n</table>"
            ]
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "type == 1"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<style>\n  td{text-align: center; vertical-align: bottom;}\nimg{margin: 5px; max-height: 40px}\n  </style>\n\n<table style='background-image: url(\"@sprite.src(table1)\"); width: 696px; height: 169px;'>"
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "i < 3"
                },
                "do": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<tr><td style='width: 180px'><img src='@sprite.src(${it}_${i})'/></td>\n<td>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "val",
                    "value": {
                      "#type": "expression",
                      "value": "value[i]"
                    }
                  },
                  {
                    "#type": "while_do_block",
                    "while": {
                      "#type": "expression",
                      "value": "val >= 5"
                    },
                    "do": [
                      {
                        "#type": "func",
                        "name": "addPartialExplanation",
                        "args": [
                          "<img src='@sprite.src(5)'/>"
                        ]
                      },
                      {
                        "#type": "variable",
                        "name": "val",
                        "value": {
                          "#type": "expression",
                          "value": "val - 5"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "while_do_block",
                    "while": {
                      "#type": "expression",
                      "value": "val  >= 1"
                    },
                    "do": [
                      {
                        "#type": "func",
                        "name": "addPartialExplanation",
                        "args": [
                          "<img src='@sprite.src(1)'/>"
                        ]
                      },
                      {
                        "#type": "variable",
                        "name": "val",
                        "value": {
                          "#type": "expression",
                          "value": "val - 1"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "</td></tr>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "i",
                    "value": {
                      "#type": "expression",
                      "value": "i+1"
                    }
                  }
                ]
              },
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</table>"
                ]
              }
            ],
            "else": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<style>\n  td{text-align: center; vertical-align: bottom;}\nimg{margin: 5px; max-height: 40px}\n  </style>\n\n<table style='background-image: url(\"@sprite.src(table3)\"); width: 696px; height: 169px;'>"
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "i < 3"
                },
                "do": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<tr><td style='width: 180px'><img src='@sprite.src(${it}_${i})'/></td>\n<td>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "j",
                    "value": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "while_do_block",
                    "while": {
                      "#type": "expression",
                      "value": "j < value[i]"
                    },
                    "do": [
                      {
                        "#type": "func",
                        "name": "addPartialExplanation",
                        "args": [
                          "<img style='max-width: 50px' src='@sprite.src(${it}_${i})'/>"
                        ]
                      },
                      {
                        "#type": "variable",
                        "name": "j",
                        "value": {
                          "#type": "expression",
                          "value": "j+1"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "</td></tr>"
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "i",
                    "value": {
                      "#type": "expression",
                      "value": "i+1"
                    }
                  }
                ]
              },
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</table>"
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</center>\n</br>\n<span style='background: rgb(250, 250, 250, 0.5)'>\n  Question focus on <b>${group[g]}</b> and favourite <b>${item[it1]}s</b>.\n</br>\nGraph focus on <b>${place[p]}</b> and favourite <b>${item[it]}</b>.</br></br>\n  \n<b>${answer == 1 ? 'Yes': 'No'}</b>, the question ${answer == 1 ? 'can':'cannot'} help to gather the data.\n</span>"
        ]
      }
    ]
  },
  {
    "#type": "question_part",
    "index": "1",
    "contents": []
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="0" y="0">
    <field name="name">Y2.SP.DA.GD.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y2.SP.DA.GD.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="YepMQT@2uwp2rRPvmnRg1">
                                    <field name="name">rank</field>
                                    <value name="value">
                                      <block type="expression" id="UxgQ^Y5]rb}vpw[D=l-!">
                                        <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="variable" id="CdbNe%m}OqW9oK3nÔ,">
                                        <field name="name">sex</field>
                                        <value name="value">
                                          <block type="random_one" id="`Cz$z@1vy~36JXGObh^8T">
                                            <value name="items">
                                              <block type="expression" id="vVf.vaAopsH^ks0a[=9X">
                                                <field name="value">['his', 'her']</field>
                                              </block>
                                            </value>
                                          </block>
                                        </value>
                                        <next>
                                          <block type="if_then_else_block" id="(2IDXnP8yna81_K2ZVRL">
                                            <value name="if">
                                              <block type="expression" id="+ZQ?.8dCPmyf3YXV$,@2O">
                                                <field name="value">sex == 'his'</field>
                                              </block>
                                            </value>
                                            <statement name="then">
                                              <block type="variable" id="cMxSlEV0yb/4h#4OrgnJ">
                                                <field name="name">list_name</field>
                                                <value name="value">
                                                  <block type="string_array" id="{wIU5%)gEoC{@1Q7v%.@2/">
                                                    <field name="items">David|Johnny|Peter|Paul|Michael|Bob|Stephen|Angelo|Boon|Kavin|Angus|Luke|Jason|Mitch|Jeremy|John</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </statement>
                                            <statement name="else">
                                              <block type="variable" id="7.Qg@1D}Xch?b#ncIvS`Y">
                                                <field name="name">list_name</field>
                                                <value name="value">
                                                  <block type="string_array" id="n,KHe#6y$4m3IpJZxy)~">
                                                    <field name="items">Mary|Jane|Lisa|Rose|Olivia|Nina|Sara|Janette|Isabelle|Angela|Bessie|Susan|Francine|Brigitta|Julie</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </statement>
                                            <next>
                                              <block type="variable" id="?oic?z~8]}^9c#7hJx1y">
                                                <field name="name">item</field>
                                                <value name="value">
                                                  <block type="string_array" id="C:~Oq3ABBsW^`|$dO@1gf">
                                                    <field name="items">pet|sport|colour|mode of transport|food|book|lolly|shape</field>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="G@1,aZn|5v_dKXur}4$;6">
                                                    <field name="name">group</field>
                                                    <value name="value">
                                                      <block type="string_array" id="#FC@2,4CMwYZ7ZUoTOk;#">
                                                        <field name="items">schoolmates|classmates|neighbours|swim team friends|soccer teammates|art club friends|playgroup friends</field>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id="xN5#E:C~(_SupbLactVW">
                                                        <field name="name">place</field>
                                                        <value name="value">
                                                          <block type="string_array" id="uXC=[(DwIfK)!zhe[T~p">
                                                            <field name="items">school|class|neighbourhood|swim team|soccer team|art club|playgroup</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="/$is3c~X7erwuhOgE.6K">
                                                            <field name="name">name</field>
                                                            <value name="value">
                                                              <block type="random_one" id="9d4YQ`J^(;C=#J`vuEg`">
                                                                <value name="items">
                                                                  <block type="expression" id=";DpfdabTZKhvE@2HA3.j.">
                                                                    <field name="value">list_name</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="variable" id=".!Zp]Hn-F7e@221FJ~NWx">
                                                                <field name="name">value</field>
                                                                <value name="value">
                                                                  <block type="random_many" id="/ZPYYvo@2nfOno!p$DE$R">
                                                                    <value name="count">
                                                                      <block type="expression" id="QI[.Bt,xZjkrXcn4L-U$">
                                                                        <field name="value">3</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="items">
                                                                      <block type="func_array_of_number" id="5}bR8;yrf^#$Qx@2|;@1m)" inline="true">
                                                                        <value name="items">
                                                                          <block type="expression" id="yCu?RMV3@1Ic?/rtH+2Z6">
                                                                            <field name="value">9</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="from">
                                                                          <block type="expression" id="cH{O??F@1VKt`(ALt_C,T">
                                                                            <field name="value">1</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="variable" id="%.a`q#cjV_uUq#$FbW9(">
                                                                    <field name="name">it1</field>
                                                                    <value name="value">
                                                                      <block type="random_number" id="^tl[uQVmbXct8yXJ#@1e8">
                                                                        <value name="min">
                                                                          <block type="expression" id="ZY#$VLbPyZXFq8kMEhqj">
                                                                            <field name="value">0</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="max">
                                                                          <block type="expression" id="cqayHp.C/^+I~Cp$bQ1z">
                                                                            <field name="value">7</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="variable" id="x|3LgAp.;CTT+}iD,0Tq">
                                                                        <field name="name">g</field>
                                                                        <value name="value">
                                                                          <block type="random_number" id="e1rI?LV!Q:5%ta]hAWh:">
                                                                            <value name="min">
                                                                              <block type="expression" id="Z6gpivcRS(~vTu.K!THs">
                                                                                <field name="value">0</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="max">
                                                                              <block type="expression" id="cV@1p~WJ8-o#V8)12B9F1">
                                                                                <field name="value">6</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="nf|~dd7j:KVMhp:FeG#U">
                                                                            <field name="name">answer</field>
                                                                            <value name="value">
                                                                              <block type="random_number" id="Z)c:I1)~ar7|e,@1_$V_9">
                                                                                <value name="min">
                                                                                  <block type="expression" id="YaW3#95BD~`O!65g$e5A">
                                                                                    <field name="value">0</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="max">
                                                                                  <block type="expression" id="`K?]fpQ]p-@2M#t(3.`0!">
                                                                                    <field name="value">1</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="if_then_else_block" id="s2+P0EcMY0|pg3taG44Y">
                                                                                <value name="if">
                                                                                  <block type="expression" id="w[4vPgoSr+neauD=L.Bw">
                                                                                    <field name="value">answer == 1</field>
                                                                                  </block>
                                                                                </value>
                                                                                <statement name="then">
                                                                                  <block type="variable" id="X5zsovZPu/;+J3G9L:Vw">
                                                                                    <field name="name">p</field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="{hr6DqfVN7salAFX#!#J">
                                                                                        <field name="value">g</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="variable" id="D3[+-cOrj.u2XndJB)S$">
                                                                                        <field name="name">it</field>
                                                                                        <value name="value">
                                                                                          <block type="expression" id="Z]mVYZ~oe[4Wz{wa?2?f">
                                                                                            <field name="value">it1</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="func_add_answer" id="=@1y^6GR~9ILi8HD5~YJ3" inline="true">
                                                                                            <value name="value">
                                                                                              <block type="expression" id="Dk}Qj@1A+b)xYY}P3ltJZ">
                                                                                                <field name="value">11</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <statement name="else">
                                                                                  <block type="do_while_block" id="ZxfRK}W!0^,bL|pQh`R6">
                                                                                    <statement name="do">
                                                                                      <block type="variable" id="F%#+WMO+Is?|6,2gwy~@1">
                                                                                        <field name="name">p</field>
                                                                                        <value name="value">
                                                                                          <block type="random_number" id="Z7rgiA1Y$:l8XmTZW71[">
                                                                                            <value name="min">
                                                                                              <block type="expression" id="~25EQ_Fclb3O-Hisq;=4">
                                                                                                <field name="value">0</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="max">
                                                                                              <block type="expression" id=")Gj9q#b%#,Z_$E!Ulw(,">
                                                                                                <field name="value">6</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="variable" id="!h}n_s{uUKoHD@2idCx.A">
                                                                                            <field name="name">it</field>
                                                                                            <value name="value">
                                                                                              <block type="random_number" id="[Km^Onc(n5I+lOyqx8kj">
                                                                                                <value name="min">
                                                                                                  <block type="expression" id="rcnNIILq{y|}LW15rfW6">
                                                                                                    <field name="value">0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="max">
                                                                                                  <block type="expression" id="bt!Mq[0m.e6:0T8IeBzC">
                                                                                                    <field name="value">7</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <value name="while">
                                                                                      <block type="expression" id="vB:[z9t1BmbA@2DEO~,3.">
                                                                                        <field name="value">it == it1 || p == g</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="func_add_answer" id="k/DMq5Nj:#_U|j%=DEt%" inline="true">
                                                                                        <value name="value">
                                                                                          <block type="expression" id="Wn]hJT9=LPxO=tcoX,Lu">
                                                                                            <field name="value">10</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="variable" id="eB[;KL6u7wtaQ?#Krczx">
                                                                                    <field name="name">type</field>
                                                                                    <value name="value">
                                                                                      <block type="random_number" id="|hJh55`ii5.LZpO|bZ)g">
                                                                                        <value name="min">
                                                                                          <block type="expression" id="H{+ly%[gmHeYG;3zNC9d">
                                                                                            <field name="value">1</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="max">
                                                                                          <block type="expression" id=":N6s5-Jrg]TRv+^)AW.d">
                                                                                            <field name="value">3</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                                                        <statement name="#props">
                                                                                          <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                                                                <field name="value">400</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                                                                <field name="value">60</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_anchor" id="O.v@2mKn@1K#+tF;|}C}xI">
                                                                                                <field name="#prop">anchor</field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="+3%GU[X3l%%X`Jv1QL7@2">
                                                                                                    <field name="value">0.5</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="Kzp23HjHF7k]A-y1BJPm">
                                                                                                    <field name="value">0.5</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="contents">
                                                                                                      <block type="string_value" id="#6btV9p,PGV}wMx.Uï¿½">
                                                                                                        <field name="value">${name} asked ${sex} ${group[g]} what were their favourite ${item[it1]}.
Did his question help to gather the data below?
                                                                                                        </field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                                                        <field name="base">text</field>
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="fontSize">
                                                                                                              <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                                                                <field name="value">32</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="fill">
                                                                                                                  <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                                                                    <field name="value">black</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="stroke">
                                                                                                                      <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                                                        <field name="value">white</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="strokeThickness">
                                                                                                                          <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                                                            <field name="value">2</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="grid_shape" id="7sGArBIz}!lIDgZ2,lYU">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_grid_dimension" id="Wf)QP6#%+,~VK,N?szLP">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="rows">
                                                                                                  <block type="expression" id="ma!4.0a~ZZv9ZD=j=J-Y">
                                                                                                    <field name="value">1</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="cols">
                                                                                                  <block type="expression" id="6,O=f2VRPJmIQOj(j5-1">
                                                                                                    <field name="value">2</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_position" id="4Sr:eWS-gEaA+H1E8b`G">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="K2WchsV{9qEZg,s+,:c_">
                                                                                                        <field name="value">400</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="@1B5vt@2qE6H841hHqHWFw">
                                                                                                        <field name="value">type == 2 ? 410 : 390</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_size" id="yt)i0!WyR,I!vs#S?`S{">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="width">
                                                                                                          <block type="expression" id="PTsR=WHHifib:Gi1!Jx7">
                                                                                                            <field name="value">600</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="height">
                                                                                                          <block type="expression" id="cL)2QB[c.~UFa1y?rRt_">
                                                                                                            <field name="value">50</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_anchor" id="V#~AM}=[qtX@1@1$q9f~#-">
                                                                                                            <field name="#prop">anchor</field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="7I^#P9#B@1APhCtMGu]=.">
                                                                                                                <field name="value">0.5</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="wG()73D,jjC2DwcZNWt?">
                                                                                                                <field name="value">0.5</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_grid_cell_source" id="p(v2};RgB3$|OB(!+XtC">
                                                                                                                <field name="#prop">cell.source</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="nv;[aQz##I4CKqbp3VhP">
                                                                                                                    <field name="value">[0, 1]</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_grid_random" id="17tzQJV:g@1w@1c;0gr9S0">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <field name="random">FALSE</field>
                                                                                                                    <next>
                                                                                                                      <block type="prop_grid_show_borders" id="i`sMYd@2jDXemkH3-Q6hC">
                                                                                                                        <field name="#prop">#showBorders</field>
                                                                                                                        <field name="value">FALSE</field>
                                                                                                                        <next>
                                                                                                                          <block type="prop_grid_cell_template_for" id="f,^7sU``E}nM}Fu6DAek">
                                                                                                                            <field name="variable">$cell</field>
                                                                                                                            <field name="condition">$cell.data == 0</field>
                                                                                                                            <field name="#prop">cell.templates[]</field>
                                                                                                                            <field name="#callback">$cell</field>
                                                                                                                            <statement name="body">
                                                                                                                              <block type="choice_custom_shape" id="cwADT_X`w2-ROo~+Q+^P">
                                                                                                                                <field name="action">click-one</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="zON}!meJy!mlY+IE8IZE">
                                                                                                                                    <field name="value">10</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <statement name="template">
                                                                                                                                  <block type="image_shape" id="=yZkBY~q=:JA-@22hc+bW">
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_position" id="Y5Aye.P]qFZPf@1qzFAOf">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="expression" id="mr0Jg6YtW!]@2jp:jpp{B">
                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id="wGaA#njNI~YR=,:,sbp-">
                                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_key" id="v{029.u@1l|`fLIPV5;!?">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="key">
                                                                                                                                              <block type="string_value" id="XMPWJqP2)wXrul|#z;)Q">
                                                                                                                                                <field name="value">no.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_src" id=".8SHZ_[hSU(%1}Uk#[}i">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="src">
                                                                                                                                                  <block type="string_value" id="1e05Ll:Ij!sd44Z^Vi{W">
                                                                                                                                                    <field name="value">${image_path}no.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="prop_grid_cell_template_for" id="cdE@2:Q$vh:_^I)m8R}{x">
                                                                                                                                <field name="variable">$cell</field>
                                                                                                                                <field name="condition">$cell.data == 1</field>
                                                                                                                                <field name="#prop">cell.templates[]</field>
                                                                                                                                <field name="#callback">$cell</field>
                                                                                                                                <statement name="body">
                                                                                                                                  <block type="choice_custom_shape" id="XQK:]1FKXG8[Z:zwmyI$">
                                                                                                                                    <field name="action">click-one</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="DC~hXs;wXXKq;,ncu[cQ">
                                                                                                                                        <field name="value">11</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <statement name="template">
                                                                                                                                      <block type="image_shape" id="aI|=_1DlXFHgCbob[(X]">
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_position" id="k~ZR5uP{0xCTZ.=UZUhA">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id=".C)b3dUqoC!6kAuQk)AI">
                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="g^%Dkj0cm~](-igS;jsk">
                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_key" id="v3Q/lV4FET3b2S+[n[qt">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="key">
                                                                                                                                                  <block type="string_value" id="87LM|MshC=Mb/gg@2:o(u">
                                                                                                                                                    <field name="value">yes.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_image_src" id="@1mKa@1t(gZ+lNcBwcSLL@2">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="src">
                                                                                                                                                      <block type="string_value" id="(IUi8O`5;dv^|IUY7Xf-">
                                                                                                                                                        <field name="value">${image_path}yes.png</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="text_shape" id="nGAèkWg|a[cpkQx6i`">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_position" id="|/|7k[!f64w=;m4P3SR6">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="2wz%RAWo^^Y0s]Xtbfu@1">
                                                                                                        <field name="value">400</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="mI7EFsjKQfhL3mTcEZmT">
                                                                                                        <field name="value">130</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_anchor" id="5UrRLIR5,on%!Z!1y.ph">
                                                                                                        <field name="#prop">anchor</field>
                                                                                                        <value name="x">
                                                                                                          <block type="expression" id="h.9aSKj#fM`IaDluDd6g">
                                                                                                            <field name="value">0.5</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id="P@1b0VO)Dp6#ip#}Yx/K2">
                                                                                                            <field name="value">0.5</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_contents" id="!C}-u(Lx_59jgJxU%QYP">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="contents">
                                                                                                              <block type="string_value" id="1CH`@2z:byg5=4M3_:!BP">
                                                                                                                <field name="value">Most favourite ${item[it]} in ${place[p]}</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style" id="{}mhRy@2wCwb:@1zA@2vaHT">
                                                                                                                <field name="base">text</field>
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_text_style_font_size" id="IHU@1d{HQ,v@1DW5g,S|dy">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="fontSize">
                                                                                                                      <block type="expression" id="3Z_.OTmX2iSY]SpaXNsy">
                                                                                                                        <field name="value">32</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_fill" id="Vx6vDKM@1zG46jZhb:b;_">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="fill">
                                                                                                                          <block type="string_value" id="5~Th95Fx%PnUPXV.vj`k">
                                                                                                                            <field name="value">black</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_stroke" id="5t@2.nusq[Nbm5C9=3$5u">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="stroke">
                                                                                                                              <block type="string_value" id="$Xa7|y42OQ:t,80IZ34l">
                                                                                                                                <field name="value">white</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_stroke_thickness" id="tsa!RmRjH0MVb-YVjj#v">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="strokeThickness">
                                                                                                                                  <block type="expression" id=";goU4mw5s3$qU)bK.%+Z">
                                                                                                                                    <field name="value">2</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="image_shape" id="I~v6F:Aqw|+x|DJDBABX">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_position" id="k[Y`B}iYZ-$=4-PL}Z#%">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="x">
                                                                                                          <block type="expression" id="AR#7!_yy1B~Vf(u{%|MK">
                                                                                                            <field name="value">400</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id="om?mR!nUr;whFeu/]zV{">
                                                                                                            <field name="value">240</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_size" id="cCQYE(lbeCFwNf2Edg$o">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="width">
                                                                                                              <block type="expression" id="/Sssc)`BS[Mlj-ZYo$zJ">
                                                                                                                <field name="value">0</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="height">
                                                                                                              <block type="expression" id="IsKu1iz+o%zEgsAR0[nG">
                                                                                                                <field name="value">0</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_image_key" id="0BMio17~SxdiLt9Z:kWm">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="key">
                                                                                                                  <block type="string_value" id="0U(!S.U1@1k+r(Zv^;8;z">
                                                                                                                    <field name="value">table${type}.png</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_image_src" id="g-Mv{6]oz}6y(Dc|CyJX">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="src">
                                                                                                                      <block type="string_value" id="nky@2i}N.~a;DnikVw2wL">
                                                                                                                        <field name="value">${image_path}table${type}.png</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="if_then_else_block" id="S,mEL`M+.^{!h3xK-g2S">
                                                                                                        <value name="if">
                                                                                                          <block type="expression" id="WUWkq@2VAY~=~R]nVW@2XS">
                                                                                                            <field name="value">type == 2</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <statement name="then">
                                                                                                          <block type="variable" id="x;[4~2/|Ny[z=f:jls)K">
                                                                                                            <field name="name">color</field>
                                                                                                            <value name="value">
                                                                                                              <block type="random_many" id="cr!uMn^-BEq-jyClFJUo">
                                                                                                                <value name="count">
                                                                                                                  <block type="expression" id="(4xehiWFdC;aDY?tw@28+">
                                                                                                                    <field name="value">3</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="items">
                                                                                                                  <block type="func_array_of_number" id="=UpfWHQ^wue6p-I1G(LD" inline="true">
                                                                                                                    <value name="items">
                                                                                                                      <block type="expression" id="vxiFZ$r{PN]3OWSp1sPj">
                                                                                                                        <field name="value">5</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="from">
                                                                                                                      <block type="expression" id="QlMjkF!(GJPbxjYHhU{7">
                                                                                                                        <field name="value">1</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="grid_shape" id="RmDdctMQH`R:@2br+(}gW">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_grid_dimension" id="lhtCL)kA+W8}r{q_q6V=">
                                                                                                                    <field name="#prop">35</field>
                                                                                                                    <value name="rows">
                                                                                                                      <block type="expression" id="yGx%#EJ7Bh_tiTL$#{@1N">
                                                                                                                        <field name="value">1</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="cols">
                                                                                                                      <block type="expression" id="?EoXHaBc,2[FMvdCN3GT">
                                                                                                                        <field name="value">3</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_position" id=",fOVgmOame~#r4jsqry/">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="Af;-_]Fy{-6}s#0=}c#:">
                                                                                                                            <field name="value">400</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="~h14^@1b`{jg3Mm[KvS45">
                                                                                                                            <field name="value">355</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_size" id="6/L7QRX~udx:#=$K?s@1,">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="width">
                                                                                                                              <block type="expression" id="X}xa[zDbOopW#$^x^+BM">
                                                                                                                                <field name="value">500</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="height">
                                                                                                                              <block type="expression" id="dFluyoFhTVuRiZ-il)y!">
                                                                                                                                <field name="value">30</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_anchor" id="Ta6C+]/d!ho+i,9iVC~Z">
                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="?Ij.NZFYF^=of68ZPMdT">
                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="LwLnWT5;]je4pIAf3e}}">
                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_grid_cell_source" id="nODZtV=Jfi{OO:TpX(8!">
                                                                                                                                    <field name="#prop">cell.source</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="l6e`R5t~4dv|!~]Sga@2@2">
                                                                                                                                        <field name="value">[0, 1, 2]</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_grid_random" id="1uM!s_};KK^RKkgVyR,`">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <field name="random">FALSE</field>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_grid_show_borders" id="5kK%zm)-0dWq_KjeCwCl">
                                                                                                                                            <field name="#prop">#showBorders</field>
                                                                                                                                            <field name="value">FALSE</field>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_grid_cell_template" id="zkS%YLGVQV#9AfF.[:}I">
                                                                                                                                                <field name="variable">$cell</field>
                                                                                                                                                <field name="#prop">cell.template</field>
                                                                                                                                                <field name="#callback">$cell</field>
                                                                                                                                                <statement name="body">
                                                                                                                                                  <block type="image_shape" id="//0n_SLPi(uwgBb.Q^%@2">
                                                                                                                                                    <statement name="#props">
                                                                                                                                                      <block type="prop_position" id="8Ky.ZcToPimX$4wHKIi%">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="x">
                                                                                                                                                          <block type="expression" id="w`}I3l:Nz|~|FOLSDcG[">
                                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="y">
                                                                                                                                                          <block type="expression" id="3$S!5WFU/H(`;8iz=Q}E">
                                                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_max_size" id="Vd8On^yUCcAo?v)~z-m]">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="maxWidth">
                                                                                                                                                              <block type="expression" id="=PbJfB|XGiBxF(kww/o@2">
                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="maxHeight">
                                                                                                                                                              <block type="expression" id="d1ueH-jZpt7giSPdeC,Y">
                                                                                                                                                                <field name="value">$cell.height - 10</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_image_key" id=",%lD,nJ0U9X1Bs}@1_6)(">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="key">
                                                                                                                                                                  <block type="string_value" id="mC$b@2/[/Kd)WhhL@1k/Zz">
                                                                                                                                                                    <field name="value">${it}_${$cell.data}.png</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_image_src" id="SeK9^@2{MlCP~nx`7|AwX">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="src">
                                                                                                                                                                      <block type="string_value" id="1S|~C5k9;0^.b%c/@2CA@2">
                                                                                                                                                                        <field name="value">${image_path}${it}_${$cell.data}.png</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="grid_shape" id="Ub4AvYtgX3KfDQAObmOY">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_grid_dimension" id="S32LNPn]fC2z9OvP+t@1D">
                                                                                                                        <field name="#prop">35</field>
                                                                                                                        <value name="rows">
                                                                                                                          <block type="expression" id="qhV-5rD!sprX16`P4j?c">
                                                                                                                            <field name="value">1</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="cols">
                                                                                                                          <block type="expression" id="zBU)X6RYun.WwA`etoKk">
                                                                                                                            <field name="value">3</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_position" id="1Fvf+|G-Jt)|-.+L:@2S?">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="Dc0GxfwemS2A0=9M:T0x">
                                                                                                                                <field name="value">400</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="m6fbH]?j}9KX!giL@1C/t">
                                                                                                                                <field name="value">240</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_size" id="4:M@1BXYsStF_smQ@1GurA">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="width">
                                                                                                                                  <block type="expression" id="/2MQ6/N`=vb@2]fompt0(">
                                                                                                                                    <field name="value">500</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="height">
                                                                                                                                  <block type="expression" id="wBb9P9%g9f,Rf;l%_4cn">
                                                                                                                                    <field name="value">195</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_anchor" id="-r`S(?uK5zYCqUkT?I,T">
                                                                                                                                    <field name="#prop">anchor</field>
                                                                                                                                    <value name="x">
                                                                                                                                      <block type="expression" id="_Ix?a:wl[`1+[Vsn(|`d">
                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="y">
                                                                                                                                      <block type="expression" id="(v)8Lz|kMwRvy41a];~P">
                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_grid_cell_source" id="8%;N1+S`LgrsSqZ:W?2Q">
                                                                                                                                        <field name="#prop">cell.source</field>
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="expression" id="bwlvec@2-9FizPVL%Kj.Y">
                                                                                                                                            <field name="value">[0, 1, 2]</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_grid_random" id="je^Q7ywN}m.Fy=R9gT{e">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <field name="random">FALSE</field>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_grid_show_borders" id="T_FO=l$I,1U#tiv{miE`">
                                                                                                                                                <field name="#prop">#showBorders</field>
                                                                                                                                                <field name="value">FALSE</field>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_grid_cell_template" id=";x~Dt7{@2}bbf+OBO?8Q~">
                                                                                                                                                    <field name="variable">$cell</field>
                                                                                                                                                    <field name="#prop">cell.template</field>
                                                                                                                                                    <field name="#callback">$cell</field>
                                                                                                                                                    <statement name="body">
                                                                                                                                                      <block type="image_shape" id="#1bkc`TP^xR+L=V5-)}1">
                                                                                                                                                        <statement name="#props">
                                                                                                                                                          <block type="prop_position" id="7xg6d.y,Sr@1IK@23i@2=@15">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="x">
                                                                                                                                                              <block type="expression" id=";Q2UI@29ds/a=RaTMf[Y+">
                                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="y">
                                                                                                                                                              <block type="expression" id=")-yU]FjF[@1VuRtYx~kK]">
                                                                                                                                                                <field name="value">$cell.bottom</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_size" id="2w.Go@1?_be[e3C4og#TA">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="width">
                                                                                                                                                                  <block type="expression" id="BhDbI;MORHgi1T2-9b0F">
                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <value name="height">
                                                                                                                                                                  <block type="expression" id="q-D2nv3+4W9+KKFrew5J">
                                                                                                                                                                    <field name="value">195/10@2value[$cell.data]</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_anchor" id="?RGx~YQM-O3YP;pUhQ">
                                                                                                                                                                    <field name="#prop">anchor</field>
                                                                                                                                                                    <value name="x">
                                                                                                                                                                      <block type="expression" id="N@1G~p|afvv@1IMHle::H9">
                                                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <value name="y">
                                                                                                                                                                      <block type="expression" id="`@1?l20@2Nw%t{mRv;EgCA">
                                                                                                                                                                        <field name="value">1</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_image_key" id="F~!rDIRORy^D74;nP@1Y@2">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="key">
                                                                                                                                                                          <block type="string_value" id="NV={Bpt9}+m99Gkf,zA/">
                                                                                                                                                                            <field name="value">ver${color[$cell.data]}.png</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_image_src" id="7PXTF8CVoKHe}2(^1g2a">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="src">
                                                                                                                                                                              <block type="string_value" id="rkS7R1V@1`Z^Nd(@2_teM/">
                                                                                                                                                                                <field name="value">${image_path}ver${color[$cell.data]}.png</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <statement name="else">
                                                                                                          <block type="grid_shape" id="6tU?K^N8ec~A@1@1NwQ72N">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_grid_dimension" id="X4K:|9dKK%s9l4n5v@2lG">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="rows">
                                                                                                                  <block type="expression" id="[Ev!.bxnzjc|-33v.%7=">
                                                                                                                    <field name="value">3</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="cols">
                                                                                                                  <block type="expression" id="u$7]/V/!ZWM4(Va??@2({">
                                                                                                                    <field name="value">1</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_position" id="zLld84TfBg#!88]1,JD-">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="Cd6g{Y_P?:GyVyvQi1wJ">
                                                                                                                        <field name="value">145</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id=".F}E71+@1i)77!yQ`hmfu">
                                                                                                                        <field name="value">240</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_size" id="+o2CWr^.ohx)_bcIG53]">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="width">
                                                                                                                          <block type="expression" id="Z[x5iHG%KWC%KWQb}c-G">
                                                                                                                            <field name="value">180</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="height">
                                                                                                                          <block type="expression" id="@2E|Ob@1jF#gYtX8-%Iui@1">
                                                                                                                            <field name="value">160</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_anchor" id="1WJjXY^.h,t]eNgxaJ}q">
                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="DHx[vqs?.?nUD:AYPJWd">
                                                                                                                                <field name="value">0.5</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="e3D|RR7w!=Sufu):zV`c">
                                                                                                                                <field name="value">0.5</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_grid_cell_source" id="v8+Yu211Qg8fX9Mu4th`">
                                                                                                                                <field name="#prop">cell.source</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="~j@1,^liddf}oDg}E91^i">
                                                                                                                                    <field name="value">[0, 1, 2]</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_grid_random" id="}?kS#x[b)9OGb@1`UdJGV">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <field name="random">FALSE</field>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_grid_show_borders" id=";y^wk4y%[VWjXOj~vrQZ">
                                                                                                                                        <field name="#prop">#showBorders</field>
                                                                                                                                        <field name="value">FALSE</field>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_grid_cell_template" id="7^6]Kv:2yrZi`cooAW2@2">
                                                                                                                                            <field name="variable">$cell</field>
                                                                                                                                            <field name="#prop">cell.template</field>
                                                                                                                                            <field name="#callback">$cell</field>
                                                                                                                                            <statement name="body">
                                                                                                                                              <block type="image_shape" id="TPJEuGf4H8;aC~JU6r|m">
                                                                                                                                                <statement name="#props">
                                                                                                                                                  <block type="prop_position" id="v;VU0C0DGæ;]l]G{x{">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="x">
                                                                                                                                                      <block type="expression" id="fC,,IA@2:8S=V)ff=g#^I">
                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="y">
                                                                                                                                                      <block type="expression" id="oCrqBp2BBBg1Xh(pq4rn">
                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_max_size" id="20gtS04b:l:vi)[9|[[Y">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="maxWidth">
                                                                                                                                                          <block type="expression" id="/%+DeiLDyONkTPM^:6d`">
                                                                                                                                                            <field name="value">50</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="maxHeight">
                                                                                                                                                          <block type="expression" id="V0D~P0^6.yAi@1)_NK`?0">
                                                                                                                                                            <field name="value">$cell.height - 10</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_image_key" id="{uWEuC@1.%lqqV{cL`ewe">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="key">
                                                                                                                                                              <block type="string_value" id="7?oum,?P)AvZXiyui2Fm">
                                                                                                                                                                <field name="value">${it}_${$cell.data}.png</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_image_src" id="PbJJu~wG(KczJPm2h%f.">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="src">
                                                                                                                                                                  <block type="string_value" id="c9gyOB%CXVg:sjP%XPu%">
                                                                                                                                                                    <field name="value">${image_path}${it}_${$cell.data}.png</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="if_then_else_block" id="P#2,1_xdQq::5f8[aoNZ">
                                                                                                                <value name="if">
                                                                                                                  <block type="expression" id="UZWefy%l3J=B#X)v%@1ch">
                                                                                                                    <field name="value">type == 1</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <statement name="then">
                                                                                                                  <block type="grid_shape" id="aGb#%7K-}wQw3sN$Gi~c">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_grid_dimension" id="@1XS#^i#xhCx2NTBex~%g">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="rows">
                                                                                                                          <block type="expression" id="lVVDP|/%X)3D`@2Z64#:t">
                                                                                                                            <field name="value">3</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="cols">
                                                                                                                          <block type="expression" id="gDxD@2MxGHQgL@14x3x8;6">
                                                                                                                            <field name="value">1</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_position" id="p=.W^U:O?R0DP,BsjRMa">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id=",~DY?LD2qkb!0_F8FBp?">
                                                                                                                                <field name="value">490</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="HEJ.f(c!y.HGz5inFCcw">
                                                                                                                                <field name="value">240</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_size" id="=P6DvO8th|!h#`Hh]j$e">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="width">
                                                                                                                                  <block type="expression" id="iC]BgtYOAk3Y8tD]3lpu">
                                                                                                                                    <field name="value">516</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="height">
                                                                                                                                  <block type="expression" id="53l:mB3lDXy73h0Y(hdF">
                                                                                                                                    <field name="value">160</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_anchor" id="U^T#MMnB(eOkAFiPy._?">
                                                                                                                                    <field name="#prop">anchor</field>
                                                                                                                                    <value name="x">
                                                                                                                                      <block type="expression" id="RWK/U=CKa-Flr@2,+efTP">
                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="y">
                                                                                                                                      <block type="expression" id="Tik[Y0B@1)~B52sw!}js)">
                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_grid_cell_source" id="BTxlJI4`al@1.BA_bYm}a">
                                                                                                                                        <field name="#prop">cell.source</field>
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="expression" id="yCIUObOyFjktdL]Hiz99">
                                                                                                                                            <field name="value">[0, 1, 2]</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_grid_random" id="GS0F9r]Nm#j|KeGoKpYd">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <field name="random">FALSE</field>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_grid_show_borders" id="Ygo#n;qRVGs=/b#tRxVz">
                                                                                                                                                <field name="#prop">#showBorders</field>
                                                                                                                                                <field name="value">FALSE</field>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_grid_cell_template" id="Y$t/.n`O~$!aKcI57C;u">
                                                                                                                                                    <field name="variable">$cell</field>
                                                                                                                                                    <field name="#prop">cell.template</field>
                                                                                                                                                    <field name="#callback">$cell</field>
                                                                                                                                                    <statement name="body">
                                                                                                                                                      <block type="variable" id="-=9;`jEx.k/7:qAsZ0;U">
                                                                                                                                                        <field name="name">val</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="UDt{Fp9Nn_)Hj09t#1({">
                                                                                                                                                            <field name="value">value[$cell.data]</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="variable" id="{w)/^3uTqLY?WgYyle-d">
                                                                                                                                                            <field name="name">ox</field>
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id="vcJeC~4|mzY1=blj@23NE">
                                                                                                                                                                <field name="value">$cell.centerX - $cell.width/2 + 50</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="while_do_block" id=":GNgXg8Kb(Sdsgx{Nhex">
                                                                                                                                                                <value name="while">
                                                                                                                                                                  <block type="expression" id="Ee6_t2G%GPJndyO_LY,)">
                                                                                                                                                                    <field name="value">val &gt;= 5</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <statement name="do">
                                                                                                                                                                  <block type="image_shape" id="8?-2s4RvOjWFidRg1Vy_">
                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                      <block type="prop_position" id="E70603SC,-;Ix2V7}2ma">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="x">
                                                                                                                                                                          <block type="expression" id="_e.POK$.3.6wB3n/yo$x">
                                                                                                                                                                            <field name="value">ox</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <value name="y">
                                                                                                                                                                          <block type="expression" id="hl{]STtdaecL7f#J5b`/">
                                                                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_max_size" id="]OPBR-CjBf7d~K.:OxB4">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="maxWidth">
                                                                                                                                                                              <block type="expression" id="W^pBQAf4%T/[L)?(@1y0]">
                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="maxHeight">
                                                                                                                                                                              <block type="expression" id="JVG|!YK8caf3M9~mJ$_b">
                                                                                                                                                                                <field name="value">$cell.height - 10</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_anchor" id="4Rg!pDdvFYe4mb0PjwZp">
                                                                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                                                                <value name="x">
                                                                                                                                                                                  <block type="expression" id="Ph-1[2U{FHg[z2n:dE=E">
                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <value name="y">
                                                                                                                                                                                  <block type="expression" id="7I~]_TjupV)zp-8hlG,;">
                                                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_image_key" id="N$eK?0WxWG#{;s$kX5Wm">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="key">
                                                                                                                                                                                      <block type="string_value" id="UC+}7Rx|B!2N9n@1nt1[j">
                                                                                                                                                                                        <field name="value">5.png</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_image_src" id="i$U%=:ww,EB(o|K:hdaw">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="src">
                                                                                                                                                                                          <block type="string_value" id="%x#Xyb)!=Nis)L@25[_2J">
                                                                                                                                                                                            <field name="value">${image_path}5.png</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="variable" id="F5b%Mz/U6l8^AkK|L/h#">
                                                                                                                                                                        <field name="name">val</field>
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="expression" id="{dm.-2`v55R5gxi+:Cz!">
                                                                                                                                                                            <field name="value">val - 5</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="variable" id="?23{v(DT^r-lgac,S!Mo">
                                                                                                                                                                            <field name="name">ox</field>
                                                                                                                                                                            <value name="value">
                                                                                                                                                                              <block type="expression" id="Z,7sw0@1z@1cNB}A^Z!f,/">
                                                                                                                                                                                <field name="value">ox + 57</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="while_do_block" id="z~B5QVsU~#!UYj9v.Sp:">
                                                                                                                                                                    <value name="while">
                                                                                                                                                                      <block type="expression" id="`V~YA$9@1^QAK@1LCsh.hA">
                                                                                                                                                                        <field name="value">val &gt;= 1</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <statement name="do">
                                                                                                                                                                      <block type="image_shape" id="QFxyspRp0d5g|(B6R0cn">
                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                          <block type="prop_position" id="bl2P@1qQj[:2CbU.;Hb=V">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="x">
                                                                                                                                                                              <block type="expression" id="/Gh:IB~mZy[_wffWw#xT">
                                                                                                                                                                                <field name="value">ox</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="y">
                                                                                                                                                                              <block type="expression" id="wAB]Zq1`Dv+{M;X7mgHE">
                                                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_max_size" id="?/`5CMXOgYBd3-7N=cIB">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="maxWidth">
                                                                                                                                                                                  <block type="expression" id="7SyOTVf{@1hhNR`W/1{5M">
                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <value name="maxHeight">
                                                                                                                                                                                  <block type="expression" id="hxqbMKQo@2UJ5mQAtZu%h">
                                                                                                                                                                                    <field name="value">$cell.height - 10</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_anchor" id="Pu!WRxSEaNvff?RS}@2hM">
                                                                                                                                                                                    <field name="#prop">anchor</field>
                                                                                                                                                                                    <value name="x">
                                                                                                                                                                                      <block type="expression" id="E8{?nGf~V8R!-^,oHceg">
                                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <value name="y">
                                                                                                                                                                                      <block type="expression" id="UWwÈ~^9`rDsrUnN;n2">
                                                                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_image_key" id="canvP]zhzAm[Sq2ZjD=@1">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="key">
                                                                                                                                                                                          <block type="string_value" id="dfXqT,`}Jma]D-p)q/}j">
                                                                                                                                                                                            <field name="value">1.png</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_image_src" id="[-|EseTkcUw)tS0?iD?a">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <value name="src">
                                                                                                                                                                                              <block type="string_value" id="3B@1.I?C,v1WyQAqY}3Uv">
                                                                                                                                                                                                <field name="value">${image_path}1.png</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="variable" id="W#`%DSM^oOLfiV}J53dS">
                                                                                                                                                                            <field name="name">val</field>
                                                                                                                                                                            <value name="value">
                                                                                                                                                                              <block type="expression" id=",;,-?aeHsG8.ji23DKO_">
                                                                                                                                                                                <field name="value">val -  1</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="variable" id="Lre5?#9q6ZnyqG!X#,bS">
                                                                                                                                                                                <field name="name">ox</field>
                                                                                                                                                                                <value name="value">
                                                                                                                                                                                  <block type="expression" id="JT!~C;b:Sf9/!dVAW1Gz">
                                                                                                                                                                                    <field name="value">ox +  9</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <statement name="else">
                                                                                                                  <block type="grid_shape" id="@1rgNdnp`yt#b8N^^U9/J">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_grid_dimension" id="t%!!ig-79@2trb2`@1.)=2">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="rows">
                                                                                                                          <block type="expression" id=".O4BT?BtUe:EhOGMEXRv">
                                                                                                                            <field name="value">3</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="cols">
                                                                                                                          <block type="expression" id="iiZk[RsoE;eKE?0pgA!i">
                                                                                                                            <field name="value">1</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_position" id="/?u@1OLsuR8oZf[!hP{`u">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="Qfz%ap_3d7]2l8@12iPNe">
                                                                                                                                <field name="value">490</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="z|%Gy/u3#-@2fVqAf91dg">
                                                                                                                                <field name="value">240</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_size" id="Mo.bY3@15[mOq6BGq_D6+">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="width">
                                                                                                                                  <block type="expression" id="u}Qo9Wc.p[o4ILseJ3lh">
                                                                                                                                    <field name="value">516</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="height">
                                                                                                                                  <block type="expression" id=";fDvctAEAuu+hG5|y;;~">
                                                                                                                                    <field name="value">160</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_anchor" id="$m),{`[LnkEYsTR0b?X+">
                                                                                                                                    <field name="#prop">anchor</field>
                                                                                                                                    <value name="x">
                                                                                                                                      <block type="expression" id="D%v?+0MyeqKjbB~{|4K+">
                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="y">
                                                                                                                                      <block type="expression" id="sjs2J,8QzHIRPLe/ksJ1">
                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_grid_cell_source" id="(Uy2UCCzaXhy+OsK)|5F">
                                                                                                                                        <field name="#prop">cell.source</field>
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="expression" id="fN@1IDsQ5^t)OpiEaPgyg">
                                                                                                                                            <field name="value">[0, 1, 2]</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_grid_random" id="}2f34DIR[yAPV~ocx+6R">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <field name="random">FALSE</field>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_grid_show_borders" id="_0B-[e(,bm0@1R(=]oS8a">
                                                                                                                                                <field name="#prop">#showBorders</field>
                                                                                                                                                <field name="value">FALSE</field>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_grid_cell_template" id="5PU2Paq4np(b$@1!e=l1O">
                                                                                                                                                    <field name="variable">$cell</field>
                                                                                                                                                    <field name="#prop">cell.template</field>
                                                                                                                                                    <field name="#callback">$cell</field>
                                                                                                                                                    <statement name="body">
                                                                                                                                                      <block type="variable" id="[^Hr_Afq2V4/6!18~Wi,">
                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="%c!(bnriLO9(TQ_hECkg">
                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="variable" id="p]~WZ!p{F]H8nnHAB+Wz">
                                                                                                                                                            <field name="name">str</field>
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id="%-5/~kda09tK|kp$}7Zy">
                                                                                                                                                                <field name="value">''</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="while_do_block" id="u[F1suK{Sdl{l)KK$%={">
                                                                                                                                                                <value name="while">
                                                                                                                                                                  <block type="expression" id="W/@2s3P@2yy^^@2qSl-TQ|E">
                                                                                                                                                                    <field name="value">i &lt; value[$cell.data]</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <statement name="do">
                                                                                                                                                                  <block type="variable" id="tr2!go5IVC_hxp$_oGL1">
                                                                                                                                                                    <field name="name">str</field>
                                                                                                                                                                    <value name="value">
                                                                                                                                                                      <block type="expression" id="Ynn?uiXN1{!!?An_9.K:">
                                                                                                                                                                        <field name="value">str + '@2'</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="variable" id="~{w$?KiWK)hx/Ava5%!~">
                                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="expression" id="{[tQ].j+s[.k~xS6M5M9">
                                                                                                                                                                            <field name="value">i+1</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="list_shape" id="Ê|E#_(z[S@1Jk{V{u?!">
                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                      <block type="prop_list_direction" id="+G!|3V@21+C6tIn:7U+OZ">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <field name="dir">horizontal</field>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_position" id="P5sv8g26?Rw{7?xw?">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="x">
                                                                                                                                                                              <block type="expression" id="v=Zoo)07TUkwD[4fUQ#O">
                                                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="y">
                                                                                                                                                                              <block type="expression" id="J|2?3X58(;cTY$4~iP8i">
                                                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_anchor" id="sl%LL8q6|qhY%i3%6OAE">
                                                                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                                                                <value name="x">
                                                                                                                                                                                  <block type="expression" id="3^?1Ek!Y4Zlrkr96+WS!">
                                                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <value name="y">
                                                                                                                                                                                  <block type="expression" id="?tsJi]Pt^%%_gKEfS9YY">
                                                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                    <statement name="items">
                                                                                                                                                                      <block type="list_item_shape" id="S|`aJUHIq[_y8F,QUNOx">
                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                          <block type="prop_list_align" id="R?ohJAh8[/{q@1ekdyk1=">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <field name="align">middle</field>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_list_item_source" id="Uc$s=D3YLE?,HQ@1Ji7}a">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="source">
                                                                                                                                                                                  <block type="expression" id="O0IX.]!wr0@2WBOWkrAp2">
                                                                                                                                                                                    <field name="value">str</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                        <statement name="template">
                                                                                                                                                                          <block type="image_shape" id=":$5.aZc{P~!(iCeCQzra">
                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                              <block type="prop_max_size" id="kdb02kef0+rupQS1|?4g">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="maxWidth">
                                                                                                                                                                                  <block type="expression" id="M:R460C,ryh;!5oTXtJ|">
                                                                                                                                                                                    <field name="value">$cell.width/10 -5</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <value name="maxHeight">
                                                                                                                                                                                  <block type="expression" id="T4#_ML?:wb2@1siUWDR-1">
                                                                                                                                                                                    <field name="value">$cell.height - 10</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_image_key" id="g@2h5bqa%zW|1N2+{IS]3">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="key">
                                                                                                                                                                                      <block type="string_value" id="Sq{u1Aet+p{#g8_BZWJp">
                                                                                                                                                                                        <field name="value">${it}_${$cell.data}.png</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_image_src" id="d(Zb)ILBTn8ywYPHT@2QS">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="src">
                                                                                                                                                                                          <block type="string_value" id="wju-|CY_+cfBj~Tzshqt">
                                                                                                                                                                                            <field name="value">${image_path}${it}_${$cell.data}.png</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </statement>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="variable" id="]5ihGYa5B]h-#h_rVKP8">
                                                                                                            <field name="name">load</field>
                                                                                                            <value name="value">
                                                                                                              <block type="custom_image_list" id="LXlf5r`FQSDP{ELoREy~">
                                                                                                                <field name="link">${image_path}</field>
                                                                                                                <field name="images">table${type}|${it}_0|${it}_1|${it}_2|ver1|ver2|ver3|ver4|ver5|1|5</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="partial_explanation" id="Yum@1.^;iBKJ_hgBOs|Bs" inline="true">
                                                                                                                <value name="value">
                                                                                                                  <block type="string_value" id="2,ZTPBv5FtEbXXWW6`RW">
                                                                                                                    <field name="value">&lt;u&gt;Step 1: Study the graph given. Pay attention to the data and title.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;
&lt;center&gt;
                                                                                                                    </field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="if_then_else_block" id="gS@2B?KBthB;@2OvW{$?y!">
                                                                                                                    <value name="if">
                                                                                                                      <block type="expression" id="sCDqvZ,uB,Fi!7q~6C12">
                                                                                                                        <field name="value">type == 2</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <statement name="then">
                                                                                                                      <block type="partial_explanation" id="a_{8C^~p#8DDUth#xX@1u" inline="true">
                                                                                                                        <value name="value">
                                                                                                                          <block type="string_value" id="XBFR)xXKtnl+]a%,mpIi">
                                                                                                                            <field name="value">&lt;style&gt;
  td{text-align: center; vertical-align: bottom; width: 300px; height: 210px}
img{margin: 5px;}
  &lt;/style&gt;

&lt;table style='background-image: url("@1sprite.src(table2)"); width: 580px; height: 210px;'&gt;
  &lt;tr&gt;
  &lt;td&gt;&lt;img src='@1sprite.src(ver${color[0]})' style='height: ${value[0]@219.5}px; max-width: 39px;'/&gt;&lt;/td&gt;
  &lt;td&gt;&lt;img src='@1sprite.src(ver${color[1]})' style='height: ${value[1]@219.5}px; max-width: 39px;'/&gt;&lt;/td&gt;
  &lt;td&gt;&lt;img src='@1sprite.src(ver${color[2]})' style='height: ${value[2]@219.5}px; max-width: 39px;'/&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
    &lt;/table&gt;


&lt;table style='width: 580px; height: 50px;'&gt;
  &lt;tr&gt;
    &lt;td style='height: 50px'&gt;&lt;img src='@1sprite.src(${it}_0)' style='max-height: 50px;'/&gt;&lt;/td&gt;
&lt;td style='height: 50px'&gt;&lt;img src='@1sprite.src(${it}_1)' style='max-height: 50px;'/&gt;&lt;/td&gt;
&lt;td style='height: 50px'&gt;&lt;img src='@1sprite.src(${it}_2)' style='max-height: 50px;'/&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
                                                                                                                            </field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <statement name="else">
                                                                                                                      <block type="if_then_else_block" id="]|jU1uUIuk}.DS4+3eGW">
                                                                                                                        <value name="if">
                                                                                                                          <block type="expression" id="XDl;^XCpXJ57ZPGNG;u]">
                                                                                                                            <field name="value">type == 1</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <statement name="then">
                                                                                                                          <block type="partial_explanation" id="A+3G@2ea!CCi@2^d]^#Xo." inline="true">
                                                                                                                            <value name="value">
                                                                                                                              <block type="string_value" id="/@2NLn~JNMUH6j3;.^w`u">
                                                                                                                                <field name="value">&lt;style&gt;
  td{text-align: center; vertical-align: bottom;}
img{margin: 5px; max-height: 40px}
  &lt;/style&gt;

&lt;table style='background-image: url("@1sprite.src(table1)"); width: 696px; height: 169px;'&gt;
                                                                                                                                </field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="variable" id="VDZ6HB^|b2hM9.LA^oGN">
                                                                                                                                <field name="name">i</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="rGGy;zfGO8`DzkJ2_^8j">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="while_do_block" id="U?VuI/#Swa06i~?gV7._">
                                                                                                                                    <value name="while">
                                                                                                                                      <block type="expression" id="t@2aOp@1sIhi,a6Q@1/pbew">
                                                                                                                                        <field name="value">i &lt; 3</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <statement name="do">
                                                                                                                                      <block type="partial_explanation" id="w[yFj]@1Kf(LDi)89iW~|" inline="true">
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="string_value" id="^jM+kCpCeaV~nFG74VaI">
                                                                                                                                            <field name="value">&lt;tr&gt;&lt;td style='width: 180px'&gt;&lt;img src='@1sprite.src(${it}_${i})'/&gt;&lt;/td&gt;
&lt;td&gt;
                                                                                                                                            </field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="variable" id="y.8@14QsVM^Wk}@11ca3T%">
                                                                                                                                            <field name="name">val</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id="kMee6$1v:m+U3TCCLj^T">
                                                                                                                                                <field name="value">value[i]</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="while_do_block" id="[(#Zv-,eT6TQcQxF.zq9">
                                                                                                                                                <value name="while">
                                                                                                                                                  <block type="expression" id="}!gj|/@2-G;p4lMeL31m2">
                                                                                                                                                    <field name="value">val &gt;= 5</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <statement name="do">
                                                                                                                                                  <block type="partial_explanation" id="$52pknv;A-+ql_N%R4L@2" inline="true">
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="string_value" id="Psq-nmOb[Vw)-|~RH5">
                                                                                                                                                        <field name="value">&lt;img src='@1sprite.src(5)'/&gt;</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="variable" id="cuFbo7LD@1B])@2R`(cQm@2">
                                                                                                                                                        <field name="name">val</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="]X@19;h!#%:JEu=:oof6/">
                                                                                                                                                            <field name="value">val - 5</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                                <next>
                                                                                                                                                  <block type="while_do_block" id="^xrFAjvZ;s+X$sflI8wK">
                                                                                                                                                    <value name="while">
                                                                                                                                                      <block type="expression" id="uM3qn?}gc@2JsL{lMz?@1d">
                                                                                                                                                        <field name="value">val  &gt;= 1</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <statement name="do">
                                                                                                                                                      <block type="partial_explanation" id=".$?T`?5no}5=K(Pu)rrC" inline="true">
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="string_value" id="A`4=HO2A8Hlul8G9}4:z">
                                                                                                                                                            <field name="value">&lt;img src='@1sprite.src(1)'/&gt;</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="variable" id="wR=0|UU@2%3@2Q?LFu@1OKa">
                                                                                                                                                            <field name="name">val</field>
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id="K@2_WtDzDOP/gZ^ez@15@2O">
                                                                                                                                                                <field name="value">val - 1</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="partial_explanation" id="rx_Kxd-}SSYj(yJ50q1F" inline="true">
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="string_value" id="ZzwM6Hb9a12sm6iV_t0o">
                                                                                                                                                            <field name="value">&lt;/td&gt;&lt;/tr&gt;</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="variable" id="MS`3EK3mapZ=Y!8HQck[">
                                                                                                                                                            <field name="name">i</field>
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id="GdpCv?daT~D/n%|O@1?[0">
                                                                                                                                                                <field name="value">i+1</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="partial_explanation" id="9.1-NbsR`ipw8zLVwuo]" inline="true">
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="string_value" id=";=f|IaSgOjM9y,v~QC7-">
                                                                                                                                            <field name="value">&lt;/table&gt;</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <statement name="else">
                                                                                                                          <block type="partial_explanation" id="9hlm/i{k8Y{uE6+B8TDE" inline="true">
                                                                                                                            <value name="value">
                                                                                                                              <block type="string_value" id="i))288(T([Ld=g;~_GWt">
                                                                                                                                <field name="value">&lt;style&gt;
  td{text-align: center; vertical-align: bottom;}
img{margin: 5px; max-height: 40px}
  &lt;/style&gt;

&lt;table style='background-image: url("@1sprite.src(table3)"); width: 696px; height: 169px;'&gt;
                                                                                                                                </field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="variable" id="C``zfh.}zz:P(fX~t-V_">
                                                                                                                                <field name="name">i</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="X_M8C4t_@1TGi+@2!QMjCh">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="while_do_block" id="Z;yx;9M/9=!t}c-.s_?j">
                                                                                                                                    <value name="while">
                                                                                                                                      <block type="expression" id="g#l^Kj!P/Vj).4Ujl%9V">
                                                                                                                                        <field name="value">i &lt; 3</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <statement name="do">
                                                                                                                                      <block type="partial_explanation" id="hNoyBnjFdNCUn1r?#21H" inline="true">
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="string_value" id="oZHGa1O5ihs;b4i2P:Wy">
                                                                                                                                            <field name="value">&lt;tr&gt;&lt;td style='width: 180px'&gt;&lt;img src='@1sprite.src(${it}_${i})'/&gt;&lt;/td&gt;
&lt;td&gt;
                                                                                                                                            </field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="variable" id="=+UN~,FHcj@1wn:eA+YUR">
                                                                                                                                            <field name="name">j</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id="/Q7ma)C-RwF8W@2eo)L#b">
                                                                                                                                                <field name="value">0</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="while_do_block" id="-h(;V1S,=eAu45O[AcfU">
                                                                                                                                                <value name="while">
                                                                                                                                                  <block type="expression" id="YO^FhyvF1z)fh(|%)z(_">
                                                                                                                                                    <field name="value">j &lt; value[i]</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <statement name="do">
                                                                                                                                                  <block type="partial_explanation" id="dKI%Cv8P]D^suD:kE5M5" inline="true">
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="string_value" id="EKG:;3cZyPPo.h~E3ytK">
                                                                                                                                                        <field name="value">&lt;img style='max-width: 50px' src='@1sprite.src(${it}_${i})'/&gt;</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="variable" id="LuNK[e#V}9U76tJUEHn,">
                                                                                                                                                        <field name="name">j</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="P27$/+yHMc(,@1NjTmyvm">
                                                                                                                                                            <field name="value">j+1</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                                <next>
                                                                                                                                                  <block type="partial_explanation" id="Nn7Xg!8cOD.lwg$7c}9+" inline="true">
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="string_value" id="|A:nhsM`R.#gUBuT7I(Y">
                                                                                                                                                        <field name="value">&lt;/td&gt;&lt;/tr&gt;</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="variable" id="W|4BDv@2@1]=ZPip|sY?6G">
                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="6R5:yv,@2rAM9#yO5VF@20">
                                                                                                                                                            <field name="value">i+1</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="partial_explanation" id="|nsSiFr[o[(eDL|M:_0B" inline="true">
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="string_value" id="sn5,4Id2VhkUm(EM@1l@1+">
                                                                                                                                            <field name="value">&lt;/table&gt;</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="partial_explanation" id="W^|C=yDFomoBOL@1u8,lw" inline="true">
                                                                                                                        <value name="value">
                                                                                                                          <block type="string_value" id="64y:-M!sZA!SP6}!l8)g">
                                                                                                                            <field name="value">&lt;/center&gt;</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="end_partial_explanation" id="4X37Za@2auppZwNV4TW6X">
                                                                                                                            <next>
                                                                                                                              <block type="partial_explanation" id="y%WRaCvA,$t%Ozgxc-R]" inline="true">
                                                                                                                                <value name="value">
                                                                                                                                  <block type="string_value" id="`V4(T}R:+ESet)#a|E2x">
                                                                                                                                    <field name="value">&lt;u&gt;Step 2: Now let's compare between the question and the graph.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;
&lt;center&gt;
                                                                                                                                    </field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="if_then_else_block" id="F`R8e[|rO:Aah}+tGO.d">
                                                                                                                                    <value name="if">
                                                                                                                                      <block type="expression" id="P%9m@2/=W0y:3t4:HutW)">
                                                                                                                                        <field name="value">type == 2</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <statement name="then">
                                                                                                                                      <block type="partial_explanation" id="jk;SY6KX2c0f(0+nzY}F" inline="true">
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="string_value" id="IVi6L0B726MGmgZD=Exv">
                                                                                                                                            <field name="value">&lt;style&gt;
  td{text-align: center; vertical-align: bottom; width: 300px; height: 210px}
img{margin: 5px;}
  &lt;/style&gt;

&lt;table style='background-image: url("@1sprite.src(table2)"); width: 580px; height: 210px;'&gt;
  &lt;tr&gt;
  &lt;td&gt;&lt;img src='@1sprite.src(ver${color[0]})' style='height: ${value[0]@219.5}px; max-width: 39px;'/&gt;&lt;/td&gt;
  &lt;td&gt;&lt;img src='@1sprite.src(ver${color[1]})' style='height: ${value[1]@219.5}px; max-width: 39px;'/&gt;&lt;/td&gt;
  &lt;td&gt;&lt;img src='@1sprite.src(ver${color[2]})' style='height: ${value[2]@219.5}px; max-width: 39px;'/&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
    &lt;/table&gt;


&lt;table style='width: 580px; height: 50px;'&gt;
  &lt;tr&gt;
    &lt;td style='height: 50px'&gt;&lt;img src='@1sprite.src(${it}_0)' style='max-height: 50px;'/&gt;&lt;/td&gt;
&lt;td style='height: 50px'&gt;&lt;img src='@1sprite.src(${it}_1)' style='max-height: 50px;'/&gt;&lt;/td&gt;
&lt;td style='height: 50px'&gt;&lt;img src='@1sprite.src(${it}_2)' style='max-height: 50px;'/&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
                                                                                                                                            </field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <statement name="else">
                                                                                                                                      <block type="if_then_else_block" id="XO;Rs$5uL0TannVGObHJ">
                                                                                                                                        <value name="if">
                                                                                                                                          <block type="expression" id=")@1x2+kj{~2OpIJVLtLel">
                                                                                                                                            <field name="value">type == 1</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <statement name="then">
                                                                                                                                          <block type="partial_explanation" id="Ac!iLyy-aZ-4%)|(Ug]9" inline="true">
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="string_value" id="Ui8;?^];Uj=p64;1vHU@2">
                                                                                                                                                <field name="value">&lt;style&gt;
  td{text-align: center; vertical-align: bottom;}
img{margin: 5px; max-height: 40px}
  &lt;/style&gt;

&lt;table style='background-image: url("@1sprite.src(table1)"); width: 696px; height: 169px;'&gt;
                                                                                                                                                </field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="variable" id="!o8OL4F0#,;p`n.?7-~d">
                                                                                                                                                <field name="name">i</field>
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="expression" id="yj3CX-|J]}DEi26Cb-5A">
                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="while_do_block" id="btn@26OHH)vQKYSyq9)9O">
                                                                                                                                                    <value name="while">
                                                                                                                                                      <block type="expression" id="P:@2p;S!7YhDS,4k~UAq%">
                                                                                                                                                        <field name="value">i &lt; 3</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <statement name="do">
                                                                                                                                                      <block type="partial_explanation" id="2#(G;GwkttqOuS0XCx$w" inline="true">
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="string_value" id="k.C=Vhn~RDe+{WU/@1B?|">
                                                                                                                                                            <field name="value">&lt;tr&gt;&lt;td style='width: 180px'&gt;&lt;img src='@1sprite.src(${it}_${i})'/&gt;&lt;/td&gt;
&lt;td&gt;
                                                                                                                                                            </field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="variable" id="c788mt9dSL3dae/5D2{3">
                                                                                                                                                            <field name="name">val</field>
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id="q7]R;%ZLkN{,yn8}lI5B">
                                                                                                                                                                <field name="value">value[i]</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="while_do_block" id="oh^[|?/S:wT.x^X|9Hcs">
                                                                                                                                                                <value name="while">
                                                                                                                                                                  <block type="expression" id="wbE6CggNu_lO[G@2ia1`3">
                                                                                                                                                                    <field name="value">val &gt;= 5</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <statement name="do">
                                                                                                                                                                  <block type="partial_explanation" id="w}@12nuC#3o,;$hr=rb?%" inline="true">
                                                                                                                                                                    <value name="value">
                                                                                                                                                                      <block type="string_value" id=";J_l.2it,0OwwvST#u9A">
                                                                                                                                                                        <field name="value">&lt;img src='@1sprite.src(5)'/&gt;</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="variable" id="`gRmE@1~GYdN%/7]p:1f[">
                                                                                                                                                                        <field name="name">val</field>
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="expression" id="skPqG?$g[6B9:(ZjJ}kk">
                                                                                                                                                                            <field name="value">val - 5</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="while_do_block" id="uS!^P}NFCl+F}d5S,(.N">
                                                                                                                                                                    <value name="while">
                                                                                                                                                                      <block type="expression" id="C.kBL9DPAw/?B+AMC=V6">
                                                                                                                                                                        <field name="value">val  &gt;= 1</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <statement name="do">
                                                                                                                                                                      <block type="partial_explanation" id="@2Hp_}2245nWr]3TBE%2`" inline="true">
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="string_value" id="t{:.E%Ks@2DDP54()dgn;">
                                                                                                                                                                            <field name="value">&lt;img src='@1sprite.src(1)'/&gt;</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="variable" id="]wS~XQ@1@2D]v9kA=+N:Ne">
                                                                                                                                                                            <field name="name">val</field>
                                                                                                                                                                            <value name="value">
                                                                                                                                                                              <block type="expression" id="p8[.qn/sir58U~}b{V]r">
                                                                                                                                                                                <field name="value">val - 1</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="partial_explanation" id=".(+Tn_1i^|-6RwXre-lk" inline="true">
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="string_value" id="(0,A1oRzVQ^;:Oyg(x(-">
                                                                                                                                                                            <field name="value">&lt;/td&gt;&lt;/tr&gt;</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="variable" id="#xmkX[V`VRUFw@1Pv0$^k">
                                                                                                                                                                            <field name="name">i</field>
                                                                                                                                                                            <value name="value">
                                                                                                                                                                              <block type="expression" id="Rd538Fp^@2f@2g_CeBy!lz">
                                                                                                                                                                                <field name="value">i+1</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="partial_explanation" id="CZ(M(|)maVX$7/{lX?rn" inline="true">
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="string_value" id="S^#p~MN;?=+]tGhMBk1q">
                                                                                                                                                            <field name="value">&lt;/table&gt;</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <statement name="else">
                                                                                                                                          <block type="partial_explanation" id="x]JqsoYIZegN~)M-!vh~" inline="true">
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="string_value" id="{BNd=81m6lp058DvrkjO">
                                                                                                                                                <field name="value">&lt;style&gt;
  td{text-align: center; vertical-align: bottom;}
img{margin: 5px; max-height: 40px}
  &lt;/style&gt;

&lt;table style='background-image: url("@1sprite.src(table3)"); width: 696px; height: 169px;'&gt;
                                                                                                                                                </field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="variable" id="IK}+tGDkZE=Xaylg~nQ#">
                                                                                                                                                <field name="name">i</field>
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="expression" id="+2Zk6Z.Yuv5W_@2kOm[_J">
                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="while_do_block" id="){e/Jns4`#E`gG#T?;{~">
                                                                                                                                                    <value name="while">
                                                                                                                                                      <block type="expression" id="(3}gg!]Y.(:VJQN!FDnM">
                                                                                                                                                        <field name="value">i &lt; 3</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <statement name="do">
                                                                                                                                                      <block type="partial_explanation" id="-@1:w$^9N]jFL~85:xB[%" inline="true">
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="string_value" id="[)v,o+XR!!X};i6]g6#~">
                                                                                                                                                            <field name="value">&lt;tr&gt;&lt;td style='width: 180px'&gt;&lt;img src='@1sprite.src(${it}_${i})'/&gt;&lt;/td&gt;
&lt;td&gt;
                                                                                                                                                            </field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="variable" id="jVHveKt{:g6ClWE;O|WF">
                                                                                                                                                            <field name="name">j</field>
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id="I@1~V|!Z7,JZfU+8=(ijg">
                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="while_do_block" id="w8Mq+O+$gE^3OSQ(gc0~">
                                                                                                                                                                <value name="while">
                                                                                                                                                                  <block type="expression" id="PZ34e}K`ONmZ|BrvR=iV">
                                                                                                                                                                    <field name="value">j &lt; value[i]</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <statement name="do">
                                                                                                                                                                  <block type="partial_explanation" id="%^0YMQ7Ue;om~f[;MRj#" inline="true">
                                                                                                                                                                    <value name="value">
                                                                                                                                                                      <block type="string_value" id="bV(_gwk`@1%|IBjPes3C^">
                                                                                                                                                                        <field name="value">&lt;img style='max-width: 50px' src='@1sprite.src(${it}_${i})'/&gt;</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="variable" id="XI=?#{cs@1bJMJ4TvQW]-">
                                                                                                                                                                        <field name="name">j</field>
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="expression" id="6LbO(eq}z9y7Af6YL@1^m">
                                                                                                                                                                            <field name="value">j+1</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="partial_explanation" id="d]k$Ix72x2Jpv}`(jD-^" inline="true">
                                                                                                                                                                    <value name="value">
                                                                                                                                                                      <block type="string_value" id="UOca3gLLndg6iL)({.?A">
                                                                                                                                                                        <field name="value">&lt;/td&gt;&lt;/tr&gt;</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="variable" id="11bt_~vnj[9]aTbAq)xA">
                                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="expression" id="Ul,NC=iwZJC(n)DW:l;a">
                                                                                                                                                                            <field name="value">i+1</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="partial_explanation" id=",wmosB$9uWXF-bRe2=!g" inline="true">
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="string_value" id="=[H@1u^F2Tqx?zX[e(#=P">
                                                                                                                                                            <field name="value">&lt;/table&gt;</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="partial_explanation" id="5(YHb)P_EA{.w#p4O^]p" inline="true">
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="string_value" id="cne%O{f?l?ymu$^IR-Gf">
                                                                                                                                            <field name="value">&lt;/center&gt;
&lt;/br&gt;
&lt;span style='background: rgb(250, 250, 250, 0.5)'&gt;
  Question focus on &lt;b&gt;${group[g]}&lt;/b&gt; and favourite &lt;b&gt;${item[it1]}s&lt;/b&gt;.
&lt;/br&gt;
Graph focus on &lt;b&gt;${place[p]}&lt;/b&gt; and favourite &lt;b&gt;${item[it]}&lt;/b&gt;.
&lt;/span&gt;
                                                                                                                                            </field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="end_partial_explanation" id="8:%D^5DI#lvyj7j+IsBV">
                                                                                                                                            <next>
                                                                                                                                              <block type="partial_explanation" id="{Gqd|Sd]t(Z2.ppC?-Sp" inline="true">
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="string_value" id="b;]6^=2ts9iqpIZ,sv^u">
                                                                                                                                                    <field name="value">&lt;u&gt;Step 3: Finally select if the question can gather the data.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;
&lt;center&gt;
                                                                                                                                                    </field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="if_then_else_block" id="sE(d=M{Ohx$5WjBlDE/E">
                                                                                                                                                    <value name="if">
                                                                                                                                                      <block type="expression" id="-`sW%aUs.cs+qs^V[?[4">
                                                                                                                                                        <field name="value">type == 2</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <statement name="then">
                                                                                                                                                      <block type="partial_explanation" id="$vKSD~cU.D18Kmwl!Fb`" inline="true">
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="string_value" id="{KV!/)Qf4_b~q({K;r64">
                                                                                                                                                            <field name="value">&lt;style&gt;
  td{text-align: center; vertical-align: bottom; width: 300px; height: 210px}
img{margin: 5px;}
  &lt;/style&gt;

&lt;table style='background-image: url("@1sprite.src(table2)"); width: 580px; height: 210px;'&gt;
  &lt;tr&gt;
  &lt;td&gt;&lt;img src='@1sprite.src(ver${color[0]})' style='height: ${value[0]@219.5}px; max-width: 39px;'/&gt;&lt;/td&gt;
  &lt;td&gt;&lt;img src='@1sprite.src(ver${color[1]})' style='height: ${value[1]@219.5}px; max-width: 39px;'/&gt;&lt;/td&gt;
  &lt;td&gt;&lt;img src='@1sprite.src(ver${color[2]})' style='height: ${value[2]@219.5}px; max-width: 39px;'/&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;tr&gt;
    &lt;/table&gt;


&lt;table style='width: 580px; height: 50px;'&gt;
  &lt;tr&gt;
    &lt;td style='height: 50px'&gt;&lt;img src='@1sprite.src(${it}_0)' style='max-height: 50px;'/&gt;&lt;/td&gt;
&lt;td style='height: 50px'&gt;&lt;img src='@1sprite.src(${it}_1)' style='max-height: 50px;'/&gt;&lt;/td&gt;
&lt;td style='height: 50px'&gt;&lt;img src='@1sprite.src(${it}_2)' style='max-height: 50px;'/&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
                                                                                                                                                            </field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <statement name="else">
                                                                                                                                                      <block type="if_then_else_block" id=":]TccrkN|w_wYxhXtx7b">
                                                                                                                                                        <value name="if">
                                                                                                                                                          <block type="expression" id="lyCa!dZ$s:CXfF.lo@2h?">
                                                                                                                                                            <field name="value">type == 1</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <statement name="then">
                                                                                                                                                          <block type="partial_explanation" id="jEIH7AchA.Q=wsZ,9Xe{" inline="true">
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="string_value" id="uwJCWBD~_gdi$/`[:K_.">
                                                                                                                                                                <field name="value">&lt;style&gt;
  td{text-align: center; vertical-align: bottom;}
img{margin: 5px; max-height: 40px}
  &lt;/style&gt;

&lt;table style='background-image: url("@1sprite.src(table1)"); width: 696px; height: 169px;'&gt;
                                                                                                                                                                </field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="variable" id="9e5DbW!YqXx7bgR27Wi=">
                                                                                                                                                                <field name="name">i</field>
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="expression" id="I?pU^FA^Mifd5dVy+wRQ">
                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="while_do_block" id="#)i0=?||6K8CAEY!`-6t">
                                                                                                                                                                    <value name="while">
                                                                                                                                                                      <block type="expression" id="_Z4fKC~s.m`M+]/#=Q:q">
                                                                                                                                                                        <field name="value">i &lt; 3</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <statement name="do">
                                                                                                                                                                      <block type="partial_explanation" id="$xf66mhb+oC5b-1v?v{H" inline="true">
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="string_value" id="I_8v?TuW#du6Bs2l1720">
                                                                                                                                                                            <field name="value">&lt;tr&gt;&lt;td style='width: 180px'&gt;&lt;img src='@1sprite.src(${it}_${i})'/&gt;&lt;/td&gt;
&lt;td&gt;
                                                                                                                                                                            </field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="variable" id="ox~eYy5Ds-R:Z8-ZYdDk">
                                                                                                                                                                            <field name="name">val</field>
                                                                                                                                                                            <value name="value">
                                                                                                                                                                              <block type="expression" id="d..yFRSuo6Vu3BPG;+=@1">
                                                                                                                                                                                <field name="value">value[i]</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="while_do_block" id="S`_wjK@17NNM{?g.0m[Z0">
                                                                                                                                                                                <value name="while">
                                                                                                                                                                                  <block type="expression" id="+[qnit@2k8lgij]7~cjK:">
                                                                                                                                                                                    <field name="value">val &gt;= 5</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <statement name="do">
                                                                                                                                                                                  <block type="partial_explanation" id="Ffo%Vbb{,i|2|Ninywc$" inline="true">
                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                      <block type="string_value" id="#S@2F(o?@1|d9L~gLyJjjb">
                                                                                                                                                                                        <field name="value">&lt;img src='@1sprite.src(5)'/&gt;</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="variable" id="mShtqZe`5F5bq0UbYyTr">
                                                                                                                                                                                        <field name="name">val</field>
                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                          <block type="expression" id="[M#H;JB(Bhq`of]lQ@1zj">
                                                                                                                                                                                            <field name="value">val - 5</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </statement>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="while_do_block" id="n6i@2r%hu_AHun+!Zgk@2L">
                                                                                                                                                                                    <value name="while">
                                                                                                                                                                                      <block type="expression" id="hg[|P8KV0Z%l%7l_{wnQ">
                                                                                                                                                                                        <field name="value">val  &gt;= 1</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <statement name="do">
                                                                                                                                                                                      <block type="partial_explanation" id="$OJp?S[@2N2-UqNR?guM;" inline="true">
                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                          <block type="string_value" id="%eZ`P:n#0!6)L^@2H1|Hx">
                                                                                                                                                                                            <field name="value">&lt;img src='@1sprite.src(1)'/&gt;</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="variable" id="kD;H(2I2:a/@2pdFC~g]i">
                                                                                                                                                                                            <field name="name">val</field>
                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                              <block type="expression" id="^_kebD}Le@2[9G|[iOz6a">
                                                                                                                                                                                                <field name="value">val - 1</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </statement>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="partial_explanation" id="v;%p;_m#SQ5DP03}${?0" inline="true">
                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                          <block type="string_value" id="jNN(L_`cIZ%9@2hBX)TJ1">
                                                                                                                                                                                            <field name="value">&lt;/td&gt;&lt;/tr&gt;</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="variable" id="9Ep,T$6j(Mje0!m!/;{K">
                                                                                                                                                                                            <field name="name">i</field>
                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                              <block type="expression" id="!eWoG-,nE^E$=ODz=Fd=">
                                                                                                                                                                                                <field name="value">i+1</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="partial_explanation" id="e%~jvB{2doDS:8J6UWlz" inline="true">
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="string_value" id="t7q=ndu?@2-}O7c.I%rGd">
                                                                                                                                                                            <field name="value">&lt;/table&gt;</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                        <statement name="else">
                                                                                                                                                          <block type="partial_explanation" id="u+sSzz7P+Y64E|R7_5{l" inline="true">
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="string_value" id="=.h!yZ]`jD[p-YEXg77t">
                                                                                                                                                                <field name="value">&lt;style&gt;
  td{text-align: center; vertical-align: bottom;}
img{margin: 5px; max-height: 40px}
  &lt;/style&gt;

&lt;table style='background-image: url("@1sprite.src(table3)"); width: 696px; height: 169px;'&gt;
                                                                                                                                                                </field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="variable" id="G^GDI]?(sk$`]u]%#.bh">
                                                                                                                                                                <field name="name">i</field>
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="expression" id="XH_-mlaaH2}j!Jn:5%;.">
                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="while_do_block" id="MT{DO2-T6+A}.@2nZX0C[">
                                                                                                                                                                    <value name="while">
                                                                                                                                                                      <block type="expression" id="Gd1LTpC-`i9W/DJN+S@2}">
                                                                                                                                                                        <field name="value">i &lt; 3</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <statement name="do">
                                                                                                                                                                      <block type="partial_explanation" id="rOwT`wIY$J,7pL9dIW4]" inline="true">
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="string_value" id="G-FW^^)+019Wxti5q8}0">
                                                                                                                                                                            <field name="value">&lt;tr&gt;&lt;td style='width: 180px'&gt;&lt;img src='@1sprite.src(${it}_${i})'/&gt;&lt;/td&gt;
&lt;td&gt;
                                                                                                                                                                            </field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="variable" id="!||gR[+I8Ug(!4a@2%]QR">
                                                                                                                                                                            <field name="name">j</field>
                                                                                                                                                                            <value name="value">
                                                                                                                                                                              <block type="expression" id="iCJH(#(/x;uSd;@13Nt?#">
                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="while_do_block" id="3iUI1JcUV3i4BH_j!0##">
                                                                                                                                                                                <value name="while">
                                                                                                                                                                                  <block type="expression" id="SA0!mPok@2Kg:Y@1U=K}Nt">
                                                                                                                                                                                    <field name="value">j &lt; value[i]</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <statement name="do">
                                                                                                                                                                                  <block type="partial_explanation" id="hK~/3/5^w;mN$i~;IZ$b" inline="true">
                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                      <block type="string_value" id="FzH|?ynH$O5RTxgN+%!K">
                                                                                                                                                                                        <field name="value">&lt;img style='max-width: 50px' src='@1sprite.src(${it}_${i})'/&gt;</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="variable" id="dT^WTkKCW$)^f4`@2SSmT">
                                                                                                                                                                                        <field name="name">j</field>
                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                          <block type="expression" id="T]G^+qY7B.~X:QAeRyC3">
                                                                                                                                                                                            <field name="value">j+1</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </statement>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="partial_explanation" id="jc{13Szt3gYtRwwF5!+`" inline="true">
                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                      <block type="string_value" id="DyqNOvXu4!:vAl`R{p/J">
                                                                                                                                                                                        <field name="value">&lt;/td&gt;&lt;/tr&gt;</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="variable" id="bZe)@1T?G]=$G@1(+!DpKt">
                                                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                          <block type="expression" id="oD9FC7TL#@16W}CAC9{?w">
                                                                                                                                                                                            <field name="value">i+1</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="partial_explanation" id="(+=w3rjsS?h7)`?T|vvS" inline="true">
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="string_value" id=",ddT^zr%8.P?IKO3ET.h">
                                                                                                                                                                            <field name="value">&lt;/table&gt;</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="partial_explanation" id="9_f}7W_M-J](B.0U4%!0" inline="true">
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="string_value" id="eMBRj$CWYz9sb,kRoltx">
                                                                                                                                                            <field name="value">&lt;/center&gt;
&lt;/br&gt;
&lt;span style='background: rgb(250, 250, 250, 0.5)'&gt;
  Question focus on &lt;b&gt;${group[g]}&lt;/b&gt; and favourite &lt;b&gt;${item[it1]}s&lt;/b&gt;.
&lt;/br&gt;
Graph focus on &lt;b&gt;${place[p]}&lt;/b&gt; and favourite &lt;b&gt;${item[it]}&lt;/b&gt;.&lt;/br&gt;&lt;/br&gt;
  
&lt;b&gt;${answer == 1 ? 'Yes': 'No'}&lt;/b&gt;, the question ${answer == 1 ? 'can':'cannot'} help to gather the data.
&lt;/span&gt;
                                                                                                                                                            </field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
  <block type="question_part" id="[B3(0=qMkUAn^:e?hzhN" x="0" y="9364">
    <field name="index">1</field>
  </block>
</xml>
END_XML]] */