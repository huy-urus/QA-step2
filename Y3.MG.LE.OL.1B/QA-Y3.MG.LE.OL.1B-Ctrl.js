
module.exports = [
  {
    "#type": "question",
    "name": "Y3.MG.LE.OL.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/Y3.MG.LE.OL.1B/"
      },
      {
        "#type": "variable",
        "name": "background",
        "value": {
          "#type": "string_array",
          "items": "bg1|bg2|bg3|bg4|bg5"
        }
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_one",
          "items": {
            "#type": "expression",
            "value": "background"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "variable",
        "name": "item",
        "value": {
          "#type": "random_one",
          "items": {
            "#type": "string_array",
            "items": "pencils|ropes|ladders|ribbons"
          }
        }
      },
      {
        "#type": "variable",
        "name": "oder",
        "value": {
          "#type": "string_array",
          "items": "longest to shortest|shortest to longest"
        }
      },
      {
        "#type": "variable",
        "name": "type",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "2"
          }
        }
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "resp_x",
              "value": "cx"
            },
            "y": {
              "#type": "expression",
              "value": "60"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "Compare and order the ${item} from ${oder[type-1]}."
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "38"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "A",
        "value": {
          "#type": "random_many",
          "count": {
            "#type": "expression",
            "value": "4"
          },
          "items": {
            "#type": "expression",
            "value": "[1, 2, 3, 4, 5, 6, 7]"
          }
        }
      },
      {
        "#type": "variable",
        "name": "asc",
        "value": {
          "#type": "func",
          "name": "sort",
          "args": [
            {
              "#type": "expression",
              "value": "A"
            },
            "asc"
          ]
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "variable",
            "name": "desc",
            "value": {
              "#type": "func",
              "name": "sort",
              "args": [
                {
                  "#type": "expression",
                  "value": "A"
                },
                "desc"
              ]
            }
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "4"
                },
                "cols": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "580"
                },
                "y": {
                  "#type": "expression",
                  "value": "100"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "20"
                },
                "height": {
                  "#type": "expression",
                  "value": "340"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "desc"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0"
                },
                "y": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "0"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "0"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "shape.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}shape.png"
                      }
                    ]
                  },
                  {
                    "#type": "drop_shape",
                    "multiple": false,
                    "resultMode": "default",
                    "groupName": "",
                    "#props": [
                      {
                        "#type": "prop_value",
                        "#prop": "",
                        "value": {
                          "#type": "expression",
                          "value": "$cell.data"
                        }
                      },
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "342"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "74"
                        }
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ],
        "else": [
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "4"
                },
                "cols": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "580"
                },
                "y": {
                  "#type": "expression",
                  "value": "100"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "20"
                },
                "height": {
                  "#type": "expression",
                  "value": "340"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "asc"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0"
                },
                "y": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "0"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "0"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "shape.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}shape.png"
                      }
                    ]
                  },
                  {
                    "#type": "drop_shape",
                    "multiple": false,
                    "resultMode": "default",
                    "groupName": "",
                    "#props": [
                      {
                        "#type": "prop_value",
                        "#prop": "",
                        "value": {
                          "#type": "expression",
                          "value": "$cell.data"
                        }
                      },
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "342"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "74"
                        }
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "4"
            },
            "cols": {
              "#type": "expression",
              "value": "1"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "50"
            },
            "y": {
              "#type": "expression",
              "value": "100"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "20"
            },
            "height": {
              "#type": "expression",
              "value": "340"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "expression",
              "value": "A"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0"
            },
            "y": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_cell_template",
            "variable": "$cell",
            "#prop": "cell.template",
            "#callback": "$cell",
            "body": [
              {
                "#type": "choice_custom_shape",
                "action": "drag",
                "value": {
                  "#type": "expression",
                  "value": "$cell.data"
                },
                "template": {
                  "#callback": "$choice",
                  "variable": "$choice",
                  "body": [
                    {
                      "#type": "image_shape",
                      "#props": [
                        {
                          "#type": "prop_anchor",
                          "#prop": "anchor",
                          "x": {
                            "#type": "expression",
                            "value": "0"
                          },
                          "y": {
                            "#type": "expression",
                            "value": "0.5"
                          }
                        },
                        {
                          "#type": "prop_position",
                          "#prop": "",
                          "x": {
                            "#type": "expression",
                            "value": "$cell.centerX"
                          },
                          "y": {
                            "#type": "expression",
                            "value": "$cell.centerY"
                          }
                        },
                        {
                          "#type": "prop_size",
                          "#prop": "",
                          "width": {
                            "#type": "expression",
                            "value": "0"
                          },
                          "height": {
                            "#type": "expression",
                            "value": "35"
                          }
                        },
                        {
                          "#type": "prop_image_key",
                          "#prop": "",
                          "key": "${item}_${$cell.data}.png"
                        },
                        {
                          "#type": "prop_image_src",
                          "#prop": "",
                          "src": "${image_path}${item}_${$cell.data}.png"
                        },
                        {
                          "#type": "prop_scale",
                          "#prop": "",
                          "scale": {
                            "#type": "expression",
                            "value": "0.9"
                          }
                        }
                      ]
                    }
                  ]
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "link",
        "value": {
          "#type": "expression",
          "value": "'http://starmathsonline.s3.amazonaws.com/' + image_path"
        }
      },
      {
        "#type": "statement",
        "value": "function re_max(a, b, c, d){\n  if(a > b && a > c && a > d){return a;}\n  if(b > a && b > c && b > d){return b;}\n  if(c > a && c > b && c > d){return c;}\n  return d;\n}\n\n\nfunction re_min(a, b, c, d){\n  if(a < b && a < c && a < d){return a;}\n  if(b < a && b < c && b < d){return b;}\n  if(c < a && c < b && c < d){return c;}\n  return d;\n}"
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 1: Look for the longest and shortest ${item}.</u></br>\n  \n  <style>\n  img{margin: 10px; max-height: 40px}\n  </style>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < 4"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</br><img src='${link}${item}_${A[i]}.png'>"
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "A[i] == re_min(A[0], A[1], A[2], A[3])"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "----> Shortest"
                ]
              }
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "A[i] == re_max(A[0], A[1], A[2], A[3])"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "----> Longest"
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 2: Now let's drag and drop the longest and shortest ${item} into the correct order.</u></br>\n\n  \n  <style>\n  img{margin: 10px; max-height: 40px}\n  </style>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < 4"
        },
        "do": [
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "A[i] != re_max(A[0], A[1], A[2], A[3]) && A[i] != re_min(A[0], A[1], A[2], A[3])"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</br><img src='${link}${item}_${A[i]}.png'>"
                ]
              }
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "A[i] == re_min(A[0], A[1], A[2], A[3])"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</br>"
                ]
              }
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "A[i] == re_max(A[0], A[1], A[2], A[3])"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</br>"
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</br></br><img src='${link}${item}_${re_max(A[0], A[1], A[2], A[3])}.png'>, __________, __________, <img src='${link}${item}_${re_min(A[0], A[1], A[2], A[3])}.png'></br>"
            ]
          }
        ],
        "else": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</br></br><img src='${link}${item}_${re_min(A[0], A[1], A[2], A[3])}.png'>, __________, __________, <img src='${link}${item}_${re_max(A[0], A[1], A[2], A[3])}.png'></br>"
            ]
          }
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 3: Next look for the longer and shorter object from the remaining 2 ${item}.</u></br>\n    \n  <style>\n  img{margin: 10px; max-height: 40px}\n  </style>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < 4"
        },
        "do": [
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "A[i] == asc[1]"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</br><img src='${link}${item}_${A[i]}.png'> ---->shorter"
                ]
              }
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "A[i] == asc[2]"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</br><img src='${link}${item}_${A[i]}.png'> ----> longer"
                ]
              }
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "A[i] ==  re_max(A[0], A[1], A[2], A[3]) || A[i] ==  re_min(A[0], A[1], A[2], A[3])"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</br>"
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</br></br></br><img src='${link}${item}_${re_max(A[0], A[1], A[2], A[3])}.png'>, __________, __________, <img src='${link}${item}_${re_min(A[0], A[1], A[2], A[3])}.png'></br>"
            ]
          }
        ],
        "else": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</br></br></br><img src='${link}${item}_${re_min(A[0], A[1], A[2], A[3])}.png'>, __________, __________, <img src='${link}${item}_${re_max(A[0], A[1], A[2], A[3])}.png'></br>"
            ]
          }
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 4: Finally, drag and drop the longer and shorter ${item} into the correct order.</u></br>\n    \n  <style>\n  img{margin: 10px; max-height: 40px}\n  </style>"
        ]
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</br><img src='${link}${item}_${asc[3]}.png'></br>\n  <img src='${link}${item}_${asc[2]}.png'></br>\n<img src='${link}${item}_${asc[1]}.png'></br>\n<img src='${link}${item}_${asc[0]}.png'></br>"
            ]
          }
        ],
        "else": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "</br><img src='${link}${item}_${asc[0]}.png'></br>\n  <img src='${link}${item}_${asc[1]}.png'></br>\n<img src='${link}${item}_${asc[2]}.png'></br>\n<img src='${link}${item}_${asc[3]}.png'></br>"
            ]
          }
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="5gGw$H.=Ik)3{`iP(j!T" x="0" y="0">
    <field name="name">Y3.MG.LE.OL.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="^68J,$)07KvC79~[zz9i">
        <field name="name">image_path</field>
        <value name="value">
          <block type="string_value" id="jPmvj[rgmDZic)5N|DA.">
            <field name="value">Develop/ImageQAs/Y3.MG.LE.OL.1B/</field>
          </block>
        </value>
        <next>
          <block type="variable" id="FWRGA@2wV4O[o!oJdr:RO">
            <field name="name">background</field>
            <value name="value">
              <block type="string_array" id="iJ==(dHlAkQL5RE58[Pf">
                <field name="items">bg1|bg2|bg3|bg4|bg5</field>
              </block>
            </value>
            <next>
              <block type="variable" id="QCRoBLD{67}!upChP-{L">
                <field name="name">drop_background</field>
                <value name="value">
                  <block type="random_one" id="x(nV0m[bFs1g2:EvdmlU">
                    <value name="items">
                      <block type="expression" id="/_t@2vuD~wQ-Z,Wu#,/zX">
                        <field name="value">background</field>
                      </block>
                    </value>
                  </block>
                </value>
                <next>
                  <block type="background_shape" id="EN#?w;AHSW!~,@1Xm(|0p">
                    <statement name="#props">
                      <block type="prop_image_key" id="V3L`+v9#9w3/4GTaf/rl">
                        <field name="#prop"></field>
                        <value name="key">
                          <block type="string_value" id="]D(y7BWEOj3kiTcDT|}j">
                            <field name="value">${drop_background}.png</field>
                          </block>
                        </value>
                        <next>
                          <block type="prop_image_src" id="O6z@1/a#B:mN8|waz@24!j">
                            <field name="#prop"></field>
                            <value name="src">
                              <block type="string_value" id="h!|mKnw`ukGW;nf2ME1W">
                                <field name="value">${image_path}${drop_background}.png</field>
                              </block>
                            </value>
                          </block>
                        </next>
                      </block>
                    </statement>
                    <next>
                      <block type="variable" id="fBAVIh=p~7Zcrcff_c=3">
                        <field name="name">item</field>
                        <value name="value">
                          <block type="random_one" id="I?LdE%Wwnh+A1Z6f{4Ok">
                            <value name="items">
                              <block type="string_array" id="}0uTM)dK1ZS3k024yo/X">
                                <field name="items">pencils|ropes|ladders|ribbons</field>
                              </block>
                            </value>
                          </block>
                        </value>
                        <next>
                          <block type="variable" id="1#-Ck!LcgK}@1J:EE0k)8">
                            <field name="name">oder</field>
                            <value name="value">
                              <block type="string_array" id="zvbYaG0lhE#q_h,D1^xw">
                                <field name="items">longest to shortest|shortest to longest</field>
                              </block>
                            </value>
                            <next>
                              <block type="variable" id="UDD)F0AE@1AFxaNo+ZD=5">
                                <field name="name">type</field>
                                <value name="value">
                                  <block type="random_number" id="yDx`qd-`^5Qtggdn4Op2">
                                    <value name="min">
                                      <block type="expression" id="{Sy0C}#8K/nWnC,:s-.R">
                                        <field name="value">1</field>
                                      </block>
                                    </value>
                                    <value name="max">
                                      <block type="expression" id="/r+M5B+TK^l6_ZrHcWM-">
                                        <field name="value">2</field>
                                      </block>
                                    </value>
                                  </block>
                                </value>
                                <next>
                                  <block type="text_shape" id="~Uwh@1h{ncg_]ErVfEv%u">
                                    <statement name="#props">
                                      <block type="prop_position" id="1(q:xD?@2,U4DDg7W^:d}">
                                        <field name="#prop"></field>
                                        <value name="x">
                                          <block type="resp_x" id="OL+y`Q:jBp[F@2~Q19MOS">
                                            <field name="value">cx</field>
                                          </block>
                                        </value>
                                        <value name="y">
                                          <block type="expression" id="K0:q,f@2x|`lfACFj|aMI">
                                            <field name="value">60</field>
                                          </block>
                                        </value>
                                        <next>
                                          <block type="prop_text_contents" id="H}ue:|c$d(NEt_f!cist">
                                            <field name="#prop"></field>
                                            <value name="contents">
                                              <block type="string_value" id="j7[+{dqw]{cNNlBfCX`{">
                                                <field name="value">Compare and order the ${item} from ${oder[type-1]}.</field>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="prop_text_style" id="kV!]d+vC(0nh)Lj;A6aN">
                                                <field name="base">text</field>
                                                <statement name="#props">
                                                  <block type="prop_text_style_font_size" id="96Yp?OBPtÊ27;G5EtE">
                                                    <field name="#prop"></field>
                                                    <value name="fontSize">
                                                      <block type="expression" id="6,6$,$G_o5xDIm~)P~vP">
                                                        <field name="value">38</field>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="prop_text_style_fill" id=":rifONB{`Im9Rw{},QjZ">
                                                        <field name="#prop"></field>
                                                        <value name="fill">
                                                          <block type="string_value" id=";kaMKn;p@2vTj,6@2HB-?6">
                                                            <field name="value">black</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="prop_text_style_stroke" id="UcuSNV`tUPB8nZSEs|/+">
                                                            <field name="#prop"></field>
                                                            <value name="stroke">
                                                              <block type="string_value" id="X0[czfR^qUP#8:=+{NAK">
                                                                <field name="value">white</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="prop_text_style_stroke_thickness" id="Tp@15(eMKB}R{c@1W`^4F@1">
                                                                <field name="#prop"></field>
                                                                <value name="strokeThickness">
                                                                  <block type="expression" id="K9V;TTja!OzY5bTss}D@1">
                                                                    <field name="value">2</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </statement>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </statement>
                                    <next>
                                      <block type="variable" id="}vC$3;~wBYPu{;dCS,NE">
                                        <field name="name">A</field>
                                        <value name="value">
                                          <block type="random_many" id="u;.EUs@1}^k7w+2I|yA%:">
                                            <value name="count">
                                              <block type="expression" id="?qZX5RO{6+~fgetS;nqr">
                                                <field name="value">4</field>
                                              </block>
                                            </value>
                                            <value name="items">
                                              <block type="expression" id="mTf-ymn9Ql/1|Yl3c!-)">
                                                <field name="value">[1, 2, 3, 4, 5, 6, 7]</field>
                                              </block>
                                            </value>
                                          </block>
                                        </value>
                                        <next>
                                          <block type="variable" id="pOpv2jiS19F#]$U9H|#^">
                                            <field name="name">asc</field>
                                            <value name="value">
                                              <block type="func_sort" id="{yVp3Pto%^pw4X!},yu)" inline="true">
                                                <value name="value">
                                                  <block type="expression" id="5){EN4|iw:$JFVdc[gsX">
                                                    <field name="value">A</field>
                                                  </block>
                                                </value>
                                                <value name="by">
                                                  <block type="string_value" id="!$}te3qqV3ylfP9yBDua">
                                                    <field name="value">asc</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="if_then_else_block" id="+p,{BB?#@2$,9_rp5`IQ4">
                                                <value name="if">
                                                  <block type="expression" id="^.2lq/}T=4[v:L3B.o^M">
                                                    <field name="value">type == 1</field>
                                                  </block>
                                                </value>
                                                <statement name="then">
                                                  <block type="variable" id="$%v1e_[5_RFUp#hGXt9Z">
                                                    <field name="name">desc</field>
                                                    <value name="value">
                                                      <block type="func_sort" id="f1ym=([%MY1DXPlp~2dM" inline="true">
                                                        <value name="value">
                                                          <block type="expression" id="uxVQ+lU(#m)wO]7hfYd@1">
                                                            <field name="value">A</field>
                                                          </block>
                                                        </value>
                                                        <value name="by">
                                                          <block type="string_value" id="3)fH8O[Z!T:MzyTa?r?(">
                                                            <field name="value">desc</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="grid_shape" id="fu?71Uakt!.+FgC_bO?p">
                                                        <statement name="#props">
                                                          <block type="prop_grid_dimension" id="i5J2beF_LF1nN?GBgo.D">
                                                            <field name="#prop"></field>
                                                            <value name="rows">
                                                              <block type="expression" id="zR.Xe0MsuEK[Cv1,k;dx">
                                                                <field name="value">4</field>
                                                              </block>
                                                            </value>
                                                            <value name="cols">
                                                              <block type="expression" id="GZ[)s8enDt(VA4K?ii@2P">
                                                                <field name="value">1</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="prop_position" id="+vgkk=gI%,8j`%B~iLRi">
                                                                <field name="#prop"></field>
                                                                <value name="x">
                                                                  <block type="expression" id="`gw]`Z5v`%biXU]a(WTt">
                                                                    <field name="value">580</field>
                                                                  </block>
                                                                </value>
                                                                <value name="y">
                                                                  <block type="expression" id="qne4KTNE@2J(7f0wfEOuA">
                                                                    <field name="value">100</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_size" id="k_fl4iq/E{20qe.ws}z(">
                                                                    <field name="#prop"></field>
                                                                    <value name="width">
                                                                      <block type="expression" id="t2?rT6UXnw8X+WmiY10T">
                                                                        <field name="value">20</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="height">
                                                                      <block type="expression" id="l,Aa}-8YcPoNW2]`5.W`">
                                                                        <field name="value">340</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_grid_cell_source" id="N-7[dx?]sF00]zLf#~44">
                                                                        <field name="#prop">cell.source</field>
                                                                        <value name="value">
                                                                          <block type="expression" id="x;r=pyQ;o-CW{c+bZgw)">
                                                                            <field name="value">desc</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_anchor" id="fZ,WeH[8HO[UmJ/N6;TA">
                                                                            <field name="#prop">anchor</field>
                                                                            <value name="x">
                                                                              <block type="expression" id="YR9,Wpt42%1/sT[L8(w=">
                                                                                <field name="value">0</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="y">
                                                                              <block type="expression" id="zYqJ25y.#i(#+.u0,?=d">
                                                                                <field name="value">0</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_grid_show_borders" id="~|IKqL:RBWI59z@1/:t@25">
                                                                                <field name="#prop">#showBorders</field>
                                                                                <field name="value">FALSE</field>
                                                                                <next>
                                                                                  <block type="prop_grid_random" id="A2EhuSR)D:@2b/Fq[2[[T">
                                                                                    <field name="#prop"></field>
                                                                                    <field name="random">FALSE</field>
                                                                                    <next>
                                                                                      <block type="prop_grid_cell_template" id="NHCl.%z]V~4P~7o7t:Co">
                                                                                        <field name="variable">$cell</field>
                                                                                        <field name="#prop">cell.template</field>
                                                                                        <field name="#callback">$cell</field>
                                                                                        <statement name="body">
                                                                                          <block type="image_shape" id="P(MAzr!o`!9sxMA8Yx@1+">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_position" id="^_6{!6Vk.FEMFYn.FN)?">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="mMr{HA@2fI1u~v#Xfu_fB">
                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="O`U=tOwq)09C,GGxj:!{">
                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_size" id="D1VDnZg?kt@20%zjJn7Le">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="width">
                                                                                                      <block type="expression" id="5D?3mqWFSm1Aj[W[dL3Z">
                                                                                                        <field name="value">0</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="height">
                                                                                                      <block type="expression" id="T8]c53;(cCg$Ebm_:kq_">
                                                                                                        <field name="value">0</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_image_key" id="50K~KqT;kUCdPzcU(UBg">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="key">
                                                                                                          <block type="string_value" id="(^(mBaJ[0|#Ih8XaVulj">
                                                                                                            <field name="value">shape.png</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_image_src" id="w5^Tr1L/CE=Vn2:o)y1~">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="src">
                                                                                                              <block type="string_value" id="MYn_LbdlIRd~=H(Z2m6Q">
                                                                                                                <field name="value">${image_path}shape.png</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="drop_shape" id="i!!FeEvBdYa1{@2ZhzE#m">
                                                                                                <field name="multiple">FALSE</field>
                                                                                                <field name="resultMode">default</field>
                                                                                                <field name="groupName"></field>
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_value" id="ruk+xaE].YtXjns1Gyvd">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="qlkJPD)xZ?CjJWZ-uj@2k">
                                                                                                        <field name="value">$cell.data</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_position" id="zq.(E;i@2.:s4d3jE7DR#">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="x">
                                                                                                          <block type="expression" id="TnO1u(ReZ5tk!NNGl=!`">
                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id="`JLHS-W!er+3a!E`ff/!">
                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_size" id="K,MB`+wZB]K@1i_F~/H2)">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="width">
                                                                                                              <block type="expression" id="kUaNhHS:.3cFKZgharpt">
                                                                                                                <field name="value">342</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="height">
                                                                                                              <block type="expression" id="1PXR(GsT%.;wTg~ArM]a">
                                                                                                                <field name="value">74</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </statement>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </statement>
                                                <statement name="else">
                                                  <block type="grid_shape" id="}0@2T8S_yWu4KaRRtUj)T">
                                                    <statement name="#props">
                                                      <block type="prop_grid_dimension" id="BjJ[krbLU?OA9n)HUfO;">
                                                        <field name="#prop"></field>
                                                        <value name="rows">
                                                          <block type="expression" id="k-[v[Hp:X5QP,6WbPI-%">
                                                            <field name="value">4</field>
                                                          </block>
                                                        </value>
                                                        <value name="cols">
                                                          <block type="expression" id="$3yxDE`orC$mmB/}@2(U@1">
                                                            <field name="value">1</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="prop_position" id="`=+):1f4~}V5P9}skJi.">
                                                            <field name="#prop"></field>
                                                            <value name="x">
                                                              <block type="expression" id="(=/RUyI3R;3v).[[VeYn">
                                                                <field name="value">580</field>
                                                              </block>
                                                            </value>
                                                            <value name="y">
                                                              <block type="expression" id="v%Fo=D$k$/xA3mfdF{`7">
                                                                <field name="value">100</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="prop_size" id="U^1qo0_9+z)3,dot1w8b">
                                                                <field name="#prop"></field>
                                                                <value name="width">
                                                                  <block type="expression" id="CqClL1Z77s/[+7d.@1jgo">
                                                                    <field name="value">20</field>
                                                                  </block>
                                                                </value>
                                                                <value name="height">
                                                                  <block type="expression" id=".0^/T5[TnW`3M#0eRr.f">
                                                                    <field name="value">340</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_grid_cell_source" id="GAZlZy$Xf]]z:(W/xDUy">
                                                                    <field name="#prop">cell.source</field>
                                                                    <value name="value">
                                                                      <block type="expression" id="uZk;SgwZ--WoxeFwxum=">
                                                                        <field name="value">asc</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_anchor" id="hu??,=b?AGa:Mj;98c+z">
                                                                        <field name="#prop">anchor</field>
                                                                        <value name="x">
                                                                          <block type="expression" id="^D1{kdSMc$Tb3,Fm@1;;+">
                                                                            <field name="value">0</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id="[Q?K?MgOslw5W4FW9Slk">
                                                                            <field name="value">0</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_grid_show_borders" id="3CNb.OdSLFE@1mt{k=`Oc">
                                                                            <field name="#prop">#showBorders</field>
                                                                            <field name="value">FALSE</field>
                                                                            <next>
                                                                              <block type="prop_grid_random" id="(bxK;!!p89o5.58GJkpe">
                                                                                <field name="#prop"></field>
                                                                                <field name="random">FALSE</field>
                                                                                <next>
                                                                                  <block type="prop_grid_cell_template" id="%(ET0/h4eFfXE0o3HoiH">
                                                                                    <field name="variable">$cell</field>
                                                                                    <field name="#prop">cell.template</field>
                                                                                    <field name="#callback">$cell</field>
                                                                                    <statement name="body">
                                                                                      <block type="image_shape" id="KP@1P9U/J5#:j@1GzA?T:7">
                                                                                        <statement name="#props">
                                                                                          <block type="prop_position" id="Hh9U)kq/fKv@2J2Y{a[~K">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="heN?+;Xp,EB.LeBN[">
                                                                                                <field name="value">$cell.centerX</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="y-i_,%pBxeq#4^JpDOyI">
                                                                                                <field name="value">$cell.centerY</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_size" id="tRLk,^6pv#k1-eW,;|8^">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="width">
                                                                                                  <block type="expression" id="`@1!9]6k9ZGE08a6OqW?y">
                                                                                                    <field name="value">0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="height">
                                                                                                  <block type="expression" id="@1y?)9ZiUp^T0yH1[ic0@1">
                                                                                                    <field name="value">0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_image_key" id="h:IGUqv8~w)p!#+a$q=j">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="key">
                                                                                                      <block type="string_value" id=";,#_kWQ[{t^q5?d=?ybW">
                                                                                                        <field name="value">shape.png</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_image_src" id="K?f_OAe|d/4r4TGZSk2C">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="src">
                                                                                                          <block type="string_value" id="BzU6st?PKSv_5c{sP!{@1">
                                                                                                            <field name="value">${image_path}shape.png</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="drop_shape" id="8,t4fI@2lbddQ%mu_b@1ny">
                                                                                            <field name="multiple">FALSE</field>
                                                                                            <field name="resultMode">default</field>
                                                                                            <field name="groupName"></field>
                                                                                            <statement name="#props">
                                                                                              <block type="prop_value" id="uEx^vGm=4%P7+tqz-558">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="b{1{4?_iZ@1]FpE`pf,0D">
                                                                                                    <field name="value">$cell.data</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_position" id="E1+(cZpNNIvwS08-i[r+">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="b}GI/3e/8aR%}vC!lGcH">
                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="Aqs/CojKf(.t$$ev9Faq">
                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_size" id="V3pc/Gq/IL=B)ZIX.V)3">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="width">
                                                                                                          <block type="expression" id="Pp=Qd2?GI6tu`dvtQTOu">
                                                                                                            <field name="value">342</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="height">
                                                                                                          <block type="expression" id="OgG~YSx2zv^+MSH+UtN}">
                                                                                                            <field name="value">74</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </statement>
                                                  </block>
                                                </statement>
                                                <next>
                                                  <block type="grid_shape" id="o4]jSDMLb}3~8W7./$q;">
                                                    <statement name="#props">
                                                      <block type="prop_grid_dimension" id="|}Y}@1@2KM@2CvO/Iv4@2LE?">
                                                        <field name="#prop"></field>
                                                        <value name="rows">
                                                          <block type="expression" id="]XV8Tq./KzK9xbC?5^xr">
                                                            <field name="value">4</field>
                                                          </block>
                                                        </value>
                                                        <value name="cols">
                                                          <block type="expression" id="nk]Y[h)?PT%y=?a{Ep(-">
                                                            <field name="value">1</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="prop_position" id=",4jBf@1E]g}dL%p{-Zpob">
                                                            <field name="#prop"></field>
                                                            <value name="x">
                                                              <block type="expression" id="y4+`!.d@2x`x6ml8vCv~T">
                                                                <field name="value">50</field>
                                                              </block>
                                                            </value>
                                                            <value name="y">
                                                              <block type="expression" id="lVe959~tWxqUqZv/Vw%-">
                                                                <field name="value">100</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="prop_size" id="[]/2~`E7+?.[}:XMi[Kv">
                                                                <field name="#prop"></field>
                                                                <value name="width">
                                                                  <block type="expression" id="(5UWQ]{J$aZ]PQ:^3)7C">
                                                                    <field name="value">20</field>
                                                                  </block>
                                                                </value>
                                                                <value name="height">
                                                                  <block type="expression" id="SSG@1rnbvGvD[DwEwjK%!">
                                                                    <field name="value">340</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_grid_cell_source" id="YehM{YY}ZqfQi%tvLO@2|">
                                                                    <field name="#prop">cell.source</field>
                                                                    <value name="value">
                                                                      <block type="expression" id="=b_k@1@1J:0xL?jXk$0:zP">
                                                                        <field name="value">A</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_anchor" id="jIH(]AI%onUIU:@1k-Ic]">
                                                                        <field name="#prop">anchor</field>
                                                                        <value name="x">
                                                                          <block type="expression" id="^(;vAHw+9b2#0|..,Zl(">
                                                                            <field name="value">0</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id=";};@1!fDBUW.$#yr,2U,w">
                                                                            <field name="value">0</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_grid_show_borders" id="$(5#CWx9zi(VKQMV_uLI">
                                                                            <field name="#prop">#showBorders</field>
                                                                            <field name="value">FALSE</field>
                                                                            <next>
                                                                              <block type="prop_grid_random" id="~Cp-_PGHIF;:e_tqWe%/">
                                                                                <field name="#prop"></field>
                                                                                <field name="random">FALSE</field>
                                                                                <next>
                                                                                  <block type="prop_grid_cell_template" id="R=b-)P,hkZ[uH;(E3cFu">
                                                                                    <field name="variable">$cell</field>
                                                                                    <field name="#prop">cell.template</field>
                                                                                    <field name="#callback">$cell</field>
                                                                                    <statement name="body">
                                                                                      <block type="choice_custom_shape" id="A@1,Kkd;UyI=x@18,sa8Kj">
                                                                                        <field name="action">drag</field>
                                                                                        <value name="value">
                                                                                          <block type="expression" id="f6h[]KPW--$k:@2lDze;w">
                                                                                            <field name="value">$cell.data</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <statement name="template">
                                                                                          <block type="image_shape" id="fGUM:){6g@1Og/pu,C=~l">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_anchor" id="Q6:X8Q|/i-3awmIHnEPm">
                                                                                                <field name="#prop">anchor</field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="vm|bB,}q~5D/czi:6=W`">
                                                                                                    <field name="value">0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="BxJL_Ml[M%U#fq+(7^B,">
                                                                                                    <field name="value">0.5</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_position" id="?%v_B;H-[O;JhhZ%L{kj">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="3N72vu,0AsBTFRFDD{i]">
                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="NtNa2Qu{1JLf?L8JDqq9">
                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_size" id="U%lej9E2:c%H)S~hH5iR">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="width">
                                                                                                          <block type="expression" id="oM~2TE5t06UwFXT})K%~">
                                                                                                            <field name="value">0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="height">
                                                                                                          <block type="expression" id=")|)[hWU4f$!0~@1ZYb:_y">
                                                                                                            <field name="value">35</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_image_key" id="ClSv?i0-TIQo-~xm$PYv">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="key">
                                                                                                              <block type="string_value" id="Qk`{CF[.bynLU@1WA}^Hj">
                                                                                                                <field name="value">${item}_${$cell.data}.png</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_image_src" id="]p`8pus.mvK8[q0hyD_%">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="src">
                                                                                                                  <block type="string_value" id="y?c2b;TYvL26HVQJXyo]">
                                                                                                                    <field name="value">${image_path}${item}_${$cell.data}.png</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_scale" id="i%wL,r?dJkMNAlZ$rBpH">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="scale">
                                                                                                                      <block type="expression" id="GEcINCe^4wC!xM}LC(Tu">
                                                                                                                        <field name="value">0.9</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                          </block>
                                                                                        </statement>
                                                                                      </block>
                                                                                    </statement>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </statement>
                                                    <next>
                                                      <block type="variable" id="q:H`!q#D(?u5J1+S,f];">
                                                        <field name="name">link</field>
                                                        <value name="value">
                                                          <block type="expression" id="JwubnS1i}|(u+{q-o4Tl">
                                                            <field name="value">'http://starmathsonline.s3.amazonaws.com/' + image_path</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="statement" id="?memS2E)mb9wo|tc2Z{(">
                                                            <field name="value">function re_max(a, b, c, d){
  if(a &gt; b &amp;&amp; a &gt; c &amp;&amp; a &gt; d){return a;}
  if(b &gt; a &amp;&amp; b &gt; c &amp;&amp; b &gt; d){return b;}
  if(c &gt; a &amp;&amp; c &gt; b &amp;&amp; c &gt; d){return c;}
  return d;
}


function re_min(a, b, c, d){
  if(a &lt; b &amp;&amp; a &lt; c &amp;&amp; a &lt; d){return a;}
  if(b &lt; a &amp;&amp; b &lt; c &amp;&amp; b &lt; d){return b;}
  if(c &lt; a &amp;&amp; c &lt; b &amp;&amp; c &lt; d){return c;}
  return d;
}
                                                            </field>
                                                            <next>
                                                              <block type="partial_explanation" id="{I6L5w3erPOYw$s7|U%G">
                                                                <value name="value">
                                                                  <block type="string_value" id="Eqj1~t]]r/u~?;w/Aw)4">
                                                                    <field name="value">&lt;u&gt;Step 1: Look for the longest and shortest ${item}.&lt;/u&gt;&lt;/br&gt;
  
  &lt;style&gt;
  img{margin: 10px; max-height: 40px}
  &lt;/style&gt;
                                                                    </field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="variable" id="|x|Y5xe}}RC/Nq;?aT`!">
                                                                    <field name="name">i</field>
                                                                    <value name="value">
                                                                      <block type="expression" id="zhZkN6=RyZ^YF}hj^E$P">
                                                                        <field name="value">0</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="while_do_block" id="J7P=ygU7(z5RoZ+oRy]f">
                                                                        <value name="while">
                                                                          <block type="expression" id="tk,kCSeM?RRGPcsj;@2OW">
                                                                            <field name="value">i &lt; 4</field>
                                                                          </block>
                                                                        </value>
                                                                        <statement name="do">
                                                                          <block type="partial_explanation" id="~X]Hvv{=v(3,mx8VkR(L">
                                                                            <value name="value">
                                                                              <block type="string_value" id="OrZ}uC4wD7ekD,@2xmZs5">
                                                                                <field name="value">&lt;/br&gt;&lt;img src='${link}${item}_${A[i]}.png'&gt;</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="if_then_block" id=":p=$8hMk.$AyE3qd#^|R">
                                                                                <value name="if">
                                                                                  <block type="expression" id="dUt)_2}~/FKVg.9wv)ln">
                                                                                    <field name="value">A[i] == re_min(A[0], A[1], A[2], A[3])</field>
                                                                                  </block>
                                                                                </value>
                                                                                <statement name="then">
                                                                                  <block type="partial_explanation" id="rZHR%2#0@2QRVJsy1SMx(">
                                                                                    <value name="value">
                                                                                      <block type="string_value" id="eCxVO4g]VHsVg}/JOD|N">
                                                                                        <field name="value">----&gt; Shortest</field>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="if_then_block" id="E2K:u2jdy^)4)mS@1Wmv@1">
                                                                                    <value name="if">
                                                                                      <block type="expression" id=";~0xo7$k,2F0FRTQ)X[L">
                                                                                        <field name="value">A[i] == re_max(A[0], A[1], A[2], A[3])</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <statement name="then">
                                                                                      <block type="partial_explanation" id="KhHIPgb0J}!pzP~Y2zDA">
                                                                                        <value name="value">
                                                                                          <block type="string_value" id="hGPyGO}FmU%rb2h-U3Fm">
                                                                                            <field name="value">----&gt; Longest</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <next>
                                                                                      <block type="variable" id=")=PMl@2am`faL0k%y^(=O">
                                                                                        <field name="name">i</field>
                                                                                        <value name="value">
                                                                                          <block type="expression" id="%5^.=wTm?FQxHXM?1@26t">
                                                                                            <field name="value">i+1</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="end_partial_explanation" id=".t1c[P7ME|`#ux2yoOB5">
                                                                            <next>
                                                                              <block type="partial_explanation" id="p57$I6f`9Kh^aA;Sr~`B">
                                                                                <value name="value">
                                                                                  <block type="string_value" id="n:_$ivwkyqrX)Il{lwt1">
                                                                                    <field name="value">&lt;u&gt;Step 2: Now let's drag and drop the longest and shortest ${item} into the correct order.&lt;/u&gt;&lt;/br&gt;

  
  &lt;style&gt;
  img{margin: 10px; max-height: 40px}
  &lt;/style&gt;
                                                                                    </field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="variable" id="?l!9|~18v+JHW0!:6fP-">
                                                                                    <field name="name">i</field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="27+d~Rf[9XjGc5%ro(q;">
                                                                                        <field name="value">0</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="while_do_block" id="_T^Td@18?z2Y4CpfROU^B">
                                                                                        <value name="while">
                                                                                          <block type="expression" id="Mb=SpF/8wNy4g!nX%!">
                                                                                            <field name="value">i &lt; 4</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <statement name="do">
                                                                                          <block type="if_then_block" id="A?(ON[(PeZUG0hF)^(9L">
                                                                                            <value name="if">
                                                                                              <block type="expression" id="^gN+[dGsgZ0xgm.A|N+x">
                                                                                                <field name="value">A[i] != re_max(A[0], A[1], A[2], A[3]) &amp;&amp; A[i] != re_min(A[0], A[1], A[2], A[3])</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <statement name="then">
                                                                                              <block type="partial_explanation" id="Fpadjl@2KO;xdN9U?aIs=">
                                                                                                <value name="value">
                                                                                                  <block type="string_value" id="cNKZ1PoO@2:H;5S7:x}CC">
                                                                                                    <field name="value">&lt;/br&gt;&lt;img src='${link}${item}_${A[i]}.png'&gt;</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="if_then_block" id="dAP)3LOVccpB4OM$/vzA">
                                                                                                <value name="if">
                                                                                                  <block type="expression" id=":ygK;6j]nScq{n%YU:|w">
                                                                                                    <field name="value">A[i] == re_min(A[0], A[1], A[2], A[3])</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <statement name="then">
                                                                                                  <block type="partial_explanation" id="d|1_}G0c+m0!LTKK.xD]">
                                                                                                    <value name="value">
                                                                                                      <block type="string_value" id="F|wWn1BmMsR[PG6%`x?%">
                                                                                                        <field name="value">&lt;/br&gt;</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="if_then_block" id="-f)cDrM87l$EC[SjUko?">
                                                                                                    <value name="if">
                                                                                                      <block type="expression" id="X`{MAuEw@2GDs+a@1uWwg:">
                                                                                                        <field name="value">A[i] == re_max(A[0], A[1], A[2], A[3])</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <statement name="then">
                                                                                                      <block type="partial_explanation" id="GUHZQ`qsl=6mk7-7L@18c">
                                                                                                        <value name="value">
                                                                                                          <block type="string_value" id="5lT-4J6BqMSf_qCtxblr">
                                                                                                            <field name="value">&lt;/br&gt;</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="variable" id=".JBYpk2)rYWT+@1n}.=q3">
                                                                                                        <field name="name">i</field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id="gkVTH=9oMfV.1HZ{cLZy">
                                                                                                            <field name="value">i+1</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="if_then_else_block" id="E6Vf@1Ld7aq6PR-6OUDQE">
                                                                                            <value name="if">
                                                                                              <block type="expression" id="ZR%p85:?/as.|_oZXd0F">
                                                                                                <field name="value">type == 1</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <statement name="then">
                                                                                              <block type="partial_explanation" id="I!h4yv0EVrg[u(10tN65">
                                                                                                <value name="value">
                                                                                                  <block type="string_value" id="yqw(#h[s2OT=T0@1s;Gl5">
                                                                                                    <field name="value">&lt;/br&gt;&lt;/br&gt;&lt;img src='${link}${item}_${re_max(A[0], A[1], A[2], A[3])}.png'&gt;, __________, __________, &lt;img src='${link}${item}_${re_min(A[0], A[1], A[2], A[3])}.png'&gt;&lt;/br&gt;</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <statement name="else">
                                                                                              <block type="partial_explanation" id=";o)Fmp:^sl6(vf8hN8VL">
                                                                                                <value name="value">
                                                                                                  <block type="string_value" id="~jQ2;+JX8~k;/d/q2W_0">
                                                                                                    <field name="value">&lt;/br&gt;&lt;/br&gt;&lt;img src='${link}${item}_${re_min(A[0], A[1], A[2], A[3])}.png'&gt;, __________, __________, &lt;img src='${link}${item}_${re_max(A[0], A[1], A[2], A[3])}.png'&gt;&lt;/br&gt;</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="end_partial_explanation" id="w:^Y7.?^#?]II`)+k|@1!">
                                                                                                <next>
                                                                                                  <block type="partial_explanation" id="q5!jn#MaBvAZ7{ZyjS.d">
                                                                                                    <value name="value">
                                                                                                      <block type="string_value" id="wCzvIc]SzJS`b}nDxa{s">
                                                                                                        <field name="value">&lt;u&gt;Step 3: Next look for the longer and shorter object from the remaining 2 ${item}.&lt;/u&gt;&lt;/br&gt;
    
  &lt;style&gt;
  img{margin: 10px; max-height: 40px}
  &lt;/style&gt;
                                                                                                        </field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="variable" id="{0dSgt9Z,sNiti`GQ:a#">
                                                                                                        <field name="name">i</field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id="7=i}@1QCF[Ah?1CS7:z31">
                                                                                                            <field name="value">0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="while_do_block" id="iq[=:dj4?x8f/Q{@1@2[HJ">
                                                                                                            <value name="while">
                                                                                                              <block type="expression" id="Y{PSvz?y^tKoa]^u(:8n">
                                                                                                                <field name="value">i &lt; 4</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <statement name="do">
                                                                                                              <block type="if_then_block" id="w`S+m_=Q`u$1GPyLsr$h">
                                                                                                                <value name="if">
                                                                                                                  <block type="expression" id="WNsPH?K2MO.l^U].]E3F">
                                                                                                                    <field name="value">A[i] == asc[1]</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <statement name="then">
                                                                                                                  <block type="partial_explanation" id="q?m`?ctd@2D$[kp!0ryER">
                                                                                                                    <value name="value">
                                                                                                                      <block type="string_value" id="18?o7$9%J~@19N@2O)kv_`">
                                                                                                                        <field name="value">&lt;/br&gt;&lt;img src='${link}${item}_${A[i]}.png'&gt; ----&gt;shorter</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="if_then_block" id="~^kR1OZ/^RQ@2o!XMoJ)3">
                                                                                                                    <value name="if">
                                                                                                                      <block type="expression" id="`c:!@2=or]]dWacq9|tT)">
                                                                                                                        <field name="value">A[i] == asc[2]</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <statement name="then">
                                                                                                                      <block type="partial_explanation" id="D_;f!zFG]JjFcGG,e|Am">
                                                                                                                        <value name="value">
                                                                                                                          <block type="string_value" id="(?SNK}e#QT;Ia%@1=ofA8">
                                                                                                                            <field name="value">&lt;/br&gt;&lt;img src='${link}${item}_${A[i]}.png'&gt; ----&gt; longer</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="if_then_block" id="u1Ds}U8xO]9]l,7raIzd">
                                                                                                                        <value name="if">
                                                                                                                          <block type="expression" id="5hesC+{+g13R1xj0jC;[">
                                                                                                                            <field name="value">A[i] ==  re_max(A[0], A[1], A[2], A[3]) || A[i] ==  re_min(A[0], A[1], A[2], A[3])</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <statement name="then">
                                                                                                                          <block type="partial_explanation" id="$sp/pQV5w]Dy7c]P#mj#">
                                                                                                                            <value name="value">
                                                                                                                              <block type="string_value" id="mn4N|%y@1y$nAyOZ6|i:T">
                                                                                                                                <field name="value">&lt;/br&gt;</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="variable" id="SViWwJ#ZeGZ[E~1`5OBN">
                                                                                                                            <field name="name">i</field>
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="Gh9@2QE^lv2-MpHu+?_|t">
                                                                                                                                <field name="value">i+1</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="if_then_else_block" id="x%!R;yQR)/Hl,vNM{=lQ">
                                                                                                                <value name="if">
                                                                                                                  <block type="expression" id="E+M[iC4,RN4f#64:N?v@2">
                                                                                                                    <field name="value">type == 1</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <statement name="then">
                                                                                                                  <block type="partial_explanation" id="CBeFzIyX(o_m8nXlI)Ep">
                                                                                                                    <value name="value">
                                                                                                                      <block type="string_value" id="}La~lB+9OVNT5}jwwRA(">
                                                                                                                        <field name="value">&lt;/br&gt;&lt;/br&gt;&lt;/br&gt;&lt;img src='${link}${item}_${re_max(A[0], A[1], A[2], A[3])}.png'&gt;, __________, __________, &lt;img src='${link}${item}_${re_min(A[0], A[1], A[2], A[3])}.png'&gt;&lt;/br&gt;</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <statement name="else">
                                                                                                                  <block type="partial_explanation" id="b!HT+WC$/G7!+R1NWHR^">
                                                                                                                    <value name="value">
                                                                                                                      <block type="string_value" id="s!@2fA/T4d#;QNL,c~/?h">
                                                                                                                        <field name="value">&lt;/br&gt;&lt;/br&gt;&lt;/br&gt;&lt;img src='${link}${item}_${re_min(A[0], A[1], A[2], A[3])}.png'&gt;, __________, __________, &lt;img src='${link}${item}_${re_max(A[0], A[1], A[2], A[3])}.png'&gt;&lt;/br&gt;</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="end_partial_explanation" id="-YI%T1i/skb]#.T?TwOH">
                                                                                                                    <next>
                                                                                                                      <block type="partial_explanation" id="$(pHl7pnODvqkGHs;@2o1">
                                                                                                                        <value name="value">
                                                                                                                          <block type="string_value" id="g[|69@10tg9gLQAY~iv,T">
                                                                                                                            <field name="value">&lt;u&gt;Step 4: Finally, drag and drop the longer and shorter ${item} into the correct order.&lt;/u&gt;&lt;/br&gt;
    
  &lt;style&gt;
  img{margin: 10px; max-height: 40px}
  &lt;/style&gt;
                                                                                                                            </field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="if_then_else_block" id="{}8K-^w^e%@2y56C@1sctV">
                                                                                                                            <value name="if">
                                                                                                                              <block type="expression" id="R%dj#^}smU^7R;nGzH5M">
                                                                                                                                <field name="value">type == 1</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <statement name="then">
                                                                                                                              <block type="partial_explanation" id="2d#,uf.NQpIEs^.%cXn]">
                                                                                                                                <value name="value">
                                                                                                                                  <block type="string_value" id="{wkU8eWUxd2{V,uxB}[[">
                                                                                                                                    <field name="value">&lt;/br&gt;&lt;img src='${link}${item}_${asc[3]}.png'&gt;&lt;/br&gt;
  &lt;img src='${link}${item}_${asc[2]}.png'&gt;&lt;/br&gt;
&lt;img src='${link}${item}_${asc[1]}.png'&gt;&lt;/br&gt;
&lt;img src='${link}${item}_${asc[0]}.png'&gt;&lt;/br&gt;
                                                                                                                                    </field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <statement name="else">
                                                                                                                              <block type="partial_explanation" id="k_thfn2|qnc[4}1gOQ6Z">
                                                                                                                                <value name="value">
                                                                                                                                  <block type="string_value" id="RidZ:g)F,5[:ZNO(~b+t">
                                                                                                                                    <field name="value">&lt;/br&gt;&lt;img src='${link}${item}_${asc[0]}.png'&gt;&lt;/br&gt;
  &lt;img src='${link}${item}_${asc[1]}.png'&gt;&lt;/br&gt;
&lt;img src='${link}${item}_${asc[2]}.png'&gt;&lt;/br&gt;
&lt;img src='${link}${item}_${asc[3]}.png'&gt;&lt;/br&gt;
                                                                                                                                    </field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="end_partial_explanation" id="]CC~5%WJsG@2Oi0}u:=Md"></block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
</xml>
END_XML]] */