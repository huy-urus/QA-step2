
module.exports = [
  {
    "#type": "question",
    "name": "Y3.MG.MA.OWSM.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/Y3.MG.MA.OWSM.1B/"
      },
      {
        "#type": "variable",
        "name": "background",
        "value": {
          "#type": "string_array",
          "items": "bg1|bg2|bg3|bg4|bg5"
        }
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_one",
          "items": {
            "#type": "expression",
            "value": "background"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "variable",
        "name": "type",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "2"
          }
        }
      },
      {
        "#type": "variable",
        "name": "x",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "variable",
        "name": "x1",
        "value": {
          "#type": "expression",
          "value": "x"
        }
      },
      {
        "#type": "variable",
        "name": "x2",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "x2 == x"
        },
        "do": [
          {
            "#type": "variable",
            "name": "x2",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "1"
              },
              "max": {
                "#type": "expression",
                "value": "15"
              }
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "y",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "y == x || y == x2"
        },
        "do": [
          {
            "#type": "variable",
            "name": "y",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "1"
              },
              "max": {
                "#type": "expression",
                "value": "15"
              }
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "i1",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "16"
          }
        }
      },
      {
        "#type": "variable",
        "name": "i2",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "16"
          }
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i2 == i1"
        },
        "do": [
          {
            "#type": "variable",
            "name": "i2",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "1"
              },
              "max": {
                "#type": "expression",
                "value": "16"
              }
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "i3",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "16"
          }
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i3 == i1 || i3 == i2"
        },
        "do": [
          {
            "#type": "variable",
            "name": "i3",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "1"
              },
              "max": {
                "#type": "expression",
                "value": "16"
              }
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "kg",
        "value": {
          "#type": "expression",
          "value": "[x1, x2, y]"
        }
      },
      {
        "#type": "variable",
        "name": "kg",
        "value": {
          "#type": "func",
          "name": "shuffle",
          "args": [
            {
              "#type": "expression",
              "value": "kg"
            }
          ]
        }
      },
      {
        "#type": "variable",
        "name": "item",
        "value": {
          "#type": "expression",
          "value": "[i1, i2, i3]"
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "resp_x",
                  "value": "cx"
                },
                "y": {
                  "#type": "expression",
                  "value": "60"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "Select the objects that have the same mass."
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "38"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    }
                  ]
                }
              }
            ]
          }
        ],
        "else": [
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "resp_x",
                  "value": "cx"
                },
                "y": {
                  "#type": "expression",
                  "value": "60"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "Select the object that is the odd one out."
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "38"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    }
                  ]
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "1"
            },
            "cols": {
              "#type": "expression",
              "value": "3"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "resp_x",
              "value": "cx"
            },
            "y": {
              "#type": "expression",
              "value": "100"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "750"
            },
            "height": {
              "#type": "expression",
              "value": "200"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "expression",
              "value": "kg"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_cell_template",
            "variable": "$cell",
            "#prop": "cell.template",
            "#callback": "$cell",
            "body": [
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_anchor",
                    "#prop": "anchor",
                    "x": {
                      "#type": "expression",
                      "value": "0.5"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "0.5"
                    }
                  },
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "0"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "shape.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}shape.png"
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "1"
            },
            "cols": {
              "#type": "expression",
              "value": "3"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "resp_x",
              "value": "cx"
            },
            "y": {
              "#type": "expression",
              "value": "100"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "750"
            },
            "height": {
              "#type": "expression",
              "value": "200"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "func",
              "name": "arrayOfNumber",
              "args": [
                {
                  "#type": "expression",
                  "value": "3"
                },
                {
                  "#type": "expression",
                  "value": "1"
                }
              ]
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_cell_template",
            "variable": "$cell",
            "#prop": "cell.template",
            "#callback": "$cell",
            "body": [
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_anchor",
                    "#prop": "anchor",
                    "x": {
                      "#type": "expression",
                      "value": "0.5"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "0.7"
                    }
                  },
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "0"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "${item[$cell.data -1]}.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}${item[$cell.data -1]}.png"
                  },
                  {
                    "#type": "prop_scale",
                    "#prop": "",
                    "scale": {
                      "#type": "expression",
                      "value": "0.9"
                    }
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "variable",
        "name": "k",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < 3"
        },
        "do": [
          {
            "#type": "variable",
            "name": "k[i]",
            "value": {
              "#type": "expression",
              "value": "kg[i]"
            }
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "kg[i] == x2"
            },
            "then": [
              {
                "#type": "variable",
                "name": "k[i]",
                "value": {
                  "#type": "expression",
                  "value": "x1"
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "1"
            },
            "cols": {
              "#type": "expression",
              "value": "3"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "resp_x",
              "value": "cx"
            },
            "y": {
              "#type": "expression",
              "value": "100"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "750"
            },
            "height": {
              "#type": "expression",
              "value": "200"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "expression",
              "value": "k"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_cell_template",
            "variable": "$cell",
            "#prop": "cell.template",
            "#callback": "$cell",
            "body": [
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${$cell.data} kg"
                  },
                  {
                    "#type": "prop_anchor",
                    "#prop": "anchor",
                    "x": {
                      "#type": "expression",
                      "value": "0"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "-0.7"
                    }
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "42"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "func",
            "name": "addAnswers",
            "args": [
              {
                "#type": "expression",
                "value": "[x1,x2]"
              }
            ]
          }
        ],
        "else": [
          {
            "#type": "func",
            "name": "addAnswers",
            "args": [
              {
                "#type": "expression",
                "value": "y"
              }
            ]
          }
        ]
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "1"
            },
            "cols": {
              "#type": "expression",
              "value": "3"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "resp_x",
              "value": "cx"
            },
            "y": {
              "#type": "expression",
              "value": "350"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "750"
            },
            "height": {
              "#type": "expression",
              "value": "100"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "func",
              "name": "arrayOfNumber",
              "args": [
                {
                  "#type": "expression",
                  "value": "3"
                },
                {
                  "#type": "expression",
                  "value": "0"
                }
              ]
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_cell_template",
            "variable": "$cell",
            "#prop": "cell.template",
            "#callback": "$cell",
            "body": [
              {
                "#type": "if_then_else_block",
                "if": {
                  "#type": "expression",
                  "value": "type == 1"
                },
                "then": [
                  {
                    "#type": "choice_custom_shape",
                    "action": "click-many",
                    "value": {
                      "#type": "expression",
                      "value": "kg[$cell.data]"
                    },
                    "template": {
                      "#callback": "$choice",
                      "variable": "$choice",
                      "body": [
                        {
                          "#type": "image_shape",
                          "#props": [
                            {
                              "#type": "prop_anchor",
                              "#prop": "anchor",
                              "x": {
                                "#type": "expression",
                                "value": "0.5"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "0.5"
                              }
                            },
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "$cell.centerX"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "$cell.centerY"
                              }
                            },
                            {
                              "#type": "prop_size",
                              "#prop": "",
                              "width": {
                                "#type": "expression",
                                "value": "0"
                              },
                              "height": {
                                "#type": "expression",
                                "value": "0"
                              }
                            },
                            {
                              "#type": "prop_image_key",
                              "#prop": "",
                              "key": "o${$cell.data + 1}.png"
                            },
                            {
                              "#type": "prop_image_src",
                              "#prop": "",
                              "src": "${image_path}o${$cell.data + 1}.png"
                            }
                          ]
                        }
                      ]
                    }
                  }
                ],
                "else": [
                  {
                    "#type": "choice_custom_shape",
                    "action": "click-one",
                    "value": {
                      "#type": "expression",
                      "value": "kg[$cell.data]"
                    },
                    "template": {
                      "#callback": "$choice",
                      "variable": "$choice",
                      "body": [
                        {
                          "#type": "image_shape",
                          "#props": [
                            {
                              "#type": "prop_anchor",
                              "#prop": "anchor",
                              "x": {
                                "#type": "expression",
                                "value": "0.5"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "0.5"
                              }
                            },
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "$cell.centerX"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "$cell.centerY"
                              }
                            },
                            {
                              "#type": "prop_size",
                              "#prop": "",
                              "width": {
                                "#type": "expression",
                                "value": "0"
                              },
                              "height": {
                                "#type": "expression",
                                "value": "0"
                              }
                            },
                            {
                              "#type": "prop_image_key",
                              "#prop": "",
                              "key": "o${$cell.data + 1}.png"
                            },
                            {
                              "#type": "prop_image_src",
                              "#prop": "",
                              "src": "${image_path}o${$cell.data + 1}.png"
                            }
                          ]
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "dem",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "da",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "j",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < 3"
        },
        "do": [
          {
            "#type": "variable",
            "name": "da[i]",
            "value": {
              "#type": "expression",
              "value": "''"
            }
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "k[i] == x1"
            },
            "then": [
              {
                "#type": "variable",
                "name": "da[i]",
                "value": {
                  "#type": "expression",
                  "value": "'border: 5px solid green;'"
                }
              },
              {
                "#type": "variable",
                "name": "dem[j]",
                "value": {
                  "#type": "expression",
                  "value": "i"
                }
              },
              {
                "#type": "variable",
                "name": "j",
                "value": {
                  "#type": "expression",
                  "value": "j+1"
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "link",
        "value": {
          "#type": "expression",
          "value": "'http://starmathsonline.s3.amazonaws.com/' + image_path"
        }
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 1: Get to know the different objects that are 1 kilogram in mass.</u>\n\n<style>\ntable{width: 900px}\ntd{border: 1px solid black; text-align: center; padding: 10px}\n</style>\n<center>\n<table>\n<tr>\n<td>Mass</td><td>Object 1</td><td>Object 2</td>\n</tr>\n\n<tr>\n<td>${x1} kg</td>\n<td><img src='${link}${item[dem[0]]}.png'></td>\n<td><img src='${link}${item[dem[1]]}.png'></td>\n</tr>\n\n</table>\n</center>\n\n</br>\n<p style='background-color: rgba(206, 241, 241, 0.6); display: inline;'>*The shape and size of the object does not matter if the mass is the same.*</p>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 2: Choose the objects that have the same mass.</u></br></br>\n\n<style>\ntable{width: 900px}\ntd{border: 1px solid black; text-align: center; padding: 10px}\n</style>\n<center>\n<table>\n\n<tr>\n<td style='${da[0]};'><img src='${link}${item[0]}.png'></br> ${k[0]} kg</td>\n<td style='${da[1]};'><img src='${link}${item[1]}.png'></br> ${k[1]} kg</td>\n<td style='${da[2]};'><img src='${link}${item[2]}.png'></br> ${k[2]} kg</td>\n</tr>\n\n</table>\n</center>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          }
        ],
        "else": [
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i< 3"
            },
            "do": [
              {
                "#type": "variable",
                "name": "da[i]",
                "value": {
                  "#type": "expression",
                  "value": "''"
                }
              },
              {
                "#type": "if_then_block",
                "if": {
                  "#type": "expression",
                  "value": "k[i] != x1"
                },
                "then": [
                  {
                    "#type": "variable",
                    "name": "da[i]",
                    "value": {
                      "#type": "expression",
                      "value": "'border: 5px solid green;'"
                    }
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 2: Choose the object that is the odd one out.</u></br></br>\n\n<style>\ntable{width: 900px}\ntd{border: 1px solid black; text-align: center; padding: 10px}\n</style>\n<center>\n<table>\n\n<tr>\n<td style='${da[0]};'><img src='${link}${item[0]}.png'></br> ${k[0]} kg</td>\n<td style='${da[1]};'><img src='${link}${item[1]}.png'></br> ${k[1]} kg</td>\n<td style='${da[2]};'><img src='${link}${item[2]}.png'></br> ${k[2]} kg</td>\n</tr>\n\n</table>\n</center>"
            ]
          }
        ]
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="5gGw$H.=Ik)3{`iP(j!T" x="0" y="0">
    <field name="name">Y3.MG.MA.OWSM.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="^68J,$)07KvC79~[zz9i">
        <field name="name">image_path</field>
        <value name="value">
          <block type="string_value" id="jPmvj[rgmDZic)5N|DA.">
            <field name="value">Develop/ImageQAs/Y3.MG.MA.OWSM.1B/</field>
          </block>
        </value>
        <next>
          <block type="variable" id="FWRGA@2wV4O[o!oJdr:RO">
            <field name="name">background</field>
            <value name="value">
              <block type="string_array" id="iJ==(dHlAkQL5RE58[Pf">
                <field name="items">bg1|bg2|bg3|bg4|bg5</field>
              </block>
            </value>
            <next>
              <block type="variable" id="QCRoBLD{67}!upChP-{L">
                <field name="name">drop_background</field>
                <value name="value">
                  <block type="random_one" id="x(nV0m[bFs1g2:EvdmlU">
                    <value name="items">
                      <block type="expression" id="/_t@2vuD~wQ-Z,Wu#,/zX">
                        <field name="value">background</field>
                      </block>
                    </value>
                  </block>
                </value>
                <next>
                  <block type="background_shape" id="EN#?w;AHSW!~,@1Xm(|0p">
                    <statement name="#props">
                      <block type="prop_image_key" id="V3L`+v9#9w3/4GTaf/rl">
                        <field name="#prop"></field>
                        <value name="key">
                          <block type="string_value" id="]D(y7BWEOj3kiTcDT|}j">
                            <field name="value">${drop_background}.png</field>
                          </block>
                        </value>
                        <next>
                          <block type="prop_image_src" id="O6z@1/a#B:mN8|waz@24!j">
                            <field name="#prop"></field>
                            <value name="src">
                              <block type="string_value" id="h!|mKnw`ukGW;nf2ME1W">
                                <field name="value">${image_path}${drop_background}.png</field>
                              </block>
                            </value>
                          </block>
                        </next>
                      </block>
                    </statement>
                    <next>
                      <block type="input_param" id="hzOgPQ4vabROkeiDbwV/" inline="true">
                        <field name="name">numberOfCorrect</field>
                        <value name="value">
                          <block type="expression" id="RmUys@1(jG(@2A$!XxJAPQ">
                            <field name="value">0</field>
                          </block>
                        </value>
                        <next>
                          <block type="input_param" id="cgu@2M::O5O[iXGW^5p.M" inline="true">
                            <field name="name">numberOfIncorrect</field>
                            <value name="value">
                              <block type="expression" id="ilsqIzW?56adfinQve=g">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="variable" id="}OBMY^Dw|lcyd$wtH:2q">
                                <field name="name">range</field>
                                <value name="value">
                                  <block type="expression" id="rf];0Hi3O1S;++[V9{;d">
                                    <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="D8^PZLjdWaWQ_(EB2y8H">
                                    <field name="name">type</field>
                                    <value name="value">
                                      <block type="random_number" id="x%BI^!RLXg@1,?dCqp4YF">
                                        <value name="min">
                                          <block type="expression" id="|O9Qd?n%kCJV22Wb;th}">
                                            <field name="value">1</field>
                                          </block>
                                        </value>
                                        <value name="max">
                                          <block type="expression" id="PCvqeW,JA-`gwvgyfW;k">
                                            <field name="value">2</field>
                                          </block>
                                        </value>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="variable" id="C7;3;5Kj.@2K[}xYAU(Bs">
                                        <field name="name">x</field>
                                        <value name="value">
                                          <block type="random_number" id="(oMuz==vd`C#_5l_cI~e">
                                            <value name="min">
                                              <block type="expression" id="1djMl@1@1$fZQ)%?zKL]jh">
                                                <field name="value">1</field>
                                              </block>
                                            </value>
                                            <value name="max">
                                              <block type="expression" id="m(-j-Fw(;BsGif+tR@2{N">
                                                <field name="value">15</field>
                                              </block>
                                            </value>
                                          </block>
                                        </value>
                                        <next>
                                          <block type="variable" id="{6p{#FJEhzi|OJ3$o4):">
                                            <field name="name">x1</field>
                                            <value name="value">
                                              <block type="expression" id="nJb:aF5NFmhjNzzPbuLI">
                                                <field name="value">x</field>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id="d9pSI#mrEQ/4~YhLe[R(">
                                                <field name="name">x2</field>
                                                <value name="value">
                                                  <block type="random_number" id="O=^Kt2XX~.ev.Y#%6Yxz">
                                                    <value name="min">
                                                      <block type="expression" id="i?=GJI(#fnZat~PsZu0d">
                                                        <field name="value">1</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="$]N:40M,iA+ETyC4Y!Q:">
                                                        <field name="value">15</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="while_do_block" id="7.c/sS[2(J9V0_qS9NE4">
                                                    <value name="while">
                                                      <block type="expression" id="Atx_p|k[)m~U4e0@2W6">
                                                        <field name="value">x2 == x</field>
                                                      </block>
                                                    </value>
                                                    <statement name="do">
                                                      <block type="variable" id="g6CmS@15sMv0YP!YX)8j-">
                                                        <field name="name">x2</field>
                                                        <value name="value">
                                                          <block type="random_number" id="ZTKi]j0z,sg:8Zr=.hmb">
                                                            <value name="min">
                                                              <block type="expression" id="DKqBtzzr5.#us!GEK#kP">
                                                                <field name="value">1</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="Bmen#e+2yuU{wGSWzS9,">
                                                                <field name="value">15</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </statement>
                                                    <next>
                                                      <block type="variable" id="Ap)(#RQe3jVglk+y[fXn">
                                                        <field name="name">y</field>
                                                        <value name="value">
                                                          <block type="random_number" id="VWx@1,:6wA)i5^|WxYF-L">
                                                            <value name="min">
                                                              <block type="expression" id="Dp[:HNJdr``@1?AR!lIFI">
                                                                <field name="value">1</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="^q^@2-{RGq8k;mNQ:pVt!">
                                                                <field name="value">15</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="while_do_block" id="7xsfEcJL)+XALhKnWB3o">
                                                            <value name="while">
                                                              <block type="expression" id="3q8r--w(1#I@1N#4tU|NS">
                                                                <field name="value">y == x || y == x2</field>
                                                              </block>
                                                            </value>
                                                            <statement name="do">
                                                              <block type="variable" id="3c^x)=^Gf]KrH89Jtvha">
                                                                <field name="name">y</field>
                                                                <value name="value">
                                                                  <block type="random_number" id="x@2;MFEHO~Wc5HRyFq6Rq">
                                                                    <value name="min">
                                                                      <block type="expression" id="O?3I`TqRtcmFOM^fM9(}">
                                                                        <field name="value">1</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="max">
                                                                      <block type="expression" id="dh}otWhBY?~^BsTQ7r0V">
                                                                        <field name="value">15</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </statement>
                                                            <next>
                                                              <block type="variable" id="DBTN{NN#,PSSwBW^ve|L">
                                                                <field name="name">i1</field>
                                                                <value name="value">
                                                                  <block type="random_number" id="o.3X{BO{-9l2XL5j%q|Y">
                                                                    <value name="min">
                                                                      <block type="expression" id="`+/}DsZLHm?+Gnk:PGY/">
                                                                        <field name="value">1</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="max">
                                                                      <block type="expression" id="TEf:@1^S+ycXp7@1cBF~1}">
                                                                        <field name="value">16</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="variable" id="(1f,f/]g[!JK.)Y`(u/!">
                                                                    <field name="name">i2</field>
                                                                    <value name="value">
                                                                      <block type="random_number" id="v@2=.y_@1c]PNuH{/3?Ag~">
                                                                        <value name="min">
                                                                          <block type="expression" id="JE,L,V`^aSI!6eW3{E7L">
                                                                            <field name="value">1</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="max">
                                                                          <block type="expression" id="UqqTN.A8H@2;:A{!AN2kl">
                                                                            <field name="value">16</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="while_do_block" id="Q~5(REImPDFd(s2pp=.a">
                                                                        <value name="while">
                                                                          <block type="expression" id="X@1yMUHbr_v5pjFGlo_i5">
                                                                            <field name="value">i2 == i1</field>
                                                                          </block>
                                                                        </value>
                                                                        <statement name="do">
                                                                          <block type="variable" id="3eD@1gQ@2d]rB8c:V=qw~g">
                                                                            <field name="name">i2</field>
                                                                            <value name="value">
                                                                              <block type="random_number" id="yiaAwJPrD5s~7hpJ@1nj3">
                                                                                <value name="min">
                                                                                  <block type="expression" id="o+P4Nv+^asZ`#}|UZqN_">
                                                                                    <field name="value">1</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="max">
                                                                                  <block type="expression" id="U60ZJaj!wx.NWmZ^06XD">
                                                                                    <field name="value">16</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="variable" id=".?Tb|z#oV##d@1Uz4ebQg">
                                                                            <field name="name">i3</field>
                                                                            <value name="value">
                                                                              <block type="random_number" id="ud]@1BbSm^_c0Qfnq6cZC">
                                                                                <value name="min">
                                                                                  <block type="expression" id=",uiz@2uB(psoxhBcRLFNq">
                                                                                    <field name="value">1</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="max">
                                                                                  <block type="expression" id="??+NbH@1N,`!M8x5Cd+sW">
                                                                                    <field name="value">16</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="while_do_block" id="=@2nitU-8Iwk!MC)aRf=h">
                                                                                <value name="while">
                                                                                  <block type="expression" id="NP7et,=-@1^ut^oChdNzY">
                                                                                    <field name="value">i3 == i1 || i3 == i2</field>
                                                                                  </block>
                                                                                </value>
                                                                                <statement name="do">
                                                                                  <block type="variable" id="j.=DA9|@1b=-#.-xC-qjT">
                                                                                    <field name="name">i3</field>
                                                                                    <value name="value">
                                                                                      <block type="random_number" id="A4-|%3xkX/^T!V66wMra">
                                                                                        <value name="min">
                                                                                          <block type="expression" id="oG9b{]Xcy%[V3VL_(Az@2">
                                                                                            <field name="value">1</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="max">
                                                                                          <block type="expression" id="u/#GegDvl3mw{.p6ZrNt">
                                                                                            <field name="value">16</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="variable" id="M650mslo:`_w(684]::@1">
                                                                                    <field name="name">kg</field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="y5g@2#0cFkU$WJL{iBodl">
                                                                                        <field name="value">[x1, x2, y]</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="variable" id="lN4c8_?i+YYf|7i:F8jH">
                                                                                        <field name="name">kg</field>
                                                                                        <value name="value">
                                                                                          <block type="func_shuffle" id="J~^:4^z,Gp~_ttI`@1CrU" inline="true">
                                                                                            <value name="value">
                                                                                              <block type="expression" id="@1AH{iO`{O|1/Ii-V]pZ}">
                                                                                                <field name="value">kg</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="variable" id="k%Pp]6;5OA7u)19V(C3b">
                                                                                            <field name="name">item</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="iCR.o%c@2qi@1AG[vKbn-$">
                                                                                                <field name="value">[i1, i2, i3]</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="if_then_else_block" id="#tp#tk,6|.AL%#fm[:08">
                                                                                                <value name="if">
                                                                                                  <block type="expression" id="5BV(Q`?Q]Ik!$,.SP;TR">
                                                                                                    <field name="value">type == 1</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <statement name="then">
                                                                                                  <block type="text_shape" id="}bWn4b63mQVW^qfl5.wa">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_position" id="gmyM7lmztFL/tr$x+C~_">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="x">
                                                                                                          <block type="resp_x" id="@2v6Lt[ZPL0m{Zg3gyA?+">
                                                                                                            <field name="value">cx</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id="(;HD`:J+GRcq?-60#F-M">
                                                                                                            <field name="value">60</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_contents" id="2M{R(9AaB^J0NTlr!!M.">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="contents">
                                                                                                              <block type="string_value" id="N)q3m.b6=@2n?[D.fu=K[">
                                                                                                                <field name="value">Select the objects that have the same mass.</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style" id="hAa$sG6R#8@1ZN{7AgHCD">
                                                                                                                <field name="base">text</field>
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_text_style_font_size" id="3{AamA}=ZZJyp8(MKK~x">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="fontSize">
                                                                                                                      <block type="expression" id=")qO^Q-h)PaWdmdEK@1r(-">
                                                                                                                        <field name="value">38</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_fill" id="=~dk1kwugU5+,=^sXc,A">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="fill">
                                                                                                                          <block type="string_value" id="YN+S[(oc.`,WZMXjRME5">
                                                                                                                            <field name="value">black</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <statement name="else">
                                                                                                  <block type="text_shape" id="}eSJWZ7Ih#=_I.xm|THM">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_position" id="ffFf,WO@1ZJX:]sO8`:[E">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="x">
                                                                                                          <block type="resp_x" id="|_$03VR0HenG%`WA?-Ce">
                                                                                                            <field name="value">cx</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id="C=3]r%1:MVVRpGBpCh6|">
                                                                                                            <field name="value">60</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_contents" id="-`G^xK8~ub{cAko`qu7a">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="contents">
                                                                                                              <block type="string_value" id="Ss8{F@1ieZ+hhriOk/Q3S">
                                                                                                                <field name="value">Select the object that is the odd one out.</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style" id="i@12hT(1sD-Pb(wM3MLJ.">
                                                                                                                <field name="base">text</field>
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_text_style_font_size" id="BtpE]@1i%1i4:#1|Vj8T)">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="fontSize">
                                                                                                                      <block type="expression" id="_ogn,-W/vj86rwXcS`qa">
                                                                                                                        <field name="value">38</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_fill" id="#Jm]bgB:X`hrRbQm|+ug">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="fill">
                                                                                                                          <block type="string_value" id="B=DswDMJ!U!xS/D,rC8)">
                                                                                                                            <field name="value">black</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="grid_shape" id="4c~6@1%XrVEs{{KCX.}}1">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_grid_dimension" id="([FLgxFg~tv/M}-|dUlX">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="rows">
                                                                                                          <block type="expression" id="^0uWqW/Q;CwQ#sAFsZIM">
                                                                                                            <field name="value">1</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="cols">
                                                                                                          <block type="expression" id="=:(R97XU`2@2+@1qV@2|BKD">
                                                                                                            <field name="value">3</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_position" id="#BrWQVNfgfbsmKlq]KG@1">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="x">
                                                                                                              <block type="resp_x" id="tYYS^S^L-U0VJVTW@1Fr3">
                                                                                                                <field name="value">cx</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="J@2WW;ZcuLt8;@1sdw-L8j">
                                                                                                                <field name="value">100</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_size" id="!R~9$^jSg_q^ou^6Z6+#">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="width">
                                                                                                                  <block type="expression" id="@2zOu]+}V6@2Vr+!UEY4}1">
                                                                                                                    <field name="value">750</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="height">
                                                                                                                  <block type="expression" id="uwH|u/muim|tMD_OUlvl">
                                                                                                                    <field name="value">200</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_grid_cell_source" id="biVoxhi=5K$lCX|qbFWc">
                                                                                                                    <field name="#prop">cell.source</field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="qdgFh^u;f$Qu4q^b6RXC">
                                                                                                                        <field name="value">kg</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_anchor" id="Xv@1f|_H;oMkS$;522tPb">
                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="8w$D#q[;%(gOSTyVnwkO">
                                                                                                                            <field name="value">0.5</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="jhIRH6m%=OeL8@2}S0htc">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_grid_random" id="]#=^97m+/9WX!$Vj,?W@1">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <field name="random">FALSE</field>
                                                                                                                            <next>
                                                                                                                              <block type="prop_grid_show_borders" id="]bi2g^;A0d!Yjw$/f:g%">
                                                                                                                                <field name="#prop">#showBorders</field>
                                                                                                                                <field name="value">FALSE</field>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_grid_cell_template" id="dotM~={][!Q#1R+59rKj">
                                                                                                                                    <field name="variable">$cell</field>
                                                                                                                                    <field name="#prop">cell.template</field>
                                                                                                                                    <field name="#callback">$cell</field>
                                                                                                                                    <statement name="body">
                                                                                                                                      <block type="image_shape" id="IW:vAGKS8yxdt9c@1`^)q">
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_anchor" id="](w.T#el079Q-S%l,{#P">
                                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id=".w6}rkK{w-?z5Ido:$V)">
                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="$@26T(NV@1:YOPh)K9#VJT">
                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_position" id="o|BjvQv5v|4AD#wI#Hjm">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="x">
                                                                                                                                                  <block type="expression" id=";%R9]vK^==nfxm_?i{.v">
                                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <value name="y">
                                                                                                                                                  <block type="expression" id="DzLwkj`x/0$RVKL@1}VHP">
                                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_size" id="cwM;+)EXj?U?.$f/5F`8">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="width">
                                                                                                                                                      <block type="expression" id="T.L97gH=]c%@1k:bp[.27">
                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="height">
                                                                                                                                                      <block type="expression" id="+5LAivg2%@2ga^Ul(zH^k">
                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_image_key" id="7G9pNcb5@2bp$tp]14dz%">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="key">
                                                                                                                                                          <block type="string_value" id="3|)_AX)HvEDWz/mJG76H">
                                                                                                                                                            <field name="value">shape.png</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_image_src" id="AQ^/;`V7lEK5Ab;RF@2Tg">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="src">
                                                                                                                                                              <block type="string_value" id="OJasuFgn;46@2We1[CG)T">
                                                                                                                                                                <field name="value">${image_path}shape.png</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="grid_shape" id="41Mb@1;Q^4Z]kpNO-7w~{">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_grid_dimension" id="XJr_I9x}dYNn)HF%/WCL">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="rows">
                                                                                                              <block type="expression" id="p%pKe]XfscGY=UQepT]d">
                                                                                                                <field name="value">1</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="cols">
                                                                                                              <block type="expression" id="n!rQ6cB-rs}Wsx-MT-,Z">
                                                                                                                <field name="value">3</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_position" id="H!hsr_$VR-7HzP2aNc46">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="resp_x" id="|==C7$_p7Jc8T0$^R$gj">
                                                                                                                    <field name="value">cx</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="^x+a0ek?}}O3{SBE/WG)">
                                                                                                                    <field name="value">100</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_size" id="9QH8gM,?V8I^~rtAA?J^">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="width">
                                                                                                                      <block type="expression" id="?4xujAYvTY:Pa1qdn0]8">
                                                                                                                        <field name="value">750</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="height">
                                                                                                                      <block type="expression" id="qk6KmaJHK:2)AKB:S3bR">
                                                                                                                        <field name="value">200</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_grid_cell_source" id="8bsy8YZQ$%:$?ai{|UIk">
                                                                                                                        <field name="#prop">cell.source</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="func_array_of_number" id="Bx2|t@2e?8kX4?7WYxhiZ" inline="true">
                                                                                                                            <value name="items">
                                                                                                                              <block type="expression" id=")o^,#nvQmv^D%6-oHM+q">
                                                                                                                                <field name="value">3</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="from">
                                                                                                                              <block type="expression" id="haXUqT-uR@1tdC9J:bDTH">
                                                                                                                                <field name="value">1</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_anchor" id="cLr8xYbGg[E.6/xN6uVI">
                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="eKXxLjT{}!h7Qh(`kA)h">
                                                                                                                                <field name="value">0.5</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="kgOxY6VNo?2c/oSp2gH[">
                                                                                                                                <field name="value">0</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_grid_random" id="5UM-.{%!SJChy0jlz+Ww">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <field name="random">FALSE</field>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_grid_show_borders" id="-K^Z?Y[XcE8_;1wRE@2Yv">
                                                                                                                                    <field name="#prop">#showBorders</field>
                                                                                                                                    <field name="value">FALSE</field>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_grid_cell_template" id="yZ{C_Vn|~lt-Wt4q!7Y;">
                                                                                                                                        <field name="variable">$cell</field>
                                                                                                                                        <field name="#prop">cell.template</field>
                                                                                                                                        <field name="#callback">$cell</field>
                                                                                                                                        <statement name="body">
                                                                                                                                          <block type="image_shape" id="=S5l7dPK]]dUYrqjt0TZ">
                                                                                                                                            <statement name="#props">
                                                                                                                                              <block type="prop_anchor" id="}Gx+{{3l4qOF2H_vKbPu">
                                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                                <value name="x">
                                                                                                                                                  <block type="expression" id="@2TA85b#VIoPY@2vTBxwY%">
                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <value name="y">
                                                                                                                                                  <block type="expression" id="B#0?Try+s~%(;1)ULu`[">
                                                                                                                                                    <field name="value">0.7</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_position" id="8ux;O(4vkrRecxlSKM%E">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="x">
                                                                                                                                                      <block type="expression" id="`xg$~:@1=to,MF^hZ14z5">
                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="y">
                                                                                                                                                      <block type="expression" id="VP-ML_({2F9YVw8ZSETB">
                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_size" id="#XHFeU]h9FNY@2i$$j4KQ">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="width">
                                                                                                                                                          <block type="expression" id="{Wyyo9!B!EBZ~Wmcv:|L">
                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="height">
                                                                                                                                                          <block type="expression" id="Q2nJvxg76D43ab`78ESU">
                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_image_key" id="t|iG!z6@2[levP[o`bNHb">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="key">
                                                                                                                                                              <block type="string_value" id="akX,9sXmi?ET;@24dj7XK">
                                                                                                                                                                <field name="value">${item[$cell.data -1]}.png</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_image_src" id="Bv-q+KFaboL}A[atrs;+">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="src">
                                                                                                                                                                  <block type="string_value" id="cnzH^{fkb=@1v5n~WO0zt">
                                                                                                                                                                    <field name="value">${image_path}${item[$cell.data -1]}.png</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_scale" id="W^P]1|b@1^$G3WM(anjhO">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="scale">
                                                                                                                                                                      <block type="expression" id="5+5CNR)5i.L(#4C`uKj}">
                                                                                                                                                                        <field name="value">0.9</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="variable" id="Rj-U~4:O5)}tEPLF`T_/">
                                                                                                            <field name="name">i</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="N^+09d[k4G!;MvB41-0s">
                                                                                                                <field name="value">0</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="variable" id="$4h,CM#+]pgWmJu~{L3,">
                                                                                                                <field name="name">k</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="UG%BX6[NlKVI]N68Je[1">
                                                                                                                    <field name="value">[]</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="while_do_block" id=")96X+MH(sZd@2|kqJ4s%i">
                                                                                                                    <value name="while">
                                                                                                                      <block type="expression" id="aza.~ZV19=Jxj#FZN$[r">
                                                                                                                        <field name="value">i &lt; 3</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <statement name="do">
                                                                                                                      <block type="variable" id="M}rT5gY.,C;[6Nh@2h0r7">
                                                                                                                        <field name="name">k[i]</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id="0k:|XZ9b^,h.xjm8_ctU">
                                                                                                                            <field name="value">kg[i]</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="if_then_block" id="}?jwV=H.i_8I)g8Wu}Jt">
                                                                                                                            <value name="if">
                                                                                                                              <block type="expression" id="C@29@2EhpcpaTQb$913O|(">
                                                                                                                                <field name="value">kg[i] == x2</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <statement name="then">
                                                                                                                              <block type="variable" id=",~r]Z-,)R?#GBa%+_]8[">
                                                                                                                                <field name="name">k[i]</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="cEWaMJAfFS/_T}{7iD53">
                                                                                                                                    <field name="value">x1</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="variable" id="@2vCBO4=!3EZ(0Qi-@2K9Z">
                                                                                                                                <field name="name">i</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="%`Z@1EVsvmr6TT?~P5Ksc">
                                                                                                                                    <field name="value">i+1</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="grid_shape" id="X}$r%y{E;)(@25gG:e%%e">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_grid_dimension" id="m+Mp`Y.|JXDr(`N8Dt%v">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="rows">
                                                                                                                              <block type="expression" id="AE/Grl~E?EUm`:S.c,c}">
                                                                                                                                <field name="value">1</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="cols">
                                                                                                                              <block type="expression" id="CU=XL$eI$3;h9MS_PyWG">
                                                                                                                                <field name="value">3</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_position" id="ch-aRW3@1k?x!to[K@2NhT">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="resp_x" id=":ZVyGVJNrfSk-W{T?K)(">
                                                                                                                                    <field name="value">cx</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="Pqo`)AC7g#vd1s^l|EW]">
                                                                                                                                    <field name="value">100</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_size" id="=r6P{cF2@2h.o;~p4Z!Q~">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="width">
                                                                                                                                      <block type="expression" id="-@2u.)=?t:Uz#`}?0NRi7">
                                                                                                                                        <field name="value">750</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="height">
                                                                                                                                      <block type="expression" id="I?cDQ.bQk05v@1o=-KMZi">
                                                                                                                                        <field name="value">200</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_grid_cell_source" id="@2,-P0]Z[X^xSnW/E:TZL">
                                                                                                                                        <field name="#prop">cell.source</field>
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="expression" id="[lEkz|Z=d|Wi-H+@1gPVR">
                                                                                                                                            <field name="value">k</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_anchor" id="i!?acVUka#2Sz6UBY`d=">
                                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id="S!/jA,$d9%g%Q%nsZ3Z+">
                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="CN8aS(AdP}e+?fPlF71?">
                                                                                                                                                <field name="value">0</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_grid_random" id="[i:_,4zi1ynIUUNS2^wy">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <field name="random">FALSE</field>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_grid_show_borders" id="R%7YG]l`/Wy(OoU5r9;$">
                                                                                                                                                    <field name="#prop">#showBorders</field>
                                                                                                                                                    <field name="value">FALSE</field>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_grid_cell_template" id="$c85NQrAec26;=1n=S:2">
                                                                                                                                                        <field name="variable">$cell</field>
                                                                                                                                                        <field name="#prop">cell.template</field>
                                                                                                                                                        <field name="#callback">$cell</field>
                                                                                                                                                        <statement name="body">
                                                                                                                                                          <block type="text_shape" id="gN,])lPAD1JV}IqJ5Gz#">
                                                                                                                                                            <statement name="#props">
                                                                                                                                                              <block type="prop_position" id="Z352@1^|!7J4c[lHW}I}`">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="x">
                                                                                                                                                                  <block type="expression" id="yI(fe+`_i^T.E41FL@2Kh">
                                                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <value name="y">
                                                                                                                                                                  <block type="expression" id="/B@17pl=GAx=i5ER1DHm3">
                                                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_text_contents" id="-2g00fdWO!1r!@1@20p|bB">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="contents">
                                                                                                                                                                      <block type="string_value" id="CRi1mo$c?lq5|:q4{RNr">
                                                                                                                                                                        <field name="value">${$cell.data} kg</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_anchor" id="H1cE`djL6pwyY@1PcJh`w">
                                                                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                                                                        <value name="x">
                                                                                                                                                                          <block type="expression" id="GdRvhfwvyQH?)Iv9U9O;">
                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <value name="y">
                                                                                                                                                                          <block type="expression" id="oaDeO~6BI{W8iti)+Ci|">
                                                                                                                                                                            <field name="value">-0.7</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_text_style" id="oxjQ@2@1N.-;u5)eW7$v3]">
                                                                                                                                                                            <field name="base">text</field>
                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                              <block type="prop_text_style_font_size" id="B$,!GJ8p1|F2_ely,Sk,">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="fontSize">
                                                                                                                                                                                  <block type="expression" id="t3`mlL@17PxFWpP$h{QRi">
                                                                                                                                                                                    <field name="value">42</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_text_style_fill" id=")y}QA|hk+L`y)r@2]ZTL!">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="fill">
                                                                                                                                                                                      <block type="string_value" id="(([yJF6ILGX]Ljiaai2=">
                                                                                                                                                                                        <field name="value">black</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </statement>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="if_then_else_block" id="n-^wAt=gt({vrHkL+w@2]">
                                                                                                                            <value name="if">
                                                                                                                              <block type="expression" id="Cu`:F,Z@1OcA..R`D-N@1@2">
                                                                                                                                <field name="value">type == 1</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <statement name="then">
                                                                                                                              <block type="func_add_answer" id="yn.Xg^Hq[ogd8#5V16IV" inline="true">
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="|3?ZmG??)R8ElGp#2,K1">
                                                                                                                                    <field name="value">[x1,x2]</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <statement name="else">
                                                                                                                              <block type="func_add_answer" id="5]|]4EG()VbwXNq4+tN2" inline="true">
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id=":`1gp]5x,4CBc@1~DD,Rf">
                                                                                                                                    <field name="value">y</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="grid_shape" id="9]xtlLZ5m-MCn5([D@2[w">
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_grid_dimension" id="bSHuHO@20@1cuW8U73@2!r0">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="rows">
                                                                                                                                      <block type="expression" id="n2l?/@16VW2^N0K-W::Z]">
                                                                                                                                        <field name="value">1</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="cols">
                                                                                                                                      <block type="expression" id="C`[mm?@1?K@2Du{OnnfV+g">
                                                                                                                                        <field name="value">3</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_position" id="5R5JIF+1qmvF0[tRMurr">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="resp_x" id="pU~{!N1)pmCHfFQgvgPn">
                                                                                                                                            <field name="value">cx</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id="oSqnMsU3)XXv[jj@1BH5-">
                                                                                                                                            <field name="value">350</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_size" id="fp@1$4zS5x`s[aalP6vjh">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="width">
                                                                                                                                              <block type="expression" id="`nzTq4@1U{MHV6FrZm|RJ">
                                                                                                                                                <field name="value">750</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="height">
                                                                                                                                              <block type="expression" id="./u+EAotDt{K``rg%k`O">
                                                                                                                                                <field name="value">100</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_grid_cell_source" id="4I#@22JU@1=pBjEfJ|dFSX">
                                                                                                                                                <field name="#prop">cell.source</field>
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="func_array_of_number" id="Uz/kN^pZB2vO8zPlKl(:" inline="true">
                                                                                                                                                    <value name="items">
                                                                                                                                                      <block type="expression" id=":caPXa%,yo_gVgMk,:(_">
                                                                                                                                                        <field name="value">3</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="from">
                                                                                                                                                      <block type="expression" id="yN#RC8NbO!xW@1|.8S9`2">
                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_anchor" id="r}J)rCZo5Gr3GrA3hcI1">
                                                                                                                                                    <field name="#prop">anchor</field>
                                                                                                                                                    <value name="x">
                                                                                                                                                      <block type="expression" id="?ar#6a8+ft^uG.KVz9OS">
                                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="y">
                                                                                                                                                      <block type="expression" id=")6j#;E(i!/U3]66x%vF3">
                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_grid_random" id="Ha|}_i4j?1}g%@1[rcIAk">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <field name="random">FALSE</field>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_grid_show_borders" id="04BVvTeU`?[;.R[0Hkn0">
                                                                                                                                                            <field name="#prop">#showBorders</field>
                                                                                                                                                            <field name="value">FALSE</field>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_grid_cell_template" id="0umkApltXvRk;}:Kfksp">
                                                                                                                                                                <field name="variable">$cell</field>
                                                                                                                                                                <field name="#prop">cell.template</field>
                                                                                                                                                                <field name="#callback">$cell</field>
                                                                                                                                                                <statement name="body">
                                                                                                                                                                  <block type="if_then_else_block" id=".0V.G|DadM-MJ8I}$}5S">
                                                                                                                                                                    <value name="if">
                                                                                                                                                                      <block type="expression" id="Sw91#6z}k3I/??_hVsa?">
                                                                                                                                                                        <field name="value">type == 1</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <statement name="then">
                                                                                                                                                                      <block type="choice_custom_shape" id="30{2Dg],1=Jgt^E]oh#?">
                                                                                                                                                                        <field name="action">click-many</field>
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="expression" id="jJB[?GnH_a!u7#Gcy_wl">
                                                                                                                                                                            <field name="value">kg[$cell.data]</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <statement name="template">
                                                                                                                                                                          <block type="image_shape" id=";]#0@2Sudc_,6p5@2X?+l3">
                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                              <block type="prop_anchor" id="z|;5_D#N0v.R6p7Uwe~+">
                                                                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                                                                <value name="x">
                                                                                                                                                                                  <block type="expression" id="EvAtVdH6dkS[VuXgBVz;">
                                                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <value name="y">
                                                                                                                                                                                  <block type="expression" id="0yoicX;2@2|uYkSZrCZ4J">
                                                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_position" id="aC7s;?5D9h{e0wKrr(x5">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="x">
                                                                                                                                                                                      <block type="expression" id="ej_2Dq^)}t+_jmWJo{C_">
                                                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <value name="y">
                                                                                                                                                                                      <block type="expression" id="5YkFA{9hqr~bu#oru/UU">
                                                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_size" id="WrCcYL}(o+T#Upo$vE,U">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="width">
                                                                                                                                                                                          <block type="expression" id="@2g~)0@1uYF=FZYZ_R?1wm">
                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <value name="height">
                                                                                                                                                                                          <block type="expression" id=".t}~$|A.y@1Q%n#2D^{tc">
                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_image_key" id="_`@2mYTQDNDn;2ySBltDe">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <value name="key">
                                                                                                                                                                                              <block type="string_value" id="e{er5A@2/.1s,e|;GX~SS">
                                                                                                                                                                                                <field name="value">o${$cell.data + 1}.png</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_image_src" id=";@247lJ)@2n=yh[ZD]fB(q">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <value name="src">
                                                                                                                                                                                                  <block type="string_value" id="aYLcZ!-EVeY{P!F.`3lK">
                                                                                                                                                                                                    <field name="value">${image_path}o${$cell.data + 1}.png</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </statement>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                    <statement name="else">
                                                                                                                                                                      <block type="choice_custom_shape" id=")U?^^6Z99V}sD^Q@2:x3k">
                                                                                                                                                                        <field name="action">click-one</field>
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="expression" id="@1Tab]mgT$lm;GWc|=lV%">
                                                                                                                                                                            <field name="value">kg[$cell.data]</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <statement name="template">
                                                                                                                                                                          <block type="image_shape" id="h2^5xaUbL%}DQbIU]aCM">
                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                              <block type="prop_anchor" id="LWud[9%$4(;y+%z^!|s0">
                                                                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                                                                <value name="x">
                                                                                                                                                                                  <block type="expression" id=";TX~74MnY/?qV~BxX..E">
                                                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <value name="y">
                                                                                                                                                                                  <block type="expression" id="h.kc`pfu9M52XDFK9fRB">
                                                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_position" id="-OBNi??EaG_S{xWR^Rl@1">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="x">
                                                                                                                                                                                      <block type="expression" id="y6Ch[Mc7PmK2-AQ^VD}h">
                                                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <value name="y">
                                                                                                                                                                                      <block type="expression" id="PEJb.;@1$YFzExH.tHX:O">
                                                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_size" id="@1h}jbvFZ1#/mh}QPO~Jn">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="width">
                                                                                                                                                                                          <block type="expression" id="MdgwKX_~Nu43TdKk$p$X">
                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <value name="height">
                                                                                                                                                                                          <block type="expression" id="T^%mxK%%^Jp5=VyEK^!?">
                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_image_key" id="eZgb96SO}:(yDX9TVp?B">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <value name="key">
                                                                                                                                                                                              <block type="string_value" id="r1+f@1/N=@2:aAIUE#eY19">
                                                                                                                                                                                                <field name="value">o${$cell.data + 1}.png</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_image_src" id="^[NFK}E@2n3Vj|gq+E=,4">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <value name="src">
                                                                                                                                                                                                  <block type="string_value" id="zdO`Sr~mrDRf8a^$WSvs">
                                                                                                                                                                                                    <field name="value">${image_path}o${$cell.data + 1}.png</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </statement>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <next>
                                                                                                                                  <block type="variable" id="OE#.-_04B!^|(ppvE)Hb">
                                                                                                                                    <field name="name">dem</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="D{S98b]oXKyi([qLGxe$">
                                                                                                                                        <field name="value">[]</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="variable" id="(4UU{~JRLw_x{xw)uVy.">
                                                                                                                                        <field name="name">da</field>
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="expression" id="Mmf2Ljbf)#SJUmjCu#5s">
                                                                                                                                            <field name="value">[]</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="variable" id="VBEg[h97t/@2I%rG9S6$o">
                                                                                                                                            <field name="name">j</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id=",|,SWlk8%I44OymJbNhv">
                                                                                                                                                <field name="value">0</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="variable" id=")WG^MTl^)~,cv4ID_yzF">
                                                                                                                                                <field name="name">i</field>
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="expression" id=":VRS.b[0zF?Q~Qjg8HkB">
                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="while_do_block" id="78MMAqO#)1[YlYUZ/p]8">
                                                                                                                                                    <value name="while">
                                                                                                                                                      <block type="expression" id="D2GdZwS!`/-4]Av?~:$Y">
                                                                                                                                                        <field name="value">i &lt; 3</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <statement name="do">
                                                                                                                                                      <block type="variable" id="i312qrlv3b@1Ft_7OFR]u">
                                                                                                                                                        <field name="name">da[i]</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="AEb$w-NUW-ufa?lXDG@1t">
                                                                                                                                                            <field name="value">''</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="if_then_block" id="w`=;peT]Tj4/@2;K;}yEA">
                                                                                                                                                            <value name="if">
                                                                                                                                                              <block type="expression" id="{?IEN?J6XfCF_^+^yTBZ">
                                                                                                                                                                <field name="value">k[i] == x1</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <statement name="then">
                                                                                                                                                              <block type="variable" id="CbWt6{X`^JA?Z(#WKdgE">
                                                                                                                                                                <field name="name">da[i]</field>
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="expression" id="YFj+K(]4Jy+_Qe+ub3k[">
                                                                                                                                                                    <field name="value">'border: 5px solid green;'</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="variable" id="CJU?}VO^M=5t1JU`-7Y@1">
                                                                                                                                                                    <field name="name">dem[j]</field>
                                                                                                                                                                    <value name="value">
                                                                                                                                                                      <block type="expression" id="KP4)t}$3/E@2pO]w!,cZ9">
                                                                                                                                                                        <field name="value">i</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="variable" id="c#EZ1)BbAz2^d@2`eYg]]">
                                                                                                                                                                        <field name="name">j</field>
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="expression" id="l}6tS^8cK(a{w`fqL~M#">
                                                                                                                                                                            <field name="value">j+1</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="variable" id=",+2$1qUH1g1QC,=+e}bv">
                                                                                                                                                                <field name="name">i</field>
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="expression" id="OUzdP5G6U#Jly}JDG@1wb">
                                                                                                                                                                    <field name="value">i+1</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="variable" id="mW(kfH1:@1Sb/yy5gFqy^">
                                                                                                                                                        <field name="name">link</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="Vs|N4P1BktJYGV./+xx~">
                                                                                                                                                            <field name="value">'http://starmathsonline.s3.amazonaws.com/' + image_path</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="partial_explanation" id="BCNBDmbt~Yn,u(4Q!Fy5" inline="true">
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="string_value" id="z;tZkKpM{=jPm~E2xa+,">
                                                                                                                                                                <field name="value">&lt;u&gt;Step 1: Get to know the different objects that are 1 kilogram in mass.&lt;/u&gt;

&lt;style&gt;
table{width: 900px}
td{border: 1px solid black; text-align: center; padding: 10px}
&lt;/style&gt;
&lt;center&gt;
&lt;table&gt;
&lt;tr&gt;
&lt;td&gt;Mass&lt;/td&gt;&lt;td&gt;Object 1&lt;/td&gt;&lt;td&gt;Object 2&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;${x1} kg&lt;/td&gt;
&lt;td&gt;&lt;img src='${link}${item[dem[0]]}.png'&gt;&lt;/td&gt;
&lt;td&gt;&lt;img src='${link}${item[dem[1]]}.png'&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;/table&gt;
&lt;/center&gt;

&lt;/br&gt;
&lt;p style='background-color: rgba(206, 241, 241, 0.6); display: inline;'&gt;@2The shape and size of the object does not matter if the mass is the same.@2&lt;/p&gt;
                                                                                                                                                                </field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="end_partial_explanation" id=";4x$pnzQVzJ-K#x?~loE">
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="if_then_else_block" id="rk@27E`C$hx[^m,rFEijg">
                                                                                                                                                                    <value name="if">
                                                                                                                                                                      <block type="expression" id="N^#R@2r?X;SG/M[4:_Lox">
                                                                                                                                                                        <field name="value">type == 1</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <statement name="then">
                                                                                                                                                                      <block type="partial_explanation" id="6GPZPv+]CQW4W@2vD4|#T" inline="true">
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="string_value" id="~NQp+^Y{Xmag)Yd5/UT8">
                                                                                                                                                                            <field name="value">&lt;u&gt;Step 2: Choose the objects that have the same mass.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
table{width: 900px}
td{border: 1px solid black; text-align: center; padding: 10px}
&lt;/style&gt;
&lt;center&gt;
&lt;table&gt;

&lt;tr&gt;
&lt;td style='${da[0]};'&gt;&lt;img src='${link}${item[0]}.png'&gt;&lt;/br&gt; ${k[0]} kg&lt;/td&gt;
&lt;td style='${da[1]};'&gt;&lt;img src='${link}${item[1]}.png'&gt;&lt;/br&gt; ${k[1]} kg&lt;/td&gt;
&lt;td style='${da[2]};'&gt;&lt;img src='${link}${item[2]}.png'&gt;&lt;/br&gt; ${k[2]} kg&lt;/td&gt;
&lt;/tr&gt;

&lt;/table&gt;
&lt;/center&gt;
                                                                                                                                                                            </field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="end_partial_explanation" id="tm_SHWhYN7;9j#@2@1NX[K"></block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                    <statement name="else">
                                                                                                                                                                      <block type="variable" id="tFRcl/Su=_uUxxJ@1B]Oj">
                                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="expression" id="a,h{!6VSeYeIao}h[Bg2">
                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="while_do_block" id="6ll!H!~hVf@10(hJMhcVk">
                                                                                                                                                                            <value name="while">
                                                                                                                                                                              <block type="expression" id="H^v5A@2ojo^u@2-SzWIt1?">
                                                                                                                                                                                <field name="value">i&lt; 3</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <statement name="do">
                                                                                                                                                                              <block type="variable" id="H:z[PkS/.g1]Q8QNB6vO">
                                                                                                                                                                                <field name="name">da[i]</field>
                                                                                                                                                                                <value name="value">
                                                                                                                                                                                  <block type="expression" id="U14-j[8@1Z/(52K@1GdQI;">
                                                                                                                                                                                    <field name="value">''</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="if_then_block" id="K]QyaSSKNo[EWxQg2z9D">
                                                                                                                                                                                    <value name="if">
                                                                                                                                                                                      <block type="expression" id="z?CGe}JG8H;o)D/Q7T59">
                                                                                                                                                                                        <field name="value">k[i] != x1</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <statement name="then">
                                                                                                                                                                                      <block type="variable" id="cwclwXjj;llp5o][I$,%">
                                                                                                                                                                                        <field name="name">da[i]</field>
                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                          <block type="expression" id="lm5bxVzp%b@2g#I)~X,yE">
                                                                                                                                                                                            <field name="value">'border: 5px solid green;'</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </statement>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="variable" id="%8xVQ3E1nnA/WKI#_8,L">
                                                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                          <block type="expression" id=")|Kf34KGf_=i}i:CbhKB">
                                                                                                                                                                                            <field name="value">i+1</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </statement>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="partial_explanation" id="+bW`3tPx`c/.YP.$`n7z" inline="true">
                                                                                                                                                                                <value name="value">
                                                                                                                                                                                  <block type="string_value" id="[p)N9I#lv`ML@1+9th5?{">
                                                                                                                                                                                    <field name="value">&lt;u&gt;Step 2: Choose the object that is the odd one out.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
table{width: 900px}
td{border: 1px solid black; text-align: center; padding: 10px}
&lt;/style&gt;
&lt;center&gt;
&lt;table&gt;

&lt;tr&gt;
&lt;td style='${da[0]};'&gt;&lt;img src='${link}${item[0]}.png'&gt;&lt;/br&gt; ${k[0]} kg&lt;/td&gt;
&lt;td style='${da[1]};'&gt;&lt;img src='${link}${item[1]}.png'&gt;&lt;/br&gt; ${k[1]} kg&lt;/td&gt;
&lt;td style='${da[2]};'&gt;&lt;img src='${link}${item[2]}.png'&gt;&lt;/br&gt; ${k[2]} kg&lt;/td&gt;
&lt;/tr&gt;

&lt;/table&gt;
&lt;/center&gt;
                                                                                                                                                                                    </field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
</xml>
END_XML]] */