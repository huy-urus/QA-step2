
module.exports = [
  {
    "#type": "question",
    "name": "Y3.MG.MA.REFU.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y3.MG.MA.REFU.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "variable",
        "name": "object",
        "value": {
          "#type": "string_array",
          "items": "school bag|hammer|chair|radio|laptop|guitar|shovel|electronic keyboard|television|fish tank|watermelon"
        }
      },
      {
        "#type": "variable",
        "name": "informal",
        "value": {
          "#type": "string_array",
          "items": "emtri|a screw|a ball|a bowl of soup|a cup|a coin|a feather"
        }
      },
      {
        "#type": "variable",
        "name": "formal",
        "value": {
          "#type": "string_array",
          "items": "emtry|kg weight|kg of rice"
        }
      },
      {
        "#type": "variable",
        "name": "condition",
        "value": {
          "#type": "expression",
          "value": "['less', 'more']"
        }
      },
      {
        "#type": "variable",
        "name": "c",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "0"
          },
          "max": {
            "#type": "expression",
            "value": "1"
          }
        }
      },
      {
        "#type": "variable",
        "name": "o",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "0"
          },
          "max": {
            "#type": "expression",
            "value": "10"
          }
        }
      },
      {
        "#type": "variable",
        "name": "x",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "5"
          }
        }
      },
      {
        "#type": "variable",
        "name": "type",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "type[0]",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "6"
          }
        }
      },
      {
        "#type": "variable",
        "name": "type[1]",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "2"
          }
        }
      },
      {
        "#type": "variable",
        "name": "arr",
        "value": {
          "#type": "func",
          "name": "shuffle",
          "args": [
            {
              "#type": "expression",
              "value": "[0, 1]"
            }
          ]
        }
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "20"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "Click the example that shows a ${condition[c]} accurate unit to measure the mass of the ${object[o]}."
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "36"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "1"
            },
            "cols": {
              "#type": "expression",
              "value": "2"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "280"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "750"
            },
            "height": {
              "#type": "expression",
              "value": "80"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "expression",
              "value": "arr"
            }
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_cell_template",
            "variable": "$cell",
            "#prop": "cell.template",
            "#callback": "$cell",
            "body": [
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX - 110"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY - $cell.height/2 + 10"
                    }
                  },
                  {
                    "#type": "prop_anchor",
                    "#prop": "anchor",
                    "x": {
                      "#type": "expression",
                      "value": "0.5"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "1"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "${o}.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}${o}.png"
                  }
                ]
              },
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX + 110"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY - $cell.height/2 + 10"
                    }
                  },
                  {
                    "#type": "prop_anchor",
                    "#prop": "anchor",
                    "x": {
                      "#type": "expression",
                      "value": "0.5"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "1"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "${$cell.data}_${type[$cell.data]}.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}${$cell.data}_${type[$cell.data]}.png"
                  }
                ]
              },
              {
                "#type": "if_then_block",
                "if": {
                  "#type": "expression",
                  "value": "$cell.data ==  1"
                },
                "then": [
                  {
                    "#type": "text_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX + 110"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY - $cell.height/2 + 5"
                        }
                      },
                      {
                        "#type": "prop_anchor",
                        "#prop": "anchor",
                        "x": {
                          "#type": "expression",
                          "value": "0.5"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "1"
                        }
                      },
                      {
                        "#type": "prop_text_contents",
                        "#prop": "",
                        "contents": "${x}kg"
                      },
                      {
                        "#prop": "",
                        "style": {
                          "#type": "json",
                          "base": "text",
                          "#props": [
                            {
                              "#type": "prop_text_style_font_size",
                              "#prop": "",
                              "fontSize": {
                                "#type": "expression",
                                "value": "26"
                              }
                            },
                            {
                              "#type": "prop_text_style_fill",
                              "#prop": "",
                              "fill": "black"
                            },
                            {
                              "#type": "prop_text_style_stroke",
                              "#prop": "",
                              "stroke": "white"
                            },
                            {
                              "#type": "prop_text_style_stroke_thickness",
                              "#prop": "",
                              "strokeThickness": {
                                "#type": "expression",
                                "value": "2"
                              }
                            }
                          ]
                        }
                      }
                    ]
                  }
                ]
              },
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "scale.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}scale.png"
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "1"
            },
            "cols": {
              "#type": "expression",
              "value": "2"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "390"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "750"
            },
            "height": {
              "#type": "expression",
              "value": "100"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "expression",
              "value": "arr"
            }
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_cell_template_for",
            "variable": "$cell",
            "condition": "$cell.col == 0",
            "#prop": "cell.templates[]",
            "#callback": "$cell",
            "body": [
              {
                "#type": "choice_custom_shape",
                "action": "click-one",
                "value": {
                  "#type": "expression",
                  "value": "1 + $cell.data"
                },
                "template": {
                  "#callback": "$choice",
                  "variable": "$choice",
                  "body": [
                    {
                      "#type": "image_shape",
                      "#props": [
                        {
                          "#type": "prop_position",
                          "#prop": "",
                          "x": {
                            "#type": "expression",
                            "value": "$cell.centerX"
                          },
                          "y": {
                            "#type": "expression",
                            "value": "$cell.centerY"
                          }
                        },
                        {
                          "#type": "prop_image_key",
                          "#prop": "",
                          "key": "a.png"
                        },
                        {
                          "#type": "prop_image_src",
                          "#prop": "",
                          "src": "${image_path}a.png"
                        }
                      ]
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "prop_grid_cell_template_for",
            "variable": "$cell",
            "condition": "$cell.col == 1",
            "#prop": "cell.templates[]",
            "#callback": "$cell",
            "body": [
              {
                "#type": "choice_custom_shape",
                "action": "click-one",
                "value": {
                  "#type": "expression",
                  "value": "1 + $cell.data"
                },
                "template": {
                  "#callback": "$choice",
                  "variable": "$choice",
                  "body": [
                    {
                      "#type": "image_shape",
                      "#props": [
                        {
                          "#type": "prop_position",
                          "#prop": "",
                          "x": {
                            "#type": "expression",
                            "value": "$cell.centerX"
                          },
                          "y": {
                            "#type": "expression",
                            "value": "$cell.centerY"
                          }
                        },
                        {
                          "#type": "prop_image_key",
                          "#prop": "",
                          "key": "b.png"
                        },
                        {
                          "#type": "prop_image_src",
                          "#prop": "",
                          "src": "${image_path}b.png"
                        }
                      ]
                    }
                  ]
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "c == 0"
        },
        "then": [
          {
            "#type": "func",
            "name": "addAnswers",
            "args": [
              {
                "#type": "expression",
                "value": "1"
              }
            ]
          }
        ],
        "else": [
          {
            "#type": "func",
            "name": "addAnswers",
            "args": [
              {
                "#type": "expression",
                "value": "2"
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "loadAssets",
        "value": {
          "#type": "custom_image_list",
          "link": "${image_path}",
          "images": "ex|scale|1_${type[1]}|0_${type[0]}|${o}"
        }
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 1: Get to know the difference between formal and informal units used to measure mass.</u></br></br>\n\n<center>\n  <img src='@sprite.src(ex)'/>\n    </center>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 2: Now let's choose the ${condition[c]} accurate unit to measure the mass of the ${object[o]}.</u></br></br>\n\n<style>\n  table {width: 750px;}\ntd{text-align: center;}\n#img{width: 145px; vertical-align: bottom; height: 125px}\nimg{}\n#stroke {\nfont-size: 24px; color: black;\n}\n#circle {width: 125px; height: 125px; border: 3px solid green; margin: auto; border-radius: 62px}\n  </style>\n<center>\n<div style='position: relative; width: 750px; height: 270px'>\n  <table style='position: absolute; top: 15px'>\n  <tr>\n    <td id='img'><img src='@sprite.src(${o})'/></td>\n    <td></td>\n    <td id='img' style='background-image: url(\"@sprite.src(${arr[0]}_${type[arr[0]]})\"); background-repeat: no-repeat; background-position: center bottom;'><span id='stroke'>${arr[0] == 1? x+'kg':''}</span></td>\n    <td id='img'><img src='@sprite.src(${o})'/></td>\n    <td></td>\n    <td id='img' style='background-image: url(\"@sprite.src(${arr[1]}_${type[arr[1]]})\"); background-repeat: no-repeat; background-position: center bottom;'><span id='stroke'>${arr[1] == 1? x+'kg':''}</span></td>\n  </tr>\n  <tr>\n    <td colspan='3'></td>\n    <td colspan='3'></td>\n  </tr>\n</table>\n\n  <table style='position: absolute;'>\n  <tr>\n    <td id='img'></td>\n    <td></td>\n    <td id='img' style='background-image:; background-repeat: no-repeat; background-position: center bottom;'><span id='stroke'></span></td>\n    <td id='img'></td>\n    <td></td>\n    <td id='img' style='background-image:; background-repeat: no-repeat; background-position: center bottom;'><span id='stroke'></span></td>\n  </tr>\n  <tr>\n    <td colspan='3'><img src='@sprite.src(scale)'/></td>\n    <td colspan='3'><img src='@sprite.src(scale)'/></td>\n  </tr>\n</table>\n\n<table style='position: absolute; top: 40px'>\n  <tr>\n    <td id='img'></td>\n    <td></td>\n    <td id='img'>${(c == 0 &&  arr[0] == 0) || (c == 1 && arr[0] == 1) ? \"<div id='circle'></div>\": \"\"}</td>\n    <td id='img'></td>\n    <td></td>\n    <td id='img'>${(c == 0 &&  arr[1] == 0) || (c == 1 && arr[1] == 1) ? \"<div id='circle'></div>\": \"\"}</td>\n  </tr>\n</table>\n</div>\n\n<span style='background: rgb(250, 250, 250, 0.4)'>\n${c == 0 ? \"*Hint: informal units have no exact weight.*\":\"*Hint: formal units have an exact weight.*\"}</br></center>\n  <span style='background: rgb(250, 250, 250, 0.4)'>\nUsing the <b>${c == 0 ? informal[type[0]]:x + ' ' + formal[type[1]]}</b> is <b>${c == 0 ? 'less':'more'}</b> accurate than using the <b>${c == 0 ? x + ' ' + formal[type[1]]:informal[type[0]]}</b> to measure the mass of the ${object[o]}.\n</span>"
        ]
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="0" y="0">
    <field name="name">Y3.MG.MA.REFU.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y3.MG.MA.REFU.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="YepMQT@2uwp2rRPvmnRg1">
                                    <field name="name">range</field>
                                    <value name="value">
                                      <block type="expression" id="UxgQ^Y5]rb}vpw[D=l-!">
                                        <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="variable" id="N9%z/Q@1)[BULHiRu@2cgE">
                                        <field name="name">object</field>
                                        <value name="value">
                                          <block type="string_array" id="0ij[a_/lt~bN)m6_(fzJ">
                                            <field name="items">school bag|hammer|chair|radio|laptop|guitar|shovel|electronic keyboard|television|fish tank|watermelon</field>
                                          </block>
                                        </value>
                                        <next>
                                          <block type="variable" id="v;.d(pOcatS5y?x.5Dd$">
                                            <field name="name">informal</field>
                                            <value name="value">
                                              <block type="string_array" id="y64_4wSPWO;}M0eO+e:R">
                                                <field name="items">emtri|a screw|a ball|a bowl of soup|a cup|a coin|a feather</field>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id="?`fA(,jNU$Nvr?6K0ulG">
                                                <field name="name">formal</field>
                                                <value name="value">
                                                  <block type="string_array" id="5FRY$vGcZ_k:b|^Z:?0V">
                                                    <field name="items">emtry|kg weight|kg of rice</field>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="g;70}+M^gxr5~K#OjIDx">
                                                    <field name="name">condition</field>
                                                    <value name="value">
                                                      <block type="expression" id=";jadKCDWu.?VHbD.5Q7D">
                                                        <field name="value">['less', 'more']</field>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id="ZU^k7zr]TiA`X.[l,fV:">
                                                        <field name="name">c</field>
                                                        <value name="value">
                                                          <block type="random_number" id="3van@2SV.nNTAmeXtTCAB">
                                                            <value name="min">
                                                              <block type="expression" id="+zgz=B,@1wRdc2;.yxIL`">
                                                                <field name="value">0</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="H8wHbA=Et[D9)iSXL8J$">
                                                                <field name="value">1</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="-6_O@2i2Q;9Az=uIhp)Lu">
                                                            <field name="name">o</field>
                                                            <value name="value">
                                                              <block type="random_number" id="H{j^.NZ|q!dK/B%JoDv.">
                                                                <value name="min">
                                                                  <block type="expression" id=":-i01J_QR6=6}~duWMVD">
                                                                    <field name="value">0</field>
                                                                  </block>
                                                                </value>
                                                                <value name="max">
                                                                  <block type="expression" id="2%yT9m`tV3H5Pn~`]qAr">
                                                                    <field name="value">10</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="variable" id="MwYNf`F=sv5g#8}o!K4:">
                                                                <field name="name">x</field>
                                                                <value name="value">
                                                                  <block type="random_number" id="2)y}V7M^Kmp]sevgXM!B">
                                                                    <value name="min">
                                                                      <block type="expression" id=")d5xd+:$u06z6/Y91miG">
                                                                        <field name="value">1</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="max">
                                                                      <block type="expression" id="?u79I6mF?T~019rLz_Xl">
                                                                        <field name="value">5</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="variable" id="kHp=YSE}=2LNe61OYJ9!">
                                                                    <field name="name">type</field>
                                                                    <value name="value">
                                                                      <block type="expression" id="bMiO-xahXp:FiCOr2Z%{">
                                                                        <field name="value">[]</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="variable" id="_+Qj8Ml:V#`2EDNY}$[.">
                                                                        <field name="name">type[0]</field>
                                                                        <value name="value">
                                                                          <block type="random_number" id="n=%T6w8$K#baneqa@1ktx">
                                                                            <value name="min">
                                                                              <block type="expression" id="Mh|#)DS#WfaTl(9EZfAY">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="max">
                                                                              <block type="expression" id="ld^,(+pRbY0,(erd/AO)">
                                                                                <field name="value">6</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="wS1P({T-QU~DHpHNxJm0">
                                                                            <field name="name">type[1]</field>
                                                                            <value name="value">
                                                                              <block type="random_number" id="p!-wOa7/(|Alx4ubTsLr">
                                                                                <value name="min">
                                                                                  <block type="expression" id="55HeI+:}=D?6@2(JhHCdm">
                                                                                    <field name="value">1</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="max">
                                                                                  <block type="expression" id="{3Zm+Q|+)@1lH|hg%u?1!">
                                                                                    <field name="value">2</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="variable" id="Ydj@2h;cMY]fcg`8z8Grt">
                                                                                <field name="name">arr</field>
                                                                                <value name="value">
                                                                                  <block type="func_shuffle" id="s((^l8F5F[JZ5M3M@2R7I" inline="true">
                                                                                    <value name="value">
                                                                                      <block type="expression" id="b]pO=Iy`e|)amoOHN0Z@1">
                                                                                        <field name="value">[0, 1]</field>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                                                    <statement name="#props">
                                                                                      <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                                                            <field name="value">400</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                                                            <field name="value">20</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_anchor" id="O.v@2mKn@1K#+tF;|}C}xI">
                                                                                            <field name="#prop">anchor</field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="+3%GU[X3l%%X`Jv1QL7@2">
                                                                                                <field name="value">0.5</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="Kzp23HjHF7k]A-y1BJPm">
                                                                                                <field name="value">0</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="contents">
                                                                                                  <block type="string_value" id="#6btV9p,PGV}wMx.U�">
                                                                                                    <field name="value">Click the example that shows a ${condition[c]} accurate unit to measure the mass of the ${object[o]}.</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                                                    <field name="base">text</field>
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="fontSize">
                                                                                                          <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                                                            <field name="value">36</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="fill">
                                                                                                              <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                                                                <field name="value">black</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="stroke">
                                                                                                                  <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                                                    <field name="value">white</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="strokeThickness">
                                                                                                                      <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                                                        <field name="value">2</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <next>
                                                                                      <block type="grid_shape" id="6tU?K^N8ec~A@1@1NwQ72N">
                                                                                        <statement name="#props">
                                                                                          <block type="prop_grid_dimension" id="X4K:|9dKK%s9l4n5v@2lG">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="rows">
                                                                                              <block type="expression" id="[Ev!.bxnzjc|-33v.%7=">
                                                                                                <field name="value">1</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="cols">
                                                                                              <block type="expression" id="u$7]/V/!ZWM4(Va??@2({">
                                                                                                <field name="value">2</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_position" id="zLld84TfBg#!88]1,JD-">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="Cd6g{Y_P?:GyVyvQi1wJ">
                                                                                                    <field name="value">400</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id=".F}E71+@1i)77!yQ`hmfu">
                                                                                                    <field name="value">280</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_size" id="+o2CWr^.ohx)_bcIG53]">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="width">
                                                                                                      <block type="expression" id="Z[x5iHG%KWC%KWQb}c-G">
                                                                                                        <field name="value">750</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="height">
                                                                                                      <block type="expression" id="@2E|Ob@1jF#gYtX8-%Iui@1">
                                                                                                        <field name="value">80</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_anchor" id="1WJjXY^.h,t]eNgxaJ}q">
                                                                                                        <field name="#prop">anchor</field>
                                                                                                        <value name="x">
                                                                                                          <block type="expression" id="DHx[vqs?.?nUD:AYPJWd">
                                                                                                            <field name="value">0.5</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id="e3D|RR7w!=Sufu):zV`c">
                                                                                                            <field name="value">0.5</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_grid_cell_source" id="v8+Yu211Qg8fX9Mu4th`">
                                                                                                            <field name="#prop">cell.source</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="~j@1,^liddf}oDg}E91^i">
                                                                                                                <field name="value">arr</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_grid_random" id="}?kS#x[b)9OGb@1`UdJGV">
                                                                                                                <field name="#prop"></field>
                                                                                                                <field name="random">FALSE</field>
                                                                                                                <next>
                                                                                                                  <block type="prop_grid_show_borders" id=";y^wk4y%[VWjXOj~vrQZ">
                                                                                                                    <field name="#prop">#showBorders</field>
                                                                                                                    <field name="value">FALSE</field>
                                                                                                                    <next>
                                                                                                                      <block type="prop_grid_cell_template" id="7^6]Kv:2yrZi`cooAW2@2">
                                                                                                                        <field name="variable">$cell</field>
                                                                                                                        <field name="#prop">cell.template</field>
                                                                                                                        <field name="#callback">$cell</field>
                                                                                                                        <statement name="body">
                                                                                                                          <block type="image_shape" id="!=6OM:Q[~FbBAH@2ez:;[">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="jj%Y(GdX{IhW3|6/@2AEh">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="~34ic=Nl1Ku.FM3jBTZe">
                                                                                                                                    <field name="value">$cell.centerX - 110</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="iIPC}l[QDkk$I6RyS;Lt">
                                                                                                                                    <field name="value">$cell.centerY - $cell.height/2 + 10</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_anchor" id="oimw%(mx|@2|JHhepa!H_">
                                                                                                                                    <field name="#prop">anchor</field>
                                                                                                                                    <value name="x">
                                                                                                                                      <block type="expression" id="[;bf(i5@1UZLT5`g?f)M1">
                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="y">
                                                                                                                                      <block type="expression" id=",0l|~yVvA9{$iop-vumb">
                                                                                                                                        <field name="value">1</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_key" id="KBaySf/;%0U|#xdko,#S">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="key">
                                                                                                                                          <block type="string_value" id="@2]?o8#[fN,{@2p;_)v8tG">
                                                                                                                                            <field name="value">${o}.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_src" id="fybJ?kWyUQv{Fp)4jk+#">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="src">
                                                                                                                                              <block type="string_value" id="[uL=,do63Eskkuvc96i`">
                                                                                                                                                <field name="value">${image_path}${o}.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="image_shape" id="`x2c[)Vw9_cNZOwy:#Qo">
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_position" id="@1jek!x@13lE{Y^.CEu`[U">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="x">
                                                                                                                                      <block type="expression" id="m@1dDDNydm:7ff=[z|NBr">
                                                                                                                                        <field name="value">$cell.centerX + 110</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="y">
                                                                                                                                      <block type="expression" id="n:hgrzga]M_)b#gD$mxp">
                                                                                                                                        <field name="value">$cell.centerY - $cell.height/2 + 10</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_anchor" id="R?kI2OpgRt@1NhMd/~%.L">
                                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="expression" id="h_=A!jKQP1xcQcdx9n~f">
                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id=".VrUDR+h)tr;`vf#,">
                                                                                                                                            <field name="value">1</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_key" id="WYikeWA,F#?W45swc$,%">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="key">
                                                                                                                                              <block type="string_value" id="?(R)Nd:i`_/@2A_N=^4Tz">
                                                                                                                                                <field name="value">${$cell.data}_${type[$cell.data]}.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_src" id="vC7aWeVr)$zr1iw^x+cO">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="src">
                                                                                                                                                  <block type="string_value" id="CY:%]vQ1m^A/SDz@15O^4">
                                                                                                                                                    <field name="value">${image_path}${$cell.data}_${type[$cell.data]}.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <next>
                                                                                                                                  <block type="if_then_block" id="/sp7W_oGDa()RZ79ObuL">
                                                                                                                                    <value name="if">
                                                                                                                                      <block type="expression" id="CGgkf;vEN$FU%T(5O~9B">
                                                                                                                                        <field name="value">$cell.data ==  1</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <statement name="then">
                                                                                                                                      <block type="text_shape" id="qRQ1[_HRmT!?-xv7RgNt">
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_position" id="YyOLp3Y+u?/Pqi~6]sJu">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id=".[RWS1qMpa.X~WH8oQoY">
                                                                                                                                                <field name="value">$cell.centerX + 110</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="$1~wbe-J7ZcCM|XG$Xdo">
                                                                                                                                                <field name="value">$cell.centerY - $cell.height/2 + 5</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_anchor" id=";qiy.6pWqt3Vt$$l3;KO">
                                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                                <value name="x">
                                                                                                                                                  <block type="expression" id="JvXB|VNWbhYm#K3KEwQ,">
                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <value name="y">
                                                                                                                                                  <block type="expression" id="ytaHbFb`0An5z:~@1/mWD">
                                                                                                                                                    <field name="value">1</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_text_contents" id="G($)nNUD((i%U)W%0J,T">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="contents">
                                                                                                                                                      <block type="string_value" id="Uybpz325UL3;@1@1JRk3RI">
                                                                                                                                                        <field name="value">${x}kg</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_text_style" id=":L|p/mJ@1)vef`{kX~GP}">
                                                                                                                                                        <field name="base">text</field>
                                                                                                                                                        <statement name="#props">
                                                                                                                                                          <block type="prop_text_style_font_size" id="A7zox%0(V9D`[9K_~FbV">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="fontSize">
                                                                                                                                                              <block type="expression" id="yZJA;b+]E^Y@1PO2|2BL)">
                                                                                                                                                                <field name="value">26</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_text_style_fill" id="6zhjF9tQb7k4OO+ws5P5">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="fill">
                                                                                                                                                                  <block type="string_value" id="koge1|HP@1PSF~L!~-`Av">
                                                                                                                                                                    <field name="value">black</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_text_style_stroke" id="cmPJ(|z5zk)9x_DN!8wf">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="stroke">
                                                                                                                                                                      <block type="string_value" id="y{Qn`L@2iu+Fkhq/C9@1Fe">
                                                                                                                                                                        <field name="value">white</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="B,CiUY7TihjPj@12?k)RD">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="strokeThickness">
                                                                                                                                                                          <block type="expression" id="FDKIT[~$n$;kbR$@2`-~/">
                                                                                                                                                                            <field name="value">2</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="image_shape" id="|GuwUZYRe{K-mNkW%qea">
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_position" id="Tc:pdP(!2xSX5FazcxjA">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id="A`r_G|v2_jHP0:Qt:C`x">
                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="=lOpBR#mokawBg+?83pN">
                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_key" id="V|;:%}n|X5bY@2w^!Ev1c">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="key">
                                                                                                                                                  <block type="string_value" id="i[,z$G##!AkSFp.Y7#@2`">
                                                                                                                                                    <field name="value">scale.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_image_src" id="#DwT4IBy`VH@1/|_JT.4u">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="src">
                                                                                                                                                      <block type="string_value" id="%bKue`L,5+!6[$S7$`r2">
                                                                                                                                                        <field name="value">${image_path}scale.png</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="grid_shape" id="2=2P$m9P/,5!)ma=LfsI">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_grid_dimension" id="q!EG-5U[eA`3W,ETyi?b">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="rows">
                                                                                                  <block type="expression" id="@2DgT(+#o/uCeR$1+h}NV">
                                                                                                    <field name="value">1</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="cols">
                                                                                                  <block type="expression" id="o+E(XO)=7!lXgNW{(q7$">
                                                                                                    <field name="value">2</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_position" id="bT`dJi##aK@1H8E5f}O[(">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="4+MFn_zeEBMAs#U6gSP7">
                                                                                                        <field name="value">400</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="|PF$J@2eCwCF8Yz}}=q]d">
                                                                                                        <field name="value">390</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_size" id="[@2MN#j:Xx=W_K2aLs,L!">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="width">
                                                                                                          <block type="expression" id="f3@1${09p,7N3d]h;CA[J">
                                                                                                            <field name="value">750</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="height">
                                                                                                          <block type="expression" id="aVB{b)DH%3(@1lJdbO-w8">
                                                                                                            <field name="value">100</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_anchor" id="@2,1iaUcf[RCVT~Qsl!:]">
                                                                                                            <field name="#prop">anchor</field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="0q,0^A49$WT+)[8S@2_)%">
                                                                                                                <field name="value">0.5</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="[I#Uv=AoRx`Rz1lzgToK">
                                                                                                                <field name="value">0.5</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_grid_cell_source" id="3y!o)F|U^5mpb_k,W1a`">
                                                                                                                <field name="#prop">cell.source</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="kMQ4y0Y778o+o0v@2%9:l">
                                                                                                                    <field name="value">arr</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_grid_random" id="as%c~4(0mwm+D;7|aFH4">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <field name="random">FALSE</field>
                                                                                                                    <next>
                                                                                                                      <block type="prop_grid_show_borders" id="}rIdf@2vVwvSwK?7tdl!4">
                                                                                                                        <field name="#prop">#showBorders</field>
                                                                                                                        <field name="value">FALSE</field>
                                                                                                                        <next>
                                                                                                                          <block type="prop_grid_cell_template_for" id="Jf;=uw$~u[K/w0Hdle/#">
                                                                                                                            <field name="variable">$cell</field>
                                                                                                                            <field name="condition">$cell.col == 0</field>
                                                                                                                            <field name="#prop">cell.templates[]</field>
                                                                                                                            <field name="#callback">$cell</field>
                                                                                                                            <statement name="body">
                                                                                                                              <block type="choice_custom_shape" id="mA1-_S}YxtMD]9^b`gKm">
                                                                                                                                <field name="action">click-one</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="uPl_XcZ#hO3Rb[jDAn7t">
                                                                                                                                    <field name="value">1 + $cell.data</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <statement name="template">
                                                                                                                                  <block type="image_shape" id="3l[]]v}W80Fys^/c5,/B">
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_position" id="F,WO(#MGOAkU+.~#KLoy">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="expression" id="n]Ois-^Z_$jIM1Ld%?Zc">
                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id="T,m_Ycj/K|PuS=G:sS+^">
                                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_key" id="+bBdU)Z9n?3tmg=!+kK{">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="key">
                                                                                                                                              <block type="string_value" id="?VQJ$8dwo#v(!xqM$mo.">
                                                                                                                                                <field name="value">a.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_src" id="n`v}K;l`L0txYiKpLnCN">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="src">
                                                                                                                                                  <block type="string_value" id="Fl~YEj#fz?C@2X{Ji}X?A">
                                                                                                                                                    <field name="value">${image_path}a.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="prop_grid_cell_template_for" id="T~1-ag_UT:0s@1df]@1A{s">
                                                                                                                                <field name="variable">$cell</field>
                                                                                                                                <field name="condition">$cell.col == 1</field>
                                                                                                                                <field name="#prop">cell.templates[]</field>
                                                                                                                                <field name="#callback">$cell</field>
                                                                                                                                <statement name="body">
                                                                                                                                  <block type="choice_custom_shape" id=":B_h%)TSOuWxNjI2k#i8">
                                                                                                                                    <field name="action">click-one</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="Q?lS0qhL/_^twQ(2o._U">
                                                                                                                                        <field name="value">1 + $cell.data</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <statement name="template">
                                                                                                                                      <block type="image_shape" id="?Qsk!#MxpkdZSF/hV0eo">
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_position" id="1!$Sd#Zi|s,(}@1!:tgOR">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id="t!@2?[+z;Mp@1{NH2)fX=o">
                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="-(eaJmr%M4WpUr,KO$zv">
                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_key" id="nrNVQo}](y^qi7lP]$lu">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="key">
                                                                                                                                                  <block type="string_value" id="zL=,r@2EpaQdT+F$;Df!$">
                                                                                                                                                    <field name="value">b.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_image_src" id="0RKecX7CleA@1Rgu9A[Zq">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="src">
                                                                                                                                                      <block type="string_value" id="BT`Htz33/ybxtr!z(By,">
                                                                                                                                                        <field name="value">${image_path}b.png</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="if_then_else_block" id="@1#(Z^_uT)W2?nR!DorGA">
                                                                                                <value name="if">
                                                                                                  <block type="expression" id="1.z-GOIL|(l=i!i7110%">
                                                                                                    <field name="value">c == 0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <statement name="then">
                                                                                                  <block type="func_add_answer" id="a@1Vb+alddtdeG=,$!Nu+" inline="true">
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="u6CGnSN/mDL.VBjGW:GH">
                                                                                                        <field name="value">1</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <statement name="else">
                                                                                                  <block type="func_add_answer" id="4T=]B$!OWEeFqwc~?4mo" inline="true">
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="F4fROR`fKA(bN]f~Z_SE">
                                                                                                        <field name="value">2</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="variable" id="weElLx3.8SB.)STr377[">
                                                                                                    <field name="name">loadAssets</field>
                                                                                                    <value name="value">
                                                                                                      <block type="custom_image_list" id="[iQG~r:b@1r?SU7Kr3cr@2">
                                                                                                        <field name="link">${image_path}</field>
                                                                                                        <field name="images">ex|scale|1_${type[1]}|0_${type[0]}|${o}</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="partial_explanation" id="DiX4ap4$C[^+(^Vjq6`0">
                                                                                                        <value name="value">
                                                                                                          <block type="string_value" id="iFO#,H#FCEj,L+gj$}t@2">
                                                                                                            <field name="value">&lt;u&gt;Step 1: Get to know the difference between formal and informal units used to measure mass.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;center&gt;
  &lt;img src='@1sprite.src(ex)'/&gt;
    &lt;/center&gt;
                                                                                                            </field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="end_partial_explanation" id="VIJSRK=ps-Jz)eP:O$D^">
                                                                                                            <next>
                                                                                                              <block type="partial_explanation" id="s6?y-8.]{`DckDQN`SNI">
                                                                                                                <value name="value">
                                                                                                                  <block type="string_value" id="rILc1O/{z#t7ON_[LxyI">
                                                                                                                    <field name="value">&lt;u&gt;Step 2: Now let's choose the ${condition[c]} accurate unit to measure the mass of the ${object[o]}.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
  table {width: 750px;}
td{text-align: center;}
#img{width: 145px; vertical-align: bottom; height: 125px}
img{}
#stroke {
font-size: 24px; color: black;
}
#circle {width: 125px; height: 125px; border: 3px solid green; margin: auto; border-radius: 62px}
  &lt;/style&gt;
&lt;center&gt;
&lt;div style='position: relative; width: 750px; height: 270px'&gt;
  &lt;table style='position: absolute; top: 15px'&gt;
  &lt;tr&gt;
    &lt;td id='img'&gt;&lt;img src='@1sprite.src(${o})'/&gt;&lt;/td&gt;
    &lt;td&gt;&lt;/td&gt;
    &lt;td id='img' style='background-image: url("@1sprite.src(${arr[0]}_${type[arr[0]]})"); background-repeat: no-repeat; background-position: center bottom;'&gt;&lt;span id='stroke'&gt;${arr[0] == 1? x+'kg':''}&lt;/span&gt;&lt;/td&gt;
    &lt;td id='img'&gt;&lt;img src='@1sprite.src(${o})'/&gt;&lt;/td&gt;
    &lt;td&gt;&lt;/td&gt;
    &lt;td id='img' style='background-image: url("@1sprite.src(${arr[1]}_${type[arr[1]]})"); background-repeat: no-repeat; background-position: center bottom;'&gt;&lt;span id='stroke'&gt;${arr[1] == 1? x+'kg':''}&lt;/span&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt;
    &lt;td colspan='3'&gt;&lt;/td&gt;
    &lt;td colspan='3'&gt;&lt;/td&gt;
  &lt;/tr&gt;
&lt;/table&gt;

  &lt;table style='position: absolute;'&gt;
  &lt;tr&gt;
    &lt;td id='img'&gt;&lt;/td&gt;
    &lt;td&gt;&lt;/td&gt;
    &lt;td id='img' style='background-image:; background-repeat: no-repeat; background-position: center bottom;'&gt;&lt;span id='stroke'&gt;&lt;/span&gt;&lt;/td&gt;
    &lt;td id='img'&gt;&lt;/td&gt;
    &lt;td&gt;&lt;/td&gt;
    &lt;td id='img' style='background-image:; background-repeat: no-repeat; background-position: center bottom;'&gt;&lt;span id='stroke'&gt;&lt;/span&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt;
    &lt;td colspan='3'&gt;&lt;img src='@1sprite.src(scale)'/&gt;&lt;/td&gt;
    &lt;td colspan='3'&gt;&lt;img src='@1sprite.src(scale)'/&gt;&lt;/td&gt;
  &lt;/tr&gt;
&lt;/table&gt;

&lt;table style='position: absolute; top: 40px'&gt;
  &lt;tr&gt;
    &lt;td id='img'&gt;&lt;/td&gt;
    &lt;td&gt;&lt;/td&gt;
    &lt;td id='img'&gt;${(c == 0 &amp;&amp;  arr[0] == 0) || (c == 1 &amp;&amp; arr[0] == 1) ? "&lt;div id='circle'&gt;&lt;/div&gt;": ""}&lt;/td&gt;
    &lt;td id='img'&gt;&lt;/td&gt;
    &lt;td&gt;&lt;/td&gt;
    &lt;td id='img'&gt;${(c == 0 &amp;&amp;  arr[1] == 0) || (c == 1 &amp;&amp; arr[1] == 1) ? "&lt;div id='circle'&gt;&lt;/div&gt;": ""}&lt;/td&gt;
  &lt;/tr&gt;
&lt;/table&gt;
&lt;/div&gt;

&lt;span style='background: rgb(250, 250, 250, 0.4)'&gt;
${c == 0 ? "@2Hint: informal units have no exact weight.@2":"@2Hint: formal units have an exact weight.@2"}&lt;/br&gt;&lt;/center&gt;
  &lt;span style='background: rgb(250, 250, 250, 0.4)'&gt;
Using the &lt;b&gt;${c == 0 ? informal[type[0]]:x + ' ' + formal[type[1]]}&lt;/b&gt; is &lt;b&gt;${c == 0 ? 'less':'more'}&lt;/b&gt; accurate than using the &lt;b&gt;${c == 0 ? x + ' ' + formal[type[1]]:informal[type[0]]}&lt;/b&gt; to measure the mass of the ${object[o]}.
&lt;/span&gt;
                                                                                                                    </field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
</xml>
END_XML]] */