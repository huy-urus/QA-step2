
module.exports = [
  {
    "#type": "question",
    "name": "Y3.NA.AS.HA2.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y3.NA.AS.HA2.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "rank",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "range < 0"
        },
        "then": [
          {
            "#type": "variable",
            "name": "x",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "1"
              },
              "max": {
                "#type": "expression",
                "value": "98"
              }
            }
          },
          {
            "#type": "variable",
            "name": "y",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "1"
              },
              "max": {
                "#type": "expression",
                "value": "9 - (x % 10)"
              }
            }
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "range < 4"
            },
            "then": [
              {
                "#type": "variable",
                "name": "x",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "11"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "99"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "y1",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "1"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "3"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "y2",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "0"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "9 - (x % 10)"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "y",
                "value": {
                  "#type": "expression",
                  "value": "y1*10 + y2"
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "x",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "11"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "99"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "y1",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "1"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "9"
                  }
                }
              },
              {
                "#type": "if_then_else_block",
                "if": {
                  "#type": "expression",
                  "value": "y1 == 9"
                },
                "then": [
                  {
                    "#type": "variable",
                    "name": "y2",
                    "value": {
                      "#type": "random_number",
                      "min": {
                        "#type": "expression",
                        "value": "0"
                      },
                      "max": {
                        "#type": "expression",
                        "value": "9 - (x % 10) > 5 ? 5: 9 - (x % 10)"
                      }
                    }
                  }
                ],
                "else": [
                  {
                    "#type": "variable",
                    "name": "y2",
                    "value": {
                      "#type": "random_number",
                      "min": {
                        "#type": "expression",
                        "value": "0"
                      },
                      "max": {
                        "#type": "expression",
                        "value": "9 - (x % 10)"
                      }
                    }
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "y",
                "value": {
                  "#type": "expression",
                  "value": "y1*10 + y2"
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "40"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "Solve the following addition problem."
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "36"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "typex",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < x % 10"
        },
        "do": [
          {
            "#type": "variable",
            "name": "typex[i]",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "1"
              },
              "max": {
                "#type": "expression",
                "value": "4"
              }
            }
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "ox",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "variable",
        "name": "ox_start",
        "value": {
          "#type": "expression",
          "value": "ox"
        }
      },
      {
        "#type": "variable",
        "name": "oy",
        "value": {
          "#type": "expression",
          "value": "-30"
        }
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < x % 10"
        },
        "do": [
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "350 - ox"
                },
                "y": {
                  "#type": "expression",
                  "value": "130 + oy"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "0"
                },
                "height": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "1_${typex[i]}.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}1_${typex[i]}.png"
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          },
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "i == 3 || i == 6"
            },
            "then": [
              {
                "#type": "variable",
                "name": "ox",
                "value": {
                  "#type": "expression",
                  "value": "ox_start"
                }
              },
              {
                "#type": "variable",
                "name": "oy",
                "value": {
                  "#type": "expression",
                  "value": "oy +30"
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "ox",
                "value": {
                  "#type": "expression",
                  "value": "ox + 25"
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "(x % 10)  < 3"
        },
        "then": [
          {
            "#type": "variable",
            "name": "ox",
            "value": {
              "#type": "expression",
              "value": "25*(x % 10)"
            }
          }
        ],
        "else": [
          {
            "#type": "variable",
            "name": "ox",
            "value": {
              "#type": "expression",
              "value": "75"
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < Math.floor(x/10)"
        },
        "do": [
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "350 - ox"
                },
                "y": {
                  "#type": "expression",
                  "value": "130"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "0"
                },
                "height": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "10.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}10.png"
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          },
          {
            "#type": "variable",
            "name": "ox",
            "value": {
              "#type": "expression",
              "value": "ox + 25"
            }
          }
        ]
      },
      {
        "#type": "image_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "415"
            },
            "y": {
              "#type": "expression",
              "value": "130"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "0"
            },
            "height": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "plus.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}plus.png"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "variable",
        "name": "ox",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < Math.floor(y/10)"
        },
        "do": [
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "480 + ox"
                },
                "y": {
                  "#type": "expression",
                  "value": "130"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "0"
                },
                "height": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "10.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}10.png"
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          },
          {
            "#type": "variable",
            "name": "ox",
            "value": {
              "#type": "expression",
              "value": "ox + 25"
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "ox_start",
        "value": {
          "#type": "expression",
          "value": "ox"
        }
      },
      {
        "#type": "variable",
        "name": "typey",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < y % 10"
        },
        "do": [
          {
            "#type": "variable",
            "name": "typey[i]",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "1"
              },
              "max": {
                "#type": "expression",
                "value": "4"
              }
            }
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "variable",
        "name": "oy",
        "value": {
          "#type": "expression",
          "value": "-30"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < y % 10"
        },
        "do": [
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "480 + ox"
                },
                "y": {
                  "#type": "expression",
                  "value": "130 + oy"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "0"
                },
                "height": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "1_${typey[i]}.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}1_${typey[i]}.png"
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          },
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "i == 3 || i == 6"
            },
            "then": [
              {
                "#type": "variable",
                "name": "ox",
                "value": {
                  "#type": "expression",
                  "value": "ox_start"
                }
              },
              {
                "#type": "variable",
                "name": "oy",
                "value": {
                  "#type": "expression",
                  "value": "oy +30"
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "ox",
                "value": {
                  "#type": "expression",
                  "value": "ox + 25"
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "str",
        "value": {
          "#type": "expression",
          "value": "x + ' + ' + y + ' = _'"
        }
      },
      {
        "#type": "list_shape",
        "#props": [
          {
            "#type": "prop_list_direction",
            "#prop": "",
            "dir": "horizontal"
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "300"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_spacing",
            "#prop": "",
            "spacing": {
              "#type": "expression",
              "value": "10"
            }
          }
        ],
        "items": [
          {
            "#type": "json",
            "#props": [
              {
                "#type": "prop_list_align",
                "#prop": "",
                "align": "middle"
              },
              {
                "#type": "prop_list_item_source",
                "#prop": "",
                "source": {
                  "#type": "expression",
                  "value": "str"
                }
              }
            ],
            "template": {
              "#callback": "$item",
              "variable": "$item",
              "body": [
                {
                  "#type": "if_then_else_block",
                  "if": {
                    "#type": "expression",
                    "value": "$item.data == '+'"
                  },
                  "then": [
                    {
                      "#type": "image_shape",
                      "#props": [
                        {
                          "#type": "prop_image_key",
                          "#prop": "",
                          "key": "plus.png"
                        },
                        {
                          "#type": "prop_image_src",
                          "#prop": "",
                          "src": "${image_path}plus.png"
                        }
                      ]
                    }
                  ],
                  "else": [
                    {
                      "#type": "if_then_else_block",
                      "if": {
                        "#type": "expression",
                        "value": "$item.data == '='"
                      },
                      "then": [
                        {
                          "#type": "image_shape",
                          "#props": [
                            {
                              "#type": "prop_image_key",
                              "#prop": "",
                              "key": "equal.png"
                            },
                            {
                              "#type": "prop_image_src",
                              "#prop": "",
                              "src": "${image_path}equal.png"
                            }
                          ]
                        }
                      ],
                      "else": [
                        {
                          "#type": "if_then_else_block",
                          "if": {
                            "#type": "expression",
                            "value": "$item.data == '_'"
                          },
                          "then": [
                            {
                              "#type": "image_shape",
                              "#props": [
                                {
                                  "#type": "prop_image_key",
                                  "#prop": "",
                                  "key": "shape.png"
                                },
                                {
                                  "#type": "prop_image_src",
                                  "#prop": "",
                                  "src": "${image_path}shape.png"
                                }
                              ]
                            },
                            {
                              "#type": "choice_input_shape",
                              "#props": [
                                {
                                  "#type": "prop_value",
                                  "#prop": "",
                                  "value": {
                                    "#type": "expression",
                                    "value": "x+y"
                                  }
                                },
                                {
                                  "#type": "prop_size",
                                  "#prop": "",
                                  "width": {
                                    "#type": "expression",
                                    "value": "197"
                                  },
                                  "height": {
                                    "#type": "expression",
                                    "value": "84"
                                  }
                                },
                                {
                                  "#type": "prop_input_keyboard",
                                  "#prop": "",
                                  "keyboard": "numbers1"
                                },
                                {
                                  "#type": "prop_input_max_length",
                                  "#prop": "",
                                  "maxLength": {
                                    "#type": "expression",
                                    "value": "(x+y).toString().length"
                                  }
                                },
                                {
                                  "#type": "prop_input_result_position",
                                  "#prop": "",
                                  "resultPosition": "bottom"
                                },
                                {
                                  "#type": "prop_tab_order",
                                  "#prop": "",
                                  "tabOrder": {
                                    "#type": "expression",
                                    "value": "0"
                                  }
                                },
                                {
                                  "#type": "prop_stroke",
                                  "#prop": "stroke"
                                },
                                {
                                  "#type": "prop_fill",
                                  "#prop": "fill"
                                },
                                {
                                  "#prop": "",
                                  "style": {
                                    "#type": "json",
                                    "base": "text",
                                    "#props": [
                                      {
                                        "#type": "prop_text_style_font_size",
                                        "#prop": "",
                                        "fontSize": {
                                          "#type": "expression",
                                          "value": "42"
                                        }
                                      },
                                      {
                                        "#type": "prop_text_style_fill",
                                        "#prop": "",
                                        "fill": "black"
                                      },
                                      {
                                        "#type": "prop_text_style_stroke",
                                        "#prop": "",
                                        "stroke": "white"
                                      },
                                      {
                                        "#type": "prop_text_style_stroke_thickness",
                                        "#prop": "",
                                        "strokeThickness": {
                                          "#type": "expression",
                                          "value": "2"
                                        }
                                      }
                                    ]
                                  }
                                }
                              ],
                              "#init": "algorithmic_input"
                            }
                          ],
                          "else": [
                            {
                              "#type": "if_then_else_block",
                              "if": {
                                "#type": "expression",
                                "value": "$item.data == ' '"
                              },
                              "then": [
                                {
                                  "#type": "text_shape",
                                  "#props": [
                                    {
                                      "#type": "prop_position",
                                      "#prop": "",
                                      "x": {
                                        "#type": "expression",
                                        "value": "0"
                                      },
                                      "y": {
                                        "#type": "expression",
                                        "value": "0"
                                      }
                                    },
                                    {
                                      "#type": "prop_text_contents",
                                      "#prop": "",
                                      "contents": "${' '}"
                                    }
                                  ]
                                }
                              ],
                              "else": [
                                {
                                  "#type": "image_shape",
                                  "#props": [
                                    {
                                      "#type": "prop_image_key",
                                      "#prop": "",
                                      "key": "${$item.data}.png"
                                    },
                                    {
                                      "#type": "prop_image_src",
                                      "#prop": "",
                                      "src": "${image_path}${$item.data}.png"
                                    }
                                  ]
                                }
                              ]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Count the number of blocks that are in the picture altogether.</u></br></br>\n<style>\n  td{text-align: center}\nimg{margin: 5px}\ntable{max-width: 950px;}\n</style>\n<center>\n  <table style='background: rgb(150, 200, 250, 0.3); padding: 10px'>\n  <tr>\n  <td style='text-align: center;' colspan='2'>Group 1</td><td></td><td style='text-align: center;' colspan='2'>Group 2</td><td></td><td style='text-align: center;' colspan='2'>Total</td>\n  </tr>\n    <tr>\n      <td style=''>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i <  Math.floor(x/10)"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<img src=\"@sprite.src(10.png)\"/>"
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "i % 8 == 7"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</br>"
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</td><td style='text-align: left'>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < x % 10"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<img src=\"@sprite.src(1_${typex[i]}.png)\"/>"
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "i % 3 == 2"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</br>"
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</td><td><img  style='padding: 10px' src=\"@sprite.src(plus.png)\"/></td><td>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < Math.floor(y/10)"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<img src=\"@sprite.src(10.png)\"/>"
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "i % 8 == 7"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</br>"
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</td><td style='text-align: left'>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < y % 10"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<img src=\"@sprite.src(1_${typey[i]}.png)\"/>"
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "i % 3 == 2"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</br>"
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</td><td><img style='padding: 10px' src=\"@sprite.src(equal.png)\"/></td><td>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < Math.floor((x+y)/10)"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<img src=\"@sprite.src(10.png)\"/>"
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "i % 8 == 7"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</br>"
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</td><td style='text-align: left'>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < x % 10"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<img src=\"@sprite.src(1_${typex[i]}.png)\"/>"
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "i % 3 == 2"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</br>"
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < x % 10 + y % 10"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<img src=\"@sprite.src(1_${typey[i - x % 10]}.png)\"/>"
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "i % 3 == 2"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "</br>"
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</td>\n</tr>\n</table>\n\n</br>\n<span style='font-size: 42px; background: rgb(150, 200, 250, 0.3)'>${x} + ${y} = ${x+y}</span>\n</center>"
        ]
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="0" y="0">
    <field name="name">Y3.NA.AS.HA2.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y3.NA.AS.HA2.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="YepMQT@2uwp2rRPvmnRg1">
                                    <field name="name">rank</field>
                                    <value name="value">
                                      <block type="expression" id="UxgQ^Y5]rb}vpw[D=l-!">
                                        <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="variable" id="ROv]twq[7q~ll[o#H~LM">
                                        <field name="name">range</field>
                                        <value name="value">
                                          <block type="expression" id="t4]:21w3IF/JkJ:}uGMs">
                                            <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                          </block>
                                        </value>
                                        <next>
                                          <block type="if_then_else_block" id="e?xVm$au08[HN.@2d7yc.">
                                            <value name="if">
                                              <block type="expression" id="eENpai?Yne$K{V3v/P,;">
                                                <field name="value">range &lt; 0</field>
                                              </block>
                                            </value>
                                            <statement name="then">
                                              <block type="variable" id="i/VeO.ldc$+?x-Wb=PSr">
                                                <field name="name">x</field>
                                                <value name="value">
                                                  <block type="random_number" id="%Mn-gSvH#T]t2yRDQm)Y">
                                                    <value name="min">
                                                      <block type="expression" id=".vYARv)F@1k§!Vfq0%!">
                                                        <field name="value">1</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="wq@1aI;/2Of)rCK0]YZAj">
                                                        <field name="value">98</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="L07:mYO!a4.Q4VK,fo-$">
                                                    <field name="name">y</field>
                                                    <value name="value">
                                                      <block type="random_number" id="9pccb/r=Sm6z!Unc^o3T">
                                                        <value name="min">
                                                          <block type="expression" id="%$yfx(!}bnyP33VZvSHe">
                                                            <field name="value">1</field>
                                                          </block>
                                                        </value>
                                                        <value name="max">
                                                          <block type="expression" id="`|a4Lb4]s5}AL3ZR[4E?">
                                                            <field name="value">9 - (x % 10)</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </next>
                                              </block>
                                            </statement>
                                            <statement name="else">
                                              <block type="if_then_else_block" id=",Bs@2Hv4-Fa:mQrL+]PU3">
                                                <value name="if">
                                                  <block type="expression" id="XY@1jeb4^x^s0kE)?F0BW">
                                                    <field name="value">range &lt; 4</field>
                                                  </block>
                                                </value>
                                                <statement name="then">
                                                  <block type="variable" id="o))j12HB;ZQGrfrH]P-J">
                                                    <field name="name">x</field>
                                                    <value name="value">
                                                      <block type="random_number" id=";q]8RoV`ZFBKy)miq]gA">
                                                        <value name="min">
                                                          <block type="expression" id="M5j+OVHsnQTt[?,7c6C=">
                                                            <field name="value">11</field>
                                                          </block>
                                                        </value>
                                                        <value name="max">
                                                          <block type="expression" id="gYXBF1jf`g(-^zA=^RD[">
                                                            <field name="value">99</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id="0tA=4Gpn2z5p_em!6Y5J">
                                                        <field name="name">y1</field>
                                                        <value name="value">
                                                          <block type="random_number" id="cEe(uwE!xKqrMHr5x-YN">
                                                            <value name="min">
                                                              <block type="expression" id="UcvQa;_?sbf|ijBy/Wk;">
                                                                <field name="value">1</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="l_6+WHRBlT`|7~|xvYEB">
                                                                <field name="value">3</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="L.fijMbT=+^C-b$H+pa,">
                                                            <field name="name">y2</field>
                                                            <value name="value">
                                                              <block type="random_number" id="%c!kV^Ov,_oku`zKo@1P1">
                                                                <value name="min">
                                                                  <block type="expression" id=")oA+(d$e#l@2HXuov:1$y">
                                                                    <field name="value">0</field>
                                                                  </block>
                                                                </value>
                                                                <value name="max">
                                                                  <block type="expression" id="2t(vxvtTfu;a_gF]67jr">
                                                                    <field name="value">9 - (x % 10)</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="variable" id="qM8KzhDPM?+9VwNL:{`@1">
                                                                <field name="name">y</field>
                                                                <value name="value">
                                                                  <block type="expression" id="M5u@2RAx=S/5fW^LaV6=e">
                                                                    <field name="value">y1@210 + y2</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </statement>
                                                <statement name="else">
                                                  <block type="variable" id="@2#,qlL)-@1k$(WSSf!]mB">
                                                    <field name="name">x</field>
                                                    <value name="value">
                                                      <block type="random_number" id="1nu[t.f)^^YUo/.g#)$?">
                                                        <value name="min">
                                                          <block type="expression" id=":?L+AkBb-xgBl-3ziYQI">
                                                            <field name="value">11</field>
                                                          </block>
                                                        </value>
                                                        <value name="max">
                                                          <block type="expression" id="@2Ri}C?w+Qx@2Ppl?p3kgf">
                                                            <field name="value">99</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id="`B%Qk!j}o#a:ARd{OHVm">
                                                        <field name="name">y1</field>
                                                        <value name="value">
                                                          <block type="random_number" id="`4Q_z`HG^.ot([)ni)vP">
                                                            <value name="min">
                                                              <block type="expression" id="ai=#kc.Cm}gB5J#XI/PR">
                                                                <field name="value">1</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="c5V!gal~$7QUXWsT%-`i">
                                                                <field name="value">9</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="if_then_else_block" id="~k^Ah1_C+o.T?ra)~nlP">
                                                            <value name="if">
                                                              <block type="expression" id="-r#):Jp9mLMGY|)3i|T3">
                                                                <field name="value">y1 == 9</field>
                                                              </block>
                                                            </value>
                                                            <statement name="then">
                                                              <block type="variable" id="#2jh^|3t]l5c5RBq!S%Z">
                                                                <field name="name">y2</field>
                                                                <value name="value">
                                                                  <block type="random_number" id="M8l2#FX(?s#n=:|$y3@27">
                                                                    <value name="min">
                                                                      <block type="expression" id=",U{DqktAIR8HsG.zB-~}">
                                                                        <field name="value">0</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="max">
                                                                      <block type="expression" id="#A(i;Akk:/T~~^{#+VmV">
                                                                        <field name="value">9 - (x % 10) &gt; 5 ? 5: 9 - (x % 10)</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </statement>
                                                            <statement name="else">
                                                              <block type="variable" id="Y2IO=+pInvygCm:fozH,">
                                                                <field name="name">y2</field>
                                                                <value name="value">
                                                                  <block type="random_number" id="@1X#{opAjI5yV9!Y+XAiN">
                                                                    <value name="min">
                                                                      <block type="expression" id="jM3v9yLVb93M9]}/+b{-">
                                                                        <field name="value">0</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="max">
                                                                      <block type="expression" id="SCbPa@1Vg_O-PQbR+ttld">
                                                                        <field name="value">9 - (x % 10)</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </statement>
                                                            <next>
                                                              <block type="variable" id="jm#%ybTiu9BL#8m_[co/">
                                                                <field name="name">y</field>
                                                                <value name="value">
                                                                  <block type="expression" id="~~q~fSCLW9%#xh$[s_4;">
                                                                    <field name="value">y1@210 + y2</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </statement>
                                              </block>
                                            </statement>
                                            <next>
                                              <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                <statement name="#props">
                                                  <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                    <field name="#prop"></field>
                                                    <value name="x">
                                                      <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                        <field name="value">400</field>
                                                      </block>
                                                    </value>
                                                    <value name="y">
                                                      <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                        <field name="value">40</field>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="prop_anchor" id="O.v@2mKn@1K#+tF;|}C}xI">
                                                        <field name="#prop">anchor</field>
                                                        <value name="x">
                                                          <block type="expression" id="+3%GU[X3l%%X`Jv1QL7@2">
                                                            <field name="value">0.5</field>
                                                          </block>
                                                        </value>
                                                        <value name="y">
                                                          <block type="expression" id="Kzp23HjHF7k]A-y1BJPm">
                                                            <field name="value">0.5</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                            <field name="#prop"></field>
                                                            <value name="contents">
                                                              <block type="string_value" id="#6btV9p,PGV}wMx.Uï¿½">
                                                                <field name="value">Solve the following addition problem.</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                <field name="base">text</field>
                                                                <statement name="#props">
                                                                  <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                    <field name="#prop"></field>
                                                                    <value name="fontSize">
                                                                      <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                        <field name="value">36</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                        <field name="#prop"></field>
                                                                        <value name="fill">
                                                                          <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                            <field name="value">black</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                            <field name="#prop"></field>
                                                                            <value name="stroke">
                                                                              <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                <field name="value">white</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                <field name="#prop"></field>
                                                                                <value name="strokeThickness">
                                                                                  <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                    <field name="value">2</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </statement>
                                                <next>
                                                  <block type="variable" id="v5THn8O5:cr@1yT2@2V/Q,">
                                                    <field name="name">typex</field>
                                                    <value name="value">
                                                      <block type="expression" id="IkY)qm6@1s#GRg@2$ppn4q">
                                                        <field name="value">[]</field>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id="?w57:oRJcufBMBPkE8Tg">
                                                        <field name="name">i</field>
                                                        <value name="value">
                                                          <block type="expression" id="|h|=fb$:HOk8pQRxv`Q0">
                                                            <field name="value">0</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="while_do_block" id="4C{/i!=6=4];;,GC/lu:">
                                                            <value name="while">
                                                              <block type="expression" id="~pIuGsQgk8K]R@1!@1|?$~">
                                                                <field name="value">i &lt; x % 10</field>
                                                              </block>
                                                            </value>
                                                            <statement name="do">
                                                              <block type="variable" id="j$;+y9S|WkGl;`Qp2WxZ">
                                                                <field name="name">typex[i]</field>
                                                                <value name="value">
                                                                  <block type="random_number" id="cw2DC/qjq.T3,gHr5+E3">
                                                                    <value name="min">
                                                                      <block type="expression" id="bM_%ZKY(w|Cs}(X}3_@2Z">
                                                                        <field name="value">1</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="max">
                                                                      <block type="expression" id="TTBYFf).t!,/;KCB+gxp">
                                                                        <field name="value">4</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="variable" id="3.CagRQFS(Zrk8wMvs.4">
                                                                    <field name="name">i</field>
                                                                    <value name="value">
                                                                      <block type="expression" id="XrwERq4Ev_z~b|AK{m?L">
                                                                        <field name="value">i+1</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </statement>
                                                            <next>
                                                              <block type="variable" id="5Z|}DFk!t@1Siow6=^IB[">
                                                                <field name="name">ox</field>
                                                                <value name="value">
                                                                  <block type="expression" id="eL3Qe!X?i@28Bk0JC1W%S">
                                                                    <field name="value">0</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="variable" id="6u6kwBy5^rZwoeb_ASpp">
                                                                    <field name="name">ox_start</field>
                                                                    <value name="value">
                                                                      <block type="expression" id="(y0!Cu!V2ZeKwEvy/xE(">
                                                                        <field name="value">ox</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="variable" id="4(tPfF2$/~|@2ep?7:QQM">
                                                                        <field name="name">oy</field>
                                                                        <value name="value">
                                                                          <block type="expression" id="@2(NHO]Y:?nK^QDX{HI3i">
                                                                            <field name="value">-30</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="ufZYLr2-UIkWWhE][e5y">
                                                                            <field name="name">i</field>
                                                                            <value name="value">
                                                                              <block type="expression" id="=5l9;2KU-(u=m0PP}iX/">
                                                                                <field name="value">0</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="while_do_block" id="S:S+}LG_12{%)9hpzjxd">
                                                                                <value name="while">
                                                                                  <block type="expression" id="=iD8:d{$`f8g0t@2[n^r=">
                                                                                    <field name="value">i &lt; x % 10</field>
                                                                                  </block>
                                                                                </value>
                                                                                <statement name="do">
                                                                                  <block type="image_shape" id="1flx7K]T0ZV=_8CKdA81">
                                                                                    <statement name="#props">
                                                                                      <block type="prop_position" id="Wp7R8GTHqoV@2-?:|ggQg">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="W^51|-uoyISw)|nmaSy@1">
                                                                                            <field name="value">350 - ox</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="{zKzdK5h#(.NMe3jWGwo">
                                                                                            <field name="value">130 + oy</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_size" id="Yy/|-B`}-`;kzF1Qv,Li">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="width">
                                                                                              <block type="expression" id="DKSPZpi$4MHAl|,t0%Rt">
                                                                                                <field name="value">0</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="height">
                                                                                              <block type="expression" id="T-O_`!g2CoJ@1|0}uWJ^`">
                                                                                                <field name="value">0</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_image_key" id="|lp[p4(H1d1lHQF6C{Kp">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="key">
                                                                                                  <block type="string_value" id="@1y5FeKO/}Pk~aX?h/|vl">
                                                                                                    <field name="value">1_${typex[i]}.png</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_image_src" id="?dZNbJ{6s6ATJ^}:sgr2">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="src">
                                                                                                      <block type="string_value" id="Y?8yc!Syt,WRWGQFv}R6">
                                                                                                        <field name="value">${image_path}1_${typex[i]}.png</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <next>
                                                                                      <block type="variable" id="T)3YDt%1VO{rK0IZ@2P5,">
                                                                                        <field name="name">i</field>
                                                                                        <value name="value">
                                                                                          <block type="expression" id="O{CH.2Vl^dzlIPOWD.8g">
                                                                                            <field name="value">i+1</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="if_then_else_block" id="aj1rnu){S;P`qSI[#c-/">
                                                                                            <value name="if">
                                                                                              <block type="expression" id="f$HNdxK`PB_ou,j%+$be">
                                                                                                <field name="value">i == 3 || i == 6</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <statement name="then">
                                                                                              <block type="variable" id="D2CN1cUoI8jHa)yxBDFl">
                                                                                                <field name="name">ox</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="O@2+`ry%o)HyLB%am[2%H">
                                                                                                    <field name="value">ox_start</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="variable" id="vZ|}X6s7pSO`$$Fg+f^y">
                                                                                                    <field name="name">oy</field>
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="e1m%}Lz`!hKnKlnSG0Vo">
                                                                                                        <field name="value">oy +30</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <statement name="else">
                                                                                              <block type="variable" id="b!2Y+YZ@2c/4IO::kVfMN">
                                                                                                <field name="name">ox</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="Gv:MXB7f20syh%SYIl_$">
                                                                                                    <field name="value">ox + 25</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </statement>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="if_then_else_block" id="[g,K2IJ(qDU}|nj#Y7Jo">
                                                                                    <value name="if">
                                                                                      <block type="expression" id="k+meIETI$g1OYnpciIwd">
                                                                                        <field name="value">(x % 10)  &lt; 3</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <statement name="then">
                                                                                      <block type="variable" id="(p0TRL:iV.xYq%4^f}Rj">
                                                                                        <field name="name">ox</field>
                                                                                        <value name="value">
                                                                                          <block type="expression" id="arr?(rY:d-UGhG)72wF^">
                                                                                            <field name="value">25@2(x % 10)</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <statement name="else">
                                                                                      <block type="variable" id="-@1}8]@2{r,M1Fvv3II]`Z">
                                                                                        <field name="name">ox</field>
                                                                                        <value name="value">
                                                                                          <block type="expression" id="Ne7TE4(NfbG|zXUFxAe=">
                                                                                            <field name="value">75</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <next>
                                                                                      <block type="variable" id="Eyq_xs9[|5;jOU9X/0tT">
                                                                                        <field name="name">i</field>
                                                                                        <value name="value">
                                                                                          <block type="expression" id="`Ca_@1)rp@2p#6P-s#T:@1F">
                                                                                            <field name="value">0</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="while_do_block" id="UboHv#;R^;Wu-C27@1!O5">
                                                                                            <value name="while">
                                                                                              <block type="expression" id="F/{yZp9ptZ[glb4mcA8U">
                                                                                                <field name="value">i &lt; Math.floor(x/10)</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <statement name="do">
                                                                                              <block type="image_shape" id=",a`3X[cQZ}va$:J95q.Q">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_position" id="9Q9)Dm.,U9Qzc5@1Y{iD~">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="}~6VF+}APGOY;iI`esM?">
                                                                                                        <field name="value">350 - ox</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="W`F_9@2@2edxYP@2z/4s^A6">
                                                                                                        <field name="value">130</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_size" id="9vHW@1y+#`6A)4`~W5AJq">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="width">
                                                                                                          <block type="expression" id="3XUQOrPt6y/8iDoAnre(">
                                                                                                            <field name="value">0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="height">
                                                                                                          <block type="expression" id="EdB^0c(#0O`MZ:i$1sU{">
                                                                                                            <field name="value">0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_image_key" id="5a1%1@2;/+H}pU3_|x@2:N">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="key">
                                                                                                              <block type="string_value" id="kD!dU[[Xr|;,xAag%{7`">
                                                                                                                <field name="value">10.png</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_image_src" id="`Q%h4pR7AYa{?XQ+.m$Y">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="src">
                                                                                                                  <block type="string_value" id="}ThlZgS9`iii~}AFz{s4">
                                                                                                                    <field name="value">${image_path}10.png</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="variable" id="-5PT;6io@1J%p$V9$d/$_">
                                                                                                    <field name="name">i</field>
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="|`|}6sJ3jh:rVfSnsxoD">
                                                                                                        <field name="value">i+1</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="variable" id="qJKo8g_cN#!y/%)HYNGY">
                                                                                                        <field name="name">ox</field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id="EYVyEh|hzz|isGP3_nd7">
                                                                                                            <field name="value">ox + 25</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="image_shape" id="L^3;7|P@2@1]V3rKX?Yzw]">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_position" id="n6a/q~RcBe#NfI@1@1r1^7">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="@1p`im(Kw7.Wlh)0IZ)l(">
                                                                                                        <field name="value">415</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="T|i~(-M5q4HvkX@22y6ry">
                                                                                                        <field name="value">130</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_size" id="uvIJhpxp|BR@1K~I`rtB4">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="width">
                                                                                                          <block type="expression" id="5RM)Wrr8i5}x_|t{CA)O">
                                                                                                            <field name="value">0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="height">
                                                                                                          <block type="expression" id="tt,a:h^%_`4lrPapcE0Z">
                                                                                                            <field name="value">0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_image_key" id="SyKZbI_5B/G@2ies}Dt^B">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="key">
                                                                                                              <block type="string_value" id="Q}cJu3pDo2}e{99!{r^=">
                                                                                                                <field name="value">plus.png</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_image_src" id="#p+8mb@2%n6VIB=P{8j0C">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="src">
                                                                                                                  <block type="string_value" id="S{Ow74@2?WM@1YSvhfGOP7">
                                                                                                                    <field name="value">${image_path}plus.png</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="variable" id="|m!1.!:0}Pn/dHhe[]#y">
                                                                                                    <field name="name">i</field>
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="eG|C6,HW]EvqEz2Z`k~a">
                                                                                                        <field name="value">0</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="variable" id="(pvNJ17G@22+:,Za2XN2F">
                                                                                                        <field name="name">ox</field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id="b_kz@1i.]Wr4N5XBUj@1GC">
                                                                                                            <field name="value">0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="while_do_block" id="|#:(yEo=r%brLV.//nIw">
                                                                                                            <value name="while">
                                                                                                              <block type="expression" id="^R?RrKTcXO4kXGzS9H,R">
                                                                                                                <field name="value">i &lt; Math.floor(y/10)</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <statement name="do">
                                                                                                              <block type="image_shape" id="JfEyeP.PmMv5c9i}VmWZ">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_position" id="jiStJ$-x77;0mzZa_,P^">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="t2$9-[xq(,9xwkPC%S2@2">
                                                                                                                        <field name="value">480 + ox</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="`NEx;]b0C~WpqD{@1F^06">
                                                                                                                        <field name="value">130</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_size" id=":|Z{F6~+B:xkYnU@2A!08">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="width">
                                                                                                                          <block type="expression" id="]k+c5a6GyLdUC(}1!Er0">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="height">
                                                                                                                          <block type="expression" id="^yDh-T/+Z1qDGVu=-@2qi">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_key" id="(EV52LhR8`9EFy22@2Og#">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="key">
                                                                                                                              <block type="string_value" id="$l0zfcJ#c4H?Bm3N}%v-">
                                                                                                                                <field name="value">10.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_src" id="G/{4f?Hu%UOlZgEf3j}p">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="src">
                                                                                                                                  <block type="string_value" id="=@2KkutY_{S,^]77t8kON">
                                                                                                                                    <field name="value">${image_path}10.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="variable" id="{jH0//ge4b:wMb=)ziq6">
                                                                                                                    <field name="name">i</field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="nknZ7~MP^Oo/a@1,9?Z3L">
                                                                                                                        <field name="value">i+1</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="variable" id="^6f1FV_.l_xU,?E4diiu">
                                                                                                                        <field name="name">ox</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id="@1:,E())=IU:/?dfTQ!)V">
                                                                                                                            <field name="value">ox + 25</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="variable" id="l}~D8t/,wet`wh_0=n.5">
                                                                                                                <field name="name">ox_start</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="$Vur7UbwCVS-jd;OQ.-o">
                                                                                                                    <field name="value">ox</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="variable" id="`x~k%{7}m)8.OTyT0h@19">
                                                                                                                    <field name="name">typey</field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="]3]I)|+c(-g|}wI^#-m/">
                                                                                                                        <field name="value">[]</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="variable" id="9$$Qanfs//Zoo7wHW#@1W">
                                                                                                                        <field name="name">i</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id="lQG9m@2Wi$o!rng,m1wTM">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="while_do_block" id="7;b-qw:vlq7b=~z9o0`:">
                                                                                                                            <value name="while">
                                                                                                                              <block type="expression" id="d~62QeY``A7DOaA}x#sv">
                                                                                                                                <field name="value">i &lt; y % 10</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <statement name="do">
                                                                                                                              <block type="variable" id="mIb+?f;sQ.6@2YnqiHkRM">
                                                                                                                                <field name="name">typey[i]</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="random_number" id="X=d-I:)mN[QWwmLhrv?S">
                                                                                                                                    <value name="min">
                                                                                                                                      <block type="expression" id="tb=leevWX$2hW[oGnw+X">
                                                                                                                                        <field name="value">1</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="max">
                                                                                                                                      <block type="expression" id="FJF(5Ke!R+^=1W#5s,(b">
                                                                                                                                        <field name="value">4</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="variable" id="tz^~FU4L3EzQkltXT)IZ">
                                                                                                                                    <field name="name">i</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="h^hd@1![cChaMG%mr+GOe">
                                                                                                                                        <field name="value">i+1</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="variable" id="PRS?_|d,KI_U$tVUxe3$">
                                                                                                                                <field name="name">i</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="ImbS|-txmt7k}LGIdK_@1">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="variable" id="]I/Ot#omExaUsPGR5JA{">
                                                                                                                                    <field name="name">oy</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="/hz[W5[U@1R+omeeFh}VZ">
                                                                                                                                        <field name="value">-30</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="while_do_block" id="ucR-tITp~c@2FBvr31(/8">
                                                                                                                                        <value name="while">
                                                                                                                                          <block type="expression" id="mqD4C%Czjpca=i|yK|$q">
                                                                                                                                            <field name="value">i &lt; y % 10</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <statement name="do">
                                                                                                                                          <block type="image_shape" id="?Oi.`AT;|u(cT$D;EKzA">
                                                                                                                                            <statement name="#props">
                                                                                                                                              <block type="prop_position" id="o44E^aGle8#3A@2u`[`X{">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="x">
                                                                                                                                                  <block type="expression" id="[$4q(X-@1]TkL|OF95:6m">
                                                                                                                                                    <field name="value">480 + ox</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <value name="y">
                                                                                                                                                  <block type="expression" id="@2O(e1zr@2CCwS:e3lfx{2">
                                                                                                                                                    <field name="value">130 + oy</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_size" id="Ua0uTmw[XYxFG3?KHipT">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="width">
                                                                                                                                                      <block type="expression" id="H6ERdF8NQwu,`MheXV@2j">
                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="height">
                                                                                                                                                      <block type="expression" id="AW#b}b-8.R,beGD8Tl^!">
                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_image_key" id="zre7:=CO;a?{M7j^D?hF">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="key">
                                                                                                                                                          <block type="string_value" id="!Ua`QkouF]7JqOA5JufR">
                                                                                                                                                            <field name="value">1_${typey[i]}.png</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_image_src" id="%roGp:=|XNA;-h)dZQ)3">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="src">
                                                                                                                                                              <block type="string_value" id="(7_j!{hhz!c#ry^+Uy%W">
                                                                                                                                                                <field name="value">${image_path}1_${typey[i]}.png</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                            <next>
                                                                                                                                              <block type="variable" id="b+E.[OH[$g9lq)O6,5,C">
                                                                                                                                                <field name="name">i</field>
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="expression" id="hhADd+_a1%7_-O,isc?g">
                                                                                                                                                    <field name="value">i+1</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="if_then_else_block" id="ACyoRNj6rn1tumo7vgQi">
                                                                                                                                                    <value name="if">
                                                                                                                                                      <block type="expression" id="(F15Bf/yIk%d+k_!~RzX">
                                                                                                                                                        <field name="value">i == 3 || i == 6</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <statement name="then">
                                                                                                                                                      <block type="variable" id="61a;l{kyjxyS8|JbW+BL">
                                                                                                                                                        <field name="name">ox</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="XPO^nAq3=WFJyQB,LT%_">
                                                                                                                                                            <field name="value">ox_start</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="variable" id="k?QVVkr5GzNxH:fq)|{@1">
                                                                                                                                                            <field name="name">oy</field>
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id="`5-u%L+q;Sb8TE|[/9/Z">
                                                                                                                                                                <field name="value">oy +30</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <statement name="else">
                                                                                                                                                      <block type="variable" id="q57}}22h{pc$6wiYVL=H">
                                                                                                                                                        <field name="name">ox</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="+@1}+mh%[WZwi[2%}Cwi(">
                                                                                                                                                            <field name="value">ox + 25</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <next>
                                                                                                                                          <block type="variable" id="}xddJ^7ArmaqbPtz_8hH">
                                                                                                                                            <field name="name">str</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id="|8swM/LC{E/w-V^ubxgz">
                                                                                                                                                <field name="value">x + ' + ' + y + ' = _'</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="list_shape" id="Bg6d_1~9f8]KB#Ays6I;">
                                                                                                                                                <statement name="#props">
                                                                                                                                                  <block type="prop_list_direction" id="6@2CBfnqbK##aAt)4pG`2">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <field name="dir">horizontal</field>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_position" id="A70#gdg59CVyF6cJ$$5R">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="x">
                                                                                                                                                          <block type="expression" id="sBM`K((KWq-VrO@2u5;yz">
                                                                                                                                                            <field name="value">400</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="y">
                                                                                                                                                          <block type="expression" id="4Z0V@2hk_b1[B4?8)ds.+">
                                                                                                                                                            <field name="value">300</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_anchor" id="@1QbjKy7wuWC}{T+fQ32r">
                                                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                                                            <value name="x">
                                                                                                                                                              <block type="expression" id="C|ez%Oe(`oqDuXhpw(|Y">
                                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="y">
                                                                                                                                                              <block type="expression" id="o+:HWmaaC)O}=`NfLT">
                                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_spacing" id="{So]$.t)+StmICX,eoWp">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="spacing">
                                                                                                                                                                  <block type="expression" id="xq}{Vod2^Wbz{cykAx+B">
                                                                                                                                                                    <field name="value">10</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                                <statement name="items">
                                                                                                                                                  <block type="list_item_shape" id="[jlZcppnQIy8@2]Ts1s[}">
                                                                                                                                                    <statement name="#props">
                                                                                                                                                      <block type="prop_list_align" id="Hx=2j9Oc@1IF+%:i]=Z+C">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <field name="align">middle</field>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_list_item_source" id="L5WgPaJ,JZ0--^S-U?kP">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="source">
                                                                                                                                                              <block type="expression" id="xPMOXXW}A|%USHP(X-pA">
                                                                                                                                                                <field name="value">str</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <statement name="template">
                                                                                                                                                      <block type="if_then_else_block" id="`,m@29gh9|jJ!h0IRLdb`">
                                                                                                                                                        <value name="if">
                                                                                                                                                          <block type="expression" id="gfLdSTJ0Jt{dlg;U8mDD">
                                                                                                                                                            <field name="value">$item.data == '+'</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <statement name="then">
                                                                                                                                                          <block type="image_shape" id="u|)26LN5hsuGb5~KU%uJ">
                                                                                                                                                            <statement name="#props">
                                                                                                                                                              <block type="prop_image_key" id="Y?R@2uQ5C#vVh^j+Q#eSb">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="key">
                                                                                                                                                                  <block type="string_value" id="0U(!S.U1@1k+r(Zv^;8;z">
                                                                                                                                                                    <field name="value">plus.png</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_image_src" id="jVmVi4r?MQj0agfq-n|7">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="src">
                                                                                                                                                                      <block type="string_value" id="nky@2i}N.~a;DnikVw2wL">
                                                                                                                                                                        <field name="value">${image_path}plus.png</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                        <statement name="else">
                                                                                                                                                          <block type="if_then_else_block" id=")K+RD];QYAHgbfqK.f]s">
                                                                                                                                                            <value name="if">
                                                                                                                                                              <block type="expression" id="j|bMlh95?`%/2y.,m3lW">
                                                                                                                                                                <field name="value">$item.data == '='</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <statement name="then">
                                                                                                                                                              <block type="image_shape" id="i:qcTZ=;=$VfH8^cgn6y">
                                                                                                                                                                <statement name="#props">
                                                                                                                                                                  <block type="prop_image_key" id="6L2Zu(oM$hI;DriiCz.h">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="key">
                                                                                                                                                                      <block type="string_value" id="=-=+VXKRLQl.Z!]LSL)-">
                                                                                                                                                                        <field name="value">equal.png</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_image_src" id="b)9z[6@2zF:|I8Q4o/FGL">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="src">
                                                                                                                                                                          <block type="string_value" id="$d/f_)$%?+kw$HKi5g2P">
                                                                                                                                                                            <field name="value">${image_path}equal.png</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                            <statement name="else">
                                                                                                                                                              <block type="if_then_else_block" id="O|:PY/v9cjrLZ]5x/g}8">
                                                                                                                                                                <value name="if">
                                                                                                                                                                  <block type="expression" id="_zRgA4+E.G1I~{Q.143y">
                                                                                                                                                                    <field name="value">$item.data == '_'</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <statement name="then">
                                                                                                                                                                  <block type="image_shape" id="[+mhM[|z;ZO.7@1jsxkIg">
                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                      <block type="prop_image_key" id="I@1aW1.;[6iR,7u#I2Va!">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="key">
                                                                                                                                                                          <block type="string_value" id="x|6l~jM:l4hHo|vPHb0?">
                                                                                                                                                                            <field name="value">shape.png</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_image_src" id="s]o#83?^f89)nrP3]Dja">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="src">
                                                                                                                                                                              <block type="string_value" id="Z6_+@2p9g0oYX$LDL@28@2?">
                                                                                                                                                                                <field name="value">${image_path}shape.png</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="algorithmic_input_shape" id="[e}LcmqF@19.%T?]@1jB-Z">
                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                          <block type="prop_value" id="TN6clfe}:4H;@1k0w%+`S">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="value">
                                                                                                                                                                              <block type="expression" id="@1Y0=VVDuz^gR8?]A.GgV">
                                                                                                                                                                                <field name="value">x+y</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_size" id="~)S$^+Glx|;{-)@1m?MCt">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="width">
                                                                                                                                                                                  <block type="expression" id="wdRRw?lccA]3huvks^0:">
                                                                                                                                                                                    <field name="value">197</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <value name="height">
                                                                                                                                                                                  <block type="expression" id="FlQ@1R-hpt5|W3YEsjl5h">
                                                                                                                                                                                    <field name="value">84</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_input_keyboard" id="pX[+1]_P03Uw?r6RhxOm">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <field name="keyboard">numbers1</field>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_input_max_length" id="e^196@2S|6ukB7~2Bxi9C">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="maxLength">
                                                                                                                                                                                          <block type="expression" id="/_E;-7``ONG+]nS_NxH+">
                                                                                                                                                                                            <field name="value">(x+y).toString().length</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_input_result_position" id="#LH4fkex}OZhG#[@2d;3P">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <field name="resultPosition">bottom</field>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_tab_order" id="s|F@1Izb{|@2lRY,asD!hu">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <value name="tabOrder">
                                                                                                                                                                                                  <block type="expression" id="2[il+_dgVQKQm?SvtFDj">
                                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="prop_stroke" id="D`nCliS/s1emx9euC0}t">
                                                                                                                                                                                                    <field name="#prop">stroke</field>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="prop_fill" id="Dz-AaH/!wx?BO[MH9dIz">
                                                                                                                                                                                                        <field name="#prop">fill</field>
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="prop_text_style" id="!1Qr3{IxD@1PMJ]G^LlcN">
                                                                                                                                                                                                            <field name="base">text</field>
                                                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                                                              <block type="prop_text_style_font_size" id="!(nlSpda9T+o/iXBj}YF">
                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                <value name="fontSize">
                                                                                                                                                                                                                  <block type="expression" id="FQTZZB_+$@1-hM?K.!NSO">
                                                                                                                                                                                                                    <field name="value">42</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                  <block type="prop_text_style_fill" id="w-4`M-zsemYrO[}g`_|q">
                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                    <value name="fill">
                                                                                                                                                                                                                      <block type="string_value" id="G;VmNGg}u8OZR_#3M_Oe">
                                                                                                                                                                                                                        <field name="value">black</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                      <block type="prop_text_style_stroke" id="ahppz0%Y{eD@2C$!vyJ4m">
                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                        <value name="stroke">
                                                                                                                                                                                                                          <block type="string_value" id="}KNcD$1%M)+@1a=7A%7,,">
                                                                                                                                                                                                                            <field name="value">white</field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                          <block type="prop_text_style_stroke_thickness" id="TBWlBG{V@2q@2LOjXi7|@16">
                                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                                            <value name="strokeThickness">
                                                                                                                                                                                                                              <block type="expression" id="bs_awM@2W;X@1OG#fQ+@2no">
                                                                                                                                                                                                                                <field name="value">2</field>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </next>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                                <statement name="else">
                                                                                                                                                                  <block type="if_then_else_block" id="m%tLjN%_ZMqz]w~Z(~S(">
                                                                                                                                                                    <value name="if">
                                                                                                                                                                      <block type="expression" id="ik2[Y,!EYzN,m0]_t+y}">
                                                                                                                                                                        <field name="value">$item.data == ' '</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <statement name="then">
                                                                                                                                                                      <block type="text_shape" id="Cjnb/{a1~%8OkfY|j7iv">
                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                          <block type="prop_position" id="(Mz/Fw[y~|_,I1-Yw@2z$">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="x">
                                                                                                                                                                              <block type="expression" id="E@28,PX-4aN$m-69vMhXX">
                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="y">
                                                                                                                                                                              <block type="expression" id=",G20;~=tGa}1#n:T/u24">
                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_text_contents" id="/-eMOnB@2nw%}f#ttjA{0">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="contents">
                                                                                                                                                                                  <block type="string_value" id="@1qi=,SM}[+8,q;o#/P_k">
                                                                                                                                                                                    <field name="value">${' '}</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                    <statement name="else">
                                                                                                                                                                      <block type="image_shape" id="eyr%vWom|24b,lu.|8c?">
                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                          <block type="prop_image_key" id="Do)LADy-/zPW;]J@1vhS.">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="key">
                                                                                                                                                                              <block type="string_value" id="SWoa/m]Wn|5QDbe|5X[V">
                                                                                                                                                                                <field name="value">${$item.data}.png</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_image_src" id="|%sY:C7enA|#bNI~#x-E">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="src">
                                                                                                                                                                                  <block type="string_value" id="?@1=@2t.ZL4%#=w2:8Btx1">
                                                                                                                                                                                    <field name="value">${image_path}${$item.data}.png</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                                <next>
                                                                                                                                                  <block type="partial_explanation" id="531X2XFdDb`~L)JU7d}(" inline="true">
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="string_value" id="h=UDnY2GPwXED-]R8b+d">
                                                                                                                                                        <field name="value">&lt;u&gt;Count the number of blocks that are in the picture altogether.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;
&lt;style&gt;
  td{text-align: center}
img{margin: 5px}
table{max-width: 950px;}
&lt;/style&gt;
&lt;center&gt;
  &lt;table style='background: rgb(150, 200, 250, 0.3); padding: 10px'&gt;
  &lt;tr&gt;
  &lt;td style='text-align: center;' colspan='2'&gt;Group 1&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;td style='text-align: center;' colspan='2'&gt;Group 2&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;td style='text-align: center;' colspan='2'&gt;Total&lt;/td&gt;
  &lt;/tr&gt;
    &lt;tr&gt;
      &lt;td style=''&gt;
                                                                                                                                                        </field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="variable" id="6/J3q.m,#9u;}iih!w8w">
                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="uyUITxtSPVJu+zlY9H5h">
                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="while_do_block" id="!:02.a?FOp}A=3mc1+_8">
                                                                                                                                                            <value name="while">
                                                                                                                                                              <block type="expression" id="{+uI0JGrfzW7o^={D6g^">
                                                                                                                                                                <field name="value">i &lt;  Math.floor(x/10)</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <statement name="do">
                                                                                                                                                              <block type="partial_explanation" id=":]2AUb=bJ8A`7z4K%W/-" inline="true">
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="string_value" id="$Z0#2ZlL9cPW-44)7)pW">
                                                                                                                                                                    <field name="value">&lt;img src="@1sprite.src(10.png)"/&gt;</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="if_then_block" id="tKu6Y//xkD%Zp]343j;p">
                                                                                                                                                                    <value name="if">
                                                                                                                                                                      <block type="expression" id=")xlE{O0C;79t:G=c5:%H">
                                                                                                                                                                        <field name="value">i % 8 == 7</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <statement name="then">
                                                                                                                                                                      <block type="partial_explanation" id="m!wN97/l+EEO.{FjF$$B" inline="true">
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="string_value" id="5fCJwCK54v{{ACU5q@1`o">
                                                                                                                                                                            <field name="value">&lt;/br&gt;</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="variable" id="h[?I[Hg@1vP0zxg3di^oV">
                                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="expression" id="eEhe!scFAm3v](xrwYD_">
                                                                                                                                                                            <field name="value">i+1</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="partial_explanation" id="l2k+XB)Y;[sArj@2Y8uCr" inline="true">
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="string_value" id="(@1Q[HPSoEJnC.e_DyeQ~">
                                                                                                                                                                    <field name="value">&lt;/td&gt;&lt;td style='text-align: left'&gt;</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="variable" id="Ky);J$1!RENE/x}UCx@1o">
                                                                                                                                                                    <field name="name">i</field>
                                                                                                                                                                    <value name="value">
                                                                                                                                                                      <block type="expression" id="!Kj$sg?c}$N)jgSm1oH9">
                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="while_do_block" id="d,Dr9C@1H3JQR=g$D%yUd">
                                                                                                                                                                        <value name="while">
                                                                                                                                                                          <block type="expression" id="{AXH))VBorxC2`A=u,!=">
                                                                                                                                                                            <field name="value">i &lt; x % 10</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <statement name="do">
                                                                                                                                                                          <block type="partial_explanation" id="vp@1XX:Hj6%QPZRq5HF`d" inline="true">
                                                                                                                                                                            <value name="value">
                                                                                                                                                                              <block type="string_value" id="y2B::ENaKcB5/U70fZVb">
                                                                                                                                                                                <field name="value">&lt;img src="@1sprite.src(1_${typex[i]}.png)"/&gt;</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="if_then_block" id="Ne?_/z]mEIdh(UqGttoX">
                                                                                                                                                                                <value name="if">
                                                                                                                                                                                  <block type="expression" id="vDOyMV-a+mzYbI4D?7(t">
                                                                                                                                                                                    <field name="value">i % 3 == 2</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <statement name="then">
                                                                                                                                                                                  <block type="partial_explanation" id="{|j-9IL%){=K!s!{]{Z." inline="true">
                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                      <block type="string_value" id="4b8%H$6!,q1^~JncX$@1@2">
                                                                                                                                                                                        <field name="value">&lt;/br&gt;</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </statement>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="variable" id="mlO816t06XvE;rux,R$H">
                                                                                                                                                                                    <field name="name">i</field>
                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                      <block type="expression" id="#FA0uA+uO^:wlCGZ|9QA">
                                                                                                                                                                                        <field name="value">i+1</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="partial_explanation" id="ji,2$I^ANaWH#Z$n|frj" inline="true">
                                                                                                                                                                            <value name="value">
                                                                                                                                                                              <block type="string_value" id="1(EI@1L+AYf,`PL~`qrF[">
                                                                                                                                                                                <field name="value">&lt;/td&gt;&lt;td&gt;&lt;img  style='padding: 10px' src="@1sprite.src(plus.png)"/&gt;&lt;/td&gt;&lt;td&gt;</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="variable" id="xu-ug%`:x+d;skazZg/e">
                                                                                                                                                                                <field name="name">i</field>
                                                                                                                                                                                <value name="value">
                                                                                                                                                                                  <block type="expression" id="dfJ]p%2xS?dysIZm?bWT">
                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="while_do_block" id="-F{5-/3uGF(QCjE.e7Lx">
                                                                                                                                                                                    <value name="while">
                                                                                                                                                                                      <block type="expression" id="!`3x.IL!F0srn~6FgrZ5">
                                                                                                                                                                                        <field name="value">i &lt; Math.floor(y/10)</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <statement name="do">
                                                                                                                                                                                      <block type="partial_explanation" id="%+vT0Gq_l^.=BFN~]+6#" inline="true">
                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                          <block type="string_value" id="8si#`WI$#dtI;kKHG`90">
                                                                                                                                                                                            <field name="value">&lt;img src="@1sprite.src(10.png)"/&gt;</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="if_then_block" id="0T]C74++S?r_R5QuOv0z">
                                                                                                                                                                                            <value name="if">
                                                                                                                                                                                              <block type="expression" id="h,ur%%|6q#A3lyqrV/-@2">
                                                                                                                                                                                                <field name="value">i % 8 == 7</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <statement name="then">
                                                                                                                                                                                              <block type="partial_explanation" id="|iM};A+W.rmy?c[PXov/" inline="true">
                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                  <block type="string_value" id="AVSqYS+@2C.K-1mHv#4qo">
                                                                                                                                                                                                    <field name="value">&lt;/br&gt;</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </statement>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="variable" id="TxxUrmKy[r@2~z)[{3c=`">
                                                                                                                                                                                                <field name="name">i</field>
                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                  <block type="expression" id=",%,dGdyV@1G|zJq~_vY~l">
                                                                                                                                                                                                    <field name="value">i+1</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </statement>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="partial_explanation" id="k7t8#r|y/|-gA/5OFkHb" inline="true">
                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                          <block type="string_value" id="-EPglMN1G`rdChHr=5qP">
                                                                                                                                                                                            <field name="value">&lt;/td&gt;&lt;td style='text-align: left'&gt;</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="variable" id="h8~!S:DaZ7T$Q{tRQpg0">
                                                                                                                                                                                            <field name="name">i</field>
                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                              <block type="expression" id="vk$@2L/`cQlZyh#!B?%8+">
                                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="while_do_block" id="+tLU$%kCclK1K)^5W3t)">
                                                                                                                                                                                                <value name="while">
                                                                                                                                                                                                  <block type="expression" id="H,}H-#ZD{45)aGPlr[0@1">
                                                                                                                                                                                                    <field name="value">i &lt; y % 10</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <statement name="do">
                                                                                                                                                                                                  <block type="partial_explanation" id="X_{%xuauco|c:;!Yg.:@2" inline="true">
                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                      <block type="string_value" id="_y(n0WNeV[lt!H3;dsrI">
                                                                                                                                                                                                        <field name="value">&lt;img src="@1sprite.src(1_${typey[i]}.png)"/&gt;</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="if_then_block" id="n{1H{c7Hznsg1JeSt2KZ">
                                                                                                                                                                                                        <value name="if">
                                                                                                                                                                                                          <block type="expression" id="N:HXO@1TD:F~Tlt-ukP|P">
                                                                                                                                                                                                            <field name="value">i % 3 == 2</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <statement name="then">
                                                                                                                                                                                                          <block type="partial_explanation" id="Lv+;P7x_osv,8m[!UV8R" inline="true">
                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                              <block type="string_value" id="g5K/lF5Efedjlb~3CQMH">
                                                                                                                                                                                                                <field name="value">&lt;/br&gt;</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="variable" id="nEt[|tw3a39[[5|^n0qU">
                                                                                                                                                                                                            <field name="name">i</field>
                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                              <block type="expression" id="AxZq4U=3y7{C@2L7:h2p,">
                                                                                                                                                                                                                <field name="value">i+1</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </statement>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="partial_explanation" id="@1D]8.}0X~SaMs,L%f_JB" inline="true">
                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                      <block type="string_value" id="-HE~Tt#TZ._u.F(h_53N">
                                                                                                                                                                                                        <field name="value">&lt;/td&gt;&lt;td&gt;&lt;img style='padding: 10px' src="@1sprite.src(equal.png)"/&gt;&lt;/td&gt;&lt;td&gt;</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="variable" id="a$)AxM9}iOH|yjAym,Vh">
                                                                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                          <block type="expression" id="QT.v(7%Kpx~[rg%A^rqc">
                                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="while_do_block" id="w6=3%Cx#8k$xugS~xgqo">
                                                                                                                                                                                                            <value name="while">
                                                                                                                                                                                                              <block type="expression" id="Ja)]ue5-9I5zWD~2Xr@1?">
                                                                                                                                                                                                                <field name="value">i &lt; Math.floor((x+y)/10)</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                            <statement name="do">
                                                                                                                                                                                                              <block type="partial_explanation" id="7g6pQ%~Nxx}Okh0T[]B_" inline="true">
                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                  <block type="string_value" id="7hOC]5Z@1w+cC187SZd!a">
                                                                                                                                                                                                                    <field name="value">&lt;img src="@1sprite.src(10.png)"/&gt;</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                  <block type="if_then_block" id="s5ZPQ)L$$%#k,Z5D/+Y!">
                                                                                                                                                                                                                    <value name="if">
                                                                                                                                                                                                                      <block type="expression" id="hz{At~_jQ]/::=h9h5)F">
                                                                                                                                                                                                                        <field name="value">i % 8 == 7</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                    <statement name="then">
                                                                                                                                                                                                                      <block type="partial_explanation" id="1L+`Yd~eVWL{H|_KmAG?" inline="true">
                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                          <block type="string_value" id="Ih#KRqV(2^B!n-piOXbe">
                                                                                                                                                                                                                            <field name="value">&lt;/br&gt;</field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                      <block type="variable" id="!yU{m@2aBKn/%8yfcP9CZ">
                                                                                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                          <block type="expression" id="pSYqq}(wl]vvxv+EfF3P">
                                                                                                                                                                                                                            <field name="value">i+1</field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </next>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                            <next>
                                                                                                                                                                                                              <block type="partial_explanation" id="9l2y|3:@1Yr!0.o.?H47O" inline="true">
                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                  <block type="string_value" id="Pzl8jR@1S?;Mmfey9^eV@1">
                                                                                                                                                                                                                    <field name="value">&lt;/td&gt;&lt;td style='text-align: left'&gt;</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                  <block type="variable" id="|L6HA|E-^eznRi}ajKDp">
                                                                                                                                                                                                                    <field name="name">i</field>
                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                      <block type="expression" id="IY8_+p669ec2}X;pD#M(">
                                                                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                      <block type="while_do_block" id="r7GfJZ+q9ZB$/,A4}m~E">
                                                                                                                                                                                                                        <value name="while">
                                                                                                                                                                                                                          <block type="expression" id="1Xi?6S9J8?:m/8QK=erR">
                                                                                                                                                                                                                            <field name="value">i &lt; x % 10</field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                        <statement name="do">
                                                                                                                                                                                                                          <block type="partial_explanation" id="HPAx#0C7k;Wb(%{EIp[=" inline="true">
                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                              <block type="string_value" id="C+)^pBbKpQmxO8Q/E!B_">
                                                                                                                                                                                                                                <field name="value">&lt;img src="@1sprite.src(1_${typex[i]}.png)"/&gt;</field>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                              <block type="if_then_block" id="or}7^oNR%7tmtz$YWgs@1">
                                                                                                                                                                                                                                <value name="if">
                                                                                                                                                                                                                                  <block type="expression" id="pm36]Yv0,:0[m1tAwi!w">
                                                                                                                                                                                                                                    <field name="value">i % 3 == 2</field>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                <statement name="then">
                                                                                                                                                                                                                                  <block type="partial_explanation" id="}j}g,nulG~P%Kc@2^Ae)9" inline="true">
                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                      <block type="string_value" id="Sktd=oCZD`nk+:Bp[!Sl">
                                                                                                                                                                                                                                        <field name="value">&lt;/br&gt;</field>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                  <block type="variable" id="##Uu-8wivc4JsQ;KRbQN">
                                                                                                                                                                                                                                    <field name="name">i</field>
                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                      <block type="expression" id="}x7=x,y$!zU5].yOACBA">
                                                                                                                                                                                                                                        <field name="value">i+1</field>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                          <block type="while_do_block" id="PmtxsTZ3}Qr3BEHpEf}V">
                                                                                                                                                                                                                            <value name="while">
                                                                                                                                                                                                                              <block type="expression" id="6g#XYYEbF+S!Iud3$xyL">
                                                                                                                                                                                                                                <field name="value">i &lt; x % 10 + y % 10</field>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                            <statement name="do">
                                                                                                                                                                                                                              <block type="partial_explanation" id="))mQo!ba_fhn?|o:$n)[" inline="true">
                                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                                  <block type="string_value" id=".|_e~qYAM7#p-#u.k)1W">
                                                                                                                                                                                                                                    <field name="value">&lt;img src="@1sprite.src(1_${typey[i - x % 10]}.png)"/&gt;</field>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                  <block type="if_then_block" id="d+Uw[5Q,QFfK,]CH;nRP">
                                                                                                                                                                                                                                    <value name="if">
                                                                                                                                                                                                                                      <block type="expression" id="xZ;e+-VpwT#7v=F?5{n-">
                                                                                                                                                                                                                                        <field name="value">i % 3 == 2</field>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                    <statement name="then">
                                                                                                                                                                                                                                      <block type="partial_explanation" id="_V$1uB+#WJ[jWm?w$5?G" inline="true">
                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                          <block type="string_value" id="2PDQd^8?V`FR@1-URj;eM">
                                                                                                                                                                                                                                            <field name="value">&lt;/br&gt;</field>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                      <block type="variable" id="}|=Uq6b5w:C~psV%G-O)">
                                                                                                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                          <block type="expression" id="+Ukb}Ph{c+{n:I[6F7Z$">
                                                                                                                                                                                                                                            <field name="value">i+1</field>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                              <block type="partial_explanation" id="P)HnqD.~q-Q#mS/]SlqM">
                                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                                  <block type="string_value" id="G%duyw1i/P`Y#B.eUUFi">
                                                                                                                                                                                                                                    <field name="value">&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;

&lt;/br&gt;
&lt;span style='font-size: 42px; background: rgb(150, 200, 250, 0.3)'&gt;${x} + ${y} = ${x+y}&lt;/span&gt;
&lt;/center&gt;
                                                                                                                                                                                                                                    </field>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </next>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </next>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
</xml>
END_XML]] */