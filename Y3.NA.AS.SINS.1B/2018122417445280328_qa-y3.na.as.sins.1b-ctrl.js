
module.exports = [
  {
    "#type": "question",
    "name": "Y3.NA.AS.SINS.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y3.NA.AS.SINS.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "range < 0"
        },
        "then": [
          {
            "#type": "variable",
            "name": "x1",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "2"
              },
              "max": {
                "#type": "expression",
                "value": "19"
              }
            }
          },
          {
            "#type": "variable",
            "name": "x5",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "1"
              },
              "max": {
                "#type": "expression",
                "value": "19"
              }
            }
          },
          {
            "#type": "variable",
            "name": "x6",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "1"
              },
              "max": {
                "#type": "expression",
                "value": "x1 - 1"
              }
            }
          },
          {
            "#type": "variable",
            "name": "max",
            "value": {
              "#type": "expression",
              "value": "2"
            }
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "range < 4"
            },
            "then": [
              {
                "#type": "variable",
                "name": "x1",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "2"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "49"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "x5",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "1"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "49"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "x6",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "1"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "x1 - 1"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "max",
                "value": {
                  "#type": "expression",
                  "value": "3"
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "x1",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "2"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "99"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "x5",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "1"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "99"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "x6",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "1"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "x1 - 1"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "max",
                "value": {
                  "#type": "expression",
                  "value": "4"
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "x2",
        "value": {
          "#type": "expression",
          "value": "x5"
        }
      },
      {
        "#type": "variable",
        "name": "x3",
        "value": {
          "#type": "expression",
          "value": "-x5"
        }
      },
      {
        "#type": "variable",
        "name": "sign",
        "value": {
          "#type": "random_one",
          "items": {
            "#type": "expression",
            "value": "[1, -1]"
          }
        }
      },
      {
        "#type": "variable",
        "name": "x4",
        "value": {
          "#type": "expression",
          "value": "sign*x6"
        }
      },
      {
        "#type": "variable",
        "name": "type",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "2"
          }
        }
      },
      {
        "#type": "variable",
        "name": "str",
        "value": {
          "#type": "expression",
          "value": "x1 + ' + ' + x2 + ' - ' + Math.abs(x3) + (x4 < 0 ? ' - ':' + ') + Math.abs(x4) + ' = x'"
        }
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "20"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "Solve the following inverse number sentence."
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "36"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "list_shape",
        "#props": [
          {
            "#type": "prop_list_direction",
            "#prop": "",
            "dir": "horizontal"
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "200"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_spacing",
            "#prop": "",
            "spacing": {
              "#type": "expression",
              "value": "2"
            }
          }
        ],
        "items": [
          {
            "#type": "json",
            "#props": [
              {
                "#type": "prop_list_align",
                "#prop": "",
                "align": "middle"
              },
              {
                "#type": "prop_list_item_source",
                "#prop": "",
                "source": {
                  "#type": "expression",
                  "value": "str"
                }
              }
            ],
            "template": {
              "#callback": "$item",
              "variable": "$item",
              "body": [
                {
                  "#type": "if_then_else_block",
                  "if": {
                    "#type": "expression",
                    "value": "$item.data == '+'"
                  },
                  "then": [
                    {
                      "#type": "image_shape",
                      "#props": [
                        {
                          "#type": "prop_image_key",
                          "#prop": "",
                          "key": "plus.png"
                        },
                        {
                          "#type": "prop_image_src",
                          "#prop": "",
                          "src": "${image_path}plus.png"
                        }
                      ]
                    }
                  ],
                  "else": [
                    {
                      "#type": "if_then_else_block",
                      "if": {
                        "#type": "expression",
                        "value": "$item.data == '-'"
                      },
                      "then": [
                        {
                          "#type": "image_shape",
                          "#props": [
                            {
                              "#type": "prop_image_key",
                              "#prop": "",
                              "key": "sub.png"
                            },
                            {
                              "#type": "prop_image_src",
                              "#prop": "",
                              "src": "${image_path}sub.png"
                            }
                          ]
                        }
                      ],
                      "else": [
                        {
                          "#type": "if_then_else_block",
                          "if": {
                            "#type": "expression",
                            "value": "$item.data == '='"
                          },
                          "then": [
                            {
                              "#type": "image_shape",
                              "#props": [
                                {
                                  "#type": "prop_image_key",
                                  "#prop": "",
                                  "key": "equal.png"
                                },
                                {
                                  "#type": "prop_image_src",
                                  "#prop": "",
                                  "src": "${image_path}equal.png"
                                }
                              ]
                            }
                          ],
                          "else": [
                            {
                              "#type": "if_then_else_block",
                              "if": {
                                "#type": "expression",
                                "value": "$item.data == ' '"
                              },
                              "then": [
                                {
                                  "#type": "text_shape",
                                  "#props": [
                                    {
                                      "#type": "prop_text_contents",
                                      "#prop": "",
                                      "contents": "${' '}"
                                    },
                                    {
                                      "#prop": "",
                                      "style": {
                                        "#type": "json",
                                        "base": "text",
                                        "#props": [
                                          {
                                            "#type": "prop_text_style_font_size",
                                            "#prop": "",
                                            "fontSize": {
                                              "#type": "expression",
                                              "value": "36"
                                            }
                                          },
                                          {
                                            "#type": "prop_text_style_fill",
                                            "#prop": "",
                                            "fill": "black"
                                          },
                                          {
                                            "#type": "prop_text_style_stroke",
                                            "#prop": "",
                                            "stroke": "white"
                                          },
                                          {
                                            "#type": "prop_text_style_stroke_thickness",
                                            "#prop": "",
                                            "strokeThickness": {
                                              "#type": "expression",
                                              "value": "2"
                                            }
                                          }
                                        ]
                                      }
                                    }
                                  ]
                                }
                              ],
                              "else": [
                                {
                                  "#type": "if_then_else_block",
                                  "if": {
                                    "#type": "expression",
                                    "value": "$item.data == 'x'"
                                  },
                                  "then": [
                                    {
                                      "#type": "if_then_else_block",
                                      "if": {
                                        "#type": "expression",
                                        "value": "type == 1"
                                      },
                                      "then": [
                                        {
                                          "#type": "image_shape",
                                          "#props": [
                                            {
                                              "#type": "prop_size",
                                              "#prop": "",
                                              "width": {
                                                "#type": "expression",
                                                "value": "90"
                                              },
                                              "height": {
                                                "#type": "expression",
                                                "value": "70"
                                              }
                                            },
                                            {
                                              "#type": "prop_image_key",
                                              "#prop": "",
                                              "key": "shape.png"
                                            },
                                            {
                                              "#type": "prop_image_src",
                                              "#prop": "",
                                              "src": "${image_path}shape.png"
                                            }
                                          ]
                                        },
                                        {
                                          "#type": "choice_input_shape",
                                          "#props": [
                                            {
                                              "#type": "prop_value",
                                              "#prop": "",
                                              "value": {
                                                "#type": "expression",
                                                "value": "x1+x4"
                                              }
                                            },
                                            {
                                              "#type": "prop_size",
                                              "#prop": "",
                                              "width": {
                                                "#type": "expression",
                                                "value": "90"
                                              },
                                              "height": {
                                                "#type": "expression",
                                                "value": "70"
                                              }
                                            },
                                            {
                                              "#type": "prop_input_keyboard",
                                              "#prop": "",
                                              "keyboard": "numbers1"
                                            },
                                            {
                                              "#type": "prop_input_max_length",
                                              "#prop": "",
                                              "maxLength": {
                                                "#type": "expression",
                                                "value": "(x1+x4).toString().length"
                                              }
                                            },
                                            {
                                              "#type": "prop_input_result_position",
                                              "#prop": "",
                                              "resultPosition": "bottom"
                                            },
                                            {
                                              "#type": "prop_tab_order",
                                              "#prop": "",
                                              "tabOrder": {
                                                "#type": "expression",
                                                "value": "0"
                                              }
                                            },
                                            {
                                              "#type": "prop_stroke",
                                              "#prop": "stroke"
                                            },
                                            {
                                              "#type": "prop_fill",
                                              "#prop": "fill"
                                            },
                                            {
                                              "#prop": "",
                                              "style": {
                                                "#type": "json",
                                                "base": "text",
                                                "#props": [
                                                  {
                                                    "#type": "prop_text_style_font_size",
                                                    "#prop": "",
                                                    "fontSize": {
                                                      "#type": "expression",
                                                      "value": "46"
                                                    }
                                                  },
                                                  {
                                                    "#type": "prop_text_style_fill",
                                                    "#prop": "",
                                                    "fill": "black"
                                                  },
                                                  {
                                                    "#type": "prop_text_style_stroke",
                                                    "#prop": "",
                                                    "stroke": "white"
                                                  },
                                                  {
                                                    "#type": "prop_text_style_stroke_thickness",
                                                    "#prop": "",
                                                    "strokeThickness": {
                                                      "#type": "expression",
                                                      "value": "2"
                                                    }
                                                  }
                                                ]
                                              }
                                            }
                                          ],
                                          "#init": "algorithmic_input"
                                        }
                                      ],
                                      "else": [
                                        {
                                          "#type": "image_shape",
                                          "#props": [
                                            {
                                              "#type": "prop_image_key",
                                              "#prop": "",
                                              "key": "qes.png"
                                            },
                                            {
                                              "#type": "prop_image_src",
                                              "#prop": "",
                                              "src": "${image_path}qes.png"
                                            }
                                          ]
                                        }
                                      ]
                                    }
                                  ],
                                  "else": [
                                    {
                                      "#type": "image_shape",
                                      "#props": [
                                        {
                                          "#type": "prop_image_key",
                                          "#prop": "",
                                          "key": "${$item.data}.png"
                                        },
                                        {
                                          "#type": "prop_image_src",
                                          "#prop": "",
                                          "src": "${image_path}${$item.data}.png"
                                        }
                                      ]
                                    }
                                  ]
                                }
                              ]
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "statement",
        "value": "function check(x, A) {\n    for(var m=0; m<30; m++){\n        if(x == A[m]){\n            return 1;\n        }\n    }\n    return 0;\n}\n\nfunction check_add(x, A) {\n    for(var m=0; m<30; m++){\n        if(x + A[m] < 1){\n            return 1;\n        }\n    }\n    return 0;\n}\n\nif(max == 2) { var arr = [0, 1];}\nelse if(max == 3) {var arr = [0, 1, 2];}\nelse {var arr = [0, 1, 2, 3];}"
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 2"
        },
        "then": [
          {
            "#type": "variable",
            "name": "text",
            "value": {
              "#type": "expression",
              "value": "['a', 'b', 'c', 'd']"
            }
          },
          {
            "#type": "variable",
            "name": "answer",
            "value": {
              "#type": "expression",
              "value": "[]"
            }
          },
          {
            "#type": "variable",
            "name": "answer[0]",
            "value": {
              "#type": "expression",
              "value": "x1+x4"
            }
          },
          {
            "#type": "do_while_block",
            "do": [
              {
                "#type": "variable",
                "name": "rd",
                "value": {
                  "#type": "random_many",
                  "count": {
                    "#type": "expression",
                    "value": "max - 1"
                  },
                  "items": {
                    "#type": "func",
                    "name": "arrayOfNumber",
                    "args": [
                      {
                        "#type": "expression",
                        "value": "21"
                      },
                      {
                        "#type": "expression",
                        "value": "-10"
                      }
                    ]
                  }
                }
              }
            ],
            "while": {
              "#type": "expression",
              "value": "check(0, rd) == 1 || check_add(answer[0], rd) == 1"
            }
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "1"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < max"
            },
            "do": [
              {
                "#type": "variable",
                "name": "answer[i]",
                "value": {
                  "#type": "expression",
                  "value": "answer[0] + rd[i-1]"
                }
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "answer",
            "value": {
              "#type": "func",
              "name": "shuffle",
              "args": [
                {
                  "#type": "expression",
                  "value": "answer"
                }
              ]
            }
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "range > 3 && type == 2 ? 2 : 1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "range > 3 && type == 2 ? 2 : max"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "350"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "700"
                },
                "height": {
                  "#type": "expression",
                  "value": "range > 3 && type == 2 ? 200 : 100"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "arr"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "variable",
                    "name": "data",
                    "value": {
                      "#type": "expression",
                      "value": "$cell.data"
                    }
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "0"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "0"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "${text[$cell.data]}.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}${text[$cell.data]}.png"
                      }
                    ]
                  },
                  {
                    "#type": "grid_shape",
                    "#props": [
                      {
                        "#type": "prop_grid_dimension",
                        "#prop": "",
                        "rows": {
                          "#type": "expression",
                          "value": "1"
                        },
                        "cols": {
                          "#type": "expression",
                          "value": "1"
                        }
                      },
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX + 40"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "100"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "60"
                        }
                      },
                      {
                        "#type": "prop_anchor",
                        "#prop": "anchor",
                        "x": {
                          "#type": "expression",
                          "value": "0.5"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "0.5"
                        }
                      },
                      {
                        "#type": "prop_grid_cell_source",
                        "#prop": "cell.source",
                        "value": {
                          "#type": "expression",
                          "value": "1"
                        }
                      },
                      {
                        "#type": "prop_grid_random",
                        "#prop": "",
                        "random": false
                      },
                      {
                        "#type": "prop_grid_show_borders",
                        "#prop": "#showBorders",
                        "value": false
                      },
                      {
                        "#type": "prop_grid_cell_template",
                        "variable": "$cell",
                        "#prop": "cell.template",
                        "#callback": "$cell",
                        "body": [
                          {
                            "#type": "list_shape",
                            "#props": [
                              {
                                "#type": "prop_list_direction",
                                "#prop": "",
                                "dir": "horizontal"
                              },
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_anchor",
                                "#prop": "anchor",
                                "x": {
                                  "#type": "expression",
                                  "value": "0.5"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "0.5"
                                }
                              }
                            ],
                            "items": [
                              {
                                "#type": "json",
                                "#props": [
                                  {
                                    "#type": "prop_list_align",
                                    "#prop": "",
                                    "align": "middle"
                                  },
                                  {
                                    "#type": "prop_list_item_source",
                                    "#prop": "",
                                    "source": {
                                      "#type": "func",
                                      "name": "charactersOf",
                                      "args": [
                                        {
                                          "#type": "expression",
                                          "value": "answer[data]"
                                        }
                                      ]
                                    }
                                  }
                                ],
                                "template": {
                                  "#callback": "$item",
                                  "variable": "$item",
                                  "body": [
                                    {
                                      "#type": "image_shape",
                                      "#props": [
                                        {
                                          "#type": "prop_image_key",
                                          "#prop": "",
                                          "key": "${$item.data}.png"
                                        },
                                        {
                                          "#type": "prop_image_src",
                                          "#prop": "",
                                          "src": "${image_path}${$item.data}.png"
                                        },
                                        {
                                          "#type": "prop_scale",
                                          "#prop": "",
                                          "scale": {
                                            "#type": "expression",
                                            "value": "0.8"
                                          }
                                        }
                                      ]
                                    }
                                  ]
                                }
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  },
                  {
                    "#type": "choice_custom_shape",
                    "action": "click-one",
                    "value": {
                      "#type": "expression",
                      "value": "answer[$cell.data]"
                    },
                    "template": {
                      "#callback": "$choice",
                      "variable": "$choice",
                      "body": [
                        {
                          "#type": "image_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "$cell.centerX"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "$cell.centerY"
                              }
                            },
                            {
                              "#type": "prop_size",
                              "#prop": "",
                              "width": {
                                "#type": "expression",
                                "value": "0"
                              },
                              "height": {
                                "#type": "expression",
                                "value": "0"
                              }
                            },
                            {
                              "#type": "prop_image_key",
                              "#prop": "",
                              "key": "hide.png"
                            },
                            {
                              "#type": "prop_image_src",
                              "#prop": "",
                              "src": "${image_path}hide.png"
                            }
                          ]
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "func",
            "name": "addAnswers",
            "args": [
              {
                "#type": "expression",
                "value": "x1+x4"
              }
            ]
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "variable",
            "name": "load",
            "value": {
              "#type": "custom_image_list",
              "link": "${image_path}",
              "images": "qes|0|1|2|3|4|5|6|7|8|9"
            }
          }
        ]
      },
      {
        "#type": "statement",
        "value": "function str_img(a){\n    var str = '';\n    for(var i = 0; i< a.toString().length; i++){\n        str += '<img src=\"@sprite.src(' + a.toString()[i] + '.png)\"/>';\n    }\n    return str;\n}\n\nfunction str_imganswer(a){\n    var str = '';\n    for(var i = 0; i< a.toString().length; i++){\n        str += '<img ' + (a > 99 ? 'style=\"max-height: 40px\"' : '') + ' src=\"@sprite.src(' + a.toString()[i] + '.png)\"/>';\n    }\n    return str;\n}\n\nfunction back_pos(x, A){\n  for(var i = 0; i< A.length; i++){\n    if(x== A[i]){\n      return i;\n    }\n  }\n  return -1;\n}"
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 1: Look out for the pair of inverse numbers.</u></br></br>\n</br>\n<center>\n${str_img(x1)} <img src='@sprite.src(plus.png)'/> ${str_img(x2)} <img src='@sprite.src(sub.png)'/> ${str_img(Math.abs(x3))} <img src='@sprite.src(${x4 < 0 ? \"sub\":\"plus\"}.png)'/> ${str_img(Math.abs(x4))} <img src='@sprite.src(equal.png)'/> <img src='@sprite.src(qes.png)'/>\n  </br></br></br>\n\n<b>${x2}</b> and <b>${x3}</b> are a pair of <b>inverse numbers</b>.    </center>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 2: Now let's cancel out the pair of inverse numbers and solve the question.</u></br></br>\n</br>\n\n<style>\n  td{height: 100px }\n  .img {margin: 0 15px}\n  </style>\n<table>\n  <tr>\n  <td>${str_img(x1)}<img class='img' src='@sprite.src(plus.png)'/>${str_img(x2)}<img class='img' src='@sprite.src(sub.png)'/>${str_img(Math.abs(x3))}<img class='img' src='@sprite.src(${x4 < 0 ? \"sub\":\"plus\"}.png)'/>${str_img(Math.abs(x4))}</td>\n  <td><img class='img' src='@sprite.src(equal.png)'/> ${str_img(x1)}<img class='img' src='@sprite.src(${x4 < 0 ? \"sub\":\"plus\"}.png)'/>${str_img(Math.abs(x4))}</td>\n  </tr>\n<tr><td></td>\n<td><img class='img' src='@sprite.src(equal.png)'/>${str_img(x1+x4)}</td>\n</tr>\n</table>\n</br>\n\nAnswer: ${str_img(x1+x4)}"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          }
        ],
        "else": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 2: Now let's cancel out the pair of inverse numbers and solve the question.</u></br></br>\n\n<style>\n  td{height: 100px }\n  .img {margin: 0 15px}\n  </style>\n<table>\n  <tr>\n  <td>${str_img(x1)}<img class='img' src='@sprite.src(plus.png)'/>${str_img(x2)}<img class='img' src='@sprite.src(sub.png)'/>${str_img(Math.abs(x3))}<img class='img' src='@sprite.src(${x4 < 0 ? \"sub\":\"plus\"}.png)'/>${str_img(Math.abs(x4))}</td>\n  <td><img class='img' src='@sprite.src(equal.png)'/> ${str_img(x1)}<img class='img' src='@sprite.src(${x4 < 0 ? \"sub\":\"plus\"}.png)'/>${str_img(Math.abs(x4))}</td>\n  </tr>\n<tr><td></td>\n<td><img class='img' src='@sprite.src(equal.png)'/>${str_img(x1+x4)}</td>\n</tr>\n</table>\n</br>\n\n<table>\n  <tr>\n  <td style='text-align: center'>Answer:</td><td style='width: 15px'></td>\n<td style='background-image: url(\"@sprite.src(${text[back_pos(x1+x4, answer)]}.png)\"); background-repeat: no-repeat; background-position: center; width: 202px; height: 68px;'><div style='display: inline-block; margin-left: 100px'>${str_imganswer(x1+x4)}</div></td>\n</tr>\n</table>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          }
        ]
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="0" y="0">
    <field name="name">Y3.NA.AS.SINS.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y3.NA.AS.SINS.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="YepMQT@2uwp2rRPvmnRg1">
                                    <field name="name">range</field>
                                    <value name="value">
                                      <block type="expression" id="UxgQ^Y5]rb}vpw[D=l-!">
                                        <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="if_then_else_block" id="KCz?6FnaNc~0ayhBPB=o">
                                        <value name="if">
                                          <block type="expression" id="gS1#=`^m;{3%lf@1|I4kN">
                                            <field name="value">range &lt; 0</field>
                                          </block>
                                        </value>
                                        <statement name="then">
                                          <block type="variable" id="co-NL(^X1-IPCHSQz6{C">
                                            <field name="name">x1</field>
                                            <value name="value">
                                              <block type="random_number" id="La9qz1,b@2BL-~Q+Q%?4}">
                                                <value name="min">
                                                  <block type="expression" id="aT/bpHF?El`9Koh/6;s}">
                                                    <field name="value">2</field>
                                                  </block>
                                                </value>
                                                <value name="max">
                                                  <block type="expression" id="b[XI)x64ZnSlie@1xyNo/">
                                                    <field name="value">19</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id="/[aCx.[D6n=murZxx-1@1">
                                                <field name="name">x5</field>
                                                <value name="value">
                                                  <block type="random_number" id="1/oHkjbbFHQPKK?h-_#H">
                                                    <value name="min">
                                                      <block type="expression" id="(K6|?V(0/u_5s:mstb+$">
                                                        <field name="value">1</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="c1J9F)CsRf(1+?c7o@1Ih">
                                                        <field name="value">19</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="pN_~pw`eanj86c+yd4MD">
                                                    <field name="name">x6</field>
                                                    <value name="value">
                                                      <block type="random_number" id="^G`rRBd0@1ATv95pBBs}0">
                                                        <value name="min">
                                                          <block type="expression" id="H@2T,Jo$6s%`]oAkhpy_P">
                                                            <field name="value">1</field>
                                                          </block>
                                                        </value>
                                                        <value name="max">
                                                          <block type="expression" id="$vO[E~@1K=7kn]`P1E-02">
                                                            <field name="value">x1 - 1</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id="dQHNdq9;mOpcpdKCStSG">
                                                        <field name="name">max</field>
                                                        <value name="value">
                                                          <block type="expression" id="$Y#gu5-?j4@1_A|02/?:e">
                                                            <field name="value">2</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </statement>
                                        <statement name="else">
                                          <block type="if_then_else_block" id="n(xznq57N``2qn{:X.-N">
                                            <value name="if">
                                              <block type="expression" id="X}5zobrJ2Rn%qvBjbbv;">
                                                <field name="value">range &lt; 4</field>
                                              </block>
                                            </value>
                                            <statement name="then">
                                              <block type="variable" id="`QLWM`WVgeMA5,J!R,g.">
                                                <field name="name">x1</field>
                                                <value name="value">
                                                  <block type="random_number" id="Yl05!8R@1f3F|+nVpz+,Z">
                                                    <value name="min">
                                                      <block type="expression" id="Y@2F#cqVVx#`Cc%:/J`X$">
                                                        <field name="value">2</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="tKzgQGiW$JDZLmc_g]Ed">
                                                        <field name="value">49</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="2?OYl+v608e2yqDzK4gH">
                                                    <field name="name">x5</field>
                                                    <value name="value">
                                                      <block type="random_number" id="5qyFc7Za{UODSjuE6172">
                                                        <value name="min">
                                                          <block type="expression" id="/.ÒtZ]Uc?UoTM/9Pg2">
                                                            <field name="value">1</field>
                                                          </block>
                                                        </value>
                                                        <value name="max">
                                                          <block type="expression" id="YglCv(YS2ytyEL[rpQBa">
                                                            <field name="value">49</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id="P-8[Ng:Y]Qhnz@1~`VmM3">
                                                        <field name="name">x6</field>
                                                        <value name="value">
                                                          <block type="random_number" id="r0Fz1gb+$,?NgYaJ8HfX">
                                                            <value name="min">
                                                              <block type="expression" id="GljA)6_EF_sj@1!1c~ncl">
                                                                <field name="value">1</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="zheD7W@1W_mES+ds?mU8y">
                                                                <field name="value">x1 - 1</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="Ipdz@1svGT-5Yo]He{dyC">
                                                            <field name="name">max</field>
                                                            <value name="value">
                                                              <block type="expression" id="`0FE$j03q_C,.bc259@2w">
                                                                <field name="value">3</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </statement>
                                            <statement name="else">
                                              <block type="variable" id="+e,c+FKiJHc^@1W.70YLN">
                                                <field name="name">x1</field>
                                                <value name="value">
                                                  <block type="random_number" id="6zYRHbG|6Sn^1meHnq)I">
                                                    <value name="min">
                                                      <block type="expression" id="mZ]:UJ791=AP_6i/!@1c?">
                                                        <field name="value">2</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="!ptK#2e`$kU_]ZSlQT$t">
                                                        <field name="value">99</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="0+z@1U!-(!D;yXBMRl{1v">
                                                    <field name="name">x5</field>
                                                    <value name="value">
                                                      <block type="random_number" id="eU%h#4.n;T,!OuVO;lNf">
                                                        <value name="min">
                                                          <block type="expression" id="#Js1_hC%MR}H6hI|sbZ:">
                                                            <field name="value">1</field>
                                                          </block>
                                                        </value>
                                                        <value name="max">
                                                          <block type="expression" id="{Eq]czR|[:mw22F66J:@1">
                                                            <field name="value">99</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id="]Dq5$;pW1j_l7d_v/v/F">
                                                        <field name="name">x6</field>
                                                        <value name="value">
                                                          <block type="random_number" id="7CwH%5TVA^xZ+`tXIJS}">
                                                            <value name="min">
                                                              <block type="expression" id="vzee%[-;Zc,UHGf$T^_G">
                                                                <field name="value">1</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="z0|%-ZCg~Ji_lJoy?=~=">
                                                                <field name="value">x1 - 1</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="30B=sV3dYN:khl8ux}GM">
                                                            <field name="name">max</field>
                                                            <value name="value">
                                                              <block type="expression" id="@1@2}cI4En{cyJq{nR)vs{">
                                                                <field name="value">4</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </statement>
                                          </block>
                                        </statement>
                                        <next>
                                          <block type="variable" id="UVs;2gxTwx;%bw[5||6D">
                                            <field name="name">x2</field>
                                            <value name="value">
                                              <block type="expression" id="@2^OWuk/Pc)N^AsEBY;Ql">
                                                <field name="value">x5</field>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id="Rjlb1dRajZw$y-F^//.T">
                                                <field name="name">x3</field>
                                                <value name="value">
                                                  <block type="expression" id="{?bkU]{@2;;5)%VK:{Y@1X">
                                                    <field name="value">-x5</field>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="n5X(-/{PB=.kU#Q2@1-02">
                                                    <field name="name">sign</field>
                                                    <value name="value">
                                                      <block type="random_one" id="{Gza|qN_pZZw:fKZg2vo">
                                                        <value name="items">
                                                          <block type="expression" id="$V^{d9:63~BaeUrGt8+j">
                                                            <field name="value">[1, -1]</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id="+bh(@20u-tJCmEW`HK@2OH">
                                                        <field name="name">x4</field>
                                                        <value name="value">
                                                          <block type="expression" id="tDA.}22Tl@1DV(O#Cm@2EM">
                                                            <field name="value">sign@2x6</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="81c+GN2^9Nip)x[)_ZVt">
                                                            <field name="name">type</field>
                                                            <value name="value">
                                                              <block type="random_number" id="Wj}i-@1wdEoYJ-%gZQ1pi">
                                                                <value name="min">
                                                                  <block type="expression" id="wX(v^)rNbC7F_=rs_V}N">
                                                                    <field name="value">1</field>
                                                                  </block>
                                                                </value>
                                                                <value name="max">
                                                                  <block type="expression" id="@2(kCn-I02biJGREwnem`">
                                                                    <field name="value">2</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="variable" id="K+`Ud%h6I1+[;ZHA4y$}">
                                                                <field name="name">str</field>
                                                                <value name="value">
                                                                  <block type="expression" id="b`EOp{.mIi|Zo.7^tL)J">
                                                                    <field name="value">x1 + ' + ' + x2 + ' - ' + Math.abs(x3) + (x4 &lt; 0 ? ' - ':' + ') + Math.abs(x4) + ' = x'</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                                    <statement name="#props">
                                                                      <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                                        <field name="#prop"></field>
                                                                        <value name="x">
                                                                          <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                                            <field name="value">400</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                                            <field name="value">20</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_anchor" id="O.v@2mKn@1K#+tF;|}C}xI">
                                                                            <field name="#prop">anchor</field>
                                                                            <value name="x">
                                                                              <block type="expression" id="+3%GU[X3l%%X`Jv1QL7@2">
                                                                                <field name="value">0.5</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="y">
                                                                              <block type="expression" id="Kzp23HjHF7k]A-y1BJPm">
                                                                                <field name="value">0</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                                                <field name="#prop"></field>
                                                                                <value name="contents">
                                                                                  <block type="string_value" id="#6btV9p,PGV}wMx.Uï¿½">
                                                                                    <field name="value">Solve the following inverse number sentence.</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                                    <field name="base">text</field>
                                                                                    <statement name="#props">
                                                                                      <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="fontSize">
                                                                                          <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                                            <field name="value">36</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="fill">
                                                                                              <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                                                <field name="value">black</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="stroke">
                                                                                                  <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                                    <field name="value">white</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="strokeThickness">
                                                                                                      <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                                        <field name="value">2</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="list_shape" id="8z7:y/Y|~L4NW3:sXp[#">
                                                                        <statement name="#props">
                                                                          <block type="prop_list_direction" id="=e6vZf#,5(H/~8bvPE]G">
                                                                            <field name="#prop"></field>
                                                                            <field name="dir">horizontal</field>
                                                                            <next>
                                                                              <block type="prop_position" id="^JGX347#;i3J0=dW;%B=">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="NW%,Gt}dT@2_rCpW`]E~M">
                                                                                    <field name="value">400</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="?v%a+pAcpitIf|@2KMwO[">
                                                                                    <field name="value">200</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_anchor" id="Ko3y)fBfe|q|]mW1[BZh">
                                                                                    <field name="#prop">anchor</field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="#3(PEm2#tw9^pY?Sx_YR">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="Qwp$,z53l+cM!!@1`4-e-">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_spacing" id="@1ZsF1StI5$2do_9/ic6y">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="spacing">
                                                                                          <block type="expression" id="Q9c$B7/B$u$sioRBC}8w">
                                                                                            <field name="value">2</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <statement name="items">
                                                                          <block type="list_item_shape" id="HQCB_nKN:%YfwBfdCwHV">
                                                                            <statement name="#props">
                                                                              <block type="prop_list_align" id="U;2zFw@2_`e1jn4[[T?.-">
                                                                                <field name="#prop"></field>
                                                                                <field name="align">middle</field>
                                                                                <next>
                                                                                  <block type="prop_list_item_source" id="ju/O#7cDiSoEA#]ULP@1@1">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="source">
                                                                                      <block type="expression" id="~}Gaf}oH#%{unF[M@2Te@1">
                                                                                        <field name="value">str</field>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <statement name="template">
                                                                              <block type="if_then_else_block" id="??O^UcBKopiGiJ|Wk%;)">
                                                                                <value name="if">
                                                                                  <block type="expression" id="AIHDN@1X]wEOh9#LvPxpD">
                                                                                    <field name="value">$item.data == '+'</field>
                                                                                  </block>
                                                                                </value>
                                                                                <statement name="then">
                                                                                  <block type="image_shape" id="(n`gxUEi[^eE]2+A@2cyR">
                                                                                    <statement name="#props">
                                                                                      <block type="prop_image_key" id="O^]/^/OmmZR1$9w2~_e@1">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="key">
                                                                                          <block type="string_value" id="X%kHZBc,x=@2A0AuPKFH~">
                                                                                            <field name="value">plus.png</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_image_src" id="7B/xgcs3RReqd@2q;bcnh">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="src">
                                                                                              <block type="string_value" id="0QX9fE2z7DQ7cx,_Rola">
                                                                                                <field name="value">${image_path}plus.png</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                  </block>
                                                                                </statement>
                                                                                <statement name="else">
                                                                                  <block type="if_then_else_block" id="P~tRhwa)3#@2}#$wLwy?=">
                                                                                    <value name="if">
                                                                                      <block type="expression" id="SdQaP}]![9#ZV1@2}?BQO">
                                                                                        <field name="value">$item.data == '-'</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <statement name="then">
                                                                                      <block type="image_shape" id="66E_e+1U8e!@1@2`G79`}J">
                                                                                        <statement name="#props">
                                                                                          <block type="prop_image_key" id="E?R;U#4@2!=@2lUpyM@1nEo">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="key">
                                                                                              <block type="string_value" id="PANKq:`7W]_z:7/4ro+%">
                                                                                                <field name="value">sub.png</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_image_src" id="]p?RA[/VdGJMiz^IzVH2">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="src">
                                                                                                  <block type="string_value" id="(Ij5[@1h/z?UCGv|s[3MO">
                                                                                                    <field name="value">${image_path}sub.png</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <statement name="else">
                                                                                      <block type="if_then_else_block" id="e2#/@1s9Nb0wO1Tg:y!yY">
                                                                                        <value name="if">
                                                                                          <block type="expression" id="Wl$RWGa%Zesd6GozS5lG">
                                                                                            <field name="value">$item.data == '='</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <statement name="then">
                                                                                          <block type="image_shape" id="CF78zt9Uw$n{FCnJB4W|">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_image_key" id="h0mT[51~-6vuQp]a6D-n">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="key">
                                                                                                  <block type="string_value" id="}NVM?Ii^@1cyrN5tK@2p7/">
                                                                                                    <field name="value">equal.png</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_image_src" id="Ap)q^CPy0YAI{(pnKBy.">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="src">
                                                                                                      <block type="string_value" id="px@1%vLs=BNROdRDQRDKV">
                                                                                                        <field name="value">${image_path}equal.png</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <statement name="else">
                                                                                          <block type="if_then_else_block" id="|aPcio3q@1Rz%r5cKZTe^">
                                                                                            <value name="if">
                                                                                              <block type="expression" id="#Zt-;XqeEK;T#1cuK4wa">
                                                                                                <field name="value">$item.data == ' '</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <statement name="then">
                                                                                              <block type="text_shape" id="guMuI?lQ=6/[Miqk?=dt">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_text_contents" id="Tf+7YyPQs=@1n:|pg_GRN">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="contents">
                                                                                                      <block type="string_value" id="#6btV9p,PGV}wMx.U�">
                                                                                                        <field name="value">${' '}</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style" id="m!4.TZ}x|,p[K7v;EIU(">
                                                                                                        <field name="base">text</field>
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_text_style_font_size" id="RHK@1eGDF2W#jcPFJa@13,">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="fontSize">
                                                                                                              <block type="expression" id="sJ%W}NuF})+j4REz`XJ/">
                                                                                                                <field name="value">36</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_fill" id=")8?)#|vI^Aa1nq)TgYc}">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="fill">
                                                                                                                  <block type="string_value" id="7HyQC@1OvyTny-@1=5gHJ~">
                                                                                                                    <field name="value">black</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_stroke" id="K.jtEz%.QZ3TZgAw4fyn">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="stroke">
                                                                                                                      <block type="string_value" id="|pbO#07xVd{b^nQ(b6Vs">
                                                                                                                        <field name="value">white</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_stroke_thickness" id="IdMR5Q/LuA;cD/.hL`^5">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="strokeThickness">
                                                                                                                          <block type="expression" id="G@1X{GHCo]OJxJhxJ%4;O">
                                                                                                                            <field name="value">2</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <statement name="else">
                                                                                              <block type="if_then_else_block" id="+mtL=EHmaP=E4q|]nSNX">
                                                                                                <value name="if">
                                                                                                  <block type="expression" id="xKUw[%iBJ@2J3:N0DZM0}">
                                                                                                    <field name="value">$item.data == 'x'</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <statement name="then">
                                                                                                  <block type="if_then_else_block" id="u9HP_H($_NgpY]l]zCTN">
                                                                                                    <value name="if">
                                                                                                      <block type="expression" id="h?B[c)!PtT2m1J}nUJt~">
                                                                                                        <field name="value">type == 1</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <statement name="then">
                                                                                                      <block type="image_shape" id="f@1oISOMyN[.`i2`voQvT">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_size" id="P/bKJa)^Z0g8fdYd=MAl">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="width">
                                                                                                              <block type="expression" id="woB(r(ImlYq8GfCUh|(}">
                                                                                                                <field name="value">90</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="height">
                                                                                                              <block type="expression" id="ytg[]t:AEyn:CT!myuY=">
                                                                                                                <field name="value">70</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_image_key" id="=E,rk#%KLLE$Nf,1.N?}">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="key">
                                                                                                                  <block type="string_value" id="LaFVjyTe{$9uejUzgl@2{">
                                                                                                                    <field name="value">shape.png</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_image_src" id="þU@1E@2EADo9Ppzk`AGH">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="src">
                                                                                                                      <block type="string_value" id="uDWyExv9cn@2GqU@1oQlBA">
                                                                                                                        <field name="value">${image_path}shape.png</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="algorithmic_input_shape" id="[e}LcmqF@19.%T?]@1jB-Z">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_value" id="TN6clfe}:4H;@1k0w%+`S">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="@1Y0=VVDuz^gR8?]A.GgV">
                                                                                                                    <field name="value">x1+x4</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_size" id="~)S$^+Glx|;{-)@1m?MCt">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="width">
                                                                                                                      <block type="expression" id="wdRRw?lccA]3huvks^0:">
                                                                                                                        <field name="value">90</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="height">
                                                                                                                      <block type="expression" id="FlQ@1R-hpt5|W3YEsjl5h">
                                                                                                                        <field name="value">70</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_input_keyboard" id="pX[+1]_P03Uw?r6RhxOm">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <field name="keyboard">numbers1</field>
                                                                                                                        <next>
                                                                                                                          <block type="prop_input_max_length" id="e^196@2S|6ukB7~2Bxi9C">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="maxLength">
                                                                                                                              <block type="expression" id="/_E;-7``ONG+]nS_NxH+">
                                                                                                                                <field name="value">(x1+x4).toString().length</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_input_result_position" id="#LH4fkex}OZhG#[@2d;3P">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <field name="resultPosition">bottom</field>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_tab_order" id="s|F@1Izb{|@2lRY,asD!hu">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="tabOrder">
                                                                                                                                      <block type="expression" id="2[il+_dgVQKQm?SvtFDj">
                                                                                                                                        <field name="value">0</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_stroke" id="D`nCliS/s1emx9euC0}t">
                                                                                                                                        <field name="#prop">stroke</field>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_fill" id="Dz-AaH/!wx?BO[MH9dIz">
                                                                                                                                            <field name="#prop">fill</field>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_text_style" id="!1Qr3{IxD@1PMJ]G^LlcN">
                                                                                                                                                <field name="base">text</field>
                                                                                                                                                <statement name="#props">
                                                                                                                                                  <block type="prop_text_style_font_size" id="!(nlSpda9T+o/iXBj}YF">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="fontSize">
                                                                                                                                                      <block type="expression" id="FQTZZB_+$@1-hM?K.!NSO">
                                                                                                                                                        <field name="value">46</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_text_style_fill" id="w-4`M-zsemYrO[}g`_|q">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="fill">
                                                                                                                                                          <block type="string_value" id="G;VmNGg}u8OZR_#3M_Oe">
                                                                                                                                                            <field name="value">black</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_text_style_stroke" id="ahppz0%Y{eD@2C$!vyJ4m">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="stroke">
                                                                                                                                                              <block type="string_value" id="}KNcD$1%M)+@1a=7A%7,,">
                                                                                                                                                                <field name="value">white</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_text_style_stroke_thickness" id="TBWlBG{V@2q@2LOjXi7|@16">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="strokeThickness">
                                                                                                                                                                  <block type="expression" id="bs_awM@2W;X@1OG#fQ+@2no">
                                                                                                                                                                    <field name="value">2</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <statement name="else">
                                                                                                      <block type="image_shape" id="[QZ6SbQ1__ojbU1yo%%%">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_image_key" id="3Q6+X2=j}+oe7^E.V/0W">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="key">
                                                                                                              <block type="string_value" id="_z`Nb5LQV}O8]M~2!!@1J">
                                                                                                                <field name="value">qes.png</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_image_src" id="s=`w-/I+MNic0Ka~qw@2!">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="src">
                                                                                                                  <block type="string_value" id="S6nnmt]10@14BriM1[j41">
                                                                                                                    <field name="value">${image_path}qes.png</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <statement name="else">
                                                                                                  <block type="image_shape" id="x1sq1$Zhgv~U:!2(R3u8">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_image_key" id="f=Wc@1mb[6AW!Y(LE2PrX">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="key">
                                                                                                          <block type="string_value" id="tL-+1YmoXY+q-/hPofIo">
                                                                                                            <field name="value">${$item.data}.png</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_image_src" id="fF^Y7Qi4@1nZ3y^xJvi.B">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="src">
                                                                                                              <block type="string_value" id="1ugTr}^WQxCt~XBJ}mVy">
                                                                                                                <field name="value">${image_path}${$item.data}.png</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </statement>
                                                                                          </block>
                                                                                        </statement>
                                                                                      </block>
                                                                                    </statement>
                                                                                  </block>
                                                                                </statement>
                                                                              </block>
                                                                            </statement>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="statement" id="A?3;o3,6=y1QtY6NnDx%">
                                                                            <field name="value">function check(x, A) {
    for(var m=0; m&lt;30; m++){
        if(x == A[m]){
            return 1;
        }
    }
    return 0;
}

function check_add(x, A) {
    for(var m=0; m&lt;30; m++){
        if(x + A[m] &lt; 1){
            return 1;
        }
    }
    return 0;
}

if(max == 2) { var arr = [0, 1];}
else if(max == 3) {var arr = [0, 1, 2];}
else {var arr = [0, 1, 2, 3];}
                                                                            </field>
                                                                            <next>
                                                                              <block type="if_then_block" id="196m5w+1Dxd!z0C[vDn:">
                                                                                <value name="if">
                                                                                  <block type="expression" id="-[VN#`654d%c[Eb}xAl4">
                                                                                    <field name="value">type == 2</field>
                                                                                  </block>
                                                                                </value>
                                                                                <statement name="then">
                                                                                  <block type="variable" id="H1AWrdIF=B1pR0A.fuH#">
                                                                                    <field name="name">text</field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="6]rA+[%|3^r+c_x6;#:j">
                                                                                        <field name="value">['a', 'b', 'c', 'd']</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="variable" id="Lr/7@1#O-0%]C;Uu@1Rltv">
                                                                                        <field name="name">answer</field>
                                                                                        <value name="value">
                                                                                          <block type="expression" id="TIs`.X:HHMKemk@1wRV0!">
                                                                                            <field name="value">[]</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="variable" id="|~$QE+0ZeCg)6C`C(Pf#">
                                                                                            <field name="name">answer[0]</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="D_EDHD@2Z5ibgs{.-PTg/">
                                                                                                <field name="value">x1+x4</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="do_while_block" id="LAF(GTMk0!.~6QI`a@1#5">
                                                                                                <statement name="do">
                                                                                                  <block type="variable" id=")6JGd1?tl@1[OegxK$%;4">
                                                                                                    <field name="name">rd</field>
                                                                                                    <value name="value">
                                                                                                      <block type="random_many" id="SzgPu}g3wRd2;eCD`,C`">
                                                                                                        <value name="count">
                                                                                                          <block type="expression" id="[^?.,niL/b`d59fw9swp">
                                                                                                            <field name="value">max - 1</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="items">
                                                                                                          <block type="func_array_of_number" id="fBOy1jLIlghn@2zBHVw@2%" inline="true">
                                                                                                            <value name="items">
                                                                                                              <block type="expression" id="wk,%8vmzM!zh8%||`(TR">
                                                                                                                <field name="value">21</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="from">
                                                                                                              <block type="expression" id="DvL(2;X)1wn1P9-T/dc5">
                                                                                                                <field name="value">-10</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <value name="while">
                                                                                                  <block type="expression" id="ZNJq~(S:x6l4xAn5[tN@1">
                                                                                                    <field name="value">check(0, rd) == 1 || check_add(answer[0], rd) == 1</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="variable" id="3bS#)5Mm2d.@2xmtmS/[.">
                                                                                                    <field name="name">i</field>
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="kWL}Ci/}t(LqtC;;r}LS">
                                                                                                        <field name="value">1</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="while_do_block" id="i_{qbj81+a[$lWcDp(F|">
                                                                                                        <value name="while">
                                                                                                          <block type="expression" id=";5$zKh#^1y-?@2+27Kt@1R">
                                                                                                            <field name="value">i &lt; max</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <statement name="do">
                                                                                                          <block type="variable" id="L8o]vV5LF;8ttP]ip,d-">
                                                                                                            <field name="name">answer[i]</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="D(xrVbsI3bvHBU@17YA@2~">
                                                                                                                <field name="value">answer[0] + rd[i-1]</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="variable" id="-2Zc(bK_;!)Br?PPA5,C">
                                                                                                                <field name="name">i</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="m2be6RP5;#Va!LH2I}@2q">
                                                                                                                    <field name="value">i+1</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="variable" id="|istF2I8-$M6!rkJNdA9">
                                                                                                            <field name="name">answer</field>
                                                                                                            <value name="value">
                                                                                                              <block type="func_shuffle" id="Hcn/o%^C:p.!C77_dp0x" inline="true">
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="K;56X/lA+6jU/h9]jQQy">
                                                                                                                    <field name="value">answer</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="grid_shape" id="(S/t;Tm!z+k#t@2n.T@1eN">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_grid_dimension" id="LD+@2[oF@27og@1bNQ,|{yR">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="rows">
                                                                                                                      <block type="expression" id="$wd;h1O,=rHw[K$x;#oN">
                                                                                                                        <field name="value">range &gt; 3 &amp;&amp; type == 2 ? 2 : 1</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="cols">
                                                                                                                      <block type="expression" id="0Yz?Co2w/QjCEh;bcdBD">
                                                                                                                        <field name="value">range &gt; 3 &amp;&amp; type == 2 ? 2 : max</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_position" id="UdovYv/lK1$zg(HNn782">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="o|gkGZ/q?JxSLGu)s}oI">
                                                                                                                            <field name="value">400</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="7wL7-U59WocJXhN~7dqs">
                                                                                                                            <field name="value">350</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_size" id="IiscLKC8WF;[]F9QTX@2+">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="width">
                                                                                                                              <block type="expression" id="=FI,Ae4sWh_`3FYa|Lq(">
                                                                                                                                <field name="value">700</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="height">
                                                                                                                              <block type="expression" id="$]ZU6X8JaK:E6)(0:Hh7">
                                                                                                                                <field name="value">range &gt; 3 &amp;&amp; type == 2 ? 200 : 100</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_anchor" id="aBnF|K:Q8M|5ZUaT/,ay">
                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="kK!J)RP?Qic]6p,^@1BA/">
                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="aT(0Dah6`g+5w5M@1MnT8">
                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_grid_cell_source" id="c}Pq;9!l({/@2-N)t6r.k">
                                                                                                                                    <field name="#prop">cell.source</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="@28.V/ixzDrxTml4H`8$#">
                                                                                                                                        <field name="value">arr</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_grid_random" id="#3uUDdee:K#;?VEwaSpU">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <field name="random">FALSE</field>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_grid_show_borders" id="!:i__5C6RD!E}@2UC2^r+">
                                                                                                                                            <field name="#prop">#showBorders</field>
                                                                                                                                            <field name="value">FALSE</field>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_grid_cell_template" id="7^6]Kv:2yrZi`cooAW2@2">
                                                                                                                                                <field name="variable">$cell</field>
                                                                                                                                                <field name="#prop">cell.template</field>
                                                                                                                                                <field name="#callback">$cell</field>
                                                                                                                                                <statement name="body">
                                                                                                                                                  <block type="variable" id="K8S`zb.oB_USX/^z/UZ^">
                                                                                                                                                    <field name="name">data</field>
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="expression" id="l$QIkeD3JLSJ{VJTP4g$">
                                                                                                                                                        <field name="value">$cell.data</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="image_shape" id=",Cwu9eOCMq_tQqMoHo2?">
                                                                                                                                                        <statement name="#props">
                                                                                                                                                          <block type="prop_position" id="g.jvsFuiqz@2L#T^y|v`L">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="x">
                                                                                                                                                              <block type="expression" id="o]wo=AQ!t]Nc,VFItv^w">
                                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="y">
                                                                                                                                                              <block type="expression" id="{gjV4?|2+fk75OB+?p5y">
                                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_size" id="+a[On4_x?,]UUfl]ETTx">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="width">
                                                                                                                                                                  <block type="expression" id="b1Q?:#f75M9I{w!8hlF;">
                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <value name="height">
                                                                                                                                                                  <block type="expression" id="?P[aHof6AF4:YosMK@1gq">
                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_image_key" id=":r!ADSSLqGppPv:YplM$">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="key">
                                                                                                                                                                      <block type="string_value" id="SuIVI@28;b0H@2E]tSe7+x">
                                                                                                                                                                        <field name="value">${text[$cell.data]}.png</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_image_src" id="^{71s9)fiO,QWG:DLVOO">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="src">
                                                                                                                                                                          <block type="string_value" id="U@1]b;kLOtUB2x{)v~!YN">
                                                                                                                                                                            <field name="value">${image_path}${text[$cell.data]}.png</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="grid_shape" id="J@24oVk3iDOTJ%@1E)Id{$">
                                                                                                                                                            <statement name="#props">
                                                                                                                                                              <block type="prop_grid_dimension" id="%n)T|^;f88{6o|}G`:|L">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="rows">
                                                                                                                                                                  <block type="expression" id="BLl1=f5j,+j9i!D^vVYc">
                                                                                                                                                                    <field name="value">1</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <value name="cols">
                                                                                                                                                                  <block type="expression" id="4?~}R46uho}QX^7ITyP~">
                                                                                                                                                                    <field name="value">1</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_position" id="Td!uUmj038=9=-oQ8=9!">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="x">
                                                                                                                                                                      <block type="expression" id="v09@2SM_ehZl;V5_JsnuS">
                                                                                                                                                                        <field name="value">$cell.centerX + 40</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <value name="y">
                                                                                                                                                                      <block type="expression" id="FAynSnVg/ynXRLc6aQ.C">
                                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_size" id="xlImQ#y_c(~pk}JYI.nP">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="width">
                                                                                                                                                                          <block type="expression" id="aB@1~/@2Z$T]^-AWqGbP3v">
                                                                                                                                                                            <field name="value">100</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <value name="height">
                                                                                                                                                                          <block type="expression" id="3IdK~EM|Pm#vwE:qBJCb">
                                                                                                                                                                            <field name="value">60</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_anchor" id="(s-fmQpBZ[;S(_kf:pCL">
                                                                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                                                                            <value name="x">
                                                                                                                                                                              <block type="expression" id="M7nJJZD,Um@1|=9,@1g4`u">
                                                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="y">
                                                                                                                                                                              <block type="expression" id="|9g91q!KSFl1Zf+3aMEj">
                                                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_grid_cell_source" id="E_i;NOBanCcn9KI;w(ng">
                                                                                                                                                                                <field name="#prop">cell.source</field>
                                                                                                                                                                                <value name="value">
                                                                                                                                                                                  <block type="expression" id="@1S@2Fd%0m)M+:o#mbkWtA">
                                                                                                                                                                                    <field name="value">1</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_grid_random" id="O).fx(%gB}$X9zT3A;Q;">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <field name="random">FALSE</field>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_grid_show_borders" id="3BqYB%$d7B{e!md5/Jvh">
                                                                                                                                                                                        <field name="#prop">#showBorders</field>
                                                                                                                                                                                        <field name="value">FALSE</field>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_grid_cell_template" id="`@1p1.Q/l/9b+$4xwJ)if">
                                                                                                                                                                                            <field name="variable">$cell</field>
                                                                                                                                                                                            <field name="#prop">cell.template</field>
                                                                                                                                                                                            <field name="#callback">$cell</field>
                                                                                                                                                                                            <statement name="body">
                                                                                                                                                                                              <block type="list_shape" id="l`Vh0~N+}S,O-9Xx_3|Y">
                                                                                                                                                                                                <statement name="#props">
                                                                                                                                                                                                  <block type="prop_list_direction" id="4}V1PGRo:Y1/#)nB~3x,">
                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                    <field name="dir">horizontal</field>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="prop_position" id="i{qEm?IY,c({BCJqSH%{">
                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                        <value name="x">
                                                                                                                                                                                                          <block type="expression" id="sW+DOjiRp@2Vf(w1vEOx`">
                                                                                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <value name="y">
                                                                                                                                                                                                          <block type="expression" id="SER0JHDZ|K`hey$fSH6s">
                                                                                                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="prop_anchor" id="m;pCBiJ94(@28?E[A^m=[">
                                                                                                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                                                                                                            <value name="x">
                                                                                                                                                                                                              <block type="expression" id="FqvM-o?KAYk_$oTH)b6r">
                                                                                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                            <value name="y">
                                                                                                                                                                                                              <block type="expression" id="ATA)g{b_?D}R.cOSiB3H">
                                                                                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </statement>
                                                                                                                                                                                                <statement name="items">
                                                                                                                                                                                                  <block type="list_item_shape" id="t/Ctub%[UAc(+P2s~6M{">
                                                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                                                      <block type="prop_list_align" id="Q%VO1N=S|@2_Benw]w;wM">
                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                        <field name="align">middle</field>
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="prop_list_item_source" id="W8J4]6!_[AK/V~6Z`JUl">
                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                            <value name="source">
                                                                                                                                                                                                              <block type="func_characters_of" id="S-Svk:Z-gnvCOJI?0~JK" inline="true">
                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                  <block type="expression" id="0gJwP2YJkUy].2rmRWe)">
                                                                                                                                                                                                                    <field name="value">answer[data]</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                    <statement name="template">
                                                                                                                                                                                                      <block type="image_shape" id="r@2oK#g7$Fm3;:Cgp@23de">
                                                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                                                          <block type="prop_image_key" id=")1Tn4laRl{teYpjpWvCq">
                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                            <value name="key">
                                                                                                                                                                                                              <block type="string_value" id="3vuTT29AFh$t8ImlsUU1">
                                                                                                                                                                                                                <field name="value">${$item.data}.png</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                            <next>
                                                                                                                                                                                                              <block type="prop_image_src" id="CcTyB@19AVor79JANFhbO">
                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                <value name="src">
                                                                                                                                                                                                                  <block type="string_value" id="ST)16:h6y4HsmD{4pj%e">
                                                                                                                                                                                                                    <field name="value">${image_path}${$item.data}.png</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                  <block type="prop_scale" id="S]zvj(]9p!.cP_6{jHo~">
                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                    <value name="scale">
                                                                                                                                                                                                                      <block type="expression" id="N`a5Ai+sWJ_69cGEV[+1">
                                                                                                                                                                                                                        <field name="value">0.8</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </next>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </next>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </statement>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </statement>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="choice_custom_shape" id="1)i6@2r_!F#Dver6S0;-N">
                                                                                                                                                                <field name="action">click-one</field>
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="expression" id="gP(z#(9bnPilU:7@2=qHq">
                                                                                                                                                                    <field name="value">answer[$cell.data]</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <statement name="template">
                                                                                                                                                                  <block type="image_shape" id="!Cm3ZR$)g$HD|_Y]7PT/">
                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                      <block type="prop_position" id="-RoMMO/F1OqdewWbO@1.}">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="x">
                                                                                                                                                                          <block type="expression" id="2zEx~u$[nNM7icl7rtfV">
                                                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <value name="y">
                                                                                                                                                                          <block type="expression" id="ZwMtw!L2oRe7{M#bChqs">
                                                                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_size" id="KC1N^f;[l6r]|Fx5BA~=">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="width">
                                                                                                                                                                              <block type="expression" id="FYYb|{?l]3=agv[4X{hx">
                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="height">
                                                                                                                                                                              <block type="expression" id="/V[xybitp+2mVw+#3Co@2">
                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_image_key" id="3xX_hRVyit@1I40s!OD7i">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="key">
                                                                                                                                                                                  <block type="string_value" id="~!.ETk|LhLATnEr7_S9h">
                                                                                                                                                                                    <field name="value">hide.png</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_image_src" id="0|@1L{B4Tgv:qPc~kY_ii">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="src">
                                                                                                                                                                                      <block type="string_value" id="VfM6eT(RhOl70__6!bO{">
                                                                                                                                                                                        <field name="value">${image_path}hide.png</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="func_add_answer" id="@2#xJ6A!?hI;b?xLT1@1HM" inline="true">
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="OqHY+@26p#Q7a2==zn;#{">
                                                                                                                        <field name="value">x1+x4</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="if_then_block" id="S80~mz_2!Q5_R50m]Yk#">
                                                                                    <value name="if">
                                                                                      <block type="expression" id="pvh?faNlF(Pp0fEc^5L:">
                                                                                        <field name="value">type == 1</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <statement name="then">
                                                                                      <block type="variable" id="k^?@1t#19Vc(d;q(?A6Kd">
                                                                                        <field name="name">load</field>
                                                                                        <value name="value">
                                                                                          <block type="custom_image_list" id="4dC@2.Pf?r~D[Ko8@1RH7t" inline="true">
                                                                                            <field name="link">${image_path}</field>
                                                                                            <field name="images">qes|0|1|2|3|4|5|6|7|8|9</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <next>
                                                                                      <block type="statement" id="kXCeNf3AKZgbK]Devk~-">
                                                                                        <field name="value">function str_img(a){
    var str = '';
    for(var i = 0; i&lt; a.toString().length; i++){
        str += '&lt;img src="@1sprite.src(' + a.toString()[i] + '.png)"/&gt;';
    }
    return str;
}

function str_imganswer(a){
    var str = '';
    for(var i = 0; i&lt; a.toString().length; i++){
        str += '&lt;img ' + (a &gt; 99 ? 'style="max-height: 40px"' : '') + ' src="@1sprite.src(' + a.toString()[i] + '.png)"/&gt;';
    }
    return str;
}

function back_pos(x, A){
  for(var i = 0; i&lt; A.length; i++){
    if(x== A[i]){
      return i;
    }
  }
  return -1;
}
                                                                                        </field>
                                                                                        <next>
                                                                                          <block type="partial_explanation" id="A#F/@2kS$2tHOJW~yFUAh" inline="true">
                                                                                            <value name="value">
                                                                                              <block type="string_value" id="=$fiywAm@1X0@2!@24ep(H+">
                                                                                                <field name="value">&lt;u&gt;Step 1: Look out for the pair of inverse numbers.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;
&lt;/br&gt;
&lt;center&gt;
${str_img(x1)} &lt;img src='@1sprite.src(plus.png)'/&gt; ${str_img(x2)} &lt;img src='@1sprite.src(sub.png)'/&gt; ${str_img(Math.abs(x3))} &lt;img src='@1sprite.src(${x4 &lt; 0 ? "sub":"plus"}.png)'/&gt; ${str_img(Math.abs(x4))} &lt;img src='@1sprite.src(equal.png)'/&gt; &lt;img src='@1sprite.src(qes.png)'/&gt;
  &lt;/br&gt;&lt;/br&gt;&lt;/br&gt;

&lt;b&gt;${x2}&lt;/b&gt; and &lt;b&gt;${x3}&lt;/b&gt; are a pair of &lt;b&gt;inverse numbers&lt;/b&gt;.    &lt;/center&gt;
                                                                                                </field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="end_partial_explanation" id="#IMBUG.i@2uS}$AAEIl?u">
                                                                                                <next>
                                                                                                  <block type="if_then_else_block" id="?:HxP(,Sy_!BnGf8wjB;">
                                                                                                    <value name="if">
                                                                                                      <block type="expression" id="@265D}SKc{A0[K=^+5$/|">
                                                                                                        <field name="value">type == 1</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <statement name="then">
                                                                                                      <block type="partial_explanation" id="04n_voen3sB!I10I7VPr" inline="true">
                                                                                                        <value name="value">
                                                                                                          <block type="string_value" id="M{gKaVg5t2(]VgTEY/GM">
                                                                                                            <field name="value">&lt;u&gt;Step 2: Now let's cancel out the pair of inverse numbers and solve the question.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;
&lt;/br&gt;

&lt;style&gt;
  td{height: 100px }
  .img {margin: 0 15px}
  &lt;/style&gt;
&lt;table&gt;
  &lt;tr&gt;
  &lt;td&gt;${str_img(x1)}&lt;img class='img' src='@1sprite.src(plus.png)'/&gt;${str_img(x2)}&lt;img class='img' src='@1sprite.src(sub.png)'/&gt;${str_img(Math.abs(x3))}&lt;img class='img' src='@1sprite.src(${x4 &lt; 0 ? "sub":"plus"}.png)'/&gt;${str_img(Math.abs(x4))}&lt;/td&gt;
  &lt;td&gt;&lt;img class='img' src='@1sprite.src(equal.png)'/&gt; ${str_img(x1)}&lt;img class='img' src='@1sprite.src(${x4 &lt; 0 ? "sub":"plus"}.png)'/&gt;${str_img(Math.abs(x4))}&lt;/td&gt;
  &lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;img class='img' src='@1sprite.src(equal.png)'/&gt;${str_img(x1+x4)}&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;/br&gt;

Answer: ${str_img(x1+x4)}
                                                                                                            </field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="end_partial_explanation" id="+pilDA](7F]4Yq,{@1p%1"></block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <statement name="else">
                                                                                                      <block type="partial_explanation" id=".;V},R8muzv8oWEHPD[9" inline="true">
                                                                                                        <value name="value">
                                                                                                          <block type="string_value" id="9Wz)61Z8a-IrG@2g/{}/2">
                                                                                                            <field name="value">&lt;u&gt;Step 2: Now let's cancel out the pair of inverse numbers and solve the question.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
  td{height: 100px }
  .img {margin: 0 15px}
  &lt;/style&gt;
&lt;table&gt;
  &lt;tr&gt;
  &lt;td&gt;${str_img(x1)}&lt;img class='img' src='@1sprite.src(plus.png)'/&gt;${str_img(x2)}&lt;img class='img' src='@1sprite.src(sub.png)'/&gt;${str_img(Math.abs(x3))}&lt;img class='img' src='@1sprite.src(${x4 &lt; 0 ? "sub":"plus"}.png)'/&gt;${str_img(Math.abs(x4))}&lt;/td&gt;
  &lt;td&gt;&lt;img class='img' src='@1sprite.src(equal.png)'/&gt; ${str_img(x1)}&lt;img class='img' src='@1sprite.src(${x4 &lt; 0 ? "sub":"plus"}.png)'/&gt;${str_img(Math.abs(x4))}&lt;/td&gt;
  &lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;&lt;/td&gt;
&lt;td&gt;&lt;img class='img' src='@1sprite.src(equal.png)'/&gt;${str_img(x1+x4)}&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;/br&gt;

&lt;table&gt;
  &lt;tr&gt;
  &lt;td style='text-align: center'&gt;Answer:&lt;/td&gt;&lt;td style='width: 15px'&gt;&lt;/td&gt;
&lt;td style='background-image: url("@1sprite.src(${text[back_pos(x1+x4, answer)]}.png)"); background-repeat: no-repeat; background-position: center; width: 202px; height: 68px;'&gt;&lt;div style='display: inline-block; margin-left: 100px'&gt;${str_imganswer(x1+x4)}&lt;/div&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
                                                                                                            </field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="end_partial_explanation" id="OLdh}IJp,r[];lS/xlW`"></block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
</xml>
END_XML]] */