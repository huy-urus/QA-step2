
module.exports = [
  {
    "#type": "question",
    "name": "Y4.MG.LE.M2D.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y4.MG.LE.M2D.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "variable",
        "name": "item",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "2"
          }
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "range < 0"
        },
        "then": [
          {
            "#type": "variable",
            "name": "a",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "1"
              },
              "max": {
                "#type": "expression",
                "value": "4"
              }
            }
          },
          {
            "#type": "variable",
            "name": "a1",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "1"
              },
              "max": {
                "#type": "expression",
                "value": "4"
              }
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "a1 == a"
            },
            "do": [
              {
                "#type": "variable",
                "name": "a1",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "1"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "4"
                  }
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "unit",
            "value": {
              "#type": "expression",
              "value": "['cm', 'm']"
            }
          },
          {
            "#type": "variable",
            "name": "c",
            "value": {
              "#type": "random_one",
              "items": {
                "#type": "expression",
                "value": "['rectangle', 'triangle']"
              }
            }
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "range < 4"
            },
            "then": [
              {
                "#type": "variable",
                "name": "a",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "3"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "12"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "a1",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "1"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "4"
                  }
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "a1 == a"
                },
                "do": [
                  {
                    "#type": "variable",
                    "name": "a1",
                    "value": {
                      "#type": "random_number",
                      "min": {
                        "#type": "expression",
                        "value": "1"
                      },
                      "max": {
                        "#type": "expression",
                        "value": "4"
                      }
                    }
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "unit",
                "value": {
                  "#type": "expression",
                  "value": "['mm', 'cm', 'm']"
                }
              },
              {
                "#type": "variable",
                "name": "c",
                "value": {
                  "#type": "random_one",
                  "items": {
                    "#type": "expression",
                    "value": "['rectangle', 'triangle', 'pentagon', 'parallelogram', 'octagon', 'square']"
                  }
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "a",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "9"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "20"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "a1",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "1"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "4"
                  }
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "a1 == a"
                },
                "do": [
                  {
                    "#type": "variable",
                    "name": "a1",
                    "value": {
                      "#type": "random_number",
                      "min": {
                        "#type": "expression",
                        "value": "1"
                      },
                      "max": {
                        "#type": "expression",
                        "value": "4"
                      }
                    }
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "unit",
                "value": {
                  "#type": "expression",
                  "value": "['mm', 'cm', 'm', 'km']"
                }
              },
              {
                "#type": "variable",
                "name": "c",
                "value": {
                  "#type": "random_one",
                  "items": {
                    "#type": "expression",
                    "value": "['rectangle', 'triangle', 'pentagon', 'parallelogram', 'octagon', 'square']"
                  }
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "b",
        "value": {
          "#type": "random_one",
          "items": {
            "#type": "expression",
            "value": "unit"
          }
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "c == 'parallelogram' || c == 'rectangle' || c == 'trapezoid'"
        },
        "then": [
          {
            "#type": "variable",
            "name": "show",
            "value": {
              "#type": "expression",
              "value": "1"
            }
          }
        ],
        "else": [
          {
            "#type": "variable",
            "name": "show",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "1"
              },
              "max": {
                "#type": "expression",
                "value": "2"
              }
            }
          }
        ]
      },
      {
        "#type": "statement",
        "value": "var ans;\n\nif(c == 'triangle'){ if(show == 2) ans = 3*a; else ans = 2*a1 + a;}\nelse if(c == 'square') {ans = 4*a;}\nelse if(c == 'trapezoid') {ans = 3*a1 + a;}\nelse if(c == 'rectangle') {ans = 2*a + 2*a1;}\nelse if(c == 'parallelogram') {ans = 2*a + 2*a1;}\nelse if(c == 'pentagon') {if(show == 2) ans = 5*a; else ans = 2*a1 + 3*a;}\nelse if(c == 'hexagon') {if(show == 2) ans = 6*a; else ans = 3*a1 + 3*a;}\nelse {if(show == 2) ans = 8*a; else ans = 3*a1 + 5*a;}"
      },
      {
        "#type": "variable",
        "name": "answer",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "2"
          }
        }
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "40"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "What is the perimeter of the ${c} blow?"
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "32"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "show == 1"
        },
        "then": [
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "c == 'rectangle'"
            },
            "then": [
              {
                "#type": "if_then_block",
                "if": {
                  "#type": "expression",
                  "value": "a < a1"
                },
                "then": [
                  {
                    "#type": "variable",
                    "name": "temp",
                    "value": {
                      "#type": "expression",
                      "value": "a"
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "a",
                    "value": {
                      "#type": "expression",
                      "value": "a1"
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "a1",
                    "value": {
                      "#type": "expression",
                      "value": "temp"
                    }
                  }
                ]
              },
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "0"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "2rectangle${item}.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}2rectangle${item}.png"
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "300"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "150"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "320"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a1} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "80"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a1} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "c == 'triangle'"
            },
            "then": [
              {
                "#type": "if_then_block",
                "if": {
                  "#type": "expression",
                  "value": "a < a1"
                },
                "then": [
                  {
                    "#type": "variable",
                    "name": "temp",
                    "value": {
                      "#type": "expression",
                      "value": "a"
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "a",
                    "value": {
                      "#type": "expression",
                      "value": "a1"
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "a1",
                    "value": {
                      "#type": "expression",
                      "value": "temp"
                    }
                  }
                ]
              },
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "0"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "2triangle${item}.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}2triangle${item}.png"
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "315"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "300"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a1} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "80"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a1} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "c == 'square'"
            },
            "then": [
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "0"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "2square${item}.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}2square${item}.png"
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "325"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "140"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "320"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "80"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "c == 'parallelogram'"
            },
            "then": [
              {
                "#type": "if_then_block",
                "if": {
                  "#type": "expression",
                  "value": "a < a1"
                },
                "then": [
                  {
                    "#type": "variable",
                    "name": "temp",
                    "value": {
                      "#type": "expression",
                      "value": "a"
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "a",
                    "value": {
                      "#type": "expression",
                      "value": "a1"
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "a1",
                    "value": {
                      "#type": "expression",
                      "value": "temp"
                    }
                  }
                ]
              },
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "0"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "2parallelogram${item}.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}2parallelogram${item}.png"
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "300"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "150"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "320"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a1} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "80"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a1} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "c == 'trapezoid'"
            },
            "then": [
              {
                "#type": "if_then_block",
                "if": {
                  "#type": "expression",
                  "value": "a < a1"
                },
                "then": [
                  {
                    "#type": "variable",
                    "name": "temp",
                    "value": {
                      "#type": "expression",
                      "value": "a"
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "a",
                    "value": {
                      "#type": "expression",
                      "value": "a1"
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "a1",
                    "value": {
                      "#type": "expression",
                      "value": "temp"
                    }
                  }
                ]
              },
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "0"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "2trapezoid${item}.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}2trapezoid${item}.png"
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "300"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "150"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a1} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "320"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a1} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "80"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a1} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "c == 'pentagon'"
            },
            "then": [
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "0"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "2pentagon${item}.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}2pentagon${item}.png"
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "325"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "300"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "265"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "100"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "250"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "130"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "160"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a1} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "290"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "180"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a1} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "c == 'hexagon'"
            },
            "then": [
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "0"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "2hexagon${item}.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}2hexagon${item}.png"
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "325"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "125"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a1} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "300"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "280"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "100"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "280"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a1} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "100"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "180"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a1} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "300"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "180"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "c == 'octagon'"
            },
            "then": [
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "0"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "2octagon${item}.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}2octagon${item}.png"
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "325"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a1} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "125"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a1} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "300"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "290"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a1} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "100"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "290"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "100"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "170"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "300"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "170"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "80"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "325"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "show == 2"
        },
        "then": [
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "c == 'triangle'"
            },
            "then": [
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "0"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "1triangle${item}.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}1triangle${item}.png"
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "315"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "c == 'square'"
            },
            "then": [
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "0"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "1square${item}.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}1square${item}.png"
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "325"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "c == 'pentagon'"
            },
            "then": [
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "0"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "1pentagon${item}.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}1pentagon${item}.png"
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "325"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "c == 'hexagon'"
            },
            "then": [
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "0"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "1hexagon${item}.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}1hexagon${item}.png"
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "325"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "c == 'octagon'"
            },
            "then": [
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "225"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "0"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "1octagon${item}.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}1octagon${item}.png"
                  }
                ]
              },
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "200"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "325"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${a} ${b}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "32"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        },
                        {
                          "#type": "prop_text_style_stroke",
                          "#prop": "",
                          "stroke": "white"
                        },
                        {
                          "#type": "prop_text_style_stroke_thickness",
                          "#prop": "",
                          "strokeThickness": {
                            "#type": "expression",
                            "value": "2"
                          }
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "answer == 1"
        },
        "then": [
          {
            "#type": "list_shape",
            "#props": [
              {
                "#type": "prop_list_direction",
                "#prop": "",
                "dir": "horizontal"
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "550"
                },
                "y": {
                  "#type": "expression",
                  "value": "225"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              }
            ],
            "items": [
              {
                "#type": "json",
                "#props": [
                  {
                    "#type": "prop_list_align",
                    "#prop": "",
                    "align": "middle"
                  }
                ],
                "template": {
                  "#callback": "$item",
                  "variable": "$item",
                  "body": [
                    {
                      "#type": "image_shape",
                      "#props": [
                        {
                          "#type": "prop_size",
                          "#prop": "",
                          "width": {
                            "#type": "expression",
                            "value": "0"
                          },
                          "height": {
                            "#type": "expression",
                            "value": "0"
                          }
                        },
                        {
                          "#type": "prop_image_key",
                          "#prop": "",
                          "key": "shape2.png"
                        },
                        {
                          "#type": "prop_image_src",
                          "#prop": "",
                          "src": "${image_path}shape2.png"
                        }
                      ]
                    },
                    {
                      "#type": "choice_input_shape",
                      "#props": [
                        {
                          "#type": "prop_value",
                          "#prop": "",
                          "value": {
                            "#type": "expression",
                            "value": "ans"
                          }
                        },
                        {
                          "#type": "prop_position",
                          "#prop": "",
                          "x": {
                            "#type": "expression",
                            "value": "0"
                          },
                          "y": {
                            "#type": "expression",
                            "value": "0"
                          }
                        },
                        {
                          "#type": "prop_size",
                          "#prop": "",
                          "width": {
                            "#type": "expression",
                            "value": "130"
                          },
                          "height": {
                            "#type": "expression",
                            "value": "74"
                          }
                        },
                        {
                          "#type": "prop_input_keyboard",
                          "#prop": "",
                          "keyboard": "numbers1"
                        },
                        {
                          "#type": "prop_input_max_length",
                          "#prop": "",
                          "maxLength": {
                            "#type": "expression",
                            "value": "ans.toString().length"
                          }
                        },
                        {
                          "#type": "prop_input_result_position",
                          "#prop": "",
                          "resultPosition": "bottom"
                        },
                        {
                          "#type": "prop_tab_order",
                          "#prop": "",
                          "tabOrder": {
                            "#type": "expression",
                            "value": "0"
                          }
                        },
                        {
                          "#type": "prop_stroke",
                          "#prop": "stroke"
                        },
                        {
                          "#type": "prop_fill",
                          "#prop": "fill"
                        },
                        {
                          "#prop": "",
                          "style": {
                            "#type": "json",
                            "base": "text",
                            "#props": [
                              {
                                "#type": "prop_text_style_font_size",
                                "#prop": "",
                                "fontSize": {
                                  "#type": "expression",
                                  "value": "32"
                                }
                              },
                              {
                                "#type": "prop_text_style_fill",
                                "#prop": "",
                                "fill": "black"
                              },
                              {
                                "#type": "prop_text_style_stroke",
                                "#prop": "",
                                "stroke": "white"
                              },
                              {
                                "#type": "prop_text_style_stroke_thickness",
                                "#prop": "",
                                "strokeThickness": {
                                  "#type": "expression",
                                  "value": "2"
                                }
                              }
                            ]
                          }
                        }
                      ],
                      "#init": "algorithmic_input"
                    }
                  ]
                }
              },
              {
                "#type": "json",
                "#props": [
                  {
                    "#type": "prop_list_align",
                    "#prop": "",
                    "align": "middle"
                  }
                ],
                "template": {
                  "#callback": "$item",
                  "variable": "$item",
                  "body": [
                    {
                      "#type": "text_shape",
                      "#props": [
                        {
                          "#type": "prop_text_contents",
                          "#prop": "",
                          "contents": "${' ' + b}"
                        },
                        {
                          "#prop": "",
                          "style": {
                            "#type": "json",
                            "base": "text",
                            "#props": [
                              {
                                "#type": "prop_text_style_font_size",
                                "#prop": "",
                                "fontSize": {
                                  "#type": "expression",
                                  "value": "36"
                                }
                              },
                              {
                                "#type": "prop_text_style_fill",
                                "#prop": "",
                                "fill": "black"
                              },
                              {
                                "#type": "prop_text_style_stroke",
                                "#prop": "",
                                "stroke": "white"
                              },
                              {
                                "#type": "prop_text_style_stroke_thickness",
                                "#prop": "",
                                "strokeThickness": {
                                  "#type": "expression",
                                  "value": "2"
                                }
                              }
                            ]
                          }
                        }
                      ]
                    }
                  ]
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "answer == 2"
        },
        "then": [
          {
            "#type": "do_while_block",
            "do": [
              {
                "#type": "variable",
                "name": "rd",
                "value": {
                  "#type": "random_many",
                  "count": {
                    "#type": "expression",
                    "value": "3"
                  },
                  "items": {
                    "#type": "expression",
                    "value": "[-5, -4, -3, -2, -1, 1, 2, 3, 4, 5]"
                  }
                }
              }
            ],
            "while": {
              "#type": "expression",
              "value": "ans + rd[0] < 1 || ans + rd[1] < 1|| ans + rd[2] < 1"
            }
          },
          {
            "#type": "do_while_block",
            "do": [
              {
                "#type": "variable",
                "name": "u",
                "value": {
                  "#type": "random_one",
                  "items": {
                    "#type": "expression",
                    "value": "unit"
                  }
                }
              }
            ],
            "while": {
              "#type": "expression",
              "value": "u == b"
            }
          },
          {
            "#type": "variable",
            "name": "option",
            "value": {
              "#type": "expression",
              "value": "[]"
            }
          },
          {
            "#type": "variable",
            "name": "option[0]",
            "value": {
              "#type": "expression",
              "value": "ans + ' ' + b"
            }
          },
          {
            "#type": "variable",
            "name": "option[1]",
            "value": {
              "#type": "expression",
              "value": "ans + rd[2] +  ' ' + b"
            }
          },
          {
            "#type": "variable",
            "name": "option[2]",
            "value": {
              "#type": "expression",
              "value": "ans + rd[0] + ' ' + b"
            }
          },
          {
            "#type": "variable",
            "name": "option[3]",
            "value": {
              "#type": "expression",
              "value": "ans + rd[1] + ' ' + u"
            }
          },
          {
            "#type": "variable",
            "name": "option",
            "value": {
              "#type": "func",
              "name": "shuffle",
              "args": [
                {
                  "#type": "expression",
                  "value": "option"
                }
              ]
            }
          },
          {
            "#type": "variable",
            "name": "text",
            "value": {
              "#type": "expression",
              "value": "['A', 'B', 'C', 'D']"
            }
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "4"
                },
                "cols": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "550"
                },
                "y": {
                  "#type": "expression",
                  "value": "240"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "400"
                },
                "height": {
                  "#type": "expression",
                  "value": "320"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "[0, 1, 2, 3]"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "choice_custom_shape",
                    "action": "click-one",
                    "value": {
                      "#type": "expression",
                      "value": "option[$cell.data]"
                    },
                    "template": {
                      "#callback": "$choice",
                      "variable": "$choice",
                      "body": [
                        {
                          "#type": "image_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "$cell.centerX"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "$cell.centerY"
                              }
                            },
                            {
                              "#type": "prop_size",
                              "#prop": "",
                              "width": {
                                "#type": "expression",
                                "value": "0"
                              },
                              "height": {
                                "#type": "expression",
                                "value": "0"
                              }
                            },
                            {
                              "#type": "prop_image_key",
                              "#prop": "",
                              "key": "shape1.png"
                            },
                            {
                              "#type": "prop_image_src",
                              "#prop": "",
                              "src": "${image_path}shape1.png"
                            }
                          ]
                        },
                        {
                          "#type": "text_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "$cell.centerX"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "$cell.centerY+3"
                              }
                            },
                            {
                              "#type": "prop_anchor",
                              "#prop": "anchor",
                              "x": {
                                "#type": "expression",
                                "value": "0.5"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "0.5"
                              }
                            },
                            {
                              "#type": "prop_text_contents",
                              "#prop": "",
                              "contents": "${text[$cell.data] + '. ' + option[$cell.data]}"
                            },
                            {
                              "#prop": "",
                              "style": {
                                "#type": "json",
                                "base": "text",
                                "#props": [
                                  {
                                    "#type": "prop_text_style_font_size",
                                    "#prop": "",
                                    "fontSize": {
                                      "#type": "expression",
                                      "value": "36"
                                    }
                                  },
                                  {
                                    "#type": "prop_text_style_fill",
                                    "#prop": "",
                                    "fill": "black"
                                  },
                                  {
                                    "#type": "prop_text_style_stroke",
                                    "#prop": "",
                                    "stroke": "white"
                                  },
                                  {
                                    "#type": "prop_text_style_stroke_thickness",
                                    "#prop": "",
                                    "strokeThickness": {
                                      "#type": "expression",
                                      "value": "2"
                                    }
                                  }
                                ]
                              }
                            }
                          ]
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "func",
            "name": "addAnswers",
            "args": [
              {
                "#type": "expression",
                "value": "ans + ' ' + b"
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "load",
        "value": {
          "#type": "custom_image_list",
          "link": "${image_path}",
          "images": "ex${show}"
        }
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<center><u>Let's learn how to find the perimeter.</u>\n</br></br>\n  </center>\n<span style='background: rgb(250, 250, 250, 0.5)'>\nA perimeter is a path that surrounds a 2D shape.</br></br>\n<center>\n<img src='@sprite.src(ex${show}.png)'/>\n  </br></br></center>\nTo do so, you simply have to <b>add them together.</b></br></br>\n  \nAll sides added, so the perimeter is <b>${ans + ' ' + b}</b>.</span>"
        ]
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="3" y="106">
    <field name="name">Y4.MG.LE.M2D.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y4.MG.LE.M2D.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="input_param" id="sFtiD6!^-uj/^]emU~)L" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="E{=?x%IwfcL!c$C@1)[#E">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="QK20YX+48]axW1a$[8WD" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="7xBTzlh5$llX9$?X09_~">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="JB_:j8}O5Z|]74q:6Es;">
                                    <field name="name">range</field>
                                    <value name="value">
                                      <block type="expression" id="m-GQo2+0_$YJ@2+Z9Dzs:">
                                        <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="variable" id="%ch#)g`IjB:W{`;oUjg~">
                                        <field name="name">item</field>
                                        <value name="value">
                                          <block type="random_number" id="UA}:e1wF`8kJ{39qB@19S">
                                            <value name="min">
                                              <block type="expression" id="l$V|aHj(?TF8{~||#9i,">
                                                <field name="value">1</field>
                                              </block>
                                            </value>
                                            <value name="max">
                                              <block type="expression" id="c,GJYYAYM5:25KDKK|+#">
                                                <field name="value">2</field>
                                              </block>
                                            </value>
                                          </block>
                                        </value>
                                        <next>
                                          <block type="if_then_else_block" id="Niwp}uWYKqAe;sOAR5R=">
                                            <value name="if">
                                              <block type="expression" id="m3h~0M|(@1kE@2{GQ|F@1.Z">
                                                <field name="value">range &lt; 0</field>
                                              </block>
                                            </value>
                                            <statement name="then">
                                              <block type="variable" id="5g4J8i_$v)laQ(B0`H}j">
                                                <field name="name">a</field>
                                                <value name="value">
                                                  <block type="random_number" id="#s6vS1fM5?D]x?3B,r;o">
                                                    <value name="min">
                                                      <block type="expression" id="bN,Iuf@1=vMvDRCAIQ=ud">
                                                        <field name="value">1</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="9a-O^RCmKQJ}?X-KqqyQ">
                                                        <field name="value">4</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="1V2Y.SH.D7W(V=!])LsO">
                                                    <field name="name">a1</field>
                                                    <value name="value">
                                                      <block type="random_number" id="qIby2rwReX/U8C}dHaGE">
                                                        <value name="min">
                                                          <block type="expression" id="h;SUN0wKL.`C1!F?Z/E=">
                                                            <field name="value">1</field>
                                                          </block>
                                                        </value>
                                                        <value name="max">
                                                          <block type="expression" id="}wr0Po$Q[w`LM/925~W.">
                                                            <field name="value">4</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="while_do_block" id="ta;3s_o20p|iO%s|VGXX">
                                                        <value name="while">
                                                          <block type="expression" id="Kf=EahdzFKMRl6EBTFmn">
                                                            <field name="value">a1 == a</field>
                                                          </block>
                                                        </value>
                                                        <statement name="do">
                                                          <block type="variable" id="Uu)3JV#GWVSaAQX_8szt">
                                                            <field name="name">a1</field>
                                                            <value name="value">
                                                              <block type="random_number" id="Y@2@1lf1f@1pS}fwGPkW|vn">
                                                                <value name="min">
                                                                  <block type="expression" id="(b?s4|tZr$M3YZ@1QR~Hn">
                                                                    <field name="value">1</field>
                                                                  </block>
                                                                </value>
                                                                <value name="max">
                                                                  <block type="expression" id="r@2k65ce)Emo|Kr{FX|=!">
                                                                    <field name="value">4</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </statement>
                                                        <next>
                                                          <block type="variable" id="z1%LJ6dtTQbQ[_$Ue@21`">
                                                            <field name="name">unit</field>
                                                            <value name="value">
                                                              <block type="expression" id="yH/6@2Ie`jUDMo76N3!n]">
                                                                <field name="value">['cm', 'm']</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="variable" id="JAzZAJ?b-;AZ]?%Vv@2V!">
                                                                <field name="name">c</field>
                                                                <value name="value">
                                                                  <block type="random_one" id="|XvEYm!1us$0h4l_c5=~">
                                                                    <value name="items">
                                                                      <block type="expression" id="LKoGO}kX^y-KmF0^drVJ">
                                                                        <field name="value">['rectangle', 'triangle']</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </statement>
                                            <statement name="else">
                                              <block type="if_then_else_block" id="@2:=t}ZP68isD?4$V@1eyj">
                                                <value name="if">
                                                  <block type="expression" id="v`So}|R^;Tpj5%yAO@2()">
                                                    <field name="value">range &lt; 4</field>
                                                  </block>
                                                </value>
                                                <statement name="then">
                                                  <block type="variable" id="-}pKJ};Er-TqbKDVXH3h">
                                                    <field name="name">a</field>
                                                    <value name="value">
                                                      <block type="random_number" id="@1F?une@2`1_juu_g)C;oy">
                                                        <value name="min">
                                                          <block type="expression" id="^66fgVJKvz,cuO_W([[m">
                                                            <field name="value">3</field>
                                                          </block>
                                                        </value>
                                                        <value name="max">
                                                          <block type="expression" id="KU`Ft{DI:/3I+/e(A2ea">
                                                            <field name="value">12</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id="kUP`@2#Ox@2P1!-@2VS4c.l">
                                                        <field name="name">a1</field>
                                                        <value name="value">
                                                          <block type="random_number" id="@2D@2Z%_$Q;90dRbz.R|d1">
                                                            <value name="min">
                                                              <block type="expression" id="(,Wfvu;q|j,@1xOB,%bRw">
                                                                <field name="value">1</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id=",:M.ftm1Xl@1:I,0{v5H?">
                                                                <field name="value">4</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="while_do_block" id="[^h!lG2HJb;=jjP}iKKc">
                                                            <value name="while">
                                                              <block type="expression" id="@1PNEwGD,Dye|s~z3(@1y+">
                                                                <field name="value">a1 == a</field>
                                                              </block>
                                                            </value>
                                                            <statement name="do">
                                                              <block type="variable" id="iV@2u}1172N6)_lQsCo@1I">
                                                                <field name="name">a1</field>
                                                                <value name="value">
                                                                  <block type="random_number" id="vGC[2`u_[%uq]gF/`cVY">
                                                                    <value name="min">
                                                                      <block type="expression" id="`-Sc6gcae4GpyNOsU@1Cg">
                                                                        <field name="value">1</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="max">
                                                                      <block type="expression" id="BME{N:ENs%mzEol=fr8.">
                                                                        <field name="value">4</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </statement>
                                                            <next>
                                                              <block type="variable" id="HL(KJpg[5?=5|n$(5_q/">
                                                                <field name="name">unit</field>
                                                                <value name="value">
                                                                  <block type="expression" id="9c7k.f9P_Gb$yw[ST_.)">
                                                                    <field name="value">['mm', 'cm', 'm']</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="variable" id="ObDIF6nEDHl!$eV5}kEz">
                                                                    <field name="name">c</field>
                                                                    <value name="value">
                                                                      <block type="random_one" id="[dQSZg{`hC7?1M9?6AxT">
                                                                        <value name="items">
                                                                          <block type="expression" id="}AIXg4aof)n8PsJ)cxtt">
                                                                            <field name="value">['rectangle', 'triangle', 'pentagon', 'parallelogram', 'octagon', 'square']</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </statement>
                                                <statement name="else">
                                                  <block type="variable" id="B}[UEÜ,Nr@1^f6Z$III">
                                                    <field name="name">a</field>
                                                    <value name="value">
                                                      <block type="random_number" id="{$Z2Mqga:d2i%Y0-0Kmw">
                                                        <value name="min">
                                                          <block type="expression" id="AspC:7}ZAnQk(lGT;D_;">
                                                            <field name="value">9</field>
                                                          </block>
                                                        </value>
                                                        <value name="max">
                                                          <block type="expression" id="8n|W=wJ/OT`@2K/oJT7|0">
                                                            <field name="value">20</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id="[[kBT@1Gc|/KRu?/,/@2#1">
                                                        <field name="name">a1</field>
                                                        <value name="value">
                                                          <block type="random_number" id="FojBI42`x1NEe4ay#0!n">
                                                            <value name="min">
                                                              <block type="expression" id=";z`)(E:5@2T7TMydl4ui=">
                                                                <field name="value">1</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="uT]!2;9_@2DxH~u/ePRDQ">
                                                                <field name="value">4</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="while_do_block" id="vtujeS,!vAP_y,4tBE/_">
                                                            <value name="while">
                                                              <block type="expression" id="(@2[Qc.CCCe5d8+%a.@2Qt">
                                                                <field name="value">a1 == a</field>
                                                              </block>
                                                            </value>
                                                            <statement name="do">
                                                              <block type="variable" id="AI%`.1==zZj`EW!Xtxg}">
                                                                <field name="name">a1</field>
                                                                <value name="value">
                                                                  <block type="random_number" id="4I4UZgDTCs,S#CYl7VGn">
                                                                    <value name="min">
                                                                      <block type="expression" id="w=}}!bdqeuDBy:;-!iPn">
                                                                        <field name="value">1</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="max">
                                                                      <block type="expression" id="M^7pNdlr:]MVyrSn,=Rp">
                                                                        <field name="value">4</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </statement>
                                                            <next>
                                                              <block type="variable" id="cy;|;i)OK_HI=X.MRVG^">
                                                                <field name="name">unit</field>
                                                                <value name="value">
                                                                  <block type="expression" id="07gTLylsb]S{Y!RK(W3+">
                                                                    <field name="value">['mm', 'cm', 'm', 'km']</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="variable" id="V;P^Am5zZdRmshZ6$qCz">
                                                                    <field name="name">c</field>
                                                                    <value name="value">
                                                                      <block type="random_one" id="JJaRG7%{2Ge3:F=EtVI4">
                                                                        <value name="items">
                                                                          <block type="expression" id="GUC_y=]5D(0_:sKxU80s">
                                                                            <field name="value">['rectangle', 'triangle', 'pentagon', 'parallelogram', 'octagon', 'square']</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </statement>
                                              </block>
                                            </statement>
                                            <next>
                                              <block type="variable" id="ZbT@1opou:ggBdx_Uk::G">
                                                <field name="name">b</field>
                                                <value name="value">
                                                  <block type="random_one" id="8r;@2F,rv=gpOxVLTYOX)">
                                                    <value name="items">
                                                      <block type="expression" id="4BRCC!%7yj]loOA)k,J8">
                                                        <field name="value">unit</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="if_then_else_block" id="o4oqg7=K]GBeTb6/PXc7">
                                                    <value name="if">
                                                      <block type="expression" id="HQ-ut.@1uA[(Y{h1@1`U">
                                                        <field name="value">c == 'parallelogram' || c == 'rectangle' || c == 'trapezoid'</field>
                                                      </block>
                                                    </value>
                                                    <statement name="then">
                                                      <block type="variable" id="%:z-7:f;}uaR[q+5M3+z">
                                                        <field name="name">show</field>
                                                        <value name="value">
                                                          <block type="expression" id="s.@1UlBLIf`GaL.UIrDv%">
                                                            <field name="value">1</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </statement>
                                                    <statement name="else">
                                                      <block type="variable" id="Sw8G=dCB`R|/j(,@2Er?B">
                                                        <field name="name">show</field>
                                                        <value name="value">
                                                          <block type="random_number" id="Fo!fNh[lm-Q4EZ2S#VT?">
                                                            <value name="min">
                                                              <block type="expression" id="lauaM-0Ao]NfO5EG#Y])">
                                                                <field name="value">1</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="[1J+_R6dIV||=AIX[1g[">
                                                                <field name="value">2</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </statement>
                                                    <next>
                                                      <block type="statement" id="U@2uN`2#4gK,$?@21:qPkt">
                                                        <field name="value">var ans;

if(c == 'triangle'){ if(show == 2) ans = 3@2a; else ans = 2@2a1 + a;}
else if(c == 'square') {ans = 4@2a;}
else if(c == 'trapezoid') {ans = 3@2a1 + a;}
else if(c == 'rectangle') {ans = 2@2a + 2@2a1;}
else if(c == 'parallelogram') {ans = 2@2a + 2@2a1;}
else if(c == 'pentagon') {if(show == 2) ans = 5@2a; else ans = 2@2a1 + 3@2a;}
else if(c == 'hexagon') {if(show == 2) ans = 6@2a; else ans = 3@2a1 + 3@2a;}
else {if(show == 2) ans = 8@2a; else ans = 3@2a1 + 5@2a;}
                                                        </field>
                                                        <next>
                                                          <block type="variable" id="!I{-m(BP(v(REet`$Cep">
                                                            <field name="name">answer</field>
                                                            <value name="value">
                                                              <block type="random_number" id="5#ql,ci0.Uj%tsOXTRe1">
                                                                <value name="min">
                                                                  <block type="expression" id="AXpFC;uD2tUYx0l(,A1L">
                                                                    <field name="value">1</field>
                                                                  </block>
                                                                </value>
                                                                <value name="max">
                                                                  <block type="expression" id="Yz:ih0b8#y|$Ugpbi+e#">
                                                                    <field name="value">2</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="text_shape" id="He9:IYMvN{qCt;~664{x">
                                                                <statement name="#props">
                                                                  <block type="prop_position" id="0R8!cKNBuZmX}:%=gAPD">
                                                                    <field name="#prop"></field>
                                                                    <value name="x">
                                                                      <block type="expression" id="qTFaA_zJgePqFb32iU[T">
                                                                        <field name="value">400</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="y">
                                                                      <block type="expression" id="T^}=5ke?/xa_.R|ho~f7">
                                                                        <field name="value">40</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_text_contents" id="LZ@1(,Z8Jz(UBES/Wbsj5">
                                                                        <field name="#prop"></field>
                                                                        <value name="contents">
                                                                          <block type="string_value" id="vn.e{JfB0@1=h@1~q9$g:G">
                                                                            <field name="value">What is the perimeter of the ${c} blow?</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_text_style" id="{?/+@2XXmcBdAgDE~@1`@1M">
                                                                            <field name="base">text</field>
                                                                            <statement name="#props">
                                                                              <block type="prop_text_style_font_size" id="|OTm2[{MZ9YS{uGr6mXY">
                                                                                <field name="#prop"></field>
                                                                                <value name="fontSize">
                                                                                  <block type="expression" id="f@2d!AiYEtE9.1wpA;=Yc">
                                                                                    <field name="value">32</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_text_style_fill" id="4:.33-!p}`?+;SGbVblQ">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="fill">
                                                                                      <block type="string_value" id="xtNAI!./F/ud|;W`7:1_">
                                                                                        <field name="value">black</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_text_style_stroke" id="INzl8|-k.^)bjafPbDNE">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="stroke">
                                                                                          <block type="string_value" id="}4_tNvTmIAWv,K5P.k[}">
                                                                                            <field name="value">white</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_style_stroke_thickness" id="d^UEEMSRs:?-XwP|]-Sy">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="strokeThickness">
                                                                                              <block type="expression" id="W$KFH7J8/]l1)$,[g7.N">
                                                                                                <field name="value">2</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <next>
                                                                  <block type="if_then_block" id="pO2^KCh77n1-x@1rJQ}}t">
                                                                    <value name="if">
                                                                      <block type="expression" id="v@2~3;YPVf;6!4%oLzBiy">
                                                                        <field name="value">show == 1</field>
                                                                      </block>
                                                                    </value>
                                                                    <statement name="then">
                                                                      <block type="if_then_block" id="i4C0a^={BB^@20-do[z!j">
                                                                        <value name="if">
                                                                          <block type="expression" id="d6iXcr8p1gHX_kO@1!j5L">
                                                                            <field name="value">c == 'rectangle'</field>
                                                                          </block>
                                                                        </value>
                                                                        <statement name="then">
                                                                          <block type="if_then_block" id="x?+ra[~`L(4T;7=^?|lW">
                                                                            <value name="if">
                                                                              <block type="expression" id="zVLL`d/ng`c.oilHD2Qb">
                                                                                <field name="value">a &lt; a1</field>
                                                                              </block>
                                                                            </value>
                                                                            <statement name="then">
                                                                              <block type="variable" id="TkvUE9h+2/!yxhwv$!Sj">
                                                                                <field name="name">temp</field>
                                                                                <value name="value">
                                                                                  <block type="expression" id="s19H+MWR07tHNgA2U{#^">
                                                                                    <field name="value">a</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="variable" id="d/8hiL~E5).U~8C1XDNu">
                                                                                    <field name="name">a</field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="CL2Iwucj6fk%1}zFvc,]">
                                                                                        <field name="value">a1</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="variable" id="?JggKGD{oDIoB^YCqVG=">
                                                                                        <field name="name">a1</field>
                                                                                        <value name="value">
                                                                                          <block type="expression" id="yXD;uiH,FY6K`$ywtaxd">
                                                                                            <field name="value">temp</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="image_shape" id="{aQaHH1PNRc!{ma#n?kv">
                                                                                <statement name="#props">
                                                                                  <block type="prop_position" id="K!kmk~i_XK_HAruB1a?f">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="6H#1~Zxb/,ipGY|T)IeX">
                                                                                        <field name="value">200</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="MbMg0.DJDrBEbIIQB=h7">
                                                                                        <field name="value">225</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_size" id="CLs.zapn6~+,QC?]lEb7">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="width">
                                                                                          <block type="expression" id="ie_t3[p`(#rY-By|N,)f">
                                                                                            <field name="value">0</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="height">
                                                                                          <block type="expression" id="ZtRCj]o+}4N-kmAFlG2F">
                                                                                            <field name="value">0</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_image_key" id="yvH0cw]4i1%1?@2wEQT#M">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="key">
                                                                                              <block type="string_value" id="E!Ox{X${oQST%u;[Qse-">
                                                                                                <field name="value">2rectangle${item}.png</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_image_src" id="dV!=v9(k4iEWM4{p5y{q">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="src">
                                                                                                  <block type="string_value" id="#[5I;uP$.7A08Bt!UV9d">
                                                                                                    <field name="value">${image_path}2rectangle${item}.png</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="text_shape" id=".%[+08!X[09PzU?n6Guj">
                                                                                    <statement name="#props">
                                                                                      <block type="prop_position" id="U`?WCo{/ZFFfEeo83q`W">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="uDhl:fsVW?cwyR(ZRQ%D">
                                                                                            <field name="value">200</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="~DwCYB(j2.PqZ_/NLlta">
                                                                                            <field name="value">300</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_contents" id="A`.=7k6B)9TrL2X,1A7j">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="contents">
                                                                                              <block type="string_value" id="^-^;#b{epNb|+4u/nVYc">
                                                                                                <field name="value">${a} ${b}</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_style" id="`H|9Km;_!6{^Qrd(2JFX">
                                                                                                <field name="base">text</field>
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_text_style_font_size" id="O7Zvi4@1g4fLwm|@1W)4o7">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="fontSize">
                                                                                                      <block type="expression" id="7i=Tl}jhxeei5)/LiiD2">
                                                                                                        <field name="value">32</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style_fill" id="!}Rykd%zjJb|-8Oy.=J(">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="fill">
                                                                                                          <block type="string_value" id="hWd^Ah@2%,}n6frdvEtA{">
                                                                                                            <field name="value">black</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style_stroke" id="7WA(yY@2`pD_l{k4;lV-G">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="stroke">
                                                                                                              <block type="string_value" id="dRNKlvim+1pQ3_jb7s4@1">
                                                                                                                <field name="value">white</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_stroke_thickness" id="!@2!C;5NM$:~eCli~itwK">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="strokeThickness">
                                                                                                                  <block type="expression" id="85F)W19%V=|~0gI/1G_9">
                                                                                                                    <field name="value">2</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <next>
                                                                                      <block type="text_shape" id="hcHT4/c}kh!?+;vVI4{L">
                                                                                        <statement name="#props">
                                                                                          <block type="prop_position" id="3]I8(CdhuMYj9a/M^XXM">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="1VpM3mo!Al^j=drqdCS+">
                                                                                                <field name="value">200</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="0wQO]043g`l#c!XIETgr">
                                                                                                <field name="value">150</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_contents" id="4UIrLGk,;p,x~gNP|1kg">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="contents">
                                                                                                  <block type="string_value" id="c0#]l-mBx=~T8{X8N5Q}">
                                                                                                    <field name="value">${a} ${b}</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style" id="7A/2I_@2G3VLIaz[uKj5o">
                                                                                                    <field name="base">text</field>
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_text_style_font_size" id="ic,ye;_rBuXyC$F@1)X?n">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="fontSize">
                                                                                                          <block type="expression" id="4Hu#d?D78r+mBeUx{,0+">
                                                                                                            <field name="value">32</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style_fill" id="YzK%g6R91SR|g)s0NCz+">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="fill">
                                                                                                              <block type="string_value" id=".#y~UsfyYCkQ=yNFyhp`">
                                                                                                                <field name="value">black</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_stroke" id="Wi;+5rU%L[Vuq75Fi@1=%">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="stroke">
                                                                                                                  <block type="string_value" id="qY.:_kzUn[Q(5J9Q2d:v">
                                                                                                                    <field name="value">white</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_stroke_thickness" id="T+sB,tqG,nhGa6]D-tw~">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="strokeThickness">
                                                                                                                      <block type="expression" id="LrXM2y`V@2CG,bhzdn[)^">
                                                                                                                        <field name="value">2</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="text_shape" id="LW2DaEG@1w$d-3WHgLj(y">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_position" id="zPk?%x]H@2d[6(+2es;gc">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="G39@2wJQ[NLm4BcOj|#+P">
                                                                                                    <field name="value">320</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="J8rZs=_{N3N@1+}9t0o{?">
                                                                                                    <field name="value">225</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_contents" id="XNmcR2O@2QfGtT9Y,VwY@2">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="contents">
                                                                                                      <block type="string_value" id="-Xai[l{5Kj]zc}HISxX;">
                                                                                                        <field name="value">${a1} ${b}</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style" id="Pu:Pi5JNaj:KE@2sVga]:">
                                                                                                        <field name="base">text</field>
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_text_style_font_size" id="w#AH]6x)iPm}9X_]AHd;">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="fontSize">
                                                                                                              <block type="expression" id="ekEz/1+F{jH__y#@1vWZQ">
                                                                                                                <field name="value">32</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_fill" id="+vbiQ%,-~Q$yobe%z4|y">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="fill">
                                                                                                                  <block type="string_value" id="T]oalNLm?j!Pp$I?A6dA">
                                                                                                                    <field name="value">black</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_stroke" id="-{L-kvE9WH0aLs+VWERR">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="stroke">
                                                                                                                      <block type="string_value" id="+uQnG%m^s?:Y[/H$m2x[">
                                                                                                                        <field name="value">white</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_stroke_thickness" id="DwYF8_nLJdFpc[C6o[HR">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="strokeThickness">
                                                                                                                          <block type="expression" id="iWWAaPXMEEb`2dF;t-nt">
                                                                                                                            <field name="value">2</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="text_shape" id="O,v#aTH`JnM[OlRYd@1ac">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_position" id="$uKmiGTZFAFir#krm=Wi">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="Ke+1Dfu6MwUoKL=FG0ey">
                                                                                                        <field name="value">80</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="%wPa-i+S7yHi6~9rg}Zx">
                                                                                                        <field name="value">225</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_contents" id="YRFIlN#vFmPRf)4UwZ@1t">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="contents">
                                                                                                          <block type="string_value" id="5AStKg0}m,:q+OeB}zaf">
                                                                                                            <field name="value">${a1} ${b}</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style" id="K0I.Ts09hwJiQvnf6b2F">
                                                                                                            <field name="base">text</field>
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_text_style_font_size" id="KkBv%k#|ZgHDl9UQ%6LA">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="fontSize">
                                                                                                                  <block type="expression" id="T|h@2]t:HL``MCmzJ|pnx">
                                                                                                                    <field name="value">32</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_fill" id="Bk.+@1dgHE{lk(WaEX:q2">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="fill">
                                                                                                                      <block type="string_value" id="mB6O=nQGTCwfi0B;ej5C">
                                                                                                                        <field name="value">black</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_stroke" id="Yw,ldBqG+LlIz#(IYGHq">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="stroke">
                                                                                                                          <block type="string_value" id="R}f=bs%CK6^:j($;nCT[">
                                                                                                                            <field name="value">white</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_stroke_thickness" id="rjO,HlsO7x.@2]qY=auaU">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="strokeThickness">
                                                                                                                              <block type="expression" id="?Wp,u:.5U)`h;A8l=i`c">
                                                                                                                                <field name="value">2</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="if_then_block" id="jv9j^M~Yly0=Sn5YI@2VF">
                                                                            <value name="if">
                                                                              <block type="expression" id="NZTH6z|#m}QBLjGLVKD~">
                                                                                <field name="value">c == 'triangle'</field>
                                                                              </block>
                                                                            </value>
                                                                            <statement name="then">
                                                                              <block type="if_then_block" id="P2q86=XNt,aoRNx!GYc/">
                                                                                <value name="if">
                                                                                  <block type="expression" id="$w9rO`7SkExQct+p`LHd">
                                                                                    <field name="value">a &lt; a1</field>
                                                                                  </block>
                                                                                </value>
                                                                                <statement name="then">
                                                                                  <block type="variable" id="opb-/lD8+~#ntKCxP%l~">
                                                                                    <field name="name">temp</field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="/bnDUb@29l~z(In(@2UCc!">
                                                                                        <field name="value">a</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="variable" id="f_(m%#ne5$nEwTi1VyQT">
                                                                                        <field name="name">a</field>
                                                                                        <value name="value">
                                                                                          <block type="expression" id="@2%mg-@20?nev_g(iO/83h">
                                                                                            <field name="value">a1</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="variable" id="qV|{Y|8}JFTu+pF-IOH=">
                                                                                            <field name="name">a1</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="mW4sbPsdS_0Rs1xm5j,9">
                                                                                                <field name="value">temp</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="image_shape" id="mkUbv9Q}6F}~;d]U{OiU">
                                                                                    <statement name="#props">
                                                                                      <block type="prop_position" id="#n~(.:wd!:k?g45pf">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="8enuBv%yrcBiSc){LPSd">
                                                                                            <field name="value">200</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="b=qX@1?Fz`%gnH[1Fk)0=">
                                                                                            <field name="value">225</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_size" id="|^v^gnshL@1c^8`YG=]|;">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="width">
                                                                                              <block type="expression" id="5XV+98xKi,wA84jh@2P6D">
                                                                                                <field name="value">0</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="height">
                                                                                              <block type="expression" id="FJ}8kJ:n~FBXU7SP!@2(_">
                                                                                                <field name="value">0</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_image_key" id="j@2`5vG;T40NQe#NV,o%e">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="key">
                                                                                                  <block type="string_value" id="e(|E?)Ek9@2duCX6EhdYQ">
                                                                                                    <field name="value">2triangle${item}.png</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_image_src" id="@2ai7Er]2kG1o)Aip.4fC">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="src">
                                                                                                      <block type="string_value" id="hH6pINU#65M%_p}_@2.!3">
                                                                                                        <field name="value">${image_path}2triangle${item}.png</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <next>
                                                                                      <block type="text_shape" id="@1b0c;!#3s,6B}D{vy.]1">
                                                                                        <statement name="#props">
                                                                                          <block type="prop_position" id="oTg4RWEGfRdatn%PqAO}">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="=,s4Q,_MsVS9%xEj4)|L">
                                                                                                <field name="value">200</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="Fn2c6@2g?Y2c?A,X(h~QF">
                                                                                                <field name="value">315</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_contents" id="]k=996F3{3O]2I@2p0AMF">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="contents">
                                                                                                  <block type="string_value" id="BdGlB7CKC%+IKn![GROE">
                                                                                                    <field name="value">${a} ${b}</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style" id="yTwX%%W`gg)P9E4Z:Ptv">
                                                                                                    <field name="base">text</field>
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_text_style_font_size" id="5RX=5!`bV?1H`Lwz25LR">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="fontSize">
                                                                                                          <block type="expression" id="ojvabxbP_y(/J@2{?-@2df">
                                                                                                            <field name="value">32</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style_fill" id="V@1QF_!IP!d,U.Y/hA3G)">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="fill">
                                                                                                              <block type="string_value" id="V`{_uk`x.w!cj{|JCAxM">
                                                                                                                <field name="value">black</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_stroke" id="/oyQIKPzDe``Tld~]tK1">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="stroke">
                                                                                                                  <block type="string_value" id="bYaNR]@1@2EJtc43d55E($">
                                                                                                                    <field name="value">white</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_stroke_thickness" id="c3l,KXx6pul$5rwTHXq3">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="strokeThickness">
                                                                                                                      <block type="expression" id="z19W$QU?oe]h_QVup:wl">
                                                                                                                        <field name="value">2</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="text_shape" id="7@1ueB+%$yVCJ.rEz2YgV">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_position" id="6BMu}n,FWziC=#gJ27^Y">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="57;YRx:4Ust:P53pMB5b">
                                                                                                    <field name="value">300</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="6He,sW!IMH?a=Pd-F}mQ">
                                                                                                    <field name="value">225</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_contents" id=";@2ba0y|QY(8W?{E5o8U7">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="contents">
                                                                                                      <block type="string_value" id="H5UiYc0[@1^xFsg@1`#lB6">
                                                                                                        <field name="value">${a1} ${b}</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style" id="93BftL0@2VO5O0/wr$gJ3">
                                                                                                        <field name="base">text</field>
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_text_style_font_size" id="0,!`80z+}+/js{|hAFfu">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="fontSize">
                                                                                                              <block type="expression" id="N:qEhfOF=y?~9BT}o{pf">
                                                                                                                <field name="value">32</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_fill" id="gL8=`])d|T_7%SM)ZiE~">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="fill">
                                                                                                                  <block type="string_value" id="-{!+zop0ag#hWGg2C%%0">
                                                                                                                    <field name="value">black</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_stroke" id=")twzvZd4ytqi|zI!~T;W">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="stroke">
                                                                                                                      <block type="string_value" id="is5#tY+q?kD0Xj=S;@2lQ">
                                                                                                                        <field name="value">white</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_stroke_thickness" id="WE{ju;4^N(:0uANv3ro`">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="strokeThickness">
                                                                                                                          <block type="expression" id="H;0[Lcs3aP8S[2o[$`ms">
                                                                                                                            <field name="value">2</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="text_shape" id="={yj`kq6P1yO~^dXCwdw">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_position" id="@1ZS~Iv=)_#LA{nlQToSL">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="zG8rof0`2%(kf:`9o@1:k">
                                                                                                        <field name="value">80</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="CrYxQF5az|,fintp,}tM">
                                                                                                        <field name="value">225</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_contents" id="q)(g#jqSi2zlKaIO+^8#">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="contents">
                                                                                                          <block type="string_value" id=",M-05sD6Eme/YMA}i[EL">
                                                                                                            <field name="value">${a1} ${b}</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style" id="BxqFJWS$-X82xZOt04So">
                                                                                                            <field name="base">text</field>
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_text_style_font_size" id="$w);YQ~:|iT:7$Vks//s">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="fontSize">
                                                                                                                  <block type="expression" id="kV39yo]A[$379!~kY=Ix">
                                                                                                                    <field name="value">32</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_fill" id="piktI-kud+w`#qoU$IJ~">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="fill">
                                                                                                                      <block type="string_value" id="FF2tk%ygT?8k/;p/t#t2">
                                                                                                                        <field name="value">black</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_stroke" id="B?)Lf/f=C`f0AP0YCukh">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="stroke">
                                                                                                                          <block type="string_value" id="@2VK%tE5bBrGtaI`W63eF">
                                                                                                                            <field name="value">white</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_stroke_thickness" id="s{V@2d55z5$9?Ru#?oDD=">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="strokeThickness">
                                                                                                                              <block type="expression" id="9XkWV[)]MTC@1tlB.6LC(">
                                                                                                                                <field name="value">2</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="if_then_block" id="fzTJO$]|dm?yCAnL@1tLX">
                                                                                <value name="if">
                                                                                  <block type="expression" id="4_?4eX:KSK[DHQ6](767">
                                                                                    <field name="value">c == 'square'</field>
                                                                                  </block>
                                                                                </value>
                                                                                <statement name="then">
                                                                                  <block type="image_shape" id="mZTVIMu);hLs`ae$N9k~">
                                                                                    <statement name="#props">
                                                                                      <block type="prop_position" id="5QdDZ)@2s}jHO)d=pIA//">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="!en[+Q|4Nle2s/ACBv:f">
                                                                                            <field name="value">200</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="#CvpMD7#KPxOAIv-(u](">
                                                                                            <field name="value">225</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_size" id="IF.v?vyJ~p%8VY0u6^@2[">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="width">
                                                                                              <block type="expression" id="taY+|)MS}UkbAm(~e.VX">
                                                                                                <field name="value">0</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="height">
                                                                                              <block type="expression" id="+IQBD:MBIZ9_1kYdcpbB">
                                                                                                <field name="value">0</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_image_key" id="j(I{^]}8JBij3912Ivqt">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="key">
                                                                                                  <block type="string_value" id="3$UUn}kCRcuK}M,-gsf5">
                                                                                                    <field name="value">2square${item}.png</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_image_src" id="7vaiINYT]@2@2+LZFt-M_Q">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="src">
                                                                                                      <block type="string_value" id="#c0jDM?t8Ucj_GP`6q;%">
                                                                                                        <field name="value">${image_path}2square${item}.png</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <next>
                                                                                      <block type="text_shape" id="zk7#c`GsR6hD}3$0-CZb">
                                                                                        <statement name="#props">
                                                                                          <block type="prop_position" id="$B]obvV!at$iDTfmjWaU">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="L2mqJ7t.3)+ipD:4YoE(">
                                                                                                <field name="value">200</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="RS#G1`AgCdJfw,4^gzG-">
                                                                                                <field name="value">325</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_contents" id="3QY4x55eY3;$QNO@1q-.B">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="contents">
                                                                                                  <block type="string_value" id="+xk9]~6NzX0RL00:k!%(">
                                                                                                    <field name="value">${a} ${b}</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style" id="3-PwLn:7sDUAZ6T=?5|R">
                                                                                                    <field name="base">text</field>
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_text_style_font_size" id="?+EKu#!gK4wa`W%5Oc8^">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="fontSize">
                                                                                                          <block type="expression" id="4vdEhFOg1x-,sg9?(2P~">
                                                                                                            <field name="value">32</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style_fill" id="mo2og{YXtJ52DIchtIj3">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="fill">
                                                                                                              <block type="string_value" id="@1KB@27V5U~8n{@2|O_E43f">
                                                                                                                <field name="value">black</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_stroke" id="8KCSdj_S5WT5crtH0s#C">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="stroke">
                                                                                                                  <block type="string_value" id="xdUG^{0ij(|;KEcA#Eud">
                                                                                                                    <field name="value">white</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_stroke_thickness" id="Z|2LzgDRU??/`2P,u}(h">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="strokeThickness">
                                                                                                                      <block type="expression" id="~f~zazIULd/JhAH:ZpJR">
                                                                                                                        <field name="value">2</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="text_shape" id="A5yDIBe3_rz.ARD|SEv!">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_position" id="|];FvXHE2yVU]8b}mM4b">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="Qt;e,6:_Ch,Sif#X:}?.">
                                                                                                    <field name="value">200</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="S8mJpxMPJZ4b+MP:Znm#">
                                                                                                    <field name="value">140</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_contents" id="0/fyozT|kmF}yjearF$e">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="contents">
                                                                                                      <block type="string_value" id=";8Jf@15ur,Q~@1aiI7PIXb">
                                                                                                        <field name="value">${a} ${b}</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style" id=",2;TL0mg-wL^c!Ji5a+;">
                                                                                                        <field name="base">text</field>
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_text_style_font_size" id="dNx|W1@1Alh,RKQL`{GfT">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="fontSize">
                                                                                                              <block type="expression" id="SjygLX~qXA,(Y^s!4)wH">
                                                                                                                <field name="value">32</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_fill" id="5euWN8RC0{Qpd5=[idn@1">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="fill">
                                                                                                                  <block type="string_value" id="#d$m-JH6-Zw?8!jxUlsn">
                                                                                                                    <field name="value">black</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_stroke" id="+69Q;?he0^0S~aD+niad">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="stroke">
                                                                                                                      <block type="string_value" id="MVvV_iTDrI}dq(eq:yUo">
                                                                                                                        <field name="value">white</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_stroke_thickness" id="A~#0,m~S~Bh_5/XEmMi.">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="strokeThickness">
                                                                                                                          <block type="expression" id="udm7?1{8Q=lZQ`vI$p#:">
                                                                                                                            <field name="value">2</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="text_shape" id="zo#x((l@2hvfxLag@1k!@2X">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_position" id="/~?}9^KX}UlNbPMD{E?B">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="TRUI}5?@2W+^f|wuml^pQ">
                                                                                                        <field name="value">320</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="}@2uY(c^0@2exc@2)T9m7(J">
                                                                                                        <field name="value">225</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_contents" id="`.f9mJAepmt3#[-fC229">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="contents">
                                                                                                          <block type="string_value" id=")QM;1%@1x?bF8xgaw++{u">
                                                                                                            <field name="value">${a} ${b}</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style" id="J3-P?Q:TIaDK:p+3k!K{">
                                                                                                            <field name="base">text</field>
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_text_style_font_size" id="yyn|J}.#`VX5;kKEYOR?">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="fontSize">
                                                                                                                  <block type="expression" id="-!EJnK$dEvRMIt=@1Wxq?">
                                                                                                                    <field name="value">32</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_fill" id="faKjgKv/(VQt8Mqs/|_U">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="fill">
                                                                                                                      <block type="string_value" id="km@1yTF+Iz7Sn!h12xlZ9">
                                                                                                                        <field name="value">black</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_stroke" id="dMf3#d?fN3P-A9~6PMu=">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="stroke">
                                                                                                                          <block type="string_value" id="vA]#yJ4SaJwh~pXN^FmN">
                                                                                                                            <field name="value">white</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_stroke_thickness" id="Ese#{(P9iT1|h)_T@1%:j">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="strokeThickness">
                                                                                                                              <block type="expression" id="ISr9(N/fDx!$0JKHZi!P">
                                                                                                                                <field name="value">2</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="text_shape" id="Ub%C):L[tIE0kd@2)S1x.">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_position" id="(95x9PDh}S[]4$V0l{xY">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="x">
                                                                                                          <block type="expression" id="[HFOImNxDufrRmPaRZ3A">
                                                                                                            <field name="value">80</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id="C(+~uY$rN0){u}fC0C}Z">
                                                                                                            <field name="value">225</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_contents" id="f78:/dLQ$M9,j)#hu59j">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="contents">
                                                                                                              <block type="string_value" id="{vl_G0nXRjGpC##.ICe;">
                                                                                                                <field name="value">${a} ${b}</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style" id="J6A_5Mvzvl5[o5?qp8FR">
                                                                                                                <field name="base">text</field>
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_text_style_font_size" id="Mu~]4nnBoMM`,wO5pR@2@1">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="fontSize">
                                                                                                                      <block type="expression" id="l8UQd|]Ja,2NBzpQd;}J">
                                                                                                                        <field name="value">32</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_fill" id="]LV)b=@1/kGNofd9M%uO=">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="fill">
                                                                                                                          <block type="string_value" id="CuBO]mO|~aG}!`_]7}8c">
                                                                                                                            <field name="value">black</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_stroke" id="|EbG-RbSuVHPcJ;aS=w|">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="stroke">
                                                                                                                              <block type="string_value" id="$u=mq}^0kPmWPZBpU!RV">
                                                                                                                                <field name="value">white</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_stroke_thickness" id="viXJ]9p%L},uYqq!:AF;">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="strokeThickness">
                                                                                                                                  <block type="expression" id="D{M,-:I@1(6;rE~dgMfg$">
                                                                                                                                    <field name="value">2</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="if_then_block" id="9hE;K3rvDEnlS87jn,l,">
                                                                                    <value name="if">
                                                                                      <block type="expression" id=";:wnDRTrE{8D}OE]24w^">
                                                                                        <field name="value">c == 'parallelogram'</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <statement name="then">
                                                                                      <block type="if_then_block" id="}/gnMAlAUy7v~91R$rC+">
                                                                                        <value name="if">
                                                                                          <block type="expression" id="d8eDCPy5NX;(l=Ih@1[)_">
                                                                                            <field name="value">a &lt; a1</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <statement name="then">
                                                                                          <block type="variable" id="B]l|)[`8VT9@2-9yw=|ZR">
                                                                                            <field name="name">temp</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id=";dO,:{}cTcz|}n8Wq;ny">
                                                                                                <field name="value">a</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="variable" id="lIO@2wltAxDzux@29A8O=6">
                                                                                                <field name="name">a</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="w4%/j#%_B:D-d$2M94/m">
                                                                                                    <field name="value">a1</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="variable" id="jCr8?-a!GZ~jFQl6X~{I">
                                                                                                    <field name="name">a1</field>
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="]CjaY[gD8ubV9fsC5J5P">
                                                                                                        <field name="value">temp</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="image_shape" id="|l{$tq7r3nq|(:6K[RBb">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_position" id="=]8vvfiNI;Bgf0F4t/+8">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="r)3Xi=Nw}72~1bz+M,zQ">
                                                                                                    <field name="value">200</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="I+k$4ar}._7H@2v@12?s!|">
                                                                                                    <field name="value">225</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_size" id=",IB6%u62eNJZ@18??+xGr">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="width">
                                                                                                      <block type="expression" id="$aR%t^0)kw]Qo,V=(9%G">
                                                                                                        <field name="value">0</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="height">
                                                                                                      <block type="expression" id="T;u7tagP(f8A5^kJ+wlK">
                                                                                                        <field name="value">0</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_image_key" id="A758GPa?UMu`YR@2#B@2yt">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="key">
                                                                                                          <block type="string_value" id="v5bsLz3D%}I3YV(Vw;n6">
                                                                                                            <field name="value">2parallelogram${item}.png</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_image_src" id="Jx7tCq}x9C=xNulOdULY">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="src">
                                                                                                              <block type="string_value" id=",:=DT@1h+hOi(;kgokPS,">
                                                                                                                <field name="value">${image_path}2parallelogram${item}.png</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="text_shape" id="Jx=vX(r~.7y(F=3`)uNf">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_position" id="c{$53I.PE;FHjTB)5d{x">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="KHFPth3{9}}zKw1_nVp/">
                                                                                                        <field name="value">200</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="pQlqdVk%{OWLMk?|`;C%">
                                                                                                        <field name="value">300</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_contents" id=",7HPk0;5BVh(](z?u{!%">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="contents">
                                                                                                          <block type="string_value" id=".j^v:DYnS~@2mgf$L%{l)">
                                                                                                            <field name="value">${a} ${b}</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style" id="y_QThAN$A0gNFe(;uLm0">
                                                                                                            <field name="base">text</field>
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_text_style_font_size" id="Hr9P}ZKrCR2_W,1d_1Xw">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="fontSize">
                                                                                                                  <block type="expression" id="qKSjvl:T9Qwru{Mk]_={">
                                                                                                                    <field name="value">32</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_fill" id="ku#D;+tbU3#:hH$O|q;b">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="fill">
                                                                                                                      <block type="string_value" id="P|FKr:(o@2_6n8MlhMp9[">
                                                                                                                        <field name="value">black</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_stroke" id="w}OWF{yoWMY_dej@2BETJ">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="stroke">
                                                                                                                          <block type="string_value" id="3TU!:v|ww|u(Y3VW,-1N">
                                                                                                                            <field name="value">white</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_stroke_thickness" id="A{F#DYy{`CNSVCF:466Q">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="strokeThickness">
                                                                                                                              <block type="expression" id=")RFg:^q`iGmv!Z=hp4~-">
                                                                                                                                <field name="value">2</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="text_shape" id="_Oo5#J+VtdTak]`m~J+%">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_position" id="Txd[U%tjeUc@295:r8/}J">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="x">
                                                                                                          <block type="expression" id="-7Pw-Z[y`=|?t9NWiC?+">
                                                                                                            <field name="value">200</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id="/(.~7.9u%?Ma/G=J%Q^j">
                                                                                                            <field name="value">150</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_contents" id=";AoK^Ws,GOn:EySF7gDO">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="contents">
                                                                                                              <block type="string_value" id="9t%(xyD]z]owb,4lVQUE">
                                                                                                                <field name="value">${a} ${b}</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style" id="i=AWz%hw`2~A4P:NU)@2,">
                                                                                                                <field name="base">text</field>
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_text_style_font_size" id="ed9x[Yg;FZ^Dmcan:38z">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="fontSize">
                                                                                                                      <block type="expression" id="-dmmB{`QU,CG@1$,a30c9">
                                                                                                                        <field name="value">32</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_fill" id="+CAuzNbl4k[N^_CJF+3q">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="fill">
                                                                                                                          <block type="string_value" id="gIFgutMF{OjB|2d)?S$+">
                                                                                                                            <field name="value">black</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_stroke" id="nmWF(+U%vRaFAM8]t:h[">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="stroke">
                                                                                                                              <block type="string_value" id="|{-DX`%(xBmBmf/%L=Ph">
                                                                                                                                <field name="value">white</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_stroke_thickness" id="p,_[cZ7G1@2g0IUj9)9f)">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="strokeThickness">
                                                                                                                                  <block type="expression" id="Jt!og1@2/7k()7?;D:%@2n">
                                                                                                                                    <field name="value">2</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="text_shape" id="C0{ZB$c^mv,_O(i30~lF">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_position" id="}._5;vcZj~iz]@1Ywg9[?">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="Bag0;}MBL:vhA.B-8iVM">
                                                                                                                <field name="value">320</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="5b`4OQ;r_Ru1k=Pfbk(Y">
                                                                                                                <field name="value">225</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_contents" id="D1D64pyPBF/Qi|QmBSp^">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="contents">
                                                                                                                  <block type="string_value" id="-W?;E3rN7+cP2rmkGh!Y">
                                                                                                                    <field name="value">${a1} ${b}</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style" id="PjJ4Do6BsIrcErxa!6L$">
                                                                                                                    <field name="base">text</field>
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_text_style_font_size" id="$f;Jec9jduGArbSR8X1w">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="fontSize">
                                                                                                                          <block type="expression" id="lfQYxkxZ~T{=c`!w!|1,">
                                                                                                                            <field name="value">32</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_fill" id="]+-8hup$CC]VpF8/|D{K">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="fill">
                                                                                                                              <block type="string_value" id="ua`w%1)8/)pqzaD5sE59">
                                                                                                                                <field name="value">black</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_stroke" id="XV.@2_[_Vc92N7Y8]1o]N">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="stroke">
                                                                                                                                  <block type="string_value" id="(}hR-gZ_cvkT10ddG[O$">
                                                                                                                                    <field name="value">white</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_stroke_thickness" id="er#P;OSUjRZkm+jN?NgP">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="strokeThickness">
                                                                                                                                      <block type="expression" id="gD7rlhaZ|y@18(W:NKu:I">
                                                                                                                                        <field name="value">2</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="text_shape" id="bs3mI32lx;D[~Q%$uBnL">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_position" id="ix4ZCq(J@2hN]j3r@15{k!">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="RuJ+t+b]jcV@1,MbJYF[V">
                                                                                                                    <field name="value">80</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="[o4t4gWvw}GR#q7)5;Bs">
                                                                                                                    <field name="value">225</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_contents" id="AXaIxLUT5]k37qW8M7=N">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="contents">
                                                                                                                      <block type="string_value" id="n#w`;]z`{rĲMSroIp">
                                                                                                                        <field name="value">${a1} ${b}</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style" id="+5q1bv47+kuFtj{`b@1Gs">
                                                                                                                        <field name="base">text</field>
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_text_style_font_size" id="x[[[:3ZxIx?cU61Ey)h@1">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="fontSize">
                                                                                                                              <block type="expression" id="G(kcQnxH+b.wlgj|XTF^">
                                                                                                                                <field name="value">32</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_fill" id="~0FB^@1BboaYE-!UK{3i0">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="fill">
                                                                                                                                  <block type="string_value" id="!;(%Iiv)f9RDKqcT_=Z_">
                                                                                                                                    <field name="value">black</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_stroke" id="cNCZ@1/Aws{nSiT1ZnNYT">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="stroke">
                                                                                                                                      <block type="string_value" id="lqxrQP$!g:;,S%:Mph@2d">
                                                                                                                                        <field name="value">white</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="dEisOti282/`^J.^/#$H">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="strokeThickness">
                                                                                                                                          <block type="expression" id="A+.T4r6u+N8wF/$wCZ1;">
                                                                                                                                            <field name="value">2</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <next>
                                                                                      <block type="if_then_block" id="@2vCs{Xvv[Sp(`/c$GXF6">
                                                                                        <value name="if">
                                                                                          <block type="expression" id="Z_ov~^$wi$E8QBK5@10:J">
                                                                                            <field name="value">c == 'trapezoid'</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <statement name="then">
                                                                                          <block type="if_then_block" id="nck?DPo_B(n-nP]$FQYe">
                                                                                            <value name="if">
                                                                                              <block type="expression" id="-@2u7~R.}PkF^8hnA+VI#">
                                                                                                <field name="value">a &lt; a1</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <statement name="then">
                                                                                              <block type="variable" id="nvc^pH_:.@2YQ0x$rEaF:">
                                                                                                <field name="name">temp</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="_q,IM:Y8ZLz7b!PN5e#4">
                                                                                                    <field name="value">a</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="variable" id="lE]79c76`iz.,+}|VT|b">
                                                                                                    <field name="name">a</field>
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="lP}@2J_LVJlXGo@2?Sq(5H">
                                                                                                        <field name="value">a1</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="variable" id="1=X^0o.Z;ctIS|G0`z{4">
                                                                                                        <field name="name">a1</field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id="SzXehPm52Q_M]6H#y~kC">
                                                                                                            <field name="value">temp</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="image_shape" id="aG]w.DiX-NF2a{3D+O^U">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_position" id="C^~DWkN]}]FVs!3sYfW,">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="{gTx_a$(FR-fR6?E?`U.">
                                                                                                        <field name="value">200</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id=")N4o0s^Cx,.XD|x9r)pU">
                                                                                                        <field name="value">225</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_size" id="n-0lg%{|z,G=Bx=G4d#P">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="width">
                                                                                                          <block type="expression" id="a$?ilY@2Dp5Z2Cd|Ci$`D">
                                                                                                            <field name="value">0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="height">
                                                                                                          <block type="expression" id=".6TF/9NFe0w2sB//0##:">
                                                                                                            <field name="value">0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_image_key" id="7@2wdG#Cy~268N4WJV@2#H">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="key">
                                                                                                              <block type="string_value" id="dm2iVbakq])T3HIY+~sA">
                                                                                                                <field name="value">2trapezoid${item}.png</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_image_src" id="e_h.!h}F,C(BYc,#Z|;9">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="src">
                                                                                                                  <block type="string_value" id="zI+#YIny!Hy_WITYA}EO">
                                                                                                                    <field name="value">${image_path}2trapezoid${item}.png</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="text_shape" id="!KsnZ`}#jjs|4@1j7F6.-">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_position" id="Wy|si){6vi7l0}ifmx9;">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="x">
                                                                                                          <block type="expression" id="yeZ(wzZNT2]{SO8UHaah">
                                                                                                            <field name="value">200</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id="JyBBwJaXu5@20YEggqp+4">
                                                                                                            <field name="value">300</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_contents" id="GqoQx2pbwvxMcay1~B`g">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="contents">
                                                                                                              <block type="string_value" id="a?HEv,R@27u#Q`g;nqTUj">
                                                                                                                <field name="value">${a} ${b}</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style" id="6]GhY$6#%G{6]YA(spm$">
                                                                                                                <field name="base">text</field>
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_text_style_font_size" id="_SAK~M]uSPZv%$SxaoMC">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="fontSize">
                                                                                                                      <block type="expression" id="z~|VuQ2qz:t~f4}n@1=q0">
                                                                                                                        <field name="value">32</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_fill" id="j~9Df~QN28pl;Rq0JF!C">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="fill">
                                                                                                                          <block type="string_value" id="=9JSua2X@2Y:#3[x(-P!P">
                                                                                                                            <field name="value">black</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_stroke" id="z}k42^eK5+?S9#wn.qtE">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="stroke">
                                                                                                                              <block type="string_value" id="EkgzX#blF7hxpkQ{)r:t">
                                                                                                                                <field name="value">white</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_stroke_thickness" id="q,MI:76.5Thp@2Y=34pcZ">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="strokeThickness">
                                                                                                                                  <block type="expression" id="f@2z+,gt`fS;jRqP2OZhn">
                                                                                                                                    <field name="value">2</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="text_shape" id="Q5a=+1Hf1^fI7Aay:whu">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_position" id="[@1N1xDCJ#XLN)+i+wy.=">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id=")#k[@2TI7;oEc}y4?g!I_">
                                                                                                                <field name="value">200</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="J_r1kz0Byr;t.$c5w--H">
                                                                                                                <field name="value">150</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_contents" id="kmfq2pH+To-91~{+sD(Y">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="contents">
                                                                                                                  <block type="string_value" id="wq5X$!GosxH7on/,@2yx[">
                                                                                                                    <field name="value">${a1} ${b}</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style" id="g~YONCeNSWFQT,$:1hH2">
                                                                                                                    <field name="base">text</field>
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_text_style_font_size" id="(@16;M/4$W!=%V]NC#fOF">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="fontSize">
                                                                                                                          <block type="expression" id="K3Qzlj/z#4h0W,l4S.-@2">
                                                                                                                            <field name="value">32</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_fill" id="MGh3xnXZXkmFbSspRvWe">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="fill">
                                                                                                                              <block type="string_value" id="MFpoh:,3vuxk:C^@14x})">
                                                                                                                                <field name="value">black</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_stroke" id="?c?#A#wr-hm?|-NDp[x+">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="stroke">
                                                                                                                                  <block type="string_value" id="8/twd{]oS~DV?M|T@1=I(">
                                                                                                                                    <field name="value">white</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_stroke_thickness" id="ikZ32pqD}Qgd%--)d+(i">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="strokeThickness">
                                                                                                                                      <block type="expression" id="aNNhYgrWtTk(E9y#D1i@2">
                                                                                                                                        <field name="value">2</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="text_shape" id="9Fj@2O6_!qBXi@1yzY=PG_">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_position" id="[bS.4jhkW-@1@172DmkBAy">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="A$D3AACZyV?GI|TXjS./">
                                                                                                                    <field name="value">320</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="_VLvi%Q}sRg|,fkVkFdm">
                                                                                                                    <field name="value">225</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_contents" id="!EmeJ).@1l3Z@2YG2_c)se">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="contents">
                                                                                                                      <block type="string_value" id=",zg9+O9kHi6fk;5D1p3j">
                                                                                                                        <field name="value">${a1} ${b}</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style" id="o9%gaH|GPhge?V2?MqXd">
                                                                                                                        <field name="base">text</field>
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_text_style_font_size" id="3U4U5rZIJ?Fh,{v#l/%L">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="fontSize">
                                                                                                                              <block type="expression" id=",f=W#,{F=}V-a__7Dob;">
                                                                                                                                <field name="value">32</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_fill" id="=a+Lr25bci|^D=uNBoYQ">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="fill">
                                                                                                                                  <block type="string_value" id="^%/;B73@2CgcLC/#2V#D4">
                                                                                                                                    <field name="value">black</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_stroke" id="`FnSxoPX6UkrOiR3Qo)b">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="stroke">
                                                                                                                                      <block type="string_value" id="^bCggxJVvr5)7fLf[c-Q">
                                                                                                                                        <field name="value">white</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="}zpgI14:[a-Rg3%|LcYC">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="strokeThickness">
                                                                                                                                          <block type="expression" id="k}%hM2Kx{ED^dZQ|U=vJ">
                                                                                                                                            <field name="value">2</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="text_shape" id="[_;Ltzouh09?qZ6+/aC(">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_position" id="gtxT;^6ar/ynm`?+QyKv">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="5UWsKsrJLR}27l^t%~y4">
                                                                                                                        <field name="value">80</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="jDqnVQ3=A|7Y]{xIE6=q">
                                                                                                                        <field name="value">225</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_contents" id="`6xoYt2`[keyTZS5bYr1">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="contents">
                                                                                                                          <block type="string_value" id="lzXul,Qr0McpY.%T2k2n">
                                                                                                                            <field name="value">${a1} ${b}</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style" id="a_K9fi8`CL5%SBeR)2mS">
                                                                                                                            <field name="base">text</field>
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_text_style_font_size" id="kGf9A^dAVQA4H]OKO-X$">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="fontSize">
                                                                                                                                  <block type="expression" id="/QJ;UqpqW0,`-l{r%DS!">
                                                                                                                                    <field name="value">32</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_fill" id="!},;!]dU}gYa]QQG?Y.s">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="fill">
                                                                                                                                      <block type="string_value" id="R4%0QnYhR+|H1DzSHQO{">
                                                                                                                                        <field name="value">black</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_stroke" id="w^vUP8pO)Fjf-2qM4H{`">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="stroke">
                                                                                                                                          <block type="string_value" id="l1YHZBT-Wh@1E_{!#2uTR">
                                                                                                                                            <field name="value">white</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style_stroke_thickness" id="G;Q_N,Eo`lKTm(,q#l(n">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="strokeThickness">
                                                                                                                                              <block type="expression" id="dDRYb8,7g,JJ+lkA=3oC">
                                                                                                                                                <field name="value">2</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="if_then_block" id="6g1V7{9s@13N3I(#(iX})">
                                                                                            <value name="if">
                                                                                              <block type="expression" id="LQ(@2r.xPTG~)-G9]Dbi1">
                                                                                                <field name="value">c == 'pentagon'</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <statement name="then">
                                                                                              <block type="image_shape" id="jzR,o6[v#D[|SCa|o+g;">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_position" id="nV{i^8Bo,W~c@1Avt1`yw">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="c4NpI2UH3g8lUt%4h%2|">
                                                                                                        <field name="value">200</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="JLKsOHWVIlZYx4f~N(b)">
                                                                                                        <field name="value">225</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_size" id="ZUDg-fw0Mb55uhG9RgcX">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="width">
                                                                                                          <block type="expression" id="OKHaXt5DQEOY1qg)@2)ZU">
                                                                                                            <field name="value">0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="height">
                                                                                                          <block type="expression" id="`!lxoIY%{lqab$!;p[;K">
                                                                                                            <field name="value">0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_image_key" id="Mks,lb6[g/sIdjP7IlLz">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="key">
                                                                                                              <block type="string_value" id="C7%^xyA,6mJQ+p~0u?ng">
                                                                                                                <field name="value">2pentagon${item}.png</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_image_src" id="8E_Mo~SR!+PCoG%$SDyb">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="src">
                                                                                                                  <block type="string_value" id="O~WFzOp)iAa2JXZ@1+EB[">
                                                                                                                    <field name="value">${image_path}2pentagon${item}.png</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="text_shape" id="S{Ne+H~GO`Pl|nOEwf`l">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_position" id="dT4gCpRYIos;u}cA!/26">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="x">
                                                                                                          <block type="expression" id="@1fi`jwr`4W@1MrD5saQ/6">
                                                                                                            <field name="value">200</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id="CTT!0X@1^5|?+%U_)LB1n">
                                                                                                            <field name="value">325</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_contents" id="vqz#;b}9{k@2c5E[@2~Z`8">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="contents">
                                                                                                              <block type="string_value" id="}4.V,`zW8Mp/1o/yqmO|">
                                                                                                                <field name="value">${a} ${b}</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style" id="Qm@1I;1[YF{cKzQ4%hCvO">
                                                                                                                <field name="base">text</field>
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_text_style_font_size" id="4xr/GBA5A^LZ5cn$Q7T{">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="fontSize">
                                                                                                                      <block type="expression" id="9y]@1!EUy1H~=AJriCpNn">
                                                                                                                        <field name="value">32</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_fill" id="K4=7x8?sG[F(Ldm~iWgF">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="fill">
                                                                                                                          <block type="string_value" id="WHmiVz.3.IU~?6^dR%Fi">
                                                                                                                            <field name="value">black</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_stroke" id="1}k1T):qry(}^PCO$_0a">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="stroke">
                                                                                                                              <block type="string_value" id="8{-0ysM/I6u-@2%ncGSx?">
                                                                                                                                <field name="value">white</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_stroke_thickness" id="02W(9#Pd1gB%=3H`zP^d">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="strokeThickness">
                                                                                                                                  <block type="expression" id="#!$/z;0-7h.dZ4#9AyP+">
                                                                                                                                    <field name="value">2</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="text_shape" id="Zox@2`BG8r3[TVFKbWb^Y">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_position" id="z^Ib5CN1XTpc;343^YqB">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="kptyE%%;+#tz4:kv]jcU">
                                                                                                                <field name="value">300</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="xVJ{,!F)?ZUW$bKW]/G[">
                                                                                                                <field name="value">265</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_contents" id="^[pUP_8wQfrz%kLXpNd6">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="contents">
                                                                                                                  <block type="string_value" id="H@2afevW#6VsD%|KsdZe$">
                                                                                                                    <field name="value">${a} ${b}</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style" id="/k|H4?mlE-!Zb()gCsE%">
                                                                                                                    <field name="base">text</field>
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_text_style_font_size" id="Q%jlf%7k{QQN#{o:_ZSE">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="fontSize">
                                                                                                                          <block type="expression" id="[Pm/F0eT)t9JpB]7]me+">
                                                                                                                            <field name="value">32</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_fill" id=")3_NH4kfEq[dyEtg{c@2p">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="fill">
                                                                                                                              <block type="string_value" id="-mgo-5BG2bm}:Aa=Xr#1">
                                                                                                                                <field name="value">black</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_stroke" id="E#dOiv_drJWjF`jf~@1SH">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="stroke">
                                                                                                                                  <block type="string_value" id="EoTh7nyQ8s(t-,1G)D,u">
                                                                                                                                    <field name="value">white</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_stroke_thickness" id="^+cf/,2lz]By5V?,aa@1?">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="strokeThickness">
                                                                                                                                      <block type="expression" id="44(8+p2wyvpklfEwt:@2d">
                                                                                                                                        <field name="value">2</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="text_shape" id="8g-it40-CTiaCL):!tYw">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_position" id="D,[jg%?YlQEZwX,q]2Pm">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="oValY7)K/2j8ik_-]@2r`">
                                                                                                                    <field name="value">100</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="awf@1ol6U0^E;3w?Cv@2@1d">
                                                                                                                    <field name="value">250</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_contents" id="p~){lPDTa[jwfT~jxq2T">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="contents">
                                                                                                                      <block type="string_value" id="Q,@1y@18m:4f2Wb}5|1qC7">
                                                                                                                        <field name="value">${a} ${b}</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style" id="|RVIMAQ`TsnmCeFv+8^Q">
                                                                                                                        <field name="base">text</field>
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_text_style_font_size" id="+}#0/oBomwNQBE4R~%.P">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="fontSize">
                                                                                                                              <block type="expression" id="/#Wx0).xeC(tv%P+:882">
                                                                                                                                <field name="value">32</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_fill" id="c@2P7`]a#@1@2:6Y-6Q^=]n">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="fill">
                                                                                                                                  <block type="string_value" id="o6^qY%(}=|V5EZHyn?+5">
                                                                                                                                    <field name="value">black</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_stroke" id="{{_X`M5.?^l2LjQS:7`Y">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="stroke">
                                                                                                                                      <block type="string_value" id="4,p}Nvhv+GYMOuhbefWt">
                                                                                                                                        <field name="value">white</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="(Dh@1jotOGO{ir3(-}Uw}">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="strokeThickness">
                                                                                                                                          <block type="expression" id="gd{?.zntp=3@1)#8oa7ke">
                                                                                                                                            <field name="value">2</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="text_shape" id="dwe_@1`fl6!ixf%K//R9Y">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_position" id="Pi5RB)oIb:gM@2gT|cFl5">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="4N~}h[OG]/#338g7xFaG">
                                                                                                                        <field name="value">130</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="-|:E~s`cDCe{N]Ty8?}g">
                                                                                                                        <field name="value">160</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_contents" id=";3HoV0g##o([t~%It@2g6">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="contents">
                                                                                                                          <block type="string_value" id="jGK8@1fyxwSr}^{?A[FM9">
                                                                                                                            <field name="value">${a1} ${b}</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style" id="Wab6#a@210Jr(JWHT/.~B">
                                                                                                                            <field name="base">text</field>
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_text_style_font_size" id="xeqNFAZ%2RHhoH}P0uKn">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="fontSize">
                                                                                                                                  <block type="expression" id="uiu5KoSx~zqE,6`Mpw@2z">
                                                                                                                                    <field name="value">32</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_fill" id="DGwb8{W,)LfG`AFMhA,Z">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="fill">
                                                                                                                                      <block type="string_value" id="#@2g58Xv{U%g9Bt=LY$v[">
                                                                                                                                        <field name="value">black</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_stroke" id="L|`{@2b4c2tpu)8cKPTFe">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="stroke">
                                                                                                                                          <block type="string_value" id="lX[`;`wF0=5!i%rnq(]2">
                                                                                                                                            <field name="value">white</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style_stroke_thickness" id="fdv0,1-0r;WKT68))_O@2">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="strokeThickness">
                                                                                                                                              <block type="expression" id="K4RyJ^[quUt|-cIGkT_2">
                                                                                                                                                <field name="value">2</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="text_shape" id="t8hUGyLzo9-2f0eU=`{-">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_position" id="Zh+.dCwiP_wNM+kds:xz">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="=u-lUyNFJ.fhl4[FIq3r">
                                                                                                                            <field name="value">290</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="CfX`s.iC%mZTR!AV{CPU">
                                                                                                                            <field name="value">180</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_contents" id="UfMQ-_^1@2N}$V5Ue/j$6">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="contents">
                                                                                                                              <block type="string_value" id="iXAB3V6eF.{`JgAlLC#p">
                                                                                                                                <field name="value">${a1} ${b}</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style" id="beaE8`Imx|ROXG?|GDN4">
                                                                                                                                <field name="base">text</field>
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_text_style_font_size" id="k]%@1;,$mH0T}~xkw=?x^">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="fontSize">
                                                                                                                                      <block type="expression" id="H]=`X{B4jF`?%P4P#dsv">
                                                                                                                                        <field name="value">32</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_fill" id="PwxfK;WKv^lAxJuyh9UA">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="fill">
                                                                                                                                          <block type="string_value" id="{`i.w4^GeP_c%%gttcBv">
                                                                                                                                            <field name="value">black</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style_stroke" id="_/?JHp8PQ#GD2^1pu3rf">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="stroke">
                                                                                                                                              <block type="string_value" id="=FUQL}k_^QJyC{-ev/{#">
                                                                                                                                                <field name="value">white</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_text_style_stroke_thickness" id="[rUbmfryy89B,{kaf+#F">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="strokeThickness">
                                                                                                                                                  <block type="expression" id="0U}%eYs|4w@1]OfoD#~wH">
                                                                                                                                                    <field name="value">2</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="if_then_block" id="5wBja%$}LKe|7QN`y8`w">
                                                                                                <value name="if">
                                                                                                  <block type="expression" id="n-=}sC92)g6a6suSd$P!">
                                                                                                    <field name="value">c == 'hexagon'</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <statement name="then">
                                                                                                  <block type="image_shape" id=":(O,c,%-l=JhEv]vP85;">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_position" id="#Nz8o@2=]mEDyqIE%ZcX-">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="x">
                                                                                                          <block type="expression" id=";u=G|xQ#~PhKklHU.ok)">
                                                                                                            <field name="value">200</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id="fhdvO=|#jFy}_[$^T(9Q">
                                                                                                            <field name="value">225</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_size" id="L?C{z-)B=]rM{Q)gQh4D">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="width">
                                                                                                              <block type="expression" id="]iwO#,9Sm1f7=]hxWg#Y">
                                                                                                                <field name="value">0</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="height">
                                                                                                              <block type="expression" id="UKjAOUe@2#$h|9T7|Th^#">
                                                                                                                <field name="value">0</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_image_key" id="_=9j:p.?QB`RiAi_^𢾊                                                                                                                &lt;field name=">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="key">
                                                                                                                  <block type="string_value" id="X:Fgoe~Shu12Jwh~lCvv">
                                                                                                                    <field name="value">2hexagon${item}.png</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_image_src" id=";=h@1T2pDGa:L-a6e#C.@2">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="src">
                                                                                                                      <block type="string_value" id="=F4?C0O+0WF#/I`glV$p">
                                                                                                                        <field name="value">${image_path}2hexagon${item}.png</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="text_shape" id=")DQlP8L~-hp,H8rnGXp@1">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_position" id="f4A6~kS@2B~[Qp5lxfAm.">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="{g[}awh`=O82RlX]mr$B">
                                                                                                                <field name="value">200</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="H[!AiR1?ifzD(y!5]is?">
                                                                                                                <field name="value">325</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_contents" id="=-PMWN,%Rf8;V}dOv0f~">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="contents">
                                                                                                                  <block type="string_value" id=":(d@1n`FpCS^;:oD9~]u4">
                                                                                                                    <field name="value">${a} ${b}</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style" id="goK)ug@1([[6zd?4.m1U]">
                                                                                                                    <field name="base">text</field>
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_text_style_font_size" id="dpcE6S3P@1)piOtyC8}[{">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="fontSize">
                                                                                                                          <block type="expression" id="?AU3J|-`[Hzr6-.`IOc9">
                                                                                                                            <field name="value">32</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_fill" id="lP?|N3xz=Lit@2g2EdT{4">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="fill">
                                                                                                                              <block type="string_value" id="z3g2,;p)72I4I:g@2gR|_">
                                                                                                                                <field name="value">black</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_stroke" id="/yjv[8/%YkYn-kdp;bVT">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="stroke">
                                                                                                                                  <block type="string_value" id="DT{;D`tm?2g-qz7$qk4m">
                                                                                                                                    <field name="value">white</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_stroke_thickness" id="yihC_YAOAN613[b+a[$V">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="strokeThickness">
                                                                                                                                      <block type="expression" id="0k).)gOwz0]j3fQFYZ//">
                                                                                                                                        <field name="value">2</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="text_shape" id="jq-Hgor2{PgTEg^RO[)3">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_position" id="H]RqB]04XTEi^gG68a[-">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="bo_)L+1CNM?VMW{#szQW">
                                                                                                                    <field name="value">200</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="sW)Q~nwmJa8R6glr+EUj">
                                                                                                                    <field name="value">125</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_contents" id="#^)@2DdqLc1X|$w@2tdwwA">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="contents">
                                                                                                                      <block type="string_value" id="USI(hE!SPH;%YZVBHU;!">
                                                                                                                        <field name="value">${a1} ${b}</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style" id="ukk;GmcBnw_XWH^n2f[o">
                                                                                                                        <field name="base">text</field>
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_text_style_font_size" id="OGX6v|sp3-f%9I2t]@1,)">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="fontSize">
                                                                                                                              <block type="expression" id="7v8CJzbVtR})~LKmlR9O">
                                                                                                                                <field name="value">32</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_fill" id="}u_X@1tI1oC4-=!D:)k2S">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="fill">
                                                                                                                                  <block type="string_value" id="2qG6,GT)pCaJ[aj)(-@24">
                                                                                                                                    <field name="value">black</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_stroke" id="bMd)iySC5(:(JE;z]Kak">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="stroke">
                                                                                                                                      <block type="string_value" id="Td0Tmr(A/x!)&#9;a60=a">
                                                                                                                                        <field name="value">white</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_stroke_thickness" id=")AW({JmWbsA~Ug(Vy$+E">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="strokeThickness">
                                                                                                                                          <block type="expression" id="CKK9qF`ghv~h_8aPH)S:">
                                                                                                                                            <field name="value">2</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="text_shape" id="8.@2m52tj{O/HU#j}v;TI">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_position" id="[jJN=@1IRE4nb,IslsiHb">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="Uhz5kYOv6U1w~:SE!H{~">
                                                                                                                        <field name="value">300</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="bh#s@2W`sn`}zVf}c5QLT">
                                                                                                                        <field name="value">280</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_contents" id="4(9XbFosgFO^Ds+S1eE:">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="contents">
                                                                                                                          <block type="string_value" id="j;8Jqz|ZwYa9W!p@2A0^Q">
                                                                                                                            <field name="value">${a} ${b}</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style" id="iZ1Ui@2m0lDwf;stfRoAL">
                                                                                                                            <field name="base">text</field>
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_text_style_font_size" id="VNRdlS{Y72~Tpa;]dcs!">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="fontSize">
                                                                                                                                  <block type="expression" id="_oZy]$jLdV)]!as?/ur;">
                                                                                                                                    <field name="value">32</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_fill" id="gkz.vB!nv_NMoeh]@2j`x">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="fill">
                                                                                                                                      <block type="string_value" id="^ruNCy,zDuSOu;pp@1yG~">
                                                                                                                                        <field name="value">black</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_stroke" id="qLg(fhA{s]Yqy@1#[/SZQ">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="stroke">
                                                                                                                                          <block type="string_value" id="U5brpHt+OpIdlYXFRzG=">
                                                                                                                                            <field name="value">white</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style_stroke_thickness" id="7MDwcC#(x)uD/ug(I%A)">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="strokeThickness">
                                                                                                                                              <block type="expression" id="$a,lD|A[.y,piZHw-#|v">
                                                                                                                                                <field name="value">2</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="text_shape" id="#2_))mr!nPTYRp$S)kp0">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_position" id="amXo|K^n5~zTy+B5`)_T">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="r-Uf5(ChQ_2L]1|~@2+Vz">
                                                                                                                            <field name="value">100</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="vtf{%{C{l#KRjkoBN=Gh">
                                                                                                                            <field name="value">280</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_contents" id="[6/Q^q:tQzOP(Z7=i#),">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="contents">
                                                                                                                              <block type="string_value" id=";+YGJ=_1rMqJ_,[5MZJ]">
                                                                                                                                <field name="value">${a1} ${b}</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style" id="SNY]Tgks9v%M6a]YK7fy">
                                                                                                                                <field name="base">text</field>
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_text_style_font_size" id="U/CmMPzZH3(gbVwnBJsg">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="fontSize">
                                                                                                                                      <block type="expression" id="p0CHJTZFMi7KoJK^@1@24G">
                                                                                                                                        <field name="value">32</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_fill" id="bJpgHhi2PEH,n$T]bPs5">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="fill">
                                                                                                                                          <block type="string_value" id="-O}KKvqB?elQuF@1DCZX)">
                                                                                                                                            <field name="value">black</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style_stroke" id="DuoebsFmSj)d(6_G6yfV">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="stroke">
                                                                                                                                              <block type="string_value" id="Y~8Z^_Z++^2uNbaAC7gD">
                                                                                                                                                <field name="value">white</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_text_style_stroke_thickness" id="TnzIWMLA9kqZ;BZo({W`">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="strokeThickness">
                                                                                                                                                  <block type="expression" id="}9i5vVZ=Vumub7ZGX|.A">
                                                                                                                                                    <field name="value">2</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="text_shape" id="Cyd%W;VfNRBmTdE7kcU8">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="xJ6ퟨ=zkKXH,S$G`z">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="UmLuyXGu1~cY4Enc%Yu|">
                                                                                                                                <field name="value">100</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="bL1HjB$pW??@1sT2I8B+C">
                                                                                                                                <field name="value">180</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_contents" id="E1d}ql!0olw={)btdd2o">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="contents">
                                                                                                                                  <block type="string_value" id="7cPPRn:)sx~F-Z3RjZMY">
                                                                                                                                    <field name="value">${a1} ${b}</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style" id="-4Dfxha57SqYc`Ay6[}g">
                                                                                                                                    <field name="base">text</field>
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_text_style_font_size" id="7ud8)T55d$v,KhMpcN,A">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="fontSize">
                                                                                                                                          <block type="expression" id="r+woaj]b,g+.oIgQr=0S">
                                                                                                                                            <field name="value">32</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style_fill" id="QScHqDfC:Jlb7ALFog@1b">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="fill">
                                                                                                                                              <block type="string_value" id="oH@2nOR%.R^}TZ?0Ho`XF">
                                                                                                                                                <field name="value">black</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_text_style_stroke" id="xGSdNhl[NPNl!(W_^:{5">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="stroke">
                                                                                                                                                  <block type="string_value" id="yod()yuoUe3k[Zh$TP]E">
                                                                                                                                                    <field name="value">white</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_text_style_stroke_thickness" id="mEg(BKwA]To7!~;%_t1X">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="strokeThickness">
                                                                                                                                                      <block type="expression" id="8rS~kMmR-}R1?Hh8AyNd">
                                                                                                                                                        <field name="value">2</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="text_shape" id="1aDw(,J/TJ13zo}MnMeH">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="vdCYmHIX8N`[dD;9};sC">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="dH5C87C9w_SODV;{-BbP">
                                                                                                                                    <field name="value">300</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="o+||!lX@1?t$3FMJk`EBy">
                                                                                                                                    <field name="value">180</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_contents" id="2zX1iYnyD!^X+V)?2r!%">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="contents">
                                                                                                                                      <block type="string_value" id="YT-+]u@1o0#6e[dKJ#Zx~">
                                                                                                                                        <field name="value">${a} ${b}</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style" id="jlhJzBALgGCV0bEZU.m4">
                                                                                                                                        <field name="base">text</field>
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_text_style_font_size" id="x93a)-RND52L@2PzoM5l0">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="fontSize">
                                                                                                                                              <block type="expression" id="{f)R@2QxU-J@1#yYR69Ti[">
                                                                                                                                                <field name="value">32</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_text_style_fill" id="Uc:1S-woHjH_@1|)wMR$M">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="fill">
                                                                                                                                                  <block type="string_value" id="]k߱ot8^Vgy@1,wc~KQ">
                                                                                                                                                    <field name="value">black</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_text_style_stroke" id="z]t@1Uh]#v$XmiVo3o_p6">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="stroke">
                                                                                                                                                      <block type="string_value" id="#D`K7~-@1BE(@1HQ4+j59z">
                                                                                                                                                        <field name="value">white</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="u[TfA]:72ADn?D-!A0Qs">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="strokeThickness">
                                                                                                                                                          <block type="expression" id="!olnH6|hg+9.+w@2wfoVe">
                                                                                                                                                            <field name="value">2</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="if_then_block" id="-5?RQ~w.9iI$`~i6V/:^">
                                                                                                    <value name="if">
                                                                                                      <block type="expression" id="N1o!Bw8UvltF_)rvmKBh">
                                                                                                        <field name="value">c == 'octagon'</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <statement name="then">
                                                                                                      <block type="image_shape" id="y]Zyw!!kSR]pV/v(Ro3N">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_position" id="aH/NIz02?)5om/$9pPJs">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="2Zz(wozKeGJ,ktbvW7lK">
                                                                                                                <field name="value">200</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id=":amB{;?gA-=32t.3HOG0">
                                                                                                                <field name="value">225</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_size" id="I.%l9RLW#ALU.8`d#/hF">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="width">
                                                                                                                  <block type="expression" id="C/kdI5a43xfTWZN-souq">
                                                                                                                    <field name="value">0</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="height">
                                                                                                                  <block type="expression" id="eJ7pA7J9|.KWTSwN[IXX">
                                                                                                                    <field name="value">0</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_image_key" id="(?TIXWYDr,BI(M6dca,W">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="key">
                                                                                                                      <block type="string_value" id="$bXeebuURoup6O-jHz`%">
                                                                                                                        <field name="value">2octagon${item}.png</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_src" id=":A.;mX-1RAnP4QM0;=YG">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="src">
                                                                                                                          <block type="string_value" id="cO#O4-m{L#r-.7ovc!r2">
                                                                                                                            <field name="value">${image_path}2octagon${item}.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="text_shape" id="eonYt(%l[Z$+S{azrQS#">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_position" id="vO|T?AUIon?@1fWMFa9ym">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="J%?1w/BZZOJ)X2|bTA8D">
                                                                                                                    <field name="value">200</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="(WLVSH(z`#)Bb+Cv9X2t">
                                                                                                                    <field name="value">325</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_contents" id="VPaTABE`_htn%$#6?j/f">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="contents">
                                                                                                                      <block type="string_value" id="vm0{;KW2ilS2FRs:m3$k">
                                                                                                                        <field name="value">${a1} ${b}</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style" id="~b/2)6R-}F6@2v67Ln[[F">
                                                                                                                        <field name="base">text</field>
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_text_style_font_size" id=")ȭ=xWMui,x|V1G0fa">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="fontSize">
                                                                                                                              <block type="expression" id="67,qER$WlB]E0U%Uz32U">
                                                                                                                                <field name="value">32</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_fill" id="Orhx}CJ3R|]DQM3x^^Q]">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="fill">
                                                                                                                                  <block type="string_value" id="1LC9jv?k^e}JgEb6OeHv">
                                                                                                                                    <field name="value">black</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_stroke" id="8m4:Kklpq:u`Rgz`y;Bw">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="stroke">
                                                                                                                                      <block type="string_value" id="THq/1Ct80goG^s@11,W96">
                                                                                                                                        <field name="value">white</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="tkLr+DELRRUL83:A4YDp">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="strokeThickness">
                                                                                                                                          <block type="expression" id=",WDR8W?IWg:dHKUYZxbm">
                                                                                                                                            <field name="value">2</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="text_shape" id="DC?K2YR`EI|`A?eFZUD1">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_position" id="@1B%OR_;YX389GN61==,h">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="Ua}f#FFHl@1AfC7P4o[aV">
                                                                                                                        <field name="value">200</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="tR(]jIuTE?z|9q~8wxkl">
                                                                                                                        <field name="value">125</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_contents" id="N)/)7_!A:sTTM`P:n=+}">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="contents">
                                                                                                                          <block type="string_value" id="NbJ?0[E#e/q!oyr3P{`i">
                                                                                                                            <field name="value">${a1} ${b}</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style" id=":$4HcT:6iSGkGcL.R/c.">
                                                                                                                            <field name="base">text</field>
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_text_style_font_size" id="#H^{]9q-}51DON2ctrCv">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="fontSize">
                                                                                                                                  <block type="expression" id="H,Iutgtd(u!;t[(z($T|">
                                                                                                                                    <field name="value">32</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_fill" id="8l4F|=cv5wj84Hf+G)Ns">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="fill">
                                                                                                                                      <block type="string_value" id=")W#q5{VY-F3j_`l.vq43">
                                                                                                                                        <field name="value">black</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_stroke" id="XbLXn4X,/9DR,%0y6si^">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="stroke">
                                                                                                                                          <block type="string_value" id="DFoK36tP[M%Dtn+g,sO2">
                                                                                                                                            <field name="value">white</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style_stroke_thickness" id="86[DPQ3?,.GGzmLvW=g-">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="strokeThickness">
                                                                                                                                              <block type="expression" id="{cPWssSZ/TB-Y;90V_w:">
                                                                                                                                                <field name="value">2</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="text_shape" id="OE)n0iGSM70lR,/](Scn">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_position" id="~j`EvoJxtR[v,v7sBHwn">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="sJ8){UZ;/#Nr]`ZY~|u$">
                                                                                                                            <field name="value">300</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="LM#FdFigm]}vzLXo2:OV">
                                                                                                                            <field name="value">290</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_contents" id="=;4]sR9[jun42MA%ri-c">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="contents">
                                                                                                                              <block type="string_value" id="5KH8/3kWZhH-$mpE;Hx[">
                                                                                                                                <field name="value">${a1} ${b}</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style" id="G(_bS:_/_5w!k_6qbV%b">
                                                                                                                                <field name="base">text</field>
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_text_style_font_size" id="LmWDZA|G.n$,3W-Q09mY">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="fontSize">
                                                                                                                                      <block type="expression" id=",1,@1UpzF:0HqAl_K{|l`">
                                                                                                                                        <field name="value">32</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_fill" id="iO?p;%H@2voGA{.~^-3)d">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="fill">
                                                                                                                                          <block type="string_value" id="M1Y;C[eVxScIkkZwqlhV">
                                                                                                                                            <field name="value">black</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style_stroke" id=".K)4WALbS~J/tw03Wno5">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="stroke">
                                                                                                                                              <block type="string_value" id="8jVum!O2O]3{mLH%iE">
                                                                                                                                                <field name="value">white</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_text_style_stroke_thickness" id="=P(3NM}Q]a@2@2OBUA3`@1e">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="strokeThickness">
                                                                                                                                                  <block type="expression" id="Q(v}#Z^.%wi00y`sUo68">
                                                                                                                                                    <field name="value">2</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="text_shape" id="tDisWdY!bx@1^?SzHNM%{">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="ye:4?oO/_UT=}jZ@1aFQD">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="V%tBg^^MIrBNbJ%0u{Vy">
                                                                                                                                <field name="value">100</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="MBu@2^z1M1eG@1:^,_W]NC">
                                                                                                                                <field name="value">290</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_contents" id="I5ooW,NT}M#+q|m1@2#:a">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="contents">
                                                                                                                                  <block type="string_value" id="=vd17R|+R#[Q~-S1|(5x">
                                                                                                                                    <field name="value">${a} ${b}</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style" id="@1TO+xmcE`dh()[Uxj~9v">
                                                                                                                                    <field name="base">text</field>
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_text_style_font_size" id="12cc9PdB2oXVr)a9nF%[">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="fontSize">
                                                                                                                                          <block type="expression" id="CK$=6eyR|$Du@26u%}7)%">
                                                                                                                                            <field name="value">32</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style_fill" id="/e.Z3XH|B{nxstaLv7oU">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="fill">
                                                                                                                                              <block type="string_value" id="c8A8mrKhdFoIv9}Qb^=;">
                                                                                                                                                <field name="value">black</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_text_style_stroke" id="w/ND3[{+[LjXjqietJkk">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="stroke">
                                                                                                                                                  <block type="string_value" id="2g|g5HI+fL1rcqryW{Vf">
                                                                                                                                                    <field name="value">white</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_text_style_stroke_thickness" id="uFOBfeb@1[7esN_fPsnTV">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="strokeThickness">
                                                                                                                                                      <block type="expression" id="be)?or{U8bELblnAY1zs">
                                                                                                                                                        <field name="value">2</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="text_shape" id="M[OW%@1lu~z]pH/n=,2+x">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="O3yV[SCwYhB|?lrW:J9[">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="n^}~sSARvkJVInso:+m]">
                                                                                                                                    <field name="value">100</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="]bsk-Squt{~v+V1SV7.Z">
                                                                                                                                    <field name="value">170</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_contents" id="UU#W(e:VZN6i|@2mzx)kF">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="contents">
                                                                                                                                      <block type="string_value" id="zwk#HAJ5H#Qm|O|mWEyN">
                                                                                                                                        <field name="value">${a} ${b}</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style" id="cI{PqZ#Ba+fu?5.V[6I.">
                                                                                                                                        <field name="base">text</field>
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_text_style_font_size" id="$R[q5R9ds6Of-{G#0HD$">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="fontSize">
                                                                                                                                              <block type="expression" id="1VS.^FqE=qp1#?cO~.(Z">
                                                                                                                                                <field name="value">32</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_text_style_fill" id="TaMz2=8%Y1m[p]@2B4j1@2">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="fill">
                                                                                                                                                  <block type="string_value" id="4:2R]zD)Vf7p-h{uHrO%">
                                                                                                                                                    <field name="value">black</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_text_style_stroke" id="M)N7-z=@2r:mW:xg?%I}h">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="stroke">
                                                                                                                                                      <block type="string_value" id="fxV+f/P3:kItz`dCvoje">
                                                                                                                                                        <field name="value">white</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="Qyv,=dEbJFi0!$-vjAgi">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="strokeThickness">
                                                                                                                                                          <block type="expression" id="xk?3Z^MYB9%-zzT1@2}yv">
                                                                                                                                                            <field name="value">2</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="text_shape" id=";9V[1_@141hZ@2tQ]/NJ6e">
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_position" id="we,WOpKLv37Fdd=g9;5$">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="x">
                                                                                                                                      <block type="expression" id="dL+y3W@1OqWil}cyd@2w0d">
                                                                                                                                        <field name="value">300</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="y">
                                                                                                                                      <block type="expression" id="VA$34/Mji[x3In8@2?hD2">
                                                                                                                                        <field name="value">170</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_contents" id="r8b^H1[,np[~5K8Ld)J:">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="contents">
                                                                                                                                          <block type="string_value" id="0(Dl_K:vYqCb}9|?N:i6">
                                                                                                                                            <field name="value">${a} ${b}</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style" id="aKhU~@1zJK[_XJq(}iwL{">
                                                                                                                                            <field name="base">text</field>
                                                                                                                                            <statement name="#props">
                                                                                                                                              <block type="prop_text_style_font_size" id="xI6T,.oFr)[d_Gp=+NZi">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="fontSize">
                                                                                                                                                  <block type="expression" id=",HsDelrjO]4xP^RE3#I:">
                                                                                                                                                    <field name="value">32</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_text_style_fill" id="J%ic5sv,#?3De%Pxucm]">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="fill">
                                                                                                                                                      <block type="string_value" id="Tbk%ny4~oBiTw~YN=1xu">
                                                                                                                                                        <field name="value">black</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_text_style_stroke" id="6nalf7|O@1Fj6alnl|dS|">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="stroke">
                                                                                                                                                          <block type="string_value" id="CC.^1Adpxjn:Z?~Q?lxx">
                                                                                                                                                            <field name="value">white</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_text_style_stroke_thickness" id="P2HroK%J_=[YugiI%H^[">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="strokeThickness">
                                                                                                                                                              <block type="expression" id="4blfj|26jjkoc/q9TE7j">
                                                                                                                                                                <field name="value">2</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <next>
                                                                                                                                  <block type="text_shape" id="nVk,So.~@2re7$l^%?:(T">
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_position" id="m6%kK.t.PsR@2njWZR%,4">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="expression" id="$Tat)enVnmR%2o@1j0D{_">
                                                                                                                                            <field name="value">80</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id="(|:i2|bL|2:LjCq8)h6j">
                                                                                                                                            <field name="value">225</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_contents" id="aMaRD/I8E|uCN]eXAFx2">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="contents">
                                                                                                                                              <block type="string_value" id="Jwj0r?4-t.kgdSBwu8B5">
                                                                                                                                                <field name="value">${a} ${b}</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_text_style" id="Jp(NI/mk`7I.[eEn{fna">
                                                                                                                                                <field name="base">text</field>
                                                                                                                                                <statement name="#props">
                                                                                                                                                  <block type="prop_text_style_font_size" id="QD@2JYI[-r3U0a9bmtt`0">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="fontSize">
                                                                                                                                                      <block type="expression" id="i6+jEp]};1p;_sr@1.6eY">
                                                                                                                                                        <field name="value">32</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_text_style_fill" id="z6ad4]YXN6RV@1HcSk@2Hk">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="fill">
                                                                                                                                                          <block type="string_value" id="P~%(3zYN!04^h2c+u)t=">
                                                                                                                                                            <field name="value">black</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_text_style_stroke" id="7X)VS!f^%6mjR7NoR`1!">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="stroke">
                                                                                                                                                              <block type="string_value" id="Bvhctx8M5;%7wNT$V|W8">
                                                                                                                                                                <field name="value">white</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_text_style_stroke_thickness" id="q-oGb7,rc~-y7-ej;5+J">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="strokeThickness">
                                                                                                                                                                  <block type="expression" id="_?@2mb,b#gROJ-xMCwAZv">
                                                                                                                                                                    <field name="value">2</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="text_shape" id="])`7KM%-uMur}5sgIxps">
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_position" id="O2S5@1(Yw=WwN(|Z_7B}(">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id="?73HXzr8T2Kn45r8^_II">
                                                                                                                                                <field name="value">325</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="||~fb1)S{_s@1(|;.jGF[">
                                                                                                                                                <field name="value">225</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_text_contents" id="jnvvDAtOy?l?I7+D)$hM">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="contents">
                                                                                                                                                  <block type="string_value" id="v$}jS64I+d{4bH}m4?.V">
                                                                                                                                                    <field name="value">${a} ${b}</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_text_style" id="bE9^jh5pChnde@2r2qa#:">
                                                                                                                                                    <field name="base">text</field>
                                                                                                                                                    <statement name="#props">
                                                                                                                                                      <block type="prop_text_style_font_size" id=")@2bsb_Rv5!)eM8$pR(R`">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="fontSize">
                                                                                                                                                          <block type="expression" id="aPqi1H?Ao9yK3V9DDZFx">
                                                                                                                                                            <field name="value">32</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_text_style_fill" id="l(}d1^JlEe$_.Ta[PfXG">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="fill">
                                                                                                                                                              <block type="string_value" id="e3)0}?iX^FG;?-.H5vfy">
                                                                                                                                                                <field name="value">black</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_text_style_stroke" id="+n@2|eZX3+ar7_]Dtn{jR">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="stroke">
                                                                                                                                                                  <block type="string_value" id="f8MnS/cd7ROq/@2C[eGxV">
                                                                                                                                                                    <field name="value">white</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_text_style_stroke_thickness" id="z5^f/;Ldac@2?f@1@2d@1)W]">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="strokeThickness">
                                                                                                                                                                      <block type="expression" id="+xR0_)jp0BM6EA]d!VrM">
                                                                                                                                                                        <field name="value">2</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="if_then_block" id="m9CS5@2?-C^(uM}TUu7A@1">
                                                                        <value name="if">
                                                                          <block type="expression" id="[(w`M@1?;pBsBFa4N-N^Y">
                                                                            <field name="value">show == 2</field>
                                                                          </block>
                                                                        </value>
                                                                        <statement name="then">
                                                                          <block type="if_then_block" id="@1CmhJ=}T3C%/fS8U+X%r">
                                                                            <value name="if">
                                                                              <block type="expression" id="B7}Ov=Loxylr,@2D01JFi">
                                                                                <field name="value">c == 'triangle'</field>
                                                                              </block>
                                                                            </value>
                                                                            <statement name="then">
                                                                              <block type="image_shape" id="cdX@1d5=Fv_ftv}]8N!X%">
                                                                                <statement name="#props">
                                                                                  <block type="prop_position" id="73,K+m)qjGlFzPQ=/y~x">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="E]yb(i+p[ml(#5Tyi$P`">
                                                                                        <field name="value">200</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="AAsuq!U4ctYkyx6DpGV7">
                                                                                        <field name="value">225</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_size" id="H+,l^x:@1kQLpDIs8;`:-">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="width">
                                                                                          <block type="expression" id="`09{4K&gt;bnh$ngboC^8">
                                                                                            <field name="value">0</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="height">
                                                                                          <block type="expression" id="pr|S{R_^tx@1I@29y5I;M}">
                                                                                            <field name="value">0</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_image_key" id=")A46B6/h4,kJ|X:]Py9v">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="key">
                                                                                              <block type="string_value" id="RfPy@2]i).jy)Mso?K)T=">
                                                                                                <field name="value">1triangle${item}.png</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_image_src" id=")g3itIdv?MD!(;Av06RA">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="src">
                                                                                                  <block type="string_value" id="z7_$+XH,+w}DlM_AYwj)">
                                                                                                    <field name="value">${image_path}1triangle${item}.png</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="text_shape" id="-N$p63.Lwo+f,}R])F}C">
                                                                                    <statement name="#props">
                                                                                      <block type="prop_position" id="|RJ(-XGgF#PNiI9BHvvf">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="f[BUP0A0?{SM|r]f96bj">
                                                                                            <field name="value">200</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="5Wxh$Sz5LhdYD45Q.suQ">
                                                                                            <field name="value">315</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_contents" id="}L$%7]b-V[Mt];F!2rrW">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="contents">
                                                                                              <block type="string_value" id="@1q6yQ7Wbd~SCeA$LkR=-">
                                                                                                <field name="value">${a} ${b}</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_style" id="@1Zn9S2eb2$$a6@1kQgvb;">
                                                                                                <field name="base">text</field>
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_text_style_font_size" id="_sktCPM!`V@1(u5hcCvwy">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="fontSize">
                                                                                                      <block type="expression" id="XvhQU6e;!~`-7_d~lf:_">
                                                                                                        <field name="value">32</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style_fill" id="Chons)hMvUPGN)-qmCW!">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="fill">
                                                                                                          <block type="string_value" id="[drjWMlo%YL9V]mMRL^r">
                                                                                                            <field name="value">black</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style_stroke" id="$lRcalWWJ2Vd7;+]wPmE">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="stroke">
                                                                                                              <block type="string_value" id="btSEvbR^BlGL!RCq$Jt4">
                                                                                                                <field name="value">white</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_stroke_thickness" id="nC}wi6[Lv;@2oR;_hw/df">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="strokeThickness">
                                                                                                                  <block type="expression" id="m/FOsOlImV,Tey!F)a%M">
                                                                                                                    <field name="value">2</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="if_then_block" id="n)BXnc!NzRnKwtinVB|S">
                                                                                <value name="if">
                                                                                  <block type="expression" id="{,?K1gsYo;D,h=jLEf8P">
                                                                                    <field name="value">c == 'square'</field>
                                                                                  </block>
                                                                                </value>
                                                                                <statement name="then">
                                                                                  <block type="image_shape" id="wqu@28g1?Em2Z@2DTxX0b~">
                                                                                    <statement name="#props">
                                                                                      <block type="prop_position" id="/k:SJi=Z,jzMjSr.@1SA}">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="7m1c^2f.nj2Uf-L_ih~@2">
                                                                                            <field name="value">200</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="J?3fpDI-gR;:k4@1k3)0M">
                                                                                            <field name="value">225</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_size" id="@1pQ)#RiLjTWQn8{NXqh,">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="width">
                                                                                              <block type="expression" id="c).y~AaQ8MGZRFL#@29L#">
                                                                                                <field name="value">0</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="height">
                                                                                              <block type="expression" id="eKNc};$RjlSSj-3?~N(v">
                                                                                                <field name="value">0</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_image_key" id="VmJ:-|[n@1~pnm0mNZ`Mk">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="key">
                                                                                                  <block type="string_value" id="Gm3xMwZ0LzYj0watrVDV">
                                                                                                    <field name="value">1square${item}.png</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_image_src" id="Y84p2T,fY9kE}_DV,!=1">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="src">
                                                                                                      <block type="string_value" id="Jyk;ywvEK:QueAXm~sLP">
                                                                                                        <field name="value">${image_path}1square${item}.png</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <next>
                                                                                      <block type="text_shape" id="#$@2;l-]gLD]J80zy{4`p">
                                                                                        <statement name="#props">
                                                                                          <block type="prop_position" id="om]?_2hxn]@28UA==hX7,">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="#,$IoL0MvO`v=gf_GCkb">
                                                                                                <field name="value">200</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="Y~JD,=(AxSD1ZqdMmqs:">
                                                                                                <field name="value">325</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_contents" id="lOFJZL3fw0wpaQ[jO$3E">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="contents">
                                                                                                  <block type="string_value" id="VK1W[3juqgpbsvs(;0Lt">
                                                                                                    <field name="value">${a} ${b}</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style" id="jfLo4J[AFzYJT!7ydF$S">
                                                                                                    <field name="base">text</field>
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_text_style_font_size" id=";%_$xoeQ4TMsS=ue?qY2">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="fontSize">
                                                                                                          <block type="expression" id="#yr9@10idUn]vmTaX%@23U">
                                                                                                            <field name="value">32</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style_fill" id="9`ig6J|krRIVLGT{0.5T">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="fill">
                                                                                                              <block type="string_value" id="L)sO{/MU[oVMJ9U#h)J@1">
                                                                                                                <field name="value">black</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_stroke" id="w7+#A#X5D)DW]O-h#P0:">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="stroke">
                                                                                                                  <block type="string_value" id=")~D.CTtZiI=P3{EBE2Vy">
                                                                                                                    <field name="value">white</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_stroke_thickness" id="rJNfGdkPC9,pg.u#6g~-">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="strokeThickness">
                                                                                                                      <block type="expression" id="=c;-T0Yxi+=#jKN2yabN">
                                                                                                                        <field name="value">2</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="if_then_block" id="(eBq;`:FL+gxsz#.B6uz">
                                                                                    <value name="if">
                                                                                      <block type="expression" id="hm1Yi7w50g`Qer,p7?{x">
                                                                                        <field name="value">c == 'pentagon'</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <statement name="then">
                                                                                      <block type="image_shape" id="xoyvsft.+I4q^(nOt9Bg">
                                                                                        <statement name="#props">
                                                                                          <block type="prop_position" id="h~K4G=!V@1-e$mO^{HY@19">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="JEl-T,%i(+`(4PzXl9}P">
                                                                                                <field name="value">200</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id=";dRjhZR@1W6{:#`VkfA$V">
                                                                                                <field name="value">225</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_size" id="OapQ[wPk{FM:{aHi@2}6E">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="width">
                                                                                                  <block type="expression" id="efyy%]]uRxPOs]FMXX3=">
                                                                                                    <field name="value">0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="height">
                                                                                                  <block type="expression" id="/j5xP6dk9h;}#/@2xJok]">
                                                                                                    <field name="value">0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_image_key" id="t]~nxG8ge61WAU~@24-m#">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="key">
                                                                                                      <block type="string_value" id="nG=~-uWWta2b+IjLC3z`">
                                                                                                        <field name="value">1pentagon${item}.png</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_image_src" id="K]+8a:I_]c[fCmv?!FlX">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="src">
                                                                                                          <block type="string_value" id="tsR=XG6z}jZ_fFnXbODQ">
                                                                                                            <field name="value">${image_path}1pentagon${item}.png</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="text_shape" id="T/Vv5.m+lKM;@1Dl}T!rL">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_position" id="r#9H8l=%jGv|1|~P.kfR">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="?7BRaw7OzDbNI=IUM2tj">
                                                                                                    <field name="value">200</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="fzSh%|T}Ao83eL@2-Ct/(">
                                                                                                    <field name="value">325</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_contents" id="0bM(!4(?z%9;M78kPCZj">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="contents">
                                                                                                      <block type="string_value" id="8E/[?hrd]`W4ft=[|R((">
                                                                                                        <field name="value">${a} ${b}</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style" id="@2?)2{i!@16TB!d[R5zs?p">
                                                                                                        <field name="base">text</field>
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_text_style_font_size" id=".${XLpizoAf}{GZfQRH-">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="fontSize">
                                                                                                              <block type="expression" id="IFUv1@1JWvvCl={YMe,MG">
                                                                                                                <field name="value">32</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_fill" id="~u.1L{?}Gw6ZJ#M:.KCs">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="fill">
                                                                                                                  <block type="string_value" id="o:8k$XbTL|@1AyUwOiwqn">
                                                                                                                    <field name="value">black</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_stroke" id="rEn]yVs7W!fKzr7%p?@1d">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="stroke">
                                                                                                                      <block type="string_value" id="=uKz#8Z1b}o!a=gQ%u#3">
                                                                                                                        <field name="value">white</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_stroke_thickness" id="h4rh`g%rbbOuE47)n[rv">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="strokeThickness">
                                                                                                                          <block type="expression" id="R$:SNn`Z?=XL]liND)q%">
                                                                                                                            <field name="value">2</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <next>
                                                                                      <block type="if_then_block" id="|@2)j/|ZK8yw.7!Tl8d_$">
                                                                                        <value name="if">
                                                                                          <block type="expression" id="j}g:4FPUv634{o-YNDh=">
                                                                                            <field name="value">c == 'hexagon'</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <statement name="then">
                                                                                          <block type="image_shape" id="JpFp+UjIbgf:DhJM|-T=">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_position" id="N)M1Rs._z@1tzZ^+@1@2}co">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="@1m0bynfc]za;5L+V2pwT">
                                                                                                    <field name="value">200</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="FrDQ=|D{5-6{%@1?Gz%P^">
                                                                                                    <field name="value">225</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_size" id=";roq:Z}atmrj!`Mc($_y">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="width">
                                                                                                      <block type="expression" id="cy)4r^Wv=+Fv1hN=?Z^q">
                                                                                                        <field name="value">0</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="height">
                                                                                                      <block type="expression" id="8;}./%JL@2p4!eK48Bbix">
                                                                                                        <field name="value">0</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_image_key" id="v[j@2_.+bS5BZqI|Lg/WN">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="key">
                                                                                                          <block type="string_value" id="Hs:LGa0-7_3G}GJ]l+%F">
                                                                                                            <field name="value">1hexagon${item}.png</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_image_src" id="^@2tp%0-[Ezeen3~,0[Za">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="src">
                                                                                                              <block type="string_value" id=")HA.gnTvpY%|X7H%of@1#">
                                                                                                                <field name="value">${image_path}1hexagon${item}.png</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="text_shape" id="Fp]_@12XKI5Y`pXwb^f{~">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_position" id="%L?ErhVYGvi-ZbA-]2Y8">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="]x=kkHh`3m9Z=m1jm?Tj">
                                                                                                        <field name="value">200</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="]3-r%(R[J0u@1!}.(O8o0">
                                                                                                        <field name="value">325</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_contents" id="Mj0:P(al@29E@219,ZR^?0">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="contents">
                                                                                                          <block type="string_value" id="3~c^m[j[2{ngn{Hw-$Lq">
                                                                                                            <field name="value">${a} ${b}</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style" id="E{(;F|V5!aH%7Y6U{p@2S">
                                                                                                            <field name="base">text</field>
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_text_style_font_size" id="b]N+/ki49h}ZO/=tW1@1,">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="fontSize">
                                                                                                                  <block type="expression" id="X^uX9wM/c=-IRy$bJHl0">
                                                                                                                    <field name="value">32</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_fill" id="ASLn5V=?oP699ykj%;8%">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="fill">
                                                                                                                      <block type="string_value" id="et8%ȹjezV0s_?PQ_m">
                                                                                                                        <field name="value">black</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_stroke" id="|ts9|yN8@1D(8oJb.H,Tr">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="stroke">
                                                                                                                          <block type="string_value" id="1!Lw^c(Y$36tM~HIiyUH">
                                                                                                                            <field name="value">white</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_stroke_thickness" id="zF-:1x%[^;l!UxK=%9@2Y">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="strokeThickness">
                                                                                                                              <block type="expression" id="+5@10p3V5RwxnJ(KGbPUc">
                                                                                                                                <field name="value">2</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="if_then_block" id="YXI/@2UC(bd2~Yu/pj{-s">
                                                                                            <value name="if">
                                                                                              <block type="expression" id="{G?eTr`fxDOD5Lg/pK)U">
                                                                                                <field name="value">c == 'octagon'</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <statement name="then">
                                                                                              <block type="image_shape" id="@2Bm?.@2_m4URVAz@1R?/nA">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_position" id="L;vSLn3TxwRV0?d1_vSb">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id=";L}6R@1aGMBbbL|t?@1v,`">
                                                                                                        <field name="value">200</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="K{qqhpoE:0kA.b=DJJjF">
                                                                                                        <field name="value">225</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_size" id="_jaMu8~4DBSlyL!:#TZ0">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="width">
                                                                                                          <block type="expression" id="z9iI;zWijpA2m=`F@1NgY">
                                                                                                            <field name="value">0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="height">
                                                                                                          <block type="expression" id="v(xUQ9l4UW!EaSIWeP/8">
                                                                                                            <field name="value">0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_image_key" id="4oNJSQlg7v~zx+xGR=^G">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="key">
                                                                                                              <block type="string_value" id="2K#2~NBYXjK܀1F`]yb">
                                                                                                                <field name="value">1octagon${item}.png</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_image_src" id="aY3pd:v_B`|eVn~1/YDa">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="src">
                                                                                                                  <block type="string_value" id="/VRNOmlhSBK@1gNxJRMA-">
                                                                                                                    <field name="value">${image_path}1octagon${item}.png</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="text_shape" id="8_shyfBv?3d,?1kH3i0m">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_position" id="q!To{bI%at-$+.VvXF$Q">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="x">
                                                                                                          <block type="expression" id="O3|1Ttaa#C|jZL1G(l2r">
                                                                                                            <field name="value">200</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id="tm$dC_!POAj/Pd(nwWl$">
                                                                                                            <field name="value">325</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_contents" id="+hPDo+U^hhx03.#AbOA6">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="contents">
                                                                                                              <block type="string_value" id="iEIMhrIr^xDas6m@2N%,;">
                                                                                                                <field name="value">${a} ${b}</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style" id="@1W7bl=jXsGd/yFw;ME{$">
                                                                                                                <field name="base">text</field>
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_text_style_font_size" id="1ٔ/A@1|o[yr[YwcM]C">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="fontSize">
                                                                                                                      <block type="expression" id="-s=q,:FAc(md8EMKM%@2{">
                                                                                                                        <field name="value">32</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_fill" id="Q/AkO1FLEa9;8:YI1SmE">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="fill">
                                                                                                                          <block type="string_value" id="I;H)(#m?)M4;60yjvj(a">
                                                                                                                            <field name="value">black</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_stroke" id="DIvAd0m1Ku|eOd6=(~uW">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="stroke">
                                                                                                                              <block type="string_value" id="`Z5`ETz7NMO4OM~x,cU4">
                                                                                                                                <field name="value">white</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_stroke_thickness" id="F_fcXfNqKr._,@131H^ui">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="strokeThickness">
                                                                                                                                  <block type="expression" id="F$ObdaU6^A^Y?{LLoBCd">
                                                                                                                                    <field name="value">2</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="if_then_block" id="jw{;x,Rc9a[G~uzva">
                                                                            <value name="if">
                                                                              <block type="expression" id="rr58u._EP}%|-hf^~mTx">
                                                                                <field name="value">answer == 1</field>
                                                                              </block>
                                                                            </value>
                                                                            <statement name="then">
                                                                              <block type="list_shape" id="uj5f`kLKD?MnBLHpGkG{">
                                                                                <statement name="#props">
                                                                                  <block type="prop_list_direction" id="K,1iD@2NoCs.3c!j:Bw5s">
                                                                                    <field name="#prop"></field>
                                                                                    <field name="dir">horizontal</field>
                                                                                    <next>
                                                                                      <block type="prop_position" id="P3ptwgtD|aKK0K/=#EJg">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="/{F1J}%/X~gF3+_sa.6j">
                                                                                            <field name="value">550</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="$v^2x@1O.PKn8!H;Bhq|U">
                                                                                            <field name="value">225</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_anchor" id="$l;5?pM?;Ajb~Xcg)`!@1">
                                                                                            <field name="#prop">anchor</field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="=3#-lInW2T^cSoJo~(G|">
                                                                                                <field name="value">0.5</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="50YnX@2,@2W~W!fU-^tnIR">
                                                                                                <field name="value">0.5</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <statement name="items">
                                                                                  <block type="list_item_shape" id="!MYA)M:/?4H4.r8lKH9$">
                                                                                    <statement name="#props">
                                                                                      <block type="prop_list_align" id="rPf).!q@1dAKPs2L3kPFN">
                                                                                        <field name="#prop"></field>
                                                                                        <field name="align">middle</field>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <statement name="template">
                                                                                      <block type="image_shape" id="vLV9u|rF]7@2JzzP}B_:}">
                                                                                        <statement name="#props">
                                                                                          <block type="prop_size" id="T:IrnD#-[Ze_27|-6Q+]">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="width">
                                                                                              <block type="expression" id="=RX5._uMZ{$1DrrWeRID">
                                                                                                <field name="value">0</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="height">
                                                                                              <block type="expression" id="qt5qLoxncESAC)RNt~SG">
                                                                                                <field name="value">0</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_image_key" id="/yLP)Ldu(M,K9,8+8DtU">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="key">
                                                                                                  <block type="string_value" id="0U(!S.U1@1k+r(Zv^;8;z">
                                                                                                    <field name="value">shape2.png</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_image_src" id="Xz%hl]!@1I.LXWbvhz8z/">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="src">
                                                                                                      <block type="string_value" id="nky@2i}N.~a;DnikVw2wL">
                                                                                                        <field name="value">${image_path}shape2.png</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="algorithmic_input_shape" id="[e}LcmqF@19.%T?]@1jB-Z">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_value" id="TN6clfe}:4H;@1k0w%+`S">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="@1Y0=VVDuz^gR8?]A.GgV">
                                                                                                    <field name="value">ans</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_position" id="(u^k=nloWKrc75-ihkQ6">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="+SQo+%UqGFz6@107iOi%4">
                                                                                                        <field name="value">0</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="aOD#P0{Z$l:BCNh)9@2~E">
                                                                                                        <field name="value">0</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_size" id="~)S$^+Glx|;{-)@1m?MCt">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="width">
                                                                                                          <block type="expression" id="wdRRw?lccA]3huvks^0:">
                                                                                                            <field name="value">130</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="height">
                                                                                                          <block type="expression" id="FlQ@1R-hpt5|W3YEsjl5h">
                                                                                                            <field name="value">74</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_input_keyboard" id="pX[+1]_P03Uw?r6RhxOm">
                                                                                                            <field name="#prop"></field>
                                                                                                            <field name="keyboard">numbers1</field>
                                                                                                            <next>
                                                                                                              <block type="prop_input_max_length" id="e^196@2S|6ukB7~2Bxi9C">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="maxLength">
                                                                                                                  <block type="expression" id="/_E;-7``ONG+]nS_NxH+">
                                                                                                                    <field name="value">ans.toString().length</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_input_result_position" id="#LH4fkex}OZhG#[@2d;3P">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <field name="resultPosition">bottom</field>
                                                                                                                    <next>
                                                                                                                      <block type="prop_tab_order" id="s|F@1Izb{|@2lRY,asD!hu">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="tabOrder">
                                                                                                                          <block type="expression" id="2[il+_dgVQKQm?SvtFDj">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_stroke" id="D`nCliS/s1emx9euC0}t">
                                                                                                                            <field name="#prop">stroke</field>
                                                                                                                            <next>
                                                                                                                              <block type="prop_fill" id="Dz-AaH/!wx?BO[MH9dIz">
                                                                                                                                <field name="#prop">fill</field>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style" id="!1Qr3{IxD@1PMJ]G^LlcN">
                                                                                                                                    <field name="base">text</field>
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_text_style_font_size" id="!(nlSpda9T+o/iXBj}YF">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="fontSize">
                                                                                                                                          <block type="expression" id="FQTZZB_+$@1-hM?K.!NSO">
                                                                                                                                            <field name="value">32</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style_fill" id="w-4`M-zsemYrO[}g`_|q">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="fill">
                                                                                                                                              <block type="string_value" id="G;VmNGg}u8OZR_#3M_Oe">
                                                                                                                                                <field name="value">black</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_text_style_stroke" id="ahppz0%Y{eD@2C$!vyJ4m">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="stroke">
                                                                                                                                                  <block type="string_value" id="}KNcD$1%M)+@1a=7A%7,,">
                                                                                                                                                    <field name="value">white</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_text_style_stroke_thickness" id="TBWlBG{V@2q@2LOjXi7|@16">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="strokeThickness">
                                                                                                                                                      <block type="expression" id="bs_awM@2W;X@1OG#fQ+@2no">
                                                                                                                                                        <field name="value">2</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <next>
                                                                                      <block type="list_item_shape" id="kZqfNVy-^F/xWK^E929(">
                                                                                        <statement name="#props">
                                                                                          <block type="prop_list_align" id="%e)Z`f:(fR`[QKmVngu~">
                                                                                            <field name="#prop"></field>
                                                                                            <field name="align">middle</field>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <statement name="template">
                                                                                          <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="contents">
                                                                                                  <block type="string_value" id="#6btV9p,PGV}wMx.U�">
                                                                                                    <field name="value">${' ' + b}</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                                                    <field name="base">text</field>
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="fontSize">
                                                                                                          <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                                                            <field name="value">36</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="fill">
                                                                                                              <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                                                                <field name="value">black</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="stroke">
                                                                                                                  <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                                                    <field name="value">white</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="strokeThickness">
                                                                                                                      <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                                                        <field name="value">2</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                          </block>
                                                                                        </statement>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="if_then_block" id="oy.SH?(-);9=(e6[HNzg">
                                                                                <value name="if">
                                                                                  <block type="expression" id="5kV=K`6v:E0;S@1.Jg)@1a">
                                                                                    <field name="value">answer == 2</field>
                                                                                  </block>
                                                                                </value>
                                                                                <statement name="then">
                                                                                  <block type="do_while_block" id="}q_7Cy#,|V8BmuDz@1j5$">
                                                                                    <statement name="do">
                                                                                      <block type="variable" id="%)Z@2R6jSog/s[_;@2i2KB">
                                                                                        <field name="name">rd</field>
                                                                                        <value name="value">
                                                                                          <block type="random_many" id="Pw/Q):O5sqx;4_}zf9/5">
                                                                                            <value name="count">
                                                                                              <block type="expression" id="^;qU_#?xSDCN/|)3[#DB">
                                                                                                <field name="value">3</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="items">
                                                                                              <block type="expression" id="AU[%+NFWji|C97j5HC1O">
                                                                                                <field name="value">[-5, -4, -3, -2, -1, 1, 2, 3, 4, 5]</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <value name="while">
                                                                                      <block type="expression" id="#~oi.obeI[#C$7d0(CRs">
                                                                                        <field name="value">ans + rd[0] &lt; 1 || ans + rd[1] &lt; 1|| ans + rd[2] &lt; 1</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="do_while_block" id="}}ub=!UosIYM0#!@2W@2=T">
                                                                                        <statement name="do">
                                                                                          <block type="variable" id="}Tzm9C3cLgQk~[wFP)hO">
                                                                                            <field name="name">u</field>
                                                                                            <value name="value">
                                                                                              <block type="random_one" id="#sm_!t`^7OLX%3v$l#6.">
                                                                                                <value name="items">
                                                                                                  <block type="expression" id="(nPr7TnTwX)$W-^C-X1!">
                                                                                                    <field name="value">unit</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <value name="while">
                                                                                          <block type="expression" id="5c4n[VuO;nAy+#Y@1PB$t">
                                                                                            <field name="value">u == b</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="variable" id="g1W6A(-Qs.30,-tc!3jo">
                                                                                            <field name="name">option</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="3QU-@1s(T!#u{G`8fR,Ff">
                                                                                                <field name="value">[]</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="variable" id="{=UJjr|8Tn5Jk.3nsV6-">
                                                                                                <field name="name">option[0]</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="5Z`bmIM%RUJg`eF8Um`w">
                                                                                                    <field name="value">ans + ' ' + b</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="variable" id="]Bp$zjU{(G]G;7]liOpi">
                                                                                                    <field name="name">option[1]</field>
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="LE,$h[Q5o#NaB@22yc$Ye">
                                                                                                        <field name="value">ans + rd[2] +  ' ' + b</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="variable" id="9WQ4;AWs@1=;hx66eY+5=">
                                                                                                        <field name="name">option[2]</field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id="FgbN@1Knz[jCx@16~Gn0ub">
                                                                                                            <field name="value">ans + rd[0] + ' ' + b</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="variable" id="e]Jj@2AziM/$miSh}ir6p">
                                                                                                            <field name="name">option[3]</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="2phE5(h#$aN})48_O[II">
                                                                                                                <field name="value">ans + rd[1] + ' ' + u</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="variable" id="`l4CCiLiQvzs(ClMDypV">
                                                                                                                <field name="name">option</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="func_shuffle" id="{?n@1$%~i.hCGQt/I=nL1" inline="true">
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="c^xMNN#+jnNFI3UptH,u">
                                                                                                                        <field name="value">option</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="variable" id="q^VDz{CO/7?xdrof]s(G">
                                                                                                                    <field name="name">text</field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="Im?q9Dz8(EtER!jiPFg]">
                                                                                                                        <field name="value">['A', 'B', 'C', 'D']</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="grid_shape" id="6tU?K^N8ec~A@1@1NwQ72N">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_grid_dimension" id="X4K:|9dKK%s9l4n5v@2lG">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="rows">
                                                                                                                              <block type="expression" id="[Ev!.bxnzjc|-33v.%7=">
                                                                                                                                <field name="value">4</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="cols">
                                                                                                                              <block type="expression" id="u$7]/V/!ZWM4(Va??@2({">
                                                                                                                                <field name="value">1</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_position" id="zLld84TfBg#!88]1,JD-">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="Cd6g{Y_P?:GyVyvQi1wJ">
                                                                                                                                    <field name="value">550</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id=".F}E71+@1i)77!yQ`hmfu">
                                                                                                                                    <field name="value">240</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_size" id="+o2CWr^.ohx)_bcIG53]">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="width">
                                                                                                                                      <block type="expression" id="Z[x5iHG%KWC%KWQb}c-G">
                                                                                                                                        <field name="value">400</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="height">
                                                                                                                                      <block type="expression" id="@2E|Ob@1jF#gYtX8-%Iui@1">
                                                                                                                                        <field name="value">320</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_anchor" id="1WJjXY^.h,t]eNgxaJ}q">
                                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="expression" id="DHx[vqs?.?nUD:AYPJWd">
                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id="e3D|RR7w!=Sufu):zV`c">
                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_grid_cell_source" id="v8+Yu211Qg8fX9Mu4th`">
                                                                                                                                            <field name="#prop">cell.source</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id="~j@1,^liddf}oDg}E91^i">
                                                                                                                                                <field name="value">[0, 1, 2, 3]</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_grid_random" id="}?kS#x[b)9OGb@1`UdJGV">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <field name="random">FALSE</field>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_grid_show_borders" id=";y^wk4y%[VWjXOj~vrQZ">
                                                                                                                                                    <field name="#prop">#showBorders</field>
                                                                                                                                                    <field name="value">FALSE</field>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_grid_cell_template" id="7^6]Kv:2yrZi`cooAW2@2">
                                                                                                                                                        <field name="variable">$cell</field>
                                                                                                                                                        <field name="#prop">cell.template</field>
                                                                                                                                                        <field name="#callback">$cell</field>
                                                                                                                                                        <statement name="body">
                                                                                                                                                          <block type="choice_custom_shape" id="_:vh9L$Y~3LwNBYyC6t!">
                                                                                                                                                            <field name="action">click-one</field>
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id="@1F#SRUt0q93S@1!IzrG^^">
                                                                                                                                                                <field name="value">option[$cell.data]</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <statement name="template">
                                                                                                                                                              <block type="image_shape" id="9BGL!,8(hwS%S!WCBs">
                                                                                                                                                                <statement name="#props">
                                                                                                                                                                  <block type="prop_position" id="YMbdE_[Nl@19-TxmBVt3n">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="x">
                                                                                                                                                                      <block type="expression" id="w@1Zo#eQSRO)5nf=@2DVH{">
                                                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <value name="y">
                                                                                                                                                                      <block type="expression" id="2Or;$p.Hy;.;XOgiFs..">
                                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_size" id="LhFn@1XK3ml1{dw,_6A--">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="width">
                                                                                                                                                                          <block type="expression" id="QH-|;`g)VgeZIH{Y{q@2U">
                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <value name="height">
                                                                                                                                                                          <block type="expression" id="bt/_{W?%L4Vwun5w?+=C">
                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_image_key" id="S3dA#T}[@1T$jSL1k.Te[">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="key">
                                                                                                                                                                              <block type="string_value" id="[r-`e;~4L,u+,;^Zk/uq">
                                                                                                                                                                                <field name="value">shape1.png</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_image_src" id="+-Im/x[f-jA]F=3P#Vh0">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="src">
                                                                                                                                                                                  <block type="string_value" id="co7RtrwI~m2@1rJ)6wi;`">
                                                                                                                                                                                    <field name="value">${image_path}shape1.png</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="text_shape" id=":ZlzU6AXHI,N1=|vZ~[6">
                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                      <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="x">
                                                                                                                                                                          <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <value name="y">
                                                                                                                                                                          <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                                                                                                                                            <field name="value">$cell.centerY+3</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_anchor" id="O.v@2mKn@1K#+tF;|}C}xI">
                                                                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                                                                            <value name="x">
                                                                                                                                                                              <block type="expression" id="+3%GU[X3l%%X`Jv1QL7@2">
                                                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="y">
                                                                                                                                                                              <block type="expression" id="Kzp23HjHF7k]A-y1BJPm">
                                                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_text_contents" id="b3(|`QueWwXjWh$nHus2">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="contents">
                                                                                                                                                                                  <block type="string_value" id="e;D=^a?fht6urHt!Fx#Z">
                                                                                                                                                                                    <field name="value">${text[$cell.data] + '. ' + option[$cell.data]}</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_text_style" id="3~cJ0$yv$~P)ok580BLj">
                                                                                                                                                                                    <field name="base">text</field>
                                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                                      <block type="prop_text_style_font_size" id="jsKYfPNXLj{ztQr@2ITj/">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="fontSize">
                                                                                                                                                                                          <block type="expression" id="NYm1?{KWco:K23wNW@11r">
                                                                                                                                                                                            <field name="value">36</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_text_style_fill" id="{F3t24%RIRBh[SJ+%_CG">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <value name="fill">
                                                                                                                                                                                              <block type="string_value" id=".%H8tjU2a301/H}Zb7=O">
                                                                                                                                                                                                <field name="value">black</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_text_style_stroke" id="qp%QeQy:@1rdi{KB?9`])">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <value name="stroke">
                                                                                                                                                                                                  <block type="string_value" id="b)b[@1qX,GSnW.j5/{Nqs">
                                                                                                                                                                                                    <field name="value">white</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="prop_text_style_stroke_thickness" id="47PvzmwPwBg+p319QOL9">
                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                    <value name="strokeThickness">
                                                                                                                                                                                                      <block type="expression" id="OGa)CCc6NEtfuc/@1,Xew">
                                                                                                                                                                                                        <field name="value">2</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </statement>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="func_add_answer" id="clWdi]7H#b0bJ|+24zV!" inline="true">
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="#y`XXE8hm+(xIo)EC6T7">
                                                                                                                                <field name="value">ans + ' ' + b</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="variable" id="Ci~0!SqcEQ!q;F7?(77I">
                                                                                    <field name="name">load</field>
                                                                                    <value name="value">
                                                                                      <block type="custom_image_list" id="c6f.#ags}_}/31]+?gC?">
                                                                                        <field name="link">${image_path}</field>
                                                                                        <field name="images">ex${show}</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="partial_explanation" id="!lTXVT/JV;k-Bhd%S#Bo" inline="true">
                                                                                        <value name="value">
                                                                                          <block type="string_value" id="Rk=xe|x~VvV/eEbpM@1Vo">
                                                                                            <field name="value">&lt;center&gt;&lt;u&gt;Let's learn how to find the perimeter.&lt;/u&gt;
&lt;/br&gt;&lt;/br&gt;
  &lt;/center&gt;
&lt;span style='background: rgb(250, 250, 250, 0.5)'&gt;
A perimeter is a path that surrounds a 2D shape.&lt;/br&gt;&lt;/br&gt;
&lt;center&gt;
&lt;img src='@1sprite.src(ex${show}.png)'/&gt;
  &lt;/br&gt;&lt;/br&gt;&lt;/center&gt;
To do so, you simply have to &lt;b&gt;add them together.&lt;/b&gt;&lt;/br&gt;&lt;/br&gt;
  
All sides added, so the perimeter is &lt;b&gt;${ans + ' ' + b}&lt;/b&gt;.&lt;/span&gt;
                                                                                            </field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
</xml>
END_XML]] */