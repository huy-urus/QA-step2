
module.exports = [
  {
    "#type": "question",
    "name": "Y5.MG.AN.MSANGLES360.1B",
    "formula": "containsAll",
    "contents": [
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/Y5.MG.AN.MSANGLES360.1B/"
      },
      {
        "#type": "variable",
        "name": "bg_list",
        "value": {
          "#type": "custom_image_list",
          "link": "Develop/ImageQAs/Y6.MG.TI.READ.ANA.1B/",
          "images": "bg1|bg2|bg3|bg4|bg5"
        }
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_one",
          "items": {
            "#type": "expression",
            "value": "bg_list"
          }
        }
      },
      {
        "#type": "image_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "0"
            },
            "y": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "resp_w",
              "value": "100vw"
            },
            "height": {
              "#type": "resp_h",
              "value": "100vh"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0"
            },
            "y": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": {
              "#type": "expression",
              "value": "drop_background"
            }
          }
        ]
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "580"
            },
            "y": {
              "#type": "expression",
              "value": "60"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "800"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "Using the protractor, measure the below angle."
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "35"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_wordwrap",
                  "#prop": "",
                  "wordWrap": {
                    "#type": "expression",
                    "value": "true"
                  }
                },
                {
                  "#type": "prop_text_style_wordWrapWidth",
                  "#prop": "",
                  "wordWrapWidth": {
                    "#type": "expression",
                    "value": "400"
                  }
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "angle",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "2"
          },
          "max": {
            "#type": "expression",
            "value": "72"
          }
        }
      },
      {
        "#type": "variable",
        "name": "angle",
        "value": {
          "#type": "expression",
          "value": "angle*5"
        }
      },
      {
        "#type": "image_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "220"
            },
            "y": {
              "#type": "expression",
              "value": "200"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_max_size",
            "#prop": "",
            "maxWidth": {
              "#type": "expression",
              "value": "400"
            },
            "maxHeight": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "pro.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}pro.png"
          }
        ]
      },
      {
        "#type": "image_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "220"
            },
            "y": {
              "#type": "expression",
              "value": "200"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "1"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "200"
            },
            "height": {
              "#type": "expression",
              "value": "2"
            }
          },
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "line.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}line.png"
          },
          {
            "#type": "prop_angle",
            "#prop": "",
            "angle": {
              "#type": "expression",
              "value": "0"
            }
          }
        ]
      },
      {
        "#type": "image_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "220"
            },
            "y": {
              "#type": "expression",
              "value": "200"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "1"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "200"
            },
            "height": {
              "#type": "expression",
              "value": "2"
            }
          },
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "line.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}line.png"
          },
          {
            "#type": "prop_angle",
            "#prop": "",
            "angle": {
              "#type": "expression",
              "value": "angle"
            }
          }
        ]
      },
      {
        "#type": "image_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "600"
            },
            "y": {
              "#type": "expression",
              "value": "200"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "0"
            },
            "height": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "shape.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}shape.png"
          },
          {
            "#type": "prop_scale",
            "#prop": "",
            "scale": {
              "#type": "expression",
              "value": "0.8"
            }
          }
        ]
      },
      {
        "#type": "choice_input_shape",
        "#props": [
          {
            "#type": "prop_value",
            "#prop": "",
            "value": {
              "#type": "expression",
              "value": "angle"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "590"
            },
            "y": {
              "#type": "expression",
              "value": "205"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "90"
            },
            "height": {
              "#type": "expression",
              "value": "50"
            }
          },
          {
            "#type": "prop_input_keyboard",
            "#prop": "",
            "keyboard": "numbers1"
          },
          {
            "#type": "prop_input_max_length",
            "#prop": "",
            "maxLength": {
              "#type": "expression",
              "value": "angle.toString().length"
            }
          },
          {
            "#type": "prop_input_result_position",
            "#prop": "",
            "resultPosition": "bottom"
          },
          {
            "#type": "prop_tab_order",
            "#prop": "",
            "tabOrder": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_stroke",
            "#prop": "stroke"
          },
          {
            "#type": "prop_fill",
            "#prop": "fill"
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "42"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ],
        "#init": "algorithmic_input"
      },
      {
        "#type": "variable",
        "name": "link",
        "value": {
          "#type": "expression",
          "value": "'http://starmathsonline.s3.amazonaws.com/' + image_path"
        }
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Let's learn how to use a protractor.</u></br></br>\n\n<style>\n            div#img_line {\n                -ms-transform: rotate(${angle}deg);\n                -webkit-transform: rotate(${angle}deg);\n                transform: rotate(${angle}deg);\n            }\n            </style>\n        <div style=\"margin: 0 auto; width: 480px; height: 480px;\">\n            <div style=\"position:absolute; width: 480px; height: 480px;\">\n                <img src=\"http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y5.MG.AN.MSANGLES360.1B/pro.png\" style='max-width: 450px; margin-left: 15px; margin-top: 15px'/>\n            </div>\n            <div style=\"position:absolute; width: 480px; height: 480px;\">\n                <img src=\"http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y5.MG.AN.MSANGLES360.1B/line.png\" style='width: 240px; max-height: 2px; margin-top: 239px'/>\n            </div>\n            <div style=\"position:absolute; width: 480px; height: 480px;\" id=\"img_line\" >\n                <img src=\"http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y5.MG.AN.MSANGLES360.1B/line.png\" style=\"width: 240px; max-height: 2px; margin-top: 239px\"/>\n            </div>\n        </div>\n</br>\nTo find the angle, we count the size between the two arms.</br>\n\nWe can see that the size is ${angle}&#176;."
        ]
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="Q2o?$umaXzm1P!01AgUk" x="0" y="0">
    <field name="name">Y5.MG.AN.MSANGLES360.1B</field>
    <field name="formula">containsAll</field>
    <statement name="contents">
      <block type="variable" id="vzF|//].u)!Tw@1r)mrKD">
        <field name="name">image_path</field>
        <value name="value">
          <block type="string_value" id="Fn~LE=y.+i|P5D0MAQ+M">
            <field name="value">Develop/ImageQAs/Y5.MG.AN.MSANGLES360.1B/</field>
          </block>
        </value>
        <next>
          <block type="variable" id="t}0T!/qKmIP]R2be1d)}">
            <field name="name">bg_list</field>
            <value name="value">
              <block type="custom_image_list" id="psG%B_mMH#.gk{Dn=jC7">
                <field name="link">Develop/ImageQAs/Y6.MG.TI.READ.ANA.1B/</field>
                <field name="images">bg1|bg2|bg3|bg4|bg5</field>
              </block>
            </value>
            <next>
              <block type="variable" id="R#SgXbbJc~nUj5y{ytv+">
                <field name="name">drop_background</field>
                <value name="value">
                  <block type="random_one" id="ZMhm,f-,:%3VHA2AaO+9">
                    <value name="items">
                      <block type="expression" id="e63SK+xvI|}%H9t4Fn%Y">
                        <field name="value">bg_list</field>
                      </block>
                    </value>
                  </block>
                </value>
                <next>
                  <block type="image_shape" id="[`zr7Zs$FGYZ~-JGQ|~9">
                    <statement name="#props">
                      <block type="prop_position" id="l.W]G3VA)Ds`d27DBX%r">
                        <field name="#prop"></field>
                        <value name="x">
                          <block type="expression" id="?H@1@1{%X/Hcr@1!@20;5.s(">
                            <field name="value">0</field>
                          </block>
                        </value>
                        <value name="y">
                          <block type="expression" id="brRJR[E;3zh-Nq-~CPW:">
                            <field name="value">0</field>
                          </block>
                        </value>
                        <next>
                          <block type="prop_size" id="W#4K-b-DVBJWS~OAl7$@2">
                            <field name="#prop"></field>
                            <value name="width">
                              <block type="resp_w" id="/nKxviTo.fJg7$n;4cw1">
                                <field name="value">100vw</field>
                              </block>
                            </value>
                            <value name="height">
                              <block type="resp_h" id="6-wt,t-S)r8jfAiSedx(">
                                <field name="value">100vh</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_anchor" id="wEt!X6#_{A$/?@1pBW~W}">
                                <field name="#prop">anchor</field>
                                <value name="x">
                                  <block type="expression" id="Cc}I-ntw_PaSeoRmt0qt">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <value name="y">
                                  <block type="expression" id="IokGkj3h{G+$rd4]REu-">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="prop_image_src" id="hsRm$$WiHB!qr$Rzea^D">
                                    <field name="#prop"></field>
                                    <value name="src">
                                      <block type="expression" id="$By|4_az~^6@1/BO,/Q50">
                                        <field name="value">drop_background</field>
                                      </block>
                                    </value>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </statement>
                    <next>
                      <block type="text_shape" id="BMZ`VXgeL9SKSUPypqR}">
                        <statement name="#props">
                          <block type="prop_position" id="2ZCJ!{{ewoqJ:U`qy/a3">
                            <field name="#prop"></field>
                            <value name="x">
                              <block type="expression" id="b)fU=as{HPO03YiC@1ld!">
                                <field name="value">580</field>
                              </block>
                            </value>
                            <value name="y">
                              <block type="expression" id="v9Xp5o$8e(vwo?-5ulv)">
                                <field name="value">60</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_anchor" id="/mlO?LRGl]Y6:miMCcVl">
                                <field name="#prop">anchor</field>
                                <value name="x">
                                  <block type="expression" id="(AL@10~x$mYek3xG#s8!R">
                                    <field name="value">0.5</field>
                                  </block>
                                </value>
                                <value name="y">
                                  <block type="expression" id="9@1zx[V:?]#@2^~.nF{K1i">
                                    <field name="value">0.5</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="prop_size" id="F]7KxTf#$CW#$-a/6Rzx">
                                    <field name="#prop"></field>
                                    <value name="width">
                                      <block type="expression" id="Z~S||![2C%z+@2R:a5L1q">
                                        <field name="value">800</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="prop_text_contents" id="a/_O-[.Q43~MHRwq0s9L">
                                        <field name="#prop"></field>
                                        <value name="contents">
                                          <block type="string_value" id="sq!]aL5[V31FJEKfV=kt">
                                            <field name="value">Using the protractor, measure the below angle.</field>
                                          </block>
                                        </value>
                                        <next>
                                          <block type="prop_text_style" id="a(3JA7@1Mb2H0sM##7f$V">
                                            <field name="base">text</field>
                                            <statement name="#props">
                                              <block type="prop_text_style_font_size" id="a#|@2`]s}o!b.Ip9+i?S4">
                                                <field name="#prop"></field>
                                                <value name="fontSize">
                                                  <block type="expression" id="rWx}E+P{y@2ktY_%^5d:j">
                                                    <field name="value">35</field>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="prop_text_style_fill" id="!yxwytpitVg|ZpaK8C!U">
                                                    <field name="#prop"></field>
                                                    <value name="fill">
                                                      <block type="string_value" id="EL(Zzz`XUBi;_$4Fo~?.">
                                                        <field name="value">black</field>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="prop_text_style_wordwrap" id=")tZkbUjGU,^ph/8JO+w#">
                                                        <field name="#prop"></field>
                                                        <value name="wordWrap">
                                                          <block type="expression" id="{K#^A#hqSm9nKO-=l|^C">
                                                            <field name="value">true</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="prop_text_style_wordWrapWidth" id="+8#,%6pVC~1DYn8tS_hZ">
                                                            <field name="#prop"></field>
                                                            <value name="wordWrapWidth">
                                                              <block type="expression" id="kftf6Hiwf_d-q-8g.}y;">
                                                                <field name="value">400</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="prop_text_style_stroke" id="INzl8|-k.^)bjafPbDNE">
                                                                <field name="#prop"></field>
                                                                <value name="stroke">
                                                                  <block type="string_value" id="}4_tNvTmIAWv,K5P.k[}">
                                                                    <field name="value">white</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_text_style_stroke_thickness" id="d^UEEMSRs:?-XwP|]-Sy">
                                                                    <field name="#prop"></field>
                                                                    <value name="strokeThickness">
                                                                      <block type="expression" id="W$KFH7J8/]l1)$,[g7.N">
                                                                        <field name="value">2</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </statement>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="variable" id="bE7g++F=3L4v95TQHFRJ">
                            <field name="name">angle</field>
                            <value name="value">
                              <block type="random_number" id=":,DGqj1xCS@2D#BM+)wSE">
                                <value name="min">
                                  <block type="expression" id="z;-~%p$x;(DygsyWu8tG">
                                    <field name="value">2</field>
                                  </block>
                                </value>
                                <value name="max">
                                  <block type="expression" id="5svQIvR5#j3AcvP7BJdM">
                                    <field name="value">72</field>
                                  </block>
                                </value>
                              </block>
                            </value>
                            <next>
                              <block type="comment" id="N3;~YGg_i1/!{K@1Ryp4u">
                                <field name="value">size of one unit is small, I random angle 10, 15, 20, 25,... (big unit) that so easy to read.</field>
                                <next>
                                  <block type="variable" id="[W``).;TCPA9IM^Jcxax">
                                    <field name="name">angle</field>
                                    <value name="value">
                                      <block type="expression" id="HW,A;4[DaoJ?cwr8ViCH">
                                        <field name="value">angle@25</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="image_shape" id="L;z:=V)4krx_[lzu?QY_">
                                        <statement name="#props">
                                          <block type="prop_position" id="!pn@2R4}/L0[XKuTZuVZb">
                                            <field name="#prop"></field>
                                            <value name="x">
                                              <block type="expression" id="B+zVDpfr1TNPmM8:AZ;d">
                                                <field name="value">220</field>
                                              </block>
                                            </value>
                                            <value name="y">
                                              <block type="expression" id="kP=,tuD8?~$VLI`VyF9e">
                                                <field name="value">200</field>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="prop_anchor" id="Xo-D.UZyjFm50/b([?/T">
                                                <field name="#prop">anchor</field>
                                                <value name="x">
                                                  <block type="expression" id="NeAD8@1r,+(TIciU(hBLB">
                                                    <field name="value">0.5</field>
                                                  </block>
                                                </value>
                                                <value name="y">
                                                  <block type="expression" id="veeoF}Nj}gN3XH4tm@2lw">
                                                    <field name="value">0.5</field>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="prop_max_size" id="u)FY4mzN~n6tn_SmjI@1d">
                                                    <field name="#prop"></field>
                                                    <value name="maxWidth">
                                                      <block type="expression" id="??11y8R@1jluf%r7Yf6(d">
                                                        <field name="value">400</field>
                                                      </block>
                                                    </value>
                                                    <value name="maxHeight">
                                                      <block type="expression" id="qg:M4N}1.q/Wm.kEiWLP">
                                                        <field name="value">0</field>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="prop_image_key" id="1oGJ`jL.gKgkf[3RY[3B">
                                                        <field name="#prop"></field>
                                                        <value name="key">
                                                          <block type="string_value" id="NsAP|bPHY=oW`_@18B~T6">
                                                            <field name="value">pro.png</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="prop_image_src" id="ss5^-]bTi_Y#I1`LqNPf">
                                                            <field name="#prop"></field>
                                                            <value name="src">
                                                              <block type="string_value" id="KcX|~iQBu{9NRlDfO9^]">
                                                                <field name="value">${image_path}pro.png</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </statement>
                                        <next>
                                          <block type="image_shape" id="Z@1~%?C2Gz4X#quSZ}z21">
                                            <statement name="#props">
                                              <block type="prop_position" id="Q2/3N1T1nF7Q:3`IMX+n">
                                                <field name="#prop"></field>
                                                <value name="x">
                                                  <block type="expression" id="!;2_1(,S}9C1j}^gY%i?">
                                                    <field name="value">220</field>
                                                  </block>
                                                </value>
                                                <value name="y">
                                                  <block type="expression" id="|bB@11:/quy!Gfp}9Y8~5">
                                                    <field name="value">200</field>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="prop_anchor" id="H!gk@1tTdQ@2IZ~c3~#AcC">
                                                    <field name="#prop">anchor</field>
                                                    <value name="x">
                                                      <block type="expression" id="0jL)f`.DXyb@2)Ld.c4F=">
                                                        <field name="value">1</field>
                                                      </block>
                                                    </value>
                                                    <value name="y">
                                                      <block type="expression" id="QJJZn_;RpSP.D32Wck)M">
                                                        <field name="value">0.5</field>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="prop_size" id="g#Ufwi81b;Rf4EI$L@1(p">
                                                        <field name="#prop"></field>
                                                        <value name="width">
                                                          <block type="expression" id="%|HRNGwuHk{L]b{aW]rk">
                                                            <field name="value">200</field>
                                                          </block>
                                                        </value>
                                                        <value name="height">
                                                          <block type="expression" id="VT}M]=)qu~eB=Yo3q!h=">
                                                            <field name="value">2</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="prop_image_key" id="H:;-q!KKOTb`7RAl%E!L">
                                                            <field name="#prop"></field>
                                                            <value name="key">
                                                              <block type="string_value" id="ySa)q(zIuGu6`|;3)G)K">
                                                                <field name="value">line.png</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="prop_image_src" id="V7w)]WO-FnYfbÂªnqH6">
                                                                <field name="#prop"></field>
                                                                <value name="src">
                                                                  <block type="string_value" id="IMGtW:!J=hv;@2hKmr2]d">
                                                                    <field name="value">${image_path}line.png</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_angle" id="8Ji@2=bzR[;^6%cneRs+8">
                                                                    <field name="#prop"></field>
                                                                    <value name="angle">
                                                                      <block type="expression" id="IwO2#P!;@2CZ:1j,k-B=o">
                                                                        <field name="value">0</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </statement>
                                            <next>
                                              <block type="image_shape" id="DCj9-_O4q~6}qc(ytK?]">
                                                <statement name="#props">
                                                  <block type="prop_position" id="6LNQy:]@1b!%A-@1s%+1%`">
                                                    <field name="#prop"></field>
                                                    <value name="x">
                                                      <block type="expression" id="NzW7O)e=N!I^j#QeIY!@2">
                                                        <field name="value">220</field>
                                                      </block>
                                                    </value>
                                                    <value name="y">
                                                      <block type="expression" id="3Q{86~qNLQ0d)#:xq$An">
                                                        <field name="value">200</field>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="prop_anchor" id="ix=5z6c@1M,EyYLSx=^8@1">
                                                        <field name="#prop">anchor</field>
                                                        <value name="x">
                                                          <block type="expression" id="W.KZ/1q8k|n8y)Y{CXRy">
                                                            <field name="value">1</field>
                                                          </block>
                                                        </value>
                                                        <value name="y">
                                                          <block type="expression" id="Zw=+oP0_MvBzm$nTn@1Y.">
                                                            <field name="value">0.5</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="prop_size" id="f14B23jp{zC|Vox#@1]s;">
                                                            <field name="#prop"></field>
                                                            <value name="width">
                                                              <block type="expression" id="%3OcS^b1YYZj@1Asm{$4|">
                                                                <field name="value">200</field>
                                                              </block>
                                                            </value>
                                                            <value name="height">
                                                              <block type="expression" id="E$v6:)YS,{@2BjYI0({`q">
                                                                <field name="value">2</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="prop_image_key" id="hB5w4ipb-Q;Ktvk}k]F1">
                                                                <field name="#prop"></field>
                                                                <value name="key">
                                                                  <block type="string_value" id="bXZ-K581KVaMbh%$`Jnq">
                                                                    <field name="value">line.png</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_image_src" id="[Pi:=zeBp-MxQb@2c4kk;">
                                                                    <field name="#prop"></field>
                                                                    <value name="src">
                                                                      <block type="string_value" id="]pHv|j%n_tPy0luIb`Ak">
                                                                        <field name="value">${image_path}line.png</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_angle" id="fVg?U`:LSQM%9HC-@1_9Y">
                                                                        <field name="#prop"></field>
                                                                        <value name="angle">
                                                                          <block type="expression" id="YGde!TbD!.Ho5G(jzj$_">
                                                                            <field name="value">angle</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </statement>
                                                <next>
                                                  <block type="image_shape" id="fypW?;s2KmpJ5N_L:r-)">
                                                    <statement name="#props">
                                                      <block type="prop_position" id="S]5qjIz4X7X7.u;D$@1ZZ">
                                                        <field name="#prop"></field>
                                                        <value name="x">
                                                          <block type="expression" id="{`,$I/7=\PKp|/]@1Rb">
                                                            <field name="value">600</field>
                                                          </block>
                                                        </value>
                                                        <value name="y">
                                                          <block type="expression" id="k;;_wLhW$I;L1yaEyS$R">
                                                            <field name="value">200</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="prop_anchor" id="JZKw;LCV5!ebO6FjX{KI">
                                                            <field name="#prop">anchor</field>
                                                            <value name="x">
                                                              <block type="expression" id="A]g=Gy`QXc9d0y_xx$H/">
                                                                <field name="value">0.5</field>
                                                              </block>
                                                            </value>
                                                            <value name="y">
                                                              <block type="expression" id="cZI%lBh,W;e)eCTKN@28g">
                                                                <field name="value">0.5</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="prop_size" id="e6LoSLM(iPOI7H/DVW+~">
                                                                <field name="#prop"></field>
                                                                <value name="width">
                                                                  <block type="expression" id="1K,[gmu13--Mp%S;Taf6">
                                                                    <field name="value">0</field>
                                                                  </block>
                                                                </value>
                                                                <value name="height">
                                                                  <block type="expression" id="eu/((JcR^^QVC!V%,AQ}">
                                                                    <field name="value">0</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_image_key" id="=3?Y%]7x53?XC}i|;zDE">
                                                                    <field name="#prop"></field>
                                                                    <value name="key">
                                                                      <block type="string_value" id="i$?QlJ_7`{hVTrI27mla">
                                                                        <field name="value">shape.png</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_image_src" id="xof+{`K-KUdxsLnYS^gg">
                                                                        <field name="#prop"></field>
                                                                        <value name="src">
                                                                          <block type="string_value" id="cZw)@12^.C9guM`V4[h0U">
                                                                            <field name="value">${image_path}shape.png</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_scale" id="H`[AJOvvQDA5O,b.$g[#">
                                                                            <field name="#prop"></field>
                                                                            <value name="scale">
                                                                              <block type="expression" id="c!rmV$zj1P9|X66//aqr">
                                                                                <field name="value">0.8</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </statement>
                                                    <next>
                                                      <block type="algorithmic_input_shape" id="Oqj|`K[(R0[/8+$]jt#[">
                                                        <statement name="#props">
                                                          <block type="prop_value" id=",^K;D3+4zEG=.x[lrRHy">
                                                            <field name="#prop"></field>
                                                            <value name="value">
                                                              <block type="expression" id="=|U`m6bH,T-OBx!+qMje">
                                                                <field name="value">angle</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="prop_position" id=",V4ue^V2?$@2kmRHvJjAK">
                                                                <field name="#prop"></field>
                                                                <value name="x">
                                                                  <block type="expression" id="itgh0bh-+fGFPfwM=x:1">
                                                                    <field name="value">590</field>
                                                                  </block>
                                                                </value>
                                                                <value name="y">
                                                                  <block type="expression" id="u@1g79@2Fl)Ho;eWAkXTQ6">
                                                                    <field name="value">205</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="prop_size" id="t7#6qLrSMcGXbEQb%x`9">
                                                                    <field name="#prop"></field>
                                                                    <value name="width">
                                                                      <block type="expression" id="lMr2JJ{xz/-:RKDM}sD%">
                                                                        <field name="value">90</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="height">
                                                                      <block type="expression" id="EeAvesm_YLT0|x@2y1J/E">
                                                                        <field name="value">50</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_input_keyboard" id="e{k^mj`8i^QUJ%`lUQ@2@2">
                                                                        <field name="#prop"></field>
                                                                        <field name="keyboard">numbers1</field>
                                                                        <next>
                                                                          <block type="prop_input_max_length" id="B7~,Q?k@1DU~xuel#8Rj!">
                                                                            <field name="#prop"></field>
                                                                            <value name="maxLength">
                                                                              <block type="expression" id="R3nucL4Se53Fq|}%@2x2:">
                                                                                <field name="value">angle.toString().length</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_input_result_position" id="s,|l7DoWhLtn@1+6bVkq;">
                                                                                <field name="#prop"></field>
                                                                                <field name="resultPosition">bottom</field>
                                                                                <next>
                                                                                  <block type="prop_tab_order" id="Tcb}T^0j9rNbo!0wM{XP">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="tabOrder">
                                                                                      <block type="expression" id="zm%x1l.X]9Ebz0SD(9EY">
                                                                                        <field name="value">0</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_stroke" id="g`?C}=+(8@2S))mRO/.8_">
                                                                                        <field name="#prop">stroke</field>
                                                                                        <next>
                                                                                          <block type="prop_fill" id=")iI}a/;_yY7I=RxWqF{k">
                                                                                            <field name="#prop">fill</field>
                                                                                            <next>
                                                                                              <block type="prop_text_style" id="{?/+@2XXmcBdAgDE~@1`@1M">
                                                                                                <field name="base">text</field>
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_text_style_font_size" id="|OTm2[{MZ9YS{uGr6mXY">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="fontSize">
                                                                                                      <block type="expression" id="f@2d!AiYEtE9.1wpA;=Yc">
                                                                                                        <field name="value">42</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style_fill" id="4:.33-!p}`?+;SGbVblQ">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="fill">
                                                                                                          <block type="string_value" id="xtNAI!./F/ud|;W`7:1_">
                                                                                                            <field name="value">black</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style_stroke" id="!YB20!f{Q;Rv[w[W#@26!">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="stroke">
                                                                                                              <block type="string_value" id="aVv1p0L9zD[.`0K86?f9">
                                                                                                                <field name="value">white</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_stroke_thickness" id="dsN9Uv;2K={?Ks[n!ZU}">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="strokeThickness">
                                                                                                                  <block type="expression" id="}]5G)(C)p4lL0b0+`oIt">
                                                                                                                    <field name="value">2</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </statement>
                                                        <next>
                                                          <block type="variable" id="LMjMV${0([g.@1Fj/#?8S">
                                                            <field name="name">link</field>
                                                            <value name="value">
                                                              <block type="expression" id="dyZ+n+h33~WrpEGt[Kr[">
                                                                <field name="value">'http://starmathsonline.s3.amazonaws.com/' + image_path</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="partial_explanation" id=")YS)Vp`zbRfn}#WFpgNv">
                                                                <value name="value">
                                                                  <block type="string_value" id="f}}D05Jk4CcehRD^.UzG">
                                                                    <field name="value">&lt;u&gt;Let's learn how to use a protractor.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
            div#img_line {
                -ms-transform: rotate(${angle}deg);
                -webkit-transform: rotate(${angle}deg);
                transform: rotate(${angle}deg);
            }
            &lt;/style&gt;
        &lt;div style="margin: 0 auto; width: 480px; height: 480px;"&gt;
            &lt;div style="position:absolute; width: 480px; height: 480px;"&gt;
                &lt;img src="http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y5.MG.AN.MSANGLES360.1B/pro.png" style='max-width: 450px; margin-left: 15px; margin-top: 15px'/&gt;
            &lt;/div&gt;
            &lt;div style="position:absolute; width: 480px; height: 480px;"&gt;
                &lt;img src="http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y5.MG.AN.MSANGLES360.1B/line.png" style='width: 240px; max-height: 2px; margin-top: 239px'/&gt;
            &lt;/div&gt;
            &lt;div style="position:absolute; width: 480px; height: 480px;" id="img_line" &gt;
                &lt;img src="http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y5.MG.AN.MSANGLES360.1B/line.png" style="width: 240px; max-height: 2px; margin-top: 239px"/&gt;
            &lt;/div&gt;
        &lt;/div&gt;
&lt;/br&gt;
To find the angle, we count the size between the two arms.&lt;/br&gt;

We can see that the size is ${angle}&amp;#176;.
                                                                    </field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
</xml>
END_XML]] */