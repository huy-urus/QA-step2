
module.exports = [
  {
    "#type": "question",
    "name": "Y5.SP.RPOSPIN.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y5.SP.RPOSPIN.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "statement",
        "value": "function re_color(color) {  \n  if(color == 'g'){return 'green';}  \n  if(color == 'r'){return 'red';}  \n  if(color == 'b'){return 'blue';}\n  if(color == 'o'){return 'orange';} \n  if(color == 'p'){return 'purple';} \n  if(color == 'y'){return 'yellow';}}"
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "range < 0"
        },
        "then": [
          {
            "#type": "variable",
            "name": "part",
            "value": {
              "#type": "expression",
              "value": "4"
            }
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "range < 4"
            },
            "then": [
              {
                "#type": "variable",
                "name": "part",
                "value": {
                  "#type": "random_one",
                  "items": {
                    "#type": "expression",
                    "value": "[4, 8]"
                  }
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "part",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "2"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "12"
                  }
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "num_color",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "num_color[0]",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "0"
          },
          "max": {
            "#type": "expression",
            "value": "part"
          }
        }
      },
      {
        "#type": "variable",
        "name": "num_color[1]",
        "value": {
          "#type": "expression",
          "value": "part - num_color[0]"
        }
      },
      {
        "#type": "variable",
        "name": "color",
        "value": {
          "#type": "random_many",
          "count": {
            "#type": "expression",
            "value": "2"
          },
          "items": {
            "#type": "expression",
            "value": "['b', 'g', 'r', 'y', 'o', 'p']"
          }
        }
      },
      {
        "#type": "variable",
        "name": "qes_color",
        "value": {
          "#type": "expression",
          "value": "color[0]"
        }
      },
      {
        "#type": "variable",
        "name": "spiner",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "variable",
        "name": "count",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < 2"
        },
        "do": [
          {
            "#type": "variable",
            "name": "j",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "j < num_color[i]"
            },
            "do": [
              {
                "#type": "variable",
                "name": "spiner[count]",
                "value": {
                  "#type": "expression",
                  "value": "color[i]"
                }
              },
              {
                "#type": "variable",
                "name": "j",
                "value": {
                  "#type": "expression",
                  "value": "j+1"
                }
              },
              {
                "#type": "variable",
                "name": "count",
                "value": {
                  "#type": "expression",
                  "value": "count + 1"
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "spiner",
        "value": {
          "#type": "func",
          "name": "shuffle",
          "args": [
            {
              "#type": "expression",
              "value": "spiner"
            }
          ]
        }
      },
      {
        "#type": "image_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "150"
            },
            "y": {
              "#type": "expression",
              "value": "270"
            }
          },
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "spin.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}spin.png"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "variable",
        "name": "angle",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < part"
        },
        "do": [
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "150"
                },
                "y": {
                  "#type": "expression",
                  "value": "150"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "1-${part}${spiner[i]}.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}1-${part}${spiner[i]}.png"
              },
              {
                "#type": "prop_angle",
                "#prop": "",
                "angle": {
                  "#type": "expression",
                  "value": "angle"
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "angle",
            "value": {
              "#type": "expression",
              "value": "angle + 360/part"
            }
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "image_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "150"
            },
            "y": {
              "#type": "expression",
              "value": "140"
            }
          },
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "point.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}point.png"
          }
        ]
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "300"
            },
            "y": {
              "#type": "expression",
              "value": "40"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "The total number of possible"
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "36"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                },
                {
                  "#type": "prop_text_style_wordwrap",
                  "#prop": "",
                  "wordWrap": {
                    "#type": "expression",
                    "value": "true"
                  }
                },
                {
                  "#type": "prop_text_style_wordWrapWidth",
                  "#prop": "",
                  "wordWrapWidth": {
                    "#type": "expression",
                    "value": "550"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "list_shape",
        "#props": [
          {
            "#type": "prop_list_direction",
            "#prop": "",
            "dir": "horizontal"
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "300"
            },
            "y": {
              "#type": "expression",
              "value": "100"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          }
        ],
        "items": [
          {
            "#type": "json",
            "#props": [
              {
                "#type": "prop_list_align",
                "#prop": "",
                "align": "middle"
              }
            ],
            "template": {
              "#callback": "$item",
              "variable": "$item",
              "body": [
                {
                  "#type": "text_shape",
                  "#props": [
                    {
                      "#type": "prop_text_contents",
                      "#prop": "",
                      "contents": "outcomes:${' '}"
                    },
                    {
                      "#prop": "",
                      "style": {
                        "#type": "json",
                        "base": "text",
                        "#props": [
                          {
                            "#type": "prop_text_style_font_size",
                            "#prop": "",
                            "fontSize": {
                              "#type": "expression",
                              "value": "36"
                            }
                          },
                          {
                            "#type": "prop_text_style_fill",
                            "#prop": "",
                            "fill": "black"
                          },
                          {
                            "#type": "prop_text_style_stroke",
                            "#prop": "",
                            "stroke": "white"
                          },
                          {
                            "#type": "prop_text_style_stroke_thickness",
                            "#prop": "",
                            "strokeThickness": {
                              "#type": "expression",
                              "value": "2"
                            }
                          }
                        ]
                      }
                    }
                  ]
                }
              ]
            }
          },
          {
            "#type": "json",
            "#props": [
              {
                "#type": "prop_list_align",
                "#prop": "",
                "align": "middle"
              }
            ],
            "template": {
              "#callback": "$item",
              "variable": "$item",
              "body": [
                {
                  "#type": "image_shape",
                  "#props": [
                    {
                      "#type": "prop_size",
                      "#prop": "",
                      "width": {
                        "#type": "expression",
                        "value": "80"
                      },
                      "height": {
                        "#type": "expression",
                        "value": "50"
                      }
                    },
                    {
                      "#type": "prop_image_key",
                      "#prop": "",
                      "key": "shape.png"
                    },
                    {
                      "#type": "prop_image_src",
                      "#prop": "",
                      "src": "${image_path}shape.png"
                    }
                  ]
                },
                {
                  "#type": "choice_input_shape",
                  "#props": [
                    {
                      "#type": "prop_value",
                      "#prop": "",
                      "value": {
                        "#type": "expression",
                        "value": "part"
                      }
                    },
                    {
                      "#type": "prop_size",
                      "#prop": "",
                      "width": {
                        "#type": "expression",
                        "value": "80"
                      },
                      "height": {
                        "#type": "expression",
                        "value": "50"
                      }
                    },
                    {
                      "#type": "prop_input_keyboard",
                      "#prop": "",
                      "keyboard": "numbers1"
                    },
                    {
                      "#type": "prop_input_max_length",
                      "#prop": "",
                      "maxLength": {
                        "#type": "expression",
                        "value": "part.toString().length"
                      }
                    },
                    {
                      "#type": "prop_input_result_position",
                      "#prop": "",
                      "resultPosition": "right"
                    },
                    {
                      "#type": "prop_tab_order",
                      "#prop": "",
                      "tabOrder": {
                        "#type": "expression",
                        "value": "0"
                      }
                    },
                    {
                      "#type": "prop_stroke",
                      "#prop": "stroke"
                    },
                    {
                      "#type": "prop_fill",
                      "#prop": "fill"
                    },
                    {
                      "#prop": "",
                      "style": {
                        "#type": "json",
                        "base": "text",
                        "#props": [
                          {
                            "#type": "prop_text_style_font_size",
                            "#prop": "",
                            "fontSize": {
                              "#type": "expression",
                              "value": "32"
                            }
                          },
                          {
                            "#type": "prop_text_style_fill",
                            "#prop": "",
                            "fill": "black"
                          },
                          {
                            "#type": "prop_text_style_stroke",
                            "#prop": "",
                            "stroke": "white"
                          },
                          {
                            "#type": "prop_text_style_stroke_thickness",
                            "#prop": "",
                            "strokeThickness": {
                              "#type": "expression",
                              "value": "2"
                            }
                          }
                        ]
                      }
                    }
                  ],
                  "#init": "algorithmic_input"
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "list_shape",
        "#props": [
          {
            "#type": "prop_list_direction",
            "#prop": "",
            "dir": "horizontal"
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "300"
            },
            "y": {
              "#type": "expression",
              "value": "160"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          }
        ],
        "items": [
          {
            "#type": "json",
            "#props": [
              {
                "#type": "prop_list_align",
                "#prop": "",
                "align": "middle"
              }
            ],
            "template": {
              "#callback": "$item",
              "variable": "$item",
              "body": [
                {
                  "#type": "text_shape",
                  "#props": [
                    {
                      "#type": "prop_text_contents",
                      "#prop": "",
                      "contents": "The number of ${re_color(color[0])} parts:${' '}"
                    },
                    {
                      "#prop": "",
                      "style": {
                        "#type": "json",
                        "base": "text",
                        "#props": [
                          {
                            "#type": "prop_text_style_font_size",
                            "#prop": "",
                            "fontSize": {
                              "#type": "expression",
                              "value": "36"
                            }
                          },
                          {
                            "#type": "prop_text_style_fill",
                            "#prop": "",
                            "fill": "black"
                          },
                          {
                            "#type": "prop_text_style_stroke",
                            "#prop": "",
                            "stroke": "white"
                          },
                          {
                            "#type": "prop_text_style_stroke_thickness",
                            "#prop": "",
                            "strokeThickness": {
                              "#type": "expression",
                              "value": "2"
                            }
                          }
                        ]
                      }
                    }
                  ]
                }
              ]
            }
          },
          {
            "#type": "json",
            "#props": [
              {
                "#type": "prop_list_align",
                "#prop": "",
                "align": "middle"
              }
            ],
            "template": {
              "#callback": "$item",
              "variable": "$item",
              "body": [
                {
                  "#type": "image_shape",
                  "#props": [
                    {
                      "#type": "prop_size",
                      "#prop": "",
                      "width": {
                        "#type": "expression",
                        "value": "80"
                      },
                      "height": {
                        "#type": "expression",
                        "value": "50"
                      }
                    },
                    {
                      "#type": "prop_image_key",
                      "#prop": "",
                      "key": "shape.png"
                    },
                    {
                      "#type": "prop_image_src",
                      "#prop": "",
                      "src": "${image_path}shape.png"
                    }
                  ]
                },
                {
                  "#type": "choice_input_shape",
                  "#props": [
                    {
                      "#type": "prop_value",
                      "#prop": "",
                      "value": {
                        "#type": "expression",
                        "value": "num_color[0]"
                      }
                    },
                    {
                      "#type": "prop_size",
                      "#prop": "",
                      "width": {
                        "#type": "expression",
                        "value": "80"
                      },
                      "height": {
                        "#type": "expression",
                        "value": "50"
                      }
                    },
                    {
                      "#type": "prop_input_keyboard",
                      "#prop": "",
                      "keyboard": "numbers1"
                    },
                    {
                      "#type": "prop_input_max_length",
                      "#prop": "",
                      "maxLength": {
                        "#type": "expression",
                        "value": "num_color[0].toString().length"
                      }
                    },
                    {
                      "#type": "prop_input_result_position",
                      "#prop": "",
                      "resultPosition": "right"
                    },
                    {
                      "#type": "prop_tab_order",
                      "#prop": "",
                      "tabOrder": {
                        "#type": "expression",
                        "value": "0"
                      }
                    },
                    {
                      "#type": "prop_stroke",
                      "#prop": "stroke"
                    },
                    {
                      "#type": "prop_fill",
                      "#prop": "fill"
                    },
                    {
                      "#prop": "",
                      "style": {
                        "#type": "json",
                        "base": "text",
                        "#props": [
                          {
                            "#type": "prop_text_style_font_size",
                            "#prop": "",
                            "fontSize": {
                              "#type": "expression",
                              "value": "32"
                            }
                          },
                          {
                            "#type": "prop_text_style_fill",
                            "#prop": "",
                            "fill": "black"
                          },
                          {
                            "#type": "prop_text_style_stroke",
                            "#prop": "",
                            "stroke": "white"
                          },
                          {
                            "#type": "prop_text_style_stroke_thickness",
                            "#prop": "",
                            "strokeThickness": {
                              "#type": "expression",
                              "value": "2"
                            }
                          }
                        ]
                      }
                    }
                  ],
                  "#init": "algorithmic_input"
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "300"
            },
            "y": {
              "#type": "expression",
              "value": "220"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "The chance to land on the"
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "36"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                },
                {
                  "#type": "prop_text_style_wordwrap",
                  "#prop": "",
                  "wordWrap": {
                    "#type": "expression",
                    "value": "true"
                  }
                },
                {
                  "#type": "prop_text_style_wordWrapWidth",
                  "#prop": "",
                  "wordWrapWidth": {
                    "#type": "expression",
                    "value": "550"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "300"
            },
            "y": {
              "#type": "expression",
              "value": "320"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "${re_color(color[0])} part:${' '}"
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "36"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                },
                {
                  "#type": "prop_text_style_wordwrap",
                  "#prop": "",
                  "wordWrap": {
                    "#type": "expression",
                    "value": "true"
                  }
                },
                {
                  "#type": "prop_text_style_wordWrapWidth",
                  "#prop": "",
                  "wordWrapWidth": {
                    "#type": "expression",
                    "value": "550"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "list_shape",
        "#props": [
          {
            "#type": "prop_list_direction",
            "#prop": "",
            "dir": "vertical"
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "color[0] == 'g' || color[0] == 'y' || color[0] == 'p' || color[0] == 'o' ? 480:450"
            },
            "y": {
              "#type": "expression",
              "value": "320"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_spacing",
            "#prop": "",
            "spacing": {
              "#type": "expression",
              "value": "10"
            }
          }
        ],
        "items": [
          {
            "#type": "json",
            "#props": [
              {
                "#type": "prop_list_align",
                "#prop": "",
                "align": "middle"
              }
            ],
            "template": {
              "#callback": "$item",
              "variable": "$item",
              "body": [
                {
                  "#type": "image_shape",
                  "#props": [
                    {
                      "#type": "prop_size",
                      "#prop": "",
                      "width": {
                        "#type": "expression",
                        "value": "80"
                      },
                      "height": {
                        "#type": "expression",
                        "value": "50"
                      }
                    },
                    {
                      "#type": "prop_image_key",
                      "#prop": "",
                      "key": "shape.png"
                    },
                    {
                      "#type": "prop_image_src",
                      "#prop": "",
                      "src": "${image_path}shape.png"
                    }
                  ]
                },
                {
                  "#type": "choice_input_shape",
                  "#props": [
                    {
                      "#type": "prop_value",
                      "#prop": "",
                      "value": {
                        "#type": "expression",
                        "value": "num_color[0]"
                      }
                    },
                    {
                      "#type": "prop_size",
                      "#prop": "",
                      "width": {
                        "#type": "expression",
                        "value": "80"
                      },
                      "height": {
                        "#type": "expression",
                        "value": "50"
                      }
                    },
                    {
                      "#type": "prop_input_keyboard",
                      "#prop": "",
                      "keyboard": "numbers1"
                    },
                    {
                      "#type": "prop_input_max_length",
                      "#prop": "",
                      "maxLength": {
                        "#type": "expression",
                        "value": "num_color[0].toString().length"
                      }
                    },
                    {
                      "#type": "prop_input_result_position",
                      "#prop": "",
                      "resultPosition": "right"
                    },
                    {
                      "#type": "prop_tab_order",
                      "#prop": "",
                      "tabOrder": {
                        "#type": "expression",
                        "value": "0"
                      }
                    },
                    {
                      "#type": "prop_stroke",
                      "#prop": "stroke"
                    },
                    {
                      "#type": "prop_fill",
                      "#prop": "fill"
                    },
                    {
                      "#prop": "",
                      "style": {
                        "#type": "json",
                        "base": "text",
                        "#props": [
                          {
                            "#type": "prop_text_style_font_size",
                            "#prop": "",
                            "fontSize": {
                              "#type": "expression",
                              "value": "32"
                            }
                          },
                          {
                            "#type": "prop_text_style_fill",
                            "#prop": "",
                            "fill": "black"
                          },
                          {
                            "#type": "prop_text_style_stroke",
                            "#prop": "",
                            "stroke": "white"
                          },
                          {
                            "#type": "prop_text_style_stroke_thickness",
                            "#prop": "",
                            "strokeThickness": {
                              "#type": "expression",
                              "value": "2"
                            }
                          }
                        ]
                      }
                    }
                  ],
                  "#init": "algorithmic_input"
                }
              ]
            }
          },
          {
            "#type": "json",
            "#props": [
              {
                "#type": "prop_list_align",
                "#prop": "",
                "align": "middle"
              }
            ],
            "template": {
              "#callback": "$item",
              "variable": "$item",
              "body": [
                {
                  "#type": "image_shape",
                  "#props": [
                    {
                      "#type": "prop_image_key",
                      "#prop": "",
                      "key": "line.png"
                    },
                    {
                      "#type": "prop_image_src",
                      "#prop": "",
                      "src": "${image_path}line.png"
                    }
                  ]
                }
              ]
            }
          },
          {
            "#type": "json",
            "#props": [
              {
                "#type": "prop_list_align",
                "#prop": "",
                "align": "middle"
              }
            ],
            "template": {
              "#callback": "$item",
              "variable": "$item",
              "body": [
                {
                  "#type": "image_shape",
                  "#props": [
                    {
                      "#type": "prop_size",
                      "#prop": "",
                      "width": {
                        "#type": "expression",
                        "value": "80"
                      },
                      "height": {
                        "#type": "expression",
                        "value": "50"
                      }
                    },
                    {
                      "#type": "prop_image_key",
                      "#prop": "",
                      "key": "shape.png"
                    },
                    {
                      "#type": "prop_image_src",
                      "#prop": "",
                      "src": "${image_path}shape.png"
                    }
                  ]
                },
                {
                  "#type": "choice_input_shape",
                  "#props": [
                    {
                      "#type": "prop_value",
                      "#prop": "",
                      "value": {
                        "#type": "expression",
                        "value": "part"
                      }
                    },
                    {
                      "#type": "prop_size",
                      "#prop": "",
                      "width": {
                        "#type": "expression",
                        "value": "80"
                      },
                      "height": {
                        "#type": "expression",
                        "value": "50"
                      }
                    },
                    {
                      "#type": "prop_input_keyboard",
                      "#prop": "",
                      "keyboard": "numbers1"
                    },
                    {
                      "#type": "prop_input_max_length",
                      "#prop": "",
                      "maxLength": {
                        "#type": "expression",
                        "value": "part.toString().length"
                      }
                    },
                    {
                      "#type": "prop_input_result_position",
                      "#prop": "",
                      "resultPosition": "right"
                    },
                    {
                      "#type": "prop_tab_order",
                      "#prop": "",
                      "tabOrder": {
                        "#type": "expression",
                        "value": "0"
                      }
                    },
                    {
                      "#type": "prop_stroke",
                      "#prop": "stroke"
                    },
                    {
                      "#type": "prop_fill",
                      "#prop": "fill"
                    },
                    {
                      "#prop": "",
                      "style": {
                        "#type": "json",
                        "base": "text",
                        "#props": [
                          {
                            "#type": "prop_text_style_font_size",
                            "#prop": "",
                            "fontSize": {
                              "#type": "expression",
                              "value": "32"
                            }
                          },
                          {
                            "#type": "prop_text_style_fill",
                            "#prop": "",
                            "fill": "black"
                          },
                          {
                            "#type": "prop_text_style_stroke",
                            "#prop": "",
                            "stroke": "white"
                          },
                          {
                            "#type": "prop_text_style_stroke_thickness",
                            "#prop": "",
                            "strokeThickness": {
                              "#type": "expression",
                              "value": "2"
                            }
                          }
                        ]
                      }
                    }
                  ],
                  "#init": "algorithmic_input"
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 1: Count the total number of parts on the spinner.</u>\n</br></br>\n  <center>\n<div style='position: relative; width:187px; height: 240px;'>\n  <img src='@sprite.src(spin.png)' style='position: absolute; left: 12px; top: 160px'/>"
        ]
      },
      {
        "#type": "variable",
        "name": "class_name",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "angle",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < part"
        },
        "do": [
          {
            "#type": "variable",
            "name": "class_name[i]",
            "value": {
              "#type": "expression",
              "value": "'angle' + i"
            }
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<style>\n   .${class_name[i]} {\n     position: absolute;\n        -ms-transform: rotate(${angle}deg);\n        /* IE 9 */\n        -webkit-transform: rotate(${angle}deg);\n        /* Safari */\n        transform: rotate(${angle}deg);\n        /* Standard syntax */\n    }\n</style>\n<div class=${class_name[i]}>\n<img src='@sprite.src(1-${part}${spiner[i]}.png)'/>\n  </div>"
            ]
          },
          {
            "#type": "variable",
            "name": "angle",
            "value": {
              "#type": "expression",
              "value": "angle + 360/part"
            }
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<img src='@sprite.src(point.png)' style='position: absolute; left: 62px; top: 40px'/>\n</div></center>\n</br>\n  <span style='background: rgb(250, 250, 250, 0.3)'>\nThe spinner has <b>${part}</b> parts.</br>\n\nTherefore there are a total of <b>${part}</b> possible outcomes.\n</span>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 2: Count the number of ${re_color(color[0])} parts on the spinner.</u>\n</br></br>\n  <center>\n<div style='position: relative; width:187px; height: 240px;'>\n  <img src='@sprite.src(spin.png)' style='position: absolute; left: 12px; top: 160px'/>"
        ]
      },
      {
        "#type": "variable",
        "name": "class_name",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "angle",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < part"
        },
        "do": [
          {
            "#type": "variable",
            "name": "class_name[i]",
            "value": {
              "#type": "expression",
              "value": "'angle' + i"
            }
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<style>\n   .${class_name[i]} {\n     position: absolute;\n        -ms-transform: rotate(${angle}deg);\n        /* IE 9 */\n        -webkit-transform: rotate(${angle}deg);\n        /* Safari */\n        transform: rotate(${angle}deg);\n        /* Standard syntax */\n    }\n</style>\n<div class=${class_name[i]}>\n<img src='@sprite.src(1-${part}${spiner[i]}.png)'/>\n  </div>"
            ]
          },
          {
            "#type": "variable",
            "name": "angle",
            "value": {
              "#type": "expression",
              "value": "angle + 360/part"
            }
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<img src='@sprite.src(point.png)' style='position: absolute; left: 62px; top: 40px'/>\n</div></center>\n</br>\n  <span style='background: rgb(250, 250, 250, 0.3)'>\nThe spinner has <b>${num_color[0]}</b> ${re_color(color[0])} ${num_color[0] > 1 ? 'parts':'part'}.\n</span>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 3: Identify the probabilities of landing on a ${re_color(color[0])} part.</u></br></br>\n\n<style>\n  td{text-align: center; padding: 10px}\n  </style>\n<table>\n  <tr>\n  <td style='border-bottom: 2px solid black'>${re_color(color[0])} parts</td>\n<td rowspan='2'>=</td>\n<td style='border-bottom: 2px solid black'>${num_color[0]}</td>\n</tr><tr>\n<td>total parts</td><td>${part}</td>\n</tr></table>\n\n<table>\n  <tr>\n  <td rowspan='2'>Chance to land on ${re_color(color[0])} part:</td>\n<td style='border-bottom: 3px solid black'><b>${num_color[0]}</b></td>\n</tr><tr>\n<td><b>${part}</b></td>\n</tr></table>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="0" y="0">
    <field name="name">Y5.SP.RPOSPIN.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y5.SP.RPOSPIN.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="input_param" id="xAl{^THC7-9|RHW:v!u~" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="xePe.o?Srstp2[(:-K8a">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="gLs`q:AE|q=i/QE27AXK" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="`IrPg,OjgQE{J[m~#{,7">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="Q6#2-|SwVM:VZAoKIvoT">
                                    <field name="name">range</field>
                                    <value name="value">
                                      <block type="expression" id="_mOr`{^q1jdI=@2R{S5$4">
                                        <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="statement" id="?1z?z6TM@2+L|:z8BF+v#">
                                        <field name="value">function re_color(color) {  
  if(color == 'g'){return 'green';}  
  if(color == 'r'){return 'red';}  
  if(color == 'b'){return 'blue';}
  if(color == 'o'){return 'orange';} 
  if(color == 'p'){return 'purple';} 
  if(color == 'y'){return 'yellow';}}
                                        </field>
                                        <next>
                                          <block type="if_then_else_block" id="^RDO;@2kUVp`jK@1K?J}R#">
                                            <value name="if">
                                              <block type="expression" id="7_V%^y~!08^IN%QG~_#k">
                                                <field name="value">range &lt; 0</field>
                                              </block>
                                            </value>
                                            <statement name="then">
                                              <block type="variable" id="iR0u:=n1:]`z?i/aPY[s">
                                                <field name="name">part</field>
                                                <value name="value">
                                                  <block type="expression" id="r=E${a#^^VVtEz?y$rql">
                                                    <field name="value">4</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </statement>
                                            <statement name="else">
                                              <block type="if_then_else_block" id="Ta`Py?:|,t=/0[`w)mju">
                                                <value name="if">
                                                  <block type="expression" id="9lqw-]Bl#5Dwgu}rCkZO">
                                                    <field name="value">range &lt; 4</field>
                                                  </block>
                                                </value>
                                                <statement name="then">
                                                  <block type="variable" id=".+e053?3^FMK.(rT?EG`">
                                                    <field name="name">part</field>
                                                    <value name="value">
                                                      <block type="random_one" id="|oe7I9o9V)!5i7!4ka9q">
                                                        <value name="items">
                                                          <block type="expression" id="%^02CnWQ[{8}A7Cnu(;v">
                                                            <field name="value">[4, 8]</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </statement>
                                                <statement name="else">
                                                  <block type="variable" id="CM(?x;u,_6Cyzyp~=cf}">
                                                    <field name="name">part</field>
                                                    <value name="value">
                                                      <block type="random_number" id="iC:OVQsk82(-1mRt^nW1">
                                                        <value name="min">
                                                          <block type="expression" id="=8L/6()X2Jz!DqOCR:.$">
                                                            <field name="value">2</field>
                                                          </block>
                                                        </value>
                                                        <value name="max">
                                                          <block type="expression" id="Dfahtjs35g%qHrF%wbQ/">
                                                            <field name="value">12</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </statement>
                                              </block>
                                            </statement>
                                            <next>
                                              <block type="variable" id="h!m;=`Qn3aD7pn1pF1f/">
                                                <field name="name">num_color</field>
                                                <value name="value">
                                                  <block type="expression" id="+|)2u%sOs@2@17lA9)Ua7b">
                                                    <field name="value">[]</field>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id=")yDC,6Ew-_C@1Gi-cgc42">
                                                    <field name="name">num_color[0]</field>
                                                    <value name="value">
                                                      <block type="random_number" id="K(!9n|{1`#v:.DB1K^1|">
                                                        <value name="min">
                                                          <block type="expression" id="8orC[$8F0^7jrVG?FkiU">
                                                            <field name="value">0</field>
                                                          </block>
                                                        </value>
                                                        <value name="max">
                                                          <block type="expression" id="$UY0(D{z?Ez86o%(+vSr">
                                                            <field name="value">part</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id="Cf!xD){x2rw4rHz{t!C~">
                                                        <field name="name">num_color[1]</field>
                                                        <value name="value">
                                                          <block type="expression" id="ykjm6i7WVN$kZiiO64fK">
                                                            <field name="value">part - num_color[0]</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="uq:LfoBJgnw0P^j@1CH(l">
                                                            <field name="name">color</field>
                                                            <value name="value">
                                                              <block type="random_many" id="xriTUM8Lz6%H56eVJUp+">
                                                                <value name="count">
                                                                  <block type="expression" id="UUeD(d_ZD5oBtVcWI?+-">
                                                                    <field name="value">2</field>
                                                                  </block>
                                                                </value>
                                                                <value name="items">
                                                                  <block type="expression" id="xJh)Vhf`YN!Z8O]mgNA1">
                                                                    <field name="value">['b', 'g', 'r', 'y', 'o', 'p']</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="variable" id="qa0tpg3[ad5^$$F0Qu]{">
                                                                <field name="name">qes_color</field>
                                                                <value name="value">
                                                                  <block type="expression" id="B@17;+KjDC|OT^`hx6a6;">
                                                                    <field name="value">color[0]</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="variable" id="1+lHlf!kHAuL4Vv:?Ewf">
                                                                    <field name="name">spiner</field>
                                                                    <value name="value">
                                                                      <block type="expression" id=",?+[e41qFl%@2oXQw;rOg">
                                                                        <field name="value">[]</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="variable" id="NPkN9P7AN2u0x!HRbI)`">
                                                                        <field name="name">i</field>
                                                                        <value name="value">
                                                                          <block type="expression" id="2Z71gT!CyJQ=-@2@1g(r[_">
                                                                            <field name="value">0</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="uo6SKuoVjzqj35S+~X88">
                                                                            <field name="name">count</field>
                                                                            <value name="value">
                                                                              <block type="expression" id="[rTP6WA|#4d{u_)K{/Tr">
                                                                                <field name="value">0</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="while_do_block" id=":/RZ.3-is$hZ{@1_9cIZp">
                                                                                <value name="while">
                                                                                  <block type="expression" id="IW+kD0#^%$)5,FkYRqPI">
                                                                                    <field name="value">i &lt; 2</field>
                                                                                  </block>
                                                                                </value>
                                                                                <statement name="do">
                                                                                  <block type="variable" id="59wb:a7NxQFL-r7%|zcR">
                                                                                    <field name="name">j</field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="_fEg^`%Z[LjtwDtG?Xmr">
                                                                                        <field name="value">0</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="while_do_block" id="5aqdQG]w:%Mi^bBfoc1w">
                                                                                        <value name="while">
                                                                                          <block type="expression" id="3AH2x.me,8i/DniXLQbt">
                                                                                            <field name="value">j &lt; num_color[i]</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <statement name="do">
                                                                                          <block type="variable" id="7lip;lU`69u0`(fH,a#v">
                                                                                            <field name="name">spiner[count]</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="ZO;bcS{lc~MOwnW@1}+8A">
                                                                                                <field name="value">color[i]</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="variable" id="eOn,hApnH%2PJ$wL@1SlG">
                                                                                                <field name="name">j</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="T^1O[]+JK/(c]tvibJZ8">
                                                                                                    <field name="value">j+1</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="variable" id="QdiLynMtpU!sm)!3bH[=">
                                                                                                    <field name="name">count</field>
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="MSJG3c~Ez0#}(gxVE%NT">
                                                                                                        <field name="value">count + 1</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="variable" id="?8mcr:o4iy]T5h26u[50">
                                                                                            <field name="name">i</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="F{Gp{XtIIwhmRV9(]xje">
                                                                                                <field name="value">i+1</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="variable" id="]_DbE:2bwm?-~jp{.XE4">
                                                                                    <field name="name">spiner</field>
                                                                                    <value name="value">
                                                                                      <block type="func_shuffle" id="g,R;@2B@1q@2D+f_yxwQXRN" inline="true">
                                                                                        <value name="value">
                                                                                          <block type="expression" id="y3P@2OP1Mu1fXA9mHFi?t">
                                                                                            <field name="value">spiner</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="image_shape" id="stnYv6tF0TZWG,%G.9|p">
                                                                                        <statement name="#props">
                                                                                          <block type="prop_position" id=",^c8(QC4F|4~3.QB@2=Vu">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="CE1+C2oNd:kHg0kH$Gh1">
                                                                                                <field name="value">150</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="7ÐY!PtEb]dtCb:G/Fl">
                                                                                                <field name="value">270</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_image_key" id="9;rtu5n-/l83HF96KaV+">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="key">
                                                                                                  <block type="string_value" id="f.w?Ga%LlwPU(~p-;0H4">
                                                                                                    <field name="value">spin.png</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_image_src" id="ZBey}(HSc^??M9~oZpN{">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="src">
                                                                                                      <block type="string_value" id="pFQ|o@1]Jqb/Q1:V)tOke">
                                                                                                        <field name="value">${image_path}spin.png</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="variable" id="ZiMU.6/J:z[7iqB)eYr2">
                                                                                            <field name="name">i</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="mKSt_{O{B;RceDv@1thXL">
                                                                                                <field name="value">0</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="variable" id=",p2xT#6n3b%{2xo+0iww">
                                                                                                <field name="name">angle</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="-syTZzyNYfMmE|=A6Ewh">
                                                                                                    <field name="value">0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="while_do_block" id="K}wG5;(koYvOG$$iq.-G">
                                                                                                    <value name="while">
                                                                                                      <block type="expression" id="kiqPCCV~mr+3I_6j=Pq{">
                                                                                                        <field name="value">i &lt; part</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <statement name="do">
                                                                                                      <block type="image_shape" id="B^c3a+e%!FB6?H9]q=$h">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_position" id="e5a=}@1n1sExE;w9?OCdZ">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="-ow}`u4yLk#a!oZB1DQv">
                                                                                                                <field name="value">150</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="I5Mx4g0gWX7as8is4?H{">
                                                                                                                <field name="value">150</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_image_key" id="8((h:PxpCq]R_lN~/zT-">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="key">
                                                                                                                  <block type="string_value" id="0U(!S.U1@1k+r(Zv^;8;z">
                                                                                                                    <field name="value">1-${part}${spiner[i]}.png</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_image_src" id="TypF1@2v]c_{%d;}PiukD">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="src">
                                                                                                                      <block type="string_value" id="nky@2i}N.~a;DnikVw2wL">
                                                                                                                        <field name="value">${image_path}1-${part}${spiner[i]}.png</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_angle" id="1v4$;GZyOa==B(E}{m:=">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="angle">
                                                                                                                          <block type="expression" id="g/HB@2J_XKaQk0/qFnb~v">
                                                                                                                            <field name="value">angle</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="variable" id="?488E%Xo#i1fZ+O-kG|w">
                                                                                                            <field name="name">angle</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="@1B/Aai4fPtX{wuGRLh=y">
                                                                                                                <field name="value">angle + 360/part</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="variable" id="TiZwhByNm_HmbLl-hTmK">
                                                                                                                <field name="name">i</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="zYd_HSBg%PPrbzS(_u_p">
                                                                                                                    <field name="value">i+1</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="image_shape" id="aeXIbpFRM}7~QMEqQ?6P">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_position" id="oJ[VS18lNuMXsn#+@1.$g">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="-!s(zkaq+(IvZOE$7RVn">
                                                                                                                <field name="value">150</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="z@2SC6N0gxa.fcANjy!vd">
                                                                                                                <field name="value">140</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_image_key" id="~J^8+#V;ax:?OV6jh)gv">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="key">
                                                                                                                  <block type="string_value" id="]_c~Lx:Qq2H))gO0dh#J">
                                                                                                                    <field name="value">point.png</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_image_src" id="lPOnero_qxG(R[kSU;!?">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="src">
                                                                                                                      <block type="string_value" id="=`GrP,Zgh$A-Q+rW0kSD">
                                                                                                                        <field name="value">${image_path}point.png</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                                                                                    <field name="value">300</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                                                                                    <field name="value">40</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_anchor" id="O.v@2mKn@1K#+tF;|}C}xI">
                                                                                                                    <field name="#prop">anchor</field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="+3%GU[X3l%%X`Jv1QL7@2">
                                                                                                                        <field name="value">0</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="Kzp23HjHF7k]A-y1BJPm">
                                                                                                                        <field name="value">0.5</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="contents">
                                                                                                                          <block type="string_value" id="#6btV9p,PGV}wMx.Uï¿½">
                                                                                                                            <field name="value">The total number of possible</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                                                                            <field name="base">text</field>
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="fontSize">
                                                                                                                                  <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                                                                                    <field name="value">36</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="fill">
                                                                                                                                      <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                                                                                        <field name="value">black</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="stroke">
                                                                                                                                          <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                                                                            <field name="value">white</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="strokeThickness">
                                                                                                                                              <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                                                                                <field name="value">2</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_text_style_wordwrap" id="CJd{@2bmj}Uq,Z5prTMAs">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="wordWrap">
                                                                                                                                                  <block type="expression" id="Y97qxe?F8.K3Ddw{2_Sm">
                                                                                                                                                    <field name="value">true</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_text_style_wordWrapWidth" id=")pj=LHhW1T?6H9o)TP0N">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="wordWrapWidth">
                                                                                                                                                      <block type="expression" id="4lYz0W[#eyw{ok:]DOCF">
                                                                                                                                                        <field name="value">550</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="list_shape" id="A?z7Lm}2Nf.RBFI?aDKK">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_list_direction" id="j[U#;PRMWXb94_RPD@1[,">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <field name="dir">horizontal</field>
                                                                                                                    <next>
                                                                                                                      <block type="prop_position" id="PFJZF9rEJuMef|_oZ~H:">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="XW~v35,TF^4Zf3#49,G,">
                                                                                                                            <field name="value">300</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="u[_AWcmxzPS@16i#sCgMI">
                                                                                                                            <field name="value">100</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_anchor" id="$RT3@1h;sxb^]KWWsC$id">
                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="6cU{7QKc7T?)~{vt)!Y]">
                                                                                                                                <field name="value">0</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="uzwn;{VJvp|h=;4R.MP.">
                                                                                                                                <field name="value">0.5</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <statement name="items">
                                                                                                                  <block type="list_item_shape" id=".C0q=OU.5h4^|DJ~Go{u">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_list_align" id="n(jeneq95=huy[8n=8/H">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <field name="align">middle</field>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <statement name="template">
                                                                                                                      <block type="text_shape" id="X!$zwk7NoD)4pg5:VXl_">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_text_contents" id="78/DrkBaB2f}s5x5.R3H">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="contents">
                                                                                                                              <block type="string_value" id="KN)cT!VW-yu-RCZB:~.|">
                                                                                                                                <field name="value">outcomes:${' '}</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style" id="xr6aHXrW9LL/#a5/ur3{">
                                                                                                                                <field name="base">text</field>
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_text_style_font_size" id=",/Ejk@2%5Q~%~xlB}^psu">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="fontSize">
                                                                                                                                      <block type="expression" id="yU%@119eEjkkSACiYNnDp">
                                                                                                                                        <field name="value">36</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_fill" id=":^S[e4}2d]55.?A`hjGT">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="fill">
                                                                                                                                          <block type="string_value" id="pi{7#C{UvG9dyt/[Mtv-">
                                                                                                                                            <field name="value">black</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style_stroke" id="`NHPP!dM(}U+QW=?bs3D">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="stroke">
                                                                                                                                              <block type="string_value" id="3v[hPY:lo=853JANl~lc">
                                                                                                                                                <field name="value">white</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_text_style_stroke_thickness" id="9)EPZ79@24euVy=Kt[@2Ii">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="strokeThickness">
                                                                                                                                                  <block type="expression" id="Fd2mF@2-wx0#U~`^uX}fC">
                                                                                                                                                    <field name="value">2</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="list_item_shape" id=";$.kEfNL5hJC4rK(c|MY">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_list_align" id="`-Uom[]@14Z/F/zvlkbDD">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <field name="align">middle</field>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <statement name="template">
                                                                                                                          <block type="image_shape" id="7ftwh4:efA@2VS-Pd7Ll;">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_size" id="q]cYinw0s!iDp3__HS_h">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="width">
                                                                                                                                  <block type="expression" id="j.IK)C$3MR6+m3myO]_R">
                                                                                                                                    <field name="value">80</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="height">
                                                                                                                                  <block type="expression" id="{1+nef)JoMYoYglSS[_N">
                                                                                                                                    <field name="value">50</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_key" id="b=3{]reMR0nw~S-zp7}3">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="key">
                                                                                                                                      <block type="string_value" id="@1~Vc]xht%sl2Aw;#Y$Od">
                                                                                                                                        <field name="value">shape.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_src" id="g0BqX7N|liSQbuKe,C@1s">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="src">
                                                                                                                                          <block type="string_value" id="Kn)K;]2`emg8#+kI{Ww8">
                                                                                                                                            <field name="value">${image_path}shape.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="algorithmic_input_shape" id="[e}LcmqF@19.%T?]@1jB-Z">
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_value" id="TN6clfe}:4H;@1k0w%+`S">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="@1Y0=VVDuz^gR8?]A.GgV">
                                                                                                                                        <field name="value">part</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_size" id="~)S$^+Glx|;{-)@1m?MCt">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="width">
                                                                                                                                          <block type="expression" id="wdRRw?lccA]3huvks^0:">
                                                                                                                                            <field name="value">80</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="height">
                                                                                                                                          <block type="expression" id="FlQ@1R-hpt5|W3YEsjl5h">
                                                                                                                                            <field name="value">50</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_input_keyboard" id="pX[+1]_P03Uw?r6RhxOm">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <field name="keyboard">numbers1</field>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_input_max_length" id="e^196@2S|6ukB7~2Bxi9C">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="maxLength">
                                                                                                                                                  <block type="expression" id="/_E;-7``ONG+]nS_NxH+">
                                                                                                                                                    <field name="value">part.toString().length</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_input_result_position" id="#LH4fkex}OZhG#[@2d;3P">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <field name="resultPosition">right</field>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_tab_order" id="s|F@1Izb{|@2lRY,asD!hu">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="tabOrder">
                                                                                                                                                          <block type="expression" id="2[il+_dgVQKQm?SvtFDj">
                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_stroke" id="D`nCliS/s1emx9euC0}t">
                                                                                                                                                            <field name="#prop">stroke</field>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_fill" id="Dz-AaH/!wx?BO[MH9dIz">
                                                                                                                                                                <field name="#prop">fill</field>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_text_style" id="!1Qr3{IxD@1PMJ]G^LlcN">
                                                                                                                                                                    <field name="base">text</field>
                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                      <block type="prop_text_style_font_size" id="!(nlSpda9T+o/iXBj}YF">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="fontSize">
                                                                                                                                                                          <block type="expression" id="FQTZZB_+$@1-hM?K.!NSO">
                                                                                                                                                                            <field name="value">32</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_text_style_fill" id="w-4`M-zsemYrO[}g`_|q">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="fill">
                                                                                                                                                                              <block type="string_value" id="G;VmNGg}u8OZR_#3M_Oe">
                                                                                                                                                                                <field name="value">black</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_text_style_stroke" id="ahppz0%Y{eD@2C$!vyJ4m">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="stroke">
                                                                                                                                                                                  <block type="string_value" id="}KNcD$1%M)+@1a=7A%7,,">
                                                                                                                                                                                    <field name="value">white</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_text_style_stroke_thickness" id="TBWlBG{V@2q@2LOjXi7|@16">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="strokeThickness">
                                                                                                                                                                                      <block type="expression" id="bs_awM@2W;X@1OG#fQ+@2no">
                                                                                                                                                                                        <field name="value">2</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="list_shape" id="dk;g5VJkG|S/oQ@2/5Po(">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_list_direction" id="t7G}U}L{QjRUd/?:h[m[">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <field name="dir">horizontal</field>
                                                                                                                        <next>
                                                                                                                          <block type="prop_position" id="OX_Na].7@2F?PEIesY+!J">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="Aj@2#[X@1z4xHq.$yJnpV#">
                                                                                                                                <field name="value">300</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="?j%H`Ce7our^d;%{t9r#">
                                                                                                                                <field name="value">160</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_anchor" id="3p%g%KhBUj]s$+5v_[V6">
                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="@1z@2UtKBiJz5iyocI;xr;">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="G.v$zm9N]?uDA%+9$O!2">
                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <statement name="items">
                                                                                                                      <block type="list_item_shape" id="s9V9NET!=2afwT}%4N}j">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_list_align" id="=.E|lsNyPGId^;^:7`N5">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <field name="align">middle</field>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <statement name="template">
                                                                                                                          <block type="text_shape" id="/C:?Hk8[`#_?ky|ZM0a.">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_text_contents" id="yzs/Oj?D{XZE:eTDPHP?">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="contents">
                                                                                                                                  <block type="string_value" id="$!S=Wks{aAm[Pxt,nMyU">
                                                                                                                                    <field name="value">The number of ${re_color(color[0])} parts:${' '}</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style" id=":{q@2-7wV1niUMwS.}@2F!">
                                                                                                                                    <field name="base">text</field>
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_text_style_font_size" id="fmT}Hn/i7[;FEJw@2(6=@1">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="fontSize">
                                                                                                                                          <block type="expression" id="/34O?RHWZ,q@1!BDpgk{z">
                                                                                                                                            <field name="value">36</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style_fill" id="#RxjI$%q/$[v8S5@2X:#^">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="fill">
                                                                                                                                              <block type="string_value" id="%ny_wz;[,smK:r48hWp:">
                                                                                                                                                <field name="value">black</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_text_style_stroke" id="^p_]Soz7$Px@2Y$Lel;3H">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="stroke">
                                                                                                                                                  <block type="string_value" id="/fR[E9qb2EoD7Y]Co=`B">
                                                                                                                                                    <field name="value">white</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_text_style_stroke_thickness" id="deMQ1=m=-mIjyUGk;8T/">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="strokeThickness">
                                                                                                                                                      <block type="expression" id="r1jQ6s0KXnqd;.BZ.d7,">
                                                                                                                                                        <field name="value">2</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="list_item_shape" id="|)6uqXE)rqdgp|Idsz=.">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_list_align" id=";!{+RcGJwJiBJ)OM46p^">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <field name="align">middle</field>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <statement name="template">
                                                                                                                              <block type="image_shape" id="h#}Eee%WpMhrOPi#NB@2C">
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_size" id="EyPXl6|@1E{z=AkVGd;6U">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="width">
                                                                                                                                      <block type="expression" id="%Eu587FM!0(ke]^m5EQy">
                                                                                                                                        <field name="value">80</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="height">
                                                                                                                                      <block type="expression" id="wmJV/l1Ats_SRm_:gJ{]">
                                                                                                                                        <field name="value">50</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_key" id="9Ng$nja-,0($t1!ItBrw">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="key">
                                                                                                                                          <block type="string_value" id="Y1HA/9SLvaqPZ_WsFDZu">
                                                                                                                                            <field name="value">shape.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_src" id="hPa3^56Ga1Hf_uM;mLX4">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="src">
                                                                                                                                              <block type="string_value" id="%U^:oG;bK@10E-i]yH|;[">
                                                                                                                                                <field name="value">${image_path}shape.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <next>
                                                                                                                                  <block type="algorithmic_input_shape" id="=j%JOwOnb@2H%R!jfp#c]">
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_value" id="SnGH]e)|~Io5;mLZKB-f">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="expression" id="vtYl:l}[T.T-d0FbhqKb">
                                                                                                                                            <field name="value">num_color[0]</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_size" id="0T!]@1DM@2z:6dae2yP1.@2">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="width">
                                                                                                                                              <block type="expression" id="^nJJciKc@1Yx]G`diZy8J">
                                                                                                                                                <field name="value">80</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="height">
                                                                                                                                              <block type="expression" id="0/d7lW!ea]OQgzkgd~,I">
                                                                                                                                                <field name="value">50</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_input_keyboard" id="/MQIhC+O:,:lLYcvgrMa">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <field name="keyboard">numbers1</field>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_input_max_length" id=";|F{Hz2eLb,74C@2@2Hnmp">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="maxLength">
                                                                                                                                                      <block type="expression" id="ggPRHE!!uspMYg6d?H8H">
                                                                                                                                                        <field name="value">num_color[0].toString().length</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_input_result_position" id="jZ{$$TyzMmv7jqsgp~oG">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <field name="resultPosition">right</field>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_tab_order" id="h$[nltM%dG:[0$0LW@2bF">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="tabOrder">
                                                                                                                                                              <block type="expression" id="qB#swQJ~@1LmNUfqI@1(/.">
                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_stroke" id="!;Q}j^bVmch6tdQ/L1D`">
                                                                                                                                                                <field name="#prop">stroke</field>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_fill" id="yV(XXHSJC}wr#.K1`DbZ">
                                                                                                                                                                    <field name="#prop">fill</field>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_text_style" id="[TlhWP+8s8;[Unh717d(">
                                                                                                                                                                        <field name="base">text</field>
                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                          <block type="prop_text_style_font_size" id="i.@2jC_k8^hpC2u:3`VyP">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="fontSize">
                                                                                                                                                                              <block type="expression" id="Xq.3J);6vMrPEUo.:8qk">
                                                                                                                                                                                <field name="value">32</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_text_style_fill" id="=^)GN4=K==$~@1D^dmN]{">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="fill">
                                                                                                                                                                                  <block type="string_value" id="~murqYDr@1@2N`aW#cO962">
                                                                                                                                                                                    <field name="value">black</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_text_style_stroke" id="bOxp;-5yt_OSvc!Xy7tM">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="stroke">
                                                                                                                                                                                      <block type="string_value" id="/bxqc8oz9Lv77k@2@1:F=o">
                                                                                                                                                                                        <field name="value">white</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="1u9~#hat}q!6GfKEy1m6">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="strokeThickness">
                                                                                                                                                                                          <block type="expression" id="/2B78^ttRCr%Csn3=O7l">
                                                                                                                                                                                            <field name="value">2</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="text_shape" id="PZ_uB?3n^o~rl[kI/c.p">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="krr0loxEhb@1QNQG2@1K@19">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="LJ]vQE@1TH!W,Zr?9n#/6">
                                                                                                                                <field name="value">300</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="4Fx2y|iEjOp4-L#iFNcF">
                                                                                                                                <field name="value">220</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_anchor" id="g4ruw^+Nm3);=C|~nbW~">
                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="K1]r/ahNlHnn,|N1|vQ~">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="Piw^x(DC?,.:ipHOi.A%">
                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_contents" id=";ov@1zo)UgR6lwQ/X;nin">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="contents">
                                                                                                                                      <block type="string_value" id="~|?tQ|u_5lV|/j(vv,PG">
                                                                                                                                        <field name="value">The chance to land on the</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style" id="m?Ua(lJ0!MUoOE$#8FhD">
                                                                                                                                        <field name="base">text</field>
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_text_style_font_size" id=")++c#x{$50hBrHod;T;F">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="fontSize">
                                                                                                                                              <block type="expression" id="1ln`V4H+$S0W`/u[uA~Z">
                                                                                                                                                <field name="value">36</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_text_style_fill" id="Fp=;Hi!si/tr/cbvy_Pa">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="fill">
                                                                                                                                                  <block type="string_value" id="iB5U54RAk6KftTJin!xT">
                                                                                                                                                    <field name="value">black</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_text_style_stroke" id="a##g^E@2;jxMWZ]t!{t=W">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="stroke">
                                                                                                                                                      <block type="string_value" id=":^7Kxp`lk4:yD?MNpMyd">
                                                                                                                                                        <field name="value">white</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="JIZv!0++a@1)27/u8Y@1P`">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="strokeThickness">
                                                                                                                                                          <block type="expression" id="^j)jG4Q0[DNICd}d9ULk">
                                                                                                                                                            <field name="value">2</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_text_style_wordwrap" id="eMslow0~eiv|q@2GQ=Cqd">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="wordWrap">
                                                                                                                                                              <block type="expression" id="K}mXn]p.Ooz_TROz}nVa">
                                                                                                                                                                <field name="value">true</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_text_style_wordWrapWidth" id="uEr+x2=;X:/4qo?Y+T?n">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="wordWrapWidth">
                                                                                                                                                                  <block type="expression" id="k@1kf1`jXdp?WO8mP)C=`">
                                                                                                                                                                    <field name="value">550</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="text_shape" id="r,xZ3=uXQm=asE]l3{k,">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="lh}1y^ShO@1@1bBa%k9]Ay">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="Oez`R63?r+_1u~d!nZzV">
                                                                                                                                    <field name="value">300</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="5-,N8Jh)+S;(aEJP^p@2Q">
                                                                                                                                    <field name="value">320</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_anchor" id="B4oS:(4W6?~-#4b)Z?O3">
                                                                                                                                    <field name="#prop">anchor</field>
                                                                                                                                    <value name="x">
                                                                                                                                      <block type="expression" id="32%4vM$ZSda4Uy:JAnv`">
                                                                                                                                        <field name="value">0</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="y">
                                                                                                                                      <block type="expression" id="ODg!_0b]U1;_cNH[d4zk">
                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_contents" id="tO7:@1AO#ns@24yvNErC]K">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="contents">
                                                                                                                                          <block type="string_value" id="=MfC{-Ob8kYeNG3u);,)">
                                                                                                                                            <field name="value">${re_color(color[0])} part:${' '}</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style" id="UnDG9!CdChbi`,}QWnPw">
                                                                                                                                            <field name="base">text</field>
                                                                                                                                            <statement name="#props">
                                                                                                                                              <block type="prop_text_style_font_size" id="NE4S:j85JB|Odg[jxp;b">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="fontSize">
                                                                                                                                                  <block type="expression" id="@1JK-rhB9z/?3maX$pdTo">
                                                                                                                                                    <field name="value">36</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_text_style_fill" id="hZUUo9vI)9AV.(@1g;,$0">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="fill">
                                                                                                                                                      <block type="string_value" id="?-_SH@1mOnbto/j@1oESVy">
                                                                                                                                                        <field name="value">black</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_text_style_stroke" id="pfG[01s3:~hy@1jO6k.vP">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="stroke">
                                                                                                                                                          <block type="string_value" id="G.guXidb1.xgNiBYWaQB">
                                                                                                                                                            <field name="value">white</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_text_style_stroke_thickness" id="Edkkcoe|aAIMT3Pm=k`a">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="strokeThickness">
                                                                                                                                                              <block type="expression" id="p3vp}on96N@19S(KEKEb-">
                                                                                                                                                                <field name="value">2</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_text_style_wordwrap" id="o8^7Hk9U[@1M0_..xEJJ/">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="wordWrap">
                                                                                                                                                                  <block type="expression" id="dUS_HKlY6U0kg,v}?TRF">
                                                                                                                                                                    <field name="value">true</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_text_style_wordWrapWidth" id="Gch[x-bH@1]~EXWs}}WM,">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="wordWrapWidth">
                                                                                                                                                                      <block type="expression" id="I,)9kit_)2;15Lb0kBc$">
                                                                                                                                                                        <field name="value">550</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="list_shape" id="5gR1Rv#78k!f)pgd$aHi">
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_list_direction" id="5ox+^tKB,my^[Jd%:?Z=">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <field name="dir">vertical</field>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_position" id="/o$sAJ=CG~_A`aK@1q,I/">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="expression" id=",x(Q),wu57huuPDKCz_0">
                                                                                                                                            <field name="value">color[0] == 'g' || color[0] == 'y' || color[0] == 'p' || color[0] == 'o' ? 480:450</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id="cVSM(4qiJ]bQ%RwZ!2Bf">
                                                                                                                                            <field name="value">320</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_anchor" id="#Q$c5IT(Za~m#]ph(0m@1">
                                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id="[jM8kOzax+dDYPeR5s`b">
                                                                                                                                                <field name="value">0</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="_8jM-OaYg4%;RKom{;dd">
                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_spacing" id="Zmj|CNF,q}vt6mODPJ?_">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="spacing">
                                                                                                                                                  <block type="expression" id="/i^k;]Z|tl0GE(AyB]|z">
                                                                                                                                                    <field name="value">10</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <statement name="items">
                                                                                                                                  <block type="list_item_shape" id="x:hNXN5wBQ6K#@1ZRDt)$">
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_list_align" id="t;;;OLn+.lg@2k+r!p5Tu">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <field name="align">middle</field>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <statement name="template">
                                                                                                                                      <block type="image_shape" id="@1rh@2|z-@24#:(T4Ekui+6">
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_size" id="rWg+1]5[R@2m|2svJW)k$">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="width">
                                                                                                                                              <block type="expression" id="8/l(G;.M0`t3R[::.#r1">
                                                                                                                                                <field name="value">80</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="height">
                                                                                                                                              <block type="expression" id="}0U#^i^?WkXsvXvdTy7@2">
                                                                                                                                                <field name="value">50</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_key" id="s78QeUw)UC:hZXGa@2k1q">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="key">
                                                                                                                                                  <block type="string_value" id="WcrZs#2=eJC-msRntwS.">
                                                                                                                                                    <field name="value">shape.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_image_src" id="p,v.p6Ja9m}Xz~e1iHD{">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="src">
                                                                                                                                                      <block type="string_value" id="pej/h?U%~,aJ(LcUce`+">
                                                                                                                                                        <field name="value">${image_path}shape.png</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <next>
                                                                                                                                          <block type="algorithmic_input_shape" id="y/@2!iT+#@1B#+bg44g0~_">
                                                                                                                                            <statement name="#props">
                                                                                                                                              <block type="prop_value" id="]bmh`PntNSp4?`j10|B)">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="expression" id="^)|F6lX)oo@2YC-6Gz9b#">
                                                                                                                                                    <field name="value">num_color[0]</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_size" id=".ahB9z!Q(sTcg@2ofzGc!">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="width">
                                                                                                                                                      <block type="expression" id="VSNTr.5fO%,L?0BV2Ccc">
                                                                                                                                                        <field name="value">80</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="height">
                                                                                                                                                      <block type="expression" id="R/W8FK?5X_{/e.bSOD?X">
                                                                                                                                                        <field name="value">50</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_input_keyboard" id="K%]eL9xlF%)OoUx3l:._">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <field name="keyboard">numbers1</field>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_input_max_length" id="IW}#.8Vhsm[REGsq4JOY">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="maxLength">
                                                                                                                                                              <block type="expression" id="@11`?`iemdDoA)N$fdAFd">
                                                                                                                                                                <field name="value">num_color[0].toString().length</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_input_result_position" id="Ry@2@1.bm9A;u)hEm0DsHM">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <field name="resultPosition">right</field>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_tab_order" id="[?P=6Av(h`Z-n]HjNegj">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="tabOrder">
                                                                                                                                                                      <block type="expression" id="?S=Zn{+EObzf;/W3Y~.T">
                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_stroke" id="lp8Ft.t61$oJIilihct_">
                                                                                                                                                                        <field name="#prop">stroke</field>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_fill" id=":Zf4{/-+D+rdyG@21n;UY">
                                                                                                                                                                            <field name="#prop">fill</field>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_text_style" id="oC+Gevg-KidA#Y$khlPc">
                                                                                                                                                                                <field name="base">text</field>
                                                                                                                                                                                <statement name="#props">
                                                                                                                                                                                  <block type="prop_text_style_font_size" id="[ttL|DO8}?[4:B$o)LpA">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="fontSize">
                                                                                                                                                                                      <block type="expression" id="W22!sw%xmV~PIwThxUX|">
                                                                                                                                                                                        <field name="value">32</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_text_style_fill" id="yK$H[2K:x:8|o?C$6W!]">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="fill">
                                                                                                                                                                                          <block type="string_value" id="u))PrJ.s{Qk``_$A6Sc/">
                                                                                                                                                                                            <field name="value">black</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_text_style_stroke" id="8sKy8}r4c}1$nR[_-ofc">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <value name="stroke">
                                                                                                                                                                                              <block type="string_value" id="`B9/dNd:[s-6GIxW^4@1o">
                                                                                                                                                                                                <field name="value">white</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_text_style_stroke_thickness" id="]c[8dc]a?-x%EoYq0bdL">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <value name="strokeThickness">
                                                                                                                                                                                                  <block type="expression" id="oxj%u{+Bl5163}n0#g9`">
                                                                                                                                                                                                    <field name="value">2</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </statement>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="list_item_shape" id="2tsHqYg$M@2)b4e1du,^E">
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_list_align" id="@2Of+=TS$=A-1_r`S]Ls`">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <field name="align">middle</field>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <statement name="template">
                                                                                                                                          <block type="image_shape" id="j0i~/q)!i%S)qA-do,h1">
                                                                                                                                            <statement name="#props">
                                                                                                                                              <block type="prop_image_key" id="8OK(Pm7z3T{w1Z]`7H`b">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="key">
                                                                                                                                                  <block type="string_value" id="tpW]Oi5(gRG{FeZK|M}y">
                                                                                                                                                    <field name="value">line.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_image_src" id="vGcANzmF+h8Ec%Of[ig7">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="src">
                                                                                                                                                      <block type="string_value" id="nC6;bAg=Rd;cq`tu]By3">
                                                                                                                                                        <field name="value">${image_path}line.png</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <next>
                                                                                                                                          <block type="list_item_shape" id=";D5z[xu5@1?y;x@1(fJjb)">
                                                                                                                                            <statement name="#props">
                                                                                                                                              <block type="prop_list_align" id="{|f8RO;g9Z?9/?^Tfcj}">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <field name="align">middle</field>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                            <statement name="template">
                                                                                                                                              <block type="image_shape" id="R^zq^`,mhoHQMJ^/9xJ;">
                                                                                                                                                <statement name="#props">
                                                                                                                                                  <block type="prop_size" id="BFa0da)9N,/~#[^a.R25">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="width">
                                                                                                                                                      <block type="expression" id="`|S$D;}p-`gt`Cm2[Iy{">
                                                                                                                                                        <field name="value">80</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="height">
                                                                                                                                                      <block type="expression" id="toM/1WQ/+YI,Pa9N@2}HW">
                                                                                                                                                        <field name="value">50</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_image_key" id="@2s_ybBg~hF@1n;g8wgxRv">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="key">
                                                                                                                                                          <block type="string_value" id="hfMLnY|gy2Q.zPBBbB#$">
                                                                                                                                                            <field name="value">shape.png</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_image_src" id="iDb(2XqORAwjUH=ll?:M">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="src">
                                                                                                                                                              <block type="string_value" id="9m@2Z4E{FUA0||;C2im$]">
                                                                                                                                                                <field name="value">${image_path}shape.png</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                                <next>
                                                                                                                                                  <block type="algorithmic_input_shape" id="Yb6;$B[g90a8b76sRDv3">
                                                                                                                                                    <statement name="#props">
                                                                                                                                                      <block type="prop_value" id="T$c!#0FaZzhX(=j{e;[w">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="g!G#{9)HpqZI.2Q[~uw$">
                                                                                                                                                            <field name="value">part</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_size" id="cmeE4wq8W@2-D`8LVumO3">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="width">
                                                                                                                                                              <block type="expression" id="o_SG%U{o/tQSY6Ac}LR;">
                                                                                                                                                                <field name="value">80</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="height">
                                                                                                                                                              <block type="expression" id="6yXCP;^3_SO)eu@1.}`3t">
                                                                                                                                                                <field name="value">50</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_input_keyboard" id=",5_,.^BvN%LACHI:[s]c">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <field name="keyboard">numbers1</field>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_input_max_length" id="t$3OkGP/g/b]F26#_2h~">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="maxLength">
                                                                                                                                                                      <block type="expression" id="Hc+PW5ye$B^L%dYc$KOy">
                                                                                                                                                                        <field name="value">part.toString().length</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_input_result_position" id="nq4eloKa9uR,6U;gV+)E">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <field name="resultPosition">right</field>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_tab_order" id="[?@2x#({gv;Y%Re3kC:S{">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="tabOrder">
                                                                                                                                                                              <block type="expression" id="/Ky|T?gwIyxSsMWO5~v|">
                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_stroke" id="u)=T:`FMkF9yDOYdu.%v">
                                                                                                                                                                                <field name="#prop">stroke</field>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_fill" id="6kyB65,HIt%QPV4[JkK!">
                                                                                                                                                                                    <field name="#prop">fill</field>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_text_style" id="vb@1gENAKt+4-7?8[~d4n">
                                                                                                                                                                                        <field name="base">text</field>
                                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                                          <block type="prop_text_style_font_size" id="w68^0PmsYr2XoS:(h/:k">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <value name="fontSize">
                                                                                                                                                                                              <block type="expression" id="oZHcj3M#p[@2k[YaL]R7~">
                                                                                                                                                                                                <field name="value">32</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_text_style_fill" id="T_Vw3n6_+(V`E9C:Sb4b">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <value name="fill">
                                                                                                                                                                                                  <block type="string_value" id="Txt-%T5i`.4)_JnQC-DH">
                                                                                                                                                                                                    <field name="value">black</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="prop_text_style_stroke" id="@1w;JGSanXvb:c-:2?[@1+">
                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                    <value name="stroke">
                                                                                                                                                                                                      <block type="string_value" id="=pX3G%8?}-GfbJFR]v{C">
                                                                                                                                                                                                        <field name="value">white</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="@13UQrNT=/p:!_@2h_Oh_n">
                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                        <value name="strokeThickness">
                                                                                                                                                                                                          <block type="expression" id="RsPYHW5jjj%l6.#]MaMz">
                                                                                                                                                                                                            <field name="value">2</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </statement>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <next>
                                                                                                                                  <block type="partial_explanation" id="?HUa7P9D!,_]UF@1BXO@1w" inline="true">
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="string_value" id="U0v7NuTLCw-]78AE[DLb">
                                                                                                                                        <field name="value">&lt;u&gt;Step 1: Count the total number of parts on the spinner.&lt;/u&gt;
&lt;/br&gt;&lt;/br&gt;
  &lt;center&gt;
&lt;div style='position: relative; width:187px; height: 240px;'&gt;
  &lt;img src='@1sprite.src(spin.png)' style='position: absolute; left: 12px; top: 160px'/&gt;
                                                                                                                                        </field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="variable" id="QwKx@1aRWRGKsqzZ7,FI8">
                                                                                                                                        <field name="name">class_name</field>
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="expression" id="7@1`3ApIj3q+tEQ3i^ú">
                                                                                                                                            <field name="value">[]</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="variable" id="p(Ujuqs{B5]:Kwr:7v[q">
                                                                                                                                            <field name="name">angle</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id="f]bi}c)KydRy3W%!JY)P">
                                                                                                                                                <field name="value">0</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="variable" id="imwMvNuiaSq84#SPYXx?">
                                                                                                                                                <field name="name">i</field>
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="expression" id="Up(?@25O}.Z$o`Obm2_/h">
                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="while_do_block" id="ThwFyg-8hP?Q@2Oj5LEdR">
                                                                                                                                                    <value name="while">
                                                                                                                                                      <block type="expression" id="?xkwt(0@21vC]y(-w^O1l">
                                                                                                                                                        <field name="value">i &lt; part</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <statement name="do">
                                                                                                                                                      <block type="variable" id="-r(+Lo1DbRNm9^b_^.XI">
                                                                                                                                                        <field name="name">class_name[i]</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="{S:HNziiej%.xkpp[3.`">
                                                                                                                                                            <field name="value">'angle' + i</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="partial_explanation" id="dJJ?=l,`r[YHxgZL(@1e{" inline="true">
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="string_value" id="bQcv]^pp@1VwFu{J9K4Th">
                                                                                                                                                                <field name="value">&lt;style&gt;
   .${class_name[i]} {
     position: absolute;
        -ms-transform: rotate(${angle}deg);
        /@2 IE 9 @2/
        -webkit-transform: rotate(${angle}deg);
        /@2 Safari @2/
        transform: rotate(${angle}deg);
        /@2 Standard syntax @2/
    }
&lt;/style&gt;
&lt;div class=${class_name[i]}&gt;
&lt;img src='@1sprite.src(1-${part}${spiner[i]}.png)'/&gt;
  &lt;/div&gt;
                                                                                                                                                                </field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="variable" id="CW1nG8T}T/Poq(EiH/.m">
                                                                                                                                                                <field name="name">angle</field>
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="expression" id="x9J.b$9q`:yIm[ÃC5)">
                                                                                                                                                                    <field name="value">angle + 360/part</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="variable" id="R-BI}In[ifO4fIVmWL:;">
                                                                                                                                                                    <field name="name">i</field>
                                                                                                                                                                    <value name="value">
                                                                                                                                                                      <block type="expression" id="C/:6uq]:@21h1OJbfW[|@1">
                                                                                                                                                                        <field name="value">i+1</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="partial_explanation" id="CcckQ{7/Bf+FKdcR8~L}" inline="true">
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="string_value" id="Qq.E4x7]OC)#N/d@2XLTe">
                                                                                                                                                            <field name="value">&lt;img src='@1sprite.src(point.png)' style='position: absolute; left: 62px; top: 40px'/&gt;
&lt;/div&gt;&lt;/center&gt;
&lt;/br&gt;
  &lt;span style='background: rgb(250, 250, 250, 0.3)'&gt;
The spinner has &lt;b&gt;${part}&lt;/b&gt; parts.&lt;/br&gt;

Therefore there are a total of &lt;b&gt;${part}&lt;/b&gt; possible outcomes.
&lt;/span&gt;
                                                                                                                                                            </field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="end_partial_explanation" id="Zz@2|nfE[CbÌ2|W%uKc">
                                                                                                                                                            <next>
                                                                                                                                                              <block type="partial_explanation" id="f$(Oj$GqcC=VI(t|.]UQ" inline="true">
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="string_value" id="2NPEQ[VoTK-zcVJy}DjH">
                                                                                                                                                                    <field name="value">&lt;u&gt;Step 2: Count the number of ${re_color(color[0])} parts on the spinner.&lt;/u&gt;
&lt;/br&gt;&lt;/br&gt;
  &lt;center&gt;
&lt;div style='position: relative; width:187px; height: 240px;'&gt;
  &lt;img src='@1sprite.src(spin.png)' style='position: absolute; left: 12px; top: 160px'/&gt;
                                                                                                                                                                    </field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="variable" id="XjrFHMV^)/(XmeA`6ZO!">
                                                                                                                                                                    <field name="name">class_name</field>
                                                                                                                                                                    <value name="value">
                                                                                                                                                                      <block type="expression" id="aKvgO-(J=+H%NrJ:#fCT">
                                                                                                                                                                        <field name="value">[]</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="variable" id="||gVOi-tI(YLBVTNR_wT">
                                                                                                                                                                        <field name="name">angle</field>
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="expression" id="06~CAtnxb_Fj`PAO-MW@1">
                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="variable" id="3E:32%S~8|d}c7dN|Y]/">
                                                                                                                                                                            <field name="name">i</field>
                                                                                                                                                                            <value name="value">
                                                                                                                                                                              <block type="expression" id="Yt#D.M;hIAHQ@1N9i@1DY7">
                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="while_do_block" id="@2$7%7.%,X$it3t,:S1rF">
                                                                                                                                                                                <value name="while">
                                                                                                                                                                                  <block type="expression" id="Zt_6$k%|f^_xm:6Xil-|">
                                                                                                                                                                                    <field name="value">i &lt; part</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <statement name="do">
                                                                                                                                                                                  <block type="variable" id="3;l$$3NrgnxTs]SI$_S`">
                                                                                                                                                                                    <field name="name">class_name[i]</field>
                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                      <block type="expression" id="M}Rgh#3hq]^q~5Z.EY.f">
                                                                                                                                                                                        <field name="value">'angle' + i</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="partial_explanation" id="0,lj72?yrxet%.ekw3@1g" inline="true">
                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                          <block type="string_value" id="5/=IjkeVkzj3dxx)7zyC">
                                                                                                                                                                                            <field name="value">&lt;style&gt;
   .${class_name[i]} {
     position: absolute;
        -ms-transform: rotate(${angle}deg);
        /@2 IE 9 @2/
        -webkit-transform: rotate(${angle}deg);
        /@2 Safari @2/
        transform: rotate(${angle}deg);
        /@2 Standard syntax @2/
    }
&lt;/style&gt;
&lt;div class=${class_name[i]}&gt;
&lt;img src='@1sprite.src(1-${part}${spiner[i]}.png)'/&gt;
  &lt;/div&gt;
                                                                                                                                                                                            </field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="variable" id="%Lp_,7t-2gq#Qw]64O-E">
                                                                                                                                                                                            <field name="name">angle</field>
                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                              <block type="expression" id="O=[XT2#MZ$Nze%GivmLw">
                                                                                                                                                                                                <field name="value">angle + 360/part</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="variable" id="!n/w|k$sY.vvl36oUwE~">
                                                                                                                                                                                                <field name="name">i</field>
                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                  <block type="expression" id="YluU(y1h)l2T?z7V?o+h">
                                                                                                                                                                                                    <field name="value">i+1</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </statement>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="partial_explanation" id="ByoBEAGWnF~v[ahU7+VN" inline="true">
                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                      <block type="string_value" id="?o){srx1|Q$LrcaM4F5%">
                                                                                                                                                                                        <field name="value">&lt;img src='@1sprite.src(point.png)' style='position: absolute; left: 62px; top: 40px'/&gt;
&lt;/div&gt;&lt;/center&gt;
&lt;/br&gt;
  &lt;span style='background: rgb(250, 250, 250, 0.3)'&gt;
The spinner has &lt;b&gt;${num_color[0]}&lt;/b&gt; ${re_color(color[0])} ${num_color[0] &gt; 1 ? 'parts':'part'}.
&lt;/span&gt;
                                                                                                                                                                                        </field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="end_partial_explanation" id="@1cIdLWF2l6b).ZmYP+hT">
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="partial_explanation" id="Y~s`S^nYdD=e5;sJhlr^" inline="true">
                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                              <block type="string_value" id="G{Zt9lHfh!?,pjGbNw3i">
                                                                                                                                                                                                <field name="value">&lt;u&gt;Step 3: Identify the probabilities of landing on a ${re_color(color[0])} part.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;style&gt;
  td{text-align: center; padding: 10px}
  &lt;/style&gt;
&lt;table&gt;
  &lt;tr&gt;
  &lt;td style='border-bottom: 2px solid black'&gt;${re_color(color[0])} parts&lt;/td&gt;
&lt;td rowspan='2'&gt;=&lt;/td&gt;
&lt;td style='border-bottom: 2px solid black'&gt;${num_color[0]}&lt;/td&gt;
&lt;/tr&gt;&lt;tr&gt;
&lt;td&gt;total parts&lt;/td&gt;&lt;td&gt;${part}&lt;/td&gt;
&lt;/tr&gt;&lt;/table&gt;

&lt;table&gt;
  &lt;tr&gt;
  &lt;td rowspan='2'&gt;Chance to land on ${re_color(color[0])} part:&lt;/td&gt;
&lt;td style='border-bottom: 3px solid black'&gt;&lt;b&gt;${num_color[0]}&lt;/b&gt;&lt;/td&gt;
&lt;/tr&gt;&lt;tr&gt;
&lt;td&gt;&lt;b&gt;${part}&lt;/b&gt;&lt;/td&gt;
&lt;/tr&gt;&lt;/table&gt;
                                                                                                                                                                                                </field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="end_partial_explanation" id="b}0#iXwb%PmO({L/@1sp!"></block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
</xml>
END_XML]] */