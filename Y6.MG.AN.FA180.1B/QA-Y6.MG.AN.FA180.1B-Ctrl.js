
module.exports = [
  {
    "#type": "question",
    "name": "Y6.MG.AN.FA180.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y6.MG.AN.FA180.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "range < 0"
        },
        "then": [
          {
            "#type": "variable",
            "name": "list_angle",
            "value": {
              "#type": "expression",
              "value": "[30, 60, 90, 120, 150, 180]"
            }
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "range < 4"
            },
            "then": [
              {
                "#type": "variable",
                "name": "list_angle",
                "value": {
                  "#type": "expression",
                  "value": "[20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170]"
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "list_angle",
                "value": {
                  "#type": "expression",
                  "value": "[20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150, 155, 160, 165, 170]"
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "statement",
        "value": "function Ox(a){\n  if(a < 60){return 50;}\n  else if(a < 110) {return 30;}\n  else {return 5;}\n}"
      },
      {
        "#type": "variable",
        "name": "type",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "4"
          }
        }
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "variable",
            "name": "angle",
            "value": {
              "#type": "random_many",
              "count": {
                "#type": "expression",
                "value": "3"
              },
              "items": {
                "#type": "expression",
                "value": "list_angle"
              }
            }
          },
          {
            "#type": "variable",
            "name": "a",
            "value": {
              "#type": "random_one",
              "items": {
                "#type": "expression",
                "value": "angle"
              }
            }
          },
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "550"
                },
                "y": {
                  "#type": "expression",
                  "value": "50"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "Which angle is ${a}°?"
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "35"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    },
                    {
                      "#type": "prop_text_style_wordwrap",
                      "#prop": "",
                      "wordWrap": {
                        "#type": "expression",
                        "value": "true"
                      }
                    },
                    {
                      "#type": "prop_text_style_wordWrapWidth",
                      "#prop": "",
                      "wordWrapWidth": {
                        "#type": "expression",
                        "value": "500"
                      }
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "choice_custom_shape",
            "action": "drag",
            "value": {
              "#type": "expression",
              "value": "undefined"
            },
            "template": {
              "#callback": "$choice",
              "variable": "$choice",
              "body": [
                {
                  "#type": "image_shape",
                  "#props": [
                    {
                      "#type": "prop_position",
                      "#prop": "",
                      "x": {
                        "#type": "expression",
                        "value": "200"
                      },
                      "y": {
                        "#type": "expression",
                        "value": "5"
                      }
                    },
                    {
                      "#type": "prop_anchor",
                      "#prop": "anchor",
                      "x": {
                        "#type": "expression",
                        "value": "0.5"
                      },
                      "y": {
                        "#type": "expression",
                        "value": "0"
                      }
                    },
                    {
                      "#type": "prop_max_size",
                      "#prop": "",
                      "maxWidth": {
                        "#type": "expression",
                        "value": "300"
                      },
                      "maxHeight": {
                        "#type": "expression",
                        "value": "0"
                      }
                    },
                    {
                      "#type": "prop_image_key",
                      "#prop": "",
                      "key": "pro.png"
                    },
                    {
                      "#type": "prop_image_src",
                      "#prop": "",
                      "src": "${image_path}pro.png"
                    }
                  ]
                }
              ]
            }
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "3"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "300"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "780"
                },
                "height": {
                  "#type": "expression",
                  "value": "200"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "angle"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "240"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "200"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "box.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}box.png"
                      }
                    ]
                  },
                  {
                    "#type": "if_then_else_block",
                    "if": {
                      "#type": "expression",
                      "value": "$cell.data == 90"
                    },
                    "then": [
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX - Ox($cell.data)"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY + 50"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "sq.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}sq.png"
                          }
                        ]
                      }
                    ],
                    "else": [
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX - Ox($cell.data)"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY + 50"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "cl.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}cl.png"
                          },
                          {
                            "#type": "prop_angle",
                            "#prop": "",
                            "angle": {
                              "#type": "expression",
                              "value": "360 - $cell.data"
                            }
                          }
                        ]
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX - Ox($cell.data)"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 50"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "liner.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}liner.png"
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX - Ox($cell.data)"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 50"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "linel.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}linel.png"
                      },
                      {
                        "#type": "prop_angle",
                        "#prop": "",
                        "angle": {
                          "#type": "expression",
                          "value": "180 - $cell.data"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX - Ox($cell.data)"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 50"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "point.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}point.png"
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "3"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "300"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "780"
                },
                "height": {
                  "#type": "expression",
                  "value": "200"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "[0, 1, 2]"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "choice_custom_shape",
                    "action": "click-one",
                    "value": {
                      "#type": "expression",
                      "value": "angle[$cell.data]"
                    },
                    "template": {
                      "#callback": "$choice",
                      "variable": "$choice",
                      "body": [
                        {
                          "#type": "image_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "$cell.centerX"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "$cell.centerY"
                              }
                            },
                            {
                              "#type": "prop_image_key",
                              "#prop": "",
                              "key": "hide.png"
                            },
                            {
                              "#type": "prop_image_src",
                              "#prop": "",
                              "src": "${image_path}hide.png"
                            }
                          ]
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "func",
            "name": "addAnswers",
            "args": [
              {
                "#type": "expression",
                "value": "a"
              }
            ]
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 2"
        },
        "then": [
          {
            "#type": "variable",
            "name": "a",
            "value": {
              "#type": "random_one",
              "items": {
                "#type": "expression",
                "value": "list_angle"
              }
            }
          },
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "40"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "${ type == 2 ? 'What is the size of the angle?' : 'Find the unknown angle.'}"
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "35"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    },
                    {
                      "#type": "prop_text_style_wordwrap",
                      "#prop": "",
                      "wordWrap": {
                        "#type": "expression",
                        "value": "true"
                      }
                    },
                    {
                      "#type": "prop_text_style_wordWrapWidth",
                      "#prop": "",
                      "wordWrapWidth": {
                        "#type": "expression",
                        "value": "500"
                      }
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "choice_custom_shape",
            "action": "drag",
            "value": {
              "#type": "expression",
              "value": "undefined"
            },
            "template": {
              "#callback": "$choice",
              "variable": "$choice",
              "body": [
                {
                  "#type": "image_shape",
                  "#props": [
                    {
                      "#type": "prop_position",
                      "#prop": "",
                      "x": {
                        "#type": "expression",
                        "value": "600"
                      },
                      "y": {
                        "#type": "expression",
                        "value": "380"
                      }
                    },
                    {
                      "#type": "prop_anchor",
                      "#prop": "anchor",
                      "x": {
                        "#type": "expression",
                        "value": "0.5"
                      },
                      "y": {
                        "#type": "expression",
                        "value": "1"
                      }
                    },
                    {
                      "#type": "prop_max_size",
                      "#prop": "",
                      "maxWidth": {
                        "#type": "expression",
                        "value": "300"
                      },
                      "maxHeight": {
                        "#type": "expression",
                        "value": "0"
                      }
                    },
                    {
                      "#type": "prop_image_key",
                      "#prop": "",
                      "key": "pro.png"
                    },
                    {
                      "#type": "prop_image_src",
                      "#prop": "",
                      "src": "${image_path}pro.png"
                    }
                  ]
                }
              ]
            }
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "250"
                },
                "y": {
                  "#type": "expression",
                  "value": "200"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "300"
                },
                "height": {
                  "#type": "expression",
                  "value": "200"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "240"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "200"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "box.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}box.png"
                      }
                    ]
                  },
                  {
                    "#type": "if_then_else_block",
                    "if": {
                      "#type": "expression",
                      "value": "$cell.data == 90"
                    },
                    "then": [
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX - Ox(a)"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY + 50"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "sq.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}sq.png"
                          }
                        ]
                      }
                    ],
                    "else": [
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX - Ox(a)"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY + 50"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "cl.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}cl.png"
                          },
                          {
                            "#type": "prop_angle",
                            "#prop": "",
                            "angle": {
                              "#type": "expression",
                              "value": "360 - a"
                            }
                          }
                        ]
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX - Ox(a)"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 50"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "liner.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}liner.png"
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX - Ox(a)"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 50"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "linel.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}linel.png"
                      },
                      {
                        "#type": "prop_angle",
                        "#prop": "",
                        "angle": {
                          "#type": "expression",
                          "value": "180 - a"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX - Ox(a)"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 50"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "point.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}point.png"
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "600"
                },
                "y": {
                  "#type": "expression",
                  "value": "150"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "shape.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}shape.png"
              }
            ]
          },
          {
            "#type": "choice_input_shape",
            "#props": [
              {
                "#type": "prop_value",
                "#prop": "",
                "value": {
                  "#type": "expression",
                  "value": "a"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "600"
                },
                "y": {
                  "#type": "expression",
                  "value": "150"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "125"
                },
                "height": {
                  "#type": "expression",
                  "value": "68"
                }
              },
              {
                "#type": "prop_input_keyboard",
                "#prop": "",
                "keyboard": "numbers1"
              },
              {
                "#type": "prop_input_max_length",
                "#prop": "",
                "maxLength": {
                  "#type": "expression",
                  "value": "a.toString().length"
                }
              },
              {
                "#type": "prop_input_result_position",
                "#prop": "",
                "resultPosition": "bottom"
              },
              {
                "#type": "prop_tab_order",
                "#prop": "",
                "tabOrder": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_stroke",
                "#prop": "stroke"
              },
              {
                "#type": "prop_fill",
                "#prop": "fill"
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "32"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ],
            "#init": "algorithmic_input"
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 3"
        },
        "then": [
          {
            "#type": "variable",
            "name": "angle",
            "value": {
              "#type": "random_many",
              "count": {
                "#type": "expression",
                "value": "2"
              },
              "items": {
                "#type": "expression",
                "value": "list_angle"
              }
            }
          },
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "Use a protractor to measure each angle."
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "35"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "choice_custom_shape",
            "action": "drag",
            "value": {
              "#type": "expression",
              "value": "undefined"
            },
            "template": {
              "#callback": "$choice",
              "variable": "$choice",
              "body": [
                {
                  "#type": "image_shape",
                  "#props": [
                    {
                      "#type": "prop_position",
                      "#prop": "",
                      "x": {
                        "#type": "expression",
                        "value": "0"
                      },
                      "y": {
                        "#type": "expression",
                        "value": "150"
                      }
                    },
                    {
                      "#type": "prop_anchor",
                      "#prop": "anchor",
                      "x": {
                        "#type": "expression",
                        "value": "0"
                      },
                      "y": {
                        "#type": "expression",
                        "value": "0.5"
                      }
                    },
                    {
                      "#type": "prop_max_size",
                      "#prop": "",
                      "maxWidth": {
                        "#type": "expression",
                        "value": "300"
                      },
                      "maxHeight": {
                        "#type": "expression",
                        "value": "0"
                      }
                    },
                    {
                      "#type": "prop_image_key",
                      "#prop": "",
                      "key": "pro.png"
                    },
                    {
                      "#type": "prop_image_src",
                      "#prop": "",
                      "src": "${image_path}pro.png"
                    }
                  ]
                }
              ]
            }
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "2"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "800"
                },
                "y": {
                  "#type": "expression",
                  "value": "150"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "500"
                },
                "height": {
                  "#type": "expression",
                  "value": "200"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "1"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "angle"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "240"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "200"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "box.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}box.png"
                      }
                    ]
                  },
                  {
                    "#type": "if_then_else_block",
                    "if": {
                      "#type": "expression",
                      "value": "$cell.data == 90"
                    },
                    "then": [
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX - Ox($cell.data)"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY + 50"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "sq.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}sq.png"
                          }
                        ]
                      }
                    ],
                    "else": [
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX - Ox($cell.data)"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY + 50"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "cl.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}cl.png"
                          },
                          {
                            "#type": "prop_angle",
                            "#prop": "",
                            "angle": {
                              "#type": "expression",
                              "value": "360 - $cell.data"
                            }
                          }
                        ]
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX - Ox($cell.data)"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 50"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "liner.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}liner.png"
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX - Ox($cell.data)"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 50"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "linel.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}linel.png"
                      },
                      {
                        "#type": "prop_angle",
                        "#prop": "",
                        "angle": {
                          "#type": "expression",
                          "value": "180 - $cell.data"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX - Ox($cell.data)"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 50"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "point.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}point.png"
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "list_shape",
            "#props": [
              {
                "#type": "prop_list_direction",
                "#prop": "",
                "dir": "horizontal"
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "295"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              }
            ],
            "items": [
              {
                "#type": "json",
                "#props": [
                  {
                    "#type": "prop_list_align",
                    "#prop": "",
                    "align": "middle"
                  }
                ],
                "template": {
                  "#callback": "$item",
                  "variable": "$item",
                  "body": [
                    {
                      "#type": "text_shape",
                      "#props": [
                        {
                          "#type": "prop_anchor",
                          "#prop": "anchor",
                          "x": {
                            "#type": "expression",
                            "value": "0.5"
                          },
                          "y": {
                            "#type": "expression",
                            "value": "0.5"
                          }
                        },
                        {
                          "#type": "prop_text_contents",
                          "#prop": "",
                          "contents": "Angle 1 ="
                        },
                        {
                          "#prop": "",
                          "style": {
                            "#type": "json",
                            "base": "text",
                            "#props": [
                              {
                                "#type": "prop_text_style_font_size",
                                "#prop": "",
                                "fontSize": {
                                  "#type": "expression",
                                  "value": "36"
                                }
                              },
                              {
                                "#type": "prop_text_style_fill",
                                "#prop": "",
                                "fill": "black"
                              },
                              {
                                "#type": "prop_text_style_stroke",
                                "#prop": "",
                                "stroke": "white"
                              },
                              {
                                "#type": "prop_text_style_stroke_thickness",
                                "#prop": "",
                                "strokeThickness": {
                                  "#type": "expression",
                                  "value": "2"
                                }
                              }
                            ]
                          }
                        }
                      ]
                    }
                  ]
                }
              },
              {
                "#type": "json",
                "#props": [
                  {
                    "#type": "prop_list_align",
                    "#prop": "",
                    "align": "middle"
                  }
                ],
                "template": {
                  "#callback": "$item",
                  "variable": "$item",
                  "body": [
                    {
                      "#type": "image_shape",
                      "#props": [
                        {
                          "#type": "prop_image_key",
                          "#prop": "",
                          "key": "shape.png"
                        },
                        {
                          "#type": "prop_image_src",
                          "#prop": "",
                          "src": "${image_path}shape.png"
                        },
                        {
                          "#type": "prop_scale",
                          "#prop": "",
                          "scale": {
                            "#type": "expression",
                            "value": "0.7"
                          }
                        }
                      ]
                    },
                    {
                      "#type": "choice_input_shape",
                      "#props": [
                        {
                          "#type": "prop_value",
                          "#prop": "",
                          "value": {
                            "#type": "expression",
                            "value": "angle[0]"
                          }
                        },
                        {
                          "#type": "prop_size",
                          "#prop": "",
                          "width": {
                            "#type": "expression",
                            "value": "120*0.7"
                          },
                          "height": {
                            "#type": "expression",
                            "value": "65*0.7"
                          }
                        },
                        {
                          "#type": "prop_input_keyboard",
                          "#prop": "",
                          "keyboard": "numbers1"
                        },
                        {
                          "#type": "prop_input_max_length",
                          "#prop": "",
                          "maxLength": {
                            "#type": "expression",
                            "value": "angle[0].toString().length"
                          }
                        },
                        {
                          "#type": "prop_input_result_position",
                          "#prop": "",
                          "resultPosition": "bottom"
                        },
                        {
                          "#type": "prop_tab_order",
                          "#prop": "",
                          "tabOrder": {
                            "#type": "expression",
                            "value": "0"
                          }
                        },
                        {
                          "#type": "prop_stroke",
                          "#prop": "stroke"
                        },
                        {
                          "#type": "prop_fill",
                          "#prop": "fill"
                        },
                        {
                          "#prop": "",
                          "style": {
                            "#type": "json",
                            "base": "text",
                            "#props": [
                              {
                                "#type": "prop_text_style_font_size",
                                "#prop": "",
                                "fontSize": {
                                  "#type": "expression",
                                  "value": "32"
                                }
                              },
                              {
                                "#type": "prop_text_style_fill",
                                "#prop": "",
                                "fill": "black"
                              },
                              {
                                "#type": "prop_text_style_stroke",
                                "#prop": "",
                                "stroke": "white"
                              },
                              {
                                "#type": "prop_text_style_stroke_thickness",
                                "#prop": "",
                                "strokeThickness": {
                                  "#type": "expression",
                                  "value": "2"
                                }
                              }
                            ]
                          }
                        }
                      ],
                      "#init": "algorithmic_input"
                    }
                  ]
                }
              },
              {
                "#type": "json",
                "#props": [
                  {
                    "#type": "prop_list_align",
                    "#prop": "",
                    "align": "middle"
                  }
                ],
                "template": {
                  "#callback": "$item",
                  "variable": "$item",
                  "body": [
                    {
                      "#type": "text_shape",
                      "#props": [
                        {
                          "#type": "prop_anchor",
                          "#prop": "anchor",
                          "x": {
                            "#type": "expression",
                            "value": "0.5"
                          },
                          "y": {
                            "#type": "expression",
                            "value": "0.5"
                          }
                        },
                        {
                          "#type": "prop_text_contents",
                          "#prop": "",
                          "contents": "Angle 2 ="
                        },
                        {
                          "#prop": "",
                          "style": {
                            "#type": "json",
                            "base": "text",
                            "#props": [
                              {
                                "#type": "prop_text_style_font_size",
                                "#prop": "",
                                "fontSize": {
                                  "#type": "expression",
                                  "value": "36"
                                }
                              },
                              {
                                "#type": "prop_text_style_fill",
                                "#prop": "",
                                "fill": "black"
                              },
                              {
                                "#type": "prop_text_style_stroke",
                                "#prop": "",
                                "stroke": "white"
                              },
                              {
                                "#type": "prop_text_style_stroke_thickness",
                                "#prop": "",
                                "strokeThickness": {
                                  "#type": "expression",
                                  "value": "2"
                                }
                              }
                            ]
                          }
                        }
                      ]
                    }
                  ]
                }
              },
              {
                "#type": "json",
                "#props": [
                  {
                    "#type": "prop_list_align",
                    "#prop": "",
                    "align": "middle"
                  }
                ],
                "template": {
                  "#callback": "$item",
                  "variable": "$item",
                  "body": [
                    {
                      "#type": "image_shape",
                      "#props": [
                        {
                          "#type": "prop_image_key",
                          "#prop": "",
                          "key": "shape.png"
                        },
                        {
                          "#type": "prop_image_src",
                          "#prop": "",
                          "src": "${image_path}shape.png"
                        },
                        {
                          "#type": "prop_scale",
                          "#prop": "",
                          "scale": {
                            "#type": "expression",
                            "value": "0.7"
                          }
                        }
                      ]
                    },
                    {
                      "#type": "choice_input_shape",
                      "#props": [
                        {
                          "#type": "prop_value",
                          "#prop": "",
                          "value": {
                            "#type": "expression",
                            "value": "angle[1]"
                          }
                        },
                        {
                          "#type": "prop_size",
                          "#prop": "",
                          "width": {
                            "#type": "expression",
                            "value": "120*0.7"
                          },
                          "height": {
                            "#type": "expression",
                            "value": "65*0.7"
                          }
                        },
                        {
                          "#type": "prop_input_keyboard",
                          "#prop": "",
                          "keyboard": "numbers1"
                        },
                        {
                          "#type": "prop_input_max_length",
                          "#prop": "",
                          "maxLength": {
                            "#type": "expression",
                            "value": "angle[1].toString().length"
                          }
                        },
                        {
                          "#type": "prop_input_result_position",
                          "#prop": "",
                          "resultPosition": "right"
                        },
                        {
                          "#type": "prop_tab_order",
                          "#prop": "",
                          "tabOrder": {
                            "#type": "expression",
                            "value": "0"
                          }
                        },
                        {
                          "#type": "prop_stroke",
                          "#prop": "stroke"
                        },
                        {
                          "#type": "prop_fill",
                          "#prop": "fill"
                        },
                        {
                          "#prop": "",
                          "style": {
                            "#type": "json",
                            "base": "text",
                            "#props": [
                              {
                                "#type": "prop_text_style_font_size",
                                "#prop": "",
                                "fontSize": {
                                  "#type": "expression",
                                  "value": "32"
                                }
                              },
                              {
                                "#type": "prop_text_style_fill",
                                "#prop": "",
                                "fill": "black"
                              },
                              {
                                "#type": "prop_text_style_stroke",
                                "#prop": "",
                                "stroke": "white"
                              },
                              {
                                "#type": "prop_text_style_stroke_thickness",
                                "#prop": "",
                                "strokeThickness": {
                                  "#type": "expression",
                                  "value": "2"
                                }
                              }
                            ]
                          }
                        }
                      ],
                      "#init": "algorithmic_input"
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "list_shape",
            "#props": [
              {
                "#type": "prop_list_direction",
                "#prop": "",
                "dir": "horizontal"
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "365"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              }
            ],
            "items": [
              {
                "#type": "json",
                "#props": [
                  {
                    "#type": "prop_list_align",
                    "#prop": "",
                    "align": "middle"
                  }
                ],
                "template": {
                  "#callback": "$item",
                  "variable": "$item",
                  "body": [
                    {
                      "#type": "text_shape",
                      "#props": [
                        {
                          "#type": "prop_anchor",
                          "#prop": "anchor",
                          "x": {
                            "#type": "expression",
                            "value": "0.5"
                          },
                          "y": {
                            "#type": "expression",
                            "value": "0.5"
                          }
                        },
                        {
                          "#type": "prop_text_contents",
                          "#prop": "",
                          "contents": "Angle 1 + Angle 2 ="
                        },
                        {
                          "#prop": "",
                          "style": {
                            "#type": "json",
                            "base": "text",
                            "#props": [
                              {
                                "#type": "prop_text_style_font_size",
                                "#prop": "",
                                "fontSize": {
                                  "#type": "expression",
                                  "value": "36"
                                }
                              },
                              {
                                "#type": "prop_text_style_fill",
                                "#prop": "",
                                "fill": "black"
                              },
                              {
                                "#type": "prop_text_style_stroke",
                                "#prop": "",
                                "stroke": "white"
                              },
                              {
                                "#type": "prop_text_style_stroke_thickness",
                                "#prop": "",
                                "strokeThickness": {
                                  "#type": "expression",
                                  "value": "2"
                                }
                              }
                            ]
                          }
                        }
                      ]
                    }
                  ]
                }
              },
              {
                "#type": "json",
                "#props": [
                  {
                    "#type": "prop_list_align",
                    "#prop": "",
                    "align": "middle"
                  }
                ],
                "template": {
                  "#callback": "$item",
                  "variable": "$item",
                  "body": [
                    {
                      "#type": "image_shape",
                      "#props": [
                        {
                          "#type": "prop_image_key",
                          "#prop": "",
                          "key": "shape.png"
                        },
                        {
                          "#type": "prop_image_src",
                          "#prop": "",
                          "src": "${image_path}shape.png"
                        },
                        {
                          "#type": "prop_scale",
                          "#prop": "",
                          "scale": {
                            "#type": "expression",
                            "value": "0.7"
                          }
                        }
                      ]
                    },
                    {
                      "#type": "choice_input_shape",
                      "#props": [
                        {
                          "#type": "prop_value",
                          "#prop": "",
                          "value": {
                            "#type": "expression",
                            "value": "angle[0]+angle[1]"
                          }
                        },
                        {
                          "#type": "prop_size",
                          "#prop": "",
                          "width": {
                            "#type": "expression",
                            "value": "120*0.7"
                          },
                          "height": {
                            "#type": "expression",
                            "value": "65*0.7"
                          }
                        },
                        {
                          "#type": "prop_input_keyboard",
                          "#prop": "",
                          "keyboard": "numbers1"
                        },
                        {
                          "#type": "prop_input_max_length",
                          "#prop": "",
                          "maxLength": {
                            "#type": "expression",
                            "value": "(angle[0]+angle[1]).toString().length"
                          }
                        },
                        {
                          "#type": "prop_input_result_position",
                          "#prop": "",
                          "resultPosition": "bottom"
                        },
                        {
                          "#type": "prop_tab_order",
                          "#prop": "",
                          "tabOrder": {
                            "#type": "expression",
                            "value": "0"
                          }
                        },
                        {
                          "#type": "prop_stroke",
                          "#prop": "stroke"
                        },
                        {
                          "#type": "prop_fill",
                          "#prop": "fill"
                        },
                        {
                          "#prop": "",
                          "style": {
                            "#type": "json",
                            "base": "text",
                            "#props": [
                              {
                                "#type": "prop_text_style_font_size",
                                "#prop": "",
                                "fontSize": {
                                  "#type": "expression",
                                  "value": "32"
                                }
                              },
                              {
                                "#type": "prop_text_style_fill",
                                "#prop": "",
                                "fill": "black"
                              },
                              {
                                "#type": "prop_text_style_stroke",
                                "#prop": "",
                                "stroke": "white"
                              },
                              {
                                "#type": "prop_text_style_stroke_thickness",
                                "#prop": "",
                                "strokeThickness": {
                                  "#type": "expression",
                                  "value": "2"
                                }
                              }
                            ]
                          }
                        }
                      ],
                      "#init": "algorithmic_input"
                    }
                  ]
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 4"
        },
        "then": [
          {
            "#type": "do_while_block",
            "do": [
              {
                "#type": "variable",
                "name": "a",
                "value": {
                  "#type": "random_one",
                  "items": {
                    "#type": "expression",
                    "value": "list_angle"
                  }
                }
              }
            ],
            "while": {
              "#type": "expression",
              "value": "a > 160"
            }
          },
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "40"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "Find the unknown angle."
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "35"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    },
                    {
                      "#type": "prop_text_style_wordwrap",
                      "#prop": "",
                      "wordWrap": {
                        "#type": "expression",
                        "value": "true"
                      }
                    },
                    {
                      "#type": "prop_text_style_wordWrapWidth",
                      "#prop": "",
                      "wordWrapWidth": {
                        "#type": "expression",
                        "value": "500"
                      }
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "180"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "300"
                },
                "height": {
                  "#type": "expression",
                  "value": "200"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "240"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "200"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "box.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}box.png"
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 50"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "linel.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}linel.png"
                      },
                      {
                        "#type": "prop_angle",
                        "#prop": "",
                        "angle": {
                          "#type": "expression",
                          "value": "180 - a"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 50"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "linel.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}linel.png"
                      },
                      {
                        "#type": "prop_angle",
                        "#prop": "",
                        "angle": {
                          "#type": "expression",
                          "value": "0"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 50"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "cl.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}cl.png"
                      },
                      {
                        "#type": "prop_angle",
                        "#prop": "",
                        "angle": {
                          "#type": "expression",
                          "value": "360 - a"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 50"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "cl4.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}cl4.png"
                      },
                      {
                        "#type": "prop_angle",
                        "#prop": "",
                        "angle": {
                          "#type": "expression",
                          "value": "180-a"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 50"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "liner.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}liner.png"
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 50"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "point.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}point.png"
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "180 - a > 35 ? 340:320"
                },
                "y": {
                  "#type": "expression",
                  "value": "220"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "180 - a > 99 ? 50:40"
                },
                "height": {
                  "#type": "expression",
                  "value": "30"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "text_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_text_contents",
                        "#prop": "",
                        "contents": "${180-a}"
                      },
                      {
                        "#prop": "",
                        "style": {
                          "#type": "json",
                          "base": "text",
                          "#props": [
                            {
                              "#type": "prop_text_style_font_size",
                              "#prop": "",
                              "fontSize": {
                                "#type": "expression",
                                "value": "32"
                              }
                            },
                            {
                              "#type": "prop_text_style_fill",
                              "#prop": "",
                              "fill": "black"
                            },
                            {
                              "#type": "prop_text_style_stroke",
                              "#prop": "",
                              "stroke": "white"
                            },
                            {
                              "#type": "prop_text_style_stroke_thickness",
                              "#prop": "",
                              "strokeThickness": {
                                "#type": "expression",
                                "value": "2"
                              }
                            }
                          ]
                        }
                      }
                    ]
                  },
                  {
                    "#type": "text_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.right"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY - 15"
                        }
                      },
                      {
                        "#type": "prop_text_contents",
                        "#prop": "",
                        "contents": "o"
                      },
                      {
                        "#prop": "",
                        "style": {
                          "#type": "json",
                          "base": "text",
                          "#props": [
                            {
                              "#type": "prop_text_style_font_size",
                              "#prop": "",
                              "fontSize": {
                                "#type": "expression",
                                "value": "24"
                              }
                            },
                            {
                              "#type": "prop_text_style_fill",
                              "#prop": "",
                              "fill": "black"
                            },
                            {
                              "#type": "prop_text_style_stroke",
                              "#prop": "",
                              "stroke": "white"
                            },
                            {
                              "#type": "prop_text_style_stroke_thickness",
                              "#prop": "",
                              "strokeThickness": {
                                "#type": "expression",
                                "value": "2"
                              }
                            }
                          ]
                        }
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "350"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "shape.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}shape.png"
              }
            ]
          },
          {
            "#type": "choice_input_shape",
            "#props": [
              {
                "#type": "prop_value",
                "#prop": "",
                "value": {
                  "#type": "expression",
                  "value": "a"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "350"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "125"
                },
                "height": {
                  "#type": "expression",
                  "value": "68"
                }
              },
              {
                "#type": "prop_input_keyboard",
                "#prop": "",
                "keyboard": "numbers1"
              },
              {
                "#type": "prop_input_max_length",
                "#prop": "",
                "maxLength": {
                  "#type": "expression",
                  "value": "a.toString().length"
                }
              },
              {
                "#type": "prop_input_result_position",
                "#prop": "",
                "resultPosition": "bottom"
              },
              {
                "#type": "prop_tab_order",
                "#prop": "",
                "tabOrder": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_stroke",
                "#prop": "stroke"
              },
              {
                "#type": "prop_fill",
                "#prop": "fill"
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "32"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ],
            "#init": "algorithmic_input"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "link",
        "value": {
          "#type": "expression",
          "value": "'http://starmathsonline.s3.amazonaws.com/' + image_path"
        }
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 1: First, look at all 3 angles.</u></br>\n  </br>\nRemember this, a straight line is 180°, Look at each angle, now calculate each angle.</br></br>\n<style>\n      #angle1 {\n              -ms-transform: rotate(${180-angle[0]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(180-angle[0]}deg);\n              /* Safari */\n              transform: rotate(${180-angle[0]}deg);\n              /* Standard syntax */\n          }\n          #angle2 {\n              -ms-transform: rotate(${180-angle[1]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${180-angle[1]}deg);\n              /* Safari */\n              transform: rotate(${180-angle[1]}deg);\n              /* Standard syntax */\n          }\n          #angle3 {\n              -ms-transform: rotate(${180-angle[2]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${180-angle[2]}deg);\n              /* Safari */\n              transform: rotate(${180-angle[2]}deg);\n              /* Standard syntax */\n          }\n\n          #anglec1 {\n              -ms-transform: rotate(${360-angle[0]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${360-angle[0]}deg);\n              /* Safari */\n              transform: rotate(${360-angle[0]}deg);\n              /* Standard syntax */\n          }\n          #anglec2 {\n              -ms-transform: rotate(${360-angle[1]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${360-angle[1]}deg);\n              /* Safari */\n              transform: rotate(${360-angle[1]}deg);\n              /* Standard syntax */\n          }\n          #anglec3 {\n              -ms-transform: rotate(${360-angle[2]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${360-angle[2]}deg);\n              /* Safari */\n              transform: rotate(${360-angle[2]}deg);\n              /* Standard syntax */\n          }\n      </style>\n\n\n      <div style='position: relative; width: 260px; height: 280px; display: inline-block;'>\n        <div style='position: absolute; width: 300px; height: 250px; left: 10px; top: 10px;'>\n          <img style='position: absolute; min-width: 240px; max-height: 200px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/box.png'/>\n          <img id='anglec1' style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/cl.png'/>\n          <img style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/liner.png'/>\n          <img id='angle1' style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/linel.png'/>\n          <img style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/point.png'/>\n        </div>\n<span style='position: absolute; left: 100px; top: 200px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'>${angle[0]}&deg;</span>\n      </div>\n<div style='width: 30px; display: inline-block'></div>\n      <div style='position: relative; width: 260px; height: 280px; display: inline-block;'>\n        <div style='position: absolute; width: 300px; height: 250px; left: 10px; top: 10px;'>\n          <img style='position: absolute; min-width: 240px; max-height: 200px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/box.png'/>\n          <img id='anglec2' style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/cl.png'/>\n          <img style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/liner.png'/>\n          <img id='angle2' style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/linel.png'/>\n          <img style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/point.png'/>\n        </div>\n<span style='position: absolute; left: 100px; top: 200px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'>${angle[1]}&deg;</span>\n      </div>\n<div style='width: 30px; display: inline-block'></div>\n      <div style='position: relative; width: 260px; height: 280px; display: inline-block;'>\n        <div style='position: absolute; width: 300px; height: 250px; left: 10px; top: 10px;'>\n          <img style='position: absolute; min-width: 240px; max-height: 200px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/box.png'/>\n          <img id='anglec3' style='position: absolute; top: 30px; left: ${10 - Ox(angle[2])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/cl.png'/>\n          <img style='position: absolute; top: 30px; left: ${10 - Ox(angle[2])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/liner.png'/>\n          <img id='angle3' style='position: absolute; top: 30px; left: ${10 - Ox(angle[2])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/linel.png'/>\n          <img style='position: absolute; top: 30px; left: ${10 - Ox(angle[2])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/point.png'/>\n        </div>\n<span style='position: absolute; left: 100px; top: 200px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'>${angle[2]}&deg;</span>\n      </div>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 2: Let's find our final answer.</u></br>\n  </br>\nSo which angle is ${a}&deg;.\n</br></br>\n<style>\n      #angle1 {\n              -ms-transform: rotate(${180-angle[0]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(180-angle[0]}deg);\n              /* Safari */\n              transform: rotate(${180-angle[0]}deg);\n              /* Standard syntax */\n          }\n          #angle2 {\n              -ms-transform: rotate(${180-angle[1]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${180-angle[1]}deg);\n              /* Safari */\n              transform: rotate(${180-angle[1]}deg);\n              /* Standard syntax */\n          }\n          #angle3 {\n              -ms-transform: rotate(${180-angle[2]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${180-angle[2]}deg);\n              /* Safari */\n              transform: rotate(${180-angle[2]}deg);\n              /* Standard syntax */\n          }\n\n          #anglec1 {\n              -ms-transform: rotate(${360-angle[0]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${360-angle[0]}deg);\n              /* Safari */\n              transform: rotate(${360-angle[0]}deg);\n              /* Standard syntax */\n          }\n          #anglec2 {\n              -ms-transform: rotate(${360-angle[1]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${360-angle[1]}deg);\n              /* Safari */\n              transform: rotate(${360-angle[1]}deg);\n              /* Standard syntax */\n          }\n          #anglec3 {\n              -ms-transform: rotate(${360-angle[2]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${360-angle[2]}deg);\n              /* Safari */\n              transform: rotate(${360-angle[2]}deg);\n              /* Standard syntax */\n          }\n      </style>\n\n      <div style='position: relative; width: 260px; height: 280px; display: inline-block; border: ${angle[0] == a ? \"3px solid green\": \"\"}'>\n        <div style='position: absolute; width: 300px; height: 250px; left: 10px; top: 10px;'>\n          <img style='position: absolute; min-width: 240px; max-height: 200px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/box.png'/>\n          <img id='anglec1' style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/cl.png'/>\n          <img style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/liner.png'/>\n          <img id='angle1' style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/linel.png'/>\n          <img style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/point.png'/>\n        </div>\n<span style='position: absolute; left: 100px; top: 200px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'>${angle[0]}&deg;</span>\n      </div>\n<div style='width: 30px; display: inline-block'></div>\n      <div style='position: relative; width: 260px; height: 280px; display: inline-block; border: ${angle[1] == a ? \"3px solid green\": \"\"}'>\n        <div style='position: absolute; width: 300px; height: 250px; left: 10px; top: 10px;'>\n          <img style='position: absolute; min-width: 240px; max-height: 200px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/box.png'/>\n          <img id='anglec2' style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/cl.png'/>\n          <img style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/liner.png'/>\n          <img id='angle2' style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/linel.png'/>\n          <img style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/point.png'/>\n        </div>\n<span style='position: absolute; left: 100px; top: 200px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'>${angle[1]}&deg;</span>\n      </div>\n<div style='width: 30px; display: inline-block'></div>\n      <div style='position: relative; width: 260px; height: 280px; display: inline-block; border: ${angle[2] == a ? \"3px solid green\": \"\"}'>\n        <div style='position: absolute; width: 300px; height: 250px; left: 10px; top: 10px;'>\n          <img style='position: absolute; min-width: 240px; max-height: 200px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/box.png'/>\n          <img id='anglec3' style='position: absolute; top: 30px; left: ${10 - Ox(angle[2])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/cl.png'/>\n          <img style='position: absolute; top: 30px; left: ${10 - Ox(angle[2])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/liner.png'/>\n          <img id='angle3' style='position: absolute; top: 30px; left: ${10 - Ox(angle[2])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/linel.png'/>\n          <img style='position: absolute; top: 30px; left: ${10 - Ox(angle[2])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/point.png'/>\n        </div>\n<span style='position: absolute; left: 100px; top: 200px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'>${angle[2]}&deg;</span>\n      </div>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 2"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "Place the midpoint of the protractor on the VERTEX of the angle.\n</br>\nAt the same time, line up one side of the angle with the zero line of the protractor \n</br></br></br>\n<style>\n                    #angle1 {\n              -ms-transform: rotate(${180-a}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${180-a}deg);\n              /* Safari */\n              transform: rotate(${180-a}deg);\n              /* Standard syntax */\n          }\n          #anglec1 {\n              -ms-transform: rotate(${360-a}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${360-a}deg);\n              /* Safari */\n              transform: rotate(${360-a}deg);\n              /* Standard syntax */\n          }\n          </style>\n\n        <div style='position: relative; width: 260px; height: 280px; display: inline-block;'>\n            <div style='position: absolute; width: 600px; height: 250px; left: 300px; top: 10px;'>\n              <img style='position: absolute; min-width: 240px; max-height: 200px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/box.png'/>\n              <img id='anglec1' style='position: absolute; top: 30px; left: ${10 - Ox(a)}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/cl.png'/>\n              <img style='position: absolute; top: 30px; left: ${10 - Ox(a)}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/liner.png'/>\n              <img id='angle1' style='position: absolute; top: 30px; left: ${10 - Ox(a)}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/linel.png'/>\n              <img style='position: absolute; top: 30px; left: ${10 - Ox(a)}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/point.png'/>\n            \n              <img style='position: absolute; max-width: 350px; left: ${- 65 - Ox(a)}px; top: -45px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/pro.png'/>\n            \n            </div>\n</div>\n</br>\n  <span style='background: rgb(250, 250, 250, 0.5)'>\nRead the degrees where the other side crosses the number scale.\n</br>\nIt says <b>${a}&deg;</b>!</span>"
            ]
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 3"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 1:</u></br>\nPlace the midpoint of the protractor on the VERTEX of the angle.</br>\nAt the same time, line up one side of the angle with the zero line of the protractor \n</br></br></br>\n<style>\n                    #angle1 {\n              -ms-transform: rotate(${180-angle[0]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${180-angle[0]}deg);\n              /* Safari */\n              transform: rotate(${180-angle[0]}deg);\n              /* Standard syntax */\n          }\n          #anglec1 {\n              -ms-transform: rotate(${360-angle[0]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${360-angle[0]}deg);\n              /* Safari */\n              transform: rotate(${360-angle[0]}deg);\n              /* Standard syntax */\n          }\n          \n          #angle2 {\n              -ms-transform: rotate(${180-angle[1]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${180-angle[1]}deg);\n              /* Safari */\n              transform: rotate(${180-angle[1]}deg);\n              /* Standard syntax */\n          }\n          #anglec2 {\n              -ms-transform: rotate(${360-angle[1]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${360-angle[1]}deg);\n              /* Safari */\n              transform: rotate(${360-angle[1]}deg);\n              /* Standard syntax */\n          }\n          </style>\n\n        <div style='position: relative; width: 260px; height: 280px; display: inline-block;'>\n            <div style='position: absolute; width: 600px; height: 250px; left: 300px; top: 10px;'>\n              <img style='position: absolute; min-width: 240px; max-height: 200px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/box.png'/>\n              <img id='anglec1' style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/cl.png'/>\n              <img style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/liner.png'/>\n              <img id='angle1' style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/linel.png'/>\n              <img style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/point.png'/>\n            \n              <img style='position: absolute; max-width: 350px; left: ${- 65 - Ox(angle[0])}px; top: -45px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/pro.png'/>\n            <span style='position: absolute; left: 300px; top: 100px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'>${angle[0]}&deg;</span>\n            </div>\n</div>\n</br><div style='position: relative; width: 260px; height: 280px; display: inline-block;'>\n            <div style='position: absolute; width: 600px; height: 250px; left: 300px; top: 10px;'>\n              <img style='position: absolute; min-width: 240px; max-height: 200px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/box.png'/>\n                <img id='anglec2' style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/cl.png'/>\n              <img style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/liner.png'/>\n              <img id='angle2' style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/linel.png'/>\n              <img style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/point.png'/>\n            \n              <img style='position: absolute; max-width: 350px; left: ${- 65 - Ox(angle[1])}px; top: -45px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/pro.png'/>\n            <span style='position: absolute; left: 300px; top: 100px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'>${angle[1]}&deg;</span>\n            </div>\n</div>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 2: Read the degrees where the other side crosses the number scale.</u></br></br>\n<span style='background: rgb(250, 250, 250, 0.4)'>\nFor angle 1</br>\nIt says <b>${angle[0]}&deg;</b></br></br>\n\nFor angle 2</br>\nIt says <b>${angle[1]}&deg;</b></br></br>\n\nAngle 1 + angle 2 = <b>${angle[0] + angle[1]}&deg;</b></span>"
            ]
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 4"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Remember the size of the straight line.</u></br></br>\n\nIt's 180°!</br></br>\n\nNow, using subtraction, let's subtract 180° by all the known angles.</br><br>\n\n180&deg; - ${180 - a}&deg; = <b>${a}&deg;</b></br></br>\n\nSo the angle is <b>${a}&deg;</b>!"
            ]
          }
        ]
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="0" y="0">
    <field name="name">Y6.MG.AN.FA180.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y6.MG.AN.FA180.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="YepMQT@2uwp2rRPvmnRg1">
                                    <field name="name">range</field>
                                    <value name="value">
                                      <block type="expression" id="UxgQ^Y5]rb}vpw[D=l-!">
                                        <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="if_then_else_block" id="0RWXxs$%M.7n6%X)PNK,">
                                        <value name="if">
                                          <block type="expression" id="2+XBg|,YRIr$@1g^-|a9J">
                                            <field name="value">range &lt; 0</field>
                                          </block>
                                        </value>
                                        <statement name="then">
                                          <block type="variable" id="9jlrMG+60[uqUgwS)]Jo">
                                            <field name="name">list_angle</field>
                                            <value name="value">
                                              <block type="expression" id="xZ$3C7saOpO94-)ipnwi">
                                                <field name="value">[30, 60, 90, 120, 150, 180]</field>
                                              </block>
                                            </value>
                                          </block>
                                        </statement>
                                        <statement name="else">
                                          <block type="if_then_else_block" id="DuGp+Hdnw#P,vf5Wyso.">
                                            <value name="if">
                                              <block type="expression" id="RA[sz+[GcrCg%(gEZymC">
                                                <field name="value">range &lt; 4</field>
                                              </block>
                                            </value>
                                            <statement name="then">
                                              <block type="variable" id="U_|!Y;AL.%qUs~5E@2f2}">
                                                <field name="name">list_angle</field>
                                                <value name="value">
                                                  <block type="expression" id="9lnkt~d;Py3ki;JBY~XU">
                                                    <field name="value">[20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170]</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </statement>
                                            <statement name="else">
                                              <block type="variable" id="?GOo_U)4uJ`bb5NDcl#8">
                                                <field name="name">list_angle</field>
                                                <value name="value">
                                                  <block type="expression" id="m@2Cw%}eGYrsWmRZz/5x6">
                                                    <field name="value">[20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150, 155, 160, 165, 170]</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </statement>
                                          </block>
                                        </statement>
                                        <next>
                                          <block type="statement" id=";t^zW4|7-TsF2z{@1EMLf">
                                            <field name="value">function Ox(a){
  if(a &lt; 60){return 50;}
  else if(a &lt; 110) {return 30;}
  else {return 5;}
}
                                            </field>
                                            <next>
                                              <block type="variable" id="6yjfR{5BB@1usX_HtxTVL">
                                                <field name="name">type</field>
                                                <value name="value">
                                                  <block type="random_number" id="=^jXDv{3%rDSd;C0AD+?">
                                                    <value name="min">
                                                      <block type="expression" id="P=7x3+2%.#6~?Yc2e5{,">
                                                        <field name="value">1</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="S[S9]8gTY[j@1`?|4E08V">
                                                        <field name="value">4</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="if_then_block" id="w#P0D;{N1ST+@2DBj{?$0">
                                                    <value name="if">
                                                      <block type="expression" id="tJJXCU}%Sqd,bZaJxa)n">
                                                        <field name="value">type == 1</field>
                                                      </block>
                                                    </value>
                                                    <statement name="then">
                                                      <block type="variable" id="bE7g++F=3L4v95TQHFRJ">
                                                        <field name="name">angle</field>
                                                        <value name="value">
                                                          <block type="random_many" id="Lkc{F8k_F%YayI7Ob|hm">
                                                            <value name="count">
                                                              <block type="expression" id="[2}%=`D1tv75LDt@1s4hJ">
                                                                <field name="value">3</field>
                                                              </block>
                                                            </value>
                                                            <value name="items">
                                                              <block type="expression" id="U=8[)9Q;6@2c::yGw^m]#">
                                                                <field name="value">list_angle</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="M]P,k5l;iLL`jl=S,Jw~">
                                                            <field name="name">a</field>
                                                            <value name="value">
                                                              <block type="random_one" id="RTGw_I@2Pl,@1}=R@2TV5bL">
                                                                <value name="items">
                                                                  <block type="expression" id="!%HvmDl!u}mdnI`HY#F_">
                                                                    <field name="value">angle</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="text_shape" id="BMZ`VXgeL9SKSUPypqR}">
                                                                <statement name="#props">
                                                                  <block type="prop_position" id="2ZCJ!{{ewoqJ:U`qy/a3">
                                                                    <field name="#prop"></field>
                                                                    <value name="x">
                                                                      <block type="expression" id="@2}VwZjF@1@26jx%[t9s+S{">
                                                                        <field name="value">550</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="y">
                                                                      <block type="expression" id="v9Xp5o$8e(vwo?-5ulv)">
                                                                        <field name="value">50</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_anchor" id="/mlO?LRGl]Y6:miMCcVl">
                                                                        <field name="#prop">anchor</field>
                                                                        <value name="x">
                                                                          <block type="expression" id="(AL@10~x$mYek3xG#s8!R">
                                                                            <field name="value">0.5</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id="9@1zx[V:?]#@2^~.nF{K1i">
                                                                            <field name="value">0</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_text_contents" id="a/_O-[.Q43~MHRwq0s9L">
                                                                            <field name="#prop"></field>
                                                                            <value name="contents">
                                                                              <block type="string_value" id="sq!]aL5[V31FJEKfV=kt">
                                                                                <field name="value">Which angle is ${a}°?</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_text_style" id="a(3JA7@1Mb2H0sM##7f$V">
                                                                                <field name="base">text</field>
                                                                                <statement name="#props">
                                                                                  <block type="prop_text_style_font_size" id="a#|@2`]s}o!b.Ip9+i?S4">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="fontSize">
                                                                                      <block type="expression" id="rWx}E+P{y@2ktY_%^5d:j">
                                                                                        <field name="value">35</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_text_style_fill" id="!yxwytpitVg|ZpaK8C!U">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="fill">
                                                                                          <block type="string_value" id="EL(Zzz`XUBi;_$4Fo~?.">
                                                                                            <field name="value">black</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="stroke">
                                                                                              <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                                <field name="value">white</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="strokeThickness">
                                                                                                  <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                                    <field name="value">2</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style_wordwrap" id="eg?/EQ2A@2O.N-04S:c9,">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="wordWrap">
                                                                                                      <block type="expression" id="0pCQ`1e+.:~f$32g`8vA">
                                                                                                        <field name="value">true</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style_wordWrapWidth" id="M2hlIxxcA_1AZq|^dgOv">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="wordWrapWidth">
                                                                                                          <block type="expression" id="!9]`O^T#$X.CD~mZ:@2@1-">
                                                                                                            <field name="value">500</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <next>
                                                                  <block type="choice_custom_shape" id=")VsH;/gH-1oZTAJh[WjZ">
                                                                    <field name="action">drag</field>
                                                                    <value name="value">
                                                                      <block type="expression" id="bPk{5{d^4^CUAFj1yEdt">
                                                                        <field name="value">undefined</field>
                                                                      </block>
                                                                    </value>
                                                                    <statement name="template">
                                                                      <block type="image_shape" id="L;z:=V)4krx_[lzu?QY_">
                                                                        <statement name="#props">
                                                                          <block type="prop_position" id="!pn@2R4}/L0[XKuTZuVZb">
                                                                            <field name="#prop"></field>
                                                                            <value name="x">
                                                                              <block type="expression" id="B+zVDpfr1TNPmM8:AZ;d">
                                                                                <field name="value">200</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="y">
                                                                              <block type="expression" id="kP=,tuD8?~$VLI`VyF9e">
                                                                                <field name="value">5</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_anchor" id="Xo-D.UZyjFm50/b([?/T">
                                                                                <field name="#prop">anchor</field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="NeAD8@1r,+(TIciU(hBLB">
                                                                                    <field name="value">0.5</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="veeoF}Nj}gN3XH4tm@2lw">
                                                                                    <field name="value">0</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_max_size" id="Q!1:6!bEMtH+%{v/(3b_">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="maxWidth">
                                                                                      <block type="expression" id="QEG-1WO#C%Xrl|C}]:%$">
                                                                                        <field name="value">300</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="maxHeight">
                                                                                      <block type="expression" id="#Yg4a5`)MKu@1OtamZ">
                                                                                        <field name="value">0</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_image_key" id="1oGJ`jL.gKgkf[3RY[3B">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="key">
                                                                                          <block type="string_value" id="NsAP|bPHY=oW`_@18B~T6">
                                                                                            <field name="value">pro.png</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_image_src" id="ss5^-]bTi_Y#I1`LqNPf">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="src">
                                                                                              <block type="string_value" id="KcX|~iQBu{9NRlDfO9^]">
                                                                                                <field name="value">${image_path}pro.png</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="grid_shape" id="6tU?K^N8ec~A@1@1NwQ72N">
                                                                        <statement name="#props">
                                                                          <block type="prop_grid_dimension" id="X4K:|9dKK%s9l4n5v@2lG">
                                                                            <field name="#prop"></field>
                                                                            <value name="rows">
                                                                              <block type="expression" id="[Ev!.bxnzjc|-33v.%7=">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="cols">
                                                                              <block type="expression" id="u$7]/V/!ZWM4(Va??@2({">
                                                                                <field name="value">3</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_position" id="zLld84TfBg#!88]1,JD-">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="Cd6g{Y_P?:GyVyvQi1wJ">
                                                                                    <field name="value">400</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id=".F}E71+@1i)77!yQ`hmfu">
                                                                                    <field name="value">300</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_size" id="+o2CWr^.ohx)_bcIG53]">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="width">
                                                                                      <block type="expression" id="Z[x5iHG%KWC%KWQb}c-G">
                                                                                        <field name="value">780</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="height">
                                                                                      <block type="expression" id="@2E|Ob@1jF#gYtX8-%Iui@1">
                                                                                        <field name="value">200</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_anchor" id="1WJjXY^.h,t]eNgxaJ}q">
                                                                                        <field name="#prop">anchor</field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="DHx[vqs?.?nUD:AYPJWd">
                                                                                            <field name="value">0.5</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="e3D|RR7w!=Sufu):zV`c">
                                                                                            <field name="value">0.5</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_grid_cell_source" id="v8+Yu211Qg8fX9Mu4th`">
                                                                                            <field name="#prop">cell.source</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="~j@1,^liddf}oDg}E91^i">
                                                                                                <field name="value">angle</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_grid_random" id="}?kS#x[b)9OGb@1`UdJGV">
                                                                                                <field name="#prop"></field>
                                                                                                <field name="random">FALSE</field>
                                                                                                <next>
                                                                                                  <block type="prop_grid_show_borders" id=";y^wk4y%[VWjXOj~vrQZ">
                                                                                                    <field name="#prop">#showBorders</field>
                                                                                                    <field name="value">FALSE</field>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_cell_template" id="7^6]Kv:2yrZi`cooAW2@2">
                                                                                                        <field name="variable">$cell</field>
                                                                                                        <field name="#prop">cell.template</field>
                                                                                                        <field name="#callback">$cell</field>
                                                                                                        <statement name="body">
                                                                                                          <block type="image_shape" id="_!Cx8#$]MEnT@2^i=1YF)">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_position" id="Kx8.bX/Dy0CHt[q,P#~q">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="oP|b862Z0as]dyadpxNi">
                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="iu//FyxWyr$F%x^27KnQ">
                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_size" id="(}-}^^$FmX{I{^lWjiqa">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="width">
                                                                                                                      <block type="expression" id="hrIIdJ#l{K-jyF};c$19">
                                                                                                                        <field name="value">240</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="height">
                                                                                                                      <block type="expression" id="!fe725))@2cfw.74BB@1P;">
                                                                                                                        <field name="value">200</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_key" id="H!Eh#nrj0l8:3yMEYNKh">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="key">
                                                                                                                          <block type="string_value" id="nj9%K)!)tvQ)?/=}-!-:">
                                                                                                                            <field name="value">box.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_src" id="WntF@2o?MDNxRJD`?I)/i">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="src">
                                                                                                                              <block type="string_value" id="w6T=Cn$!;_)9pmJeVX]-">
                                                                                                                                <field name="value">${image_path}box.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="if_then_else_block" id="XC$1g@18b7ONK{F{X2loj">
                                                                                                                <value name="if">
                                                                                                                  <block type="expression" id="oQFwzwo2D;+c?CON=pWH">
                                                                                                                    <field name="value">$cell.data == 90</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <statement name="then">
                                                                                                                  <block type="image_shape" id="(^`N,lKpoJI[DKN5E?fR">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_position" id="$NKhS!jz_YUsOazNwF:m">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="^qIBD4UAKRt3uyCa@1M)2">
                                                                                                                            <field name="value">$cell.centerX - Ox($cell.data)</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="@2Q+9C|WzvSugr=IW.It!">
                                                                                                                            <field name="value">$cell.centerY + 50</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_key" id=";76DR7U3P|R:nv%z@1">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="key">
                                                                                                                              <block type="string_value" id="7,J]dcp!/N=k`egU6%.=">
                                                                                                                                <field name="value">sq.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_src" id="DQ/syGE4S{eSPrR_]qbo">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="src">
                                                                                                                                  <block type="string_value" id="6((b3K5@1u4@2K6}.DgH{r">
                                                                                                                                    <field name="value">${image_path}sq.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <statement name="else">
                                                                                                                  <block type="image_shape" id="R~c#G@2o-cE[zDQ]2u~/j">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_position" id="?Z9=NM!QBrtdEU)rDlHC">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="^gQf?g%Y+4gi8^B}|x_4">
                                                                                                                            <field name="value">$cell.centerX - Ox($cell.data)</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="DZ`)FjJba?$.:(Q!S+o:">
                                                                                                                            <field name="value">$cell.centerY + 50</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_key" id="Ut#)FKn!~?Oj$r-q2Wj0">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="key">
                                                                                                                              <block type="string_value" id="qoXy4A2/0dDHa!Hc@1xFa">
                                                                                                                                <field name="value">cl.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_src" id="5e^IP+}U~MV3!2)|$7l9">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="src">
                                                                                                                                  <block type="string_value" id="tBN8,}8qQ?MebRu4/7_6">
                                                                                                                                    <field name="value">${image_path}cl.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_angle" id="@2PlAh$9ssc+5[7E4+vlR">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="angle">
                                                                                                                                      <block type="expression" id="R,D/qfnOffwU_}ex@1euR">
                                                                                                                                        <field name="value">360 - $cell.data</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="image_shape" id="{Ba2ypy=uDVvj:YOWabP">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_position" id="gy8=[Gjv)RQ7H:Q:F~Hu">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="MQHla1GBg$e@2gtjaQWv,">
                                                                                                                            <field name="value">$cell.centerX - Ox($cell.data)</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="`a.{bi_z.jR]jO6hw7dW">
                                                                                                                            <field name="value">$cell.centerY + 50</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_key" id="#o/YLFSHz0/REba,3eeO">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="key">
                                                                                                                              <block type="string_value" id="kW[I5k|_BL`KpCC+/l7G">
                                                                                                                                <field name="value">liner.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_src" id="noDr6@1|MuTeaNQr36Nvh">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="src">
                                                                                                                                  <block type="string_value" id=",|@1d/r^matCX)`[-MY@2l">
                                                                                                                                    <field name="value">${image_path}liner.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="image_shape" id="Nw|98#[REYjsCnpmw][0">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="mjUZY}fA.:1ihydR@1@2cD">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="OwOirD%MX:KD_O,6N3=P">
                                                                                                                                <field name="value">$cell.centerX - Ox($cell.data)</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="cJ|Gqy{VodD8w`_QBx`O">
                                                                                                                                <field name="value">$cell.centerY + 50</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_key" id="BGx[j%lbrS8e+r}-yej]">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="key">
                                                                                                                                  <block type="string_value" id="F@1ET;S%h{cQOHHKQXvm%">
                                                                                                                                    <field name="value">linel.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_src" id="P2yIL@2x^d%6RRmh^x#9y">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="src">
                                                                                                                                      <block type="string_value" id="ec],n@2tHLLta0i.}W^-9">
                                                                                                                                        <field name="value">${image_path}linel.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_angle" id=")k/uvtQNg){@1NxfEV9(1">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="angle">
                                                                                                                                          <block type="expression" id="$aVBTFW~M.?zN(XJg/1r">
                                                                                                                                            <field name="value">180 - $cell.data</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="image_shape" id="D[UA}TR@12ttbioohuTx;">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="rSpmopD^0CAdi/26.k$;">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="-I^YUiLz+^YyLHMrO`2|">
                                                                                                                                    <field name="value">$cell.centerX - Ox($cell.data)</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="H@2?[0fT@1O46h[[r,9Fw-">
                                                                                                                                    <field name="value">$cell.centerY + 50</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_key" id="_{(}MkF}@2$aI{+{TyZAo">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="key">
                                                                                                                                      <block type="string_value" id="B]6d{=_el`C?g#BO@1%:z">
                                                                                                                                        <field name="value">point.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_src" id="h$Q/Y()3]Fl0D/K!FIU$">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="src">
                                                                                                                                          <block type="string_value" id="mE;$~g3-zUuEjy[d?!l+">
                                                                                                                                            <field name="value">${image_path}point.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="grid_shape" id="]Y$F+f3%%XMz%$cyI;]{">
                                                                            <statement name="#props">
                                                                              <block type="prop_grid_dimension" id="VQq+KsZT7K3bU2{;4j3o">
                                                                                <field name="#prop"></field>
                                                                                <value name="rows">
                                                                                  <block type="expression" id=".??OrJmmSKH~{X,bgx%F">
                                                                                    <field name="value">1</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="cols">
                                                                                  <block type="expression" id="d~7wh7aJ3Jyc:5$Kj3cT">
                                                                                    <field name="value">3</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_position" id="`Z#hTVM;--n2:EjMLiw4">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="]ITB=AO-x.0g!1)-IdQ0">
                                                                                        <field name="value">400</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="`vD3G,cBe@2S:o@2QpI(8n">
                                                                                        <field name="value">300</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_size" id="1Wg4l@2OOAYB0Rp@2MuI[L">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="width">
                                                                                          <block type="expression" id="js)#WE#t[vS+slGT)%Vq">
                                                                                            <field name="value">780</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="height">
                                                                                          <block type="expression" id="TjIlXMD1My|)]/$m`y`(">
                                                                                            <field name="value">200</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_anchor" id="b`a9Ix4p,bJCg$v1oEW6">
                                                                                            <field name="#prop">anchor</field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="GE+Z~(H9bxZ;(^g!A%gE">
                                                                                                <field name="value">0.5</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="j;CW/~(B!GN2MWFJef]F">
                                                                                                <field name="value">0.5</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_grid_cell_source" id="YEmN`f@27PtUK=UGRTz6L">
                                                                                                <field name="#prop">cell.source</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="})G6k[l1zY0AmPX2joQE">
                                                                                                    <field name="value">[0, 1, 2]</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_grid_random" id="jy)n?jNi0:6bn@2cq{Pr/">
                                                                                                    <field name="#prop"></field>
                                                                                                    <field name="random">FALSE</field>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_show_borders" id="pVg_5qrg=8omxVNMHpa[">
                                                                                                        <field name="#prop">#showBorders</field>
                                                                                                        <field name="value">FALSE</field>
                                                                                                        <next>
                                                                                                          <block type="prop_grid_cell_template" id="Viw_pd[0KFB%+!2Nb:ur">
                                                                                                            <field name="variable">$cell</field>
                                                                                                            <field name="#prop">cell.template</field>
                                                                                                            <field name="#callback">$cell</field>
                                                                                                            <statement name="body">
                                                                                                              <block type="choice_custom_shape" id="0~e++_lJwIi@2e59wsgwZ">
                                                                                                                <field name="action">click-one</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="RxzNBMYolBajvDpocI)l">
                                                                                                                    <field name="value">angle[$cell.data]</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <statement name="template">
                                                                                                                  <block type="image_shape" id="D|XP18n=I[2+^^mp0hlJ">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_position" id="NS,gQXG:TVgf058^Ut)M">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="8w`$jxu.Z_`3HGasX8:N">
                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="NI.@1mh95p8OsU~fs5v@2H">
                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_key" id="fTJaU!|o1J:B8$wjjZqK">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="key">
                                                                                                                              <block type="string_value" id="D}VvV7ynqH7CUV,`qBay">
                                                                                                                                <field name="value">hide.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_src" id="+O^Uz]01aT|,xbf0FUFe">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="src">
                                                                                                                                  <block type="string_value" id="^?6zj3r7Bj+i+LMzB9V:">
                                                                                                                                    <field name="value">${image_path}hide.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="func_add_answer" id="SWk`z?10./oV2Y_yOq.X" inline="true">
                                                                                <value name="value">
                                                                                  <block type="expression" id="4FYtlQjcI@1}l2TE7jeS^">
                                                                                    <field name="value">a</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </statement>
                                                    <next>
                                                      <block type="if_then_block" id="xHxG,3-?P!PYs3o=0u^@2">
                                                        <value name="if">
                                                          <block type="expression" id="{j`;M/lyr=@1l+L}t:a">
                                                            <field name="value">type == 2</field>
                                                          </block>
                                                        </value>
                                                        <statement name="then">
                                                          <block type="variable" id="`IP}L,,I1M1G`X_ty7va">
                                                            <field name="name">a</field>
                                                            <value name="value">
                                                              <block type="random_one" id=":?3|1XG?[w3}#SjDp?#?">
                                                                <value name="items">
                                                                  <block type="expression" id="Hk-V?xV1m!XLWa;])_ZQ">
                                                                    <field name="value">list_angle</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="text_shape" id="z]HcULE%VOp/sax1!0uv">
                                                                <statement name="#props">
                                                                  <block type="prop_position" id="8E@2H5JM?kZjrmjG-2?,~">
                                                                    <field name="#prop"></field>
                                                                    <value name="x">
                                                                      <block type="expression" id="G@1]8`X;F=E,#u/Gf:fT.">
                                                                        <field name="value">400</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="y">
                                                                      <block type="expression" id="D@29o:wsPkhsGlFT8xEU}">
                                                                        <field name="value">40</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_anchor" id="]gw7wg;$CE$TsdDb.QgG">
                                                                        <field name="#prop">anchor</field>
                                                                        <value name="x">
                                                                          <block type="expression" id=",?cv4V:ZL.0wtsrOwDZz">
                                                                            <field name="value">0.5</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id="Rwq-X94XI-ljDoBS8f6D">
                                                                            <field name="value">0.5</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_text_contents" id="~D01Pos@14fWm]|~fqxX,">
                                                                            <field name="#prop"></field>
                                                                            <value name="contents">
                                                                              <block type="string_value" id="epejIMo=cB3-@1vSIZG5)">
                                                                                <field name="value">${ type == 2 ? 'What is the size of the angle?' : 'Find the unknown angle.'}</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_text_style" id="}se0C#(._s0|/@12])^q^">
                                                                                <field name="base">text</field>
                                                                                <statement name="#props">
                                                                                  <block type="prop_text_style_font_size" id="B;)F9lEHvypnhpB7Dun^">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="fontSize">
                                                                                      <block type="expression" id="0RQ/QE{wj!Gq_J4{{/-)">
                                                                                        <field name="value">35</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_text_style_fill" id="R}eRnWKi|4g.!dVp@2BEW">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="fill">
                                                                                          <block type="string_value" id="0,OJ=@1Yq-bebTPG)z9C6">
                                                                                            <field name="value">black</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_style_stroke" id="rmvSkn5G^/rJh]7#o;|]">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="stroke">
                                                                                              <block type="string_value" id="0#7)tkAId|Wa[hx756C6">
                                                                                                <field name="value">white</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_style_stroke_thickness" id="=lI5N;Cfe|!/=#)n4bU,">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="strokeThickness">
                                                                                                  <block type="expression" id="(ah!oxuCJmxh/){=;nDs">
                                                                                                    <field name="value">2</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style_wordwrap" id="I,2|Rr#B7Mc`e^.JF0}F">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="wordWrap">
                                                                                                      <block type="expression" id="FrjUkUz{D#NGVj@25r:Gi">
                                                                                                        <field name="value">true</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style_wordWrapWidth" id="h..xaiUAK9K5(H@2IGE}!">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="wordWrapWidth">
                                                                                                          <block type="expression" id="}@1A!@20F1Z8|_.Txk#R34">
                                                                                                            <field name="value">500</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <next>
                                                                  <block type="choice_custom_shape" id="2!QEk.iqF35(4#b1O[ZC">
                                                                    <field name="action">drag</field>
                                                                    <value name="value">
                                                                      <block type="expression" id="Lx?mT#uoj/E[KCq?gdS@1">
                                                                        <field name="value">undefined</field>
                                                                      </block>
                                                                    </value>
                                                                    <statement name="template">
                                                                      <block type="image_shape" id="DF)mTKwrQMv(DTsUzZZK">
                                                                        <statement name="#props">
                                                                          <block type="prop_position" id="{hR(8e3S#%6gjE1UYr3:">
                                                                            <field name="#prop"></field>
                                                                            <value name="x">
                                                                              <block type="expression" id="TGxIf`)!=A6KDV@2n9o9h">
                                                                                <field name="value">600</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="y">
                                                                              <block type="expression" id="|2nDR|fyP+~+Y^x4]6@2@2">
                                                                                <field name="value">380</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_anchor" id="P+-HJDvfU+URT)Fv^4t)">
                                                                                <field name="#prop">anchor</field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="ad%#:g!f7yY;OSfc}}In">
                                                                                    <field name="value">0.5</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="A:l).$)I`xh38`;IrJFu">
                                                                                    <field name="value">1</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_max_size" id="!4=ao8|Or_:D?@1_:I;PT">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="maxWidth">
                                                                                      <block type="expression" id="AlfSG3#PnW3~uU#2SDQ_">
                                                                                        <field name="value">300</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="maxHeight">
                                                                                      <block type="expression" id="tr+H;t}q.RMv#MJ8]}HA">
                                                                                        <field name="value">0</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_image_key" id="p`4f[o_,@12L9H8ZeS0X?">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="key">
                                                                                          <block type="string_value" id="JeW5twm1/Q}CtXt_+#Im">
                                                                                            <field name="value">pro.png</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_image_src" id="XF^jLq7qku!=;Zo)so#f">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="src">
                                                                                              <block type="string_value" id="{AJA?Z3Y,h_=YG[rWhGV">
                                                                                                <field name="value">${image_path}pro.png</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="grid_shape" id=")HS(ng1[i`R)2_/MDM0F">
                                                                        <statement name="#props">
                                                                          <block type="prop_grid_dimension" id="|i|+gB/dp_6PI32.(eOG">
                                                                            <field name="#prop"></field>
                                                                            <value name="rows">
                                                                              <block type="expression" id="B`$#^$ZuLlj+fRye2#Sv">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="cols">
                                                                              <block type="expression" id=";,=-5g(WgluMt[u^%?o$">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_position" id="X+@2NS5u5@2`:!mt||H-/7">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="t@1c,Dy4hUVl#NvmnO:a3">
                                                                                    <field name="value">250</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="jp12pnKU_%@2IgM+_[,jk">
                                                                                    <field name="value">200</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_size" id="1nTz1`n#E?9^(Ss[%[P^">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="width">
                                                                                      <block type="expression" id="z@2D%HkFym{Nm1k9dyDS#">
                                                                                        <field name="value">300</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="height">
                                                                                      <block type="expression" id="s%Xvx/~vP4C_~3_.m]CN">
                                                                                        <field name="value">200</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_anchor" id="1Gdw%[A+62;5~jW_?I;r">
                                                                                        <field name="#prop">anchor</field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="ZXJ[g:Qmme^),`{2afpU">
                                                                                            <field name="value">0.5</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="0Y4R0),MdExc[H#2V3!(">
                                                                                            <field name="value">0.5</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_grid_cell_source" id="Z0CwMs=Y7ke+!hAVU[WL">
                                                                                            <field name="#prop">cell.source</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="]UD$3$kwmv3eyVWS+9De">
                                                                                                <field name="value">1</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_grid_random" id="8k~wfn-ck{bOn89/_;@2g">
                                                                                                <field name="#prop"></field>
                                                                                                <field name="random">FALSE</field>
                                                                                                <next>
                                                                                                  <block type="prop_grid_show_borders" id="WK$.qJf%MTJ0_)+5Yl/3">
                                                                                                    <field name="#prop">#showBorders</field>
                                                                                                    <field name="value">FALSE</field>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_cell_template" id="d+-^o|6n}cA86LiB-XeM">
                                                                                                        <field name="variable">$cell</field>
                                                                                                        <field name="#prop">cell.template</field>
                                                                                                        <field name="#callback">$cell</field>
                                                                                                        <statement name="body">
                                                                                                          <block type="image_shape" id="s59es+y~ucVWaMC98If4">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_position" id="SSUjaYKyuS1+`la,[z%E">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="r(^32G[?K9bEG{#V:#J;">
                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="Wi4A3@17pm#$_Z#AmWp6[">
                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_size" id="d9)RYtJNT;4kXr=woF?_">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="width">
                                                                                                                      <block type="expression" id="6?3V(Xb)B7`q=@13.6Ow,">
                                                                                                                        <field name="value">240</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="height">
                                                                                                                      <block type="expression" id="g-s}TkN+DVs1dZTzBbnM">
                                                                                                                        <field name="value">200</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_key" id=")bV7GbKs@1oL=?9+cS?}~">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="key">
                                                                                                                          <block type="string_value" id="gYng!/fgk4F{+~1+y?2u">
                                                                                                                            <field name="value">box.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_src" id="i%#wD.vJO`]l:ZKcwLbp">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="src">
                                                                                                                              <block type="string_value" id="cS#]B#Ax+rW32},zHo{,">
                                                                                                                                <field name="value">${image_path}box.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="if_then_else_block" id="}%)H=K_WDrwZ_}~ljR86">
                                                                                                                <value name="if">
                                                                                                                  <block type="expression" id="Os-7}+P%WEY}u{K]Hz7w">
                                                                                                                    <field name="value">$cell.data == 90</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <statement name="then">
                                                                                                                  <block type="image_shape" id="K2LCx`rC}~U/Q]O1uNmr">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_position" id="3gPa,FW|?L#IurborQ0^">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="S3Rm5~;y{JNu7drgANt6">
                                                                                                                            <field name="value">$cell.centerX - Ox(a)</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="44Ald#4g.jTqA65CJV|p">
                                                                                                                            <field name="value">$cell.centerY + 50</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_key" id="A!Xt1gJ4~A)?)Mr$C0Vm">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="key">
                                                                                                                              <block type="string_value" id="qT;D1;y}t1K+KIBhlhG[">
                                                                                                                                <field name="value">sq.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_src" id="w@1qx+1T~qyd!X(2.G$fi">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="src">
                                                                                                                                  <block type="string_value" id="_I^|Nz,FIji?O%)7X+yx">
                                                                                                                                    <field name="value">${image_path}sq.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <statement name="else">
                                                                                                                  <block type="image_shape" id="Q+),z`+]S-W_wZq^8I^=">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_position" id="Nqi35%|lgC`2sewbx;]_">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="h1@2.GU1a?YLTQTnC93=e">
                                                                                                                            <field name="value">$cell.centerX - Ox(a)</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="[MdS3p$6=S;uK)gOhh=/">
                                                                                                                            <field name="value">$cell.centerY + 50</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_key" id="j6m;90d:5nbBcVDL(,V,">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="key">
                                                                                                                              <block type="string_value" id="aP|9t,HN^D7UP,Bl6gj)">
                                                                                                                                <field name="value">cl.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_src" id="WVkZe^T_ZCiRw(N}5FMQ">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="src">
                                                                                                                                  <block type="string_value" id="@25hm(F|ndIBg!nQjBeGj">
                                                                                                                                    <field name="value">${image_path}cl.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_angle" id="Zf+oP4j5R~n4wzxUf3B%">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="angle">
                                                                                                                                      <block type="expression" id="p8#E=4I|z}]Z#lGJe%[X">
                                                                                                                                        <field name="value">360 - a</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="image_shape" id="IXNc=}.{$!`|]L=Bbwar">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_position" id="F.:i}$6B?_{WgZsDpjvk">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="yUVd;`F/n~5q?pLmD9~H">
                                                                                                                            <field name="value">$cell.centerX - Ox(a)</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="weNVWgSAE`_8U@28a%7MX">
                                                                                                                            <field name="value">$cell.centerY + 50</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_key" id="y3m9:#4F:J$NAw{h=(;O">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="key">
                                                                                                                              <block type="string_value" id="Q6uh[]NnTS6kzZ^xb5i$">
                                                                                                                                <field name="value">liner.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_src" id="[rc%xa@1]txYA,n5Sj_g.">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="src">
                                                                                                                                  <block type="string_value" id="GQRzq.ydq+a4`4Q3:CUS">
                                                                                                                                    <field name="value">${image_path}liner.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="image_shape" id="?WzW=L:qwmSh@1LLQb$%I">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="?I8[_xGR7JT6br7)8x^~">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="3B(9Y9rI`2LRHav#H[-k">
                                                                                                                                <field name="value">$cell.centerX - Ox(a)</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="tF_^lbN6ud!+esmK[3aN">
                                                                                                                                <field name="value">$cell.centerY + 50</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_key" id="@1w)tdV?NAIfB=Y$@1?)-`">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="key">
                                                                                                                                  <block type="string_value" id="0lSHFqx,(YB7SZEkHZ1_">
                                                                                                                                    <field name="value">linel.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_src" id="vN={L#}rZu,Xr(I{cRJc">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="src">
                                                                                                                                      <block type="string_value" id="X+XZ3EuXRKxC?NJhFKFF">
                                                                                                                                        <field name="value">${image_path}linel.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_angle" id="#[pY4w@19evse:fWR6}:r">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="angle">
                                                                                                                                          <block type="expression" id="G~F^_yqN4OhzbQLduTn}">
                                                                                                                                            <field name="value">180 - a</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="image_shape" id="iWIEYp#75qruJu:ZeHYF">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="/S7Y~fSzB,$E+/@1qq{nb">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="(YXP0%lm/m~(QPZ=Y5]3">
                                                                                                                                    <field name="value">$cell.centerX - Ox(a)</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="ZSJQ[DA):npIKMkV2moV">
                                                                                                                                    <field name="value">$cell.centerY + 50</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_key" id="@1}vWJlk.GfF%/(]HOE=@1">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="key">
                                                                                                                                      <block type="string_value" id="72k6UY$#8S79SE,k5e%F">
                                                                                                                                        <field name="value">point.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_src" id="gCRj_Q^EFfNIIFj-A{I=">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="src">
                                                                                                                                          <block type="string_value" id="hI2A~q))zsQG7Poin24A">
                                                                                                                                            <field name="value">${image_path}point.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="image_shape" id="{b@2OG.JZavV~!`rv3h,I">
                                                                            <statement name="#props">
                                                                              <block type="prop_position" id="[B^jsIm}cDI%eS~X|CA0">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="V8m,+{_Oyqo`P-DBVRT1">
                                                                                    <field name="value">600</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="/D6H^Dj.y4Cj%kp?$VI.">
                                                                                    <field name="value">150</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_image_key" id="ln9i5%#@2pSQ`oFDu/eDA">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="key">
                                                                                      <block type="string_value" id="TI^V0#)vQ7LQ-,Nt7o7=">
                                                                                        <field name="value">shape.png</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_image_src" id="c%l{]rF1Smzp{r5vVkQ3">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="src">
                                                                                          <block type="string_value" id="^R#Z?CH=A!|5q[W4Umt=">
                                                                                            <field name="value">${image_path}shape.png</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="algorithmic_input_shape" id="[e}LcmqF@19.%T?]@1jB-Z">
                                                                                <statement name="#props">
                                                                                  <block type="prop_value" id="TN6clfe}:4H;@1k0w%+`S">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="@1Y0=VVDuz^gR8?]A.GgV">
                                                                                        <field name="value">a</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_position" id="(u^k=nloWKrc75-ihkQ6">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="+SQo+%UqGFz6@107iOi%4">
                                                                                            <field name="value">600</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="aOD#P0{Z$l:BCNh)9@2~E">
                                                                                            <field name="value">150</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_size" id="~)S$^+Glx|;{-)@1m?MCt">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="width">
                                                                                              <block type="expression" id="wdRRw?lccA]3huvks^0:">
                                                                                                <field name="value">125</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="height">
                                                                                              <block type="expression" id="FlQ@1R-hpt5|W3YEsjl5h">
                                                                                                <field name="value">68</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_input_keyboard" id="pX[+1]_P03Uw?r6RhxOm">
                                                                                                <field name="#prop"></field>
                                                                                                <field name="keyboard">numbers1</field>
                                                                                                <next>
                                                                                                  <block type="prop_input_max_length" id="e^196@2S|6ukB7~2Bxi9C">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="maxLength">
                                                                                                      <block type="expression" id="/_E;-7``ONG+]nS_NxH+">
                                                                                                        <field name="value">a.toString().length</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_input_result_position" id="#LH4fkex}OZhG#[@2d;3P">
                                                                                                        <field name="#prop"></field>
                                                                                                        <field name="resultPosition">bottom</field>
                                                                                                        <next>
                                                                                                          <block type="prop_tab_order" id="s|F@1Izb{|@2lRY,asD!hu">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="tabOrder">
                                                                                                              <block type="expression" id="2[il+_dgVQKQm?SvtFDj">
                                                                                                                <field name="value">0</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_stroke" id="D`nCliS/s1emx9euC0}t">
                                                                                                                <field name="#prop">stroke</field>
                                                                                                                <next>
                                                                                                                  <block type="prop_fill" id="Dz-AaH/!wx?BO[MH9dIz">
                                                                                                                    <field name="#prop">fill</field>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style" id="!1Qr3{IxD@1PMJ]G^LlcN">
                                                                                                                        <field name="base">text</field>
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_text_style_font_size" id="!(nlSpda9T+o/iXBj}YF">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="fontSize">
                                                                                                                              <block type="expression" id="FQTZZB_+$@1-hM?K.!NSO">
                                                                                                                                <field name="value">32</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_fill" id="w-4`M-zsemYrO[}g`_|q">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="fill">
                                                                                                                                  <block type="string_value" id="G;VmNGg}u8OZR_#3M_Oe">
                                                                                                                                    <field name="value">black</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_stroke" id="ahppz0%Y{eD@2C$!vyJ4m">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="stroke">
                                                                                                                                      <block type="string_value" id="}KNcD$1%M)+@1a=7A%7,,">
                                                                                                                                        <field name="value">white</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="TBWlBG{V@2q@2LOjXi7|@16">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="strokeThickness">
                                                                                                                                          <block type="expression" id="bs_awM@2W;X@1OG#fQ+@2no">
                                                                                                                                            <field name="value">2</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </statement>
                                                        <next>
                                                          <block type="if_then_block" id="qK:qBkHv(mk#,S+BN0aS">
                                                            <value name="if">
                                                              <block type="expression" id="Ov$-D6)N4G4d4QhB(0tA">
                                                                <field name="value">type == 3</field>
                                                              </block>
                                                            </value>
                                                            <statement name="then">
                                                              <block type="variable" id="2[$%]87GK/51Th/o,/p@1">
                                                                <field name="name">angle</field>
                                                                <value name="value">
                                                                  <block type="random_many" id="[ms^LiA7#CN[vCa(K@1Rk">
                                                                    <value name="count">
                                                                      <block type="expression" id=")r^%paErj#t@1[r60QNk^">
                                                                        <field name="value">2</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="items">
                                                                      <block type="expression" id="-kG0LW%`zn==WzB9vh+2">
                                                                        <field name="value">list_angle</field>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="text_shape" id="jD`nhU52onYEB7FF|wxx">
                                                                    <statement name="#props">
                                                                      <block type="prop_position" id="7n=cV3o?Rk-=Sdowx..+">
                                                                        <field name="#prop"></field>
                                                                        <value name="x">
                                                                          <block type="expression" id="-~M7JpvOl8|il=(=7}EW">
                                                                            <field name="value">400</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id="LO)tUHM/9@2T(4.{j]IrV">
                                                                            <field name="value">0</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_anchor" id="4$;gHJgyoGdfwU~@2u!/`">
                                                                            <field name="#prop">anchor</field>
                                                                            <value name="x">
                                                                              <block type="expression" id="joCyUc8cBU$t-gZWeGrl">
                                                                                <field name="value">0.5</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="y">
                                                                              <block type="expression" id="mv.92n23V=f)A$HLWF+X">
                                                                                <field name="value">0</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_text_contents" id="T@2d:+tOLf.!z`Keo^Xy-">
                                                                                <field name="#prop"></field>
                                                                                <value name="contents">
                                                                                  <block type="string_value" id="_k8TNhIfpf5lgp=h-s`j">
                                                                                    <field name="value">Use a protractor to measure each angle.</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_text_style" id="fIZWp2^Bdm#u0qUm9#(|">
                                                                                    <field name="base">text</field>
                                                                                    <statement name="#props">
                                                                                      <block type="prop_text_style_font_size" id="tEahV.xg%f`v61o@1TOR}">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="fontSize">
                                                                                          <block type="expression" id="MD(zZw;C:+^GG|Yy@1jl4">
                                                                                            <field name="value">35</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_style_fill" id="SRlWH3F|P2ZIlf)~jtqy">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="fill">
                                                                                              <block type="string_value" id="_k]e1}L1z5=[2H{bibnW">
                                                                                                <field name="value">black</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_style_stroke" id="K+@1+@1fDk;a/YOl?U55t#">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="stroke">
                                                                                                  <block type="string_value" id="DT#Shw9BD/ils|LW1lsc">
                                                                                                    <field name="value">white</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style_stroke_thickness" id="BQWF:Ix2%X)t,{Z2zpMB">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="strokeThickness">
                                                                                                      <block type="expression" id="Ag@1L(,~[gu=L;gZw!tC{">
                                                                                                        <field name="value">2</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="choice_custom_shape" id="U!e5558{lrMv0!7;[=nF">
                                                                        <field name="action">drag</field>
                                                                        <value name="value">
                                                                          <block type="expression" id="@2S??Vw@1N6x??M(0@2UW6k">
                                                                            <field name="value">undefined</field>
                                                                          </block>
                                                                        </value>
                                                                        <statement name="template">
                                                                          <block type="image_shape" id="h^C[iz@1.N?+P[[=h~8`W">
                                                                            <statement name="#props">
                                                                              <block type="prop_position" id="afVk6RohBDbSy)qKN)O;">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id=".tORne2{1yYPUBKm1k9T">
                                                                                    <field name="value">0</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="/vENce,,(!zKSEkdPCDJ">
                                                                                    <field name="value">150</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_anchor" id="01[t)a}`^-CB@25GhU#0V">
                                                                                    <field name="#prop">anchor</field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="J,t4V9w:%lD:YFWnhv`V">
                                                                                        <field name="value">0</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="PbB5g{zt$NoDibP,odU_">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_max_size" id="-b?H5TDKH@2[T{C7p=1;Y">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="maxWidth">
                                                                                          <block type="expression" id="2q{V~ii~EhyF6lMj~nV@2">
                                                                                            <field name="value">300</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="maxHeight">
                                                                                          <block type="expression" id="FF-V2wigB,fPWctAPPky">
                                                                                            <field name="value">0</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_image_key" id="^Tq@2U}Y!HX|@2+uyFl0{C">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="key">
                                                                                              <block type="string_value" id="G(6m5QMg@2,,%8u4K[;Tl">
                                                                                                <field name="value">pro.png</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_image_src" id="VXdvwlakP@1w)0x^eziy)">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="src">
                                                                                                  <block type="string_value" id="A+@1e?bpql^Qid`{ZGv@1v">
                                                                                                    <field name="value">${image_path}pro.png</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="grid_shape" id="Ak3I^w;,/`nRLwIFpze|">
                                                                            <statement name="#props">
                                                                              <block type="prop_grid_dimension" id="QvN6oi5z|:qN3cn=NjmT">
                                                                                <field name="#prop"></field>
                                                                                <value name="rows">
                                                                                  <block type="expression" id=")j[B?hdW:$eAJ5OyF|VZ">
                                                                                    <field name="value">1</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="cols">
                                                                                  <block type="expression" id="+vl_[dZ5q)xa@1(D{}cvI">
                                                                                    <field name="value">2</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_position" id="2m[PHVfgkD^0^Z`y24-s">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="{KZ@1s(euI}rDALNftRV:">
                                                                                        <field name="value">800</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="Sgr$A/;)r:Z0.@1VWsQ3G">
                                                                                        <field name="value">150</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_size" id="U0H((!_.B$,LD3z(3$y.">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="width">
                                                                                          <block type="expression" id="z$Bmd]ElXkl^[{$yXO}(">
                                                                                            <field name="value">500</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="height">
                                                                                          <block type="expression" id="apR1YwGE@1}5H9u+5v;(=">
                                                                                            <field name="value">200</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_anchor" id="%p}oX=49WXs=aETgxE_|">
                                                                                            <field name="#prop">anchor</field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="Zu;yLcteDa2mip{U#Y8`">
                                                                                                <field name="value">1</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="~lap,/{!!p4}.SqO~nJ6">
                                                                                                <field name="value">0.5</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_grid_cell_source" id="@2vRS3)wldg!lh=8fe5X1">
                                                                                                <field name="#prop">cell.source</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="B~JFQ?06rtA4OsUZ7q}I">
                                                                                                    <field name="value">angle</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_grid_random" id="H1aRyx@1PS0ehnQ2[[$hA">
                                                                                                    <field name="#prop"></field>
                                                                                                    <field name="random">FALSE</field>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_show_borders" id=";ZZZ.n8Mp#}A3Lx%NGlF">
                                                                                                        <field name="#prop">#showBorders</field>
                                                                                                        <field name="value">FALSE</field>
                                                                                                        <next>
                                                                                                          <block type="prop_grid_cell_template" id="WA/bXXS-~3Yna)r|[in,">
                                                                                                            <field name="variable">$cell</field>
                                                                                                            <field name="#prop">cell.template</field>
                                                                                                            <field name="#callback">$cell</field>
                                                                                                            <statement name="body">
                                                                                                              <block type="image_shape" id="2bLa}U/A@2R^c8oX?@2b^/">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_position" id="@1T(@1mr95ls5PMj|`V[)y">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="j,S%j7T69~fUse-K;`X9">
                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="U^4^(T.(y+@2K)!%TH]]K">
                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_size" id="j%-.asJoO@1JPu^%M6|cM">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="width">
                                                                                                                          <block type="expression" id="kfU]^q_mxhWS@1U)?@2w#Y">
                                                                                                                            <field name="value">240</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="height">
                                                                                                                          <block type="expression" id="NX{o|ol^^mmr,4JwI{5X">
                                                                                                                            <field name="value">200</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_key" id="Dqom#X,F3p20^bm]u-B?">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="key">
                                                                                                                              <block type="string_value" id="R6!U8b3$7OrLQporENS:">
                                                                                                                                <field name="value">box.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_src" id="oe(d_^W-~^f)VzzKa}u;">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="src">
                                                                                                                                  <block type="string_value" id=".+~BfZ2^V_l?{H|w4w4y">
                                                                                                                                    <field name="value">${image_path}box.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="if_then_else_block" id="`HEQ%r,qwKIX?Jt]eewG">
                                                                                                                    <value name="if">
                                                                                                                      <block type="expression" id="uUF@1P!2k,uU$)yVer2{Y">
                                                                                                                        <field name="value">$cell.data == 90</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <statement name="then">
                                                                                                                      <block type="image_shape" id="Fr$s@1OOaf?-K0[4Y?d(Z">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="%tMh3!.idN0Rxwea(H?k">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="q~WH[a;bEF:gC_A3Xb!N">
                                                                                                                                <field name="value">$cell.centerX - Ox($cell.data)</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="f,W7X=32+qu0oqBz39BH">
                                                                                                                                <field name="value">$cell.centerY + 50</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_key" id="l@19Z?b^[ZzG@1OW+,Ec[Z">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="key">
                                                                                                                                  <block type="string_value" id="BJTrY4^N)f3O%T-9=Ney">
                                                                                                                                    <field name="value">sq.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_src" id="@1,3X/#}3Zc%7=q#.#bgn">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="src">
                                                                                                                                      <block type="string_value" id="D{n_kAJ#]M~b}c7[`HqR">
                                                                                                                                        <field name="value">${image_path}sq.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <statement name="else">
                                                                                                                      <block type="image_shape" id="@19N~TL`_DDhXdDQ4@1+O%">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="(b~i07r6Dx/dxC6_SZ@2y">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="RO;|Vqlj~)b?`neAR!D$">
                                                                                                                                <field name="value">$cell.centerX - Ox($cell.data)</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="qqNhTKAKS,cBfc^Ula@2G">
                                                                                                                                <field name="value">$cell.centerY + 50</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_key" id="Q8gWRsA[nEWl~%s_f@27u">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="key">
                                                                                                                                  <block type="string_value" id="v76(-p]D.4meIiSQ`6`i">
                                                                                                                                    <field name="value">cl.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_src" id="mIY##H_L==m9{J1YItp6">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="src">
                                                                                                                                      <block type="string_value" id="ZfrTI1RKP78U59-EQzca">
                                                                                                                                        <field name="value">${image_path}cl.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_angle" id="8O=]$QEU6[G6?zTRy8?B">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="angle">
                                                                                                                                          <block type="expression" id="=@1(aHWH55InX2HmjJ-JJ">
                                                                                                                                            <field name="value">360 - $cell.data</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="image_shape" id="QKR(f-U|]OQ@1P)6o3f[l">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="IKmatJsQgy@1bKxkW]G+N">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="-@2A]}Kw{;)A!Kal(`jrK">
                                                                                                                                <field name="value">$cell.centerX - Ox($cell.data)</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="Z6oO7U9IJ_kZS$e+EB94">
                                                                                                                                <field name="value">$cell.centerY + 50</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_key" id="1);|/wo=8qy$aDG1#2v%">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="key">
                                                                                                                                  <block type="string_value" id="?~X$DMg;qun6/R=|G7%Z">
                                                                                                                                    <field name="value">liner.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_src" id="2`=f5n]YrMW/!S;ifPxL">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="src">
                                                                                                                                      <block type="string_value" id="ipw7!N,CT_`[}Dapj#oW">
                                                                                                                                        <field name="value">${image_path}liner.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="image_shape" id="lLnh`3Gb+f3}0Vk)@1TQf">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="[EG_jW1Rj|0-$?@1~Xa4@1">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="e89V)@2I_aa[uI[U(.ozH">
                                                                                                                                    <field name="value">$cell.centerX - Ox($cell.data)</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="BihTi|O-w.8_4RxO71@2R">
                                                                                                                                    <field name="value">$cell.centerY + 50</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_key" id="fi]gSMaig/#)14@2g`JH$">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="key">
                                                                                                                                      <block type="string_value" id=".^0|!|NA|Ejv$]EE3hue">
                                                                                                                                        <field name="value">linel.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_src" id="yFd/:K+CCYg(ws}ZVLg%">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="src">
                                                                                                                                          <block type="string_value" id="W_@1U#%MhPz1WOvnyl?eD">
                                                                                                                                            <field name="value">${image_path}linel.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_angle" id="~=5[G-Zd9BnnysdX^Z7h">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="angle">
                                                                                                                                              <block type="expression" id="Rm@2|9]UIP5|`siv9Ikc+">
                                                                                                                                                <field name="value">180 - $cell.data</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="image_shape" id="F(]sA?.5r1G]9vxgNV,M">
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_position" id="TV1FyT=!M4qt$@1x.[_17">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="x">
                                                                                                                                      <block type="expression" id="IwOPo4z(+F`@16aT~:gXM">
                                                                                                                                        <field name="value">$cell.centerX - Ox($cell.data)</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="y">
                                                                                                                                      <block type="expression" id="hrZ+G4@1YCm.jBE4QNruh">
                                                                                                                                        <field name="value">$cell.centerY + 50</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_key" id="4u8+6ZASz^/y/|A/~(##">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="key">
                                                                                                                                          <block type="string_value" id="%D!dTOD2c~E6TF=,@2}QS">
                                                                                                                                            <field name="value">point.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_src" id="`kcinW~Lzf;3Rm@2B0$QV">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="src">
                                                                                                                                              <block type="string_value" id="3[,OGDV|xZnGMuD2Ne?w">
                                                                                                                                                <field name="value">${image_path}point.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="list_shape" id="Z$|_x=Ie8YXk!8Y]bVP/">
                                                                                <statement name="#props">
                                                                                  <block type="prop_list_direction" id="?jcUseW4s3dE+%d{`!9Q">
                                                                                    <field name="#prop"></field>
                                                                                    <field name="dir">horizontal</field>
                                                                                    <next>
                                                                                      <block type="prop_position" id="!m]Dt.%}Tw,W[@1^6^0@2f">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="UTLN|2R`$BM=(!B/kXCi">
                                                                                            <field name="value">400</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="6!]i!ESFr@2l[({(PK+|N">
                                                                                            <field name="value">295</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_anchor" id="zsO}a8Plsf}+BeLM~l|%">
                                                                                            <field name="#prop">anchor</field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id=";OSH3?j@2+v)6uBQ?4P0d">
                                                                                                <field name="value">0.5</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="c-^Bk^z[fy$Zq##c:A">
                                                                                                <field name="value">0.5</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <statement name="items">
                                                                                  <block type="list_item_shape" id="x[)ytzZ9(@2h^WPPRjGt9">
                                                                                    <statement name="#props">
                                                                                      <block type="prop_list_align" id="5e[ltO[5?]5d5Ds?G66F">
                                                                                        <field name="#prop"></field>
                                                                                        <field name="align">middle</field>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <statement name="template">
                                                                                      <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                                                        <statement name="#props">
                                                                                          <block type="prop_anchor" id="O.v@2mKn@1K#+tF;|}C}xI">
                                                                                            <field name="#prop">anchor</field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="+3%GU[X3l%%X`Jv1QL7@2">
                                                                                                <field name="value">0.5</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="Kzp23HjHF7k]A-y1BJPm">
                                                                                                <field name="value">0.5</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="contents">
                                                                                                  <block type="string_value" id="#6btV9p,PGV}wMx.U�">
                                                                                                    <field name="value">Angle 1 =</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                                                    <field name="base">text</field>
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="fontSize">
                                                                                                          <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                                                            <field name="value">36</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="fill">
                                                                                                              <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                                                                <field name="value">black</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_stroke" id=";x|d2{7?:h^S:!3?;c,_">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="stroke">
                                                                                                                  <block type="string_value" id="b#%nq(~oWvdb6A=~|2fN">
                                                                                                                    <field name="value">white</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_stroke_thickness" id="azD0cRb~OLWNidpJ!`|3">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="strokeThickness">
                                                                                                                      <block type="expression" id="$-#@1cMJ4L[WN`W~!-2qT">
                                                                                                                        <field name="value">2</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <next>
                                                                                      <block type="list_item_shape" id="QeZ5qW^fXOSMrL/dHSpO">
                                                                                        <statement name="#props">
                                                                                          <block type="prop_list_align" id="Rv-M2xST5nQ(H:h.j36=">
                                                                                            <field name="#prop"></field>
                                                                                            <field name="align">middle</field>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <statement name="template">
                                                                                          <block type="image_shape" id="?G~x._8m/Iy?@2P/lfA(%">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_image_key" id="{u1[!m@1)-lYSP_k-}g`(">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="key">
                                                                                                  <block type="string_value" id="D1LW+va[/Eg7lTqFQl._">
                                                                                                    <field name="value">shape.png</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_image_src" id="kHWx73DPu?QK.HZ93o?c">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="src">
                                                                                                      <block type="string_value" id="v8HR/u|!V($no0[z^~#G">
                                                                                                        <field name="value">${image_path}shape.png</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_scale" id="cUGCGh::`DoMsw,,]?@1t">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="scale">
                                                                                                          <block type="expression" id="qz8G59X738xI3~(n(X?L">
                                                                                                            <field name="value">0.7</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="algorithmic_input_shape" id="uX$O,4PCgJNZw!3f1t_o">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_value" id="@1OdkF)q|k5;.D}OaElC^">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="uBo2oi)7+:0]hn}@2IqwE">
                                                                                                        <field name="value">angle[0]</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_size" id="eScIjvxMVPwyEF8YF^?h">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="width">
                                                                                                          <block type="expression" id="J%WxVv.Zt}7il#$lku$|">
                                                                                                            <field name="value">120@20.7</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="height">
                                                                                                          <block type="expression" id="1@2lV5Q~83rortharXaFh">
                                                                                                            <field name="value">65@20.7</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_input_keyboard" id="CQ0!eI6.OY.H#3e!qn@2n">
                                                                                                            <field name="#prop"></field>
                                                                                                            <field name="keyboard">numbers1</field>
                                                                                                            <next>
                                                                                                              <block type="prop_input_max_length" id="kQKTN,.(yoY|.5!/(V5!">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="maxLength">
                                                                                                                  <block type="expression" id="[UFt@2p1CVsl){Tl?,}Lb">
                                                                                                                    <field name="value">angle[0].toString().length</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_input_result_position" id="CyaJ6vmD!}?cox:-It4R">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <field name="resultPosition">bottom</field>
                                                                                                                    <next>
                                                                                                                      <block type="prop_tab_order" id="+tmD9:v/5wVoC_3;T.R_">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="tabOrder">
                                                                                                                          <block type="expression" id="hc8$Ax(,f+am%aYPLg@22">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_stroke" id="XptIn7LNA;TlnBPK-d!|">
                                                                                                                            <field name="#prop">stroke</field>
                                                                                                                            <next>
                                                                                                                              <block type="prop_fill" id="iRGc`jnA3_[T(xKV#QSa">
                                                                                                                                <field name="#prop">fill</field>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style" id="L6DsjQb|_@2i@1xY3T~9;u">
                                                                                                                                    <field name="base">text</field>
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_text_style_font_size" id="X:x=Ds`~E=afWQqnBK-h">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="fontSize">
                                                                                                                                          <block type="expression" id="%|oi}5}/;bfv?OW%R+!L">
                                                                                                                                            <field name="value">32</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style_fill" id="!.A-4?@2-%#u=]oHNA@27V">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="fill">
                                                                                                                                              <block type="string_value" id="+^nq,Jhm/;F1(G_!,AwP">
                                                                                                                                                <field name="value">black</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_text_style_stroke" id="+`k{^V`v1$RcA/xOOK/Y">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="stroke">
                                                                                                                                                  <block type="string_value" id="`Zx_WCceYt]!u#y|wqY5">
                                                                                                                                                    <field name="value">white</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_text_style_stroke_thickness" id="xryXvc!7G]!__@2s?b/Y2">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="strokeThickness">
                                                                                                                                                      <block type="expression" id="^cJ{V@1Xb@2A~#bIpLhgRt">
                                                                                                                                                        <field name="value">2</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="list_item_shape" id="x1:FH52@2LS{lE%%1jljA">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_list_align" id="44:|]6KO%$NL/FI|u/H9">
                                                                                                <field name="#prop"></field>
                                                                                                <field name="align">middle</field>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <statement name="template">
                                                                                              <block type="text_shape" id="w.u-8z;TDe;NU`Loh/Tl">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_anchor" id="G7+-v1G1!/QNOG8~=HO,">
                                                                                                    <field name="#prop">anchor</field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="|Eg?==?ji;sxapkf^sm0">
                                                                                                        <field name="value">0.5</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="DR,k#w%~@2T$J[6M:Z`-u">
                                                                                                        <field name="value">0.5</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_contents" id="(_,[-W].uH[#bGjtNk9j">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="contents">
                                                                                                          <block type="string_value" id="vt=[P~B7qZc,e|EIb[2U">
                                                                                                            <field name="value">Angle 2 =</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style" id="+7GoT`@1c:I16=nFf/~UE">
                                                                                                            <field name="base">text</field>
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_text_style_font_size" id="=rybTsH8tv1Z1THU%p6J">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="fontSize">
                                                                                                                  <block type="expression" id="n2nFwu$gamzAl}SjEU3c">
                                                                                                                    <field name="value">36</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_fill" id="SaoYv!3gsf_4f@2M@2jqhS">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="fill">
                                                                                                                      <block type="string_value" id="=-E2K1QN(BnF|ihz%a@2V">
                                                                                                                        <field name="value">black</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_stroke" id="Ys?GEfk8;%0M(u#%2:gK">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="stroke">
                                                                                                                          <block type="string_value" id="@2#=e[ol]G`GntI%N}n?a">
                                                                                                                            <field name="value">white</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_stroke_thickness" id="2zCn@1OUT1OUءEtr{M">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="strokeThickness">
                                                                                                                              <block type="expression" id="y#pFoG6E{/tbKx0#BGg`">
                                                                                                                                <field name="value">2</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="list_item_shape" id="#6?.95axi]DUn7k{NxY.">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_list_align" id="Jd0nC=mM[z,u=3Hwm0zU">
                                                                                                    <field name="#prop"></field>
                                                                                                    <field name="align">middle</field>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <statement name="template">
                                                                                                  <block type="image_shape" id="k,o+vRXOdRIH4q,uMYK6">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_image_key" id="XI2|]rQ1d@2lkV$)VU6U)">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="key">
                                                                                                          <block type="string_value" id=",^Pi:@1?5?3x9Y{d/s6fh">
                                                                                                            <field name="value">shape.png</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_image_src" id="K~-)3T45g{Iq{,#`u$B4">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="src">
                                                                                                              <block type="string_value" id="HFr.r!=b$$eVjyZ:YRC}">
                                                                                                                <field name="value">${image_path}shape.png</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_scale" id="igm:tB@1Jf~wc4)9taNv%">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="scale">
                                                                                                                  <block type="expression" id="@1,ZJr][c/h_(1J=]IX(x">
                                                                                                                    <field name="value">0.7</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="algorithmic_input_shape" id="vRgXDNHw8ZLEM!h5RxrY">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_value" id="[BfGc,^upFV:Ilpkp#2S">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="zUFL5ej:lg_0N@1@1gE90@1">
                                                                                                                <field name="value">angle[1]</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_size" id="Ug68](/D(ujkGN.d6r9X">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="width">
                                                                                                                  <block type="expression" id="dxy/i0=k8.@2W!TvGYE_j">
                                                                                                                    <field name="value">120@20.7</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="height">
                                                                                                                  <block type="expression" id="C4~JR}[(TO?8nNtoEg6f">
                                                                                                                    <field name="value">65@20.7</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_input_keyboard" id="6??E#^52TEH.xz)IAYi?">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <field name="keyboard">numbers1</field>
                                                                                                                    <next>
                                                                                                                      <block type="prop_input_max_length" id="ZBt7bVmP_T?G~RvLMp)_">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="maxLength">
                                                                                                                          <block type="expression" id="vQNil|O|p49A`NXM%wn{">
                                                                                                                            <field name="value">angle[1].toString().length</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_input_result_position" id="E[U;KV]!!@1T@1A~RajXqP">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <field name="resultPosition">right</field>
                                                                                                                            <next>
                                                                                                                              <block type="prop_tab_order" id="z4`UvRPE(`ODBPo`}+.w">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="tabOrder">
                                                                                                                                  <block type="expression" id="tVpMsJPyoMm7khNQz1W(">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_stroke" id="CZRT8N6!CP~plm2;%eGt">
                                                                                                                                    <field name="#prop">stroke</field>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_fill" id="~mMqwO-}6k)L93],VHM?">
                                                                                                                                        <field name="#prop">fill</field>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style" id="2~I3uWz;aen=fdwJo^R!">
                                                                                                                                            <field name="base">text</field>
                                                                                                                                            <statement name="#props">
                                                                                                                                              <block type="prop_text_style_font_size" id="a}:)9mw^C0JpQa)_uMe+">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="fontSize">
                                                                                                                                                  <block type="expression" id="@2l=/jk5:He)d1mn@1=NW(">
                                                                                                                                                    <field name="value">32</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_text_style_fill" id="`a9HpjVB)UQ5IuMJ-o+J">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="fill">
                                                                                                                                                      <block type="string_value" id="}uplsh+00P0oUXW|$a23">
                                                                                                                                                        <field name="value">black</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_text_style_stroke" id=")_1@2By4FT+L)2H)dBPLX">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="stroke">
                                                                                                                                                          <block type="string_value" id="nP;LjF:yr[?Am,S?dgx0">
                                                                                                                                                            <field name="value">white</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_text_style_stroke_thickness" id="C:,rDAX8)p,3YaL4)=IM">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="strokeThickness">
                                                                                                                                                              <block type="expression" id="Y]rAblz|G[AUG_9LvTO,">
                                                                                                                                                                <field name="value">2</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="list_shape" id="$JH%@2Sr;0lzHVH1)+H;l">
                                                                                    <statement name="#props">
                                                                                      <block type="prop_list_direction" id="f}8$c~3.g~h(aQTvkz-.">
                                                                                        <field name="#prop"></field>
                                                                                        <field name="dir">horizontal</field>
                                                                                        <next>
                                                                                          <block type="prop_position" id="Rxs(QVcwwP,|kNFPNxj9">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="_m2^jo2c2A4t=7eNKoP.">
                                                                                                <field name="value">400</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="ze9zf_gz+6@2KAgNo8Ty-">
                                                                                                <field name="value">365</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_anchor" id="3^[H)LpZM%pvL:8Z[|L2">
                                                                                                <field name="#prop">anchor</field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="l510g-{_SrDJ@1pD1BE7Q">
                                                                                                    <field name="value">0.5</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="{,UxlQJXS)+6k;aI!896">
                                                                                                    <field name="value">0.5</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <statement name="items">
                                                                                      <block type="list_item_shape" id="aG@2WT5/o1I^-]20|f0_@1">
                                                                                        <statement name="#props">
                                                                                          <block type="prop_list_align" id="mjHF~+El7W$8MS8p.bGn">
                                                                                            <field name="#prop"></field>
                                                                                            <field name="align">middle</field>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <statement name="template">
                                                                                          <block type="text_shape" id="VeOOl4TGa_M=r)AE#wv4">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_anchor" id="rH_@2+S:MFmWqXbsg1QMQ">
                                                                                                <field name="#prop">anchor</field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="{/d4P)Br:6yReCqL{0Bs">
                                                                                                    <field name="value">0.5</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id=")2E+qF4cSd_+ucp!h=,;">
                                                                                                    <field name="value">0.5</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_contents" id="f}{tسlGehSMe5OeTY">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="contents">
                                                                                                      <block type="string_value" id="k:=XalXEPSY]@1[UFiaQK">
                                                                                                        <field name="value">Angle 1 + Angle 2 =</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style" id="R`[l7A)gCgOk}hYMp5fd">
                                                                                                        <field name="base">text</field>
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_text_style_font_size" id="sS6@2Ou4J^@1e7;Ym%PK2~">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="fontSize">
                                                                                                              <block type="expression" id="q`+33ijBUe0!;CO5)$da">
                                                                                                                <field name="value">36</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_fill" id="p;v6MgD2@2?0JKX6q:,a$">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="fill">
                                                                                                                  <block type="string_value" id=":x};Qr6u@2~f[L7SW-ZJi">
                                                                                                                    <field name="value">black</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_stroke" id="4aM)!8$.JzlD_AocmM=C">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="stroke">
                                                                                                                      <block type="string_value" id="3:]`:eb{gz@2{i_;nQ|G~">
                                                                                                                        <field name="value">white</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_stroke_thickness" id="[WN(J{t@1dML}uP6a[V#a">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="strokeThickness">
                                                                                                                          <block type="expression" id="^FQyXp[N@1qXs%Tfa^=)V">
                                                                                                                            <field name="value">2</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="list_item_shape" id="htN]Y`/T5$#=GT0W-N41">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_list_align" id="j{qmzT7I%ye3^t~QiFJ_">
                                                                                                <field name="#prop"></field>
                                                                                                <field name="align">middle</field>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <statement name="template">
                                                                                              <block type="image_shape" id="=C5i{{bxmX=kuiVAl{~Q">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_image_key" id="m9KQf%`;Vty:vQBfzPwI">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="key">
                                                                                                      <block type="string_value" id="hFVH+0DHB.wJLX,LGMNR">
                                                                                                        <field name="value">shape.png</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_image_src" id="NN=!Mmg7ha.xdBh^[2|v">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="src">
                                                                                                          <block type="string_value" id="!AEQs^e,^=pcu{U[Xq_4">
                                                                                                            <field name="value">${image_path}shape.png</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_scale" id="mu?q3Q4m6ltlXm[oh`{6">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="scale">
                                                                                                              <block type="expression" id="^@1|wOSfT`P@2_M$Ou!ur1">
                                                                                                                <field name="value">0.7</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="algorithmic_input_shape" id="=cTRb8eNv.@2[KwJ1kQ8U">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_value" id="?;aLztIYx)Dd3jDYJq=)">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id=",9Kr3!{W@1g=$yeB,`j#S">
                                                                                                            <field name="value">angle[0]+angle[1]</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_size" id=")oloX:dXV~MLPu$VnGm+">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="width">
                                                                                                              <block type="expression" id="cSxmPJkHVj@1K{e?4!n+5">
                                                                                                                <field name="value">120@20.7</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="height">
                                                                                                              <block type="expression" id="m.+fg?j+#JmPG$g%]cU~">
                                                                                                                <field name="value">65@20.7</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_input_keyboard" id=".][l_N.ZMw.G=:aFw~!U">
                                                                                                                <field name="#prop"></field>
                                                                                                                <field name="keyboard">numbers1</field>
                                                                                                                <next>
                                                                                                                  <block type="prop_input_max_length" id="!1{z6CQ`0gQu)@2Qm!2!-">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="maxLength">
                                                                                                                      <block type="expression" id="sE+|Gw;Q|iV;Y[$FIYrb">
                                                                                                                        <field name="value">(angle[0]+angle[1]).toString().length</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_input_result_position" id="I#Xl:XtR;[;N6[yPKC?h">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <field name="resultPosition">bottom</field>
                                                                                                                        <next>
                                                                                                                          <block type="prop_tab_order" id="7B=~iVh~RLlJmfpo1`Y:">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="tabOrder">
                                                                                                                              <block type="expression" id="WI#-Kzx|rJgd{)eJYFw7">
                                                                                                                                <field name="value">0</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_stroke" id="M|?.1DpI1@1s88qFnt@1^;">
                                                                                                                                <field name="#prop">stroke</field>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_fill" id="Bl+M_,6TZ3-?G+]wTmQ(">
                                                                                                                                    <field name="#prop">fill</field>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style" id="#GO_Z/Ur`vhjL(2PR[2o">
                                                                                                                                        <field name="base">text</field>
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_text_style_font_size" id="bMv@16heoM~1REiWA]dPs">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="fontSize">
                                                                                                                                              <block type="expression" id="eyX2E)0}:QV)])bS)NzS">
                                                                                                                                                <field name="value">32</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_text_style_fill" id="tP-V9[=pn[:c9pKGYMH6">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="fill">
                                                                                                                                                  <block type="string_value" id="|m@24I~YbBwPVFI%Y?odO">
                                                                                                                                                    <field name="value">black</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_text_style_stroke" id="E6q|JKyB1[#w@1+|@1naxW">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="stroke">
                                                                                                                                                      <block type="string_value" id="+56CJ%b#jXXM06^+-IX~">
                                                                                                                                                        <field name="value">white</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="u[ZF@1eam#+vNGOZ~]X([">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="strokeThickness">
                                                                                                                                                          <block type="expression" id="(!bkQWfl)a{FCaiL1ewc">
                                                                                                                                                            <field name="value">2</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </statement>
                                                            <next>
                                                              <block type="if_then_block" id="=u]%^|$De;n76^lMSZ2X">
                                                                <value name="if">
                                                                  <block type="expression" id="Ivb^LU`=Z/SN+Y=iVv%=">
                                                                    <field name="value">type == 4</field>
                                                                  </block>
                                                                </value>
                                                                <statement name="then">
                                                                  <block type="do_while_block" id="ZqqA@1EXmsN8zR/TXH9y)">
                                                                    <statement name="do">
                                                                      <block type="variable" id="dBCa,l;nu/72B/!R0^V_">
                                                                        <field name="name">a</field>
                                                                        <value name="value">
                                                                          <block type="random_one" id="4u4T~d^NSf-,Yq`0@1|cD">
                                                                            <value name="items">
                                                                              <block type="expression" id="}7wJ],9O:V?st]WBCLbl">
                                                                                <field name="value">list_angle</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </statement>
                                                                    <value name="while">
                                                                      <block type="expression" id="yjCeL1s?NqSG4%O4~^o9">
                                                                        <field name="value">a &gt; 160</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="text_shape" id="lgP%1Onmtz#oBU4ZW1Fi">
                                                                        <statement name="#props">
                                                                          <block type="prop_position" id="4`[6OL#V%TQ-BP+%M%p3">
                                                                            <field name="#prop"></field>
                                                                            <value name="x">
                                                                              <block type="expression" id="j=KLMJATPldQ)J2y4YNE">
                                                                                <field name="value">400</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="y">
                                                                              <block type="expression" id="9-gXi$,IKEI(u0m-qXK1">
                                                                                <field name="value">40</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_anchor" id="egrK,A;$-]OKdLclNdZj">
                                                                                <field name="#prop">anchor</field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="zf~yFfWP9|~8`4I~T:_i">
                                                                                    <field name="value">0.5</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="AUA=!p7rEC]emO!c.7dd">
                                                                                    <field name="value">0.5</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_text_contents" id="DJB0I!`1+;XrsVF.KF70">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="contents">
                                                                                      <block type="string_value" id="Un_GmFNp,0WbynHY=.lU">
                                                                                        <field name="value">Find the unknown angle.</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_text_style" id="zh7DHB,5NS5a=|;A.@1Ao">
                                                                                        <field name="base">text</field>
                                                                                        <statement name="#props">
                                                                                          <block type="prop_text_style_font_size" id="(1jt!]30]aTjVMkaP3v}">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="fontSize">
                                                                                              <block type="expression" id=";LV[F+3O/]4Q.8L^B/R%">
                                                                                                <field name="value">35</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_style_fill" id="7RGYC[hQlN0l1@1v|cMfr">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="fill">
                                                                                                  <block type="string_value" id="5rz0Z-)=eNNr3_@1r`$;O">
                                                                                                    <field name="value">black</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style_stroke" id="{ff$:@2ZVsqu-$i2n_^7]">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="stroke">
                                                                                                      <block type="string_value" id="ojip7jXon6T3eASF@2mG3">
                                                                                                        <field name="value">white</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style_stroke_thickness" id="7+@22UiZ.%;%](zt,^2TE">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="strokeThickness">
                                                                                                          <block type="expression" id="bd5S1Itji+^6L4aJrIv)">
                                                                                                            <field name="value">2</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style_wordwrap" id="PwQPeA+oU9kiqSZoRn^t">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="wordWrap">
                                                                                                              <block type="expression" id="Pf^S5X-yM@2O|w!0Uj">
                                                                                                                <field name="value">true</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_style_wordWrapWidth" id="9~rSQIv9%-kD@2L)a/bFw">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="wordWrapWidth">
                                                                                                                  <block type="expression" id=".`XV6T%$LpAf}z%akC^e">
                                                                                                                    <field name="value">500</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="grid_shape" id="p]W]ob@27[bLoJ1@1T{q|P">
                                                                            <statement name="#props">
                                                                              <block type="prop_grid_dimension" id="YwgA8EQto/W;{F]w[1z_">
                                                                                <field name="#prop"></field>
                                                                                <value name="rows">
                                                                                  <block type="expression" id="=lR;R/!yyms/gNwG51Bi">
                                                                                    <field name="value">1</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="cols">
                                                                                  <block type="expression" id="@2UlvRg{obh89`urU@2{~+">
                                                                                    <field name="value">1</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_position" id="$+S;kHA?N@1wu8smk[7o@2">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="Ytbv$AE(m8en-D`7N_TC">
                                                                                        <field name="value">400</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="_$#I$an-Q8th60C##w{1">
                                                                                        <field name="value">180</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_size" id="KSF_[q@27:OFvH}C^F(2F">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="width">
                                                                                          <block type="expression" id="+;p}F?)I_d68SXt$;G|h">
                                                                                            <field name="value">300</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="height">
                                                                                          <block type="expression" id="pAhErJ@1YoRcSL1VCJPpY">
                                                                                            <field name="value">200</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_anchor" id="ymD(3tJY(|v9F^+%:Ri-">
                                                                                            <field name="#prop">anchor</field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="1yqUMB-;ay-]7Bt[WcVX">
                                                                                                <field name="value">0.5</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id=".RG{]f@1Dk,I~}}]T_-3B">
                                                                                                <field name="value">0.5</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_grid_cell_source" id="YAjKE_@2:iomYh:K:,Jmp">
                                                                                                <field name="#prop">cell.source</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="OrudGM?uN{%@1@2d1==gVj">
                                                                                                    <field name="value">1</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_grid_random" id="T7a./fAmmFZeeD!ELy}!">
                                                                                                    <field name="#prop"></field>
                                                                                                    <field name="random">FALSE</field>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_show_borders" id="WWA:aZvdD5GZ2A0|{_F]">
                                                                                                        <field name="#prop">#showBorders</field>
                                                                                                        <field name="value">FALSE</field>
                                                                                                        <next>
                                                                                                          <block type="prop_grid_cell_template" id="TRr7wEF/1cmNvnT2`hx4">
                                                                                                            <field name="variable">$cell</field>
                                                                                                            <field name="#prop">cell.template</field>
                                                                                                            <field name="#callback">$cell</field>
                                                                                                            <statement name="body">
                                                                                                              <block type="image_shape" id="e!8,qLM(m|%j-m_;[K#`">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_position" id="yn2A$%WVJ}mY4X1`+$:X">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="eQc+@1n]+[LaGa1U|Qs27">
                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="E!q|Lr1gxI6uC:SOQZBg">
                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_size" id="CHUPzU+{GslN-?1`w1tY">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="width">
                                                                                                                          <block type="expression" id="STuDWA!`ui)N?qW/CDf+">
                                                                                                                            <field name="value">240</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="height">
                                                                                                                          <block type="expression" id="/YHF#zz}.~D+}E5];v9V">
                                                                                                                            <field name="value">200</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_key" id="K#EIp+yX]WaU.qX_2cQz">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="key">
                                                                                                                              <block type="string_value" id="2i%}=SR`OgiR%)Ae}cDo">
                                                                                                                                <field name="value">box.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_src" id="E|@2l-dv;]UeCJMoj`S))">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="src">
                                                                                                                                  <block type="string_value" id="tpJQiu`UC@1FKDHl)6HND">
                                                                                                                                    <field name="value">${image_path}box.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="image_shape" id="EMO/z`KLLYGBBefL%}20">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_position" id="{BSTOoJ8x/2/TsZobEAm">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="{G{gcYnHBsZN}[4245y3">
                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="VC,He8[hc!Xsk?y+mq!C">
                                                                                                                            <field name="value">$cell.centerY + 50</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_key" id=";n-,v(K`NL-1CnK$w+0t">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="key">
                                                                                                                              <block type="string_value" id="7E5@2$AU,v+vUtp`z@1N7y">
                                                                                                                                <field name="value">linel.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_src" id="$VW40t3xJXOKjI;LAq:6">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="src">
                                                                                                                                  <block type="string_value" id="_s~bTvTl$IZ6}}_sHI4@2">
                                                                                                                                    <field name="value">${image_path}linel.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_angle" id="jTpCy{5)kXcWWh3gXpDt">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="angle">
                                                                                                                                      <block type="expression" id="k1gpLklky)O?~!.1wOzr">
                                                                                                                                        <field name="value">180 - a</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="image_shape" id="E3Nt_@1Opdp[^dg4t2-#y">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="MJuvpDe?j!.W=!,$F%oo">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="=VYGoAgBWjki/VumFmTO">
                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="jvU;yYoW1ClmbHA|oomh">
                                                                                                                                <field name="value">$cell.centerY + 50</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_key" id="O^q_ZZzv%i+9Sf4KqgSv">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="key">
                                                                                                                                  <block type="string_value" id="BYi6s.jBE|:@1Bjg6f;30">
                                                                                                                                    <field name="value">linel.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_src" id="-yCWQdvc35u3p=[`~N}E">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="src">
                                                                                                                                      <block type="string_value" id="I?-je~#h5iOXQOI7A]Xj">
                                                                                                                                        <field name="value">${image_path}linel.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_angle" id="`q^zPXrG}P{+.7EnhW?,">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="angle">
                                                                                                                                          <block type="expression" id="CgCprDr5G!$4%`$Yaxvb">
                                                                                                                                            <field name="value">0</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="image_shape" id="YK7JP=FCnf5DWK0iv.U.">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="tP=j|D^(W;2p4FcGGOzg">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="o,Im[k_]m?V/ykzGO@2Un">
                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="j8Uj`zif2r%en1{,:tsj">
                                                                                                                                    <field name="value">$cell.centerY + 50</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_key" id="~#lk6zljX$`|L_a%r{Qg">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="key">
                                                                                                                                      <block type="string_value" id="-rW|w@2Qs00Rw5KgTg;tU">
                                                                                                                                        <field name="value">cl.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_src" id="s|Md@1CCn+=K|tDokeMRi">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="src">
                                                                                                                                          <block type="string_value" id="g^$ta,VI6=Z/.pq5BvEy">
                                                                                                                                            <field name="value">${image_path}cl.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_angle" id="~%m7iF!IL@20H2m^)RM%#">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="angle">
                                                                                                                                              <block type="expression" id="^2TYLERUxTZI@2~)pA^Du">
                                                                                                                                                <field name="value">360 - a</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="image_shape" id="kB:N?cKZe`c8Tv0x6.~j">
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_position" id="wrucwdz3@1+kacvnu);g2">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="x">
                                                                                                                                      <block type="expression" id="3zXA`Fvejj.!?zMABrIi">
                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="y">
                                                                                                                                      <block type="expression" id="KqT8gX9{]C?Wg1OWbx@2u">
                                                                                                                                        <field name="value">$cell.centerY + 50</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_key" id="oKKRnKLrlwjMF.=p)QYs">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="key">
                                                                                                                                          <block type="string_value" id="p(K$v3dx=H!49[Dr}E[j">
                                                                                                                                            <field name="value">cl4.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_src" id="nML(+!h9fym8B]uA^^B;">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="src">
                                                                                                                                              <block type="string_value" id="aI2f+V6UpJA6Nsq,xM9r">
                                                                                                                                                <field name="value">${image_path}cl4.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_angle" id="4JR%]R#U/z-Bqzpb6Pj/">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="angle">
                                                                                                                                                  <block type="expression" id="@12Hof[D2pm:Ct,t0fEz5">
                                                                                                                                                    <field name="value">180-a</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <next>
                                                                                                                                  <block type="image_shape" id="NUa0PHxY`9Kq4mL7HNIC">
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_position" id="1Dack+^A2/:zTw[@29k0y">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="expression" id="hUnWfw}xfB=Q0!9=;+y,">
                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id="ZAJc)5y_68gTwN8nZPVG">
                                                                                                                                            <field name="value">$cell.centerY + 50</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_key" id="mfmVC?uc4/9m?@2hx!(n`">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="key">
                                                                                                                                              <block type="string_value" id="e[FJA,8p9n}JHpjHj6Yl">
                                                                                                                                                <field name="value">liner.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_src" id="uCbmS.cHeS-;1P#]r__5">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="src">
                                                                                                                                                  <block type="string_value" id="`;$[5_db8M]VOwU7@2JII">
                                                                                                                                                    <field name="value">${image_path}liner.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="image_shape" id="w#%++LM3p6m~3q�1">
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_position" id="]e@2;KelQm+c9{uTT4}o8">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id="WN#M.@195Ua.@2;}cuB5ey">
                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="zaiamHvqVCD(hu+%+5j%">
                                                                                                                                                <field name="value">$cell.centerY + 50</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_key" id=".N6BJXQ]2Q3!xuD)r(v^">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="key">
                                                                                                                                                  <block type="string_value" id="][Hnc[sjC4!8ZiWmzfQh">
                                                                                                                                                    <field name="value">point.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_image_src" id="vDJjvd|ge0hiHP;]EUqM">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="src">
                                                                                                                                                      <block type="string_value" id="su7`QUgR94+9UNp1V$HX">
                                                                                                                                                        <field name="value">${image_path}point.png</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="grid_shape" id="]IXJZ0?#(6`tMc8J)45e">
                                                                                <statement name="#props">
                                                                                  <block type="prop_grid_dimension" id="gLi:.o8^v,DZq$J:Q,k,">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="rows">
                                                                                      <block type="expression" id="`YdoM!#vEX@1_!5_msd44">
                                                                                        <field name="value">1</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="cols">
                                                                                      <block type="expression" id="XYJd`te?LCMUWtD7^@2y#">
                                                                                        <field name="value">1</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_position" id="PNRqO6-ftG2F(cpu9h)N">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="$+VIOP@2J^Z(Xo+F_.-C+">
                                                                                            <field name="value">180 - a &gt; 35 ? 340:320</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="jc`^xYzo!qN9m(7mD?ZN">
                                                                                            <field name="value">220</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_size" id="9^#+]nTBvLzrGnpco+CF">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="width">
                                                                                              <block type="expression" id="aolY]X`g:I87;cyNKqz.">
                                                                                                <field name="value">180 - a &gt; 99 ? 50:40</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="height">
                                                                                              <block type="expression" id="8??]^A63:HW%$`7su#%w">
                                                                                                <field name="value">30</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_anchor" id="?bU$]{DsdO|MY^PACpU@2">
                                                                                                <field name="#prop">anchor</field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="_OF%X83RWwE^N?x4uXp]">
                                                                                                    <field name="value">0.5</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="Ef@1E4zYNLcvIz9d,ri1k">
                                                                                                    <field name="value">0.5</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_grid_cell_source" id="J8EFo[{06SJD%c,qD2fx">
                                                                                                    <field name="#prop">cell.source</field>
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="y~z8=m@1z+uyRd3M_W!gA">
                                                                                                        <field name="value">1</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_random" id="e:HY^L~MyA;qxx`v_f$]">
                                                                                                        <field name="#prop"></field>
                                                                                                        <field name="random">FALSE</field>
                                                                                                        <next>
                                                                                                          <block type="prop_grid_show_borders" id="Pme@2SLFF}c0q_1=)Ij{5">
                                                                                                            <field name="#prop">#showBorders</field>
                                                                                                            <field name="value">FALSE</field>
                                                                                                            <next>
                                                                                                              <block type="prop_grid_cell_template" id="ZOy.QfqSmQs@1X}Ae0YrH">
                                                                                                                <field name="variable">$cell</field>
                                                                                                                <field name="#prop">cell.template</field>
                                                                                                                <field name="#callback">$cell</field>
                                                                                                                <statement name="body">
                                                                                                                  <block type="text_shape" id="P)n}ej1L7vz%)`U}[bkF">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_contents" id="IIREA@26P%G$lfeEyjxXQ">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="contents">
                                                                                                                              <block type="string_value" id="ma,v.5FwjOA!l+pRo,Kn">
                                                                                                                                <field name="value">${180-a}</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style" id="Cga,9i#g,z@2wCj+[,s)i">
                                                                                                                                <field name="base">text</field>
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_text_style_font_size" id="a:6V[|,`e~:()}qlfMEz">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="fontSize">
                                                                                                                                      <block type="expression" id="E[79|p?X^CXFJxb9-W}$">
                                                                                                                                        <field name="value">32</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_fill" id="WUOMu#k|/-f]yN=w[9hd">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="fill">
                                                                                                                                          <block type="string_value" id="deI$+-6=m5#{0yy)dU,(">
                                                                                                                                            <field name="value">black</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style_stroke" id="opyA8(?h2fN@1.WqKaO8k">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="stroke">
                                                                                                                                              <block type="string_value" id="Y{YlcaJyeo|yDkY10;dh">
                                                                                                                                                <field name="value">white</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_text_style_stroke_thickness" id="T0gJ=n3SXa5E}1p)%L_g">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="strokeThickness">
                                                                                                                                                  <block type="expression" id="ajYuBeJJHJl=76m}vXeE">
                                                                                                                                                    <field name="value">2</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="text_shape" id="XrPl#./emX}f!C4$^#Md">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="s97EV6-eZP5qu9i5WjxR">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="_MLLlz=N@1Z,e.O(sB9^j">
                                                                                                                                <field name="value">$cell.right</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id=".aWts7@21_}yZ-.fbFr}$">
                                                                                                                                <field name="value">$cell.centerY - 15</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_contents" id=":+(XHr%uoqY3IcW7l%t=">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="contents">
                                                                                                                                  <block type="string_value" id="+AiDoA0?aY~MiR+FOsz2">
                                                                                                                                    <field name="value">o</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style" id="?KSBz%t#;#9(:+Hq5R]G">
                                                                                                                                    <field name="base">text</field>
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_text_style_font_size" id="SkH0lPR4|!vQ?u~GmnTb">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="fontSize">
                                                                                                                                          <block type="expression" id="K6-d#@1ei?t4|(dN`0,IF">
                                                                                                                                            <field name="value">24</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style_fill" id="W/U5SdELiP[fhqrUIKN;">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="fill">
                                                                                                                                              <block type="string_value" id="Nn3UY;eCPwB=4E/no/m=">
                                                                                                                                                <field name="value">black</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_text_style_stroke" id="rsR=|fIKy8C1kMLS.OC9">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="stroke">
                                                                                                                                                  <block type="string_value" id="9aK:-6CPQrE:sf@2[k1!$">
                                                                                                                                                    <field name="value">white</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_text_style_stroke_thickness" id="z6,PRHy|qsur82g[N0;O">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="strokeThickness">
                                                                                                                                                      <block type="expression" id="V|e_7K~]^%]S;xr:REFs">
                                                                                                                                                        <field name="value">2</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="image_shape" id="o}K,(PFuy=t$wNgJM%Tt">
                                                                                    <statement name="#props">
                                                                                      <block type="prop_position" id=".EC3=`cJ@15A6Qh9DKg~v">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="$LDc/2;M{{Z.Q/;M:4?D">
                                                                                            <field name="value">400</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id=")?XEGy=dHXV;U~^s{c7i">
                                                                                            <field name="value">350</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_image_key" id="W7?]}AsF,7H(xw2ۧ]">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="key">
                                                                                              <block type="string_value" id="d:|nf@1~)NNy/YF{sQf$h">
                                                                                                <field name="value">shape.png</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_image_src" id="%f+WB9J5}/OkF$:j4%V@2">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="src">
                                                                                                  <block type="string_value" id="OkIh}r}PIdWQta9Ulx|@2">
                                                                                                    <field name="value">${image_path}shape.png</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <next>
                                                                                      <block type="algorithmic_input_shape" id="Tzt^74(2eY]1-!NVbhYD">
                                                                                        <statement name="#props">
                                                                                          <block type="prop_value" id="0%i@2Skgf-4QR[Bjjo]SG">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="7vr+@1TiJ$PD,0}W7/,@1X">
                                                                                                <field name="value">a</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_position" id="@20r,IciW_L]PMuImvI8c">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="m]0=_2h+4N-/zd#jD)h`">
                                                                                                    <field name="value">400</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="aH}ofRgogU6X^k0ThSmd">
                                                                                                    <field name="value">350</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_size" id="/sWt5aZ297Gu;~fduf`]">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="width">
                                                                                                      <block type="expression" id="42q!yh:)50=d(4F6Jn)~">
                                                                                                        <field name="value">125</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="height">
                                                                                                      <block type="expression" id="]@2-d,IHrd7)?~8H_6=5w">
                                                                                                        <field name="value">68</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_input_keyboard" id="9nU{9?TE-QkG-=}8Iz0|">
                                                                                                        <field name="#prop"></field>
                                                                                                        <field name="keyboard">numbers1</field>
                                                                                                        <next>
                                                                                                          <block type="prop_input_max_length" id="{{Lw)6U4/?PBghlWm2=9">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="maxLength">
                                                                                                              <block type="expression" id="unv6:YoXh=9F$J@24[[EJ">
                                                                                                                <field name="value">a.toString().length</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_input_result_position" id="6+e[TT{5Dpwy~g=ou,px">
                                                                                                                <field name="#prop"></field>
                                                                                                                <field name="resultPosition">bottom</field>
                                                                                                                <next>
                                                                                                                  <block type="prop_tab_order" id="?m!!bvU#=dQsrN8N0=~r">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="tabOrder">
                                                                                                                      <block type="expression" id="A8!#IGuQ@1O,p0B`gZpO{">
                                                                                                                        <field name="value">0</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_stroke" id="l];MWNZ,y2@2A}Y;U)Zgk">
                                                                                                                        <field name="#prop">stroke</field>
                                                                                                                        <next>
                                                                                                                          <block type="prop_fill" id="A!|oA[A+q]wSe]-tf[{+">
                                                                                                                            <field name="#prop">fill</field>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style" id="}55#bGDe.$w6Abz=e859">
                                                                                                                                <field name="base">text</field>
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_text_style_font_size" id="MJL%AKhM1@1|:eaLk%nY$">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="fontSize">
                                                                                                                                      <block type="expression" id="B}9J3m2}Ll~i]sck+n5n">
                                                                                                                                        <field name="value">32</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_fill" id="^dHsbxI#p?~~.zpiQ~nr">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="fill">
                                                                                                                                          <block type="string_value" id=")lVeZB?xO[N?ayFtU?N|">
                                                                                                                                            <field name="value">black</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style_stroke" id="fKwnha4lW_EcDa(C31Dp">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="stroke">
                                                                                                                                              <block type="string_value" id="^_#(jDs54D0)x]4)nL;:">
                                                                                                                                                <field name="value">white</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_text_style_stroke_thickness" id="qZ.-[(qB8=:-,][%@2Wyy">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="strokeThickness">
                                                                                                                                                  <block type="expression" id="b?(y}+/O`hV#=}yxWa:t">
                                                                                                                                                    <field name="value">2</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <next>
                                                                  <block type="variable" id="LMjMV${0([g.@1Fj/#?8S">
                                                                    <field name="name">link</field>
                                                                    <value name="value">
                                                                      <block type="expression" id="dyZ+n+h33~WrpEGt[Kr[">
                                                                        <field name="value">'http://starmathsonline.s3.amazonaws.com/' + image_path</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="if_then_block" id="NVuLX{v+C{mYQn@10k#{~">
                                                                        <value name="if">
                                                                          <block type="expression" id="F`B4$?|!L[S;(hOrwiLt">
                                                                            <field name="value">type == 1</field>
                                                                          </block>
                                                                        </value>
                                                                        <statement name="then">
                                                                          <block type="partial_explanation" id=")YS)Vp`zbRfn}#WFpgNv" inline="true">
                                                                            <value name="value">
                                                                              <block type="string_value" id="f}}D05Jk4CcehRD^.UzG">
                                                                                <field name="value">&lt;u&gt;Step 1: First, look at all 3 angles.&lt;/u&gt;&lt;/br&gt;
  &lt;/br&gt;
Remember this, a straight line is 180°, Look at each angle, now calculate each angle.&lt;/br&gt;&lt;/br&gt;
&lt;style&gt;
      #angle1 {
              -ms-transform: rotate(${180-angle[0]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(180-angle[0]}deg);
              /@2 Safari @2/
              transform: rotate(${180-angle[0]}deg);
              /@2 Standard syntax @2/
          }
          #angle2 {
              -ms-transform: rotate(${180-angle[1]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${180-angle[1]}deg);
              /@2 Safari @2/
              transform: rotate(${180-angle[1]}deg);
              /@2 Standard syntax @2/
          }
          #angle3 {
              -ms-transform: rotate(${180-angle[2]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${180-angle[2]}deg);
              /@2 Safari @2/
              transform: rotate(${180-angle[2]}deg);
              /@2 Standard syntax @2/
          }

          #anglec1 {
              -ms-transform: rotate(${360-angle[0]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${360-angle[0]}deg);
              /@2 Safari @2/
              transform: rotate(${360-angle[0]}deg);
              /@2 Standard syntax @2/
          }
          #anglec2 {
              -ms-transform: rotate(${360-angle[1]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${360-angle[1]}deg);
              /@2 Safari @2/
              transform: rotate(${360-angle[1]}deg);
              /@2 Standard syntax @2/
          }
          #anglec3 {
              -ms-transform: rotate(${360-angle[2]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${360-angle[2]}deg);
              /@2 Safari @2/
              transform: rotate(${360-angle[2]}deg);
              /@2 Standard syntax @2/
          }
      &lt;/style&gt;


      &lt;div style='position: relative; width: 260px; height: 280px; display: inline-block;'&gt;
        &lt;div style='position: absolute; width: 300px; height: 250px; left: 10px; top: 10px;'&gt;
          &lt;img style='position: absolute; min-width: 240px; max-height: 200px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/box.png'/&gt;
          &lt;img id='anglec1' style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/cl.png'/&gt;
          &lt;img style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/liner.png'/&gt;
          &lt;img id='angle1' style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/linel.png'/&gt;
          &lt;img style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/point.png'/&gt;
        &lt;/div&gt;
&lt;span style='position: absolute; left: 100px; top: 200px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'&gt;${angle[0]}&amp;deg;&lt;/span&gt;
      &lt;/div&gt;
&lt;div style='width: 30px; display: inline-block'&gt;&lt;/div&gt;
      &lt;div style='position: relative; width: 260px; height: 280px; display: inline-block;'&gt;
        &lt;div style='position: absolute; width: 300px; height: 250px; left: 10px; top: 10px;'&gt;
          &lt;img style='position: absolute; min-width: 240px; max-height: 200px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/box.png'/&gt;
          &lt;img id='anglec2' style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/cl.png'/&gt;
          &lt;img style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/liner.png'/&gt;
          &lt;img id='angle2' style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/linel.png'/&gt;
          &lt;img style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/point.png'/&gt;
        &lt;/div&gt;
&lt;span style='position: absolute; left: 100px; top: 200px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'&gt;${angle[1]}&amp;deg;&lt;/span&gt;
      &lt;/div&gt;
&lt;div style='width: 30px; display: inline-block'&gt;&lt;/div&gt;
      &lt;div style='position: relative; width: 260px; height: 280px; display: inline-block;'&gt;
        &lt;div style='position: absolute; width: 300px; height: 250px; left: 10px; top: 10px;'&gt;
          &lt;img style='position: absolute; min-width: 240px; max-height: 200px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/box.png'/&gt;
          &lt;img id='anglec3' style='position: absolute; top: 30px; left: ${10 - Ox(angle[2])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/cl.png'/&gt;
          &lt;img style='position: absolute; top: 30px; left: ${10 - Ox(angle[2])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/liner.png'/&gt;
          &lt;img id='angle3' style='position: absolute; top: 30px; left: ${10 - Ox(angle[2])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/linel.png'/&gt;
          &lt;img style='position: absolute; top: 30px; left: ${10 - Ox(angle[2])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/point.png'/&gt;
        &lt;/div&gt;
&lt;span style='position: absolute; left: 100px; top: 200px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'&gt;${angle[2]}&amp;deg;&lt;/span&gt;
      &lt;/div&gt;
                                                                                </field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="end_partial_explanation" id="?e[48pyD2?Ue)na9Txf=">
                                                                                <next>
                                                                                  <block type="partial_explanation" id="([d$9r=R)5k}UxBfT?u6" inline="true">
                                                                                    <value name="value">
                                                                                      <block type="string_value" id="xPp1U2:,C+FyNV6F6T)m">
                                                                                        <field name="value">&lt;u&gt;Step 2: Let's find our final answer.&lt;/u&gt;&lt;/br&gt;
  &lt;/br&gt;
So which angle is ${a}&amp;deg;.
&lt;/br&gt;&lt;/br&gt;
&lt;style&gt;
      #angle1 {
              -ms-transform: rotate(${180-angle[0]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(180-angle[0]}deg);
              /@2 Safari @2/
              transform: rotate(${180-angle[0]}deg);
              /@2 Standard syntax @2/
          }
          #angle2 {
              -ms-transform: rotate(${180-angle[1]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${180-angle[1]}deg);
              /@2 Safari @2/
              transform: rotate(${180-angle[1]}deg);
              /@2 Standard syntax @2/
          }
          #angle3 {
              -ms-transform: rotate(${180-angle[2]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${180-angle[2]}deg);
              /@2 Safari @2/
              transform: rotate(${180-angle[2]}deg);
              /@2 Standard syntax @2/
          }

          #anglec1 {
              -ms-transform: rotate(${360-angle[0]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${360-angle[0]}deg);
              /@2 Safari @2/
              transform: rotate(${360-angle[0]}deg);
              /@2 Standard syntax @2/
          }
          #anglec2 {
              -ms-transform: rotate(${360-angle[1]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${360-angle[1]}deg);
              /@2 Safari @2/
              transform: rotate(${360-angle[1]}deg);
              /@2 Standard syntax @2/
          }
          #anglec3 {
              -ms-transform: rotate(${360-angle[2]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${360-angle[2]}deg);
              /@2 Safari @2/
              transform: rotate(${360-angle[2]}deg);
              /@2 Standard syntax @2/
          }
      &lt;/style&gt;

      &lt;div style='position: relative; width: 260px; height: 280px; display: inline-block; border: ${angle[0] == a ? "3px solid green": ""}'&gt;
        &lt;div style='position: absolute; width: 300px; height: 250px; left: 10px; top: 10px;'&gt;
          &lt;img style='position: absolute; min-width: 240px; max-height: 200px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/box.png'/&gt;
          &lt;img id='anglec1' style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/cl.png'/&gt;
          &lt;img style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/liner.png'/&gt;
          &lt;img id='angle1' style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/linel.png'/&gt;
          &lt;img style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/point.png'/&gt;
        &lt;/div&gt;
&lt;span style='position: absolute; left: 100px; top: 200px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'&gt;${angle[0]}&amp;deg;&lt;/span&gt;
      &lt;/div&gt;
&lt;div style='width: 30px; display: inline-block'&gt;&lt;/div&gt;
      &lt;div style='position: relative; width: 260px; height: 280px; display: inline-block; border: ${angle[1] == a ? "3px solid green": ""}'&gt;
        &lt;div style='position: absolute; width: 300px; height: 250px; left: 10px; top: 10px;'&gt;
          &lt;img style='position: absolute; min-width: 240px; max-height: 200px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/box.png'/&gt;
          &lt;img id='anglec2' style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/cl.png'/&gt;
          &lt;img style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/liner.png'/&gt;
          &lt;img id='angle2' style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/linel.png'/&gt;
          &lt;img style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/point.png'/&gt;
        &lt;/div&gt;
&lt;span style='position: absolute; left: 100px; top: 200px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'&gt;${angle[1]}&amp;deg;&lt;/span&gt;
      &lt;/div&gt;
&lt;div style='width: 30px; display: inline-block'&gt;&lt;/div&gt;
      &lt;div style='position: relative; width: 260px; height: 280px; display: inline-block; border: ${angle[2] == a ? "3px solid green": ""}'&gt;
        &lt;div style='position: absolute; width: 300px; height: 250px; left: 10px; top: 10px;'&gt;
          &lt;img style='position: absolute; min-width: 240px; max-height: 200px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/box.png'/&gt;
          &lt;img id='anglec3' style='position: absolute; top: 30px; left: ${10 - Ox(angle[2])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/cl.png'/&gt;
          &lt;img style='position: absolute; top: 30px; left: ${10 - Ox(angle[2])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/liner.png'/&gt;
          &lt;img id='angle3' style='position: absolute; top: 30px; left: ${10 - Ox(angle[2])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/linel.png'/&gt;
          &lt;img style='position: absolute; top: 30px; left: ${10 - Ox(angle[2])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/point.png'/&gt;
        &lt;/div&gt;
&lt;span style='position: absolute; left: 100px; top: 200px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'&gt;${angle[2]}&amp;deg;&lt;/span&gt;
      &lt;/div&gt;
                                                                                        </field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="end_partial_explanation" id="2czYJo~!]53D/Nygd|Am"></block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="if_then_block" id="tZ,PG}wRTy5TVO=Zh_p[">
                                                                            <value name="if">
                                                                              <block type="expression" id="BnJM=9`]!F}nv[@1E!0yi">
                                                                                <field name="value">type == 2</field>
                                                                              </block>
                                                                            </value>
                                                                            <statement name="then">
                                                                              <block type="partial_explanation" id="3[KU@2dsE[|m@2C=v_.y=A" inline="true">
                                                                                <value name="value">
                                                                                  <block type="string_value" id=";p:0E8)0xZ+zdZ`m6em+">
                                                                                    <field name="value">Place the midpoint of the protractor on the VERTEX of the angle.
&lt;/br&gt;
At the same time, line up one side of the angle with the zero line of the protractor 
&lt;/br&gt;&lt;/br&gt;&lt;/br&gt;
&lt;style&gt;
                    #angle1 {
              -ms-transform: rotate(${180-a}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${180-a}deg);
              /@2 Safari @2/
              transform: rotate(${180-a}deg);
              /@2 Standard syntax @2/
          }
          #anglec1 {
              -ms-transform: rotate(${360-a}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${360-a}deg);
              /@2 Safari @2/
              transform: rotate(${360-a}deg);
              /@2 Standard syntax @2/
          }
          &lt;/style&gt;

        &lt;div style='position: relative; width: 260px; height: 280px; display: inline-block;'&gt;
            &lt;div style='position: absolute; width: 600px; height: 250px; left: 300px; top: 10px;'&gt;
              &lt;img style='position: absolute; min-width: 240px; max-height: 200px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/box.png'/&gt;
              &lt;img id='anglec1' style='position: absolute; top: 30px; left: ${10 - Ox(a)}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/cl.png'/&gt;
              &lt;img style='position: absolute; top: 30px; left: ${10 - Ox(a)}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/liner.png'/&gt;
              &lt;img id='angle1' style='position: absolute; top: 30px; left: ${10 - Ox(a)}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/linel.png'/&gt;
              &lt;img style='position: absolute; top: 30px; left: ${10 - Ox(a)}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/point.png'/&gt;
            
              &lt;img style='position: absolute; max-width: 350px; left: ${- 65 - Ox(a)}px; top: -45px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/pro.png'/&gt;
            
            &lt;/div&gt;
&lt;/div&gt;
&lt;/br&gt;
  &lt;span style='background: rgb(250, 250, 250, 0.5)'&gt;
Read the degrees where the other side crosses the number scale.
&lt;/br&gt;
It says &lt;b&gt;${a}&amp;deg;&lt;/b&gt;!&lt;/span&gt;
                                                                                    </field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="if_then_block" id="iGcYiPKQPoG)MwsuK1BP">
                                                                                <value name="if">
                                                                                  <block type="expression" id="KWR@21WyJEKDpE9[rZU_`">
                                                                                    <field name="value">type == 3</field>
                                                                                  </block>
                                                                                </value>
                                                                                <statement name="then">
                                                                                  <block type="partial_explanation" id="uD7]s}a?BVobX-3?gFCU" inline="true">
                                                                                    <value name="value">
                                                                                      <block type="string_value" id="Sw$Vtz)OQ_Ksds0{_?r#">
                                                                                        <field name="value">&lt;u&gt;Step 1:&lt;/u&gt;&lt;/br&gt;
Place the midpoint of the protractor on the VERTEX of the angle.&lt;/br&gt;
At the same time, line up one side of the angle with the zero line of the protractor 
&lt;/br&gt;&lt;/br&gt;&lt;/br&gt;
&lt;style&gt;
                    #angle1 {
              -ms-transform: rotate(${180-angle[0]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${180-angle[0]}deg);
              /@2 Safari @2/
              transform: rotate(${180-angle[0]}deg);
              /@2 Standard syntax @2/
          }
          #anglec1 {
              -ms-transform: rotate(${360-angle[0]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${360-angle[0]}deg);
              /@2 Safari @2/
              transform: rotate(${360-angle[0]}deg);
              /@2 Standard syntax @2/
          }
          
          #angle2 {
              -ms-transform: rotate(${180-angle[1]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${180-angle[1]}deg);
              /@2 Safari @2/
              transform: rotate(${180-angle[1]}deg);
              /@2 Standard syntax @2/
          }
          #anglec2 {
              -ms-transform: rotate(${360-angle[1]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${360-angle[1]}deg);
              /@2 Safari @2/
              transform: rotate(${360-angle[1]}deg);
              /@2 Standard syntax @2/
          }
          &lt;/style&gt;

        &lt;div style='position: relative; width: 260px; height: 280px; display: inline-block;'&gt;
            &lt;div style='position: absolute; width: 600px; height: 250px; left: 300px; top: 10px;'&gt;
              &lt;img style='position: absolute; min-width: 240px; max-height: 200px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/box.png'/&gt;
              &lt;img id='anglec1' style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/cl.png'/&gt;
              &lt;img style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/liner.png'/&gt;
              &lt;img id='angle1' style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/linel.png'/&gt;
              &lt;img style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/point.png'/&gt;
            
              &lt;img style='position: absolute; max-width: 350px; left: ${- 65 - Ox(angle[0])}px; top: -45px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/pro.png'/&gt;
            &lt;span style='position: absolute; left: 300px; top: 100px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'&gt;${angle[0]}&amp;deg;&lt;/span&gt;
            &lt;/div&gt;
&lt;/div&gt;
&lt;/br&gt;&lt;div style='position: relative; width: 260px; height: 280px; display: inline-block;'&gt;
            &lt;div style='position: absolute; width: 600px; height: 250px; left: 300px; top: 10px;'&gt;
              &lt;img style='position: absolute; min-width: 240px; max-height: 200px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/box.png'/&gt;
                &lt;img id='anglec2' style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/cl.png'/&gt;
              &lt;img style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/liner.png'/&gt;
              &lt;img id='angle2' style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/linel.png'/&gt;
              &lt;img style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/point.png'/&gt;
            
              &lt;img style='position: absolute; max-width: 350px; left: ${- 65 - Ox(angle[1])}px; top: -45px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/pro.png'/&gt;
            &lt;span style='position: absolute; left: 300px; top: 100px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'&gt;${angle[1]}&amp;deg;&lt;/span&gt;
            &lt;/div&gt;
&lt;/div&gt;
                                                                                        </field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="end_partial_explanation" id="Wfo$ib/LK1fPz8Vnt%5R">
                                                                                        <next>
                                                                                          <block type="partial_explanation" id="Yd[em48T.I;G@1US/~Zj4" inline="true">
                                                                                            <value name="value">
                                                                                              <block type="string_value" id="]4/P^UPjH4W:]anjw6/i">
                                                                                                <field name="value">&lt;u&gt;Step 2: Read the degrees where the other side crosses the number scale.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;
&lt;span style='background: rgb(250, 250, 250, 0.4)'&gt;
For angle 1&lt;/br&gt;
It says &lt;b&gt;${angle[0]}&amp;deg;&lt;/b&gt;&lt;/br&gt;&lt;/br&gt;

For angle 2&lt;/br&gt;
It says &lt;b&gt;${angle[1]}&amp;deg;&lt;/b&gt;&lt;/br&gt;&lt;/br&gt;

Angle 1 + angle 2 = &lt;b&gt;${angle[0] + angle[1]}&amp;deg;&lt;/b&gt;&lt;/span&gt;
                                                                                                </field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="if_then_block" id="wyS2:awM#B1{cG`9@1n2v">
                                                                                    <value name="if">
                                                                                      <block type="expression" id="ggh:@1^S1Tb4_qe/b3uW/">
                                                                                        <field name="value">type == 4</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <statement name="then">
                                                                                      <block type="partial_explanation" id="/FYy+]ub:eB@1h1?1=?po" inline="true">
                                                                                        <value name="value">
                                                                                          <block type="string_value" id="xDd{X0@23Gx^vC0Fs~L5_">
                                                                                            <field name="value">&lt;u&gt;Remember the size of the straight line.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

It's 180°!&lt;/br&gt;&lt;/br&gt;

Now, using subtraction, let's subtract 180° by all the known angles.&lt;/br&gt;&lt;br&gt;

180&amp;deg; - ${180 - a}&amp;deg; = &lt;b&gt;${a}&amp;deg;&lt;/b&gt;&lt;/br&gt;&lt;/br&gt;

So the angle is &lt;b&gt;${a}&amp;deg;&lt;/b&gt;!
                                                                                            </field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </statement>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
  <block type="variable" id="mVYxcq+P2d$$Xx+mExXW" x="611" y="2955">
    <field name="name">text</field>
    <value name="value">
      <block type="expression" id="Q)`6B3s$]RG3|j]Mw4%m">
        <field name="value">['a', 'b', 'c']</field>
      </block>
    </value>
  </block>
</xml>
END_XML]] */