
module.exports = [
  {
    "#type": "question",
    "name": "Y6.MG.AN.FA360.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y6.MG.AN.FA360.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "range < 0"
        },
        "then": [
          {
            "#type": "variable",
            "name": "list_angle",
            "value": {
              "#type": "expression",
              "value": "[30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330]"
            }
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "range < 4"
            },
            "then": [
              {
                "#type": "variable",
                "name": "list_angle",
                "value": {
                  "#type": "expression",
                  "value": "[20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 260, 270, 280, 290, 300, 310, 320, 330, 340, 350]"
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "list_angle",
                "value": {
                  "#type": "expression",
                  "value": "[20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150, 155, 160, 165, 170, 175, 180, 185, 190, 195, 200, 205, 210, 215, 220, 225, 230, 235, 240, 245, 250, 255, 260, 265, 270, 275, 280, 285, 290, 295, 300, 305, 310, 315, 320, 325, 330, 335, 340, 345, 350, 355]"
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "statement",
        "value": "function Ox(a){\n  if(a < 60){return 50;}\n  else if(a < 110) {return 30;}\n  else {return 5;}\n}"
      },
      {
        "#type": "variable",
        "name": "type",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "3"
          }
        }
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "variable",
            "name": "angle",
            "value": {
              "#type": "random_many",
              "count": {
                "#type": "expression",
                "value": "3"
              },
              "items": {
                "#type": "expression",
                "value": "list_angle"
              }
            }
          },
          {
            "#type": "variable",
            "name": "a",
            "value": {
              "#type": "random_one",
              "items": {
                "#type": "expression",
                "value": "angle"
              }
            }
          },
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "550"
                },
                "y": {
                  "#type": "expression",
                  "value": "50"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "Which angle is ${a}°?"
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "35"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    },
                    {
                      "#type": "prop_text_style_wordwrap",
                      "#prop": "",
                      "wordWrap": {
                        "#type": "expression",
                        "value": "true"
                      }
                    },
                    {
                      "#type": "prop_text_style_wordWrapWidth",
                      "#prop": "",
                      "wordWrapWidth": {
                        "#type": "expression",
                        "value": "500"
                      }
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "3"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "310"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "780"
                },
                "height": {
                  "#type": "expression",
                  "value": "230"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "angle"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "240"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "230"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "box.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}box.png"
                      }
                    ]
                  },
                  {
                    "#type": "drop_shape",
                    "multiple": false,
                    "resultMode": "none",
                    "groupName": "",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "0"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "0"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "if_then_else_block",
                    "if": {
                      "#type": "expression",
                      "value": "$cell.data < 180"
                    },
                    "then": [
                      {
                        "#type": "if_then_else_block",
                        "if": {
                          "#type": "expression",
                          "value": "$cell.data == 90"
                        },
                        "then": [
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "sq.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}sq.png"
                              }
                            ]
                          }
                        ],
                        "else": [
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "cl.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}cl.png"
                              },
                              {
                                "#type": "prop_angle",
                                "#prop": "",
                                "angle": {
                                  "#type": "expression",
                                  "value": "$cell.data"
                                }
                              }
                            ]
                          }
                        ]
                      },
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "liner.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}liner.png"
                          }
                        ]
                      },
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "linel.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}linel.png"
                          },
                          {
                            "#type": "prop_angle",
                            "#prop": "",
                            "angle": {
                              "#type": "expression",
                              "value": "$cell.data - 180"
                            }
                          }
                        ]
                      },
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "point.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}point.png"
                          }
                        ]
                      }
                    ],
                    "else": [
                      {
                        "#type": "if_then_else_block",
                        "if": {
                          "#type": "expression",
                          "value": "$cell.data < 270"
                        },
                        "then": [
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "cl.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}cl.png"
                              },
                              {
                                "#type": "prop_angle",
                                "#prop": "",
                                "angle": {
                                  "#type": "expression",
                                  "value": "$cell.data"
                                }
                              }
                            ]
                          },
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "liner270.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}liner270.png"
                              }
                            ]
                          },
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "linel270.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}linel270.png"
                              },
                              {
                                "#type": "prop_angle",
                                "#prop": "",
                                "angle": {
                                  "#type": "expression",
                                  "value": "$cell.data - 180"
                                }
                              }
                            ]
                          },
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "point.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}point.png"
                              }
                            ]
                          }
                        ],
                        "else": [
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "cl.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}cl.png"
                              },
                              {
                                "#type": "prop_angle",
                                "#prop": "",
                                "angle": {
                                  "#type": "expression",
                                  "value": "$cell.data"
                                }
                              }
                            ]
                          },
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "liner360.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}liner360.png"
                              }
                            ]
                          },
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "linel270.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}linel270.png"
                              },
                              {
                                "#type": "prop_angle",
                                "#prop": "",
                                "angle": {
                                  "#type": "expression",
                                  "value": "$cell.data - 180"
                                }
                              }
                            ]
                          },
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "show.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}show.png"
                              }
                            ]
                          },
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "point.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}point.png"
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "3"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "310"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "780"
                },
                "height": {
                  "#type": "expression",
                  "value": "230"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "angle"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "choice_custom_shape",
                    "action": "click-one",
                    "value": {
                      "#type": "expression",
                      "value": "$cell.data"
                    },
                    "template": {
                      "#callback": "$choice",
                      "variable": "$choice",
                      "body": [
                        {
                          "#type": "image_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "$cell.centerX"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "$cell.centerY"
                              }
                            },
                            {
                              "#type": "prop_size",
                              "#prop": "",
                              "width": {
                                "#type": "expression",
                                "value": "240"
                              },
                              "height": {
                                "#type": "expression",
                                "value": "230"
                              }
                            },
                            {
                              "#type": "prop_image_key",
                              "#prop": "",
                              "key": "hide.png"
                            },
                            {
                              "#type": "prop_image_src",
                              "#prop": "",
                              "src": "${image_path}hide.png"
                            }
                          ]
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "choice_custom_shape",
            "action": "drag",
            "value": {
              "#type": "expression",
              "value": "1"
            },
            "template": {
              "#callback": "$choice",
              "variable": "$choice",
              "body": [
                {
                  "#type": "image_shape",
                  "#props": [
                    {
                      "#type": "prop_position",
                      "#prop": "",
                      "x": {
                        "#type": "expression",
                        "value": "280"
                      },
                      "y": {
                        "#type": "expression",
                        "value": "0"
                      }
                    },
                    {
                      "#type": "prop_anchor",
                      "#prop": "anchor",
                      "x": {
                        "#type": "expression",
                        "value": "0.5"
                      },
                      "y": {
                        "#type": "expression",
                        "value": "0"
                      }
                    },
                    {
                      "#type": "prop_max_size",
                      "#prop": "",
                      "maxWidth": {
                        "#type": "expression",
                        "value": "250"
                      },
                      "maxHeight": {
                        "#type": "expression",
                        "value": "0"
                      }
                    },
                    {
                      "#type": "prop_image_key",
                      "#prop": "",
                      "key": "pro.png"
                    },
                    {
                      "#type": "prop_image_src",
                      "#prop": "",
                      "src": "${image_path}pro.png"
                    }
                  ]
                }
              ]
            }
          },
          {
            "#type": "func",
            "name": "addAnswers",
            "args": [
              {
                "#type": "expression",
                "value": "a"
              }
            ]
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 2"
        },
        "then": [
          {
            "#type": "variable",
            "name": "a",
            "value": {
              "#type": "random_one",
              "items": {
                "#type": "expression",
                "value": "list_angle"
              }
            }
          },
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "40"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "What is the size of the angle?"
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "35"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    },
                    {
                      "#type": "prop_text_style_wordwrap",
                      "#prop": "",
                      "wordWrap": {
                        "#type": "expression",
                        "value": "true"
                      }
                    },
                    {
                      "#type": "prop_text_style_wordWrapWidth",
                      "#prop": "",
                      "wordWrapWidth": {
                        "#type": "expression",
                        "value": "500"
                      }
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "250"
                },
                "y": {
                  "#type": "expression",
                  "value": "180"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "300"
                },
                "height": {
                  "#type": "expression",
                  "value": "200"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "240"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "230"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "box.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}box.png"
                      }
                    ]
                  },
                  {
                    "#type": "drop_shape",
                    "multiple": false,
                    "resultMode": "none",
                    "groupName": "",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "0"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "0"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "if_then_else_block",
                    "if": {
                      "#type": "expression",
                      "value": "a < 180"
                    },
                    "then": [
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "cl.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}cl.png"
                          },
                          {
                            "#type": "prop_angle",
                            "#prop": "",
                            "angle": {
                              "#type": "expression",
                              "value": "a"
                            }
                          }
                        ]
                      },
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "liner.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}liner.png"
                          }
                        ]
                      },
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "linel.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}linel.png"
                          },
                          {
                            "#type": "prop_angle",
                            "#prop": "",
                            "angle": {
                              "#type": "expression",
                              "value": "a - 180"
                            }
                          }
                        ]
                      },
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "point.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}point.png"
                          }
                        ]
                      }
                    ],
                    "else": [
                      {
                        "#type": "if_then_else_block",
                        "if": {
                          "#type": "expression",
                          "value": "a < 270"
                        },
                        "then": [
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "cl.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}cl.png"
                              },
                              {
                                "#type": "prop_angle",
                                "#prop": "",
                                "angle": {
                                  "#type": "expression",
                                  "value": "a"
                                }
                              }
                            ]
                          },
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "liner270.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}liner270.png"
                              }
                            ]
                          },
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "linel270.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}linel270.png"
                              },
                              {
                                "#type": "prop_angle",
                                "#prop": "",
                                "angle": {
                                  "#type": "expression",
                                  "value": "a - 180"
                                }
                              }
                            ]
                          },
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "point.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}point.png"
                              }
                            ]
                          }
                        ],
                        "else": [
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "cl.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}cl.png"
                              },
                              {
                                "#type": "prop_angle",
                                "#prop": "",
                                "angle": {
                                  "#type": "expression",
                                  "value": "a"
                                }
                              }
                            ]
                          },
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "liner360.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}liner360.png"
                              }
                            ]
                          },
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "linel270.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}linel270.png"
                              },
                              {
                                "#type": "prop_angle",
                                "#prop": "",
                                "angle": {
                                  "#type": "expression",
                                  "value": "a - 180"
                                }
                              }
                            ]
                          },
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "show.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}show.png"
                              }
                            ]
                          },
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "point.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}point.png"
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "choice_custom_shape",
            "action": "drag",
            "value": {
              "#type": "expression",
              "value": "1"
            },
            "template": {
              "#callback": "$choice",
              "variable": "$choice",
              "body": [
                {
                  "#type": "image_shape",
                  "#props": [
                    {
                      "#type": "prop_position",
                      "#prop": "",
                      "x": {
                        "#type": "expression",
                        "value": "600"
                      },
                      "y": {
                        "#type": "expression",
                        "value": "365"
                      }
                    },
                    {
                      "#type": "prop_anchor",
                      "#prop": "anchor",
                      "x": {
                        "#type": "expression",
                        "value": "0.5"
                      },
                      "y": {
                        "#type": "expression",
                        "value": "1"
                      }
                    },
                    {
                      "#type": "prop_max_size",
                      "#prop": "",
                      "maxWidth": {
                        "#type": "expression",
                        "value": "300"
                      },
                      "maxHeight": {
                        "#type": "expression",
                        "value": "0"
                      }
                    },
                    {
                      "#type": "prop_image_key",
                      "#prop": "",
                      "key": "pro.png"
                    },
                    {
                      "#type": "prop_image_src",
                      "#prop": "",
                      "src": "${image_path}pro.png"
                    }
                  ]
                }
              ]
            }
          },
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "250"
                },
                "y": {
                  "#type": "expression",
                  "value": "350"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "shape.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}shape.png"
              }
            ]
          },
          {
            "#type": "choice_input_shape",
            "#props": [
              {
                "#type": "prop_value",
                "#prop": "",
                "value": {
                  "#type": "expression",
                  "value": "a"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "250"
                },
                "y": {
                  "#type": "expression",
                  "value": "350"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "125"
                },
                "height": {
                  "#type": "expression",
                  "value": "68"
                }
              },
              {
                "#type": "prop_input_keyboard",
                "#prop": "",
                "keyboard": "numbers1"
              },
              {
                "#type": "prop_input_max_length",
                "#prop": "",
                "maxLength": {
                  "#type": "expression",
                  "value": "a.toString().length"
                }
              },
              {
                "#type": "prop_input_result_position",
                "#prop": "",
                "resultPosition": "bottom"
              },
              {
                "#type": "prop_tab_order",
                "#prop": "",
                "tabOrder": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_stroke",
                "#prop": "stroke"
              },
              {
                "#type": "prop_fill",
                "#prop": "fill"
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "32"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ],
            "#init": "algorithmic_input"
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 3"
        },
        "then": [
          {
            "#type": "do_while_block",
            "do": [
              {
                "#type": "variable",
                "name": "a",
                "value": {
                  "#type": "random_one",
                  "items": {
                    "#type": "expression",
                    "value": "list_angle"
                  }
                }
              }
            ],
            "while": {
              "#type": "expression",
              "value": "a < 185"
            }
          },
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "40"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "Measure with each reflex angles to the nearest ${range < 0 ? 30 : range < 4 ? 10 : 5}°."
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "35"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "250"
                },
                "y": {
                  "#type": "expression",
                  "value": "180"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "300"
                },
                "height": {
                  "#type": "expression",
                  "value": "200"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "240"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "230"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "box.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}box.png"
                      }
                    ]
                  },
                  {
                    "#type": "drop_shape",
                    "multiple": false,
                    "resultMode": "none",
                    "groupName": "",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "0"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "0"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "if_then_else_block",
                    "if": {
                      "#type": "expression",
                      "value": "a < 180"
                    },
                    "then": [
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "cl.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}cl.png"
                          },
                          {
                            "#type": "prop_angle",
                            "#prop": "",
                            "angle": {
                              "#type": "expression",
                              "value": "a"
                            }
                          }
                        ]
                      },
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "liner.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}liner.png"
                          }
                        ]
                      },
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "linel.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}linel.png"
                          },
                          {
                            "#type": "prop_angle",
                            "#prop": "",
                            "angle": {
                              "#type": "expression",
                              "value": "a - 180"
                            }
                          }
                        ]
                      },
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "point.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}point.png"
                          }
                        ]
                      }
                    ],
                    "else": [
                      {
                        "#type": "if_then_else_block",
                        "if": {
                          "#type": "expression",
                          "value": "a < 270"
                        },
                        "then": [
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "cl.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}cl.png"
                              },
                              {
                                "#type": "prop_angle",
                                "#prop": "",
                                "angle": {
                                  "#type": "expression",
                                  "value": "a"
                                }
                              }
                            ]
                          },
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "liner270.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}liner270.png"
                              }
                            ]
                          },
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "linel270.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}linel270.png"
                              },
                              {
                                "#type": "prop_angle",
                                "#prop": "",
                                "angle": {
                                  "#type": "expression",
                                  "value": "a - 180"
                                }
                              }
                            ]
                          },
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "point.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}point.png"
                              }
                            ]
                          }
                        ],
                        "else": [
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "cl.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}cl.png"
                              },
                              {
                                "#type": "prop_angle",
                                "#prop": "",
                                "angle": {
                                  "#type": "expression",
                                  "value": "a"
                                }
                              }
                            ]
                          },
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "liner360.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}liner360.png"
                              }
                            ]
                          },
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "linel270.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}linel270.png"
                              },
                              {
                                "#type": "prop_angle",
                                "#prop": "",
                                "angle": {
                                  "#type": "expression",
                                  "value": "a - 180"
                                }
                              }
                            ]
                          },
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "show.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}show.png"
                              }
                            ]
                          },
                          {
                            "#type": "image_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_image_key",
                                "#prop": "",
                                "key": "point.png"
                              },
                              {
                                "#type": "prop_image_src",
                                "#prop": "",
                                "src": "${image_path}point.png"
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "choice_custom_shape",
            "action": "drag",
            "value": {
              "#type": "expression",
              "value": "1"
            },
            "template": {
              "#callback": "$choice",
              "variable": "$choice",
              "body": [
                {
                  "#type": "image_shape",
                  "#props": [
                    {
                      "#type": "prop_position",
                      "#prop": "",
                      "x": {
                        "#type": "expression",
                        "value": "600"
                      },
                      "y": {
                        "#type": "expression",
                        "value": "365"
                      }
                    },
                    {
                      "#type": "prop_anchor",
                      "#prop": "anchor",
                      "x": {
                        "#type": "expression",
                        "value": "0.5"
                      },
                      "y": {
                        "#type": "expression",
                        "value": "1"
                      }
                    },
                    {
                      "#type": "prop_max_size",
                      "#prop": "",
                      "maxWidth": {
                        "#type": "expression",
                        "value": "300"
                      },
                      "maxHeight": {
                        "#type": "expression",
                        "value": "0"
                      }
                    },
                    {
                      "#type": "prop_image_key",
                      "#prop": "",
                      "key": "pro.png"
                    },
                    {
                      "#type": "prop_image_src",
                      "#prop": "",
                      "src": "${image_path}pro.png"
                    }
                  ]
                }
              ]
            }
          },
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "250"
                },
                "y": {
                  "#type": "expression",
                  "value": "350"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "shape.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}shape.png"
              }
            ]
          },
          {
            "#type": "choice_input_shape",
            "#props": [
              {
                "#type": "prop_value",
                "#prop": "",
                "value": {
                  "#type": "expression",
                  "value": "a"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "250"
                },
                "y": {
                  "#type": "expression",
                  "value": "350"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "125"
                },
                "height": {
                  "#type": "expression",
                  "value": "68"
                }
              },
              {
                "#type": "prop_input_keyboard",
                "#prop": "",
                "keyboard": "numbers1"
              },
              {
                "#type": "prop_input_max_length",
                "#prop": "",
                "maxLength": {
                  "#type": "expression",
                  "value": "a.toString().length"
                }
              },
              {
                "#type": "prop_input_result_position",
                "#prop": "",
                "resultPosition": "bottom"
              },
              {
                "#type": "prop_tab_order",
                "#prop": "",
                "tabOrder": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_stroke",
                "#prop": "stroke"
              },
              {
                "#type": "prop_fill",
                "#prop": "fill"
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "32"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ],
            "#init": "algorithmic_input"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "link",
        "value": {
          "#type": "expression",
          "value": "'http://starmathsonline.s3.amazonaws.com/' + image_path"
        }
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 1: First, look at all 3 angles.</u></br>\n  </br>\nRemember this, a full angle is 360°. Look at each angle, now calculate each angle.</br></br>\n<style>\n      #angle1 {\n              -ms-transform: rotate(${angle[0] - 180}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(angle[0] - 180}deg);\n              /* Safari */\n              transform: rotate(${angle[0] - 180}deg);\n              /* Standard syntax */\n          }\n          #angle2 {\n              -ms-transform: rotate(${angle[1] - 180}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(angle[1] - 180}deg);\n              /* Safari */\n              transform: rotate(${angle[1] - 180}deg);\n              /* Standard syntax */\n          }\n          #angle3 {\n              -ms-transform: rotate(${angle[2] - 180}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${angle[2] - 180}deg);\n              /* Safari */\n              transform: rotate(${angle[2] - 180}deg);\n              /* Standard syntax */\n          }\n\n          #anglec1 {\n              -ms-transform: rotate(${angle[0]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${angle[0]}deg);\n              /* Safari */\n              transform: rotate(${angle[0]}deg);\n              /* Standard syntax */\n          }\n          #anglec2 {\n              -ms-transform: rotate(${angle[1]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${angle[1]}deg);\n              /* Safari */\n              transform: rotate(${angle[1]}deg);\n              /* Standard syntax */\n          }\n          #anglec3 {\n              -ms-transform: rotate(${angle[2]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${angle[2]}deg);\n              /* Safari */\n              transform: rotate(${angle[2]}deg);\n              /* Standard syntax */\n          }\n      </style>\n\n\n      <div style='position: relative; width: 260px; height: 280px; display: inline-block;'>\n              <div style='position: absolute; width: 300px; height: 300px; left: 10px; top: 10px;'>\n                <img style='position: absolute; min-width: 240px; max-height: 240px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/box.png'/>\n                <img id='anglec1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/cl.png'/>\n  ${angle[0] < 180 ? \"<img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner.png'/><img id='angle1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel.png'/>\" : angle[0] < 270 ? \"<img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner270.png'/><img id='angle1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/>\" : \"<img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner360.png'/><img id='angle1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/><img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/show.png'/>\"}\n                <img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/point.png'/>\n              </div>\n      <span style='position: absolute; left: 80px; top: 270px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'>${angle[0]}&deg;</span>\n            </div>\n<div style='width: 30px; display: inline-block'></div>\n      <div style='position: relative; width: 260px; height: 280px; display: inline-block;'>\n              <div style='position: absolute; width: 300px; height: 300px; left: 10px; top: 10px;'>\n                <img style='position: absolute; min-width: 240px; max-height: 240px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/box.png'/>\n                <img id='anglec2' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/cl.png'/>\n${angle[1] < 180 ? \"<img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner.png'/><img id='angle2' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel.png'/>\" : angle[1] < 270 ? \"<img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner270.png'/><img id='angle2' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/>\" : \"<img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner360.png'/><img id='angle2' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/><img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/show.png'/>\"}\n  <img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/point.png'/>\n              </div>\n      <span style='position: absolute; left: 80px; top: 270px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'>${angle[1]}&deg;</span>\n            </div>\n<div style='width: 30px; display: inline-block'></div>\n      <div style='position: relative; width: 260px; height: 280px; display: inline-block;'>\n              <div style='position: absolute; width: 300px; height: 300px; left: 10px; top: 10px;'>\n                <img style='position: absolute; min-width: 240px; max-height: 240px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/box.png'/>\n                <img id='anglec3' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/cl.png'/>\n                 ${angle[2] < 180 ? \"<img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner.png'/><img id='angle3' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel.png'/>\" : angle[2] < 270 ? \"<img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner270.png'/><img id='angle3' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/>\" : \"<img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner360.png'/><img id='angle3' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/><img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/show.png'/>\"}\n  <img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/point.png'/>\n              </div>\n      <span style='position: absolute; left: 80px; top: 270px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'>${angle[2]}&deg;</span>\n            </div>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 2: Let's find our final answer.</u></br>\n  </br>\nSo which angle is ${a}&deg;\n</br></br>\n<style>\n      #angle1 {\n              -ms-transform: rotate(${angle[0] - 180}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(angle[0] - 180}deg);\n              /* Safari */\n              transform: rotate(${angle[0] - 180}deg);\n              /* Standard syntax */\n          }\n          #angle2 {\n              -ms-transform: rotate(${angle[1] - 180}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(angle[1] - 180}deg);\n              /* Safari */\n              transform: rotate(${angle[1] - 180}deg);\n              /* Standard syntax */\n          }\n          #angle3 {\n              -ms-transform: rotate(${angle[2] - 180}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${angle[2] - 180}deg);\n              /* Safari */\n              transform: rotate(${angle[2] - 180}deg);\n              /* Standard syntax */\n          }\n\n          #anglec1 {\n              -ms-transform: rotate(${angle[0]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${angle[0]}deg);\n              /* Safari */\n              transform: rotate(${angle[0]}deg);\n              /* Standard syntax */\n          }\n          #anglec2 {\n              -ms-transform: rotate(${angle[1]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${angle[1]}deg);\n              /* Safari */\n              transform: rotate(${angle[1]}deg);\n              /* Standard syntax */\n          }\n          #anglec3 {\n              -ms-transform: rotate(${angle[2]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${angle[2]}deg);\n              /* Safari */\n              transform: rotate(${angle[2]}deg);\n              /* Standard syntax */\n          }\n      </style>\n\n<div style='position: relative; width: 260px; height: 280px; display: inline-block; border: ${angle[0] == a ? \"3px solid green\": \"\"}'>\n    <div style='position: absolute; width: 300px; height: 300px; left: 10px; top: 10px;'>\n        <img style='position: absolute; min-width: 240px; max-height: 240px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/box.png'/>\n        <img id='anglec1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/cl.png'/>\n        ${angle[0] < 180 ? \"<img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner.png'/><img id='angle1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel.png'/>\" : angle[0] < 270 ? \"<img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner270.png'/><img id='angle1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/>\" : \"<img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner360.png'/><img id='angle1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/><img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/show.png'/>\"}\n        <img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/point.png'/>\n    </div>\n    <span style='position: absolute; left: 80px; top: 270px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'>${angle[0]}&deg;</span>\n</div>\n\n<div style='width: 30px; display: inline-block'></div>\n\n<div style='position: relative; width: 260px; height: 280px; display: inline-block;  border: ${angle[1] == a ? \"3px solid green\": \"\"}'>\n    <div style='position: absolute; width: 300px; height: 300px; left: 10px; top: 10px;'>\n        <img style='position: absolute; min-width: 240px; max-height: 240px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/box.png'/>\n        <img id='anglec2' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/cl.png'/>\n        ${angle[1] < 180 ? \"<img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner.png'/><img id='angle2' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel.png'/>\" : angle[1] < 270 ? \"<img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner270.png'/><img id='angle2' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/>\" : \"<img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner360.png'/><img id='angle2' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/><img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/show.png'/>\"}\n        <img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/point.png'/>\n    </div>\n    <span style='position: absolute; left: 80px; top: 270px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'>${angle[1]}&deg;</span>\n</div>\n\n<div style='width: 30px; display: inline-block'></div>\n\n<div style='position: relative; width: 260px; height: 280px; display: inline-block; border: ${angle[2] == a ? \"3px solid green\": \"\"}'>\n    <div style='position: absolute; width: 300px; height: 300px; left: 10px; top: 10px;'>\n        <img style='position: absolute; min-width: 240px; max-height: 240px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/box.png'/>\n        <img id='anglec3' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/cl.png'/>\n        ${angle[2] < 180 ? \"<img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner.png'/><img id='angle3' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel.png'/>\" : angle[2] < 270 ? \"<img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner270.png'/><img id='angle3' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/>\" : \"<img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner360.png'/><img id='angle3' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/><img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/show.png'/>\"}\n        <img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/point.png'/>\n    </div>\n    <span style='position: absolute; left: 80px; top: 270px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'>${angle[2]}&deg;</span>\n</div>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 2"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "Place the midpoint of the protractor on the VERTEX of the angle.\n</br>\nAt the same time, line up one side of the angle with the zero line of the protractor.\n</br></br></br>\n<style>\n                    #angle1 {\n              -ms-transform: rotate(${a-180}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${a-180}deg);\n              /* Safari */\n              transform: rotate(${a-180}deg);\n              /* Standard syntax */\n          }\n          #anglec1 {\n              -ms-transform: rotate(${a}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${a}deg);\n              /* Safari */\n              transform: rotate(${a}deg);\n              /* Standard syntax */\n          }\n          </style>\n\n        <div style='position: relative; width: 260px; height: 280px; display: inline-block;'>\n            <div style='position: absolute; width: 600px; height: 250px; left: 300px; top: 10px;'>\n              <img style='position: absolute; min-width: 240px; max-height: 240px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/box.png'/>\n                <img id='anglec1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/cl.png'/>\n  ${a < 180 ? \"<img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner.png'/><img id='angle1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel.png'/>\" : a < 270 ? \"<img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner270.png'/><img id='angle1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/>\" : \"<img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner360.png'/><img id='angle1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/><img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/show.png'/>\"}\n                <img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/point.png'/>\n              \n              <img style='position: absolute; max-width: 350px; left: -55px; top: -45px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/pro.png'/>\n            \n            </div>\n</div>\n</br></br>\n  <span style='background: rgb(250, 250, 250, 0.5)'>\nRead the degrees where the other side crosses the number scale.\n</br>\nIt says <b>${a}&deg;</b>!</span>"
            ]
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 3"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "Place the midpoint of the protractor on the VERTEX of the angle.\n</br>\nAt the same time, line up one side of the angle with the zero line of the protractor.\n</br></br></br>\n<style>\n                    #angle1 {\n              -ms-transform: rotate(${a-180}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${a-180}deg);\n              /* Safari */\n              transform: rotate(${a-180}deg);\n              /* Standard syntax */\n          }\n          #anglec1 {\n              -ms-transform: rotate(${a}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${a}deg);\n              /* Safari */\n              transform: rotate(${a}deg);\n              /* Standard syntax */\n          }\n          </style>\n\n        <div style='position: relative; width: 260px; height: 280px; display: inline-block;'>\n            <div style='position: absolute; width: 600px; height: 250px; left: 300px; top: 10px;'>\n              <img style='position: absolute; min-width: 240px; max-height: 240px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/box.png'/>\n                <img id='anglec1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/cl.png'/>\n  ${a < 180 ? \"<img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner.png'/><img id='angle1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel.png'/>\" : a < 270 ? \"<img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner270.png'/><img id='angle1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/>\" : \"<img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner360.png'/><img id='angle1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/><img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/show.png'/>\"}\n                <img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/point.png'/>\n              \n              <img style='position: absolute; max-width: 350px; left: -55px; top: -45px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/pro.png'/>\n            \n            </div>\n</div>\n</br></br>\n  <span style='background: rgb(250, 250, 250, 0.5)'>\nRead the degrees where the other side crosses the number scale.\n</br>\nIt says <b>${a}&deg;</b>!</span>"
            ]
          }
        ]
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="0" y="0">
    <field name="name">Y6.MG.AN.FA360.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y6.MG.AN.FA360.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="YepMQT@2uwp2rRPvmnRg1">
                                    <field name="name">range</field>
                                    <value name="value">
                                      <block type="expression" id="UxgQ^Y5]rb}vpw[D=l-!">
                                        <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="if_then_else_block" id="0RWXxs$%M.7n6%X)PNK,">
                                        <value name="if">
                                          <block type="expression" id="2+XBg|,YRIr$@1g^-|a9J">
                                            <field name="value">range &lt; 0</field>
                                          </block>
                                        </value>
                                        <statement name="then">
                                          <block type="variable" id="9jlrMG+60[uqUgwS)]Jo">
                                            <field name="name">list_angle</field>
                                            <value name="value">
                                              <block type="expression" id="xZ$3C7saOpO94-)ipnwi">
                                                <field name="value">[30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330]</field>
                                              </block>
                                            </value>
                                          </block>
                                        </statement>
                                        <statement name="else">
                                          <block type="if_then_else_block" id="DuGp+Hdnw#P,vf5Wyso.">
                                            <value name="if">
                                              <block type="expression" id="RA[sz+[GcrCg%(gEZymC">
                                                <field name="value">range &lt; 4</field>
                                              </block>
                                            </value>
                                            <statement name="then">
                                              <block type="variable" id="U_|!Y;AL.%qUs~5E@2f2}">
                                                <field name="name">list_angle</field>
                                                <value name="value">
                                                  <block type="expression" id="9lnkt~d;Py3ki;JBY~XU">
                                                    <field name="value">[20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 200, 210, 220, 230, 240, 250, 260, 270, 280, 290, 300, 310, 320, 330, 340, 350]</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </statement>
                                            <statement name="else">
                                              <block type="variable" id="?GOo_U)4uJ`bb5NDcl#8">
                                                <field name="name">list_angle</field>
                                                <value name="value">
                                                  <block type="expression" id="m@2Cw%}eGYrsWmRZz/5x6">
                                                    <field name="value">[20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100, 105, 110, 115, 120, 125, 130, 135, 140, 145, 150, 155, 160, 165, 170, 175, 180, 185, 190, 195, 200, 205, 210, 215, 220, 225, 230, 235, 240, 245, 250, 255, 260, 265, 270, 275, 280, 285, 290, 295, 300, 305, 310, 315, 320, 325, 330, 335, 340, 345, 350, 355]</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </statement>
                                          </block>
                                        </statement>
                                        <next>
                                          <block type="statement" id=";t^zW4|7-TsF2z{@1EMLf">
                                            <field name="value">function Ox(a){
  if(a &lt; 60){return 50;}
  else if(a &lt; 110) {return 30;}
  else {return 5;}
}
                                            </field>
                                            <next>
                                              <block type="variable" id="6yjfR{5BB@1usX_HtxTVL">
                                                <field name="name">type</field>
                                                <value name="value">
                                                  <block type="random_number" id="=^jXDv{3%rDSd;C0AD+?">
                                                    <value name="min">
                                                      <block type="expression" id="P=7x3+2%.#6~?Yc2e5{,">
                                                        <field name="value">1</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="S[S9]8gTY[j@1`?|4E08V">
                                                        <field name="value">3</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="if_then_block" id="w#P0D;{N1ST+@2DBj{?$0">
                                                    <value name="if">
                                                      <block type="expression" id="tJJXCU}%Sqd,bZaJxa)n">
                                                        <field name="value">type == 1</field>
                                                      </block>
                                                    </value>
                                                    <statement name="then">
                                                      <block type="variable" id="bE7g++F=3L4v95TQHFRJ">
                                                        <field name="name">angle</field>
                                                        <value name="value">
                                                          <block type="random_many" id="Lkc{F8k_F%YayI7Ob|hm">
                                                            <value name="count">
                                                              <block type="expression" id="[2}%=`D1tv75LDt@1s4hJ">
                                                                <field name="value">3</field>
                                                              </block>
                                                            </value>
                                                            <value name="items">
                                                              <block type="expression" id="U=8[)9Q;6@2c::yGw^m]#">
                                                                <field name="value">list_angle</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="M]P,k5l;iLL`jl=S,Jw~">
                                                            <field name="name">a</field>
                                                            <value name="value">
                                                              <block type="random_one" id="RTGw_I@2Pl,@1}=R@2TV5bL">
                                                                <value name="items">
                                                                  <block type="expression" id="!%HvmDl!u}mdnI`HY#F_">
                                                                    <field name="value">angle</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="text_shape" id="BMZ`VXgeL9SKSUPypqR}">
                                                                <statement name="#props">
                                                                  <block type="prop_position" id="2ZCJ!{{ewoqJ:U`qy/a3">
                                                                    <field name="#prop"></field>
                                                                    <value name="x">
                                                                      <block type="expression" id="@2}VwZjF@1@26jx%[t9s+S{">
                                                                        <field name="value">550</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="y">
                                                                      <block type="expression" id="v9Xp5o$8e(vwo?-5ulv)">
                                                                        <field name="value">50</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_anchor" id="/mlO?LRGl]Y6:miMCcVl">
                                                                        <field name="#prop">anchor</field>
                                                                        <value name="x">
                                                                          <block type="expression" id="(AL@10~x$mYek3xG#s8!R">
                                                                            <field name="value">0.5</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id="9@1zx[V:?]#@2^~.nF{K1i">
                                                                            <field name="value">0</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_text_contents" id="a/_O-[.Q43~MHRwq0s9L">
                                                                            <field name="#prop"></field>
                                                                            <value name="contents">
                                                                              <block type="string_value" id="sq!]aL5[V31FJEKfV=kt">
                                                                                <field name="value">Which angle is ${a}°?</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_text_style" id="a(3JA7@1Mb2H0sM##7f$V">
                                                                                <field name="base">text</field>
                                                                                <statement name="#props">
                                                                                  <block type="prop_text_style_font_size" id="a#|@2`]s}o!b.Ip9+i?S4">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="fontSize">
                                                                                      <block type="expression" id="rWx}E+P{y@2ktY_%^5d:j">
                                                                                        <field name="value">35</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_text_style_fill" id="!yxwytpitVg|ZpaK8C!U">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="fill">
                                                                                          <block type="string_value" id="EL(Zzz`XUBi;_$4Fo~?.">
                                                                                            <field name="value">black</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="stroke">
                                                                                              <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                                <field name="value">white</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="strokeThickness">
                                                                                                  <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                                    <field name="value">2</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style_wordwrap" id="eg?/EQ2A@2O.N-04S:c9,">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="wordWrap">
                                                                                                      <block type="expression" id="0pCQ`1e+.:~f$32g`8vA">
                                                                                                        <field name="value">true</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style_wordWrapWidth" id="M2hlIxxcA_1AZq|^dgOv">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="wordWrapWidth">
                                                                                                          <block type="expression" id="!9]`O^T#$X.CD~mZ:@2@1-">
                                                                                                            <field name="value">500</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <next>
                                                                  <block type="grid_shape" id="6tU?K^N8ec~A@1@1NwQ72N">
                                                                    <statement name="#props">
                                                                      <block type="prop_grid_dimension" id="X4K:|9dKK%s9l4n5v@2lG">
                                                                        <field name="#prop"></field>
                                                                        <value name="rows">
                                                                          <block type="expression" id="[Ev!.bxnzjc|-33v.%7=">
                                                                            <field name="value">1</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="cols">
                                                                          <block type="expression" id="u$7]/V/!ZWM4(Va??@2({">
                                                                            <field name="value">3</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_position" id="zLld84TfBg#!88]1,JD-">
                                                                            <field name="#prop"></field>
                                                                            <value name="x">
                                                                              <block type="expression" id="Cd6g{Y_P?:GyVyvQi1wJ">
                                                                                <field name="value">400</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="y">
                                                                              <block type="expression" id=".F}E71+@1i)77!yQ`hmfu">
                                                                                <field name="value">310</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_size" id="+o2CWr^.ohx)_bcIG53]">
                                                                                <field name="#prop"></field>
                                                                                <value name="width">
                                                                                  <block type="expression" id="Z[x5iHG%KWC%KWQb}c-G">
                                                                                    <field name="value">780</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="height">
                                                                                  <block type="expression" id="@2E|Ob@1jF#gYtX8-%Iui@1">
                                                                                    <field name="value">230</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_anchor" id="1WJjXY^.h,t]eNgxaJ}q">
                                                                                    <field name="#prop">anchor</field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="DHx[vqs?.?nUD:AYPJWd">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="e3D|RR7w!=Sufu):zV`c">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_grid_cell_source" id="v8+Yu211Qg8fX9Mu4th`">
                                                                                        <field name="#prop">cell.source</field>
                                                                                        <value name="value">
                                                                                          <block type="expression" id="~j@1,^liddf}oDg}E91^i">
                                                                                            <field name="value">angle</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_grid_random" id="}?kS#x[b)9OGb@1`UdJGV">
                                                                                            <field name="#prop"></field>
                                                                                            <field name="random">FALSE</field>
                                                                                            <next>
                                                                                              <block type="prop_grid_show_borders" id=";y^wk4y%[VWjXOj~vrQZ">
                                                                                                <field name="#prop">#showBorders</field>
                                                                                                <field name="value">FALSE</field>
                                                                                                <next>
                                                                                                  <block type="prop_grid_cell_template" id="7^6]Kv:2yrZi`cooAW2@2">
                                                                                                    <field name="variable">$cell</field>
                                                                                                    <field name="#prop">cell.template</field>
                                                                                                    <field name="#callback">$cell</field>
                                                                                                    <statement name="body">
                                                                                                      <block type="image_shape" id="_!Cx8#$]MEnT@2^i=1YF)">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_position" id="Kx8.bX/Dy0CHt[q,P#~q">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="oP|b862Z0as]dyadpxNi">
                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="iu//FyxWyr$F%x^27KnQ">
                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_size" id="(}-}^^$FmX{I{^lWjiqa">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="width">
                                                                                                                  <block type="expression" id="hrIIdJ#l{K-jyF};c$19">
                                                                                                                    <field name="value">240</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="height">
                                                                                                                  <block type="expression" id="!fe725))@2cfw.74BB@1P;">
                                                                                                                    <field name="value">230</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_image_key" id="H!Eh#nrj0l8:3yMEYNKh">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="key">
                                                                                                                      <block type="string_value" id="nj9%K)!)tvQ)?/=}-!-:">
                                                                                                                        <field name="value">box.png</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_src" id="WntF@2o?MDNxRJD`?I)/i">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="src">
                                                                                                                          <block type="string_value" id="w6T=Cn$!;_)9pmJeVX]-">
                                                                                                                            <field name="value">${image_path}box.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="drop_shape" id="H;bkiGmxrF`ax;iMs=QD">
                                                                                                            <field name="multiple">FALSE</field>
                                                                                                            <field name="resultMode">none</field>
                                                                                                            <field name="groupName"></field>
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_position" id="@1.NzY!(02.|hP#4uey!U">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="wx-ECtsSYyd,yIcJpGgr">
                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="x@1:%VI#f#`Qm_]N8mZk%">
                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_size" id="ZY|Jk~UzkNrhu.QxOhzn">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="width">
                                                                                                                      <block type="expression" id=":dN~zh-)i,(j0NvQE;">
                                                                                                                        <field name="value">0</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="height">
                                                                                                                      <block type="expression" id="R6~VuGl-`GyWQ@20dZyGc">
                                                                                                                        <field name="value">0</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="if_then_else_block" id="L5+%Xjl7]O~+=2-/]hZf">
                                                                                                                <value name="if">
                                                                                                                  <block type="expression" id="`N^)3/@1F1SyuwpiRKl.:">
                                                                                                                    <field name="value">$cell.data &lt; 180</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <statement name="then">
                                                                                                                  <block type="if_then_else_block" id="XC$1g@18b7ONK{F{X2loj">
                                                                                                                    <value name="if">
                                                                                                                      <block type="expression" id="oQFwzwo2D;+c?CON=pWH">
                                                                                                                        <field name="value">$cell.data == 90</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <statement name="then">
                                                                                                                      <block type="image_shape" id="(^`N,lKpoJI[DKN5E?fR">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="$NKhS!jz_YUsOazNwF:m">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="^qIBD4UAKRt3uyCa@1M)2">
                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="@2Q+9C|WzvSugr=IW.It!">
                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_key" id=";76DR7U3P|R:nv%z@1">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="key">
                                                                                                                                  <block type="string_value" id="7,J]dcp!/N=k`egU6%.=">
                                                                                                                                    <field name="value">sq.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_src" id="DQ/syGE4S{eSPrR_]qbo">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="src">
                                                                                                                                      <block type="string_value" id="6((b3K5@1u4@2K6}.DgH{r">
                                                                                                                                        <field name="value">${image_path}sq.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <statement name="else">
                                                                                                                      <block type="image_shape" id="R~c#G@2o-cE[zDQ]2u~/j">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="?Z9=NM!QBrtdEU)rDlHC">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="^gQf?g%Y+4gi8^B}|x_4">
                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="DZ`)FjJba?$.:(Q!S+o:">
                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_key" id="Ut#)FKn!~?Oj$r-q2Wj0">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="key">
                                                                                                                                  <block type="string_value" id="qoXy4A2/0dDHa!Hc@1xFa">
                                                                                                                                    <field name="value">cl.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_src" id="5e^IP+}U~MV3!2)|$7l9">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="src">
                                                                                                                                      <block type="string_value" id="tBN8,}8qQ?MebRu4/7_6">
                                                                                                                                        <field name="value">${image_path}cl.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_angle" id="@2PlAh$9ssc+5[7E4+vlR">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="angle">
                                                                                                                                          <block type="expression" id="R,D/qfnOffwU_}ex@1euR">
                                                                                                                                            <field name="value">$cell.data</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="image_shape" id="{Ba2ypy=uDVvj:YOWabP">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="gy8=[Gjv)RQ7H:Q:F~Hu">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="MQHla1GBg$e@2gtjaQWv,">
                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="`a.{bi_z.jR]jO6hw7dW">
                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_key" id="#o/YLFSHz0/REba,3eeO">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="key">
                                                                                                                                  <block type="string_value" id="kW[I5k|_BL`KpCC+/l7G">
                                                                                                                                    <field name="value">liner.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_src" id="noDr6@1|MuTeaNQr36Nvh">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="src">
                                                                                                                                      <block type="string_value" id=",|@1d/r^matCX)`[-MY@2l">
                                                                                                                                        <field name="value">${image_path}liner.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="image_shape" id="Nw|98#[REYjsCnpmw][0">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="mjUZY}fA.:1ihydR@1@2cD">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="OwOirD%MX:KD_O,6N3=P">
                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="cJ|Gqy{VodD8w`_QBx`O">
                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_key" id="BGx[j%lbrS8e+r}-yej]">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="key">
                                                                                                                                      <block type="string_value" id="F@1ET;S%h{cQOHHKQXvm%">
                                                                                                                                        <field name="value">linel.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_src" id="P2yIL@2x^d%6RRmh^x#9y">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="src">
                                                                                                                                          <block type="string_value" id="ec],n@2tHLLta0i.}W^-9">
                                                                                                                                            <field name="value">${image_path}linel.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_angle" id=")k/uvtQNg){@1NxfEV9(1">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="angle">
                                                                                                                                              <block type="expression" id="$aVBTFW~M.?zN(XJg/1r">
                                                                                                                                                <field name="value">$cell.data - 180</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="image_shape" id="D[UA}TR@12ttbioohuTx;">
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_position" id="rSpmopD^0CAdi/26.k$;">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="x">
                                                                                                                                      <block type="expression" id="-I^YUiLz+^YyLHMrO`2|">
                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="y">
                                                                                                                                      <block type="expression" id="H@2?[0fT@1O46h[[r,9Fw-">
                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_key" id="_{(}MkF}@2$aI{+{TyZAo">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="key">
                                                                                                                                          <block type="string_value" id="B]6d{=_el`C?g#BO@1%:z">
                                                                                                                                            <field name="value">point.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_src" id="h$Q/Y()3]Fl0D/K!FIU$">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="src">
                                                                                                                                              <block type="string_value" id="mE;$~g3-zUuEjy[d?!l+">
                                                                                                                                                <field name="value">${image_path}point.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <statement name="else">
                                                                                                                  <block type="if_then_else_block" id="_KI0iu.4A@1`TaFfU[42j">
                                                                                                                    <value name="if">
                                                                                                                      <block type="expression" id="i=/{7QD6WJUS;t@2J){F0">
                                                                                                                        <field name="value">$cell.data &lt; 270</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <statement name="then">
                                                                                                                      <block type="image_shape" id="-k}Xj%NeNX;nw@11)uWTM">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="0SIo=Tl+mE#6PXxCs[T)">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="~`d@2Q@2ISe0+DgK+K+mO4">
                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="^VgTx{@2K@22X.6g/Y7/j}">
                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_key" id="+jg?l@1@1($[{4f/V%qG![">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="key">
                                                                                                                                  <block type="string_value" id="Q[_LKJe)D8Mv^/l|@1@1Nj">
                                                                                                                                    <field name="value">cl.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_src" id="A]gc%luj(xt}lV~g}I4n">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="src">
                                                                                                                                      <block type="string_value" id="qEi1_(A{Yu62SmRp^OY]">
                                                                                                                                        <field name="value">${image_path}cl.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_angle" id="Gb,U1JfdXOyvYC+_5(;8">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="angle">
                                                                                                                                          <block type="expression" id="L%6=W2R?jGbolj=yJRs0">
                                                                                                                                            <field name="value">$cell.data</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="image_shape" id="y?~qiOYHt6IDZ.l,NMG2">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="3g;-|nx}MjG#$C3=Pc%4">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="sllYojRWY)ob=sEs]PIl">
                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="5$-Y1#@15/(vUxyrrUHfu">
                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_key" id="4c6q^X?Fx$7e3,^Bnc`I">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="key">
                                                                                                                                      <block type="string_value" id="uD.z9_I2y}yEKRtY3|gS">
                                                                                                                                        <field name="value">liner270.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_src" id="[]RNFav8KX+/x9)%M{kt">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="src">
                                                                                                                                          <block type="string_value" id="uL`Oz?.2s,e@2@1$a7jN5g">
                                                                                                                                            <field name="value">${image_path}liner270.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="image_shape" id="p7Dj59,hq/C1C]a}|U2]">
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_position" id=".:A!P?uVA7}LyEW@1bR@2a">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="x">
                                                                                                                                      <block type="expression" id="c|dsZ.E5E3#5,NMi9hUF">
                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="y">
                                                                                                                                      <block type="expression" id="NZEiSpj2sp.@1A4]13Y}~">
                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_key" id="w~H[9IV8-Q{wzOrgNU$#">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="key">
                                                                                                                                          <block type="string_value" id="X:kpC.~{qIPY-=K^@20xv">
                                                                                                                                            <field name="value">linel270.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_src" id="m#-}^/Pr1g-2I4lO#QP$">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="src">
                                                                                                                                              <block type="string_value" id="6#g:q.z4_ad;-BG-]v!Q">
                                                                                                                                                <field name="value">${image_path}linel270.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_angle" id="3S)=o-Sr{@20$Tr9mzg%[">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="angle">
                                                                                                                                                  <block type="expression" id="]eIY}HI@18|HI0B~d-d[.">
                                                                                                                                                    <field name="value">$cell.data - 180</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <next>
                                                                                                                                  <block type="image_shape" id="f}/gUQgi9#sZIjijK;4U">
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_position" id="E1C.Y0rw,=O;92?tG0ZX">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="expression" id="}#`7~]8:33i4fhNQ6c!C">
                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id="p@1uE5}Lvq|GaT|B@1+p9]">
                                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_key" id="}nyy@22OYaYI,E6x`hh[=">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="key">
                                                                                                                                              <block type="string_value" id="V]L~BeJ;qP;D@2+C}=toy">
                                                                                                                                                <field name="value">point.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_src" id=";(PX5(+S8Xqty/Y+L?1h">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="src">
                                                                                                                                                  <block type="string_value" id="|x`pS@1q~xO=,wiNOL{uh">
                                                                                                                                                    <field name="value">${image_path}point.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <statement name="else">
                                                                                                                      <block type="image_shape" id="$`,,(h7.xbEJ`Xf5P$eq">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="]#)/!Y8zlPj^phI/Ks7S">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="?Xg|R:}SMu?od:JdEclj">
                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="rFjvKIKfRLCPf01pk4:l">
                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_key" id="@1F6RRy793i#b5IIMtoV8">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="key">
                                                                                                                                  <block type="string_value" id=",_|q{_[Yy.~q.M`L3bqn">
                                                                                                                                    <field name="value">cl.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_src" id="U2D.ph$WN7cFA62i.Eg+">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="src">
                                                                                                                                      <block type="string_value" id="wAik/CueEGv+Ij.7op/=">
                                                                                                                                        <field name="value">${image_path}cl.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_angle" id="^w@2%iPPs$pV}5@1TUsQP:">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="angle">
                                                                                                                                          <block type="expression" id="Fo2ZvXucfF~ES6^Jkt,r">
                                                                                                                                            <field name="value">$cell.data</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="image_shape" id="U@1[|@25]%h(O/p:[Nd@1gs">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="fJ=:eJSP_S@2nmsfyQ#Al">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="+YCs2VuKAMs=eNIx!=`J">
                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="@2-M!AG6R[x=4ob-/,rra">
                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_key" id="do|pj)Kyjli24}d)7?wP">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="key">
                                                                                                                                      <block type="string_value" id="9q{71dS7xj)L$?%P.g/D">
                                                                                                                                        <field name="value">liner360.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_src" id="{+#o#.EX-B#L^T,W@2((=">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="src">
                                                                                                                                          <block type="string_value" id="m9pv,.Dka:69qy?|tZlp">
                                                                                                                                            <field name="value">${image_path}liner360.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="image_shape" id="2=y@2qBkVNZc#QzV0EmCa">
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_position" id="=k+)BN2^~L`m`l2c]RXv">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="x">
                                                                                                                                      <block type="expression" id="vb%3%AxZ+6:s%~vtJ7dY">
                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="y">
                                                                                                                                      <block type="expression" id="R8c5jmIgxoT?xvfq%6,e">
                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_key" id="UmJkdNT?|1[K^o!^qq#_">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="key">
                                                                                                                                          <block type="string_value" id="C5Rj|14AnaI8o0L:ISOL">
                                                                                                                                            <field name="value">linel270.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_src" id="g_wFSAF-aXc[z9~j/t!0">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="src">
                                                                                                                                              <block type="string_value" id="4=ESkKL/k/i!^Q?/^#,+">
                                                                                                                                                <field name="value">${image_path}linel270.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_angle" id="[E,?5OU7DD@21%,/Bykb^">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="angle">
                                                                                                                                                  <block type="expression" id="uL8P^@1jkq?h2B{G)$TZ6">
                                                                                                                                                    <field name="value">$cell.data - 180</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <next>
                                                                                                                                  <block type="image_shape" id="9s_D|D-nDt=ASoHW#%oX">
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_position" id="E4vLY%@1E`O@19WUO7=9JU">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="expression" id="].`L7UN`=8JGTq(k#:al">
                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id="KUkcCaF-NH:Rq+@1E5NqY">
                                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_key" id="Jix.lIP2JewYMYT]l8Sc">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="key">
                                                                                                                                              <block type="string_value" id="DHX]BISE+4/8%3,KVvW,">
                                                                                                                                                <field name="value">show.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_src" id="|m=WTCmSS!lMtZbo|fRP">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="src">
                                                                                                                                                  <block type="string_value" id="LGVdIni:oJq!pDUHmDs[">
                                                                                                                                                    <field name="value">${image_path}show.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="image_shape" id="iQpD4g2T)vENXhak.:|8">
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_position" id="y%4qKttM89YeG#9Pen;X">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id="2B^jsu-?:iO|:P%!_9s,">
                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="MwY4G@2GJCw6wB-B@1%I[P">
                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_key" id="^GKxzpm@2pxwCruSsO8FB">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="key">
                                                                                                                                                  <block type="string_value" id="fddR)s/%!`UG;$Ap{~~S">
                                                                                                                                                    <field name="value">point.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_image_src" id="`x)@24r/ZHbMaOL)~k$sU">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="src">
                                                                                                                                                      <block type="string_value" id="7p@2T=KGu@22To_jen[^RA">
                                                                                                                                                        <field name="value">${image_path}point.png</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="grid_shape" id="(FatmKuL(?48wPlRQ|I=">
                                                                        <statement name="#props">
                                                                          <block type="prop_grid_dimension" id="vZdzken^c;B.G)8n/d}}">
                                                                            <field name="#prop"></field>
                                                                            <value name="rows">
                                                                              <block type="expression" id="%wX5C/o/ZN;x|d3JKB.7">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="cols">
                                                                              <block type="expression" id="Yx:NzYx{F|E:w0X0#aKX">
                                                                                <field name="value">3</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_position" id="WaaKT4@24;95/sqB7=_}$">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="{(QB6C0YUo`EK3NbzhS_">
                                                                                    <field name="value">400</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="Ec3[WXBLDN^CniHEt@27_">
                                                                                    <field name="value">310</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_size" id="ruFn~(#LtRvoZd9_Mh7w">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="width">
                                                                                      <block type="expression" id="`%x=gYhH4|JRqW0N%h9y">
                                                                                        <field name="value">780</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="height">
                                                                                      <block type="expression" id="Tq(|=KO)T~zJ(9.Bxx6W">
                                                                                        <field name="value">230</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_anchor" id="%g4M@2hZ.I.}@2vc$VKU(A">
                                                                                        <field name="#prop">anchor</field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="^s{m)F!@29C$j[Yk}WDNC">
                                                                                            <field name="value">0.5</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="wn#Pl?fR8J3#WJH]f|/9">
                                                                                            <field name="value">0.5</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_grid_cell_source" id="`cB?$RsZI7anpkfG3nUA">
                                                                                            <field name="#prop">cell.source</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="{!aCSl8hQ!XnV$[Tb=GD">
                                                                                                <field name="value">angle</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_grid_random" id="LJ$`)}u3a6@1MB9zF6mg@1">
                                                                                                <field name="#prop"></field>
                                                                                                <field name="random">FALSE</field>
                                                                                                <next>
                                                                                                  <block type="prop_grid_show_borders" id="_[jON!zT~%cNw@2#dU}-s">
                                                                                                    <field name="#prop">#showBorders</field>
                                                                                                    <field name="value">FALSE</field>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_cell_template" id="RFBniC+w-Rb=mp3_RU=N">
                                                                                                        <field name="variable">$cell</field>
                                                                                                        <field name="#prop">cell.template</field>
                                                                                                        <field name="#callback">$cell</field>
                                                                                                        <statement name="body">
                                                                                                          <block type="choice_custom_shape" id="KSM9iD#fhCLu$$n#1mI-">
                                                                                                            <field name="action">click-one</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="R:F/$TApg^EaK.~u!0/k">
                                                                                                                <field name="value">$cell.data</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <statement name="template">
                                                                                                              <block type="image_shape" id="%sT[:$qBiAqG7k{t]+G)">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_position" id="j?2]f7J,+rEC7Wf+gxKw">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="iMwdSb@1+]@1HhW4:JHBv;">
                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="h}XN80hTAq5Ŵk/m3u">
                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_size" id="07DWUUdR7]4b:9#}cr~y">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="width">
                                                                                                                          <block type="expression" id="@2.8$PQR/dW6sY}|afhqU">
                                                                                                                            <field name="value">240</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="height">
                                                                                                                          <block type="expression" id="TF=zJ01Hy3V4W1o^-j-{">
                                                                                                                            <field name="value">230</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_key" id="o1@2.tdRMqBWq$7ZJMH08">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="key">
                                                                                                                              <block type="string_value" id="]by4O?qxZ=0W}Wx_IXwZ">
                                                                                                                                <field name="value">hide.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_src" id="^vy}J?bw:g|9N8L|w|[%">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="src">
                                                                                                                                  <block type="string_value" id="rZ.i%]Gbm=}O?51XW+$@2">
                                                                                                                                    <field name="value">${image_path}hide.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="choice_custom_shape" id=")VsH;/gH-1oZTAJh[WjZ">
                                                                            <field name="action">drag</field>
                                                                            <value name="value">
                                                                              <block type="expression" id="bPk{5{d^4^CUAFj1yEdt">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <statement name="template">
                                                                              <block type="image_shape" id="L;z:=V)4krx_[lzu?QY_">
                                                                                <statement name="#props">
                                                                                  <block type="prop_position" id="!pn@2R4}/L0[XKuTZuVZb">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="B+zVDpfr1TNPmM8:AZ;d">
                                                                                        <field name="value">280</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="kP=,tuD8?~$VLI`VyF9e">
                                                                                        <field name="value">0</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_anchor" id="Xo-D.UZyjFm50/b([?/T">
                                                                                        <field name="#prop">anchor</field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="NeAD8@1r,+(TIciU(hBLB">
                                                                                            <field name="value">0.5</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="veeoF}Nj}gN3XH4tm@2lw">
                                                                                            <field name="value">0</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_max_size" id="Q!1:6!bEMtH+%{v/(3b_">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="maxWidth">
                                                                                              <block type="expression" id="QEG-1WO#C%Xrl|C}]:%$">
                                                                                                <field name="value">250</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="maxHeight">
                                                                                              <block type="expression" id="#Yg4a5`)MKu@1OtamZ">
                                                                                                <field name="value">0</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_image_key" id="1oGJ`jL.gKgkf[3RY[3B">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="key">
                                                                                                  <block type="string_value" id="NsAP|bPHY=oW`_@18B~T6">
                                                                                                    <field name="value">pro.png</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_image_src" id="ss5^-]bTi_Y#I1`LqNPf">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="src">
                                                                                                      <block type="string_value" id="KcX|~iQBu{9NRlDfO9^]">
                                                                                                        <field name="value">${image_path}pro.png</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="func_add_answer" id="SWk`z?10./oV2Y_yOq.X" inline="true">
                                                                                <value name="value">
                                                                                  <block type="expression" id="4FYtlQjcI@1}l2TE7jeS^">
                                                                                    <field name="value">a</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </statement>
                                                    <next>
                                                      <block type="if_then_block" id="xHxG,3-?P!PYs3o=0u^@2">
                                                        <value name="if">
                                                          <block type="expression" id="{j`;M/lyr=@1l+L}t:a">
                                                            <field name="value">type == 2</field>
                                                          </block>
                                                        </value>
                                                        <statement name="then">
                                                          <block type="variable" id="`IP}L,,I1M1G`X_ty7va">
                                                            <field name="name">a</field>
                                                            <value name="value">
                                                              <block type="random_one" id=":?3|1XG?[w3}#SjDp?#?">
                                                                <value name="items">
                                                                  <block type="expression" id="Hk-V?xV1m!XLWa;])_ZQ">
                                                                    <field name="value">list_angle</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="text_shape" id="z]HcULE%VOp/sax1!0uv">
                                                                <statement name="#props">
                                                                  <block type="prop_position" id="8E@2H5JM?kZjrmjG-2?,~">
                                                                    <field name="#prop"></field>
                                                                    <value name="x">
                                                                      <block type="expression" id="G@1]8`X;F=E,#u/Gf:fT.">
                                                                        <field name="value">400</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="y">
                                                                      <block type="expression" id="D@29o:wsPkhsGlFT8xEU}">
                                                                        <field name="value">40</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_anchor" id="]gw7wg;$CE$TsdDb.QgG">
                                                                        <field name="#prop">anchor</field>
                                                                        <value name="x">
                                                                          <block type="expression" id=",?cv4V:ZL.0wtsrOwDZz">
                                                                            <field name="value">0.5</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id="Rwq-X94XI-ljDoBS8f6D">
                                                                            <field name="value">0.5</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_text_contents" id="~D01Pos@14fWm]|~fqxX,">
                                                                            <field name="#prop"></field>
                                                                            <value name="contents">
                                                                              <block type="string_value" id="epejIMo=cB3-@1vSIZG5)">
                                                                                <field name="value">What is the size of the angle?</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_text_style" id="}se0C#(._s0|/@12])^q^">
                                                                                <field name="base">text</field>
                                                                                <statement name="#props">
                                                                                  <block type="prop_text_style_font_size" id="B;)F9lEHvypnhpB7Dun^">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="fontSize">
                                                                                      <block type="expression" id="0RQ/QE{wj!Gq_J4{{/-)">
                                                                                        <field name="value">35</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_text_style_fill" id="R}eRnWKi|4g.!dVp@2BEW">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="fill">
                                                                                          <block type="string_value" id="0,OJ=@1Yq-bebTPG)z9C6">
                                                                                            <field name="value">black</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_style_stroke" id="rmvSkn5G^/rJh]7#o;|]">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="stroke">
                                                                                              <block type="string_value" id="0#7)tkAId|Wa[hx756C6">
                                                                                                <field name="value">white</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_style_stroke_thickness" id="=lI5N;Cfe|!/=#)n4bU,">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="strokeThickness">
                                                                                                  <block type="expression" id="(ah!oxuCJmxh/){=;nDs">
                                                                                                    <field name="value">2</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style_wordwrap" id="I,2|Rr#B7Mc`e^.JF0}F">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="wordWrap">
                                                                                                      <block type="expression" id="FrjUkUz{D#NGVj@25r:Gi">
                                                                                                        <field name="value">true</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style_wordWrapWidth" id="h..xaiUAK9K5(H@2IGE}!">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="wordWrapWidth">
                                                                                                          <block type="expression" id="}@1A!@20F1Z8|_.Txk#R34">
                                                                                                            <field name="value">500</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <next>
                                                                  <block type="grid_shape" id=")HS(ng1[i`R)2_/MDM0F">
                                                                    <statement name="#props">
                                                                      <block type="prop_grid_dimension" id="|i|+gB/dp_6PI32.(eOG">
                                                                        <field name="#prop"></field>
                                                                        <value name="rows">
                                                                          <block type="expression" id="B`$#^$ZuLlj+fRye2#Sv">
                                                                            <field name="value">1</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="cols">
                                                                          <block type="expression" id=";,=-5g(WgluMt[u^%?o$">
                                                                            <field name="value">1</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_position" id="X+@2NS5u5@2`:!mt||H-/7">
                                                                            <field name="#prop"></field>
                                                                            <value name="x">
                                                                              <block type="expression" id="t@1c,Dy4hUVl#NvmnO:a3">
                                                                                <field name="value">250</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="y">
                                                                              <block type="expression" id="jp12pnKU_%@2IgM+_[,jk">
                                                                                <field name="value">180</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_size" id="1nTz1`n#E?9^(Ss[%[P^">
                                                                                <field name="#prop"></field>
                                                                                <value name="width">
                                                                                  <block type="expression" id="z@2D%HkFym{Nm1k9dyDS#">
                                                                                    <field name="value">300</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="height">
                                                                                  <block type="expression" id="s%Xvx/~vP4C_~3_.m]CN">
                                                                                    <field name="value">200</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_anchor" id="1Gdw%[A+62;5~jW_?I;r">
                                                                                    <field name="#prop">anchor</field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="ZXJ[g:Qmme^),`{2afpU">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="0Y4R0),MdExc[H#2V3!(">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_grid_cell_source" id="Z0CwMs=Y7ke+!hAVU[WL">
                                                                                        <field name="#prop">cell.source</field>
                                                                                        <value name="value">
                                                                                          <block type="expression" id="]UD$3$kwmv3eyVWS+9De">
                                                                                            <field name="value">1</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_grid_random" id="8k~wfn-ck{bOn89/_;@2g">
                                                                                            <field name="#prop"></field>
                                                                                            <field name="random">FALSE</field>
                                                                                            <next>
                                                                                              <block type="prop_grid_show_borders" id="WK$.qJf%MTJ0_)+5Yl/3">
                                                                                                <field name="#prop">#showBorders</field>
                                                                                                <field name="value">FALSE</field>
                                                                                                <next>
                                                                                                  <block type="prop_grid_cell_template" id="d+-^o|6n}cA86LiB-XeM">
                                                                                                    <field name="variable">$cell</field>
                                                                                                    <field name="#prop">cell.template</field>
                                                                                                    <field name="#callback">$cell</field>
                                                                                                    <statement name="body">
                                                                                                      <block type="image_shape" id="(%{D5^dP7^VYRjI/zT[1">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_position" id="Z$iB/TBn?IIFreQM.X)^">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="7G(kpGSG0DL|Z@1+DD}++">
                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="4PhgGDy]V_s|O}dD@1}1K">
                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_size" id="g1:@2C9(F6[C/Z8qlZAWO">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="width">
                                                                                                                  <block type="expression" id="~,M#CCy%f$CskT/x~e#b">
                                                                                                                    <field name="value">240</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="height">
                                                                                                                  <block type="expression" id="L@2i7NzFKZaH{En9C#mVC">
                                                                                                                    <field name="value">230</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_image_key" id="KT9!n-Kl);[T@1)SonP%y">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="key">
                                                                                                                      <block type="string_value" id="zN3^gyD(B23P5|KDRP,z">
                                                                                                                        <field name="value">box.png</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_src" id="6S{VufAFiRD%?KdaOD6{">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="src">
                                                                                                                          <block type="string_value" id="5xLdhFck~TGw3TSn/VU,">
                                                                                                                            <field name="value">${image_path}box.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="drop_shape" id="-KP??l@1C48+1J7W}njMt">
                                                                                                            <field name="multiple">FALSE</field>
                                                                                                            <field name="resultMode">none</field>
                                                                                                            <field name="groupName"></field>
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_position" id="WgzPs74abhN3lf!dOmec">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id=";2|8Zb%RJ?yg:W{iYZ~}">
                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="-wUS/9KJBh:0LgfB_}N6">
                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_size" id="4@1Tc6D_W:`kY6/]F7ZcF">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="width">
                                                                                                                      <block type="expression" id="=@24/~6dCUDk.`1Qgy;NP">
                                                                                                                        <field name="value">0</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="height">
                                                                                                                      <block type="expression" id="Bz.6I6CW9w]2AmXn[|i2">
                                                                                                                        <field name="value">0</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="if_then_else_block" id="[O52/a/hp5X#@1M-z5i`P">
                                                                                                                <value name="if">
                                                                                                                  <block type="expression" id="tr9e?Cp0_X[.`(0p`R2/">
                                                                                                                    <field name="value">a &lt; 180</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <statement name="then">
                                                                                                                  <block type="image_shape" id="PqWa(=5SR@2mNTudwEFoT">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_position" id="@1Y|5MPy1aM!418@28f35}">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="Hl0]nT@1b!Uw-GKfswY)C">
                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="nHFY7t[E_1[;#H!,[NS?">
                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_key" id="~tzP-{pC)Pkp[|PD|u@1J">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="key">
                                                                                                                              <block type="string_value" id="v^vk{L{u$O?[Tnl0;w2H">
                                                                                                                                <field name="value">cl.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_src" id="g1!)}=3%gORdl/})(=|:">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="src">
                                                                                                                                  <block type="string_value" id="nKr4%+HI7-4#{kKFIf0D">
                                                                                                                                    <field name="value">${image_path}cl.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_angle" id="WX+A$V[x@1(|L^^?K(o!V">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="angle">
                                                                                                                                      <block type="expression" id="k@2lPbA#oHiijTfi!ZM@2r">
                                                                                                                                        <field name="value">a</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="image_shape" id="[mDTe(cTY1+E5rMGf]1`">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="@1K,Jk5pBC_HbbkUmiL:b">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="Y0Mefbutq+U.sJEm5qUK">
                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="x2n{rwXmLbdXD4NS(NaM">
                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_key" id="A2oR.=j_N^!.rryY-wRl">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="key">
                                                                                                                                  <block type="string_value" id="|4]|bIU~ZV6u%!/KC!fN">
                                                                                                                                    <field name="value">liner.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_src" id="^dOr)N6x24Fk{WOTF(zU">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="src">
                                                                                                                                      <block type="string_value" id="DBO=yh/rO!@21+U#lZh2y">
                                                                                                                                        <field name="value">${image_path}liner.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="image_shape" id="-y44h]_rt7x:9OkXrwPg">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="[4Lwt/k1-39z+N#tgnLe">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="o33wP4QqtQ|Mo_A0{kn{">
                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="m(6.JPL.+#Jb`k:kY(yU">
                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_key" id="1:%[z5.bOL+@2TmVrhN?z">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="key">
                                                                                                                                      <block type="string_value" id="k:.m!Fo|H|k_7~7~h:3p">
                                                                                                                                        <field name="value">linel.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_src" id="tzQh=ftlVD-4CCbo;uQ?">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="src">
                                                                                                                                          <block type="string_value" id=":~7FhU!,c:|A.vTTI@2q=">
                                                                                                                                            <field name="value">${image_path}linel.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_angle" id="@1fe5/3W`9bwGwQ(_wimW">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="angle">
                                                                                                                                              <block type="expression" id="A.EEDNuH/~G!uRHcmJ#n">
                                                                                                                                                <field name="value">a - 180</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="image_shape" id="YUcP^yQAnA.CH,B/,N6Z">
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_position" id="O`j$?I4=g@2c[`g=G|Enl">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="x">
                                                                                                                                      <block type="expression" id="lY`AOq@1DfH@21sat.OxoF">
                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="y">
                                                                                                                                      <block type="expression" id="?|]6{CaOy!a[ACz@1AUQi">
                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_key" id="1{.b6h^UC@2k3+=qa@1q%i">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="key">
                                                                                                                                          <block type="string_value" id="fS@1Sm?~AwBIZz`bs|o$?">
                                                                                                                                            <field name="value">point.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_src" id="54MWsLxy^gDSr:Hb;hZ+">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="src">
                                                                                                                                              <block type="string_value" id="hr!f@1=9#3kz0R|JV6`VY">
                                                                                                                                                <field name="value">${image_path}point.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <statement name="else">
                                                                                                                  <block type="if_then_else_block" id="Pr7;k6G`A(wRY:%|/^4=">
                                                                                                                    <value name="if">
                                                                                                                      <block type="expression" id="#(r8]Os,=fBl#i{f|4jB">
                                                                                                                        <field name="value">a &lt; 270</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <statement name="then">
                                                                                                                      <block type="image_shape" id="`q]s=qu:G;nfM:MH|F1D">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="!~.`VE[@16^=zL@2^Mb]j.">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="L{:_?W%{]i,cxS4YH`UL">
                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="Zba6x`@2BYp8caP?f6oZZ">
                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_key" id="JL=jR]XS#@2.7+o+}@2Dqh">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="key">
                                                                                                                                  <block type="string_value" id="%A}tDA~?@2#%XZ/P)FC@1u">
                                                                                                                                    <field name="value">cl.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_src" id="C|!7)[.?u}`a!i.f`tj4">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="src">
                                                                                                                                      <block type="string_value" id="Kwy8#CT#jL:,/1QBA@2h)">
                                                                                                                                        <field name="value">${image_path}cl.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_angle" id="cmwUW5Nf?0Gs8q~=}Hcg">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="angle">
                                                                                                                                          <block type="expression" id="0~Vl51]@2kCOk3w;RLgC!">
                                                                                                                                            <field name="value">a</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="image_shape" id="8qFWFYH@1o=?dW+%wb-0G">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="Zy}4en+jT6hu(NBZp,c@1">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="24_M%f:P7Q/.IJVf:d=i">
                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="/h.$Wr4@1YQ6@2M@2E]2B!L">
                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_key" id="5=gU=$V3THv(_b@1KQGzt">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="key">
                                                                                                                                      <block type="string_value" id="=o:D9/r#xq%wzP2.(nYc">
                                                                                                                                        <field name="value">liner270.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_src" id="Z:.o6efqXMN.Fyav?0N5">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="src">
                                                                                                                                          <block type="string_value" id="`IuUV$/FCaz#8NKTC=28">
                                                                                                                                            <field name="value">${image_path}liner270.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="image_shape" id="{C@2hZILPrf+!XsRdNe.@2">
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_position" id="(P%zhE/f0Vtg2%|-6sC_">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="x">
                                                                                                                                      <block type="expression" id=":BdTX3$LFCTWEHeRcN+b">
                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="y">
                                                                                                                                      <block type="expression" id="TJ$1=Joh@2Sq0,tX[C:TI">
                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_key" id="!qk}gJc`s+aT(xNX/(0A">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="key">
                                                                                                                                          <block type="string_value" id="ZpRXszD/cReqEQD;T.i7">
                                                                                                                                            <field name="value">linel270.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_src" id="gEQkwkWL;y;NGO06f#_!">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="src">
                                                                                                                                              <block type="string_value" id="7yQ,hQw^#7-aG:dZv.bv">
                                                                                                                                                <field name="value">${image_path}linel270.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_angle" id="TnV@1E;KMx}@1u3@1xOz32/">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="angle">
                                                                                                                                                  <block type="expression" id="fQZ2b~mSh$0ez~+@1Z9he">
                                                                                                                                                    <field name="value">a - 180</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <next>
                                                                                                                                  <block type="image_shape" id="}wUR^,`;/]gi]6?|o]hP">
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_position" id="YrC`8`0!HH_azbSimul}">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="expression" id="VA=fQcPnbtH}3jNuP@2a;">
                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id="NKGd5;7[a_g?uO-hR%xL">
                                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_key" id="ZVLrGr+_{eOWDHkeBKDu">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="key">
                                                                                                                                              <block type="string_value" id="2?}FSuV$v^^3^{3@1Gli)">
                                                                                                                                                <field name="value">point.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_src" id="!N5(4M:`Z0~e]dPsY#Sz">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="src">
                                                                                                                                                  <block type="string_value" id="PG.$q5f]Hm(DpF(;}cbp">
                                                                                                                                                    <field name="value">${image_path}point.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <statement name="else">
                                                                                                                      <block type="image_shape" id="%Gm3U)b^T[dfE`WGlAhI">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="Xr^/4Oc@1#t]vZs4@1@1sAc">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="chGx@2cm[o;K1UX4u|S@2Q">
                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="W`4o_UW#:/8VLx(0rAHN">
                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_key" id="d0@1uMEFI?YAe$eLUm;8d">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="key">
                                                                                                                                  <block type="string_value" id="|c!!CRjTXXW9yxM+f]f[">
                                                                                                                                    <field name="value">cl.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_src" id="V~@2JpU,4leOWz4WR.wtT">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="src">
                                                                                                                                      <block type="string_value" id="nMMG8ozWwh3{3;M16j1v">
                                                                                                                                        <field name="value">${image_path}cl.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_angle" id="V(2@24eB3~?F}Bxn-gfJK">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="angle">
                                                                                                                                          <block type="expression" id="AQ[}{{yLc`jf4:kmQ%jD">
                                                                                                                                            <field name="value">a</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="image_shape" id="s1O7IBEwa5$=9()Ojz9]">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="4xa}Mk5@2[W?YX?Mq5WBV">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="%Ar4-vb.q}IW{J7COy7=">
                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="F;N)KVwF=a5uYQdg~@1W|">
                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_key" id="|tq[pb@2-6Qv^@1s_@1kD]l">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="key">
                                                                                                                                      <block type="string_value" id=",mFRn)Z!NHfl@2UNt4@1-M">
                                                                                                                                        <field name="value">liner360.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_src" id="!J+|9jfGI|2uZh[:3^Qb">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="src">
                                                                                                                                          <block type="string_value" id="#DBNZH2)M:%`qHp#yPcV">
                                                                                                                                            <field name="value">${image_path}liner360.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="image_shape" id="2~jC{-=C{$@1ngWF#H!@2b">
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_position" id="}gL#.vz=-_o;j:8c2T5/">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="x">
                                                                                                                                      <block type="expression" id="c}gGl8!S}LVkK(1cfy)i">
                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="y">
                                                                                                                                      <block type="expression" id="%x}lVO|ddQ/Ivx7]{6Si">
                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_key" id="Y49#a(OK[W{2u@1d5s;FY">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="key">
                                                                                                                                          <block type="string_value" id="1~sPys6-Tro=:JW5NWo,">
                                                                                                                                            <field name="value">linel270.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_src" id="z=u0Xv-uyMok[zC2,kO(">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="src">
                                                                                                                                              <block type="string_value" id="ngi!$NWv[#V9RoDe=u0|">
                                                                                                                                                <field name="value">${image_path}linel270.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_angle" id="@2kw5iC#$W9_#G2dBGtC`">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="angle">
                                                                                                                                                  <block type="expression" id="?n0AD?;FPF1:K;5`t3lX">
                                                                                                                                                    <field name="value">a - 180</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <next>
                                                                                                                                  <block type="image_shape" id="9PaE|P.xqBcTQ]NW_+^]">
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_position" id="_u5^~H9/Nh@1X)7~$I0Pc">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="expression" id="#`fO+;#LB3C7cceI1?6s">
                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id="y6S)m#LA:Q1@1y^YDBSf9">
                                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_key" id="3U6f|Zkr=.x_uI_j(-jM">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="key">
                                                                                                                                              <block type="string_value" id="XY~S~K[u62r5R?+pw!P3">
                                                                                                                                                <field name="value">show.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_src" id="pZM?tpX~9|6WAXEH2785">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="src">
                                                                                                                                                  <block type="string_value" id="mC~_tM[HL[0E]$V92f{9">
                                                                                                                                                    <field name="value">${image_path}show.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="image_shape" id="R~..%A,UE1Y:HbtSdE}@1">
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_position" id="kF+,(}gYVoS-s{_pa,ei">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id="9km]!RBk16Thv^7U-S|,">
                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="nk)Hz/EelHxi_wV@2kKZq">
                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_key" id="nXo~uL!kGX;usucHzXD]">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="key">
                                                                                                                                                  <block type="string_value" id=".Ma~j7J~!dD[))+@2lAyf">
                                                                                                                                                    <field name="value">point.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_image_src" id=",HqgRDqEb-6TZTo2an1=">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="src">
                                                                                                                                                      <block type="string_value" id="b@1Vk!R-]Av{9I)/_]iJ)">
                                                                                                                                                        <field name="value">${image_path}point.png</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="choice_custom_shape" id="2!QEk.iqF35(4#b1O[ZC">
                                                                        <field name="action">drag</field>
                                                                        <value name="value">
                                                                          <block type="expression" id="Lx?mT#uoj/E[KCq?gdS@1">
                                                                            <field name="value">1</field>
                                                                          </block>
                                                                        </value>
                                                                        <statement name="template">
                                                                          <block type="image_shape" id="DF)mTKwrQMv(DTsUzZZK">
                                                                            <statement name="#props">
                                                                              <block type="prop_position" id="{hR(8e3S#%6gjE1UYr3:">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="TGxIf`)!=A6KDV@2n9o9h">
                                                                                    <field name="value">600</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="|2nDR|fyP+~+Y^x4]6@2@2">
                                                                                    <field name="value">365</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_anchor" id="P+-HJDvfU+URT)Fv^4t)">
                                                                                    <field name="#prop">anchor</field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="ad%#:g!f7yY;OSfc}}In">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="A:l).$)I`xh38`;IrJFu">
                                                                                        <field name="value">1</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_max_size" id="!4=ao8|Or_:D?@1_:I;PT">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="maxWidth">
                                                                                          <block type="expression" id="AlfSG3#PnW3~uU#2SDQ_">
                                                                                            <field name="value">300</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="maxHeight">
                                                                                          <block type="expression" id="tr+H;t}q.RMv#MJ8]}HA">
                                                                                            <field name="value">0</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_image_key" id="p`4f[o_,@12L9H8ZeS0X?">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="key">
                                                                                              <block type="string_value" id="JeW5twm1/Q}CtXt_+#Im">
                                                                                                <field name="value">pro.png</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_image_src" id="XF^jLq7qku!=;Zo)so#f">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="src">
                                                                                                  <block type="string_value" id="{AJA?Z3Y,h_=YG[rWhGV">
                                                                                                    <field name="value">${image_path}pro.png</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="image_shape" id="{b@2OG.JZavV~!`rv3h,I">
                                                                            <statement name="#props">
                                                                              <block type="prop_position" id="[B^jsIm}cDI%eS~X|CA0">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="V8m,+{_Oyqo`P-DBVRT1">
                                                                                    <field name="value">250</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="/D6H^Dj.y4Cj%kp?$VI.">
                                                                                    <field name="value">350</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_image_key" id="ln9i5%#@2pSQ`oFDu/eDA">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="key">
                                                                                      <block type="string_value" id="TI^V0#)vQ7LQ-,Nt7o7=">
                                                                                        <field name="value">shape.png</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_image_src" id="c%l{]rF1Smzp{r5vVkQ3">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="src">
                                                                                          <block type="string_value" id="^R#Z?CH=A!|5q[W4Umt=">
                                                                                            <field name="value">${image_path}shape.png</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="algorithmic_input_shape" id="[e}LcmqF@19.%T?]@1jB-Z">
                                                                                <statement name="#props">
                                                                                  <block type="prop_value" id="TN6clfe}:4H;@1k0w%+`S">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="@1Y0=VVDuz^gR8?]A.GgV">
                                                                                        <field name="value">a</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_position" id="(u^k=nloWKrc75-ihkQ6">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="+SQo+%UqGFz6@107iOi%4">
                                                                                            <field name="value">250</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="aOD#P0{Z$l:BCNh)9@2~E">
                                                                                            <field name="value">350</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_size" id="~)S$^+Glx|;{-)@1m?MCt">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="width">
                                                                                              <block type="expression" id="wdRRw?lccA]3huvks^0:">
                                                                                                <field name="value">125</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="height">
                                                                                              <block type="expression" id="FlQ@1R-hpt5|W3YEsjl5h">
                                                                                                <field name="value">68</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_input_keyboard" id="pX[+1]_P03Uw?r6RhxOm">
                                                                                                <field name="#prop"></field>
                                                                                                <field name="keyboard">numbers1</field>
                                                                                                <next>
                                                                                                  <block type="prop_input_max_length" id="e^196@2S|6ukB7~2Bxi9C">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="maxLength">
                                                                                                      <block type="expression" id="/_E;-7``ONG+]nS_NxH+">
                                                                                                        <field name="value">a.toString().length</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_input_result_position" id="#LH4fkex}OZhG#[@2d;3P">
                                                                                                        <field name="#prop"></field>
                                                                                                        <field name="resultPosition">bottom</field>
                                                                                                        <next>
                                                                                                          <block type="prop_tab_order" id="s|F@1Izb{|@2lRY,asD!hu">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="tabOrder">
                                                                                                              <block type="expression" id="2[il+_dgVQKQm?SvtFDj">
                                                                                                                <field name="value">0</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_stroke" id="D`nCliS/s1emx9euC0}t">
                                                                                                                <field name="#prop">stroke</field>
                                                                                                                <next>
                                                                                                                  <block type="prop_fill" id="Dz-AaH/!wx?BO[MH9dIz">
                                                                                                                    <field name="#prop">fill</field>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style" id="!1Qr3{IxD@1PMJ]G^LlcN">
                                                                                                                        <field name="base">text</field>
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_text_style_font_size" id="!(nlSpda9T+o/iXBj}YF">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="fontSize">
                                                                                                                              <block type="expression" id="FQTZZB_+$@1-hM?K.!NSO">
                                                                                                                                <field name="value">32</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_fill" id="w-4`M-zsemYrO[}g`_|q">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="fill">
                                                                                                                                  <block type="string_value" id="G;VmNGg}u8OZR_#3M_Oe">
                                                                                                                                    <field name="value">black</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_stroke" id="ahppz0%Y{eD@2C$!vyJ4m">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="stroke">
                                                                                                                                      <block type="string_value" id="}KNcD$1%M)+@1a=7A%7,,">
                                                                                                                                        <field name="value">white</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="TBWlBG{V@2q@2LOjXi7|@16">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="strokeThickness">
                                                                                                                                          <block type="expression" id="bs_awM@2W;X@1OG#fQ+@2no">
                                                                                                                                            <field name="value">2</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </statement>
                                                        <next>
                                                          <block type="if_then_block" id="V|%3@1q-bm|70m-MBn_HD">
                                                            <value name="if">
                                                              <block type="expression" id=":}{B6~N0-~A7eP8wXo#_">
                                                                <field name="value">type == 3</field>
                                                              </block>
                                                            </value>
                                                            <statement name="then">
                                                              <block type="do_while_block" id="t7(=j}8tQ{G?,=n[=h.l">
                                                                <statement name="do">
                                                                  <block type="variable" id="}o54|MM,yR6$Cw-.GJmP">
                                                                    <field name="name">a</field>
                                                                    <value name="value">
                                                                      <block type="random_one" id="uv~@2%P^k{@2T#YcCN|ufo">
                                                                        <value name="items">
                                                                          <block type="expression" id="%)WKd%#qm)X{4ZD!VF^P">
                                                                            <field name="value">list_angle</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </statement>
                                                                <value name="while">
                                                                  <block type="expression" id="3Q?}Ow_7SNy;f5%E)5{h">
                                                                    <field name="value">a &lt; 185</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="text_shape" id="e@1MGql~S4j)AsgzRriox">
                                                                    <statement name="#props">
                                                                      <block type="prop_position" id="}PoM6u@2|n2k=_2)%Sgj:">
                                                                        <field name="#prop"></field>
                                                                        <value name="x">
                                                                          <block type="expression" id="jS+gN@1PuwLB};1soYwRb">
                                                                            <field name="value">400</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id="MzxG19kJfakGobv@1QuOx">
                                                                            <field name="value">40</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_anchor" id="hoh1E0Lc^K@1zVH^ENBwp">
                                                                            <field name="#prop">anchor</field>
                                                                            <value name="x">
                                                                              <block type="expression" id="Z]FC?Z|2Xa$r6oKvD{yS">
                                                                                <field name="value">0.5</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="y">
                                                                              <block type="expression" id=":6Z#R1/ZllIaHnd%C,`V">
                                                                                <field name="value">0.5</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_text_contents" id="%N^Ril9JBzmDmYJW,]J/">
                                                                                <field name="#prop"></field>
                                                                                <value name="contents">
                                                                                  <block type="string_value" id="ICgn=!`z8iwp{+.]SVC#">
                                                                                    <field name="value">Measure with each reflex angles to the nearest ${range &lt; 0 ? 30 : range &lt; 4 ? 10 : 5}°.</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_text_style" id="7A1?Wr8l5VGmRWCa6tPp">
                                                                                    <field name="base">text</field>
                                                                                    <statement name="#props">
                                                                                      <block type="prop_text_style_font_size" id="`eC@2q)9b}Cx^(NwD@24aO">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="fontSize">
                                                                                          <block type="expression" id="ac@1Cf9.66kDPonX~[V%z">
                                                                                            <field name="value">35</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_style_fill" id="/+Af[)?`9M:gQfK[/F[)">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="fill">
                                                                                              <block type="string_value" id="RG:nM1=QvxbCTVTVLxnj">
                                                                                                <field name="value">black</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_style_stroke" id="d8pvImV%unOdLaK9uQEC">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="stroke">
                                                                                                  <block type="string_value" id="C21AQ#_m[#uFHXE!ytae">
                                                                                                    <field name="value">white</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style_stroke_thickness" id="ahRr-9+n2Oj}_qHY4{gv">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="strokeThickness">
                                                                                                      <block type="expression" id="PojsL@2J;.-rs7=f_L,?U">
                                                                                                        <field name="value">2</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="grid_shape" id="xRp;q=qxFEUU_o5]Ul.6">
                                                                        <statement name="#props">
                                                                          <block type="prop_grid_dimension" id="c4_o|_aHfD]]z+K$scW!">
                                                                            <field name="#prop"></field>
                                                                            <value name="rows">
                                                                              <block type="expression" id="BzG0oWv3RYn;WhKSHmY^">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="cols">
                                                                              <block type="expression" id="Mpp#/fQruP(1u3I9qUr)">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_position" id="0z.(]g@2x1#U.t;.{95dd">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="@1$ydVf?IDO!0Na@1Y=YA!">
                                                                                    <field name="value">250</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="7xt9!JNr@2{LH=PvVjv6$">
                                                                                    <field name="value">180</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_size" id="`D=E6b}),Xa@1%Hb::+2n">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="width">
                                                                                      <block type="expression" id="^BeX+Ud7%k|_,5:@2x~bo">
                                                                                        <field name="value">300</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="height">
                                                                                      <block type="expression" id="Hav@1O)72N-I-f3Clp/3-">
                                                                                        <field name="value">200</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_anchor" id="3V}Pz]wwl}o$)K|:qOu$">
                                                                                        <field name="#prop">anchor</field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="G@2z6Q?xREE@1c,wDs}MIw">
                                                                                            <field name="value">0.5</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="S-^NU#0vt@1[;#Vi@2ykqp">
                                                                                            <field name="value">0.5</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_grid_cell_source" id="z/8xn/IZNe{1?V8^fgN?">
                                                                                            <field name="#prop">cell.source</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="+;I#Pt,RK[7U4#S5Rk~#">
                                                                                                <field name="value">1</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_grid_random" id="gW_vGoj5Jo6b|=[+lJL]">
                                                                                                <field name="#prop"></field>
                                                                                                <field name="random">FALSE</field>
                                                                                                <next>
                                                                                                  <block type="prop_grid_show_borders" id=";5L5l9W7Vrci5R94]@1H/">
                                                                                                    <field name="#prop">#showBorders</field>
                                                                                                    <field name="value">FALSE</field>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_cell_template" id="7^S1oqNT~JrSwtC#(|K=">
                                                                                                        <field name="variable">$cell</field>
                                                                                                        <field name="#prop">cell.template</field>
                                                                                                        <field name="#callback">$cell</field>
                                                                                                        <statement name="body">
                                                                                                          <block type="image_shape" id="+I,gIsK[+{6!b0Jk`@1Cw">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_position" id="f7LTSgOscglP8Q!GF;fH">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id=".)#@17N(4#g^L!Exu~Daz">
                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="@2@2F.o2+{Xqq78O5N0hVi">
                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_size" id="1(lTAWXP=Hf42pJ2,xbm">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="width">
                                                                                                                      <block type="expression" id="nm@2]Vq#6/k?704DZ5#]8">
                                                                                                                        <field name="value">240</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="height">
                                                                                                                      <block type="expression" id="$t(m+jP9kR/C]HF]WCAX">
                                                                                                                        <field name="value">230</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_key" id="9`EAw|qUIXO_-N`:NSt+">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="key">
                                                                                                                          <block type="string_value" id="4x;h-khjf~dAb!EqiFs/">
                                                                                                                            <field name="value">box.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_src" id="tB1G9ywJUIE7weGM^!Zz">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="src">
                                                                                                                              <block type="string_value" id="|-;/46xQA.b@16n8_%dI3">
                                                                                                                                <field name="value">${image_path}box.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="drop_shape" id="pi@1LUT(c4-;z:{}Vh4CQ">
                                                                                                                <field name="multiple">FALSE</field>
                                                                                                                <field name="resultMode">none</field>
                                                                                                                <field name="groupName"></field>
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_position" id="$s7LJ]5l9I=w_Cav+BK1">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="6uevAT=}J30M=3Ylf75}">
                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="e~xu,rvYNi:E]0}jW|PZ">
                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_size" id="xt~=4jdNlbWw[utjX,ir">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="width">
                                                                                                                          <block type="expression" id="F@2pa^nqXgj$o-73Lsx#;">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="height">
                                                                                                                          <block type="expression" id="d31#-92Xc4WevUJ|2s9?">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="if_then_else_block" id="$iuL0f3FU,ei@1cXQ:OOk">
                                                                                                                    <value name="if">
                                                                                                                      <block type="expression" id="z}nJeRbX#9jP0_:OBvs+">
                                                                                                                        <field name="value">a &lt; 180</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <statement name="then">
                                                                                                                      <block type="image_shape" id="5sB?Y{gY#z%@1J8bENJoA">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="$EEzqjSE?#yOAb%?S_WE">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="EMgu2N{K@1W=[~kT}h:29">
                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="pjl/7XHZNMT3o_O?qIUl">
                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_key" id="^c]EN3g#.ntN5`k#oeUv">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="key">
                                                                                                                                  <block type="string_value" id="=hcdb.0GR)p=Z5%=5lb~">
                                                                                                                                    <field name="value">cl.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_src" id="L#s)4V2M1~G$^5/[XmWY">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="src">
                                                                                                                                      <block type="string_value" id="r8{7ViLdhn}oQ2c9lQOT">
                                                                                                                                        <field name="value">${image_path}cl.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_angle" id="[7i(q.h]/u0R_QZ2PWgu">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="angle">
                                                                                                                                          <block type="expression" id="@2r.{ZuEsJT@2Zyf#gLa9Q">
                                                                                                                                            <field name="value">a</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="image_shape" id="+oH#g-ByD`I_z7Hq:0q0">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="omXa@2@2j2q02yDO,zg!YB">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="-~XOG5{rD.f_Av12.[61">
                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="(d%Q9qE7_:!-U;f/NSVF">
                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_key" id="e%?kM@2my/;SEPbRk`Rla">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="key">
                                                                                                                                      <block type="string_value" id="96Gh%yOfU{R0dbJG!C_R">
                                                                                                                                        <field name="value">liner.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_src" id="CpVM^t1Q+U[8W78lCS!k">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="src">
                                                                                                                                          <block type="string_value" id="DX$^h{taU@2N,o0^0f@1@14">
                                                                                                                                            <field name="value">${image_path}liner.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="image_shape" id="0WI@2DQ,zF-i/Np^wg@1Z?">
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_position" id="v0I`:_0|1g8X;Rn7|=/J">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="x">
                                                                                                                                      <block type="expression" id="vlrD-HR,]%NM%Nk}[T">
                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="y">
                                                                                                                                      <block type="expression" id="v#^czOqg3_MD{/!9dE4@1">
                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_key" id="p;jaLkBQ6iYQPPP1fDM,">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="key">
                                                                                                                                          <block type="string_value" id="s57Px^#9I^Grv{q;H,Ep">
                                                                                                                                            <field name="value">linel.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_src" id="-,2Fng[L5x5BeuSHA(aD">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="src">
                                                                                                                                              <block type="string_value" id="Iox8nm2JF2e@1$M(]J$_|">
                                                                                                                                                <field name="value">${image_path}linel.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_angle" id="~h)=Gc/wfu:_OMA0~I;,">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="angle">
                                                                                                                                                  <block type="expression" id=",|#`wmDQ=ki}oj.-7ED5">
                                                                                                                                                    <field name="value">a - 180</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <next>
                                                                                                                                  <block type="image_shape" id="juW#0=GNn~%eO5{V)5{Y">
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_position" id="]c9QsOO{{n;i|nDGnL~P">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="expression" id="PF%+DkHyC-x+y@1Wl/vfQ">
                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id="|?8y5m!v-kWr4$1-ityb">
                                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_key" id="G3%iJ,cj[8]eftj2{@1]}">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="key">
                                                                                                                                              <block type="string_value" id=")q0o?@2ydw}JbvCIIfzGh">
                                                                                                                                                <field name="value">point.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_src" id="~zFIJ(iKf^NwmayKWH53">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="src">
                                                                                                                                                  <block type="string_value" id=":8VJ$V{ft(q-R+RoExoh">
                                                                                                                                                    <field name="value">${image_path}point.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <statement name="else">
                                                                                                                      <block type="if_then_else_block" id="RFb,p[xqtMjK?qr)E/cu">
                                                                                                                        <value name="if">
                                                                                                                          <block type="expression" id="dmbf[2c?3B/aVI)|KAS|">
                                                                                                                            <field name="value">a &lt; 270</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <statement name="then">
                                                                                                                          <block type="image_shape" id="w_LfB7u)c8HI.`1k2GU)">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="cS=Z53Ryqy5@2Kz1p!eY|">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="MCOH#m?=!YHnXX!#3LA)">
                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="Bh4A^p]mE}w@1A?x~,y;y">
                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_key" id="1bjQSH^#rhrrFsu:d,SM">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="key">
                                                                                                                                      <block type="string_value" id="_/hyRd(Q(#1t-7m31=wX">
                                                                                                                                        <field name="value">cl.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_src" id="o2@2TE{vGKDr?^}l`GBp`">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="src">
                                                                                                                                          <block type="string_value" id="wzAHanr-Z4itDgtdMkn~">
                                                                                                                                            <field name="value">${image_path}cl.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_angle" id="9IT|oA)?CKuvRoToe~5=">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="angle">
                                                                                                                                              <block type="expression" id=";f)-]U7,viNFZ0i=%Zc/">
                                                                                                                                                <field name="value">a</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="image_shape" id=".21Hp)K:D]ly{@2^8UM=9">
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_position" id="xts(THFu{J_c9ZWjNt#e">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="x">
                                                                                                                                      <block type="expression" id="%e^GbmhX8^#[]28H-9p4">
                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="y">
                                                                                                                                      <block type="expression" id="%0q@1QV$xf8^dDh6H)LF@2">
                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_key" id="W(Bm:]GmK{FPC[oB3l`g">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="key">
                                                                                                                                          <block type="string_value" id="IBATDPUrX@1[uWzGBm[^v">
                                                                                                                                            <field name="value">liner270.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_src" id="+9gZF}CEGwtnGd~DF6%b">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="src">
                                                                                                                                              <block type="string_value" id=",N%G}0)}pgR@1]#6MJgH;">
                                                                                                                                                <field name="value">${image_path}liner270.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <next>
                                                                                                                                  <block type="image_shape" id="[F{)x~SJ[ir%EJ0a7}GN">
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_position" id="Z4)(RX(qnnHJpU,DVjAu">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="expression" id="+7Ain{NF);_YS:DQSXoN">
                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id="Z~PNhV[T#`$hFPLcu{os">
                                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_key" id="OaP`}fX]=(nL@1l@1dG?b+">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="key">
                                                                                                                                              <block type="string_value" id="A;n7fRc5ou!YYB((/,:b">
                                                                                                                                                <field name="value">linel270.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_src" id="3?wepFZq.PwBk5^CVr;8">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="src">
                                                                                                                                                  <block type="string_value" id="Af(jFiVcMbBJRAi1DUcN">
                                                                                                                                                    <field name="value">${image_path}linel270.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_angle" id="#SHPE)[Kpn+;jdsSW-Jk">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="angle">
                                                                                                                                                      <block type="expression" id="bqboBLLiT?eTS4Nes$zh">
                                                                                                                                                        <field name="value">a - 180</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="image_shape" id="}Rg8dD5nSbR2HFVExANj">
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_position" id="SOjF3]~X|P~ufMw0Wv^W">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id="9@1g85A(/XiN3C8=dOTOV">
                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="ecrA@1yxWs{{!VMN$cMTY">
                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_key" id="s{$3?a;QgA!sxXu-a.%(">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="key">
                                                                                                                                                  <block type="string_value" id="3243peq~K`wRnBLebEfd">
                                                                                                                                                    <field name="value">point.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_image_src" id="rE2nWx{@2q9F!%UX2dn,9">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="src">
                                                                                                                                                      <block type="string_value" id="wd@2JPTR@2yMvb9^+(Iqn,">
                                                                                                                                                        <field name="value">${image_path}point.png</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <statement name="else">
                                                                                                                          <block type="image_shape" id="k#IZS@2(o7@2vU/riLO%+G">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="?8n7SMaldT2(3MG)uQlx">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="^:a2Uq?](5qY9Hhb)?pj">
                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="1ybIyYC?%B{NtLD9+A{+">
                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_key" id="Hn|s(JB|-};{82,c9TW)">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="key">
                                                                                                                                      <block type="string_value" id="g?twg=#`k)@2%+Wk6[Ez=">
                                                                                                                                        <field name="value">cl.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_src" id="#fh|Mvl$0YI,m09)o}(P">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="src">
                                                                                                                                          <block type="string_value" id="qmNP%WJ{nOe7ZB%hXYYg">
                                                                                                                                            <field name="value">${image_path}cl.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_angle" id="q=1O{I$2LD,RG:?]:{n_">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="angle">
                                                                                                                                              <block type="expression" id="SCq^p9_loZ|#}A{f@2gAr">
                                                                                                                                                <field name="value">a</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="image_shape" id="VuaMEO`o~xHV5z?#raJM">
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_position" id="{geeXSnSt]px!r9P|cq?">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="x">
                                                                                                                                      <block type="expression" id="d8-ifp6-gD(1I?]8BDuC">
                                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="y">
                                                                                                                                      <block type="expression" id="xROAQpt?1fpbamt]/}h$">
                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_key" id="@1(t1,DDm}(wI$D#T9mu@1">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="key">
                                                                                                                                          <block type="string_value" id="^W@1[57sWodHC%Y@2izAp6">
                                                                                                                                            <field name="value">liner360.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_src" id="_#/JK4|,xqI8NR,hpX!D">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="src">
                                                                                                                                              <block type="string_value" id="+Ck8t_no,ZFdJX:u}gFe">
                                                                                                                                                <field name="value">${image_path}liner360.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <next>
                                                                                                                                  <block type="image_shape" id=".v)u!d9#cvM-pJLo,J~h">
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_position" id="eygc?%Pe!_w7#1-F$(j6">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="expression" id="+SyRc|nWp}?GG:KYwq@2`">
                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id="(=IZ|dmGF5LCKnwgmGDT">
                                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_key" id="@2NQTOvyKX@2{w@2`.@2OK0e">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="key">
                                                                                                                                              <block type="string_value" id="-O[{5K;dqcL5v/fW@29xc">
                                                                                                                                                <field name="value">linel270.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_src" id="?#@2umEe;!%)`@1RyQ%=KC">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="src">
                                                                                                                                                  <block type="string_value" id="%U$fx9kn3L_0xg2q$+N1">
                                                                                                                                                    <field name="value">${image_path}linel270.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_angle" id="R(5{W4A,y|;/N1_)F!D6">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="angle">
                                                                                                                                                      <block type="expression" id="y|Z.43$B3jL#,suKnT9y">
                                                                                                                                                        <field name="value">a - 180</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="image_shape" id="T}[~8l5}hp$mSOw$bg38">
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_position" id="@1y.M;R9wJxCef|e:bAk1">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id="3J4jKHa{9^s@1PV%s0=3T">
                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="-Kv3M`aIL1}vH#an8EG[">
                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_key" id="#PVRQ=d4o0ySJV2UKfn~">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="key">
                                                                                                                                                  <block type="string_value" id="`3@2_%wnFop1UK,/at=CN">
                                                                                                                                                    <field name="value">show.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_image_src" id="Z00KQot%rT8/4^l~4ZBB">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="src">
                                                                                                                                                      <block type="string_value" id="{EodR18y}5}(+#Y|nj,5">
                                                                                                                                                        <field name="value">${image_path}show.png</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <next>
                                                                                                                                          <block type="image_shape" id="L?[4X9aGKrJ-p.af(h.P">
                                                                                                                                            <statement name="#props">
                                                                                                                                              <block type="prop_position" id="/_(=S_tYZ7e^[a.P]yfd">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="x">
                                                                                                                                                  <block type="expression" id="%zDk_pVe@21!$f|sd[m~^">
                                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <value name="y">
                                                                                                                                                  <block type="expression" id="@1%,1-=]3r=)@2rmzU!znn">
                                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_image_key" id="/aY3$Z_l.}@1-9XFxkH^$">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="key">
                                                                                                                                                      <block type="string_value" id="[ZhNy7jI[R%c-:McL[,x">
                                                                                                                                                        <field name="value">point.png</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_image_src" id="zL?x!G0g1z-UsQsbKtF1">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="src">
                                                                                                                                                          <block type="string_value" id="XK`dM=/I])I%Y58e-Y2U">
                                                                                                                                                            <field name="value">${image_path}point.png</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="choice_custom_shape" id=",30s(TW^o;N!D1dl1S~Z">
                                                                            <field name="action">drag</field>
                                                                            <value name="value">
                                                                              <block type="expression" id="ktVcjDfifvcAym.4_W,c">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <statement name="template">
                                                                              <block type="image_shape" id="aZtem8RQ,Mkzw.prqcyN">
                                                                                <statement name="#props">
                                                                                  <block type="prop_position" id="Y3zlf@28/)S+x0_b^3y@1F">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="+gw(O-HCa#x0LR|O1qmw">
                                                                                        <field name="value">600</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="YTMh8vu0a{1a!k;GkOGL">
                                                                                        <field name="value">365</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_anchor" id="AA3?ULQ!J+bn(ruJTIxW">
                                                                                        <field name="#prop">anchor</field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="LM7f|-hkj:{1x4hh^/#p">
                                                                                            <field name="value">0.5</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="=LEf;u7_w?KAZq$@202$6">
                                                                                            <field name="value">1</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_max_size" id="4IgPagD613?M~vS%Q(5j">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="maxWidth">
                                                                                              <block type="expression" id="SIO-`SXzneBWE9_Ak2/;">
                                                                                                <field name="value">300</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="maxHeight">
                                                                                              <block type="expression" id="dq=uHXO7x2MFfwa_4Hsq">
                                                                                                <field name="value">0</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_image_key" id="V0Qto0d)(l@1Ni{1p0c~s">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="key">
                                                                                                  <block type="string_value" id="pY)%lZcJozJcGX8J(`~Z">
                                                                                                    <field name="value">pro.png</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_image_src" id="d(+?YZrYN-1E`j)6Z3h^">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="src">
                                                                                                      <block type="string_value" id="tVEZ8j4)?9QR}OVP8E?4">
                                                                                                        <field name="value">${image_path}pro.png</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="image_shape" id="V^.EMb{uE9NU9M_g9%RI">
                                                                                <statement name="#props">
                                                                                  <block type="prop_position" id="6aP(IlNPk).nhC:%|L9m">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="7,KfuUS}{4}6dlpqVBc-">
                                                                                        <field name="value">250</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="R|#_HNTx02?/:=J1/Trh">
                                                                                        <field name="value">350</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_image_key" id="oQ1.0!^SC)zrRKblFnQW">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="key">
                                                                                          <block type="string_value" id="e{@1(1~I(%PuW10h[Q/sF">
                                                                                            <field name="value">shape.png</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_image_src" id="tEfTWO2_J34iVa3A;v">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="src">
                                                                                              <block type="string_value" id="E12.xZY%!qOPP~Tp8seI">
                                                                                                <field name="value">${image_path}shape.png</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="algorithmic_input_shape" id="@1;UtBR6h+eTC[8Dr8ZR}">
                                                                                    <statement name="#props">
                                                                                      <block type="prop_value" id="7n/mR0MKJY!j43yIcu[t">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="value">
                                                                                          <block type="expression" id="]5e(F%vOTIdrw`AqxqAD">
                                                                                            <field name="value">a</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_position" id="b?`g@2a$VUtSd^kzN0]9A">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="x">
                                                                                              <block type="expression" id="@1V-h[6zz=Gm{Jz@1S;dQM">
                                                                                                <field name="value">250</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="y">
                                                                                              <block type="expression" id="YnhW[rJ@1[73o!G;#r[kI">
                                                                                                <field name="value">350</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_size" id="Gj/2Px[O8ksuf[IHCKVo">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="width">
                                                                                                  <block type="expression" id="EeA_Fo!]l=)?S~9Al~WL">
                                                                                                    <field name="value">125</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="height">
                                                                                                  <block type="expression" id="5}Vj1Td}]Be?PpAG=/GJ">
                                                                                                    <field name="value">68</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_input_keyboard" id="5Rx_=teHAX_HR6Nwz-oE">
                                                                                                    <field name="#prop"></field>
                                                                                                    <field name="keyboard">numbers1</field>
                                                                                                    <next>
                                                                                                      <block type="prop_input_max_length" id="!ynEb)~nUiNGMdVCS03K">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="maxLength">
                                                                                                          <block type="expression" id="4niHCwJV$mm3v9]^TnfL">
                                                                                                            <field name="value">a.toString().length</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_input_result_position" id="3NEIYSAGervKgL;fLeaw">
                                                                                                            <field name="#prop"></field>
                                                                                                            <field name="resultPosition">bottom</field>
                                                                                                            <next>
                                                                                                              <block type="prop_tab_order" id="d=xF[FD8^/%yAEPRl_bV">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="tabOrder">
                                                                                                                  <block type="expression" id="gBqfUMriy@2/fii1#7mFl">
                                                                                                                    <field name="value">0</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_stroke" id="wZZ:u{_S#=1xd#$OIO;t">
                                                                                                                    <field name="#prop">stroke</field>
                                                                                                                    <next>
                                                                                                                      <block type="prop_fill" id="HB#TP$`-(hz]+QZRrj.`">
                                                                                                                        <field name="#prop">fill</field>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style" id="gG%?`?E72}Z~L62e@1N8S">
                                                                                                                            <field name="base">text</field>
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_text_style_font_size" id="w`G)]D%X9tu/M8A:8lC%">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="fontSize">
                                                                                                                                  <block type="expression" id="+_6rwT:NRB3:G8vaDQ5[">
                                                                                                                                    <field name="value">32</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_fill" id="LMt%.ss7=jFxK#d6LBHb">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="fill">
                                                                                                                                      <block type="string_value" id="eXXl[OKcZ)c.NCaE(r+3">
                                                                                                                                        <field name="value">black</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_stroke" id="aT8Ij9]V4Oto8_ZC;Zt)">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="stroke">
                                                                                                                                          <block type="string_value" id="8/:/D5_1qTDw^/_jv?!C">
                                                                                                                                            <field name="value">white</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_text_style_stroke_thickness" id="1x$e/@2BO7#)iZb@1/j/;#">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="strokeThickness">
                                                                                                                                              <block type="expression" id="J4{K=1pvD)zmvN:5lPj$">
                                                                                                                                                <field name="value">2</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </statement>
                                                            <next>
                                                              <block type="variable" id="LMjMV${0([g.@1Fj/#?8S">
                                                                <field name="name">link</field>
                                                                <value name="value">
                                                                  <block type="expression" id="dyZ+n+h33~WrpEGt[Kr[">
                                                                    <field name="value">'http://starmathsonline.s3.amazonaws.com/' + image_path</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="if_then_block" id="NVuLX{v+C{mYQn@10k#{~">
                                                                    <value name="if">
                                                                      <block type="expression" id="F`B4$?|!L[S;(hOrwiLt">
                                                                        <field name="value">type == 1</field>
                                                                      </block>
                                                                    </value>
                                                                    <statement name="then">
                                                                      <block type="partial_explanation" id=")YS)Vp`zbRfn}#WFpgNv" inline="true">
                                                                        <value name="value">
                                                                          <block type="string_value" id="f}}D05Jk4CcehRD^.UzG">
                                                                            <field name="value">&lt;u&gt;Step 1: First, look at all 3 angles.&lt;/u&gt;&lt;/br&gt;
  &lt;/br&gt;
Remember this, a full angle is 360°. Look at each angle, now calculate each angle.&lt;/br&gt;&lt;/br&gt;
&lt;style&gt;
      #angle1 {
              -ms-transform: rotate(${angle[0] - 180}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(angle[0] - 180}deg);
              /@2 Safari @2/
              transform: rotate(${angle[0] - 180}deg);
              /@2 Standard syntax @2/
          }
          #angle2 {
              -ms-transform: rotate(${angle[1] - 180}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(angle[1] - 180}deg);
              /@2 Safari @2/
              transform: rotate(${angle[1] - 180}deg);
              /@2 Standard syntax @2/
          }
          #angle3 {
              -ms-transform: rotate(${angle[2] - 180}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${angle[2] - 180}deg);
              /@2 Safari @2/
              transform: rotate(${angle[2] - 180}deg);
              /@2 Standard syntax @2/
          }

          #anglec1 {
              -ms-transform: rotate(${angle[0]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${angle[0]}deg);
              /@2 Safari @2/
              transform: rotate(${angle[0]}deg);
              /@2 Standard syntax @2/
          }
          #anglec2 {
              -ms-transform: rotate(${angle[1]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${angle[1]}deg);
              /@2 Safari @2/
              transform: rotate(${angle[1]}deg);
              /@2 Standard syntax @2/
          }
          #anglec3 {
              -ms-transform: rotate(${angle[2]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${angle[2]}deg);
              /@2 Safari @2/
              transform: rotate(${angle[2]}deg);
              /@2 Standard syntax @2/
          }
      &lt;/style&gt;


      &lt;div style='position: relative; width: 260px; height: 280px; display: inline-block;'&gt;
              &lt;div style='position: absolute; width: 300px; height: 300px; left: 10px; top: 10px;'&gt;
                &lt;img style='position: absolute; min-width: 240px; max-height: 240px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/box.png'/&gt;
                &lt;img id='anglec1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/cl.png'/&gt;
  ${angle[0] &lt; 180 ? "&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner.png'/&gt;&lt;img id='angle1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel.png'/&gt;" : angle[0] &lt; 270 ? "&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner270.png'/&gt;&lt;img id='angle1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/&gt;" : "&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner360.png'/&gt;&lt;img id='angle1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/&gt;&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/show.png'/&gt;"}
                &lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/point.png'/&gt;
              &lt;/div&gt;
      &lt;span style='position: absolute; left: 80px; top: 270px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'&gt;${angle[0]}&amp;deg;&lt;/span&gt;
            &lt;/div&gt;
&lt;div style='width: 30px; display: inline-block'&gt;&lt;/div&gt;
      &lt;div style='position: relative; width: 260px; height: 280px; display: inline-block;'&gt;
              &lt;div style='position: absolute; width: 300px; height: 300px; left: 10px; top: 10px;'&gt;
                &lt;img style='position: absolute; min-width: 240px; max-height: 240px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/box.png'/&gt;
                &lt;img id='anglec2' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/cl.png'/&gt;
${angle[1] &lt; 180 ? "&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner.png'/&gt;&lt;img id='angle2' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel.png'/&gt;" : angle[1] &lt; 270 ? "&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner270.png'/&gt;&lt;img id='angle2' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/&gt;" : "&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner360.png'/&gt;&lt;img id='angle2' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/&gt;&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/show.png'/&gt;"}
  &lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/point.png'/&gt;
              &lt;/div&gt;
      &lt;span style='position: absolute; left: 80px; top: 270px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'&gt;${angle[1]}&amp;deg;&lt;/span&gt;
            &lt;/div&gt;
&lt;div style='width: 30px; display: inline-block'&gt;&lt;/div&gt;
      &lt;div style='position: relative; width: 260px; height: 280px; display: inline-block;'&gt;
              &lt;div style='position: absolute; width: 300px; height: 300px; left: 10px; top: 10px;'&gt;
                &lt;img style='position: absolute; min-width: 240px; max-height: 240px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/box.png'/&gt;
                &lt;img id='anglec3' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/cl.png'/&gt;
                 ${angle[2] &lt; 180 ? "&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner.png'/&gt;&lt;img id='angle3' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel.png'/&gt;" : angle[2] &lt; 270 ? "&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner270.png'/&gt;&lt;img id='angle3' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/&gt;" : "&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner360.png'/&gt;&lt;img id='angle3' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/&gt;&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/show.png'/&gt;"}
  &lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/point.png'/&gt;
              &lt;/div&gt;
      &lt;span style='position: absolute; left: 80px; top: 270px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'&gt;${angle[2]}&amp;deg;&lt;/span&gt;
            &lt;/div&gt;
                                                                            </field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="end_partial_explanation" id="?e[48pyD2?Ue)na9Txf=">
                                                                            <next>
                                                                              <block type="partial_explanation" id="([d$9r=R)5k}UxBfT?u6" inline="true">
                                                                                <value name="value">
                                                                                  <block type="string_value" id="xPp1U2:,C+FyNV6F6T)m">
                                                                                    <field name="value">&lt;u&gt;Step 2: Let's find our final answer.&lt;/u&gt;&lt;/br&gt;
  &lt;/br&gt;
So which angle is ${a}&amp;deg;
&lt;/br&gt;&lt;/br&gt;
&lt;style&gt;
      #angle1 {
              -ms-transform: rotate(${angle[0] - 180}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(angle[0] - 180}deg);
              /@2 Safari @2/
              transform: rotate(${angle[0] - 180}deg);
              /@2 Standard syntax @2/
          }
          #angle2 {
              -ms-transform: rotate(${angle[1] - 180}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(angle[1] - 180}deg);
              /@2 Safari @2/
              transform: rotate(${angle[1] - 180}deg);
              /@2 Standard syntax @2/
          }
          #angle3 {
              -ms-transform: rotate(${angle[2] - 180}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${angle[2] - 180}deg);
              /@2 Safari @2/
              transform: rotate(${angle[2] - 180}deg);
              /@2 Standard syntax @2/
          }

          #anglec1 {
              -ms-transform: rotate(${angle[0]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${angle[0]}deg);
              /@2 Safari @2/
              transform: rotate(${angle[0]}deg);
              /@2 Standard syntax @2/
          }
          #anglec2 {
              -ms-transform: rotate(${angle[1]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${angle[1]}deg);
              /@2 Safari @2/
              transform: rotate(${angle[1]}deg);
              /@2 Standard syntax @2/
          }
          #anglec3 {
              -ms-transform: rotate(${angle[2]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${angle[2]}deg);
              /@2 Safari @2/
              transform: rotate(${angle[2]}deg);
              /@2 Standard syntax @2/
          }
      &lt;/style&gt;

&lt;div style='position: relative; width: 260px; height: 280px; display: inline-block; border: ${angle[0] == a ? "3px solid green": ""}'&gt;
    &lt;div style='position: absolute; width: 300px; height: 300px; left: 10px; top: 10px;'&gt;
        &lt;img style='position: absolute; min-width: 240px; max-height: 240px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/box.png'/&gt;
        &lt;img id='anglec1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/cl.png'/&gt;
        ${angle[0] &lt; 180 ? "&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner.png'/&gt;&lt;img id='angle1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel.png'/&gt;" : angle[0] &lt; 270 ? "&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner270.png'/&gt;&lt;img id='angle1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/&gt;" : "&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner360.png'/&gt;&lt;img id='angle1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/&gt;&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/show.png'/&gt;"}
        &lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/point.png'/&gt;
    &lt;/div&gt;
    &lt;span style='position: absolute; left: 80px; top: 270px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'&gt;${angle[0]}&amp;deg;&lt;/span&gt;
&lt;/div&gt;

&lt;div style='width: 30px; display: inline-block'&gt;&lt;/div&gt;

&lt;div style='position: relative; width: 260px; height: 280px; display: inline-block;  border: ${angle[1] == a ? "3px solid green": ""}'&gt;
    &lt;div style='position: absolute; width: 300px; height: 300px; left: 10px; top: 10px;'&gt;
        &lt;img style='position: absolute; min-width: 240px; max-height: 240px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/box.png'/&gt;
        &lt;img id='anglec2' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/cl.png'/&gt;
        ${angle[1] &lt; 180 ? "&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner.png'/&gt;&lt;img id='angle2' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel.png'/&gt;" : angle[1] &lt; 270 ? "&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner270.png'/&gt;&lt;img id='angle2' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/&gt;" : "&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner360.png'/&gt;&lt;img id='angle2' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/&gt;&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/show.png'/&gt;"}
        &lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/point.png'/&gt;
    &lt;/div&gt;
    &lt;span style='position: absolute; left: 80px; top: 270px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'&gt;${angle[1]}&amp;deg;&lt;/span&gt;
&lt;/div&gt;

&lt;div style='width: 30px; display: inline-block'&gt;&lt;/div&gt;

&lt;div style='position: relative; width: 260px; height: 280px; display: inline-block; border: ${angle[2] == a ? "3px solid green": ""}'&gt;
    &lt;div style='position: absolute; width: 300px; height: 300px; left: 10px; top: 10px;'&gt;
        &lt;img style='position: absolute; min-width: 240px; max-height: 240px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/box.png'/&gt;
        &lt;img id='anglec3' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/cl.png'/&gt;
        ${angle[2] &lt; 180 ? "&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner.png'/&gt;&lt;img id='angle3' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel.png'/&gt;" : angle[2] &lt; 270 ? "&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner270.png'/&gt;&lt;img id='angle3' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/&gt;" : "&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner360.png'/&gt;&lt;img id='angle3' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/&gt;&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/show.png'/&gt;"}
        &lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/point.png'/&gt;
    &lt;/div&gt;
    &lt;span style='position: absolute; left: 80px; top: 270px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'&gt;${angle[2]}&amp;deg;&lt;/span&gt;
&lt;/div&gt;
                                                                                    </field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="end_partial_explanation" id="2czYJo~!]53D/Nygd|Am"></block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="if_then_block" id="tZ,PG}wRTy5TVO=Zh_p[">
                                                                        <value name="if">
                                                                          <block type="expression" id="BnJM=9`]!F}nv[@1E!0yi">
                                                                            <field name="value">type == 2</field>
                                                                          </block>
                                                                        </value>
                                                                        <statement name="then">
                                                                          <block type="partial_explanation" id="3[KU@2dsE[|m@2C=v_.y=A" inline="true">
                                                                            <value name="value">
                                                                              <block type="string_value" id=";p:0E8)0xZ+zdZ`m6em+">
                                                                                <field name="value">Place the midpoint of the protractor on the VERTEX of the angle.
&lt;/br&gt;
At the same time, line up one side of the angle with the zero line of the protractor.
&lt;/br&gt;&lt;/br&gt;&lt;/br&gt;
&lt;style&gt;
                    #angle1 {
              -ms-transform: rotate(${a-180}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${a-180}deg);
              /@2 Safari @2/
              transform: rotate(${a-180}deg);
              /@2 Standard syntax @2/
          }
          #anglec1 {
              -ms-transform: rotate(${a}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${a}deg);
              /@2 Safari @2/
              transform: rotate(${a}deg);
              /@2 Standard syntax @2/
          }
          &lt;/style&gt;

        &lt;div style='position: relative; width: 260px; height: 280px; display: inline-block;'&gt;
            &lt;div style='position: absolute; width: 600px; height: 250px; left: 300px; top: 10px;'&gt;
              &lt;img style='position: absolute; min-width: 240px; max-height: 240px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/box.png'/&gt;
                &lt;img id='anglec1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/cl.png'/&gt;
  ${a &lt; 180 ? "&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner.png'/&gt;&lt;img id='angle1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel.png'/&gt;" : a &lt; 270 ? "&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner270.png'/&gt;&lt;img id='angle1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/&gt;" : "&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner360.png'/&gt;&lt;img id='angle1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/&gt;&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/show.png'/&gt;"}
                &lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/point.png'/&gt;
              
              &lt;img style='position: absolute; max-width: 350px; left: -55px; top: -45px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/pro.png'/&gt;
            
            &lt;/div&gt;
&lt;/div&gt;
&lt;/br&gt;&lt;/br&gt;
  &lt;span style='background: rgb(250, 250, 250, 0.5)'&gt;
Read the degrees where the other side crosses the number scale.
&lt;/br&gt;
It says &lt;b&gt;${a}&amp;deg;&lt;/b&gt;!&lt;/span&gt;
                                                                                </field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="if_then_block" id="j6h@2$^p(AL?4!b04WWHp">
                                                                            <value name="if">
                                                                              <block type="expression" id="{[T(UEOG%M(z-FkxXa5x">
                                                                                <field name="value">type == 3</field>
                                                                              </block>
                                                                            </value>
                                                                            <statement name="then">
                                                                              <block type="partial_explanation" id="cd;UXL[EVegLDUBJ,zkX" inline="true">
                                                                                <value name="value">
                                                                                  <block type="string_value" id="I6Sd!V5SKdeoM-s)x!xB">
                                                                                    <field name="value">Place the midpoint of the protractor on the VERTEX of the angle.
&lt;/br&gt;
At the same time, line up one side of the angle with the zero line of the protractor.
&lt;/br&gt;&lt;/br&gt;&lt;/br&gt;
&lt;style&gt;
                    #angle1 {
              -ms-transform: rotate(${a-180}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${a-180}deg);
              /@2 Safari @2/
              transform: rotate(${a-180}deg);
              /@2 Standard syntax @2/
          }
          #anglec1 {
              -ms-transform: rotate(${a}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${a}deg);
              /@2 Safari @2/
              transform: rotate(${a}deg);
              /@2 Standard syntax @2/
          }
          &lt;/style&gt;

        &lt;div style='position: relative; width: 260px; height: 280px; display: inline-block;'&gt;
            &lt;div style='position: absolute; width: 600px; height: 250px; left: 300px; top: 10px;'&gt;
              &lt;img style='position: absolute; min-width: 240px; max-height: 240px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/box.png'/&gt;
                &lt;img id='anglec1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/cl.png'/&gt;
  ${a &lt; 180 ? "&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner.png'/&gt;&lt;img id='angle1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel.png'/&gt;" : a &lt; 270 ? "&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner270.png'/&gt;&lt;img id='angle1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/&gt;" : "&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/liner360.png'/&gt;&lt;img id='angle1' style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/linel270.png'/&gt;&lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/show.png'/&gt;"}
                &lt;img style='position: absolute; top: 30px; left: 20px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/point.png'/&gt;
              
              &lt;img style='position: absolute; max-width: 350px; left: -55px; top: -45px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA360.1B/pro.png'/&gt;
            
            &lt;/div&gt;
&lt;/div&gt;
&lt;/br&gt;&lt;/br&gt;
  &lt;span style='background: rgb(250, 250, 250, 0.5)'&gt;
Read the degrees where the other side crosses the number scale.
&lt;/br&gt;
It says &lt;b&gt;${a}&amp;deg;&lt;/b&gt;!&lt;/span&gt;
                                                                                    </field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </statement>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
</xml>
END_XML]] */