
module.exports = [
  {
    "#type": "question",
    "name": "Y6.MG.AN.FA90.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y6.MG.AN.FA90.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "range < 0"
        },
        "then": [
          {
            "#type": "variable",
            "name": "list_angle",
            "value": {
              "#type": "expression",
              "value": "[30, 60, 90]"
            }
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "range < 4"
            },
            "then": [
              {
                "#type": "variable",
                "name": "list_angle",
                "value": {
                  "#type": "expression",
                  "value": "[20, 30, 40, 50, 60, 70, 80, 90]"
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "list_angle",
                "value": {
                  "#type": "expression",
                  "value": "[20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85]"
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "statement",
        "value": "function Ox(a){\n  if(a < 60){return 70;}\n  else {return 50;}\n}"
      },
      {
        "#type": "variable",
        "name": "type",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "3"
          }
        }
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "variable",
            "name": "angle",
            "value": {
              "#type": "random_many",
              "count": {
                "#type": "expression",
                "value": "3"
              },
              "items": {
                "#type": "expression",
                "value": "list_angle"
              }
            }
          },
          {
            "#type": "variable",
            "name": "a",
            "value": {
              "#type": "random_one",
              "items": {
                "#type": "expression",
                "value": "angle"
              }
            }
          },
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "550"
                },
                "y": {
                  "#type": "expression",
                  "value": "50"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "Which angle is ${a}°?"
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "35"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    },
                    {
                      "#type": "prop_text_style_wordwrap",
                      "#prop": "",
                      "wordWrap": {
                        "#type": "expression",
                        "value": "true"
                      }
                    },
                    {
                      "#type": "prop_text_style_wordWrapWidth",
                      "#prop": "",
                      "wordWrapWidth": {
                        "#type": "expression",
                        "value": "500"
                      }
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "3"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "300"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "780"
                },
                "height": {
                  "#type": "expression",
                  "value": "200"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "angle"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "240"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "220"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "box.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}box.png"
                      }
                    ]
                  },
                  {
                    "#type": "if_then_else_block",
                    "if": {
                      "#type": "expression",
                      "value": "$cell.data == 90"
                    },
                    "then": [
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX - 70"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY + 60"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "sq.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}sq.png"
                          }
                        ]
                      }
                    ],
                    "else": [
                      {
                        "#type": "image_shape",
                        "#props": [
                          {
                            "#type": "prop_position",
                            "#prop": "",
                            "x": {
                              "#type": "expression",
                              "value": "$cell.centerX - 70"
                            },
                            "y": {
                              "#type": "expression",
                              "value": "$cell.centerY + 60"
                            }
                          },
                          {
                            "#type": "prop_image_key",
                            "#prop": "",
                            "key": "cl.png"
                          },
                          {
                            "#type": "prop_image_src",
                            "#prop": "",
                            "src": "${image_path}cl.png"
                          },
                          {
                            "#type": "prop_angle",
                            "#prop": "",
                            "angle": {
                              "#type": "expression",
                              "value": "360 - $cell.data"
                            }
                          }
                        ]
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX - 70"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 60"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "liner.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}liner.png"
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX - 70"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 60"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "linel.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}linel.png"
                      },
                      {
                        "#type": "prop_angle",
                        "#prop": "",
                        "angle": {
                          "#type": "expression",
                          "value": "180 - $cell.data"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX - 70"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 60"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "point.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}point.png"
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "3"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "300"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "780"
                },
                "height": {
                  "#type": "expression",
                  "value": "200"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "angle"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "choice_custom_shape",
                    "action": "click-one",
                    "value": {
                      "#type": "expression",
                      "value": "$cell.data"
                    },
                    "template": {
                      "#callback": "$choice",
                      "variable": "$choice",
                      "body": [
                        {
                          "#type": "image_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "$cell.centerX"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "$cell.centerY"
                              }
                            },
                            {
                              "#type": "prop_size",
                              "#prop": "",
                              "width": {
                                "#type": "expression",
                                "value": "240"
                              },
                              "height": {
                                "#type": "expression",
                                "value": "220"
                              }
                            },
                            {
                              "#type": "prop_image_key",
                              "#prop": "",
                              "key": "hide.png"
                            },
                            {
                              "#type": "prop_image_src",
                              "#prop": "",
                              "src": "${image_path}hide.png"
                            }
                          ]
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "choice_custom_shape",
            "action": "drag",
            "value": {
              "#type": "expression",
              "value": "undefined"
            },
            "template": {
              "#callback": "$choice",
              "variable": "$choice",
              "body": [
                {
                  "#type": "image_shape",
                  "#props": [
                    {
                      "#type": "prop_position",
                      "#prop": "",
                      "x": {
                        "#type": "expression",
                        "value": "200"
                      },
                      "y": {
                        "#type": "expression",
                        "value": "5"
                      }
                    },
                    {
                      "#type": "prop_anchor",
                      "#prop": "anchor",
                      "x": {
                        "#type": "expression",
                        "value": "0.5"
                      },
                      "y": {
                        "#type": "expression",
                        "value": "0"
                      }
                    },
                    {
                      "#type": "prop_max_size",
                      "#prop": "",
                      "maxWidth": {
                        "#type": "expression",
                        "value": "300"
                      },
                      "maxHeight": {
                        "#type": "expression",
                        "value": "0"
                      }
                    },
                    {
                      "#type": "prop_image_key",
                      "#prop": "",
                      "key": "pro.png"
                    },
                    {
                      "#type": "prop_image_src",
                      "#prop": "",
                      "src": "${image_path}pro.png"
                    }
                  ]
                }
              ]
            }
          },
          {
            "#type": "func",
            "name": "addAnswers",
            "args": [
              {
                "#type": "expression",
                "value": "a"
              }
            ]
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 2"
        },
        "then": [
          {
            "#type": "variable",
            "name": "a",
            "value": {
              "#type": "random_one",
              "items": {
                "#type": "expression",
                "value": "list_angle"
              }
            }
          },
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "40"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "${ type == 2 ? 'What is the size of the angle?' : 'Find the unknown angle.'}"
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "35"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    },
                    {
                      "#type": "prop_text_style_wordwrap",
                      "#prop": "",
                      "wordWrap": {
                        "#type": "expression",
                        "value": "true"
                      }
                    },
                    {
                      "#type": "prop_text_style_wordWrapWidth",
                      "#prop": "",
                      "wordWrapWidth": {
                        "#type": "expression",
                        "value": "500"
                      }
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "choice_custom_shape",
            "action": "drag",
            "value": {
              "#type": "expression",
              "value": "undefined"
            },
            "template": {
              "#callback": "$choice",
              "variable": "$choice",
              "body": [
                {
                  "#type": "image_shape",
                  "#props": [
                    {
                      "#type": "prop_position",
                      "#prop": "",
                      "x": {
                        "#type": "expression",
                        "value": "600"
                      },
                      "y": {
                        "#type": "expression",
                        "value": "380"
                      }
                    },
                    {
                      "#type": "prop_anchor",
                      "#prop": "anchor",
                      "x": {
                        "#type": "expression",
                        "value": "0.5"
                      },
                      "y": {
                        "#type": "expression",
                        "value": "1"
                      }
                    },
                    {
                      "#type": "prop_max_size",
                      "#prop": "",
                      "maxWidth": {
                        "#type": "expression",
                        "value": "300"
                      },
                      "maxHeight": {
                        "#type": "expression",
                        "value": "0"
                      }
                    },
                    {
                      "#type": "prop_image_key",
                      "#prop": "",
                      "key": "pro.png"
                    },
                    {
                      "#type": "prop_image_src",
                      "#prop": "",
                      "src": "${image_path}pro.png"
                    }
                  ]
                }
              ]
            }
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "250"
                },
                "y": {
                  "#type": "expression",
                  "value": "200"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "300"
                },
                "height": {
                  "#type": "expression",
                  "value": "200"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "240"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "230"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "box.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}box.png"
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX - 70"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 60"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "cl.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}cl.png"
                      },
                      {
                        "#type": "prop_angle",
                        "#prop": "",
                        "angle": {
                          "#type": "expression",
                          "value": "360 - a"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX - 70"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 60"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "liner.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}liner.png"
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX - 70"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 60"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "linel.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}linel.png"
                      },
                      {
                        "#type": "prop_angle",
                        "#prop": "",
                        "angle": {
                          "#type": "expression",
                          "value": "180 - a"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX - 70"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 60"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "point.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}point.png"
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "600"
                },
                "y": {
                  "#type": "expression",
                  "value": "150"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "shape.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}shape.png"
              }
            ]
          },
          {
            "#type": "choice_input_shape",
            "#props": [
              {
                "#type": "prop_value",
                "#prop": "",
                "value": {
                  "#type": "expression",
                  "value": "a"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "600"
                },
                "y": {
                  "#type": "expression",
                  "value": "150"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "125"
                },
                "height": {
                  "#type": "expression",
                  "value": "68"
                }
              },
              {
                "#type": "prop_input_keyboard",
                "#prop": "",
                "keyboard": "numbers1"
              },
              {
                "#type": "prop_input_max_length",
                "#prop": "",
                "maxLength": {
                  "#type": "expression",
                  "value": "a.toString().length"
                }
              },
              {
                "#type": "prop_input_result_position",
                "#prop": "",
                "resultPosition": "bottom"
              },
              {
                "#type": "prop_tab_order",
                "#prop": "",
                "tabOrder": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_stroke",
                "#prop": "stroke"
              },
              {
                "#type": "prop_fill",
                "#prop": "fill"
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "32"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ],
            "#init": "algorithmic_input"
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 3"
        },
        "then": [
          {
            "#type": "do_while_block",
            "do": [
              {
                "#type": "variable",
                "name": "a",
                "value": {
                  "#type": "random_one",
                  "items": {
                    "#type": "expression",
                    "value": "list_angle"
                  }
                }
              }
            ],
            "while": {
              "#type": "expression",
              "value": "a > 70"
            }
          },
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "40"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "${ type == 2 ? 'What is the size of the angle?' : 'Find the unknown angle.'}"
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "35"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    },
                    {
                      "#type": "prop_text_style_wordwrap",
                      "#prop": "",
                      "wordWrap": {
                        "#type": "expression",
                        "value": "true"
                      }
                    },
                    {
                      "#type": "prop_text_style_wordWrapWidth",
                      "#prop": "",
                      "wordWrapWidth": {
                        "#type": "expression",
                        "value": "500"
                      }
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "250"
                },
                "y": {
                  "#type": "expression",
                  "value": "200"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "300"
                },
                "height": {
                  "#type": "expression",
                  "value": "200"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "240"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "230"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "box.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}box.png"
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX - 70"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 60"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "cl.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}cl.png"
                      },
                      {
                        "#type": "prop_angle",
                        "#prop": "",
                        "angle": {
                          "#type": "expression",
                          "value": "360 - a"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX - 70"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 60"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "cl4.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}cl4.png"
                      },
                      {
                        "#type": "prop_angle",
                        "#prop": "",
                        "angle": {
                          "#type": "expression",
                          "value": "90 - a"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX - 70"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 60"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "liner.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}liner.png"
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX - 70"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 60"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "linel.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}linel.png"
                      },
                      {
                        "#type": "prop_angle",
                        "#prop": "",
                        "angle": {
                          "#type": "expression",
                          "value": "180 - a"
                        }
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX - 70"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 60"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "liner90.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}liner90.png"
                      }
                    ]
                  },
                  {
                    "#type": "image_shape",
                    "#props": [
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX - 70"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY + 60"
                        }
                      },
                      {
                        "#type": "prop_image_key",
                        "#prop": "",
                        "key": "point.png"
                      },
                      {
                        "#type": "prop_image_src",
                        "#prop": "",
                        "src": "${image_path}point.png"
                      }
                    ]
                  },
                  {
                    "#type": "grid_shape",
                    "#props": [
                      {
                        "#type": "prop_grid_dimension",
                        "#prop": "",
                        "rows": {
                          "#type": "expression",
                          "value": "1"
                        },
                        "cols": {
                          "#type": "expression",
                          "value": "1"
                        }
                      },
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.centerX-50"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY - 40"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "50"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "30"
                        }
                      },
                      {
                        "#type": "prop_anchor",
                        "#prop": "anchor",
                        "x": {
                          "#type": "expression",
                          "value": "0.5"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "0.5"
                        }
                      },
                      {
                        "#type": "prop_grid_cell_source",
                        "#prop": "cell.source",
                        "value": {
                          "#type": "expression",
                          "value": "1"
                        }
                      },
                      {
                        "#type": "prop_grid_random",
                        "#prop": "",
                        "random": false
                      },
                      {
                        "#type": "prop_grid_show_borders",
                        "#prop": "#showBorders",
                        "value": false
                      },
                      {
                        "#type": "prop_grid_cell_template",
                        "variable": "$cell",
                        "#prop": "cell.template",
                        "#callback": "$cell",
                        "body": [
                          {
                            "#type": "text_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.centerX"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY"
                                }
                              },
                              {
                                "#type": "prop_text_contents",
                                "#prop": "",
                                "contents": "${90-a}"
                              },
                              {
                                "#prop": "",
                                "style": {
                                  "#type": "json",
                                  "base": "text",
                                  "#props": [
                                    {
                                      "#type": "prop_text_style_font_size",
                                      "#prop": "",
                                      "fontSize": {
                                        "#type": "expression",
                                        "value": "36"
                                      }
                                    },
                                    {
                                      "#type": "prop_text_style_fill",
                                      "#prop": "",
                                      "fill": "black"
                                    },
                                    {
                                      "#type": "prop_text_style_stroke",
                                      "#prop": "",
                                      "stroke": "white"
                                    },
                                    {
                                      "#type": "prop_text_style_stroke_thickness",
                                      "#prop": "",
                                      "strokeThickness": {
                                        "#type": "expression",
                                        "value": "2"
                                      }
                                    }
                                  ]
                                }
                              }
                            ]
                          },
                          {
                            "#type": "text_shape",
                            "#props": [
                              {
                                "#type": "prop_position",
                                "#prop": "",
                                "x": {
                                  "#type": "expression",
                                  "value": "$cell.right"
                                },
                                "y": {
                                  "#type": "expression",
                                  "value": "$cell.centerY-15"
                                }
                              },
                              {
                                "#type": "prop_text_contents",
                                "#prop": "",
                                "contents": "o"
                              },
                              {
                                "#prop": "",
                                "style": {
                                  "#type": "json",
                                  "base": "text",
                                  "#props": [
                                    {
                                      "#type": "prop_text_style_font_size",
                                      "#prop": "",
                                      "fontSize": {
                                        "#type": "expression",
                                        "value": "24"
                                      }
                                    },
                                    {
                                      "#type": "prop_text_style_fill",
                                      "#prop": "",
                                      "fill": "black"
                                    },
                                    {
                                      "#type": "prop_text_style_stroke",
                                      "#prop": "",
                                      "stroke": "white"
                                    },
                                    {
                                      "#type": "prop_text_style_stroke_thickness",
                                      "#prop": "",
                                      "strokeThickness": {
                                        "#type": "expression",
                                        "value": "2"
                                      }
                                    }
                                  ]
                                }
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "600"
                },
                "y": {
                  "#type": "expression",
                  "value": "250"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "shape.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}shape.png"
              }
            ]
          },
          {
            "#type": "choice_input_shape",
            "#props": [
              {
                "#type": "prop_value",
                "#prop": "",
                "value": {
                  "#type": "expression",
                  "value": "a"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "600"
                },
                "y": {
                  "#type": "expression",
                  "value": "250"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "125"
                },
                "height": {
                  "#type": "expression",
                  "value": "68"
                }
              },
              {
                "#type": "prop_input_keyboard",
                "#prop": "",
                "keyboard": "numbers1"
              },
              {
                "#type": "prop_input_max_length",
                "#prop": "",
                "maxLength": {
                  "#type": "expression",
                  "value": "a.toString().length"
                }
              },
              {
                "#type": "prop_input_result_position",
                "#prop": "",
                "resultPosition": "bottom"
              },
              {
                "#type": "prop_tab_order",
                "#prop": "",
                "tabOrder": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_stroke",
                "#prop": "stroke"
              },
              {
                "#type": "prop_fill",
                "#prop": "fill"
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "32"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ],
            "#init": "algorithmic_input"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "link",
        "value": {
          "#type": "expression",
          "value": "'http://starmathsonline.s3.amazonaws.com/' + image_path"
        }
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 1: First, look at all 3 angles.</u></br>\n  </br>\n<style>\n      #angle1 {\n              -ms-transform: rotate(${180-angle[0]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(180-angle[0]}deg);\n              /* Safari */\n              transform: rotate(${180-angle[0]}deg);\n              /* Standard syntax */\n          }\n          #angle2 {\n              -ms-transform: rotate(${180-angle[1]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${180-angle[1]}deg);\n              /* Safari */\n              transform: rotate(${180-angle[1]}deg);\n              /* Standard syntax */\n          }\n          #angle3 {\n              -ms-transform: rotate(${180-angle[2]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${180-angle[2]}deg);\n              /* Safari */\n              transform: rotate(${180-angle[2]}deg);\n              /* Standard syntax */\n          }\n\n          #anglec1 {\n              -ms-transform: rotate(${360-angle[0]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${360-angle[0]}deg);\n              /* Safari */\n              transform: rotate(${360-angle[0]}deg);\n              /* Standard syntax */\n          }\n          #anglec2 {\n              -ms-transform: rotate(${360-angle[1]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${360-angle[1]}deg);\n              /* Safari */\n              transform: rotate(${360-angle[1]}deg);\n              /* Standard syntax */\n          }\n          #anglec3 {\n              -ms-transform: rotate(${360-angle[2]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${360-angle[2]}deg);\n              /* Safari */\n              transform: rotate(${360-angle[2]}deg);\n              /* Standard syntax */\n          }\n      </style>\nRemember this, a right angle is 90°, Look at each angle, now calculate each angle.\n</br></br>\n<div style='position: relative; width: 280px; height: 280px; display: inline-block;'>\n                      <div style='position: absolute; width: 280px; height: 250px; left: 10px; top: 10px;'>\n                        <img style='position: absolute; min-width: 280px; max-height: 250px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/box.png'/>\n                        <img id='anglec1' style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/cl.png'/>\n                        <img style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/liner.png'/>\n                        <img id='angle1' style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/linel.png'/>\n                        <img style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/point.png'/>\n                      </div>\n              <span style='position: absolute; left: 100px; top: 270px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'>${angle[0]}&deg;</span>\n                    </div>\n              <div style='width: 20px; display: inline-block'></div>\n                    <div style='position: relative; width: 280px; height: 280px; display: inline-block;'>\n                      <div style='position: absolute; width: 280px; height: 250px; left: 10px; top: 10px;'>\n                        <img style='position: absolute; min-width: 280px; max-height: 250px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/box.png'/>\n                        <img id='anglec2' style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/cl.png'/>\n                        <img style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/liner.png'/>\n                        <img id='angle2' style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/linel.png'/>\n                        <img style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/point.png'/>\n                      </div>\n              <span style='position: absolute; left: 100px; top: 270px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'>${angle[1]}&deg;</span>\n                    </div>\n              <div style='width: 20px; display: inline-block'></div>\n                    <div style='position: relative; width: 280px; height: 280px; display: inline-block;'>\n                      <div style='position: absolute; width: 280px; height: 250px; left: 10px; top: 10px;'>\n                        <img style='position: absolute; min-width: 280px; max-height: 250px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/box.png'/>\n                        <img id='anglec3' style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/cl.png'/>\n                        <img style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/liner.png'/>\n                        <img id='angle3' style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/linel.png'/>\n                        <img style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/point.png'/>\n                      </div>\n              <span style='position: absolute; left: 100px; top: 270px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'>${angle[2]}&deg;</span>\n                    </div>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Step 2: Let's find our final answer.</u></br>\n  </br>\nSo which angle is ${a}&deg;.</br></br>\n  \n<style>\n      #angle1 {\n              -ms-transform: rotate(${180-angle[0]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(180-angle[0]}deg);\n              /* Safari */\n              transform: rotate(${180-angle[0]}deg);\n              /* Standard syntax */\n          }\n          #angle2 {\n              -ms-transform: rotate(${180-angle[1]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${180-angle[1]}deg);\n              /* Safari */\n              transform: rotate(${180-angle[1]}deg);\n              /* Standard syntax */\n          }\n          #angle3 {\n              -ms-transform: rotate(${180-angle[2]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${180-angle[2]}deg);\n              /* Safari */\n              transform: rotate(${180-angle[2]}deg);\n              /* Standard syntax */\n          }\n\n          #anglec1 {\n              -ms-transform: rotate(${360-angle[0]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${360-angle[0]}deg);\n              /* Safari */\n              transform: rotate(${360-angle[0]}deg);\n              /* Standard syntax */\n          }\n          #anglec2 {\n              -ms-transform: rotate(${360-angle[1]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${360-angle[1]}deg);\n              /* Safari */\n              transform: rotate(${360-angle[1]}deg);\n              /* Standard syntax */\n          }\n          #anglec3 {\n              -ms-transform: rotate(${360-angle[2]}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${360-angle[2]}deg);\n              /* Safari */\n              transform: rotate(${360-angle[2]}deg);\n              /* Standard syntax */\n          }\n      </style>\n\n\n<div style='position: relative; width: 300px; height: 280px; display: inline-block; border: ${angle[0] == a ? \"3px solid green\": \"\"}'>\n                      <div style='position: absolute; width: 280px; height: 250px; left: 10px; top: 10px;'>\n                        <img style='position: absolute; min-width: 280px; max-height: 250px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/box.png'/>\n                        <img id='anglec1' style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/cl.png'/>\n                        <img style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/liner.png'/>\n                        <img id='angle1' style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/linel.png'/>\n                        <img style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/point.png'/>\n                      </div>\n              <span style='position: absolute; left: 100px; top: 270px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'>${angle[0]}&deg;</span>\n                    </div>\n              <div style='width: 20px; display: inline-block'></div>\n<div style='position: relative; width: 300px; height: 280px; display: inline-block; border: ${angle[1] == a ? \"3px solid green\": \"\"}'>\n                      <div style='position: absolute; width: 280px; height: 250px; left: 10px; top: 10px;'>\n                        <img style='position: absolute; min-width: 280px; max-height: 250px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/box.png'/>\n                        <img id='anglec2' style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/cl.png'/>\n                        <img style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/liner.png'/>\n                        <img id='angle2' style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/linel.png'/>\n                        <img style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/point.png'/>\n                      </div>\n              <span style='position: absolute; left: 100px; top: 270px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'>${angle[1]}&deg;</span>\n                    </div>\n              <div style='width: 20px; display: inline-block'></div>\n<div style='position: relative; width: 300px; height: 280px; display: inline-block; border: ${angle[2] == a ? \"3px solid green\": \"\"}'>\n                      <div style='position: absolute; width: 280px; height: 250px; left: 10px; top: 10px;'>\n                        <img style='position: absolute; min-width: 280px; max-height: 250px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/box.png'/>\n                        <img id='anglec3' style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/cl.png'/>\n                        <img style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/liner.png'/>\n                        <img id='angle3' style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/linel.png'/>\n                        <img style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/point.png'/>\n                      </div>\n\n              <span style='position: absolute; left: 100px; top: 270px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'>${angle[2]}&deg;</span>\n                    </div>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 2"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "Place the midpoint of the protractor on the VERTEX of the angle.\n</br>\nAt the same time, line up one side of the angle with the zero line of the protractor.\n</br></br></br>\n<style>\n                    #angle1 {\n              -ms-transform: rotate(${180-a}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${180-a}deg);\n              /* Safari */\n              transform: rotate(${180-a}deg);\n              /* Standard syntax */\n          }\n          #anglec1 {\n              -ms-transform: rotate(${360-a}deg);\n              /* IE 9 */\n              -webkit-transform: rotate(${360-a}deg);\n              /* Safari */\n              transform: rotate(${360-a}deg);\n              /* Standard syntax */\n          }\n          </style>\n\n        <div style='position: relative; width: 260px; height: 280px; display: inline-block;'>\n            <div style='position: absolute; width: 600px; height: 350px; left: 300px; top: 10px;'>\n              <img style='position: absolute; min-width: 320px; max-height: 250px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/box.png'/>\n              <img id='anglec1' style='position: absolute; top: 30px; left: -60px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/cl.png'/>\n              <img style='position: absolute; top: 30px; left: -60px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/liner.png'/>\n              <img id='angle1' style='position: absolute; top: 30px; left: -60px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/linel.png'/>\n              <img style='position: absolute; top: 30px; left: -60px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/point.png'/>\n            \n              <img style='position: absolute; max-width: 350px; left: -85px; top: 5px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/pro.png'/>\n            \n            </div>\n</div>\n</br>\n  <span style='background: rgb(250, 250, 250, 0.5)'>\nRead the degrees where the other side crosses the number scale.\n</br>\nIt says <b>${a}&deg;</b>!</span>"
            ]
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 3"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<u>Remember the size of the straight line.</u></br></br>\n\nIt's 90°!</br></br>\n\nNow, using subtraction, let's subtract 90° by all the known angles.</br><br>\n\n90&deg; - ${90 - a}&deg; = <b>${a}&deg;</b></br></br>\n\nSo the angle is <b>${a}&deg;</b>!"
            ]
          }
        ]
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="0" y="0">
    <field name="name">Y6.MG.AN.FA90.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y6.MG.AN.FA90.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="YepMQT@2uwp2rRPvmnRg1">
                                    <field name="name">range</field>
                                    <value name="value">
                                      <block type="expression" id="UxgQ^Y5]rb}vpw[D=l-!">
                                        <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="if_then_else_block" id="0RWXxs$%M.7n6%X)PNK,">
                                        <value name="if">
                                          <block type="expression" id="2+XBg|,YRIr$@1g^-|a9J">
                                            <field name="value">range &lt; 0</field>
                                          </block>
                                        </value>
                                        <statement name="then">
                                          <block type="variable" id="9jlrMG+60[uqUgwS)]Jo">
                                            <field name="name">list_angle</field>
                                            <value name="value">
                                              <block type="expression" id="xZ$3C7saOpO94-)ipnwi">
                                                <field name="value">[30, 60, 90]</field>
                                              </block>
                                            </value>
                                          </block>
                                        </statement>
                                        <statement name="else">
                                          <block type="if_then_else_block" id="DuGp+Hdnw#P,vf5Wyso.">
                                            <value name="if">
                                              <block type="expression" id="RA[sz+[GcrCg%(gEZymC">
                                                <field name="value">range &lt; 4</field>
                                              </block>
                                            </value>
                                            <statement name="then">
                                              <block type="variable" id="U_|!Y;AL.%qUs~5E@2f2}">
                                                <field name="name">list_angle</field>
                                                <value name="value">
                                                  <block type="expression" id="9lnkt~d;Py3ki;JBY~XU">
                                                    <field name="value">[20, 30, 40, 50, 60, 70, 80, 90]</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </statement>
                                            <statement name="else">
                                              <block type="variable" id="?GOo_U)4uJ`bb5NDcl#8">
                                                <field name="name">list_angle</field>
                                                <value name="value">
                                                  <block type="expression" id="m@2Cw%}eGYrsWmRZz/5x6">
                                                    <field name="value">[20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85]</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </statement>
                                          </block>
                                        </statement>
                                        <next>
                                          <block type="statement" id=";t^zW4|7-TsF2z{@1EMLf">
                                            <field name="value">function Ox(a){
  if(a &lt; 60){return 70;}
  else {return 50;}
}
                                            </field>
                                            <next>
                                              <block type="variable" id="6yjfR{5BB@1usX_HtxTVL">
                                                <field name="name">type</field>
                                                <value name="value">
                                                  <block type="random_number" id="=^jXDv{3%rDSd;C0AD+?">
                                                    <value name="min">
                                                      <block type="expression" id="P=7x3+2%.#6~?Yc2e5{,">
                                                        <field name="value">1</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="S[S9]8gTY[j@1`?|4E08V">
                                                        <field name="value">3</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="if_then_block" id="w#P0D;{N1ST+@2DBj{?$0">
                                                    <value name="if">
                                                      <block type="expression" id="tJJXCU}%Sqd,bZaJxa)n">
                                                        <field name="value">type == 1</field>
                                                      </block>
                                                    </value>
                                                    <statement name="then">
                                                      <block type="variable" id="bE7g++F=3L4v95TQHFRJ">
                                                        <field name="name">angle</field>
                                                        <value name="value">
                                                          <block type="random_many" id="Lkc{F8k_F%YayI7Ob|hm">
                                                            <value name="count">
                                                              <block type="expression" id="[2}%=`D1tv75LDt@1s4hJ">
                                                                <field name="value">3</field>
                                                              </block>
                                                            </value>
                                                            <value name="items">
                                                              <block type="expression" id="U=8[)9Q;6@2c::yGw^m]#">
                                                                <field name="value">list_angle</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="M]P,k5l;iLL`jl=S,Jw~">
                                                            <field name="name">a</field>
                                                            <value name="value">
                                                              <block type="random_one" id="RTGw_I@2Pl,@1}=R@2TV5bL">
                                                                <value name="items">
                                                                  <block type="expression" id="!%HvmDl!u}mdnI`HY#F_">
                                                                    <field name="value">angle</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="text_shape" id="BMZ`VXgeL9SKSUPypqR}">
                                                                <statement name="#props">
                                                                  <block type="prop_position" id="2ZCJ!{{ewoqJ:U`qy/a3">
                                                                    <field name="#prop"></field>
                                                                    <value name="x">
                                                                      <block type="expression" id="@2}VwZjF@1@26jx%[t9s+S{">
                                                                        <field name="value">550</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="y">
                                                                      <block type="expression" id="v9Xp5o$8e(vwo?-5ulv)">
                                                                        <field name="value">50</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_anchor" id="/mlO?LRGl]Y6:miMCcVl">
                                                                        <field name="#prop">anchor</field>
                                                                        <value name="x">
                                                                          <block type="expression" id="(AL@10~x$mYek3xG#s8!R">
                                                                            <field name="value">0.5</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id="9@1zx[V:?]#@2^~.nF{K1i">
                                                                            <field name="value">0</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_text_contents" id="a/_O-[.Q43~MHRwq0s9L">
                                                                            <field name="#prop"></field>
                                                                            <value name="contents">
                                                                              <block type="string_value" id="sq!]aL5[V31FJEKfV=kt">
                                                                                <field name="value">Which angle is ${a}°?</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_text_style" id="a(3JA7@1Mb2H0sM##7f$V">
                                                                                <field name="base">text</field>
                                                                                <statement name="#props">
                                                                                  <block type="prop_text_style_font_size" id="a#|@2`]s}o!b.Ip9+i?S4">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="fontSize">
                                                                                      <block type="expression" id="rWx}E+P{y@2ktY_%^5d:j">
                                                                                        <field name="value">35</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_text_style_fill" id="!yxwytpitVg|ZpaK8C!U">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="fill">
                                                                                          <block type="string_value" id="EL(Zzz`XUBi;_$4Fo~?.">
                                                                                            <field name="value">black</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="stroke">
                                                                                              <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                                <field name="value">white</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="strokeThickness">
                                                                                                  <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                                    <field name="value">2</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style_wordwrap" id="eg?/EQ2A@2O.N-04S:c9,">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="wordWrap">
                                                                                                      <block type="expression" id="0pCQ`1e+.:~f$32g`8vA">
                                                                                                        <field name="value">true</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style_wordWrapWidth" id="M2hlIxxcA_1AZq|^dgOv">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="wordWrapWidth">
                                                                                                          <block type="expression" id="!9]`O^T#$X.CD~mZ:@2@1-">
                                                                                                            <field name="value">500</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <next>
                                                                  <block type="grid_shape" id="6tU?K^N8ec~A@1@1NwQ72N">
                                                                    <statement name="#props">
                                                                      <block type="prop_grid_dimension" id="X4K:|9dKK%s9l4n5v@2lG">
                                                                        <field name="#prop"></field>
                                                                        <value name="rows">
                                                                          <block type="expression" id="[Ev!.bxnzjc|-33v.%7=">
                                                                            <field name="value">1</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="cols">
                                                                          <block type="expression" id="u$7]/V/!ZWM4(Va??@2({">
                                                                            <field name="value">3</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_position" id="zLld84TfBg#!88]1,JD-">
                                                                            <field name="#prop"></field>
                                                                            <value name="x">
                                                                              <block type="expression" id="Cd6g{Y_P?:GyVyvQi1wJ">
                                                                                <field name="value">400</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="y">
                                                                              <block type="expression" id=".F}E71+@1i)77!yQ`hmfu">
                                                                                <field name="value">300</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_size" id="+o2CWr^.ohx)_bcIG53]">
                                                                                <field name="#prop"></field>
                                                                                <value name="width">
                                                                                  <block type="expression" id="Z[x5iHG%KWC%KWQb}c-G">
                                                                                    <field name="value">780</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="height">
                                                                                  <block type="expression" id="@2E|Ob@1jF#gYtX8-%Iui@1">
                                                                                    <field name="value">200</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_anchor" id="1WJjXY^.h,t]eNgxaJ}q">
                                                                                    <field name="#prop">anchor</field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="DHx[vqs?.?nUD:AYPJWd">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="e3D|RR7w!=Sufu):zV`c">
                                                                                        <field name="value">0.5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_grid_cell_source" id="v8+Yu211Qg8fX9Mu4th`">
                                                                                        <field name="#prop">cell.source</field>
                                                                                        <value name="value">
                                                                                          <block type="expression" id="~j@1,^liddf}oDg}E91^i">
                                                                                            <field name="value">angle</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_grid_random" id="}?kS#x[b)9OGb@1`UdJGV">
                                                                                            <field name="#prop"></field>
                                                                                            <field name="random">FALSE</field>
                                                                                            <next>
                                                                                              <block type="prop_grid_show_borders" id=";y^wk4y%[VWjXOj~vrQZ">
                                                                                                <field name="#prop">#showBorders</field>
                                                                                                <field name="value">FALSE</field>
                                                                                                <next>
                                                                                                  <block type="prop_grid_cell_template" id="7^6]Kv:2yrZi`cooAW2@2">
                                                                                                    <field name="variable">$cell</field>
                                                                                                    <field name="#prop">cell.template</field>
                                                                                                    <field name="#callback">$cell</field>
                                                                                                    <statement name="body">
                                                                                                      <block type="image_shape" id="_!Cx8#$]MEnT@2^i=1YF)">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_position" id="Kx8.bX/Dy0CHt[q,P#~q">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="oP|b862Z0as]dyadpxNi">
                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="iu//FyxWyr$F%x^27KnQ">
                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_size" id="(}-}^^$FmX{I{^lWjiqa">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="width">
                                                                                                                  <block type="expression" id="hrIIdJ#l{K-jyF};c$19">
                                                                                                                    <field name="value">240</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="height">
                                                                                                                  <block type="expression" id="!fe725))@2cfw.74BB@1P;">
                                                                                                                    <field name="value">220</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_image_key" id="H!Eh#nrj0l8:3yMEYNKh">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="key">
                                                                                                                      <block type="string_value" id="nj9%K)!)tvQ)?/=}-!-:">
                                                                                                                        <field name="value">box.png</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_src" id="WntF@2o?MDNxRJD`?I)/i">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="src">
                                                                                                                          <block type="string_value" id="w6T=Cn$!;_)9pmJeVX]-">
                                                                                                                            <field name="value">${image_path}box.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="if_then_else_block" id="XC$1g@18b7ONK{F{X2loj">
                                                                                                            <value name="if">
                                                                                                              <block type="expression" id="oQFwzwo2D;+c?CON=pWH">
                                                                                                                <field name="value">$cell.data == 90</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <statement name="then">
                                                                                                              <block type="image_shape" id="(^`N,lKpoJI[DKN5E?fR">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_position" id="$NKhS!jz_YUsOazNwF:m">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="^qIBD4UAKRt3uyCa@1M)2">
                                                                                                                        <field name="value">$cell.centerX - 70</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="@2Q+9C|WzvSugr=IW.It!">
                                                                                                                        <field name="value">$cell.centerY + 60</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_key" id=";76DR7U3P|R:nv%z@1">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="key">
                                                                                                                          <block type="string_value" id="7,J]dcp!/N=k`egU6%.=">
                                                                                                                            <field name="value">sq.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_src" id="DQ/syGE4S{eSPrR_]qbo">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="src">
                                                                                                                              <block type="string_value" id="6((b3K5@1u4@2K6}.DgH{r">
                                                                                                                                <field name="value">${image_path}sq.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <statement name="else">
                                                                                                              <block type="image_shape" id="R~c#G@2o-cE[zDQ]2u~/j">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_position" id="?Z9=NM!QBrtdEU)rDlHC">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="^gQf?g%Y+4gi8^B}|x_4">
                                                                                                                        <field name="value">$cell.centerX - 70</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="DZ`)FjJba?$.:(Q!S+o:">
                                                                                                                        <field name="value">$cell.centerY + 60</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_key" id="Ut#)FKn!~?Oj$r-q2Wj0">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="key">
                                                                                                                          <block type="string_value" id="qoXy4A2/0dDHa!Hc@1xFa">
                                                                                                                            <field name="value">cl.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_src" id="5e^IP+}U~MV3!2)|$7l9">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="src">
                                                                                                                              <block type="string_value" id="tBN8,}8qQ?MebRu4/7_6">
                                                                                                                                <field name="value">${image_path}cl.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_angle" id="@2PlAh$9ssc+5[7E4+vlR">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="angle">
                                                                                                                                  <block type="expression" id="R,D/qfnOffwU_}ex@1euR">
                                                                                                                                    <field name="value">360 - $cell.data</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="image_shape" id="{Ba2ypy=uDVvj:YOWabP">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_position" id="gy8=[Gjv)RQ7H:Q:F~Hu">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="MQHla1GBg$e@2gtjaQWv,">
                                                                                                                        <field name="value">$cell.centerX - 70</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="`a.{bi_z.jR]jO6hw7dW">
                                                                                                                        <field name="value">$cell.centerY + 60</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_key" id="#o/YLFSHz0/REba,3eeO">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="key">
                                                                                                                          <block type="string_value" id="kW[I5k|_BL`KpCC+/l7G">
                                                                                                                            <field name="value">liner.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_src" id="noDr6@1|MuTeaNQr36Nvh">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="src">
                                                                                                                              <block type="string_value" id=",|@1d/r^matCX)`[-MY@2l">
                                                                                                                                <field name="value">${image_path}liner.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="image_shape" id="Nw|98#[REYjsCnpmw][0">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_position" id="mjUZY}fA.:1ihydR@1@2cD">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="OwOirD%MX:KD_O,6N3=P">
                                                                                                                            <field name="value">$cell.centerX - 70</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="cJ|Gqy{VodD8w`_QBx`O">
                                                                                                                            <field name="value">$cell.centerY + 60</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_key" id="BGx[j%lbrS8e+r}-yej]">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="key">
                                                                                                                              <block type="string_value" id="F@1ET;S%h{cQOHHKQXvm%">
                                                                                                                                <field name="value">linel.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_src" id="P2yIL@2x^d%6RRmh^x#9y">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="src">
                                                                                                                                  <block type="string_value" id="ec],n@2tHLLta0i.}W^-9">
                                                                                                                                    <field name="value">${image_path}linel.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_angle" id=")k/uvtQNg){@1NxfEV9(1">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="angle">
                                                                                                                                      <block type="expression" id="$aVBTFW~M.?zN(XJg/1r">
                                                                                                                                        <field name="value">180 - $cell.data</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="image_shape" id="D[UA}TR@12ttbioohuTx;">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="rSpmopD^0CAdi/26.k$;">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="-I^YUiLz+^YyLHMrO`2|">
                                                                                                                                <field name="value">$cell.centerX - 70</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="H@2?[0fT@1O46h[[r,9Fw-">
                                                                                                                                <field name="value">$cell.centerY + 60</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_key" id="_{(}MkF}@2$aI{+{TyZAo">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="key">
                                                                                                                                  <block type="string_value" id="B]6d{=_el`C?g#BO@1%:z">
                                                                                                                                    <field name="value">point.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_src" id="h$Q/Y()3]Fl0D/K!FIU$">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="src">
                                                                                                                                      <block type="string_value" id="mE;$~g3-zUuEjy[d?!l+">
                                                                                                                                        <field name="value">${image_path}point.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="grid_shape" id="SmAI5@2msssytd8RXGWD-">
                                                                        <statement name="#props">
                                                                          <block type="prop_grid_dimension" id="C+?7n@2ZIw647(S$bdNz!">
                                                                            <field name="#prop"></field>
                                                                            <value name="rows">
                                                                              <block type="expression" id="(Br=N0:}q[U_9i#-M#{Z">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="cols">
                                                                              <block type="expression" id="F;6~lCYe[PMAGo;tXIJ9">
                                                                                <field name="value">3</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_position" id=";U|-QJ1QI+_w+;M2RB_I">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="RGw1CFlUMRN0bw+7Rvaz">
                                                                                    <field name="value">400</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="QcIr|^maD6|F{?$X|qM5">
                                                                                    <field name="value">300</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_size" id="mLqu^OKI7Nh7raq/k84u">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="width">
                                                                                      <block type="expression" id="P#-Ww$sgaz}|8mn)I=nB">
                                                                                        <field name="value">780</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="height">
                                                                                      <block type="expression" id="GP(VbK_Z@2!mQq:bQx#)V">
                                                                                        <field name="value">200</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_anchor" id="Ix}sRD?92CTK@2oS^Pc-B">
                                                                                        <field name="#prop">anchor</field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="/RvMr{g)vjSW::!.Za;V">
                                                                                            <field name="value">0.5</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="PATD8d32[AEUl}|ZV.%e">
                                                                                            <field name="value">0.5</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_grid_cell_source" id="mk#hQCDQ8=1p?mts:fdG">
                                                                                            <field name="#prop">cell.source</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id=",N0@204aAz0xnLGb-|hrg">
                                                                                                <field name="value">angle</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_grid_random" id="a.|W?29twp{|%P(JVey9">
                                                                                                <field name="#prop"></field>
                                                                                                <field name="random">FALSE</field>
                                                                                                <next>
                                                                                                  <block type="prop_grid_show_borders" id="G;pdOvn[d2.e[@1%iozel">
                                                                                                    <field name="#prop">#showBorders</field>
                                                                                                    <field name="value">FALSE</field>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_cell_template" id="bcAMnR[FbX$K:@2z;eu(M">
                                                                                                        <field name="variable">$cell</field>
                                                                                                        <field name="#prop">cell.template</field>
                                                                                                        <field name="#callback">$cell</field>
                                                                                                        <statement name="body">
                                                                                                          <block type="choice_custom_shape" id="0~e++_lJwIi@2e59wsgwZ">
                                                                                                            <field name="action">click-one</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="RxzNBMYolBajvDpocI)l">
                                                                                                                <field name="value">$cell.data</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <statement name="template">
                                                                                                              <block type="image_shape" id=".$@1P/hg+Ob6Ze1axWddS">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_position" id="@1osfv|w@1X[wlw{x#][(H">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="bKfGv?kRcXO:wk7gY|Xn">
                                                                                                                        <field name="value">$cell.centerX</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="4Htz!$9C.=~I3]3?8![1">
                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_size" id="w+}O0#.s$Ee@2R$tS#j|I">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="width">
                                                                                                                          <block type="expression" id="[RXE~Fd$6(%SXYv{FP^I">
                                                                                                                            <field name="value">240</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="height">
                                                                                                                          <block type="expression" id="x@2GyF=!:W9Wd.$E7n@1J!">
                                                                                                                            <field name="value">220</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_key" id="{F1Af^eoPh2sVw_|g8cv">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="key">
                                                                                                                              <block type="string_value" id="ZrpW@1+p@2BuuLxO3):11$">
                                                                                                                                <field name="value">hide.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_src" id="`/A,I,b-uL(Pw@2I)@1waa">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="src">
                                                                                                                                  <block type="string_value" id="!{MAQ,:6Q87_`^E|ePgM">
                                                                                                                                    <field name="value">${image_path}hide.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="choice_custom_shape" id=")VsH;/gH-1oZTAJh[WjZ">
                                                                            <field name="action">drag</field>
                                                                            <value name="value">
                                                                              <block type="expression" id="bPk{5{d^4^CUAFj1yEdt">
                                                                                <field name="value">undefined</field>
                                                                              </block>
                                                                            </value>
                                                                            <statement name="template">
                                                                              <block type="image_shape" id="L;z:=V)4krx_[lzu?QY_">
                                                                                <statement name="#props">
                                                                                  <block type="prop_position" id="!pn@2R4}/L0[XKuTZuVZb">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="B+zVDpfr1TNPmM8:AZ;d">
                                                                                        <field name="value">200</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="kP=,tuD8?~$VLI`VyF9e">
                                                                                        <field name="value">5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_anchor" id="Xo-D.UZyjFm50/b([?/T">
                                                                                        <field name="#prop">anchor</field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="NeAD8@1r,+(TIciU(hBLB">
                                                                                            <field name="value">0.5</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="veeoF}Nj}gN3XH4tm@2lw">
                                                                                            <field name="value">0</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_max_size" id="Q!1:6!bEMtH+%{v/(3b_">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="maxWidth">
                                                                                              <block type="expression" id="QEG-1WO#C%Xrl|C}]:%$">
                                                                                                <field name="value">300</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="maxHeight">
                                                                                              <block type="expression" id="#Yg4a5`)MKu@1OtamZ">
                                                                                                <field name="value">0</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_image_key" id="1oGJ`jL.gKgkf[3RY[3B">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="key">
                                                                                                  <block type="string_value" id="NsAP|bPHY=oW`_@18B~T6">
                                                                                                    <field name="value">pro.png</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_image_src" id="ss5^-]bTi_Y#I1`LqNPf">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="src">
                                                                                                      <block type="string_value" id="KcX|~iQBu{9NRlDfO9^]">
                                                                                                        <field name="value">${image_path}pro.png</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="func_add_answer" id="SWk`z?10./oV2Y_yOq.X" inline="true">
                                                                                <value name="value">
                                                                                  <block type="expression" id="4FYtlQjcI@1}l2TE7jeS^">
                                                                                    <field name="value">a</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </statement>
                                                    <next>
                                                      <block type="if_then_block" id="xHxG,3-?P!PYs3o=0u^@2">
                                                        <value name="if">
                                                          <block type="expression" id="{j`;M/lyr=@1l+L}t:a">
                                                            <field name="value">type == 2</field>
                                                          </block>
                                                        </value>
                                                        <statement name="then">
                                                          <block type="variable" id="`IP}L,,I1M1G`X_ty7va">
                                                            <field name="name">a</field>
                                                            <value name="value">
                                                              <block type="random_one" id=":?3|1XG?[w3}#SjDp?#?">
                                                                <value name="items">
                                                                  <block type="expression" id="Hk-V?xV1m!XLWa;])_ZQ">
                                                                    <field name="value">list_angle</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="text_shape" id="z]HcULE%VOp/sax1!0uv">
                                                                <statement name="#props">
                                                                  <block type="prop_position" id="8E@2H5JM?kZjrmjG-2?,~">
                                                                    <field name="#prop"></field>
                                                                    <value name="x">
                                                                      <block type="expression" id="G@1]8`X;F=E,#u/Gf:fT.">
                                                                        <field name="value">400</field>
                                                                      </block>
                                                                    </value>
                                                                    <value name="y">
                                                                      <block type="expression" id="D@29o:wsPkhsGlFT8xEU}">
                                                                        <field name="value">40</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="prop_anchor" id="]gw7wg;$CE$TsdDb.QgG">
                                                                        <field name="#prop">anchor</field>
                                                                        <value name="x">
                                                                          <block type="expression" id=",?cv4V:ZL.0wtsrOwDZz">
                                                                            <field name="value">0.5</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id="Rwq-X94XI-ljDoBS8f6D">
                                                                            <field name="value">0.5</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_text_contents" id="~D01Pos@14fWm]|~fqxX,">
                                                                            <field name="#prop"></field>
                                                                            <value name="contents">
                                                                              <block type="string_value" id="epejIMo=cB3-@1vSIZG5)">
                                                                                <field name="value">${ type == 2 ? 'What is the size of the angle?' : 'Find the unknown angle.'}</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_text_style" id="}se0C#(._s0|/@12])^q^">
                                                                                <field name="base">text</field>
                                                                                <statement name="#props">
                                                                                  <block type="prop_text_style_font_size" id="B;)F9lEHvypnhpB7Dun^">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="fontSize">
                                                                                      <block type="expression" id="0RQ/QE{wj!Gq_J4{{/-)">
                                                                                        <field name="value">35</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_text_style_fill" id="R}eRnWKi|4g.!dVp@2BEW">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="fill">
                                                                                          <block type="string_value" id="0,OJ=@1Yq-bebTPG)z9C6">
                                                                                            <field name="value">black</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_style_stroke" id="rmvSkn5G^/rJh]7#o;|]">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="stroke">
                                                                                              <block type="string_value" id="0#7)tkAId|Wa[hx756C6">
                                                                                                <field name="value">white</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_style_stroke_thickness" id="=lI5N;Cfe|!/=#)n4bU,">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="strokeThickness">
                                                                                                  <block type="expression" id="(ah!oxuCJmxh/){=;nDs">
                                                                                                    <field name="value">2</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style_wordwrap" id="I,2|Rr#B7Mc`e^.JF0}F">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="wordWrap">
                                                                                                      <block type="expression" id="FrjUkUz{D#NGVj@25r:Gi">
                                                                                                        <field name="value">true</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style_wordWrapWidth" id="h..xaiUAK9K5(H@2IGE}!">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="wordWrapWidth">
                                                                                                          <block type="expression" id="}@1A!@20F1Z8|_.Txk#R34">
                                                                                                            <field name="value">500</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <next>
                                                                  <block type="choice_custom_shape" id="2!QEk.iqF35(4#b1O[ZC">
                                                                    <field name="action">drag</field>
                                                                    <value name="value">
                                                                      <block type="expression" id="Lx?mT#uoj/E[KCq?gdS@1">
                                                                        <field name="value">undefined</field>
                                                                      </block>
                                                                    </value>
                                                                    <statement name="template">
                                                                      <block type="image_shape" id="DF)mTKwrQMv(DTsUzZZK">
                                                                        <statement name="#props">
                                                                          <block type="prop_position" id="{hR(8e3S#%6gjE1UYr3:">
                                                                            <field name="#prop"></field>
                                                                            <value name="x">
                                                                              <block type="expression" id="TGxIf`)!=A6KDV@2n9o9h">
                                                                                <field name="value">600</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="y">
                                                                              <block type="expression" id="|2nDR|fyP+~+Y^x4]6@2@2">
                                                                                <field name="value">380</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_anchor" id="P+-HJDvfU+URT)Fv^4t)">
                                                                                <field name="#prop">anchor</field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="ad%#:g!f7yY;OSfc}}In">
                                                                                    <field name="value">0.5</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="A:l).$)I`xh38`;IrJFu">
                                                                                    <field name="value">1</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_max_size" id="!4=ao8|Or_:D?@1_:I;PT">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="maxWidth">
                                                                                      <block type="expression" id="AlfSG3#PnW3~uU#2SDQ_">
                                                                                        <field name="value">300</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="maxHeight">
                                                                                      <block type="expression" id="tr+H;t}q.RMv#MJ8]}HA">
                                                                                        <field name="value">0</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_image_key" id="p`4f[o_,@12L9H8ZeS0X?">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="key">
                                                                                          <block type="string_value" id="JeW5twm1/Q}CtXt_+#Im">
                                                                                            <field name="value">pro.png</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_image_src" id="XF^jLq7qku!=;Zo)so#f">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="src">
                                                                                              <block type="string_value" id="{AJA?Z3Y,h_=YG[rWhGV">
                                                                                                <field name="value">${image_path}pro.png</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="grid_shape" id=")HS(ng1[i`R)2_/MDM0F">
                                                                        <statement name="#props">
                                                                          <block type="prop_grid_dimension" id="|i|+gB/dp_6PI32.(eOG">
                                                                            <field name="#prop"></field>
                                                                            <value name="rows">
                                                                              <block type="expression" id="B`$#^$ZuLlj+fRye2#Sv">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="cols">
                                                                              <block type="expression" id=";,=-5g(WgluMt[u^%?o$">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_position" id="X+@2NS5u5@2`:!mt||H-/7">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="t@1c,Dy4hUVl#NvmnO:a3">
                                                                                    <field name="value">250</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="jp12pnKU_%@2IgM+_[,jk">
                                                                                    <field name="value">200</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_size" id="1nTz1`n#E?9^(Ss[%[P^">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="width">
                                                                                      <block type="expression" id="z@2D%HkFym{Nm1k9dyDS#">
                                                                                        <field name="value">300</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="height">
                                                                                      <block type="expression" id="s%Xvx/~vP4C_~3_.m]CN">
                                                                                        <field name="value">200</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_anchor" id="1Gdw%[A+62;5~jW_?I;r">
                                                                                        <field name="#prop">anchor</field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="ZXJ[g:Qmme^),`{2afpU">
                                                                                            <field name="value">0.5</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="0Y4R0),MdExc[H#2V3!(">
                                                                                            <field name="value">0.5</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_grid_cell_source" id="Z0CwMs=Y7ke+!hAVU[WL">
                                                                                            <field name="#prop">cell.source</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="]UD$3$kwmv3eyVWS+9De">
                                                                                                <field name="value">1</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_grid_random" id="8k~wfn-ck{bOn89/_;@2g">
                                                                                                <field name="#prop"></field>
                                                                                                <field name="random">FALSE</field>
                                                                                                <next>
                                                                                                  <block type="prop_grid_show_borders" id="WK$.qJf%MTJ0_)+5Yl/3">
                                                                                                    <field name="#prop">#showBorders</field>
                                                                                                    <field name="value">FALSE</field>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_cell_template" id="d+-^o|6n}cA86LiB-XeM">
                                                                                                        <field name="variable">$cell</field>
                                                                                                        <field name="#prop">cell.template</field>
                                                                                                        <field name="#callback">$cell</field>
                                                                                                        <statement name="body">
                                                                                                          <block type="image_shape" id="s59es+y~ucVWaMC98If4">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_position" id="SSUjaYKyuS1+`la,[z%E">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="r(^32G[?K9bEG{#V:#J;">
                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="Wi4A3@17pm#$_Z#AmWp6[">
                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_size" id="d9)RYtJNT;4kXr=woF?_">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="width">
                                                                                                                      <block type="expression" id="6?3V(Xb)B7`q=@13.6Ow,">
                                                                                                                        <field name="value">240</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="height">
                                                                                                                      <block type="expression" id="g-s}TkN+DVs1dZTzBbnM">
                                                                                                                        <field name="value">230</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_key" id=")bV7GbKs@1oL=?9+cS?}~">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="key">
                                                                                                                          <block type="string_value" id="gYng!/fgk4F{+~1+y?2u">
                                                                                                                            <field name="value">box.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_src" id="i%#wD.vJO`]l:ZKcwLbp">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="src">
                                                                                                                              <block type="string_value" id="cS#]B#Ax+rW32},zHo{,">
                                                                                                                                <field name="value">${image_path}box.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="image_shape" id="Q+),z`+]S-W_wZq^8I^=">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_position" id="Nqi35%|lgC`2sewbx;]_">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="h1@2.GU1a?YLTQTnC93=e">
                                                                                                                        <field name="value">$cell.centerX - 70</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="[MdS3p$6=S;uK)gOhh=/">
                                                                                                                        <field name="value">$cell.centerY + 60</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_key" id="j6m;90d:5nbBcVDL(,V,">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="key">
                                                                                                                          <block type="string_value" id="aP|9t,HN^D7UP,Bl6gj)">
                                                                                                                            <field name="value">cl.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_src" id="WVkZe^T_ZCiRw(N}5FMQ">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="src">
                                                                                                                              <block type="string_value" id="@25hm(F|ndIBg!nQjBeGj">
                                                                                                                                <field name="value">${image_path}cl.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_angle" id="Zf+oP4j5R~n4wzxUf3B%">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="angle">
                                                                                                                                  <block type="expression" id="p8#E=4I|z}]Z#lGJe%[X">
                                                                                                                                    <field name="value">360 - a</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="image_shape" id="IXNc=}.{$!`|]L=Bbwar">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_position" id="F.:i}$6B?_{WgZsDpjvk">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="yUVd;`F/n~5q?pLmD9~H">
                                                                                                                            <field name="value">$cell.centerX - 70</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="weNVWgSAE`_8U@28a%7MX">
                                                                                                                            <field name="value">$cell.centerY + 60</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_key" id="y3m9:#4F:J$NAw{h=(;O">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="key">
                                                                                                                              <block type="string_value" id="Q6uh[]NnTS6kzZ^xb5i$">
                                                                                                                                <field name="value">liner.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_src" id="[rc%xa@1]txYA,n5Sj_g.">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="src">
                                                                                                                                  <block type="string_value" id="GQRzq.ydq+a4`4Q3:CUS">
                                                                                                                                    <field name="value">${image_path}liner.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="image_shape" id="?WzW=L:qwmSh@1LLQb$%I">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="?I8[_xGR7JT6br7)8x^~">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="3B(9Y9rI`2LRHav#H[-k">
                                                                                                                                <field name="value">$cell.centerX - 70</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="tF_^lbN6ud!+esmK[3aN">
                                                                                                                                <field name="value">$cell.centerY + 60</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_key" id="@1w)tdV?NAIfB=Y$@1?)-`">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="key">
                                                                                                                                  <block type="string_value" id="0lSHFqx,(YB7SZEkHZ1_">
                                                                                                                                    <field name="value">linel.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_src" id="vN={L#}rZu,Xr(I{cRJc">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="src">
                                                                                                                                      <block type="string_value" id="X+XZ3EuXRKxC?NJhFKFF">
                                                                                                                                        <field name="value">${image_path}linel.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_angle" id="#[pY4w@19evse:fWR6}:r">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="angle">
                                                                                                                                          <block type="expression" id="G~F^_yqN4OhzbQLduTn}">
                                                                                                                                            <field name="value">180 - a</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="image_shape" id="iWIEYp#75qruJu:ZeHYF">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="/S7Y~fSzB,$E+/@1qq{nb">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="(YXP0%lm/m~(QPZ=Y5]3">
                                                                                                                                    <field name="value">$cell.centerX - 70</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="ZSJQ[DA):npIKMkV2moV">
                                                                                                                                    <field name="value">$cell.centerY + 60</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_key" id="@1}vWJlk.GfF%/(]HOE=@1">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="key">
                                                                                                                                      <block type="string_value" id="72k6UY$#8S79SE,k5e%F">
                                                                                                                                        <field name="value">point.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_src" id="gCRj_Q^EFfNIIFj-A{I=">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="src">
                                                                                                                                          <block type="string_value" id="hI2A~q))zsQG7Poin24A">
                                                                                                                                            <field name="value">${image_path}point.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="image_shape" id="{b@2OG.JZavV~!`rv3h,I">
                                                                            <statement name="#props">
                                                                              <block type="prop_position" id="[B^jsIm}cDI%eS~X|CA0">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="V8m,+{_Oyqo`P-DBVRT1">
                                                                                    <field name="value">600</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="/D6H^Dj.y4Cj%kp?$VI.">
                                                                                    <field name="value">150</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_image_key" id="ln9i5%#@2pSQ`oFDu/eDA">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="key">
                                                                                      <block type="string_value" id="TI^V0#)vQ7LQ-,Nt7o7=">
                                                                                        <field name="value">shape.png</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_image_src" id="c%l{]rF1Smzp{r5vVkQ3">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="src">
                                                                                          <block type="string_value" id="^R#Z?CH=A!|5q[W4Umt=">
                                                                                            <field name="value">${image_path}shape.png</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="algorithmic_input_shape" id="[e}LcmqF@19.%T?]@1jB-Z">
                                                                                <statement name="#props">
                                                                                  <block type="prop_value" id="TN6clfe}:4H;@1k0w%+`S">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="@1Y0=VVDuz^gR8?]A.GgV">
                                                                                        <field name="value">a</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_position" id="(u^k=nloWKrc75-ihkQ6">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="+SQo+%UqGFz6@107iOi%4">
                                                                                            <field name="value">600</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="aOD#P0{Z$l:BCNh)9@2~E">
                                                                                            <field name="value">150</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_size" id="~)S$^+Glx|;{-)@1m?MCt">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="width">
                                                                                              <block type="expression" id="wdRRw?lccA]3huvks^0:">
                                                                                                <field name="value">125</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="height">
                                                                                              <block type="expression" id="FlQ@1R-hpt5|W3YEsjl5h">
                                                                                                <field name="value">68</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_input_keyboard" id="pX[+1]_P03Uw?r6RhxOm">
                                                                                                <field name="#prop"></field>
                                                                                                <field name="keyboard">numbers1</field>
                                                                                                <next>
                                                                                                  <block type="prop_input_max_length" id="e^196@2S|6ukB7~2Bxi9C">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="maxLength">
                                                                                                      <block type="expression" id="/_E;-7``ONG+]nS_NxH+">
                                                                                                        <field name="value">a.toString().length</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_input_result_position" id="#LH4fkex}OZhG#[@2d;3P">
                                                                                                        <field name="#prop"></field>
                                                                                                        <field name="resultPosition">bottom</field>
                                                                                                        <next>
                                                                                                          <block type="prop_tab_order" id="s|F@1Izb{|@2lRY,asD!hu">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="tabOrder">
                                                                                                              <block type="expression" id="2[il+_dgVQKQm?SvtFDj">
                                                                                                                <field name="value">0</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_stroke" id="D`nCliS/s1emx9euC0}t">
                                                                                                                <field name="#prop">stroke</field>
                                                                                                                <next>
                                                                                                                  <block type="prop_fill" id="Dz-AaH/!wx?BO[MH9dIz">
                                                                                                                    <field name="#prop">fill</field>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style" id="!1Qr3{IxD@1PMJ]G^LlcN">
                                                                                                                        <field name="base">text</field>
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_text_style_font_size" id="!(nlSpda9T+o/iXBj}YF">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="fontSize">
                                                                                                                              <block type="expression" id="FQTZZB_+$@1-hM?K.!NSO">
                                                                                                                                <field name="value">32</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_fill" id="w-4`M-zsemYrO[}g`_|q">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="fill">
                                                                                                                                  <block type="string_value" id="G;VmNGg}u8OZR_#3M_Oe">
                                                                                                                                    <field name="value">black</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_stroke" id="ahppz0%Y{eD@2C$!vyJ4m">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="stroke">
                                                                                                                                      <block type="string_value" id="}KNcD$1%M)+@1a=7A%7,,">
                                                                                                                                        <field name="value">white</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="TBWlBG{V@2q@2LOjXi7|@16">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="strokeThickness">
                                                                                                                                          <block type="expression" id="bs_awM@2W;X@1OG#fQ+@2no">
                                                                                                                                            <field name="value">2</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </statement>
                                                        <next>
                                                          <block type="if_then_block" id="h%eWN%nUa3RRbPJi}~Hb">
                                                            <value name="if">
                                                              <block type="expression" id=";rv:SA+gU|^MMvRN9o09">
                                                                <field name="value">type == 3</field>
                                                              </block>
                                                            </value>
                                                            <statement name="then">
                                                              <block type="do_while_block" id="/tcKB@2pA]T?:7~$VK6Mc">
                                                                <statement name="do">
                                                                  <block type="variable" id="Pi^ZEukDpzwB0p-0EgqI">
                                                                    <field name="name">a</field>
                                                                    <value name="value">
                                                                      <block type="random_one" id=")-g@2aow@1=oY_@2@2gxLx:L">
                                                                        <value name="items">
                                                                          <block type="expression" id="M4Mim:mw$m)(#l9PkKNS">
                                                                            <field name="value">list_angle</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </value>
                                                                  </block>
                                                                </statement>
                                                                <value name="while">
                                                                  <block type="expression" id="_!SB=pm$la#natk;[;i~">
                                                                    <field name="value">a &gt; 70</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="text_shape" id="V3=_@2Lx7VS-aLT@1@2XuHJ">
                                                                    <statement name="#props">
                                                                      <block type="prop_position" id="w9pd2V[PCyJx@25B-tA{P">
                                                                        <field name="#prop"></field>
                                                                        <value name="x">
                                                                          <block type="expression" id="xLNDy6c6c}Ll6`Z8BRqD">
                                                                            <field name="value">400</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="y">
                                                                          <block type="expression" id="gAeDF)!75I!z1V@1hg=UJ">
                                                                            <field name="value">40</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="prop_anchor" id="FgEl;VBx(L1S$mBr5CkM">
                                                                            <field name="#prop">anchor</field>
                                                                            <value name="x">
                                                                              <block type="expression" id="XA9jMNkrq+]a|Qd@2!|n3">
                                                                                <field name="value">0.5</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="y">
                                                                              <block type="expression" id="1|TCi0tVXGNc3}?CQYcd">
                                                                                <field name="value">0.5</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_text_contents" id="$6IO]|4I=y-J5Gq}k+?L">
                                                                                <field name="#prop"></field>
                                                                                <value name="contents">
                                                                                  <block type="string_value" id=".xO,|e($J$pNA(-?aj./">
                                                                                    <field name="value">${ type == 2 ? 'What is the size of the angle?' : 'Find the unknown angle.'}</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_text_style" id="if{{3pIAs3gGDyo@1gMNK">
                                                                                    <field name="base">text</field>
                                                                                    <statement name="#props">
                                                                                      <block type="prop_text_style_font_size" id="}`QGj).t$!9`.l^7{{!:">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="fontSize">
                                                                                          <block type="expression" id="6.~B)`},RkYoh%X=rO5_">
                                                                                            <field name="value">35</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_style_fill" id="B?jz(K2)illNl.V;qDjM">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="fill">
                                                                                              <block type="string_value" id="?CMja#f^S{@2dg2bd};Cb">
                                                                                                <field name="value">black</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_style_stroke" id="iuZuihS/Te(KEn%t=WXP">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="stroke">
                                                                                                  <block type="string_value" id="kDByinH1Z|BaqyHMhp:0">
                                                                                                    <field name="value">white</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style_stroke_thickness" id="0P1:V:!t}u-l@2[k[|i`v">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="strokeThickness">
                                                                                                      <block type="expression" id="uzyIQbTj^$LgQScDM1+j">
                                                                                                        <field name="value">2</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style_wordwrap" id="q0=BxZ#IbYv{bvx)rU+U">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="wordWrap">
                                                                                                          <block type="expression" id="!a6ygUPiIqDTP42.Sacd">
                                                                                                            <field name="value">true</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style_wordWrapWidth" id="ux1Q@2~b}_ThzZ)ӿE=">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="wordWrapWidth">
                                                                                                              <block type="expression" id="_.@1`kn~Iq`yyNh~0NCup">
                                                                                                                <field name="value">500</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="grid_shape" id="C3x|9I]Nx`:IYv]wv4iw">
                                                                        <statement name="#props">
                                                                          <block type="prop_grid_dimension" id="x(I^cei=oBRP^En5G_@1n">
                                                                            <field name="#prop"></field>
                                                                            <value name="rows">
                                                                              <block type="expression" id="nK)Pp%Y$wjdaw,jTysP{">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="cols">
                                                                              <block type="expression" id="-B]n%#DuXm9cPx+,5BgQ">
                                                                                <field name="value">1</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="prop_position" id="wge:SJjUj!izSgrVh8iW">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="/0Uy=icN6B|m$8YOd._7">
                                                                                    <field name="value">250</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id=":0@1@1@1[fb=89]E@2#1E.PO">
                                                                                    <field name="value">200</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_size" id="zKosw}ps;iGxw`T$LRFO">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="width">
                                                                                      <block type="expression" id="nh1av{5:7uRzRo=yeWP7">
                                                                                        <field name="value">300</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="height">
                                                                                      <block type="expression" id="mBB%KBV]GJmB;4XD9)@1G">
                                                                                        <field name="value">200</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_anchor" id=";/@2_~WT4@1oE^RhW%d(j9">
                                                                                        <field name="#prop">anchor</field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id=":.+%bl;Q71ID0F=WX-G%">
                                                                                            <field name="value">0.5</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="CB{$/h/@2!86YcBYRfH9z">
                                                                                            <field name="value">0.5</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_grid_cell_source" id="MEh+f;|+!5-|zZ@1(I@2@1}">
                                                                                            <field name="#prop">cell.source</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="~eXF_Nd{r!oe@2)jqn~5[">
                                                                                                <field name="value">1</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_grid_random" id="N%KJie#Ui:qBRVgUfRl@2">
                                                                                                <field name="#prop"></field>
                                                                                                <field name="random">FALSE</field>
                                                                                                <next>
                                                                                                  <block type="prop_grid_show_borders" id="oU_VfSYH[,_M@2}rD(N5g">
                                                                                                    <field name="#prop">#showBorders</field>
                                                                                                    <field name="value">FALSE</field>
                                                                                                    <next>
                                                                                                      <block type="prop_grid_cell_template" id="yonjd.ev!`)`j:Ut|GnN">
                                                                                                        <field name="variable">$cell</field>
                                                                                                        <field name="#prop">cell.template</field>
                                                                                                        <field name="#callback">$cell</field>
                                                                                                        <statement name="body">
                                                                                                          <block type="image_shape" id="S7|K-,0)qVgWi.=xvqz,">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_position" id="QCzMm4oTrKjr]xd@2vFu1">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="N8BKcR0;l_}Z5_w}1{C$">
                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="}[AaQ,qg}E[r`PkM^yw#">
                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_size" id="`$07S6tX5f$[7@25k:%/Q">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="width">
                                                                                                                      <block type="expression" id="|$v`#_M=pK@1+i$[yFAZ`">
                                                                                                                        <field name="value">240</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="height">
                                                                                                                      <block type="expression" id="Qb]c7-8Or9u[`kGfel-4">
                                                                                                                        <field name="value">230</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_key" id="9/88/5)t~x^r3]THRwkw">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="key">
                                                                                                                          <block type="string_value" id="s0w:UMQ/DM5|Fgd8xm?y">
                                                                                                                            <field name="value">box.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_src" id="m.(~Ei7z)zB!)L(G~[nj">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="src">
                                                                                                                              <block type="string_value" id="bN@24MRoq8n}J-XMIOGAU">
                                                                                                                                <field name="value">${image_path}box.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="image_shape" id="#XF:QD_}1^ZE1pNaD`%Y">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_position" id="j$99fva7oKN;HKHgzK~_">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="Ms!Ak{:@2%M~wepJp57z.">
                                                                                                                        <field name="value">$cell.centerX - 70</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="L$R`gz@2WRfFscJg4l@26-">
                                                                                                                        <field name="value">$cell.centerY + 60</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_key" id="v/SjeEbC[Cy.!|HJ]6%5">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="key">
                                                                                                                          <block type="string_value" id="UQLbD]YH.d7n`zu,c,U@1">
                                                                                                                            <field name="value">cl.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_src" id="!z74x8K~a4cstB]+u:Wl">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="src">
                                                                                                                              <block type="string_value" id="fqp]%Y?@1PyFrys#+O@2Bg">
                                                                                                                                <field name="value">${image_path}cl.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_angle" id="g#I5LR2L+.HA=8a{09e+">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="angle">
                                                                                                                                  <block type="expression" id="$l~pGgc8^exyJc|@1qwd`">
                                                                                                                                    <field name="value">360 - a</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="image_shape" id="pa=8{#5`tv-axS9cIf/[">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_position" id="W8E`F0$H|:YB`qSHz?2.">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="1R)9%2uqof?Bz]jt=dBX">
                                                                                                                            <field name="value">$cell.centerX - 70</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="QlM{zZkilh#ZJO[z{0;Z">
                                                                                                                            <field name="value">$cell.centerY + 60</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_key" id="AacfE!C@1d9H$zv^c)ObY">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="key">
                                                                                                                              <block type="string_value" id="hUMyLh`JQ(up.!iK_F:{">
                                                                                                                                <field name="value">cl4.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_src" id="$-gBz|(w=pu[|.yTb5$@1">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="src">
                                                                                                                                  <block type="string_value" id="(eeq}y3YV]s5c!^@2;W;h">
                                                                                                                                    <field name="value">${image_path}cl4.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_angle" id="Rx7G%7Y^4HJ,}9)_Da`7">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="angle">
                                                                                                                                      <block type="expression" id="b2[-|3Wh]W:[u$_k).+m">
                                                                                                                                        <field name="value">90 - a</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="image_shape" id="(efD@2$j7-6[fVbK=2tf/">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_position" id="b|@2?L/!h`o9f_KXKb%j6">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="YmnXo5V|%[:[L/y@2Sv:}">
                                                                                                                                <field name="value">$cell.centerX - 70</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="6vTKuO5cOv0M)7g^BaBa">
                                                                                                                                <field name="value">$cell.centerY + 60</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_image_key" id="Wty;,X;Uw5l_!k`Kz-eq">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="key">
                                                                                                                                  <block type="string_value" id="TQ;{zxC2fNqeSa]n4BbH">
                                                                                                                                    <field name="value">liner.png</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_src" id="Lf7ZW3Z#HmM(FDdM]4[/">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="src">
                                                                                                                                      <block type="string_value" id="Q/Jd{FA.awPo?|H;oG6d">
                                                                                                                                        <field name="value">${image_path}liner.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="image_shape" id="0LUc0D9L$8)}.pOCU`cg">
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_position" id="aJ)-lojrhJJnF_GD{i-J">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="^q|sHH{#FB;i{_;Lo3G`">
                                                                                                                                    <field name="value">$cell.centerX - 70</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="EqF:~Hp)G~y6E4h5|CRD">
                                                                                                                                    <field name="value">$cell.centerY + 60</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_image_key" id="+Rr2|qgAan$~~)Ios0uY">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="key">
                                                                                                                                      <block type="string_value" id=".v1=f9qyH@1(i8{Q0i1lm">
                                                                                                                                        <field name="value">linel.png</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_src" id="r;-l~uSDmx2WD4{T!KH$">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="src">
                                                                                                                                          <block type="string_value" id="~7Bo~5nuD:ron@1Nk%`+.">
                                                                                                                                            <field name="value">${image_path}linel.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_angle" id="N-;.mq#Fkh7,%3}]R`?%">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="angle">
                                                                                                                                              <block type="expression" id="07Tmn{vb2syu[vZF%MR.">
                                                                                                                                                <field name="value">180 - a</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                            <next>
                                                                                                                              <block type="image_shape" id="I:s:Tk3{U~r;iU|4[r{-">
                                                                                                                                <statement name="#props">
                                                                                                                                  <block type="prop_position" id="gXP+8Q}%Z4];T-8SrE5#">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="x">
                                                                                                                                      <block type="expression" id="fCm4,e.6Vk#n(;zIcdiZ">
                                                                                                                                        <field name="value">$cell.centerX - 70</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="y">
                                                                                                                                      <block type="expression" id="D,y,!Vzl{@1H,GU@1.T154">
                                                                                                                                        <field name="value">$cell.centerY + 60</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_image_key" id="w#]Hh-^voDw/Av(hS,]3">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="key">
                                                                                                                                          <block type="string_value" id="8WBOsg3l54_W(Z.?psO%">
                                                                                                                                            <field name="value">liner90.png</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_src" id="Dh6QPP$0s/K)v4I+nE?H">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="src">
                                                                                                                                              <block type="string_value" id="wYT;!HBBq8}H?|?g=UoT">
                                                                                                                                                <field name="value">${image_path}liner90.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <next>
                                                                                                                                  <block type="image_shape" id="-.y;vvD78|SQFVT#GE+q">
                                                                                                                                    <statement name="#props">
                                                                                                                                      <block type="prop_position" id="2gxT3w3hMirvP/=jb72d">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="expression" id=":M|]UzZm;]ldZ`7`|x#w">
                                                                                                                                            <field name="value">$cell.centerX - 70</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id="|Jb/j|,@1@2-?~G.BbI@1R[">
                                                                                                                                            <field name="value">$cell.centerY + 60</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_image_key" id="+@1GcX)wgF![ed.mgnr9:">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="key">
                                                                                                                                              <block type="string_value" id="),^?kP~WC@2}IEp=l$VG^">
                                                                                                                                                <field name="value">point.png</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_image_src" id="+EHAyy2Y0@2puyFp_@1{2V">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="src">
                                                                                                                                                  <block type="string_value" id="@1]mF^pTi/gn,,+G!rUNe">
                                                                                                                                                    <field name="value">${image_path}point.png</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="grid_shape" id="V#1!Q2D~4U;E;46;1yWH">
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_grid_dimension" id="d{KZF?ud`@2[]Nkn6J@1}h">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="rows">
                                                                                                                                              <block type="expression" id="MXsf}M,ssEqsCOL=%as=">
                                                                                                                                                <field name="value">1</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="cols">
                                                                                                                                              <block type="expression" id="Xu2uN;|M=jZH]kQS-k82">
                                                                                                                                                <field name="value">1</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_position" id="yE?B?k@1)7.6xyEi3T^F;">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="x">
                                                                                                                                                  <block type="expression" id="Q6W1B@1d+)SnjsdG[RrCq">
                                                                                                                                                    <field name="value">$cell.centerX-50</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <value name="y">
                                                                                                                                                  <block type="expression" id="LHPW)DRg~|!kQ4:}D_u2">
                                                                                                                                                    <field name="value">$cell.centerY - 40</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_size" id="kLzl`6mzg3d:vOs]={x}">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="width">
                                                                                                                                                      <block type="expression" id="KW]^txK!(B3I}#b(@2XkX">
                                                                                                                                                        <field name="value">50</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <value name="height">
                                                                                                                                                      <block type="expression" id=";dqQ@2d~,O}y/:@2A5JrP%">
                                                                                                                                                        <field name="value">30</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_anchor" id="+_dPCUv%gd8Rk+HCMex3">
                                                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                                                        <value name="x">
                                                                                                                                                          <block type="expression" id="WoHsH!On9}ZoB,URN:}:">
                                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="y">
                                                                                                                                                          <block type="expression" id="g[Ym7tUqE93Jfy,PCMAy">
                                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_grid_cell_source" id="cb`XLjq+%-zEHb=YgOg3">
                                                                                                                                                            <field name="#prop">cell.source</field>
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id="o$wt~lIw7q}ENEl(I^c/">
                                                                                                                                                                <field name="value">1</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_grid_random" id="L|V]Y,X1M@2OH%[Le,?H`">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <field name="random">FALSE</field>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_grid_show_borders" id="WMuO#$yS[l8_Cp_)xpd2">
                                                                                                                                                                    <field name="#prop">#showBorders</field>
                                                                                                                                                                    <field name="value">FALSE</field>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_grid_cell_template" id="MjC}Ita[UMS_8jha`,kL">
                                                                                                                                                                        <field name="variable">$cell</field>
                                                                                                                                                                        <field name="#prop">cell.template</field>
                                                                                                                                                                        <field name="#callback">$cell</field>
                                                                                                                                                                        <statement name="body">
                                                                                                                                                                          <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                              <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="x">
                                                                                                                                                                                  <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <value name="y">
                                                                                                                                                                                  <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="contents">
                                                                                                                                                                                      <block type="string_value" id="#6btV9p,PGV}wMx.U�">
                                                                                                                                                                                        <field name="value">${90-a}</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                                                                                                                                        <field name="base">text</field>
                                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                                          <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <value name="fontSize">
                                                                                                                                                                                              <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                                                                                                                                                <field name="value">36</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <value name="fill">
                                                                                                                                                                                                  <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                                                                                                                                                    <field name="value">black</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="prop_text_style_stroke" id="{I44R4!2.lcgY4)Ek(S@2">
                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                    <value name="stroke">
                                                                                                                                                                                                      <block type="string_value" id="waz}!_4MQm!(TGllb;j4">
                                                                                                                                                                                                        <field name="value">white</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="{hEHRoj9oo8r$Xg)tJ3P">
                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                        <value name="strokeThickness">
                                                                                                                                                                                                          <block type="expression" id="}qht(yHHZ|fQn;1fX#gR">
                                                                                                                                                                                                            <field name="value">2</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </statement>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </statement>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="text_shape" id="58#9[OVLO;K2jvd9pxQi">
                                                                                                                                                                                <statement name="#props">
                                                                                                                                                                                  <block type="prop_position" id="TUZr3^F4$Ekgb:6@1XW53">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="x">
                                                                                                                                                                                      <block type="expression" id="b|Yk^)2.W9!MsyDzQfQ$">
                                                                                                                                                                                        <field name="value">$cell.right</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <value name="y">
                                                                                                                                                                                      <block type="expression" id=")#_;Ox|F_;#]5kiR=QhV">
                                                                                                                                                                                        <field name="value">$cell.centerY-15</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_text_contents" id="B|I6EiN-sE}F3:Ac_S#t">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="contents">
                                                                                                                                                                                          <block type="string_value" id="#%q~@1J8~EZNixZ/qHY09">
                                                                                                                                                                                            <field name="value">o</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_text_style" id="CN?N{B]].bd|KD@1yYGx#">
                                                                                                                                                                                            <field name="base">text</field>
                                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                                              <block type="prop_text_style_font_size" id="GGHegr@2rBKA#QseN_KPa">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <value name="fontSize">
                                                                                                                                                                                                  <block type="expression" id="L_5B~TauVW=^(te5s;Go">
                                                                                                                                                                                                    <field name="value">24</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="prop_text_style_fill" id="_MXlB27n4ihTl,Dg$S`o">
                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                    <value name="fill">
                                                                                                                                                                                                      <block type="string_value" id="Z|QL2ix$+[/]R@2Xi8dZ7">
                                                                                                                                                                                                        <field name="value">black</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="prop_text_style_stroke" id="m,D1C9FyXG?[Q2#H$F9z">
                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                        <value name="stroke">
                                                                                                                                                                                                          <block type="string_value" id="q5R{]aBP,@1int^YPP+HR">
                                                                                                                                                                                                            <field name="value">white</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="prop_text_style_stroke_thickness" id="GzJryDCK[APuF`Z:k(zK">
                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                            <value name="strokeThickness">
                                                                                                                                                                                                              <block type="expression" id="Rw]jRfi(%,SVoM6/_M[7">
                                                                                                                                                                                                                <field name="value">2</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </statement>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </statement>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="image_shape" id="u#%wy/=e#Kn.{MTYo@2!d">
                                                                            <statement name="#props">
                                                                              <block type="prop_position" id="[JCnD5K3}4MOH(U@1Qqxh">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id="C2?X2cHzmR~i~Z~evE;|">
                                                                                    <field name="value">600</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="JUG(?ZMJ=6e:c|tuMILE">
                                                                                    <field name="value">250</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_image_key" id="}a`xqD0j@1+ab?6V)!{16">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="key">
                                                                                      <block type="string_value" id="lPx8|_y]hW/g+bY2S;!$">
                                                                                        <field name="value">shape.png</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_image_src" id="UuL?-+u-Oy=W`}3y_3x1">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="src">
                                                                                          <block type="string_value" id="o@2]/3bfEBPxDY,:025]+">
                                                                                            <field name="value">${image_path}shape.png</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="algorithmic_input_shape" id="SD_1@1#R!41z)p2DL[diW">
                                                                                <statement name="#props">
                                                                                  <block type="prop_value" id="K=]%??n/@1dyDpuZDvGO|">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="mw@1cWFTI%KS,`L/ak?sc">
                                                                                        <field name="value">a</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_position" id="nLq;hK(pV@1JV/b@1lzzvA">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="x">
                                                                                          <block type="expression" id="[Y:)A7?bQA?.Eym53BTI">
                                                                                            <field name="value">600</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="y">
                                                                                          <block type="expression" id="NWiA/(%($m9i65b.;sBw">
                                                                                            <field name="value">250</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_size" id="~~7L~-YfRP$1F|/:WaxE">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="width">
                                                                                              <block type="expression" id="7QJ.et~w28c}%MLfzDI8">
                                                                                                <field name="value">125</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="height">
                                                                                              <block type="expression" id="Q|Jjl3D~{/AH6016Yys#">
                                                                                                <field name="value">68</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_input_keyboard" id="KRBfs6q}@1aLs2_VeSnIG">
                                                                                                <field name="#prop"></field>
                                                                                                <field name="keyboard">numbers1</field>
                                                                                                <next>
                                                                                                  <block type="prop_input_max_length" id="QC=Ny^/1cHM~)fgwA0T,">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="maxLength">
                                                                                                      <block type="expression" id="/K?#y^4T5u9rovUJL-y8">
                                                                                                        <field name="value">a.toString().length</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_input_result_position" id="8+aJN|z,~!/Wm_LulsfO">
                                                                                                        <field name="#prop"></field>
                                                                                                        <field name="resultPosition">bottom</field>
                                                                                                        <next>
                                                                                                          <block type="prop_tab_order" id="C9Rvmc`n8:pl!`8w{9+V">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="tabOrder">
                                                                                                              <block type="expression" id="(q2w@28FUCf{Q`:_Py$55">
                                                                                                                <field name="value">0</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_stroke" id="{1h#MS)yv;pu(m/iy(0a">
                                                                                                                <field name="#prop">stroke</field>
                                                                                                                <next>
                                                                                                                  <block type="prop_fill" id="MalW-n,Zz{tPyb_LBPFw">
                                                                                                                    <field name="#prop">fill</field>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style" id=")tLVfD-(YeXM6q3V2XD]">
                                                                                                                        <field name="base">text</field>
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_text_style_font_size" id="kNUwKr:kyuyTv8kIUwx}">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="fontSize">
                                                                                                                              <block type="expression" id="Zh_7=ru}L:gPj@2#6C}CM">
                                                                                                                                <field name="value">32</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_fill" id="TB@2oW)f8EJ:~/gg%6jkP">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="fill">
                                                                                                                                  <block type="string_value" id="t:Krr;)UO.yu~m~$u]mn">
                                                                                                                                    <field name="value">black</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_stroke" id="AMkgzoTomWL2_28-Q=yL">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="stroke">
                                                                                                                                      <block type="string_value" id="edu|=)}~FojXONkR%D)N">
                                                                                                                                        <field name="value">white</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="{WVhRKe#El5cD(FiApsQ">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="strokeThickness">
                                                                                                                                          <block type="expression" id="rrC6!z~YZO-/aIIRs{O9">
                                                                                                                                            <field name="value">2</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </statement>
                                                            <next>
                                                              <block type="variable" id="LMjMV${0([g.@1Fj/#?8S">
                                                                <field name="name">link</field>
                                                                <value name="value">
                                                                  <block type="expression" id="dyZ+n+h33~WrpEGt[Kr[">
                                                                    <field name="value">'http://starmathsonline.s3.amazonaws.com/' + image_path</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="if_then_block" id="NVuLX{v+C{mYQn@10k#{~">
                                                                    <value name="if">
                                                                      <block type="expression" id="F`B4$?|!L[S;(hOrwiLt">
                                                                        <field name="value">type == 1</field>
                                                                      </block>
                                                                    </value>
                                                                    <statement name="then">
                                                                      <block type="partial_explanation" id=")YS)Vp`zbRfn}#WFpgNv" inline="true">
                                                                        <value name="value">
                                                                          <block type="string_value" id="f}}D05Jk4CcehRD^.UzG">
                                                                            <field name="value">&lt;u&gt;Step 1: First, look at all 3 angles.&lt;/u&gt;&lt;/br&gt;
  &lt;/br&gt;
&lt;style&gt;
      #angle1 {
              -ms-transform: rotate(${180-angle[0]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(180-angle[0]}deg);
              /@2 Safari @2/
              transform: rotate(${180-angle[0]}deg);
              /@2 Standard syntax @2/
          }
          #angle2 {
              -ms-transform: rotate(${180-angle[1]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${180-angle[1]}deg);
              /@2 Safari @2/
              transform: rotate(${180-angle[1]}deg);
              /@2 Standard syntax @2/
          }
          #angle3 {
              -ms-transform: rotate(${180-angle[2]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${180-angle[2]}deg);
              /@2 Safari @2/
              transform: rotate(${180-angle[2]}deg);
              /@2 Standard syntax @2/
          }

          #anglec1 {
              -ms-transform: rotate(${360-angle[0]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${360-angle[0]}deg);
              /@2 Safari @2/
              transform: rotate(${360-angle[0]}deg);
              /@2 Standard syntax @2/
          }
          #anglec2 {
              -ms-transform: rotate(${360-angle[1]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${360-angle[1]}deg);
              /@2 Safari @2/
              transform: rotate(${360-angle[1]}deg);
              /@2 Standard syntax @2/
          }
          #anglec3 {
              -ms-transform: rotate(${360-angle[2]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${360-angle[2]}deg);
              /@2 Safari @2/
              transform: rotate(${360-angle[2]}deg);
              /@2 Standard syntax @2/
          }
      &lt;/style&gt;
Remember this, a right angle is 90°, Look at each angle, now calculate each angle.
&lt;/br&gt;&lt;/br&gt;
&lt;div style='position: relative; width: 280px; height: 280px; display: inline-block;'&gt;
                      &lt;div style='position: absolute; width: 280px; height: 250px; left: 10px; top: 10px;'&gt;
                        &lt;img style='position: absolute; min-width: 280px; max-height: 250px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/box.png'/&gt;
                        &lt;img id='anglec1' style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/cl.png'/&gt;
                        &lt;img style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/liner.png'/&gt;
                        &lt;img id='angle1' style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/linel.png'/&gt;
                        &lt;img style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/point.png'/&gt;
                      &lt;/div&gt;
              &lt;span style='position: absolute; left: 100px; top: 270px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'&gt;${angle[0]}&amp;deg;&lt;/span&gt;
                    &lt;/div&gt;
              &lt;div style='width: 20px; display: inline-block'&gt;&lt;/div&gt;
                    &lt;div style='position: relative; width: 280px; height: 280px; display: inline-block;'&gt;
                      &lt;div style='position: absolute; width: 280px; height: 250px; left: 10px; top: 10px;'&gt;
                        &lt;img style='position: absolute; min-width: 280px; max-height: 250px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/box.png'/&gt;
                        &lt;img id='anglec2' style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/cl.png'/&gt;
                        &lt;img style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/liner.png'/&gt;
                        &lt;img id='angle2' style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/linel.png'/&gt;
                        &lt;img style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/point.png'/&gt;
                      &lt;/div&gt;
              &lt;span style='position: absolute; left: 100px; top: 270px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'&gt;${angle[1]}&amp;deg;&lt;/span&gt;
                    &lt;/div&gt;
              &lt;div style='width: 20px; display: inline-block'&gt;&lt;/div&gt;
                    &lt;div style='position: relative; width: 280px; height: 280px; display: inline-block;'&gt;
                      &lt;div style='position: absolute; width: 280px; height: 250px; left: 10px; top: 10px;'&gt;
                        &lt;img style='position: absolute; min-width: 280px; max-height: 250px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/box.png'/&gt;
                        &lt;img id='anglec3' style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/cl.png'/&gt;
                        &lt;img style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/liner.png'/&gt;
                        &lt;img id='angle3' style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/linel.png'/&gt;
                        &lt;img style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/point.png'/&gt;
                      &lt;/div&gt;
              &lt;span style='position: absolute; left: 100px; top: 270px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'&gt;${angle[2]}&amp;deg;&lt;/span&gt;
                    &lt;/div&gt;
                                                                            </field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="end_partial_explanation" id="?e[48pyD2?Ue)na9Txf=">
                                                                            <next>
                                                                              <block type="partial_explanation" id="3sMi^yW`nZ0o3StL+8{8" inline="true">
                                                                                <value name="value">
                                                                                  <block type="string_value" id="z?Fp.r;4LD5oi~:3Bi?G">
                                                                                    <field name="value">&lt;u&gt;Step 2: Let's find our final answer.&lt;/u&gt;&lt;/br&gt;
  &lt;/br&gt;
So which angle is ${a}&amp;deg;.&lt;/br&gt;&lt;/br&gt;
  
&lt;style&gt;
      #angle1 {
              -ms-transform: rotate(${180-angle[0]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(180-angle[0]}deg);
              /@2 Safari @2/
              transform: rotate(${180-angle[0]}deg);
              /@2 Standard syntax @2/
          }
          #angle2 {
              -ms-transform: rotate(${180-angle[1]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${180-angle[1]}deg);
              /@2 Safari @2/
              transform: rotate(${180-angle[1]}deg);
              /@2 Standard syntax @2/
          }
          #angle3 {
              -ms-transform: rotate(${180-angle[2]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${180-angle[2]}deg);
              /@2 Safari @2/
              transform: rotate(${180-angle[2]}deg);
              /@2 Standard syntax @2/
          }

          #anglec1 {
              -ms-transform: rotate(${360-angle[0]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${360-angle[0]}deg);
              /@2 Safari @2/
              transform: rotate(${360-angle[0]}deg);
              /@2 Standard syntax @2/
          }
          #anglec2 {
              -ms-transform: rotate(${360-angle[1]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${360-angle[1]}deg);
              /@2 Safari @2/
              transform: rotate(${360-angle[1]}deg);
              /@2 Standard syntax @2/
          }
          #anglec3 {
              -ms-transform: rotate(${360-angle[2]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${360-angle[2]}deg);
              /@2 Safari @2/
              transform: rotate(${360-angle[2]}deg);
              /@2 Standard syntax @2/
          }
      &lt;/style&gt;


&lt;div style='position: relative; width: 300px; height: 280px; display: inline-block; border: ${angle[0] == a ? "3px solid green": ""}'&gt;
                      &lt;div style='position: absolute; width: 280px; height: 250px; left: 10px; top: 10px;'&gt;
                        &lt;img style='position: absolute; min-width: 280px; max-height: 250px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/box.png'/&gt;
                        &lt;img id='anglec1' style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/cl.png'/&gt;
                        &lt;img style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/liner.png'/&gt;
                        &lt;img id='angle1' style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/linel.png'/&gt;
                        &lt;img style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/point.png'/&gt;
                      &lt;/div&gt;
              &lt;span style='position: absolute; left: 100px; top: 270px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'&gt;${angle[0]}&amp;deg;&lt;/span&gt;
                    &lt;/div&gt;
              &lt;div style='width: 20px; display: inline-block'&gt;&lt;/div&gt;
&lt;div style='position: relative; width: 300px; height: 280px; display: inline-block; border: ${angle[1] == a ? "3px solid green": ""}'&gt;
                      &lt;div style='position: absolute; width: 280px; height: 250px; left: 10px; top: 10px;'&gt;
                        &lt;img style='position: absolute; min-width: 280px; max-height: 250px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/box.png'/&gt;
                        &lt;img id='anglec2' style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/cl.png'/&gt;
                        &lt;img style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/liner.png'/&gt;
                        &lt;img id='angle2' style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/linel.png'/&gt;
                        &lt;img style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/point.png'/&gt;
                      &lt;/div&gt;
              &lt;span style='position: absolute; left: 100px; top: 270px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'&gt;${angle[1]}&amp;deg;&lt;/span&gt;
                    &lt;/div&gt;
              &lt;div style='width: 20px; display: inline-block'&gt;&lt;/div&gt;
&lt;div style='position: relative; width: 300px; height: 280px; display: inline-block; border: ${angle[2] == a ? "3px solid green": ""}'&gt;
                      &lt;div style='position: absolute; width: 280px; height: 250px; left: 10px; top: 10px;'&gt;
                        &lt;img style='position: absolute; min-width: 280px; max-height: 250px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/box.png'/&gt;
                        &lt;img id='anglec3' style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/cl.png'/&gt;
                        &lt;img style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/liner.png'/&gt;
                        &lt;img id='angle3' style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/linel.png'/&gt;
                        &lt;img style='position: absolute; top: 40px; left: -80px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/point.png'/&gt;
                      &lt;/div&gt;

              &lt;span style='position: absolute; left: 100px; top: 270px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'&gt;${angle[2]}&amp;deg;&lt;/span&gt;
                    &lt;/div&gt;
                                                                                    </field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="end_partial_explanation" id="2czYJo~!]53D/Nygd|Am"></block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <next>
                                                                      <block type="if_then_block" id="tZ,PG}wRTy5TVO=Zh_p[">
                                                                        <value name="if">
                                                                          <block type="expression" id="BnJM=9`]!F}nv[@1E!0yi">
                                                                            <field name="value">type == 2</field>
                                                                          </block>
                                                                        </value>
                                                                        <statement name="then">
                                                                          <block type="partial_explanation" id="3[KU@2dsE[|m@2C=v_.y=A" inline="true">
                                                                            <value name="value">
                                                                              <block type="string_value" id=";p:0E8)0xZ+zdZ`m6em+">
                                                                                <field name="value">Place the midpoint of the protractor on the VERTEX of the angle.
&lt;/br&gt;
At the same time, line up one side of the angle with the zero line of the protractor.
&lt;/br&gt;&lt;/br&gt;&lt;/br&gt;
&lt;style&gt;
                    #angle1 {
              -ms-transform: rotate(${180-a}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${180-a}deg);
              /@2 Safari @2/
              transform: rotate(${180-a}deg);
              /@2 Standard syntax @2/
          }
          #anglec1 {
              -ms-transform: rotate(${360-a}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${360-a}deg);
              /@2 Safari @2/
              transform: rotate(${360-a}deg);
              /@2 Standard syntax @2/
          }
          &lt;/style&gt;

        &lt;div style='position: relative; width: 260px; height: 280px; display: inline-block;'&gt;
            &lt;div style='position: absolute; width: 600px; height: 350px; left: 300px; top: 10px;'&gt;
              &lt;img style='position: absolute; min-width: 320px; max-height: 250px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/box.png'/&gt;
              &lt;img id='anglec1' style='position: absolute; top: 30px; left: -60px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/cl.png'/&gt;
              &lt;img style='position: absolute; top: 30px; left: -60px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/liner.png'/&gt;
              &lt;img id='angle1' style='position: absolute; top: 30px; left: -60px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/linel.png'/&gt;
              &lt;img style='position: absolute; top: 30px; left: -60px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/point.png'/&gt;
            
              &lt;img style='position: absolute; max-width: 350px; left: -85px; top: 5px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA90.1B/pro.png'/&gt;
            
            &lt;/div&gt;
&lt;/div&gt;
&lt;/br&gt;
  &lt;span style='background: rgb(250, 250, 250, 0.5)'&gt;
Read the degrees where the other side crosses the number scale.
&lt;/br&gt;
It says &lt;b&gt;${a}&amp;deg;&lt;/b&gt;!&lt;/span&gt;
                                                                                </field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="if_then_block" id="c+3(78~j5~n?j2k8w5-K">
                                                                            <value name="if">
                                                                              <block type="expression" id="(K[M84prWYV@1B)zsx}On">
                                                                                <field name="value">type == 3</field>
                                                                              </block>
                                                                            </value>
                                                                            <statement name="then">
                                                                              <block type="partial_explanation" id="K3r9H/w#WlY7o/mubeyz" inline="true">
                                                                                <value name="value">
                                                                                  <block type="string_value" id="2UqqYb^hV$d_9vx(y|fC">
                                                                                    <field name="value">&lt;u&gt;Remember the size of the straight line.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

It's 90°!&lt;/br&gt;&lt;/br&gt;

Now, using subtraction, let's subtract 90° by all the known angles.&lt;/br&gt;&lt;br&gt;

90&amp;deg; - ${90 - a}&amp;deg; = &lt;b&gt;${a}&amp;deg;&lt;/b&gt;&lt;/br&gt;&lt;/br&gt;

So the angle is &lt;b&gt;${a}&amp;deg;&lt;/b&gt;!
                                                                                    </field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </statement>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
  <block type="if_then_else_block" id="}%)H=K_WDrwZ_}~ljR86" x="429" y="10428">
    <value name="if">
      <block type="expression" id="Os-7}+P%WEY}u{K]Hz7w">
        <field name="value">$cell.data == 90</field>
      </block>
    </value>
    <statement name="then">
      <block type="image_shape" id="K2LCx`rC}~U/Q]O1uNmr">
        <statement name="#props">
          <block type="prop_position" id="3gPa,FW|?L#IurborQ0^">
            <field name="#prop"></field>
            <value name="x">
              <block type="expression" id="S3Rm5~;y{JNu7drgANt6">
                <field name="value">$cell.centerX 70</field>
              </block>
            </value>
            <value name="y">
              <block type="expression" id="44Ald#4g.jTqA65CJV|p">
                <field name="value">$cell.centerY + 50</field>
              </block>
            </value>
            <next>
              <block type="prop_image_key" id="A!Xt1gJ4~A)?)Mr$C0Vm">
                <field name="#prop"></field>
                <value name="key">
                  <block type="string_value" id="qT;D1;y}t1K+KIBhlhG[">
                    <field name="value">sq.png</field>
                  </block>
                </value>
                <next>
                  <block type="prop_image_src" id="w@1qx+1T~qyd!X(2.G$fi">
                    <field name="#prop"></field>
                    <value name="src">
                      <block type="string_value" id="_I^|Nz,FIji?O%)7X+yx">
                        <field name="value">${image_path}sq.png</field>
                      </block>
                    </value>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </statement>
      </block>
    </statement>
  </block>
  <block type="partial_explanation" id="([d$9r=R)5k}UxBfT?u6" inline="true" x="0" y="10672">
    <value name="value">
      <block type="string_value" id="xPp1U2:,C+FyNV6F6T)m">
        <field name="value">&lt;u&gt;Step 2: Let's find our final answer.&lt;/u&gt;&lt;/br&gt;
  &lt;/br&gt;
So which angle is ${a}&amp;deg;
&lt;/br&gt;&lt;/br&gt;
&lt;style&gt;
      #angle1 {
              -ms-transform: rotate(${180-angle[0]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(180-angle[0]}deg);
              /@2 Safari @2/
              transform: rotate(${180-angle[0]}deg);
              /@2 Standard syntax @2/
          }
          #angle2 {
              -ms-transform: rotate(${180-angle[1]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${180-angle[1]}deg);
              /@2 Safari @2/
              transform: rotate(${180-angle[1]}deg);
              /@2 Standard syntax @2/
          }
          #angle3 {
              -ms-transform: rotate(${180-angle[2]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${180-angle[2]}deg);
              /@2 Safari @2/
              transform: rotate(${180-angle[2]}deg);
              /@2 Standard syntax @2/
          }

          #anglec1 {
              -ms-transform: rotate(${360-angle[0]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${360-angle[0]}deg);
              /@2 Safari @2/
              transform: rotate(${360-angle[0]}deg);
              /@2 Standard syntax @2/
          }
          #anglec2 {
              -ms-transform: rotate(${360-angle[1]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${360-angle[1]}deg);
              /@2 Safari @2/
              transform: rotate(${360-angle[1]}deg);
              /@2 Standard syntax @2/
          }
          #anglec3 {
              -ms-transform: rotate(${360-angle[2]}deg);
              /@2 IE 9 @2/
              -webkit-transform: rotate(${360-angle[2]}deg);
              /@2 Safari @2/
              transform: rotate(${360-angle[2]}deg);
              /@2 Standard syntax @2/
          }
      &lt;/style&gt;

      &lt;div style='position: relative; width: 260px; height: 280px; display: inline-block; border: ${angle[0] == a ? "3px solid green": ""}'&gt;
        &lt;div style='position: absolute; width: 300px; height: 250px; left: 10px; top: 10px;'&gt;
          &lt;img style='position: absolute; min-width: 240px; max-height: 200px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/box.png'/&gt;
          &lt;img id='anglec1' style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/cl.png'/&gt;
          &lt;img style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/liner.png'/&gt;
          &lt;img id='angle1' style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/linel.png'/&gt;
          &lt;img style='position: absolute; top: 30px; left: ${10 - Ox(angle[0])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/point.png'/&gt;
        &lt;/div&gt;
&lt;span style='position: absolute; left: 100px; top: 200px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'&gt;${angle[0]}&amp;deg;&lt;/span&gt;
      &lt;/div&gt;
&lt;div style='width: 30px; display: inline-block'&gt;&lt;/div&gt;
      &lt;div style='position: relative; width: 260px; height: 280px; display: inline-block; border: ${angle[1] == a ? "3px solid green": ""}'&gt;
        &lt;div style='position: absolute; width: 300px; height: 250px; left: 10px; top: 10px;'&gt;
          &lt;img style='position: absolute; min-width: 240px; max-height: 200px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/box.png'/&gt;
          &lt;img id='anglec2' style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/cl.png'/&gt;
          &lt;img style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/liner.png'/&gt;
          &lt;img id='angle2' style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/linel.png'/&gt;
          &lt;img style='position: absolute; top: 30px; left: ${10 - Ox(angle[1])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/point.png'/&gt;
        &lt;/div&gt;
&lt;span style='position: absolute; left: 100px; top: 200px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'&gt;${angle[1]}&amp;deg;&lt;/span&gt;
      &lt;/div&gt;
&lt;div style='width: 30px; display: inline-block'&gt;&lt;/div&gt;
      &lt;div style='position: relative; width: 260px; height: 280px; display: inline-block; border: ${angle[2] == a ? "3px solid green": ""}'&gt;
        &lt;div style='position: absolute; width: 300px; height: 250px; left: 10px; top: 10px;'&gt;
          &lt;img style='position: absolute; min-width: 240px; max-height: 200px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/box.png'/&gt;
          &lt;img id='anglec3' style='position: absolute; top: 30px; left: ${10 - Ox(angle[2])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/cl.png'/&gt;
          &lt;img style='position: absolute; top: 30px; left: ${10 - Ox(angle[2])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/liner.png'/&gt;
          &lt;img id='angle3' style='position: absolute; top: 30px; left: ${10 - Ox(angle[2])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/linel.png'/&gt;
          &lt;img style='position: absolute; top: 30px; left: ${10 - Ox(angle[2])}px' src='http://starmathsonline.s3.amazonaws.com/Develop/ImageQAs/Y6.MG.AN.FA180.1B/point.png'/&gt;
        &lt;/div&gt;
&lt;span style='position: absolute; left: 100px; top: 200px; background: rgb(250, 250, 250, 0.5); transform: translate(50%, 50%)'&gt;${angle[2]}&amp;deg;&lt;/span&gt;
      &lt;/div&gt;
        </field>
      </block>
    </value>
  </block>
</xml>
END_XML]] */