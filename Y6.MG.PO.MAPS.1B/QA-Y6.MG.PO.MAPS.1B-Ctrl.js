
module.exports = [
  {
    "#type": "question",
    "name": "Y6.MG.PO.MAPS.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y6.MG.PO.MAPS.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "range < 0"
        },
        "then": [
          {
            "#type": "variable",
            "name": "city",
            "value": {
              "#type": "random_many",
              "count": {
                "#type": "expression",
                "value": "5"
              },
              "items": {
                "#type": "func",
                "name": "arrayOfNumber",
                "args": [
                  {
                    "#type": "expression",
                    "value": "20"
                  },
                  {
                    "#type": "expression",
                    "value": "0"
                  }
                ]
              }
            }
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "range < 4"
            },
            "then": [
              {
                "#type": "variable",
                "name": "city",
                "value": {
                  "#type": "random_many",
                  "count": {
                    "#type": "expression",
                    "value": "10"
                  },
                  "items": {
                    "#type": "func",
                    "name": "arrayOfNumber",
                    "args": [
                      {
                        "#type": "expression",
                        "value": "20"
                      },
                      {
                        "#type": "expression",
                        "value": "0"
                      }
                    ]
                  }
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "city",
                "value": {
                  "#type": "random_many",
                  "count": {
                    "#type": "expression",
                    "value": "20"
                  },
                  "items": {
                    "#type": "func",
                    "name": "arrayOfNumber",
                    "args": [
                      {
                        "#type": "expression",
                        "value": "20"
                      },
                      {
                        "#type": "expression",
                        "value": "0"
                      }
                    ]
                  }
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "statement",
        "value": "function check_exist(x, A){\n    for(var i=0; i<A.length; i++){\n        if(x == A[i]){\n            return 1;\n        }\n    }\n    return 0;\n}"
      },
      {
        "#type": "variable",
        "name": "city_name",
        "value": {
          "#type": "string_array",
          "items": "Rio de Janeiro|Lima|Cape Town|Busno Aires|Sydney|Perth|Jakarta|Tripoli|Caracas|Luanda|Riyadh|Ha Noi|New Delhi|Beijing|Ulaanbaotar|Mascow|Berlin|London|Nuuk|San Francisco"
        }
      },
      {
        "#type": "variable",
        "name": "city_location",
        "value": {
          "#type": "string_array",
          "items": "E4|D4|G5|E5|L5|J5|J4|G3|D3|G4|H3|J3|I3|K2|J2|H2|G2|F2|D2|B2"
        }
      },
      {
        "#type": "variable",
        "name": "location",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < 6"
        },
        "do": [
          {
            "#type": "do_while_block",
            "do": [
              {
                "#type": "variable",
                "name": "X",
                "value": {
                  "#type": "random_one",
                  "items": {
                    "#type": "expression",
                    "value": "['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L']"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "Y",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "1"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "6"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "temp",
                "value": {
                  "#type": "expression",
                  "value": "X+Y"
                }
              }
            ],
            "while": {
              "#type": "expression",
              "value": "check_exist(temp, location) == 1"
            }
          },
          {
            "#type": "variable",
            "name": "location[i]",
            "value": {
              "#type": "expression",
              "value": "temp"
            }
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "statement",
        "value": "if(check_exist(city_location[city[0]], location) == 0){\n  location.push(city_location[city[0]]);\n  for(var i =0; i< location.length; i++){\n    if(location[i] != city_location[city[1]]){\n      location.splice(i, 1);\n      break;\n    }\n  }\n}\n\nif(check_exist(city_location[city[1]], location) == 0){\n  location.push(city_location[city[1]]);\n  for(var i =0; i< location.length; i++){\n    if(location[i] != city_location[city[0]]){\n      location.splice(i, 1);\n      break;\n    }\n  }\n}"
      },
      {
        "#type": "variable",
        "name": "location",
        "value": {
          "#type": "func",
          "name": "shuffle",
          "args": [
            {
              "#type": "expression",
              "value": "location"
            }
          ]
        }
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "40"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "What are the coordinates for ${city_name[city[0]]} and ${city_name[city[1]]}?"
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "36"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "image_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "540"
            },
            "y": {
              "#type": "expression",
              "value": "220"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "0"
            },
            "height": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "map.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}map.png"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < city.length"
        },
        "do": [
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "540"
                },
                "y": {
                  "#type": "expression",
                  "value": "220"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "0"
                },
                "height": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "${city[i]}.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}${city[i]}.png"
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "list_shape",
        "#props": [
          {
            "#type": "prop_list_direction",
            "#prop": "",
            "dir": "horizontal"
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "400"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_spacing",
            "#prop": "",
            "spacing": {
              "#type": "expression",
              "value": "10"
            }
          }
        ],
        "items": [
          {
            "#type": "json",
            "#props": [
              {
                "#type": "prop_list_align",
                "#prop": "",
                "align": "middle"
              }
            ],
            "template": {
              "#callback": "$item",
              "variable": "$item",
              "body": [
                {
                  "#type": "text_shape",
                  "#props": [
                    {
                      "#type": "prop_text_contents",
                      "#prop": "",
                      "contents": "${city_name[city[0]]}"
                    },
                    {
                      "#prop": "",
                      "style": {
                        "#type": "json",
                        "base": "text",
                        "#props": [
                          {
                            "#type": "prop_text_style_font_size",
                            "#prop": "",
                            "fontSize": {
                              "#type": "expression",
                              "value": "36"
                            }
                          },
                          {
                            "#type": "prop_text_style_fill",
                            "#prop": "",
                            "fill": "black"
                          },
                          {
                            "#type": "prop_text_style_stroke",
                            "#prop": "",
                            "stroke": "white"
                          },
                          {
                            "#type": "prop_text_style_stroke_thickness",
                            "#prop": "",
                            "strokeThickness": {
                              "#type": "expression",
                              "value": "2"
                            }
                          }
                        ]
                      }
                    }
                  ]
                }
              ]
            }
          },
          {
            "#type": "json",
            "#props": [
              {
                "#type": "prop_list_align",
                "#prop": "",
                "align": "middle"
              }
            ],
            "template": {
              "#callback": "$item",
              "variable": "$item",
              "body": [
                {
                  "#type": "image_shape",
                  "#props": [
                    {
                      "#type": "prop_image_key",
                      "#prop": "",
                      "key": "box.png"
                    },
                    {
                      "#type": "prop_image_src",
                      "#prop": "",
                      "src": "${image_path}box.png"
                    }
                  ]
                },
                {
                  "#type": "drop_shape",
                  "multiple": false,
                  "resultMode": "default",
                  "groupName": "",
                  "#props": [
                    {
                      "#type": "prop_size",
                      "#prop": "",
                      "width": {
                        "#type": "expression",
                        "value": "114"
                      },
                      "height": {
                        "#type": "expression",
                        "value": "60"
                      }
                    },
                    {
                      "#type": "prop_value",
                      "#prop": "",
                      "value": {
                        "#type": "expression",
                        "value": "city_location[city[0]]"
                      }
                    }
                  ]
                }
              ]
            }
          },
          {
            "#type": "json",
            "#props": [
              {
                "#type": "prop_list_align",
                "#prop": "",
                "align": "middle"
              }
            ],
            "template": {
              "#callback": "$item",
              "variable": "$item",
              "body": [
                {
                  "#type": "text_shape",
                  "#props": [
                    {
                      "#type": "prop_text_contents",
                      "#prop": "",
                      "contents": "${'  '}${city_name[city[1]]}"
                    },
                    {
                      "#prop": "",
                      "style": {
                        "#type": "json",
                        "base": "text",
                        "#props": [
                          {
                            "#type": "prop_text_style_font_size",
                            "#prop": "",
                            "fontSize": {
                              "#type": "expression",
                              "value": "36"
                            }
                          },
                          {
                            "#type": "prop_text_style_fill",
                            "#prop": "",
                            "fill": "black"
                          },
                          {
                            "#type": "prop_text_style_stroke",
                            "#prop": "",
                            "stroke": "white"
                          },
                          {
                            "#type": "prop_text_style_stroke_thickness",
                            "#prop": "",
                            "strokeThickness": {
                              "#type": "expression",
                              "value": "2"
                            }
                          }
                        ]
                      }
                    }
                  ]
                }
              ]
            }
          },
          {
            "#type": "json",
            "#props": [
              {
                "#type": "prop_list_align",
                "#prop": "",
                "align": "middle"
              }
            ],
            "template": {
              "#callback": "$item",
              "variable": "$item",
              "body": [
                {
                  "#type": "image_shape",
                  "#props": [
                    {
                      "#type": "prop_image_key",
                      "#prop": "",
                      "key": "box.png"
                    },
                    {
                      "#type": "prop_image_src",
                      "#prop": "",
                      "src": "${image_path}box.png"
                    }
                  ]
                },
                {
                  "#type": "drop_shape",
                  "multiple": false,
                  "resultMode": "default",
                  "groupName": "",
                  "#props": [
                    {
                      "#type": "prop_position",
                      "#prop": "",
                      "x": {
                        "#type": "expression",
                        "value": "0"
                      },
                      "y": {
                        "#type": "expression",
                        "value": "0"
                      }
                    },
                    {
                      "#type": "prop_size",
                      "#prop": "",
                      "width": {
                        "#type": "expression",
                        "value": "114"
                      },
                      "height": {
                        "#type": "expression",
                        "value": "60"
                      }
                    },
                    {
                      "#type": "prop_value",
                      "#prop": "",
                      "value": {
                        "#type": "expression",
                        "value": "city_location[city[1]]"
                      }
                    }
                  ]
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "3"
            },
            "cols": {
              "#type": "expression",
              "value": "2"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "150"
            },
            "y": {
              "#type": "expression",
              "value": "220"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "250"
            },
            "height": {
              "#type": "expression",
              "value": "220"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "expression",
              "value": "location"
            }
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_cell_template",
            "variable": "$cell",
            "#prop": "cell.template",
            "#callback": "$cell",
            "body": [
              {
                "#type": "choice_custom_shape",
                "action": "drag",
                "value": {
                  "#type": "expression",
                  "value": "$cell.data"
                },
                "template": {
                  "#callback": "$choice",
                  "variable": "$choice",
                  "body": [
                    {
                      "#type": "image_shape",
                      "#props": [
                        {
                          "#type": "prop_position",
                          "#prop": "",
                          "x": {
                            "#type": "expression",
                            "value": "$cell.centerX"
                          },
                          "y": {
                            "#type": "expression",
                            "value": "$cell.centerY"
                          }
                        },
                        {
                          "#type": "prop_size",
                          "#prop": "",
                          "width": {
                            "#type": "expression",
                            "value": "114"
                          },
                          "height": {
                            "#type": "expression",
                            "value": "60"
                          }
                        },
                        {
                          "#type": "prop_image_key",
                          "#prop": "",
                          "key": "shape.png"
                        },
                        {
                          "#type": "prop_image_src",
                          "#prop": "",
                          "src": "${image_path}shape.png"
                        }
                      ]
                    },
                    {
                      "#type": "text_shape",
                      "#props": [
                        {
                          "#type": "prop_position",
                          "#prop": "",
                          "x": {
                            "#type": "expression",
                            "value": "$cell.centerX"
                          },
                          "y": {
                            "#type": "expression",
                            "value": "$cell.centerY + 3"
                          }
                        },
                        {
                          "#type": "prop_text_contents",
                          "#prop": "",
                          "contents": "${$cell.data}"
                        },
                        {
                          "#prop": "",
                          "style": {
                            "#type": "json",
                            "base": "text",
                            "#props": [
                              {
                                "#type": "prop_text_style_font_size",
                                "#prop": "",
                                "fontSize": {
                                  "#type": "expression",
                                  "value": "36"
                                }
                              },
                              {
                                "#type": "prop_text_style_fill",
                                "#prop": "",
                                "fill": "black"
                              }
                            ]
                          }
                        }
                      ]
                    }
                  ]
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "Variable",
        "value": {
          "#type": "custom_image_list",
          "link": "${image_path}",
          "images": "ex${city[0]}|ex${city[1]}|map"
        }
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<center><u>Let's answer ${city_name[city[0]]} location</u></br></br>\n\n<div style='position: relative; width: 487px; height: 260px;'>\n<img src='@sprite.src(map.png)' style='position: absolute; left: 0px'/>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < city.length"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<img src='@sprite.src(${city[i]}.png)' style='position: absolute; left: 0px'/>"
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<img src='@sprite.src(ex${city[0]}.png)' style='position: absolute; left: 0px'/>\n</div>\n</center>\n<span style='background: rgb(250, 250, 250, 0.3)'>\nVertically, you can see where ${city_name[city[0]]} is <b>${city_location[city[0]][0]}</b></br>\nHorizontally, you can see where ${city_name[city[0]]} is <b>${city_location[city[0]][1]}</b></br></br>\n<center><span style='background: rgb(250, 250, 250, 0.3)'>So now you know, ${city_name[city[0]]} is <b>${city_location[city[0]]}</b></center></span>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<center><u>For ${city_name[city[1]]}</u></br></br>\n\n<div style='position: relative; width: 487px; height: 260px;'>\n<img src='@sprite.src(map.png)' style='position: absolute; left: 0px'/>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < city.length"
        },
        "do": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<img src='@sprite.src(${city[i]}.png)' style='position: absolute; left: 0px'/>"
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<img src='@sprite.src(ex${city[1]}.png)' style='position: absolute; left: 0px'/>\n</div>\n</center>\n<span style='background: rgb(250, 250, 250, 0.3)'>\nVertically, you can see where ${city_name[city[1]]} is <b>${city_location[city[1]][0]}</b></br>\nHorizontally, you can see where ${city_name[city[1]]} is <b>${city_location[city[1]][1]}</b></br></br>\n<center><span style='background: rgb(250, 250, 250, 0.3)'>So again, now you know where ${city_name[city[1]]} is <b>${city_location[city[1]]}</b></center></span>"
        ]
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="0" y="0">
    <field name="name">Y6.MG.PO.MAPS.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y6.MG.PO.MAPS.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="YepMQT@2uwp2rRPvmnRg1">
                                    <field name="name">range</field>
                                    <value name="value">
                                      <block type="expression" id="UxgQ^Y5]rb}vpw[D=l-!">
                                        <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="if_then_else_block" id="qGoB8}}k~=0@2NhJ[#)pA">
                                        <value name="if">
                                          <block type="expression" id="-/+VZ4j}Gvgl}K_2}Bd`">
                                            <field name="value">range &lt; 0</field>
                                          </block>
                                        </value>
                                        <statement name="then">
                                          <block type="variable" id="#nl]|1oU0KRS8sbfk@2#-">
                                            <field name="name">city</field>
                                            <value name="value">
                                              <block type="random_many" id="OUuzEm%(^:lUK[-nZL_a">
                                                <value name="count">
                                                  <block type="expression" id="|ajz(T;@1j[kpUblgU$T,">
                                                    <field name="value">5</field>
                                                  </block>
                                                </value>
                                                <value name="items">
                                                  <block type="func_array_of_number" id=".5m-9jTQKCOx(=22UPkd" inline="true">
                                                    <value name="items">
                                                      <block type="expression" id="9fh}g4E@1@1/Ck.yiYeKes">
                                                        <field name="value">20</field>
                                                      </block>
                                                    </value>
                                                    <value name="from">
                                                      <block type="expression" id="0Uc;nXe2QO8(y?t(G#]m">
                                                        <field name="value">0</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                              </block>
                                            </value>
                                          </block>
                                        </statement>
                                        <statement name="else">
                                          <block type="if_then_else_block" id="lQjÚP3vFr8^qK+y5$z">
                                            <value name="if">
                                              <block type="expression" id="1BD9)dz5GM15CF,b(G$g">
                                                <field name="value">range &lt; 4</field>
                                              </block>
                                            </value>
                                            <statement name="then">
                                              <block type="variable" id="c4;e#?sl4AG#mGXZ#)g5">
                                                <field name="name">city</field>
                                                <value name="value">
                                                  <block type="random_many" id="LxLLX]b7M4o[.B13Gw2m">
                                                    <value name="count">
                                                      <block type="expression" id="g=s9X%tNkGiihwN/0gA@2">
                                                        <field name="value">10</field>
                                                      </block>
                                                    </value>
                                                    <value name="items">
                                                      <block type="func_array_of_number" id="{WJ`@1ahki5?mf`[FU?-T" inline="true">
                                                        <value name="items">
                                                          <block type="expression" id="t3q(JDIHH@1mI4W`p(h%z">
                                                            <field name="value">20</field>
                                                          </block>
                                                        </value>
                                                        <value name="from">
                                                          <block type="expression" id="~`}.jz[nGqgmu,rQAtYx">
                                                            <field name="value">0</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                              </block>
                                            </statement>
                                            <statement name="else">
                                              <block type="variable" id="|4FyI;~M{_Pf.AuZvwF=">
                                                <field name="name">city</field>
                                                <value name="value">
                                                  <block type="random_many" id="4cg6.V[XFpNNptHW}K0p">
                                                    <value name="count">
                                                      <block type="expression" id="[p8Ez]I@1jwIrN`(X.h(P">
                                                        <field name="value">20</field>
                                                      </block>
                                                    </value>
                                                    <value name="items">
                                                      <block type="func_array_of_number" id="lze.U6JDhOBmNb!?`L=S" inline="true">
                                                        <value name="items">
                                                          <block type="expression" id="2kj3zKIoUrS?R_%-@10|N">
                                                            <field name="value">20</field>
                                                          </block>
                                                        </value>
                                                        <value name="from">
                                                          <block type="expression" id="XSP@14[k.{Dl/(G~RIxhO">
                                                            <field name="value">0</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                              </block>
                                            </statement>
                                          </block>
                                        </statement>
                                        <next>
                                          <block type="statement" id="Y]@1?Im{=aWK.rp,$x4r@2">
                                            <field name="value">function check_exist(x, A){
    for(var i=0; i&lt;A.length; i++){
        if(x == A[i]){
            return 1;
        }
    }
    return 0;
}
                                            </field>
                                            <next>
                                              <block type="variable" id="xwsW:qLGU4428sHtf2Qf">
                                                <field name="name">city_name</field>
                                                <value name="value">
                                                  <block type="string_array" id="~KpUmO:;cx,O`JRyb+#`">
                                                    <field name="items">Rio de Janeiro|Lima|Cape Town|Busno Aires|Sydney|Perth|Jakarta|Tripoli|Caracas|Luanda|Riyadh|Ha Noi|New Delhi|Beijing|Ulaanbaotar|Mascow|Berlin|London|Nuuk|San Francisco</field>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="|bI~uMT)$#INx=RVYE|.">
                                                    <field name="name">city_location</field>
                                                    <value name="value">
                                                      <block type="string_array" id=";_Da;eRw{F3:l;UallE4">
                                                        <field name="items">E4|D4|G5|E5|L5|J5|J4|G3|D3|G4|H3|J3|I3|K2|J2|H2|G2|F2|D2|B2</field>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id=".d^jlr9?@2dV:5R@2{(JUa">
                                                        <field name="name">location</field>
                                                        <value name="value">
                                                          <block type="expression" id="JFsTz@1ON$i5~Cn0kaECP">
                                                            <field name="value">[]</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="D)oeMm]T$1@2XSNs3z7^F">
                                                            <field name="name">i</field>
                                                            <value name="value">
                                                              <block type="expression" id="ZTA3=|BB5evKx9jlFpGP">
                                                                <field name="value">0</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="while_do_block" id="Y;B}}BOhL91W?P3l7Lg?">
                                                                <value name="while">
                                                                  <block type="expression" id="}5o{wn)VCtz`_KvnVqE8">
                                                                    <field name="value">i &lt; 6</field>
                                                                  </block>
                                                                </value>
                                                                <statement name="do">
                                                                  <block type="do_while_block" id="2HQP[^fu32ze3(@2s9k|W">
                                                                    <statement name="do">
                                                                      <block type="variable" id="%u~6Y@2WLQAN+w|qBsH.,">
                                                                        <field name="name">X</field>
                                                                        <value name="value">
                                                                          <block type="random_one" id="{rdndl)M()WK{MglAMt4">
                                                                            <value name="items">
                                                                              <block type="expression" id="#hIem[uHdIQ9sX,UW@1T-">
                                                                                <field name="value">['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L']</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="/m9fIh6``;+N__l[O4m@1">
                                                                            <field name="name">Y</field>
                                                                            <value name="value">
                                                                              <block type="random_number" id="6ewGR+]^@1P~x0H8gko+E">
                                                                                <value name="min">
                                                                                  <block type="expression" id="=HB^2eE}[P![+GB7a6NL">
                                                                                    <field name="value">1</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="max">
                                                                                  <block type="expression" id="?tMpJ5,-XsULk#Z%t/Bg">
                                                                                    <field name="value">6</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="variable" id="xXMO#zNdBp@2f|l;s7Fy7">
                                                                                <field name="name">temp</field>
                                                                                <value name="value">
                                                                                  <block type="expression" id="wj4$vmj@2.Y.|36f-5Dmo">
                                                                                    <field name="value">X+Y</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <value name="while">
                                                                      <block type="expression" id="@2v:hrpHxFkG=Ryt?:eEw">
                                                                        <field name="value">check_exist(temp, location) == 1</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="variable" id="_@1kR@2|tp,!ZFwjN)$m%p">
                                                                        <field name="name">location[i]</field>
                                                                        <value name="value">
                                                                          <block type="expression" id="rAUs5GG;ieEss3^`TDO|">
                                                                            <field name="value">temp</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="`NL99k}`aMYqmXq(mK0e">
                                                                            <field name="name">i</field>
                                                                            <value name="value">
                                                                              <block type="expression" id="0Mo)sA4N=gxq.D/AuI}j">
                                                                                <field name="value">i+1</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <next>
                                                                  <block type="statement" id="f3|QhD}W;CM^brb]@1`Gz">
                                                                    <field name="value">if(check_exist(city_location[city[0]], location) == 0){
  location.push(city_location[city[0]]);
  for(var i =0; i&lt; location.length; i++){
    if(location[i] != city_location[city[1]]){
      location.splice(i, 1);
      break;
    }
  }
}

if(check_exist(city_location[city[1]], location) == 0){
  location.push(city_location[city[1]]);
  for(var i =0; i&lt; location.length; i++){
    if(location[i] != city_location[city[0]]){
      location.splice(i, 1);
      break;
    }
  }
}
                                                                    </field>
                                                                    <next>
                                                                      <block type="variable" id="I?whW2hvX[ErLp0T4zc;">
                                                                        <field name="name">location</field>
                                                                        <value name="value">
                                                                          <block type="func_shuffle" id="Q+yR|[vN@2YG3dq%o+BEx" inline="true">
                                                                            <value name="value">
                                                                              <block type="expression" id="i5WP)[uDN32hux=sZ:ck">
                                                                                <field name="value">location</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                                            <statement name="#props">
                                                                              <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                                                <field name="#prop"></field>
                                                                                <value name="x">
                                                                                  <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                                                    <field name="value">400</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="y">
                                                                                  <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                                                    <field name="value">40</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="contents">
                                                                                      <block type="string_value" id="#6btV9p,PGV}wMx.Uï¿½">
                                                                                        <field name="value">What are the coordinates for ${city_name[city[0]]} and ${city_name[city[1]]}?</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                                        <field name="base">text</field>
                                                                                        <statement name="#props">
                                                                                          <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="fontSize">
                                                                                              <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                                                <field name="value">36</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="fill">
                                                                                                  <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                                                    <field name="value">black</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="stroke">
                                                                                                      <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                                        <field name="value">white</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="strokeThickness">
                                                                                                          <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                                            <field name="value">2</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="image_shape" id="v}UmU!KR^Du(=p5[y{By">
                                                                                <statement name="#props">
                                                                                  <block type="prop_position" id="9yL[tNAC=an~Yp_8?+K%">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="x">
                                                                                      <block type="expression" id="1Q`MNBw/yQ%dGF)OGxsY">
                                                                                        <field name="value">540</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="93z;lK({Xjv6=7iBU7t8">
                                                                                        <field name="value">220</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_size" id="Vu1CS_e{{c)E^=m?x%(,">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="width">
                                                                                          <block type="expression" id="h+lO_@1_y-uAl/PX?,cS9">
                                                                                            <field name="value">0</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="height">
                                                                                          <block type="expression" id="Xpsoyj@2#-@2(n+kx?`1@2(">
                                                                                            <field name="value">0</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_image_key" id="=]69wVK0nBNcw{$%(bmV">
                                                                                            <field name="#prop"></field>
                                                                                            <value name="key">
                                                                                              <block type="string_value" id="0U(!S.U1@1k+r(Zv^;8;z">
                                                                                                <field name="value">map.png</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="prop_image_src" id="pYIc(XM@1nR2n)92%A;es">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="src">
                                                                                                  <block type="string_value" id="nky@2i}N.~a;DnikVw2wL">
                                                                                                    <field name="value">${image_path}map.png</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="variable" id="`W}/gTjaQzRVYy@1yq,t)">
                                                                                    <field name="name">i</field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="Y$DMS?HX2D8I_eJ|bhf@2">
                                                                                        <field name="value">0</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="while_do_block" id="%3hK5Xe[6WZSMEaane8V">
                                                                                        <value name="while">
                                                                                          <block type="expression" id="CtiVQ/CdlR3vu)#gmjCk">
                                                                                            <field name="value">i &lt; city.length</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <statement name="do">
                                                                                          <block type="image_shape" id="v283~2g2^3J!V7ZrKfPc">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_position" id="d9#vf~3)ZO1`IN]51`uG">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="k/3ghq8`JxQhiQJkqC{_">
                                                                                                    <field name="value">540</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id="KQj@1;v1OJ](3WUb65k[K">
                                                                                                    <field name="value">220</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_size" id="?d7.YU`mTYwO:_R)zk?M">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="width">
                                                                                                      <block type="expression" id=",Vk-d|ZD686E@2A3:qs)l">
                                                                                                        <field name="value">0</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="height">
                                                                                                      <block type="expression" id="e1~d`yUX:qj4Rk.J=H{j">
                                                                                                        <field name="value">0</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_image_key" id="r%vD/A6RftW13y;:47A(">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="key">
                                                                                                          <block type="string_value" id="S3}@2zgWy?xaf?+Qj1{`!">
                                                                                                            <field name="value">${city[i]}.png</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_image_src" id="Ue6s^[MyBXY,d5}HA}`^">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="src">
                                                                                                              <block type="string_value" id="@2+D:2JM$-`M{W-GBRvhl">
                                                                                                                <field name="value">${image_path}${city[i]}.png</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="variable" id=";=mc@14JWuis)?,cY:532">
                                                                                                <field name="name">i</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="-!g?1Rhc@1VvDvi-y+F^D">
                                                                                                    <field name="value">i+1</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="list_shape" id="1=u`7HGNI9d?8LUb)#Q|">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_list_direction" id="g/L;Wta;[Hk}9SjD-#jh">
                                                                                                <field name="#prop"></field>
                                                                                                <field name="dir">horizontal</field>
                                                                                                <next>
                                                                                                  <block type="prop_position" id="|HzF@2?1!(uewpfm$S/X;">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="5)m19GwDiD/m|M[|Jd$;">
                                                                                                        <field name="value">400</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="BE5k6r_8X94#k2p/7(?;">
                                                                                                        <field name="value">400</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_anchor" id="zswT@15SR8:H).y~(2n,^">
                                                                                                        <field name="#prop">anchor</field>
                                                                                                        <value name="x">
                                                                                                          <block type="expression" id="KlNEcEGAuKxCYJKQgmAu">
                                                                                                            <field name="value">0.5</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id=":kNwqkZpm0@1;2H,^c4[Q">
                                                                                                            <field name="value">0.5</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_spacing" id="s@1xxhdnC~UJ_H`ZhnAI_">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="spacing">
                                                                                                              <block type="expression" id="9EUct(G0sZ#yha[wUC;e">
                                                                                                                <field name="value">10</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <statement name="items">
                                                                                              <block type="list_item_shape" id="Yx6O8e#@1X%}fODxpw#Pr">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_list_align" id="o(FfguaH~zLQxf@24!#(I">
                                                                                                    <field name="#prop"></field>
                                                                                                    <field name="align">middle</field>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <statement name="template">
                                                                                                  <block type="text_shape" id="i=.45JN2+GQT+)05_wgn">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_text_contents" id="CF3!b1lhZ^-PGY`U@2@2(s">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="contents">
                                                                                                          <block type="string_value" id="T:K9Z#trQYAqe@2G0LITb">
                                                                                                            <field name="value">${city_name[city[0]]}</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style" id="pWW[F@1_j0C6dI;07w6">
                                                                                                            <field name="base">text</field>
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_text_style_font_size" id="lo]kI7v!f15ZaprbbU=!">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="fontSize">
                                                                                                                  <block type="expression" id="uE1t:Li)ePp0Ju}1z3g:">
                                                                                                                    <field name="value">36</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style_fill" id="oJ7czNeT(lMwaw]4Fm-l">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="fill">
                                                                                                                      <block type="string_value" id="U=9I~^Y?]t`S;awPB]?z">
                                                                                                                        <field name="value">black</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style_stroke" id="a@2#mA(5ae35l3w0C[Le$">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="stroke">
                                                                                                                          <block type="string_value" id="aig_bT|?qikiMXql#Y|I">
                                                                                                                            <field name="value">white</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_stroke_thickness" id="kE!|J;@2QRt}Eg,r-gpZ#">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="strokeThickness">
                                                                                                                              <block type="expression" id="[#5vqp88_-.l?RP/~P-L">
                                                                                                                                <field name="value">2</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="list_item_shape" id="5g(-gJEo5cs5~g[Tzf/K">
                                                                                                    <statement name="#props">
                                                                                                      <block type="prop_list_align" id="l0=raYoy[(VXIb7WW-x0">
                                                                                                        <field name="#prop"></field>
                                                                                                        <field name="align">middle</field>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <statement name="template">
                                                                                                      <block type="image_shape" id="4~%%c[-khH`6+=db(_DM">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_image_key" id="!^.E@1@2.9LzZz%f)`_cvR">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="key">
                                                                                                              <block type="string_value" id="A;gWE{nwzNX,]-(w](6S">
                                                                                                                <field name="value">box.png</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_image_src" id="PXvoMfp7O/:7b[[~B:Ae">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="src">
                                                                                                                  <block type="string_value" id="W7.~y?aa39K^@2n#u|?wb">
                                                                                                                    <field name="value">${image_path}box.png</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="drop_shape" id="(A0mNtAq:R{=Es0Y?I$k">
                                                                                                            <field name="multiple">FALSE</field>
                                                                                                            <field name="resultMode">default</field>
                                                                                                            <field name="groupName"></field>
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_size" id="fOP@2DY(}d$`$o.:2)s!J">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="width">
                                                                                                                  <block type="expression" id="PanZA:2^@2eHpzV1IIPDu">
                                                                                                                    <field name="value">114</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="height">
                                                                                                                  <block type="expression" id="jh,2goF;a!ECi:ewe)qk">
                                                                                                                    <field name="value">60</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_value" id="j??4vtJLMs/wja}UOvFN">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="CZarixsrl@2~;^pzGG]r3">
                                                                                                                        <field name="value">city_location[city[0]]</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="list_item_shape" id="/3OIYF1J=UNWx?]evBY!">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_list_align" id="ytV4#o6a([vBDz/OrCGH">
                                                                                                            <field name="#prop"></field>
                                                                                                            <field name="align">middle</field>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <statement name="template">
                                                                                                          <block type="text_shape" id="h(xtGr+;Hs(v68G={qS)">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_text_contents" id="n36~tZxoAe|Udjh4;dUW">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="contents">
                                                                                                                  <block type="string_value" id="U{G_0_lg=djBhWSk7Dro">
                                                                                                                    <field name="value">${'  '}${city_name[city[1]]}</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style" id="yQZ_~FZj[6cX1S]]nCE(">
                                                                                                                    <field name="base">text</field>
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_text_style_font_size" id="c5Qx8U@2y7Mn48b:];Pky">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="fontSize">
                                                                                                                          <block type="expression" id="c_l.GM.a|,zLv^A}lZGj">
                                                                                                                            <field name="value">36</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_fill" id="PH|7|fYzd?+@2)%/Dr,}C">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="fill">
                                                                                                                              <block type="string_value" id="ShrJ^#Zu^IzEGr(TQ96v">
                                                                                                                                <field name="value">black</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_stroke" id="d,-^!fK=80jcTEY0AIX@2">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="stroke">
                                                                                                                                  <block type="string_value" id="QI}rztvG}]mBcdH7-jFq">
                                                                                                                                    <field name="value">white</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_stroke_thickness" id="Rhtp9bCmV4!B|W3eA3uA">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="strokeThickness">
                                                                                                                                      <block type="expression" id="/F!!s+M38mBNz(82Sm1D">
                                                                                                                                        <field name="value">2</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="list_item_shape" id="fc1MDbu),AyZP-s@14/Hv">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_list_align" id=";OR]H|@2RmE1qZ@2$/AAFl">
                                                                                                                <field name="#prop"></field>
                                                                                                                <field name="align">middle</field>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <statement name="template">
                                                                                                              <block type="image_shape" id="kMO`ZS`_vs$nf~s5mqyt">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_image_key" id="d)j$y=yLN1f?GpDCm)e3">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="key">
                                                                                                                      <block type="string_value" id="ha0#$=@15[!SH%;A2XR:J">
                                                                                                                        <field name="value">box.png</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_src" id="36~|@2hl)~0bZ4$dIGEL.">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="src">
                                                                                                                          <block type="string_value" id="EH~l_L9UkRFC|O)4s)/n">
                                                                                                                            <field name="value">${image_path}box.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="drop_shape" id="W}G[HnU|]A}eg=1ipO^@1">
                                                                                                                    <field name="multiple">FALSE</field>
                                                                                                                    <field name="resultMode">default</field>
                                                                                                                    <field name="groupName"></field>
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_position" id="wp@1+4@2q{HPh].Get]BRY">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="OIOWP7$v$Pyq2hZ[x^;^">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id="C{H,nBvkysF_dE8I#a.H">
                                                                                                                            <field name="value">0</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_size" id="Cstj9w10xQ;ypa8N{TcR">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="width">
                                                                                                                              <block type="expression" id="a9@1Wk[/P@11M;9p9Y5u2w">
                                                                                                                                <field name="value">114</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="height">
                                                                                                                              <block type="expression" id="uBVJn{y#o|!)GDNK$SKz">
                                                                                                                                <field name="value">60</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_value" id="03([v1EwfCLu@12u]Yi1W">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="K9KP9lM=_,oFgxOi@1|9D">
                                                                                                                                    <field name="value">city_location[city[1]]</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="grid_shape" id="6tU?K^N8ec~A@1@1NwQ72N">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_grid_dimension" id="X4K:|9dKK%s9l4n5v@2lG">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="rows">
                                                                                                      <block type="expression" id="[Ev!.bxnzjc|-33v.%7=">
                                                                                                        <field name="value">3</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="cols">
                                                                                                      <block type="expression" id="u$7]/V/!ZWM4(Va??@2({">
                                                                                                        <field name="value">2</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_position" id="zLld84TfBg#!88]1,JD-">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="x">
                                                                                                          <block type="expression" id="Cd6g{Y_P?:GyVyvQi1wJ">
                                                                                                            <field name="value">150</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="y">
                                                                                                          <block type="expression" id=".F}E71+@1i)77!yQ`hmfu">
                                                                                                            <field name="value">220</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_size" id="+o2CWr^.ohx)_bcIG53]">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="width">
                                                                                                              <block type="expression" id="Z[x5iHG%KWC%KWQb}c-G">
                                                                                                                <field name="value">250</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="height">
                                                                                                              <block type="expression" id="@2E|Ob@1jF#gYtX8-%Iui@1">
                                                                                                                <field name="value">220</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_anchor" id="1WJjXY^.h,t]eNgxaJ}q">
                                                                                                                <field name="#prop">anchor</field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="DHx[vqs?.?nUD:AYPJWd">
                                                                                                                    <field name="value">0.5</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="e3D|RR7w!=Sufu):zV`c">
                                                                                                                    <field name="value">0.5</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_grid_cell_source" id="v8+Yu211Qg8fX9Mu4th`">
                                                                                                                    <field name="#prop">cell.source</field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="~j@1,^liddf}oDg}E91^i">
                                                                                                                        <field name="value">location</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_grid_random" id="}?kS#x[b)9OGb@1`UdJGV">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <field name="random">FALSE</field>
                                                                                                                        <next>
                                                                                                                          <block type="prop_grid_show_borders" id=";y^wk4y%[VWjXOj~vrQZ">
                                                                                                                            <field name="#prop">#showBorders</field>
                                                                                                                            <field name="value">FALSE</field>
                                                                                                                            <next>
                                                                                                                              <block type="prop_grid_cell_template" id="7^6]Kv:2yrZi`cooAW2@2">
                                                                                                                                <field name="variable">$cell</field>
                                                                                                                                <field name="#prop">cell.template</field>
                                                                                                                                <field name="#callback">$cell</field>
                                                                                                                                <statement name="body">
                                                                                                                                  <block type="choice_custom_shape" id="vzlC3MrIK1Dc}i;)5{,@2">
                                                                                                                                    <field name="action">drag</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="c_L__4FSTo-R:^A:Jh/O">
                                                                                                                                        <field name="value">$cell.data</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <statement name="template">
                                                                                                                                      <block type="image_shape" id="0!.BnmWS4=VL1HnC3T8U">
                                                                                                                                        <statement name="#props">
                                                                                                                                          <block type="prop_position" id="T.$md=!ZE32)w)|0}QI@1">
                                                                                                                                            <field name="#prop"></field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id="Xo,TGEdw@2-a_GCjQSh^C">
                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="WJ$(Weiy$~|Y[GWvF@2Qv">
                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_size" id=")|14szEhie=}k?pMT+xx">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="width">
                                                                                                                                                  <block type="expression" id="Iph@1(}m,(kT]UGUB@1HFg">
                                                                                                                                                    <field name="value">114</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <value name="height">
                                                                                                                                                  <block type="expression" id="?;0iRN.pRwm=6.i{G6_b">
                                                                                                                                                    <field name="value">60</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_image_key" id=";ir)eA]xUfVlnb@1r)R1.">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="key">
                                                                                                                                                      <block type="string_value" id="O~%Z+!/IrGz2fU//tbc^">
                                                                                                                                                        <field name="value">shape.png</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_image_src" id="AxIsHJ6jNkQ7zZDUSU3?">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="src">
                                                                                                                                                          <block type="string_value" id="X3l8!{B[3R!;:b#G6?wb">
                                                                                                                                                            <field name="value">${image_path}shape.png</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </statement>
                                                                                                                                        <next>
                                                                                                                                          <block type="text_shape" id="$4otcCY]-Z^65~b-~N(x">
                                                                                                                                            <statement name="#props">
                                                                                                                                              <block type="prop_position" id="Tc2ouu0NM-D`Q5TVT{MQ">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <value name="x">
                                                                                                                                                  <block type="expression" id="8;bRZWqc%T,EH(@1AlgWY">
                                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <value name="y">
                                                                                                                                                  <block type="expression" id="p3m{g@2,Rf^dP%NcA[LAb">
                                                                                                                                                    <field name="value">$cell.centerY + 3</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_text_contents" id="~Vl?5$lS!EXZ1|kH((8n">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <value name="contents">
                                                                                                                                                      <block type="string_value" id="#6btV9p,PGV}wMx.U�">
                                                                                                                                                        <field name="value">${$cell.data}</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_text_style" id="$o^Cd}^(1s0V,G-2$7rd">
                                                                                                                                                        <field name="base">text</field>
                                                                                                                                                        <statement name="#props">
                                                                                                                                                          <block type="prop_text_style_font_size" id="9NTf{8?~OI#Kqvt]zHa=">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="fontSize">
                                                                                                                                                              <block type="expression" id="cIiYM/aJyMZU[wu3-`k^">
                                                                                                                                                                <field name="value">36</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_text_style_fill" id="ouuy}u#]W2ls-^c@2+-dp">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="fill">
                                                                                                                                                                  <block type="string_value" id="9@2]@2PB@2{;#r@1G?fJ0sVf">
                                                                                                                                                                    <field name="value">black</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="variable" id="UNl:+VXI``U~U5FQO1_P">
                                                                                                    <field name="name">Variable</field>
                                                                                                    <value name="value">
                                                                                                      <block type="custom_image_list" id="?KjN/$fxoD5IfrNyDw4C">
                                                                                                        <field name="link">${image_path}</field>
                                                                                                        <field name="images">ex${city[0]}|ex${city[1]}|map</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="partial_explanation" id="7#5h0N`7ET`0J,__2W;7" inline="true">
                                                                                                        <value name="value">
                                                                                                          <block type="string_value" id="U/[@2P3SK}H!Oqb3q}I10">
                                                                                                            <field name="value">&lt;center&gt;&lt;u&gt;Let's answer ${city_name[city[0]]} location&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;div style='position: relative; width: 487px; height: 260px;'&gt;
&lt;img src='@1sprite.src(map.png)' style='position: absolute; left: 0px'/&gt;
                                                                                                            </field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="variable" id="aa]@1xS?aT,x[0X@1;ouo{">
                                                                                                            <field name="name">i</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="u~[GT-IOsH;x2_W_U1pC">
                                                                                                                <field name="value">0</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="while_do_block" id="Jx=u4X60Z=qRFb$Zb/vz">
                                                                                                                <value name="while">
                                                                                                                  <block type="expression" id="[Fz!Ku)OJqJtp(.Vm~9T">
                                                                                                                    <field name="value">i &lt; city.length</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <statement name="do">
                                                                                                                  <block type="partial_explanation" id="B5{e8dN-0T.`es/v)H}J" inline="true">
                                                                                                                    <value name="value">
                                                                                                                      <block type="string_value" id="m_6cJ?zWc}A,r|~oe.A,">
                                                                                                                        <field name="value">&lt;img src='@1sprite.src(${city[i]}.png)' style='position: absolute; left: 0px'/&gt;</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="variable" id="rdD.A+ZdW(@1cL8LxsZ%`">
                                                                                                                        <field name="name">i</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id=":Y`nAD=?QC[g_h^aHI)a">
                                                                                                                            <field name="value">i+1</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="partial_explanation" id=",PKBv;5^:F@1piQ#!w2.)" inline="true">
                                                                                                                    <value name="value">
                                                                                                                      <block type="string_value" id="vZ)wgy%W;a|ImP`mQHa^">
                                                                                                                        <field name="value">&lt;img src='@1sprite.src(ex${city[0]}.png)' style='position: absolute; left: 0px'/&gt;
&lt;/div&gt;
&lt;/center&gt;
&lt;span style='background: rgb(250, 250, 250, 0.3)'&gt;
Vertically, you can see where ${city_name[city[0]]} is &lt;b&gt;${city_location[city[0]][0]}&lt;/b&gt;&lt;/br&gt;
Horizontally, you can see where ${city_name[city[0]]} is &lt;b&gt;${city_location[city[0]][1]}&lt;/b&gt;&lt;/br&gt;&lt;/br&gt;
&lt;center&gt;&lt;span style='background: rgb(250, 250, 250, 0.3)'&gt;So now you know, ${city_name[city[0]]} is &lt;b&gt;${city_location[city[0]]}&lt;/b&gt;&lt;/center&gt;&lt;/span&gt;
                                                                                                                        </field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="end_partial_explanation" id="@2aBRh#|,p(Hldb1!A2r{">
                                                                                                                        <next>
                                                                                                                          <block type="partial_explanation" id="Lm)TKrb3KdqK@1,MKFnhX" inline="true">
                                                                                                                            <value name="value">
                                                                                                                              <block type="string_value" id="c[hSZOb|=9Fj9~~~?mpw">
                                                                                                                                <field name="value">&lt;center&gt;&lt;u&gt;For ${city_name[city[1]]}&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;div style='position: relative; width: 487px; height: 260px;'&gt;
&lt;img src='@1sprite.src(map.png)' style='position: absolute; left: 0px'/&gt;
                                                                                                                                </field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="variable" id="Q],Kv{;u%:7bGP}CdUPH">
                                                                                                                                <field name="name">i</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="`am^kc)=W7Ayfp$Fv3@2T">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="while_do_block" id="t}eq2cGQn5]vB3W(Vb|v">
                                                                                                                                    <value name="while">
                                                                                                                                      <block type="expression" id="4;)kq]Y3$plW5[03)$A7">
                                                                                                                                        <field name="value">i &lt; city.length</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <statement name="do">
                                                                                                                                      <block type="partial_explanation" id="p($^[n5kA]mJV,Q=-BZ:" inline="true">
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="string_value" id="Cf$~$N!Y=wFLlxQ?BF]B">
                                                                                                                                            <field name="value">&lt;img src='@1sprite.src(${city[i]}.png)' style='position: absolute; left: 0px'/&gt;</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="variable" id="Ize4%@1%9o+s@2Olp+%|xI">
                                                                                                                                            <field name="name">i</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id="+|((UB(_-y]))2iE`Z26">
                                                                                                                                                <field name="value">i+1</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="partial_explanation" id="Of7hr$}yEE@2Fz=,P1XX9" inline="true">
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="string_value" id="BH,%hkOfjS$`|y;?78NG">
                                                                                                                                            <field name="value">&lt;img src='@1sprite.src(ex${city[1]}.png)' style='position: absolute; left: 0px'/&gt;
&lt;/div&gt;
&lt;/center&gt;
&lt;span style='background: rgb(250, 250, 250, 0.3)'&gt;
Vertically, you can see where ${city_name[city[1]]} is &lt;b&gt;${city_location[city[1]][0]}&lt;/b&gt;&lt;/br&gt;
Horizontally, you can see where ${city_name[city[1]]} is &lt;b&gt;${city_location[city[1]][1]}&lt;/b&gt;&lt;/br&gt;&lt;/br&gt;
&lt;center&gt;&lt;span style='background: rgb(250, 250, 250, 0.3)'&gt;So again, now you know where ${city_name[city[1]]} is &lt;b&gt;${city_location[city[1]]}&lt;/b&gt;&lt;/center&gt;&lt;/span&gt;
                                                                                                                                            </field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
</xml>
END_XML]] */