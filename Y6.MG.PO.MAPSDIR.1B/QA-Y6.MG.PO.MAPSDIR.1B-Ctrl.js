
module.exports = [
  {
    "#type": "question",
    "name": "Y6.MG.PO.MAPSDIR.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "concept_code",
        "value": "Y6.MG.PO.MAPSDIR.1B"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/${concept_code}/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "range < 0"
        },
        "then": [
          {
            "#type": "variable",
            "name": "city",
            "value": {
              "#type": "random_many",
              "count": {
                "#type": "expression",
                "value": "5"
              },
              "items": {
                "#type": "func",
                "name": "arrayOfNumber",
                "args": [
                  {
                    "#type": "expression",
                    "value": "20"
                  },
                  {
                    "#type": "expression",
                    "value": "0"
                  }
                ]
              }
            }
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "range < 4"
            },
            "then": [
              {
                "#type": "variable",
                "name": "city",
                "value": {
                  "#type": "random_many",
                  "count": {
                    "#type": "expression",
                    "value": "10"
                  },
                  "items": {
                    "#type": "func",
                    "name": "arrayOfNumber",
                    "args": [
                      {
                        "#type": "expression",
                        "value": "20"
                      },
                      {
                        "#type": "expression",
                        "value": "0"
                      }
                    ]
                  }
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "city",
                "value": {
                  "#type": "random_many",
                  "count": {
                    "#type": "expression",
                    "value": "20"
                  },
                  "items": {
                    "#type": "func",
                    "name": "arrayOfNumber",
                    "args": [
                      {
                        "#type": "expression",
                        "value": "20"
                      },
                      {
                        "#type": "expression",
                        "value": "0"
                      }
                    ]
                  }
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "statement",
        "value": "function check_exist(x, A){\n    for(var i=0; i<A.length; i++){\n        if(x == A[i]){\n            return 1;\n        }\n    }\n    return 0;\n}"
      },
      {
        "#type": "variable",
        "name": "city_name",
        "value": {
          "#type": "string_array",
          "items": "Rio de Janeiro|Lima|Cape Town|Busno Aires|Sydney|Perth|Jakarta|Tripoli|Caracas|Luanda|Riyadh|Ha Noi|New Delhi|Beijing|Ulaanbaatar|Mascow|Berlin|London|Nuuk|San Francisco"
        }
      },
      {
        "#type": "variable",
        "name": "city_location",
        "value": {
          "#type": "string_array",
          "items": "E4|D4|G5|E5|L5|J5|J4|G3|D3|G4|H3|J3|I3|K2|J2|H2|G2|F2|D2|B2"
        }
      },
      {
        "#type": "variable",
        "name": "location",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "type",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "2"
          }
        }
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < 5"
            },
            "do": [
              {
                "#type": "variable",
                "name": "name",
                "value": {
                  "#type": "random_many",
                  "count": {
                    "#type": "expression",
                    "value": "5"
                  },
                  "items": {
                    "#type": "expression",
                    "value": "city_name"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "statement",
        "value": "if(type == 1 && check_exist(city_name[city[0]], name) == 0){\n  name.push(city_name[city[0]]);\n  name.splice(0, 1);\n}"
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "variable",
            "name": "name",
            "value": {
              "#type": "func",
              "name": "shuffle",
              "args": [
                {
                  "#type": "expression",
                  "value": "name"
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 2"
        },
        "then": [
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < 5"
            },
            "do": [
              {
                "#type": "do_while_block",
                "do": [
                  {
                    "#type": "variable",
                    "name": "X",
                    "value": {
                      "#type": "random_one",
                      "items": {
                        "#type": "expression",
                        "value": "['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L']"
                      }
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "Y",
                    "value": {
                      "#type": "random_number",
                      "min": {
                        "#type": "expression",
                        "value": "1"
                      },
                      "max": {
                        "#type": "expression",
                        "value": "6"
                      }
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "temp",
                    "value": {
                      "#type": "expression",
                      "value": "X+Y"
                    }
                  }
                ],
                "while": {
                  "#type": "expression",
                  "value": "check_exist(temp, location) == 1"
                }
              },
              {
                "#type": "variable",
                "name": "location[i]",
                "value": {
                  "#type": "expression",
                  "value": "temp"
                }
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "statement",
        "value": "if(type == 2 && check_exist(city_location[city[0]], location) == 0){\n  location.push(city_location[city[0]]);\n  location.splice(0, 1);\n}"
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 2"
        },
        "then": [
          {
            "#type": "variable",
            "name": "location",
            "value": {
              "#type": "func",
              "name": "shuffle",
              "args": [
                {
                  "#type": "expression",
                  "value": "location"
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 1"
        },
        "then": [
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "540"
                },
                "y": {
                  "#type": "expression",
                  "value": "230"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "0"
                },
                "height": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "map.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}map.png"
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < city.length"
            },
            "do": [
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "540"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "230"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "0"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "${city[i]}.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}${city[i]}.png"
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "40"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "Choose the city inside this coordinate ${city_location[city[0]]}."
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "36"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "5"
                },
                "cols": {
                  "#type": "expression",
                  "value": "1"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "160"
                },
                "y": {
                  "#type": "expression",
                  "value": "250"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "250"
                },
                "height": {
                  "#type": "expression",
                  "value": "360"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "name"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "choice_custom_shape",
                    "action": "click-one",
                    "value": {
                      "#type": "expression",
                      "value": "$cell.data"
                    },
                    "template": {
                      "#callback": "$choice",
                      "variable": "$choice",
                      "body": [
                        {
                          "#type": "image_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "$cell.centerX"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "$cell.centerY"
                              }
                            },
                            {
                              "#type": "prop_size",
                              "#prop": "",
                              "width": {
                                "#type": "expression",
                                "value": "220"
                              },
                              "height": {
                                "#type": "expression",
                                "value": "50"
                              }
                            },
                            {
                              "#type": "prop_image_key",
                              "#prop": "",
                              "key": "shape.png"
                            },
                            {
                              "#type": "prop_image_src",
                              "#prop": "",
                              "src": "${image_path}shape.png"
                            }
                          ]
                        },
                        {
                          "#type": "text_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "$cell.centerX"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "$cell.centerY + 3"
                              }
                            },
                            {
                              "#type": "prop_text_contents",
                              "#prop": "",
                              "contents": "${$cell.data}"
                            },
                            {
                              "#prop": "",
                              "style": {
                                "#type": "json",
                                "base": "text",
                                "#props": [
                                  {
                                    "#type": "prop_text_style_font_size",
                                    "#prop": "",
                                    "fontSize": {
                                      "#type": "expression",
                                      "value": "28"
                                    }
                                  },
                                  {
                                    "#type": "prop_text_style_fill",
                                    "#prop": "",
                                    "fill": "black"
                                  }
                                ]
                              }
                            }
                          ]
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "func",
            "name": "addAnswers",
            "args": [
              {
                "#type": "expression",
                "value": "city_name[city[0]]"
              }
            ]
          },
          {
            "#type": "variable",
            "name": "Variable",
            "value": {
              "#type": "custom_image_list",
              "link": "${image_path}",
              "images": "2ex${city[0]}"
            }
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<center><u>Let's answer ${city_name[city[0]]} location</u></br></br>\n\n<div style='position: relative; width: 487px; height: 260px;'>\n<img src='@sprite.src(map.png)' style='position: absolute; left: 0px'/>"
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < city.length"
            },
            "do": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<img src='@sprite.src(${city[i]}.png)' style='position: absolute; left: 0px'/>"
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<img src='@sprite.src(2ex${city[0]}.png)' style='position: absolute; left: 0px'/>\n</div>\n</center>\n<span style='background: rgb(250, 250, 250, 0.3)'>\nVertically, you can see which line ${city_location[city[0]][0]} is <b>${city_name[city[0]]}</b></br>\nHorizontally, you can see which line ${city_location[city[0]][1]} is <b>${city_name[city[0]]}</b></br></br>\n<center><span style='background: rgb(250, 250, 250, 0.3)'>On the spot gone through both ${city_location[city[0]][0]} and ${city_location[city[0]][1]} contains <b>${city_name[city[0]]}</b>!</center></span>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "type == 2"
        },
        "then": [
          {
            "#type": "image_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "210"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "0"
                },
                "height": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "prop_image_key",
                "#prop": "",
                "key": "map.png"
              },
              {
                "#type": "prop_image_src",
                "#prop": "",
                "src": "${image_path}map.png"
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < city.length"
            },
            "do": [
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "400"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "210"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "0"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "${city[i]}.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}${city[i]}.png"
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "text_shape",
            "#props": [
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "40"
                }
              },
              {
                "#type": "prop_text_contents",
                "#prop": "",
                "contents": "Choose the coordinates for ${city_name[city[0]]}."
              },
              {
                "#prop": "",
                "style": {
                  "#type": "json",
                  "base": "text",
                  "#props": [
                    {
                      "#type": "prop_text_style_font_size",
                      "#prop": "",
                      "fontSize": {
                        "#type": "expression",
                        "value": "36"
                      }
                    },
                    {
                      "#type": "prop_text_style_fill",
                      "#prop": "",
                      "fill": "black"
                    },
                    {
                      "#type": "prop_text_style_stroke",
                      "#prop": "",
                      "stroke": "white"
                    },
                    {
                      "#type": "prop_text_style_stroke_thickness",
                      "#prop": "",
                      "strokeThickness": {
                        "#type": "expression",
                        "value": "2"
                      }
                    }
                  ]
                }
              }
            ]
          },
          {
            "#type": "grid_shape",
            "#props": [
              {
                "#type": "prop_grid_dimension",
                "#prop": "",
                "rows": {
                  "#type": "expression",
                  "value": "1"
                },
                "cols": {
                  "#type": "expression",
                  "value": "5"
                }
              },
              {
                "#type": "prop_position",
                "#prop": "",
                "x": {
                  "#type": "expression",
                  "value": "400"
                },
                "y": {
                  "#type": "expression",
                  "value": "400"
                }
              },
              {
                "#type": "prop_size",
                "#prop": "",
                "width": {
                  "#type": "expression",
                  "value": "700"
                },
                "height": {
                  "#type": "expression",
                  "value": "80"
                }
              },
              {
                "#type": "prop_anchor",
                "#prop": "anchor",
                "x": {
                  "#type": "expression",
                  "value": "0.5"
                },
                "y": {
                  "#type": "expression",
                  "value": "0.5"
                }
              },
              {
                "#type": "prop_grid_cell_source",
                "#prop": "cell.source",
                "value": {
                  "#type": "expression",
                  "value": "location"
                }
              },
              {
                "#type": "prop_grid_random",
                "#prop": "",
                "random": false
              },
              {
                "#type": "prop_grid_show_borders",
                "#prop": "#showBorders",
                "value": false
              },
              {
                "#type": "prop_grid_cell_template",
                "variable": "$cell",
                "#prop": "cell.template",
                "#callback": "$cell",
                "body": [
                  {
                    "#type": "choice_custom_shape",
                    "action": "click-one",
                    "value": {
                      "#type": "expression",
                      "value": "$cell.data"
                    },
                    "template": {
                      "#callback": "$choice",
                      "variable": "$choice",
                      "body": [
                        {
                          "#type": "image_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "$cell.centerX"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "$cell.centerY"
                              }
                            },
                            {
                              "#type": "prop_size",
                              "#prop": "",
                              "width": {
                                "#type": "expression",
                                "value": "114"
                              },
                              "height": {
                                "#type": "expression",
                                "value": "60"
                              }
                            },
                            {
                              "#type": "prop_image_key",
                              "#prop": "",
                              "key": "shape.png"
                            },
                            {
                              "#type": "prop_image_src",
                              "#prop": "",
                              "src": "${image_path}shape.png"
                            }
                          ]
                        },
                        {
                          "#type": "text_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "$cell.centerX"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "$cell.centerY + 3"
                              }
                            },
                            {
                              "#type": "prop_text_contents",
                              "#prop": "",
                              "contents": "${$cell.data}"
                            },
                            {
                              "#prop": "",
                              "style": {
                                "#type": "json",
                                "base": "text",
                                "#props": [
                                  {
                                    "#type": "prop_text_style_font_size",
                                    "#prop": "",
                                    "fontSize": {
                                      "#type": "expression",
                                      "value": "36"
                                    }
                                  },
                                  {
                                    "#type": "prop_text_style_fill",
                                    "#prop": "",
                                    "fill": "black"
                                  }
                                ]
                              }
                            }
                          ]
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "func",
            "name": "addAnswers",
            "args": [
              {
                "#type": "expression",
                "value": "city_location[city[0]]"
              }
            ]
          },
          {
            "#type": "variable",
            "name": "Variable",
            "value": {
              "#type": "custom_image_list",
              "link": "${image_path}",
              "images": "ex${city[0]}|ex${city[1]}|map"
            }
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<center><u>Let's answer ${city_name[city[0]]} location</u></br></br>\n\n<div style='position: relative; width: 487px; height: 260px;'>\n<img src='@sprite.src(map.png)' style='position: absolute; left: 0px'/>"
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i < city.length"
            },
            "do": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<img src='@sprite.src(${city[i]}.png)' style='position: absolute; left: 0px'/>"
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "<img src='@sprite.src(ex${city[0]}.png)' style='position: absolute; left: 0px'/>\n</div>\n</center>\n<span style='background: rgb(250, 250, 250, 0.3)'>\nVertically, you can see where ${city_name[city[0]]} is <b>${city_location[city[0]][0]}</b></br>\nHorizontally, you can see where ${city_name[city[0]]} is <b>${city_location[city[0]][1]}</b></br></br>\n<center><span style='background: rgb(250, 250, 250, 0.3)'>So now you know, ${city_name[city[0]]} is <b>${city_location[city[0]]}</b></center></span>"
            ]
          },
          {
            "#type": "func",
            "name": "endPartialExplanation",
            "args": []
          }
        ]
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="70NGEtfpIbwkCl,F9lWW" x="0" y="0">
    <field name="name">Y6.MG.PO.MAPSDIR.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="Oj6`Os(m,tTwHN:?-|zC">
        <field name="name">concept_code</field>
        <value name="value">
          <block type="string_value" id="zM=!wJG$|ZA(Ej+xq+Ag">
            <field name="value">Y6.MG.PO.MAPSDIR.1B</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="|3pnQ{zP0!C5{f#ffWAo">
                <field name="name">image_path</field>
                <value name="value">
                  <block type="string_value" id="u={MR7g8SVGdxb[`F`vB">
                    <field name="value">Develop/ImageQAs/${concept_code}/</field>
                  </block>
                </value>
                <next>
                  <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                    <field name="name">drop_background</field>
                    <value name="value">
                      <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                        <value name="min">
                          <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                            <field name="value">1</field>
                          </block>
                        </value>
                        <value name="max">
                          <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                            <field name="value">15</field>
                          </block>
                        </value>
                      </block>
                    </value>
                    <next>
                      <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                        <statement name="#props">
                          <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                            <field name="#prop"></field>
                            <value name="key">
                              <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                                <field name="value">bg${drop_background}.png</field>
                              </block>
                            </value>
                            <next>
                              <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                                <field name="#prop"></field>
                                <value name="src">
                                  <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                    <field name="value">${background_path}bg${drop_background}.png</field>
                                  </block>
                                </value>
                              </block>
                            </next>
                          </block>
                        </statement>
                        <next>
                          <block type="input_param" id="+b%GrQqrg-mP/}-l~o9}" inline="true">
                            <field name="name">numberOfCorrect</field>
                            <value name="value">
                              <block type="expression" id="4ARiVx/vd/{5h{?qvB~F">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="input_param" id="[hAMWj@1C3BN!#+_?@23Pl" inline="true">
                                <field name="name">numberOfIncorrect</field>
                                <value name="value">
                                  <block type="expression" id="UEtcRk04B2FVq{M?fs{^">
                                    <field name="value">0</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="YepMQT@2uwp2rRPvmnRg1">
                                    <field name="name">range</field>
                                    <value name="value">
                                      <block type="expression" id="UxgQ^Y5]rb}vpw[D=l-!">
                                        <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="if_then_else_block" id="qGoB8}}k~=0@2NhJ[#)pA">
                                        <value name="if">
                                          <block type="expression" id="-/+VZ4j}Gvgl}K_2}Bd`">
                                            <field name="value">range &lt; 0</field>
                                          </block>
                                        </value>
                                        <statement name="then">
                                          <block type="variable" id="#nl]|1oU0KRS8sbfk@2#-">
                                            <field name="name">city</field>
                                            <value name="value">
                                              <block type="random_many" id="OUuzEm%(^:lUK[-nZL_a">
                                                <value name="count">
                                                  <block type="expression" id="|ajz(T;@1j[kpUblgU$T,">
                                                    <field name="value">5</field>
                                                  </block>
                                                </value>
                                                <value name="items">
                                                  <block type="func_array_of_number" id=".5m-9jTQKCOx(=22UPkd" inline="true">
                                                    <value name="items">
                                                      <block type="expression" id="9fh}g4E@1@1/Ck.yiYeKes">
                                                        <field name="value">20</field>
                                                      </block>
                                                    </value>
                                                    <value name="from">
                                                      <block type="expression" id="0Uc;nXe2QO8(y?t(G#]m">
                                                        <field name="value">0</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                              </block>
                                            </value>
                                          </block>
                                        </statement>
                                        <statement name="else">
                                          <block type="if_then_else_block" id="lQjÃšP3vFr8^qK+y5$z">
                                            <value name="if">
                                              <block type="expression" id="1BD9)dz5GM15CF,b(G$g">
                                                <field name="value">range &lt; 4</field>
                                              </block>
                                            </value>
                                            <statement name="then">
                                              <block type="variable" id="c4;e#?sl4AG#mGXZ#)g5">
                                                <field name="name">city</field>
                                                <value name="value">
                                                  <block type="random_many" id="LxLLX]b7M4o[.B13Gw2m">
                                                    <value name="count">
                                                      <block type="expression" id="g=s9X%tNkGiihwN/0gA@2">
                                                        <field name="value">10</field>
                                                      </block>
                                                    </value>
                                                    <value name="items">
                                                      <block type="func_array_of_number" id="{WJ`@1ahki5?mf`[FU?-T" inline="true">
                                                        <value name="items">
                                                          <block type="expression" id="t3q(JDIHH@1mI4W`p(h%z">
                                                            <field name="value">20</field>
                                                          </block>
                                                        </value>
                                                        <value name="from">
                                                          <block type="expression" id="~`}.jz[nGqgmu,rQAtYx">
                                                            <field name="value">0</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                              </block>
                                            </statement>
                                            <statement name="else">
                                              <block type="variable" id="|4FyI;~M{_Pf.AuZvwF=">
                                                <field name="name">city</field>
                                                <value name="value">
                                                  <block type="random_many" id="4cg6.V[XFpNNptHW}K0p">
                                                    <value name="count">
                                                      <block type="expression" id="[p8Ez]I@1jwIrN`(X.h(P">
                                                        <field name="value">20</field>
                                                      </block>
                                                    </value>
                                                    <value name="items">
                                                      <block type="func_array_of_number" id="lze.U6JDhOBmNb!?`L=S" inline="true">
                                                        <value name="items">
                                                          <block type="expression" id="2kj3zKIoUrS?R_%-@10|N">
                                                            <field name="value">20</field>
                                                          </block>
                                                        </value>
                                                        <value name="from">
                                                          <block type="expression" id="XSP@14[k.{Dl/(G~RIxhO">
                                                            <field name="value">0</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                              </block>
                                            </statement>
                                          </block>
                                        </statement>
                                        <next>
                                          <block type="statement" id="Y]@1?Im{=aWK.rp,$x4r@2">
                                            <field name="value">function check_exist(x, A){
    for(var i=0; i&lt;A.length; i++){
        if(x == A[i]){
            return 1;
        }
    }
    return 0;
}
                                            </field>
                                            <next>
                                              <block type="variable" id="xwsW:qLGU4428sHtf2Qf">
                                                <field name="name">city_name</field>
                                                <value name="value">
                                                  <block type="string_array" id="~KpUmO:;cx,O`JRyb+#`">
                                                    <field name="items">Rio de Janeiro|Lima|Cape Town|Busno Aires|Sydney|Perth|Jakarta|Tripoli|Caracas|Luanda|Riyadh|Ha Noi|New Delhi|Beijing|Ulaanbaatar|Mascow|Berlin|London|Nuuk|San Francisco</field>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="|bI~uMT)$#INx=RVYE|.">
                                                    <field name="name">city_location</field>
                                                    <value name="value">
                                                      <block type="string_array" id=";_Da;eRw{F3:l;UallE4">
                                                        <field name="items">E4|D4|G5|E5|L5|J5|J4|G3|D3|G4|H3|J3|I3|K2|J2|H2|G2|F2|D2|B2</field>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id=".d^jlr9?@2dV:5R@2{(JUa">
                                                        <field name="name">location</field>
                                                        <value name="value">
                                                          <block type="expression" id="JFsTz@1ON$i5~Cn0kaECP">
                                                            <field name="value">[]</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="MtYekZSVf7YgF:T-x$P!">
                                                            <field name="name">type</field>
                                                            <value name="value">
                                                              <block type="random_number" id="(|:gKrd357^ao%`n.vUl">
                                                                <value name="min">
                                                                  <block type="expression" id="z3^7xsrhPE8:U_$N~p@2E">
                                                                    <field name="value">1</field>
                                                                  </block>
                                                                </value>
                                                                <value name="max">
                                                                  <block type="expression" id="(`4SvsUL=H|VAy$1kelf">
                                                                    <field name="value">2</field>
                                                                  </block>
                                                                </value>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="if_then_block" id="5YM1BF#R4`GAooW/V|yi">
                                                                <value name="if">
                                                                  <block type="expression" id="vcA=^,Z6zDaUzf_Hk^%Y">
                                                                    <field name="value">type == 1</field>
                                                                  </block>
                                                                </value>
                                                                <statement name="then">
                                                                  <block type="variable" id="/wICD`i=#dsl3z4:vg`n">
                                                                    <field name="name">i</field>
                                                                    <value name="value">
                                                                      <block type="expression" id="}ow_s}+~,I13cKxB?SWG">
                                                                        <field name="value">0</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="while_do_block" id="`qjtLZ/)%lCM]W+s?zbl">
                                                                        <value name="while">
                                                                          <block type="expression" id="p6o%~iFRjUMQ[$vqa2/u">
                                                                            <field name="value">i &lt; 5</field>
                                                                          </block>
                                                                        </value>
                                                                        <statement name="do">
                                                                          <block type="variable" id="zb:Gl)R]2Z}-RyQYaT#,">
                                                                            <field name="name">name</field>
                                                                            <value name="value">
                                                                              <block type="random_many" id="$pL/e]cHpOh)W4D)AiMZ">
                                                                                <value name="count">
                                                                                  <block type="expression" id="xZSWf,ts8M?iMW]@1`hGh">
                                                                                    <field name="value">5</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="items">
                                                                                  <block type="expression" id="QtKaYKM9CmDTm%dry;qf">
                                                                                    <field name="value">city_name</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="variable" id="{P`TC}duaW(Pj3_DUj9J">
                                                                                <field name="name">i</field>
                                                                                <value name="value">
                                                                                  <block type="expression" id="xpQLI[aVa4]C4@1`uD,$r">
                                                                                    <field name="value">i+1</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </statement>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <next>
                                                                  <block type="statement" id="|/sp_HY9YvfNwm^#u~md">
                                                                    <field name="value">if(type == 1 &amp;&amp; check_exist(city_name[city[0]], name) == 0){
  name.push(city_name[city[0]]);
  name.splice(0, 1);
}
                                                                    </field>
                                                                    <next>
                                                                      <block type="if_then_block" id="0e{zxHCwVoa=)6(t)q-}">
                                                                        <value name="if">
                                                                          <block type="expression" id="W$S?5?xa_pr)Zbi=K;%.">
                                                                            <field name="value">type == 1</field>
                                                                          </block>
                                                                        </value>
                                                                        <statement name="then">
                                                                          <block type="variable" id="8B+#JFZCCTXs/uKY#B#,">
                                                                            <field name="name">name</field>
                                                                            <value name="value">
                                                                              <block type="func_shuffle" id="1qq9?H)fSH~8p-qnDDhR" inline="true">
                                                                                <value name="value">
                                                                                  <block type="expression" id="ahGdj6]rKh0s#vHqR@1yb">
                                                                                    <field name="value">name</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </statement>
                                                                        <next>
                                                                          <block type="if_then_block" id=";Eh4LP3n7Hwz@1}z.1++g">
                                                                            <value name="if">
                                                                              <block type="expression" id="UF[fo.L4ELY:8fMDM]Q7">
                                                                                <field name="value">type == 2</field>
                                                                              </block>
                                                                            </value>
                                                                            <statement name="then">
                                                                              <block type="variable" id="D)oeMm]T$1@2XSNs3z7^F">
                                                                                <field name="name">i</field>
                                                                                <value name="value">
                                                                                  <block type="expression" id="ZTA3=|BB5evKx9jlFpGP">
                                                                                    <field name="value">0</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="while_do_block" id="Y;B}}BOhL91W?P3l7Lg?">
                                                                                    <value name="while">
                                                                                      <block type="expression" id="}5o{wn)VCtz`_KvnVqE8">
                                                                                        <field name="value">i &lt; 5</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <statement name="do">
                                                                                      <block type="do_while_block" id="2HQP[^fu32ze3(@2s9k|W">
                                                                                        <statement name="do">
                                                                                          <block type="variable" id="%u~6Y@2WLQAN+w|qBsH.,">
                                                                                            <field name="name">X</field>
                                                                                            <value name="value">
                                                                                              <block type="random_one" id="{rdndl)M()WK{MglAMt4">
                                                                                                <value name="items">
                                                                                                  <block type="expression" id="#hIem[uHdIQ9sX,UW@1T-">
                                                                                                    <field name="value">['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L']</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="variable" id="/m9fIh6``;+N__l[O4m@1">
                                                                                                <field name="name">Y</field>
                                                                                                <value name="value">
                                                                                                  <block type="random_number" id="6ewGR+]^@1P~x0H8gko+E">
                                                                                                    <value name="min">
                                                                                                      <block type="expression" id="=HB^2eE}[P![+GB7a6NL">
                                                                                                        <field name="value">1</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="max">
                                                                                                      <block type="expression" id="?tMpJ5,-XsULk#Z%t/Bg">
                                                                                                        <field name="value">6</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="variable" id="xXMO#zNdBp@2f|l;s7Fy7">
                                                                                                    <field name="name">temp</field>
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="wj4$vmj@2.Y.|36f-5Dmo">
                                                                                                        <field name="value">X+Y</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <value name="while">
                                                                                          <block type="expression" id="@2v:hrpHxFkG=Ryt?:eEw">
                                                                                            <field name="value">check_exist(temp, location) == 1</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="variable" id="_@1kR@2|tp,!ZFwjN)$m%p">
                                                                                            <field name="name">location[i]</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="rAUs5GG;ieEss3^`TDO|">
                                                                                                <field name="value">temp</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="variable" id="`NL99k}`aMYqmXq(mK0e">
                                                                                                <field name="name">i</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="0Mo)sA4N=gxq.D/AuI}j">
                                                                                                    <field name="value">i+1</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </statement>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="statement" id="f3|QhD}W;CM^brb]@1`Gz">
                                                                                <field name="value">if(type == 2 &amp;&amp; check_exist(city_location[city[0]], location) == 0){
  location.push(city_location[city[0]]);
  location.splice(0, 1);
}
                                                                                </field>
                                                                                <next>
                                                                                  <block type="if_then_block" id="F;IhwS=us/gjfHsCMp4_">
                                                                                    <value name="if">
                                                                                      <block type="expression" id="GNvhswh0hN,]ey(SH5S4">
                                                                                        <field name="value">type == 2</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <statement name="then">
                                                                                      <block type="variable" id="I?whW2hvX[ErLp0T4zc;">
                                                                                        <field name="name">location</field>
                                                                                        <value name="value">
                                                                                          <block type="func_shuffle" id="Q+yR|[vN@2YG3dq%o+BEx" inline="true">
                                                                                            <value name="value">
                                                                                              <block type="expression" id="i5WP)[uDN32hux=sZ:ck">
                                                                                                <field name="value">location</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </statement>
                                                                                    <next>
                                                                                      <block type="if_then_block" id="dnQsc%5uy~TZ31!kz9PP">
                                                                                        <value name="if">
                                                                                          <block type="expression" id="vBo;C0/3I1jC8FV#31||">
                                                                                            <field name="value">type == 1</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <statement name="then">
                                                                                          <block type="image_shape" id="S9SXxwY)9Kref:eQBZkn">
                                                                                            <statement name="#props">
                                                                                              <block type="prop_position" id="RlmH]LJ[C@1:RE9ViVgXp">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="x">
                                                                                                  <block type="expression" id="A|N/#pO8r8zQV_0{O~|6">
                                                                                                    <field name="value">540</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <value name="y">
                                                                                                  <block type="expression" id=")i{uYd%B[]37NSqK(r14">
                                                                                                    <field name="value">230</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_size" id="B{ac#+yf44^t]gM-8]nN">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="width">
                                                                                                      <block type="expression" id="?xcYVzD?+s7@1DmXg]HB?">
                                                                                                        <field name="value">0</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="height">
                                                                                                      <block type="expression" id="(uH.7Mpb?:/2([WO?=,+">
                                                                                                        <field name="value">0</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_image_key" id="Ouavnw:yRCR_s:g|%g0K">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="key">
                                                                                                          <block type="string_value" id="/Wgfqjo4HTv!bOPMk%zM">
                                                                                                            <field name="value">map.png</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_image_src" id="Cr~20E}QQdT.Qz@2bZ@26v">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="src">
                                                                                                              <block type="string_value" id="KOk-u$^$[xEN`7i[9D51">
                                                                                                                <field name="value">${image_path}map.png</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                            <next>
                                                                                              <block type="variable" id="vZr4#-LVY$y2sKiW(?-E">
                                                                                                <field name="name">i</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="h]r$}r@1K(?3.=$3,!nI6">
                                                                                                    <field name="value">0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="while_do_block" id="82jzy@1d~Hi5ehgO-?UXk">
                                                                                                    <value name="while">
                                                                                                      <block type="expression" id="uTXRJ|vw7Ej=ep|F.PyV">
                                                                                                        <field name="value">i &lt; city.length</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <statement name="do">
                                                                                                      <block type="image_shape" id="[Z_#E)0mh;,:Qk$`9M3f">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_position" id="u.f`M{c/e5yo:ehgXQ44">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="JOeuPPg#JKvF-{`O6Yw4">
                                                                                                                <field name="value">540</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="[$p$([XjKxKG^QAN[isf">
                                                                                                                <field name="value">230</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_size" id="U%l|QxYsq?nFG0~H`I%)">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="width">
                                                                                                                  <block type="expression" id="(K+c3KrK2ZJV{BMuj|5Y">
                                                                                                                    <field name="value">0</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="height">
                                                                                                                  <block type="expression" id="sKo]sY[?=Cx1!,~7uE+W">
                                                                                                                    <field name="value">0</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_image_key" id="GE4LQPt;([FA8@2L^a1nF">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="key">
                                                                                                                      <block type="string_value" id="nUUAVaZQ,916Tnn[AWf/">
                                                                                                                        <field name="value">${city[i]}.png</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_src" id="=JvD@1_Dn9zUcjLqDYw^h">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="src">
                                                                                                                          <block type="string_value" id=":h|O_W4#F@2jSCJ%$j`-?">
                                                                                                                            <field name="value">${image_path}${city[i]}.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="variable" id="_H:xf$Ut1(y=5upmoP4v">
                                                                                                            <field name="name">i</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="-wZw|@1[t!AX3+Jk~$;}t">
                                                                                                                <field name="value">i+1</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="text_shape" id="5Qjz^:e^bpF,?,bt$GGX">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_position" id="wIoFW0x+ebe]1!]OUWLO">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="`iocHaTansQqH3t+cgES">
                                                                                                                <field name="value">400</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="jq^%Rt%P}|@1IUom.)q?I">
                                                                                                                <field name="value">40</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_text_contents" id="7BfQO][ug7BVPz-~2@2NS">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="contents">
                                                                                                                  <block type="string_value" id="+5V~#]85.nfo0llK#Cf6">
                                                                                                                    <field name="value">Choose the city inside this coordinate ${city_location[city[0]]}.</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_style" id="4:HzCo9XWV4y2{$v6">
                                                                                                                    <field name="base">text</field>
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_text_style_font_size" id="b]n[lj]zV;/P?Yh/uG{D">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="fontSize">
                                                                                                                          <block type="expression" id="z$E0SM=LJ@2Rg.P1`dCFb">
                                                                                                                            <field name="value">36</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style_fill" id="g;f[~}8uT83SGg=4k/yf">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="fill">
                                                                                                                              <block type="string_value" id="#lJY^Z%:y,[?Ch7)=^Rw">
                                                                                                                                <field name="value">black</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_stroke" id="usB]o=,6A{MQ:$)[B7Xe">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="stroke">
                                                                                                                                  <block type="string_value" id="Aeo1}U#)8!0g#;Gd{U`_">
                                                                                                                                    <field name="value">white</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_stroke_thickness" id="R0vWl8^d9c,_)/cIb0-P">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="strokeThickness">
                                                                                                                                      <block type="expression" id="%,c@2ER8]]qS}SF$rE~;9">
                                                                                                                                        <field name="value">2</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="grid_shape" id="Y,U0iV(nEe@1d6^Q@1iPmB">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_grid_dimension" id="lTak#8fA5crmu,?v?|S~">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="rows">
                                                                                                                  <block type="expression" id="dF=h;4)ie=%MtVg8$UV`">
                                                                                                                    <field name="value">5</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="cols">
                                                                                                                  <block type="expression" id="W]YA/_AL1q78Z=~Qe0s_">
                                                                                                                    <field name="value">1</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_position" id="oA#H=hWN-c#v@1W#K8k?g">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="+Se$Xd$k4{{r^Wb]GIyk">
                                                                                                                        <field name="value">160</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="}Gu9T+Xj~85)I.w^wCTS">
                                                                                                                        <field name="value">250</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_size" id="x6bnnA,5-.O3_?e;x]k6">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="width">
                                                                                                                          <block type="expression" id="OJL#flO7DPXPUJ=0a#@1w">
                                                                                                                            <field name="value">250</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="height">
                                                                                                                          <block type="expression" id="qf42,M`FDfNS|JaG`@2Hr">
                                                                                                                            <field name="value">360</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_anchor" id="[G(dopES$;)AWvJr+rZb">
                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id=".77v0h!9VYVJ{,([RuJW">
                                                                                                                                <field name="value">0.5</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="+#)SR1|ZK_QNy8h4F{mH">
                                                                                                                                <field name="value">0.5</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_grid_cell_source" id="#4R|6Y.eVuNB,+be(RMM">
                                                                                                                                <field name="#prop">cell.source</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="s#bf_utw]Sq[xTMae=?6">
                                                                                                                                    <field name="value">name</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_grid_random" id="Vv)Vm:wBgOdo7l;,X::c">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <field name="random">FALSE</field>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_grid_show_borders" id="E=pJgk}|~j6lw:gWYACf">
                                                                                                                                        <field name="#prop">#showBorders</field>
                                                                                                                                        <field name="value">FALSE</field>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_grid_cell_template" id="OKO[hGKkkVUz7f1Bdcrq">
                                                                                                                                            <field name="variable">$cell</field>
                                                                                                                                            <field name="#prop">cell.template</field>
                                                                                                                                            <field name="#callback">$cell</field>
                                                                                                                                            <statement name="body">
                                                                                                                                              <block type="choice_custom_shape" id="}?21v+])8QI.i:+5/Q">
                                                                                                                                                <field name="action">click-one</field>
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="expression" id="|Kwobg|-Sk0k#M0)%WLU">
                                                                                                                                                    <field name="value">$cell.data</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <statement name="template">
                                                                                                                                                  <block type="image_shape" id="G7}5bQB_su8p,3G]9Xgu">
                                                                                                                                                    <statement name="#props">
                                                                                                                                                      <block type="prop_position" id="5N7h+5g(,~R0V%/@2q$@10">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="x">
                                                                                                                                                          <block type="expression" id="Zq8.rngB]%{S%Y;NA,2w">
                                                                                                                                                            <field name="value">$cell.centerX</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="y">
                                                                                                                                                          <block type="expression" id="4OG^cuA1LO5BH7K|N4EF">
                                                                                                                                                            <field name="value">$cell.centerY</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_size" id="xG0%h.@2lb,16G0zB;|Hu">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="width">
                                                                                                                                                              <block type="expression" id="{WP[}0.^:;d,bEQgUg^o">
                                                                                                                                                                <field name="value">220</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="height">
                                                                                                                                                              <block type="expression" id="%vBM[2h{#:G?yc+F%3">
                                                                                                                                                                <field name="value">50</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_image_key" id="wrK.un:q~gCwb@2DDB[q-">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="key">
                                                                                                                                                                  <block type="string_value" id="Hp|/[XszSIlhBKN]4SHF">
                                                                                                                                                                    <field name="value">shape.png</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_image_src" id="rpWW8FV8k.f:iG%jPiBO">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="src">
                                                                                                                                                                      <block type="string_value" id="gqEg6[w/1Vm|?C7]qUVJ">
                                                                                                                                                                        <field name="value">${image_path}shape.png</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="text_shape" id="o9-_dZgC+Xtt]_9J{u^?">
                                                                                                                                                        <statement name="#props">
                                                                                                                                                          <block type="prop_position" id="cnM`m!Ad#l2]xQLc/1}k">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="x">
                                                                                                                                                              <block type="expression" id="(Ia,@250v%m18,+@2cC9DV">
                                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="y">
                                                                                                                                                              <block type="expression" id="]h6?=!R+0$A`8}|POD|V">
                                                                                                                                                                <field name="value">$cell.centerY + 3</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_text_contents" id="Yp@15@1F|{n%X0a[hsd0(E">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="contents">
                                                                                                                                                                  <block type="string_value" id="g`aiLhq9}5n8NiCyprjE">
                                                                                                                                                                    <field name="value">${$cell.data}</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_text_style" id="xC[))8h^0pwES;_QV!%O">
                                                                                                                                                                    <field name="base">text</field>
                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                      <block type="prop_text_style_font_size" id="tli~e_-f+B{EbEn8/EL:">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="fontSize">
                                                                                                                                                                          <block type="expression" id="k_m@2f^~oMjByMa;gprac">
                                                                                                                                                                            <field name="value">28</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_text_style_fill" id="51fI@2!:NlE~KS4-=hxSp">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="fill">
                                                                                                                                                                              <block type="string_value" id=".J^j/l:nic$e)|JR`pPi">
                                                                                                                                                                                <field name="value">black</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="func_add_answer" id="j|iCqTI$g|w8e,t+q4fc" inline="true">
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="@2j+gf$,AR7#w)EEha8L8">
                                                                                                                    <field name="value">city_name[city[0]]</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="variable" id="T-{]9$}5kx-ogL^_NE5o">
                                                                                                                    <field name="name">Variable</field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="custom_image_list" id="tFv0`yp97_Wn@1eBLu)qM">
                                                                                                                        <field name="link">${image_path}</field>
                                                                                                                        <field name="images">2ex${city[0]}</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="partial_explanation" id="-v(Fzz/R{Re@2PvR1uEvs" inline="true">
                                                                                                                        <value name="value">
                                                                                                                          <block type="string_value" id="y=UOP-`hG}2OHB3eH,__">
                                                                                                                            <field name="value">&lt;center&gt;&lt;u&gt;Let's answer ${city_name[city[0]]} location&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;div style='position: relative; width: 487px; height: 260px;'&gt;
&lt;img src='@1sprite.src(map.png)' style='position: absolute; left: 0px'/&gt;
                                                                                                                            </field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="variable" id="LdqVü_SNbNzpUljQ`J">
                                                                                                                            <field name="name">i</field>
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="6,3AX`Xe5+Y?a0hW~31b">
                                                                                                                                <field name="value">0</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="while_do_block" id="([$@1hY`@29^MJe,To!b/w">
                                                                                                                                <value name="while">
                                                                                                                                  <block type="expression" id="R=]Y!n6Lb;SaMtZA-g;N">
                                                                                                                                    <field name="value">i &lt; city.length</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <statement name="do">
                                                                                                                                  <block type="partial_explanation" id="DtJw^i9a:4FWpH_6N7si" inline="true">
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="string_value" id="YL/y}U[S@1HO.-IC1[d;$">
                                                                                                                                        <field name="value">&lt;img src='@1sprite.src(${city[i]}.png)' style='position: absolute; left: 0px'/&gt;</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="variable" id="W8|Z=VC+5u2`S,YvD@1tq">
                                                                                                                                        <field name="name">i</field>
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="expression" id="!oNvKfR[~ZL4C9j?W~|?">
                                                                                                                                            <field name="value">i+1</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </statement>
                                                                                                                                <next>
                                                                                                                                  <block type="partial_explanation" id="E+iK(j}+vd:UmU0[#~0}" inline="true">
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="string_value" id="g@25E2S7,?Gs`#5#Z2Fe:">
                                                                                                                                        <field name="value">&lt;img src='@1sprite.src(2ex${city[0]}.png)' style='position: absolute; left: 0px'/&gt;
&lt;/div&gt;
&lt;/center&gt;
&lt;span style='background: rgb(250, 250, 250, 0.3)'&gt;
Vertically, you can see which line ${city_location[city[0]][0]} is &lt;b&gt;${city_name[city[0]]}&lt;/b&gt;&lt;/br&gt;
Horizontally, you can see which line ${city_location[city[0]][1]} is &lt;b&gt;${city_name[city[0]]}&lt;/b&gt;&lt;/br&gt;&lt;/br&gt;
&lt;center&gt;&lt;span style='background: rgb(250, 250, 250, 0.3)'&gt;On the spot gone through both ${city_location[city[0]][0]} and ${city_location[city[0]][1]} contains &lt;b&gt;${city_name[city[0]]}&lt;/b&gt;!&lt;/center&gt;&lt;/span&gt;
                                                                                                                                        </field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="end_partial_explanation" id="1x=-0puiTh;gd]t1uwn!"></block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </statement>
                                                                                        <next>
                                                                                          <block type="if_then_block" id="OG/I6s,!LleI7nwQLRC@1">
                                                                                            <value name="if">
                                                                                              <block type="expression" id="XA}r7Qb)@2U?ixKBi6%S3">
                                                                                                <field name="value">type == 2</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <statement name="then">
                                                                                              <block type="image_shape" id="v}UmU!KR^Du(=p5[y{By">
                                                                                                <statement name="#props">
                                                                                                  <block type="prop_position" id="9yL[tNAC=an~Yp_8?+K%">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="x">
                                                                                                      <block type="expression" id="1Q`MNBw/yQ%dGF)OGxsY">
                                                                                                        <field name="value">400</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <value name="y">
                                                                                                      <block type="expression" id="93z;lK({Xjv6=7iBU7t8">
                                                                                                        <field name="value">210</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_size" id="Vu1CS_e{{c)E^=m?x%(,">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="width">
                                                                                                          <block type="expression" id="h+lO_@1_y-uAl/PX?,cS9">
                                                                                                            <field name="value">0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <value name="height">
                                                                                                          <block type="expression" id="Xpsoyj@2#-@2(n+kx?`1@2(">
                                                                                                            <field name="value">0</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_image_key" id="=]69wVK0nBNcw{$%(bmV">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="key">
                                                                                                              <block type="string_value" id="0U(!S.U1@1k+r(Zv^;8;z">
                                                                                                                <field name="value">map.png</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_image_src" id="pYIc(XM@1nR2n)92%A;es">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="src">
                                                                                                                  <block type="string_value" id="nky@2i}N.~a;DnikVw2wL">
                                                                                                                    <field name="value">${image_path}map.png</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </statement>
                                                                                                <next>
                                                                                                  <block type="variable" id="`W}/gTjaQzRVYy@1yq,t)">
                                                                                                    <field name="name">i</field>
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="Y$DMS?HX2D8I_eJ|bhf@2">
                                                                                                        <field name="value">0</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="while_do_block" id="%3hK5Xe[6WZSMEaane8V">
                                                                                                        <value name="while">
                                                                                                          <block type="expression" id="CtiVQ/CdlR3vu)#gmjCk">
                                                                                                            <field name="value">i &lt; city.length</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <statement name="do">
                                                                                                          <block type="image_shape" id="v283~2g2^3J!V7ZrKfPc">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_position" id="d9#vf~3)ZO1`IN]51`uG">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="k/3ghq8`JxQhiQJkqC{_">
                                                                                                                    <field name="value">400</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="KQj@1;v1OJ](3WUb65k[K">
                                                                                                                    <field name="value">210</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_size" id="?d7.YU`mTYwO:_R)zk?M">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="width">
                                                                                                                      <block type="expression" id=",Vk-d|ZD686E@2A3:qs)l">
                                                                                                                        <field name="value">0</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="height">
                                                                                                                      <block type="expression" id="e1~d`yUX:qj4Rk.J=H{j">
                                                                                                                        <field name="value">0</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_key" id="r%vD/A6RftW13y;:47A(">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="key">
                                                                                                                          <block type="string_value" id="S3}@2zgWy?xaf?+Qj1{`!">
                                                                                                                            <field name="value">${city[i]}.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_src" id="Ue6s^[MyBXY,d5}HA}`^">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="src">
                                                                                                                              <block type="string_value" id="@2+D:2JM$-`M{W-GBRvhl">
                                                                                                                                <field name="value">${image_path}${city[i]}.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="variable" id=";=mc@14JWuis)?,cY:532">
                                                                                                                <field name="name">i</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="-!g?1Rhc@1VvDvi-y+F^D">
                                                                                                                    <field name="value">i+1</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="text_shape" id="6-:XH2=5QF/fZ-?:Z):~">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_position" id="fQUd32-r@1MZcSa1(=mAD">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id=":fHgfD]%2/ZXR#`n_O{G">
                                                                                                                    <field name="value">400</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="1p7JOzXLXek^lYR)b(QO">
                                                                                                                    <field name="value">40</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_contents" id="nH:5S#(VG.lyfoWp{%pu">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="contents">
                                                                                                                      <block type="string_value" id="#6btV9p,PGV}wMx.UÃ¯Â¿Â½">
                                                                                                                        <field name="value">Choose the coordinates for ${city_name[city[0]]}.</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style" id="Y=Lm+5%@2vEr)lOI!J1go">
                                                                                                                        <field name="base">text</field>
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_text_style_font_size" id="p,!:FvCO4V$oW@2Y_oHXw">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="fontSize">
                                                                                                                              <block type="expression" id="y+0~p12X%|H8K[L!T+K9">
                                                                                                                                <field name="value">36</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_fill" id="%KgLpDvH7=IpdY]QF+|w">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="fill">
                                                                                                                                  <block type="string_value" id="@2FH%l.O+7R+S#[y4nDCi">
                                                                                                                                    <field name="value">black</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="stroke">
                                                                                                                                      <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                                                                        <field name="value">white</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <value name="strokeThickness">
                                                                                                                                          <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                                                                            <field name="value">2</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="grid_shape" id="6tU?K^N8ec~A@1@1NwQ72N">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_grid_dimension" id="X4K:|9dKK%s9l4n5v@2lG">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="rows">
                                                                                                                      <block type="expression" id="[Ev!.bxnzjc|-33v.%7=">
                                                                                                                        <field name="value">1</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="cols">
                                                                                                                      <block type="expression" id="u$7]/V/!ZWM4(Va??@2({">
                                                                                                                        <field name="value">5</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_position" id="zLld84TfBg#!88]1,JD-">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="x">
                                                                                                                          <block type="expression" id="Cd6g{Y_P?:GyVyvQi1wJ">
                                                                                                                            <field name="value">400</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="y">
                                                                                                                          <block type="expression" id=".F}E71+@1i)77!yQ`hmfu">
                                                                                                                            <field name="value">400</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_size" id="+o2CWr^.ohx)_bcIG53]">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="width">
                                                                                                                              <block type="expression" id="Z[x5iHG%KWC%KWQb}c-G">
                                                                                                                                <field name="value">700</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="height">
                                                                                                                              <block type="expression" id="@2E|Ob@1jF#gYtX8-%Iui@1">
                                                                                                                                <field name="value">80</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_anchor" id="1WJjXY^.h,t]eNgxaJ}q">
                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="DHx[vqs?.?nUD:AYPJWd">
                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="e3D|RR7w!=Sufu):zV`c">
                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_grid_cell_source" id="v8+Yu211Qg8fX9Mu4th`">
                                                                                                                                    <field name="#prop">cell.source</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="~j@1,^liddf}oDg}E91^i">
                                                                                                                                        <field name="value">location</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_grid_random" id="}?kS#x[b)9OGb@1`UdJGV">
                                                                                                                                        <field name="#prop"></field>
                                                                                                                                        <field name="random">FALSE</field>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_grid_show_borders" id=";y^wk4y%[VWjXOj~vrQZ">
                                                                                                                                            <field name="#prop">#showBorders</field>
                                                                                                                                            <field name="value">FALSE</field>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_grid_cell_template" id="7^6]Kv:2yrZi`cooAW2@2">
                                                                                                                                                <field name="variable">$cell</field>
                                                                                                                                                <field name="#prop">cell.template</field>
                                                                                                                                                <field name="#callback">$cell</field>
                                                                                                                                                <statement name="body">
                                                                                                                                                  <block type="choice_custom_shape" id="vzlC3MrIK1Dc}i;)5{,@2">
                                                                                                                                                    <field name="action">click-one</field>
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="expression" id="c_L__4FSTo-R:^A:Jh/O">
                                                                                                                                                        <field name="value">$cell.data</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <statement name="template">
                                                                                                                                                      <block type="image_shape" id="0!.BnmWS4=VL1HnC3T8U">
                                                                                                                                                        <statement name="#props">
                                                                                                                                                          <block type="prop_position" id="T.$md=!ZE32)w)|0}QI@1">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="x">
                                                                                                                                                              <block type="expression" id="Xo,TGEdw@2-a_GCjQSh^C">
                                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="y">
                                                                                                                                                              <block type="expression" id="WJ$(Weiy$~|Y[GWvF@2Qv">
                                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_size" id=")|14szEhie=}k?pMT+xx">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="width">
                                                                                                                                                                  <block type="expression" id="Iph@1(}m,(kT]UGUB@1HFg">
                                                                                                                                                                    <field name="value">114</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <value name="height">
                                                                                                                                                                  <block type="expression" id="?;0iRN.pRwm=6.i{G6_b">
                                                                                                                                                                    <field name="value">60</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_image_key" id=";ir)eA]xUfVlnb@1r)R1.">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="key">
                                                                                                                                                                      <block type="string_value" id="O~%Z+!/IrGz2fU//tbc^">
                                                                                                                                                                        <field name="value">shape.png</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_image_src" id="AxIsHJ6jNkQ7zZDUSU3?">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="src">
                                                                                                                                                                          <block type="string_value" id="X3l8!{B[3R!;:b#G6?wb">
                                                                                                                                                                            <field name="value">${image_path}shape.png</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="text_shape" id="$4otcCY]-Z^65~b-~N(x">
                                                                                                                                                            <statement name="#props">
                                                                                                                                                              <block type="prop_position" id="Tc2ouu0NM-D`Q5TVT{MQ">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="x">
                                                                                                                                                                  <block type="expression" id="8;bRZWqc%T,EH(@1AlgWY">
                                                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <value name="y">
                                                                                                                                                                  <block type="expression" id="p3m{g@2,Rf^dP%NcA[LAb">
                                                                                                                                                                    <field name="value">$cell.centerY + 3</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_text_contents" id="~Vl?5$lS!EXZ1|kH((8n">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="contents">
                                                                                                                                                                      <block type="string_value" id="#6btV9p,PGV}wMx.Uï¿½">
                                                                                                                                                                        <field name="value">${$cell.data}</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_text_style" id="$o^Cd}^(1s0V,G-2$7rd">
                                                                                                                                                                        <field name="base">text</field>
                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                          <block type="prop_text_style_font_size" id="9NTf{8?~OI#Kqvt]zHa=">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="fontSize">
                                                                                                                                                                              <block type="expression" id="cIiYM/aJyMZU[wu3-`k^">
                                                                                                                                                                                <field name="value">36</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_text_style_fill" id="ouuy}u#]W2ls-^c@2+-dp">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="fill">
                                                                                                                                                                                  <block type="string_value" id="9@2]@2PB@2{;#r@1G?fJ0sVf">
                                                                                                                                                                                    <field name="value">black</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="func_add_answer" id="`9Es?W.KMC7br}Ar(U`e" inline="true">
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="@1W:^;p~e|bHS-.+d??3@2">
                                                                                                                        <field name="value">city_location[city[0]]</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="variable" id="UNl:+VXI``U~U5FQO1_P">
                                                                                                                        <field name="name">Variable</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="custom_image_list" id="?KjN/$fxoD5IfrNyDw4C">
                                                                                                                            <field name="link">${image_path}</field>
                                                                                                                            <field name="images">ex${city[0]}|ex${city[1]}|map</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="partial_explanation" id="7#5h0N`7ET`0J,__2W;7" inline="true">
                                                                                                                            <value name="value">
                                                                                                                              <block type="string_value" id="U/[@2P3SK}H!Oqb3q}I10">
                                                                                                                                <field name="value">&lt;center&gt;&lt;u&gt;Let's answer ${city_name[city[0]]} location&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;

&lt;div style='position: relative; width: 487px; height: 260px;'&gt;
&lt;img src='@1sprite.src(map.png)' style='position: absolute; left: 0px'/&gt;
                                                                                                                                </field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="variable" id="aa]@1xS?aT,x[0X@1;ouo{">
                                                                                                                                <field name="name">i</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="u~[GT-IOsH;x2_W_U1pC">
                                                                                                                                    <field name="value">0</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="while_do_block" id="Jx=u4X60Z=qRFb$Zb/vz">
                                                                                                                                    <value name="while">
                                                                                                                                      <block type="expression" id="[Fz!Ku)OJqJtp(.Vm~9T">
                                                                                                                                        <field name="value">i &lt; city.length</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <statement name="do">
                                                                                                                                      <block type="partial_explanation" id="B5{e8dN-0T.`es/v)H}J" inline="true">
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="string_value" id="m_6cJ?zWc}A,r|~oe.A,">
                                                                                                                                            <field name="value">&lt;img src='@1sprite.src(${city[i]}.png)' style='position: absolute; left: 0px'/&gt;</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="variable" id="rdD.A+ZdW(@1cL8LxsZ%`">
                                                                                                                                            <field name="name">i</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id=":Y`nAD=?QC[g_h^aHI)a">
                                                                                                                                                <field name="value">i+1</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </statement>
                                                                                                                                    <next>
                                                                                                                                      <block type="partial_explanation" id=",PKBv;5^:F@1piQ#!w2.)" inline="true">
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="string_value" id="vZ)wgy%W;a|ImP`mQHa^">
                                                                                                                                            <field name="value">&lt;img src='@1sprite.src(ex${city[0]}.png)' style='position: absolute; left: 0px'/&gt;
&lt;/div&gt;
&lt;/center&gt;
&lt;span style='background: rgb(250, 250, 250, 0.3)'&gt;
Vertically, you can see where ${city_name[city[0]]} is &lt;b&gt;${city_location[city[0]][0]}&lt;/b&gt;&lt;/br&gt;
Horizontally, you can see where ${city_name[city[0]]} is &lt;b&gt;${city_location[city[0]][1]}&lt;/b&gt;&lt;/br&gt;&lt;/br&gt;
&lt;center&gt;&lt;span style='background: rgb(250, 250, 250, 0.3)'&gt;So now you know, ${city_name[city[0]]} is &lt;b&gt;${city_location[city[0]]}&lt;/b&gt;&lt;/center&gt;&lt;/span&gt;
                                                                                                                                            </field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="end_partial_explanation" id="@2aBRh#|,p(Hldb1!A2r{"></block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
</xml>
END_XML]] */