
module.exports = [
  {
    "#type": "question",
    "name": "Y6.MG.TI.TI.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/Y6.MG.TI.TI.1B/"
      },
      {
        "#type": "variable",
        "name": "background_path",
        "value": "Develop/ImageQAs/General/Backgrounds/"
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "1"
          },
          "max": {
            "#type": "expression",
            "value": "15"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "bg${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${background_path}bg${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "lst_m",
        "value": {
          "#type": "string_array",
          "items": "Red|Green|Blue|Yellow|Silver|Black|East|West|North|South|Central"
        }
      },
      {
        "#type": "variable",
        "name": "lst_p",
        "value": {
          "#type": "string_array",
          "items": "Pier|Beach|Hole|Garden|Valley|Forest|Wood|Square|Diamond|Creek|Avenue|Parade"
        }
      },
      {
        "#type": "variable",
        "name": "lst_t",
        "value": {
          "#type": "string_array",
          "items": "train|ferry|tram|bus"
        }
      },
      {
        "#type": "variable",
        "name": "lst_T",
        "value": {
          "#type": "string_array",
          "items": "TRAIN|FERRY|TRAM|BUS"
        }
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "variable",
        "name": "an1",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "0"
          },
          "max": {
            "#type": "expression",
            "value": "4"
          }
        }
      },
      {
        "#type": "variable",
        "name": "an2",
        "value": {
          "#type": "random_number",
          "min": {
            "#type": "expression",
            "value": "an1 + 1"
          },
          "max": {
            "#type": "expression",
            "value": "5"
          }
        }
      },
      {
        "#type": "variable",
        "name": "times",
        "value": {
          "#type": "expression",
          "value": "[12, 13, 14, 15, 16, 17, 18, 24, 25, 26, 27, 28, 29, 30, 36, 37, 38, 39, 40, 41, 42]"
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "range < 0"
        },
        "then": [
          {
            "#type": "variable",
            "name": "x",
            "value": {
              "#type": "random_many",
              "count": {
                "#type": "expression",
                "value": "6"
              },
              "items": {
                "#type": "expression",
                "value": "[12, 13, 14, 15, 16, 17, 18, 24, 25, 26, 27, 28, 29, 30, 36, 37]"
              }
            }
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "while_do_block",
            "while": {
              "#type": "expression",
              "value": "i <  6 + an1 + 1"
            },
            "do": [
              {
                "#type": "variable",
                "name": "x[i]",
                "value": {
                  "#type": "expression",
                  "value": "x[i]*5"
                }
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "i+1"
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "max",
            "value": {
              "#type": "expression",
              "value": "37*5"
            }
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "range < 4"
            },
            "then": [
              {
                "#type": "variable",
                "name": "x",
                "value": {
                  "#type": "random_many",
                  "count": {
                    "#type": "expression",
                    "value": "6"
                  },
                  "items": {
                    "#type": "func",
                    "name": "arrayOfNumber",
                    "args": [
                      {
                        "#type": "expression",
                        "value": "720"
                      },
                      {
                        "#type": "expression",
                        "value": "60"
                      }
                    ]
                  }
                }
              },
              {
                "#type": "variable",
                "name": "max",
                "value": {
                  "#type": "expression",
                  "value": "779"
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "x",
                "value": {
                  "#type": "random_many",
                  "count": {
                    "#type": "expression",
                    "value": "6"
                  },
                  "items": {
                    "#type": "func",
                    "name": "arrayOfNumber",
                    "args": [
                      {
                        "#type": "expression",
                        "value": "1380"
                      },
                      {
                        "#type": "expression",
                        "value": "60"
                      }
                    ]
                  }
                }
              },
              {
                "#type": "variable",
                "name": "max",
                "value": {
                  "#type": "expression",
                  "value": "1380+59"
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "x",
        "value": {
          "#type": "func",
          "name": "sort",
          "args": [
            {
              "#type": "expression",
              "value": "x"
            },
            "asc"
          ]
        }
      },
      {
        "#type": "variable",
        "name": "t",
        "value": {
          "#type": "random_many",
          "count": {
            "#type": "expression",
            "value": "2"
          },
          "items": {
            "#type": "expression",
            "value": "lst_t"
          }
        }
      },
      {
        "#type": "statement",
        "value": "function two(a){\n  if(a < 10){return '0' + a;}\n  return a;\n}\n\nfunction jsUcfirst(string) \n{\n    return string.charAt(0).toUpperCase() + string.slice(1);\n}\n\nfunction check(x, A) {\n    for(var m=0; m<18 || A[m] < 0; m++){\n        if(x == A[m]){\n            return 1;\n        }\n    }\n    return 0;\n}"
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "resp_x",
              "value": "cx"
            },
            "y": {
              "#type": "expression",
              "value": "40"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "The timetable below shows the arrival time of the ${t[0]} and the ${t[1]} to different places."
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "32"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "m",
        "value": {
          "#type": "random_many",
          "count": {
            "#type": "expression",
            "value": "6+an1+1"
          },
          "items": {
            "#type": "expression",
            "value": "lst_m"
          }
        }
      },
      {
        "#type": "variable",
        "name": "p",
        "value": {
          "#type": "random_many",
          "count": {
            "#type": "expression",
            "value": "6+an1+1"
          },
          "items": {
            "#type": "expression",
            "value": "lst_p"
          }
        }
      },
      {
        "#type": "variable",
        "name": "z",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < 6"
        },
        "do": [
          {
            "#type": "variable",
            "name": "z[i]",
            "value": {
              "#type": "expression",
              "value": "two(Math.floor(x[i]/60)) + ' : ' + two(x[i] % 60)"
            }
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "image_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "400"
            },
            "y": {
              "#type": "expression",
              "value": "190"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "0"
            },
            "height": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "table.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}table.png"
          }
        ]
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "260"
            },
            "y": {
              "#type": "expression",
              "value": "95"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "${jsUcfirst(t[0])} timetable"
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "26"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "590"
            },
            "y": {
              "#type": "expression",
              "value": "95"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "${jsUcfirst(t[1])} timetable"
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "26"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "6"
            },
            "cols": {
              "#type": "expression",
              "value": "1"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "180"
            },
            "y": {
              "#type": "expression",
              "value": "205"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "220"
            },
            "height": {
              "#type": "expression",
              "value": "200"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "func",
              "name": "arrayOfNumber",
              "args": [
                {
                  "#type": "expression",
                  "value": "6"
                },
                {
                  "#type": "expression",
                  "value": "0"
                }
              ]
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_cell_template",
            "variable": "$cell",
            "#prop": "cell.template",
            "#callback": "$cell",
            "body": [
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY+5"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${m[$cell.data]} ${p[$cell.data]}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "26"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "6"
            },
            "cols": {
              "#type": "expression",
              "value": "1"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "345"
            },
            "y": {
              "#type": "expression",
              "value": "205"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "110"
            },
            "height": {
              "#type": "expression",
              "value": "200"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "func",
              "name": "arrayOfNumber",
              "args": [
                {
                  "#type": "expression",
                  "value": "6"
                },
                {
                  "#type": "expression",
                  "value": "0"
                }
              ]
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_cell_template",
            "variable": "$cell",
            "#prop": "cell.template",
            "#callback": "$cell",
            "body": [
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY+5"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${z[$cell.data]}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "26"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "w",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "y",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "array",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < 6"
        },
        "do": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "i < an1 + 1"
            },
            "then": [
              {
                "#type": "variable",
                "name": "array[i]",
                "value": {
                  "#type": "expression",
                  "value": "i+6"
                }
              },
              {
                "#type": "if_then_else_block",
                "if": {
                  "#type": "expression",
                  "value": "range < 0"
                },
                "then": [
                  {
                    "#type": "variable",
                    "name": "y[i]",
                    "value": {
                      "#type": "random_one",
                      "items": {
                        "#type": "expression",
                        "value": "[12, 13, 14, 15, 16, 17, 18, 24, 25, 26, 27, 28, 29, 30, 36, 37]"
                      }
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "y[i]",
                    "value": {
                      "#type": "expression",
                      "value": "y[i] * 5"
                    }
                  }
                ],
                "else": [
                  {
                    "#type": "if_then_else_block",
                    "if": {
                      "#type": "expression",
                      "value": "range < 4"
                    },
                    "then": [
                      {
                        "#type": "variable",
                        "name": "y[i]",
                        "value": {
                          "#type": "random_one",
                          "items": {
                            "#type": "func",
                            "name": "arrayOfNumber",
                            "args": [
                              {
                                "#type": "expression",
                                "value": "720"
                              },
                              {
                                "#type": "expression",
                                "value": "60"
                              }
                            ]
                          }
                        }
                      }
                    ],
                    "else": [
                      {
                        "#type": "variable",
                        "name": "y[i]",
                        "value": {
                          "#type": "random_one",
                          "items": {
                            "#type": "func",
                            "name": "arrayOfNumber",
                            "args": [
                              {
                                "#type": "expression",
                                "value": "1380"
                              },
                              {
                                "#type": "expression",
                                "value": "60"
                              }
                            ]
                          }
                        }
                      }
                    ]
                  }
                ]
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "array[i]",
                "value": {
                  "#type": "expression",
                  "value": "i"
                }
              },
              {
                "#type": "do_while_block",
                "do": [
                  {
                    "#type": "if_then_else_block",
                    "if": {
                      "#type": "expression",
                      "value": "range < 0"
                    },
                    "then": [
                      {
                        "#type": "do_while_block",
                        "do": [
                          {
                            "#type": "variable",
                            "name": "rdm",
                            "value": {
                              "#type": "random_one",
                              "items": {
                                "#type": "expression",
                                "value": "[5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60]"
                              }
                            }
                          }
                        ],
                        "while": {
                          "#type": "expression",
                          "value": "check(rdm/5 + x[i]/5, times) == 0 || check(rdm+x[i], y) == 1"
                        }
                      }
                    ],
                    "else": [
                      {
                        "#type": "variable",
                        "name": "rdm",
                        "value": {
                          "#type": "random_number",
                          "min": {
                            "#type": "expression",
                            "value": "5"
                          },
                          "max": {
                            "#type": "expression",
                            "value": "60"
                          }
                        }
                      }
                    ]
                  },
                  {
                    "#type": "variable",
                    "name": "y[i]",
                    "value": {
                      "#type": "expression",
                      "value": "x[i] + rdm"
                    }
                  }
                ],
                "while": {
                  "#type": "expression",
                  "value": "y[i] > max"
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "w[i]",
            "value": {
              "#type": "expression",
              "value": "two(Math.floor(y[i]/60)) + ' : ' + two(y[i] % 60)"
            }
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "statement",
        "value": "for(var i=0; i<6; i++){\n        for(var j=i+1; j<6; j++){\n            if(y[i] > y[j]){\n                temp = y[i];\n                y[i] = y[j];\n                y[j] = temp;\n\n                temp = array[i];\n                array[i] = array[j];\n                array[j] = temp;\n\n                temp = w[i];\n                w[i] = w[j];\n                w[j] = temp;\n            }\n        }\n    }"
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "6"
            },
            "cols": {
              "#type": "expression",
              "value": "1"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "510"
            },
            "y": {
              "#type": "expression",
              "value": "205"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "210"
            },
            "height": {
              "#type": "expression",
              "value": "200"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "func",
              "name": "arrayOfNumber",
              "args": [
                {
                  "#type": "expression",
                  "value": "6"
                },
                {
                  "#type": "expression",
                  "value": "0"
                }
              ]
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_cell_template",
            "variable": "$cell",
            "#prop": "cell.template",
            "#callback": "$cell",
            "body": [
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY+5"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${m[array[$cell.data]]} ${p[array[$cell.data]]}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "26"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "6"
            },
            "cols": {
              "#type": "expression",
              "value": "1"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "670"
            },
            "y": {
              "#type": "expression",
              "value": "205"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "110"
            },
            "height": {
              "#type": "expression",
              "value": "200"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "func",
              "name": "arrayOfNumber",
              "args": [
                {
                  "#type": "expression",
                  "value": "6"
                },
                {
                  "#type": "expression",
                  "value": "0"
                }
              ]
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_cell_template",
            "variable": "$cell",
            "#prop": "cell.template",
            "#callback": "$cell",
            "body": [
              {
                "#type": "text_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY+5"
                    }
                  },
                  {
                    "#type": "prop_text_contents",
                    "#prop": "",
                    "contents": "${w[$cell.data]}"
                  },
                  {
                    "#prop": "",
                    "style": {
                      "#type": "json",
                      "base": "text",
                      "#props": [
                        {
                          "#type": "prop_text_style_font_size",
                          "#prop": "",
                          "fontSize": {
                            "#type": "expression",
                            "value": "26"
                          }
                        },
                        {
                          "#type": "prop_text_style_fill",
                          "#prop": "",
                          "fill": "black"
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "10"
            },
            "y": {
              "#type": "expression",
              "value": "360"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "How long does it take to travel from ${m[an1]} ${p[an1]} to ${m[an2]} ${p[an2]} and wait for a ${t[1]} there?"
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "26"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                },
                {
                  "#type": "prop_text_style_wordwrap",
                  "#prop": "",
                  "wordWrap": {
                    "#type": "expression",
                    "value": "true"
                  }
                },
                {
                  "#type": "prop_text_style_wordWrapWidth",
                  "#prop": "",
                  "wordWrapWidth": {
                    "#type": "expression",
                    "value": "430"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "statement",
        "value": "function out_min(str, array){\n    for(var i = 0; i< array.length; i++){\n        if(str == array[i]){\n          return i;\n        }\n    }\n}"
      },
      {
        "#type": "variable",
        "name": "time",
        "value": {
          "#type": "expression",
          "value": "x"
        }
      },
      {
        "#type": "variable",
        "name": "a",
        "value": {
          "#type": "expression",
          "value": "time[an2] - time[an1]"
        }
      },
      {
        "#type": "variable",
        "name": "b",
        "value": {
          "#type": "expression",
          "value": "a + (y[out_min(an2, array)] - time[an2])"
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "Math.floor(a/60) <2"
        },
        "then": [
          {
            "#type": "variable",
            "name": "hour",
            "value": {
              "#type": "expression",
              "value": "'hour'"
            }
          }
        ],
        "else": [
          {
            "#type": "variable",
            "name": "hour",
            "value": {
              "#type": "expression",
              "value": "'hours'"
            }
          }
        ]
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "a % 60 <2"
        },
        "then": [
          {
            "#type": "variable",
            "name": "minute",
            "value": {
              "#type": "expression",
              "value": "'minute'"
            }
          }
        ],
        "else": [
          {
            "#type": "variable",
            "name": "minute",
            "value": {
              "#type": "expression",
              "value": "'minutes'"
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "str",
        "value": {
          "#type": "expression",
          "value": "'_  ' + (Math.floor(b/60) <2 ? 'hour':'hours')+ '  ^  ' + (b % 60 <2 ? 'minute':'minutes')"
        }
      },
      {
        "#type": "list_shape",
        "#props": [
          {
            "#type": "prop_list_direction",
            "#prop": "",
            "dir": "horizontal"
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "780"
            },
            "y": {
              "#type": "expression",
              "value": "350"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "1"
            },
            "y": {
              "#type": "expression",
              "value": "0.5"
            }
          },
          {
            "#type": "prop_spacing",
            "#prop": "",
            "spacing": {
              "#type": "expression",
              "value": "-2"
            }
          }
        ],
        "items": [
          {
            "#type": "json",
            "#props": [
              {
                "#type": "prop_list_align",
                "#prop": "",
                "align": "middle"
              },
              {
                "#type": "prop_list_item_source",
                "#prop": "",
                "source": {
                  "#type": "expression",
                  "value": "str"
                }
              }
            ],
            "template": {
              "#callback": "$item",
              "variable": "$item",
              "body": [
                {
                  "#type": "if_then_else_block",
                  "if": {
                    "#type": "expression",
                    "value": "$item.data == '_'"
                  },
                  "then": [
                    {
                      "#type": "image_shape",
                      "#props": [
                        {
                          "#type": "prop_position",
                          "#prop": "",
                          "x": {
                            "#type": "expression",
                            "value": "0"
                          },
                          "y": {
                            "#type": "expression",
                            "value": "0"
                          }
                        },
                        {
                          "#type": "prop_size",
                          "#prop": "",
                          "width": {
                            "#type": "expression",
                            "value": "0"
                          },
                          "height": {
                            "#type": "expression",
                            "value": "0"
                          }
                        },
                        {
                          "#type": "prop_image_key",
                          "#prop": "",
                          "key": "shape.png"
                        },
                        {
                          "#type": "prop_image_src",
                          "#prop": "",
                          "src": "${image_path}shape.png"
                        },
                        {
                          "#type": "prop_scale",
                          "#prop": "",
                          "scale": {
                            "#type": "expression",
                            "value": "0.8"
                          }
                        }
                      ]
                    },
                    {
                      "#type": "choice_input_shape",
                      "#props": [
                        {
                          "#type": "prop_value",
                          "#prop": "",
                          "value": {
                            "#type": "expression",
                            "value": "Math.floor(b/60)"
                          }
                        },
                        {
                          "#type": "prop_position",
                          "#prop": "",
                          "x": {
                            "#type": "expression",
                            "value": "0"
                          },
                          "y": {
                            "#type": "expression",
                            "value": "0"
                          }
                        },
                        {
                          "#type": "prop_size",
                          "#prop": "",
                          "width": {
                            "#type": "expression",
                            "value": "75"
                          },
                          "height": {
                            "#type": "expression",
                            "value": "35"
                          }
                        },
                        {
                          "#type": "prop_input_keyboard",
                          "#prop": "",
                          "keyboard": "numbers1"
                        },
                        {
                          "#type": "prop_input_max_length",
                          "#prop": "",
                          "maxLength": {
                            "#type": "expression",
                            "value": "Math.floor(b/60).toString().length"
                          }
                        },
                        {
                          "#type": "prop_input_result_position",
                          "#prop": "",
                          "resultPosition": "bottom"
                        },
                        {
                          "#type": "prop_tab_order",
                          "#prop": "",
                          "tabOrder": {
                            "#type": "expression",
                            "value": "0"
                          }
                        },
                        {
                          "#type": "prop_stroke",
                          "#prop": "stroke"
                        },
                        {
                          "#type": "prop_fill",
                          "#prop": "fill"
                        },
                        {
                          "#prop": "",
                          "style": {
                            "#type": "json",
                            "base": "text",
                            "#props": [
                              {
                                "#type": "prop_text_style_font_size",
                                "#prop": "",
                                "fontSize": {
                                  "#type": "expression",
                                  "value": "32"
                                }
                              },
                              {
                                "#type": "prop_text_style_fill",
                                "#prop": "",
                                "fill": "black"
                              },
                              {
                                "#type": "prop_text_style_stroke",
                                "#prop": "",
                                "stroke": "white"
                              },
                              {
                                "#type": "prop_text_style_stroke_thickness",
                                "#prop": "",
                                "strokeThickness": {
                                  "#type": "expression",
                                  "value": "2"
                                }
                              }
                            ]
                          }
                        }
                      ],
                      "#init": "algorithmic_input"
                    }
                  ],
                  "else": [
                    {
                      "#type": "if_then_else_block",
                      "if": {
                        "#type": "expression",
                        "value": "$item.data == '^'"
                      },
                      "then": [
                        {
                          "#type": "image_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "0"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "0"
                              }
                            },
                            {
                              "#type": "prop_size",
                              "#prop": "",
                              "width": {
                                "#type": "expression",
                                "value": "0"
                              },
                              "height": {
                                "#type": "expression",
                                "value": "0"
                              }
                            },
                            {
                              "#type": "prop_image_key",
                              "#prop": "",
                              "key": "shape.png"
                            },
                            {
                              "#type": "prop_image_src",
                              "#prop": "",
                              "src": "${image_path}shape.png"
                            },
                            {
                              "#type": "prop_scale",
                              "#prop": "",
                              "scale": {
                                "#type": "expression",
                                "value": "0.8"
                              }
                            }
                          ]
                        },
                        {
                          "#type": "choice_input_shape",
                          "#props": [
                            {
                              "#type": "prop_value",
                              "#prop": "",
                              "value": {
                                "#type": "expression",
                                "value": "b % 60"
                              }
                            },
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "0"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "0"
                              }
                            },
                            {
                              "#type": "prop_size",
                              "#prop": "",
                              "width": {
                                "#type": "expression",
                                "value": "75"
                              },
                              "height": {
                                "#type": "expression",
                                "value": "35"
                              }
                            },
                            {
                              "#type": "prop_input_keyboard",
                              "#prop": "",
                              "keyboard": "numbers1"
                            },
                            {
                              "#type": "prop_input_max_length",
                              "#prop": "",
                              "maxLength": {
                                "#type": "expression",
                                "value": "(b % 60).toString().length"
                              }
                            },
                            {
                              "#type": "prop_input_result_position",
                              "#prop": "",
                              "resultPosition": "bottom"
                            },
                            {
                              "#type": "prop_tab_order",
                              "#prop": "",
                              "tabOrder": {
                                "#type": "expression",
                                "value": "0"
                              }
                            },
                            {
                              "#type": "prop_stroke",
                              "#prop": "stroke"
                            },
                            {
                              "#type": "prop_fill",
                              "#prop": "fill"
                            },
                            {
                              "#prop": "",
                              "style": {
                                "#type": "json",
                                "base": "text",
                                "#props": [
                                  {
                                    "#type": "prop_text_style_font_size",
                                    "#prop": "",
                                    "fontSize": {
                                      "#type": "expression",
                                      "value": "32"
                                    }
                                  },
                                  {
                                    "#type": "prop_text_style_fill",
                                    "#prop": "",
                                    "fill": "black"
                                  },
                                  {
                                    "#type": "prop_text_style_stroke",
                                    "#prop": "",
                                    "stroke": "white"
                                  },
                                  {
                                    "#type": "prop_text_style_stroke_thickness",
                                    "#prop": "",
                                    "strokeThickness": {
                                      "#type": "expression",
                                      "value": "2"
                                    }
                                  }
                                ]
                              }
                            }
                          ],
                          "#init": "algorithmic_input"
                        }
                      ],
                      "else": [
                        {
                          "#type": "text_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "0"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "0"
                              }
                            },
                            {
                              "#type": "prop_text_contents",
                              "#prop": "",
                              "contents": "${$item.data}"
                            },
                            {
                              "#prop": "",
                              "style": {
                                "#type": "json",
                                "base": "text",
                                "#props": [
                                  {
                                    "#type": "prop_text_style_font_size",
                                    "#prop": "",
                                    "fontSize": {
                                      "#type": "expression",
                                      "value": "26"
                                    }
                                  },
                                  {
                                    "#type": "prop_text_style_fill",
                                    "#prop": "",
                                    "fill": "black"
                                  },
                                  {
                                    "#type": "prop_text_style_stroke",
                                    "#prop": "",
                                    "stroke": "white"
                                  },
                                  {
                                    "#type": "prop_text_style_stroke_thickness",
                                    "#prop": "",
                                    "strokeThickness": {
                                      "#type": "expression",
                                      "value": "2"
                                    }
                                  }
                                ]
                              }
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "configureKeyboard",
        "args": [
          "numbers1",
          {
            "#type": "expression",
            "value": "0.5"
          },
          {
            "#type": "expression",
            "value": "1"
          },
          {
            "#type": "expression",
            "value": "400"
          },
          {
            "#type": "expression",
            "value": "450"
          },
          {
            "#type": "expression",
            "value": "0.8"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 1: From the question, identify the starting point and ending point of the journey.</u></br>\n</br>\n\"How long does it take to go from ${m[an1]} ${p[an1]} to ${m[an2]} ${p[an2]}?\"</br></br>\n\nStarting point = <b style='color: green'>${m[an1]} ${p[an1]}</b></br>\n\nEnding point = <b style='color: orange'>${m[an2]} ${p[an2]}</b>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 2: From the timetable, identify the departure time at ${m[an1]} ${p[an1]} and arrival time at ${m[an2]} ${p[an2]}.</u></br>\n</br>\n<style>\n  table{background: rgb(204, 204, 0, 0.3)}\ntd{border: 1px solid orange; text-align: center; width: 300px}\n</style>\n<center>\n<table>\n<tr>\n<td colspan='2'>${t[0].toUpperCase()} TIMETABLE</td>\n</tr>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < 6"
        },
        "do": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "i == an1"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<tr>\n<td><b style='color: green'>${m[i]} ${p[i]}</b></td><td><b style='color: green'>${z[i]}</b></td>\n</tr>"
                ]
              }
            ],
            "else": [
              {
                "#type": "if_then_else_block",
                "if": {
                  "#type": "expression",
                  "value": "i == an2"
                },
                "then": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<tr>\n<td><b style='color: orange'>${m[i]} ${p[i]}</b></td><td><b style='color: orange'>${z[i]}</b></td>\n</tr>"
                    ]
                  }
                ],
                "else": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<tr>\n<td>${m[i]} ${p[i]}</td><td>${z[i]}</td>\n</tr>"
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</table>\n</center>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 3: Finally, calculate the difference between the times. </u>\n\n<style>\ntable{width: 900px}\ntd{ text-align: center}\n</style>\n\n<center>\n<table>\n<tr><td colspan='5'>T - CHART</td></tr>\n\n<tr>\n<td rowspan='2' style='background-color: #41fc66'>Start</br>${z[an1]}</td>\n<td style='border-bottom: 3px solid red; border-right: 3px solid red;'></td><td style='border-bottom: 3px solid red'></td>\n<td rowspan='2' style='background-color: #fcc040'>End</br>${z[an2]}</td>\n</tr>\n\n<tr>\n<td style='border-right: 3px solid red;'></td><td></td>\n</tr>\n\n<tr>\n<td></td>"
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<td style='border-right: 3px solid red; vertical-align: top'>"
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<span style='background-color: #41fc66'>${z[an1]}</span></br>"
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "60 - (time[an1] % 60) > 0 && 60 - (time[an1] % 60) < a"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "${two(Math.floor(x[an1]/60)+1) + ' : ' + '00' }</br>"
            ]
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "Math.floor((a- 60 + (time[an1] % 60))/60) > 0"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "${two(Math.floor(x[an2]/60)) + ' : ' + '00' }</br>"
            ]
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<span style='background-color: #fcc040'>${z[an2]}</span></br>"
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</td><td style='background: rgb(150, 200, 250, 0.3); vertical-align: top;'><span>"
        ]
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "60 - (time[an1] % 60) > a"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "${a} minute${a<2?'':'s'} </br>"
            ]
          }
        ],
        "else": [
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "60 - (time[an1] % 60) > 0"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "${60 - (time[an1] % 60)} minute${(60 - (time[an1] % 60))<2?'':'s'} </br>"
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "Math.floor((a- 60 + (time[an1] % 60))/60) > 0"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "${Math.floor((a- 60 + (time[an1] % 60))/60)} hour${Math.floor((a- 60 + (time[an1] % 60))/60)<2?'':'s'} </br>"
            ]
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "((a- 60 + (time[an1] % 60)) % 60) > 0"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "${(a- 60 + (time[an1] % 60)) % 60} minute${((a- 60 + (time[an1] % 60)) % 60)<2?'':'s'} </br>"
            ]
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</br>\nTotal time taken:</br>"
        ]
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "60 - (time[an1] % 60) > a"
        },
        "then": [
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "${a} minutes"
            ]
          }
        ],
        "else": [
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "60 - (time[an1] % 60) > 0"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "${60 - (time[an1] % 60)} minute${(60 - (time[an1] % 60))<2?'':'s'}"
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "Math.floor((a- 60 + (time[an1] % 60))/60) > 0"
        },
        "then": [
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "60 - (time[an1] % 60) > 0"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  {
                    "#type": "expression",
                    "value": "' + '"
                  }
                ]
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "${Math.floor((a- 60 + (time[an1] % 60))/60)} hour${Math.floor((a- 60 + (time[an1] % 60))/60)<2?'':'s'}"
            ]
          }
        ]
      },
      {
        "#type": "if_then_block",
        "if": {
          "#type": "expression",
          "value": "(a- 60 + (time[an1] % 60) % 60) > 0"
        },
        "then": [
          {
            "#type": "if_then_block",
            "if": {
              "#type": "expression",
              "value": "60 - (time[an1] % 60) > 0  || Math.floor((a- 60 + (time[an1] % 60))/60) > 0"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  {
                    "#type": "expression",
                    "value": "' + '"
                  }
                ]
              }
            ]
          },
          {
            "#type": "func",
            "name": "addPartialExplanation",
            "args": [
              "${(a- 60 + (time[an1] % 60)) % 60} minute${((a- 60 + (time[an1] % 60)) % 60)<2?'':'s'}"
            ]
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          {
            "#type": "expression",
            "value": "' = '"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "${Math.floor(a/60)} ${hour} ${a % 60} ${minute}</span></td></tr></table>\n</br>\n<span style='background: rgb(250, 250, 250, 0.3)'>\nIt takes <b>${Math.floor(a/60)}</b> ${hour} <b>${a % 60}</b> ${minute} to travel from <b>${m[an1]} ${p[an1]}</b> to <b>${m[an2]} ${p[an2]}</b>. \n</span></center>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      },
      {
        "#type": "variable",
        "name": "border",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "bobo",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < 12"
        },
        "do": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "an2 == i"
            },
            "then": [
              {
                "#type": "variable",
                "name": "border[i]",
                "value": {
                  "#type": "expression",
                  "value": "'3px solid blue'"
                }
              },
              {
                "#type": "variable",
                "name": "bobo[i]",
                "value": {
                  "#type": "expression",
                  "value": "'3px solid red'"
                }
              }
            ],
            "else": [
              {
                "#type": "if_then_else_block",
                "if": {
                  "#type": "expression",
                  "value": "an2 == array[i]"
                },
                "then": [
                  {
                    "#type": "variable",
                    "name": "border[array[i]]",
                    "value": {
                      "#type": "expression",
                      "value": "'3px solid blue'"
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "bobo[array[i]]",
                    "value": {
                      "#type": "expression",
                      "value": "'3px solid red'"
                    }
                  }
                ],
                "else": [
                  {
                    "#type": "variable",
                    "name": "border[i]",
                    "value": {
                      "#type": "expression",
                      "value": "''"
                    }
                  },
                  {
                    "#type": "variable",
                    "name": "bobo[i]",
                    "value": {
                      "#type": "expression",
                      "value": "''"
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "min",
        "value": {
          "#type": "expression",
          "value": "y[out_min(an2, array)] - x[an2]"
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "Math.floor(b/60) <2"
        },
        "then": [
          {
            "#type": "variable",
            "name": "hour",
            "value": {
              "#type": "expression",
              "value": "'hour'"
            }
          }
        ],
        "else": [
          {
            "#type": "variable",
            "name": "hour",
            "value": {
              "#type": "expression",
              "value": "'hours'"
            }
          }
        ]
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "b % 60 <2"
        },
        "then": [
          {
            "#type": "variable",
            "name": "minute",
            "value": {
              "#type": "expression",
              "value": "'minute'"
            }
          }
        ],
        "else": [
          {
            "#type": "variable",
            "name": "minute",
            "value": {
              "#type": "expression",
              "value": "'minutes'"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Step 4: Now, find out the wait time.</u></br></br>"
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<style>\n  table{background: rgb(204, 204, 0, 0.3)}\ntd{border: 1px solid orange; text-align: center; width: 300px; color: black}\n</style>\n<center>\n<table>\n<tr>\n<td colspan='2'>${t[0].toUpperCase()} TIMETABLE</td><td colspan='2'>${t[1].toUpperCase()} TIMETABLE</td>\n</tr>\n\n<tr>\n<td><span style='border: ${border[0]}; border-radius: 10px;'>${m[0]} ${p[0]}</span></td><td><span style='border: ${bobo[0]}; border-radius: 10px;'>${z[0]}</span></td><td><span style='border: ${border[array[0]]}; border-radius: 10px;'>${m[array[0]]} ${p[array[0]]}</span></td><td><span style='border: ${bobo[array[0]]}; border-radius: 10px;'>${w[0]}</span></td>\n</tr>\n\n<tr>\n<td><span style='border: ${border[1]}; border-radius: 10px;'>${m[1]} ${p[1]}</span></td><td><span style='border: ${bobo[1]}; border-radius: 10px;'>${z[1]}</span></td><td><span style='border: ${border[array[1]]}; border-radius: 10px;'>${m[array[1]]} ${p[array[1]]}</span></td><td><span style='border: ${bobo[array[1]]}; border-radius: 10px;'>${w[1]}</span></td>\n</tr>\n\n<tr>\n<td><span style='border: ${border[2]}; border-radius: 10px;'>${m[2]} ${p[2]}</span></td><td><span style='border: ${bobo[2]}; border-radius: 10px;'>${z[2]}</span></td><td><span style='border: ${border[array[2]]}; border-radius: 10px;'>${m[array[2]]} ${p[array[2]]}</span></td><td><span style='border: ${bobo[array[2]]}; border-radius: 10px;'>${w[2]}</span></td>\n</tr>\n\n<tr>\n<td><span style='border: ${border[3]}; border-radius: 10px;'>${m[3]} ${p[3]}</span></td><td><span style='border: ${bobo[3]}; border-radius: 10px;'>${z[3]}</span></td><td><span style='border: ${border[array[3]]}; border-radius: 10px;'>${m[array[3]]} ${p[array[3]]}</span></td><td><span style='border: ${bobo[array[3]]}; border-radius: 10px;'>${w[3]}</span></td>\n</tr>\n\n<tr>\n<td><span style='border: ${border[4]}; border-radius: 10px;'>${m[4]} ${p[4]}</span></td><td><span style='border: ${bobo[4]}; border-radius: 10px;'>${z[4]}</span></td><td><span style='border: ${border[array[4]]}; border-radius: 10px;'>${m[array[4]]} ${p[array[4]]}</span></td><td><span style='border: ${bobo[array[4]]}; border-radius: 10px;'>${w[4]}</span></td>\n</tr>\n\n<tr>\n<td><span style='border: ${border[5]}; border-radius: 10px;'>${m[5]} ${p[5]}</span></td><td><span style='border: ${bobo[5]}; border-radius: 10px;'>${z[5]}</span></td><td><span style='border: ${border[array[5]]}; border-radius: 10px;'>${m[array[5]]} ${p[array[5]]}</span></td><td><span style='border: ${bobo[array[5]]}; border-radius: 10px;'>${w[5]}</span></td>\n</tr>\n</table>\n\n</br>\n<span style='background: rgb(250, 250, 250, 0.4)'>\nIt will take <b>${min + (min > 2 ? ' minutes': 'minute')}</b> to wait for the bus</br>\nTotally, it takes <b>${Math.floor(b/60)}</b> ${hour} and <b>${b % 60}</b> ${minute}.</span>\n</center>"
        ]
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="sPE;!:R61Fbya5Tt@1r^+" x="0" y="0">
    <field name="name">Y6.MG.TI.TI.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="w=kRm)T7L~;Q5OT/7Pw-">
        <field name="name">image_path</field>
        <value name="value">
          <block type="string_value" id="4IE.c)#Pml6O;~bvy41@1">
            <field name="value">Develop/ImageQAs/Y6.MG.TI.TI.1B/</field>
          </block>
        </value>
        <next>
          <block type="variable" id="]%:O,l_8KryutHtgvd(6">
            <field name="name">background_path</field>
            <value name="value">
              <block type="string_value" id="W87]T]XiO_QQ3nN~nG}E">
                <field name="value">Develop/ImageQAs/General/Backgrounds/</field>
              </block>
            </value>
            <next>
              <block type="variable" id="FyE641`wPL8hDTQNO+!t">
                <field name="name">drop_background</field>
                <value name="value">
                  <block type="random_number" id="F^WBcDcWb(-a!!-]+5pT">
                    <value name="min">
                      <block type="expression" id="6uQkA#xVrKU4y%@2#%I=x">
                        <field name="value">1</field>
                      </block>
                    </value>
                    <value name="max">
                      <block type="expression" id="$)Vhn^`SGD[bT|:5jzs~">
                        <field name="value">15</field>
                      </block>
                    </value>
                  </block>
                </value>
                <next>
                  <block type="background_shape" id="mpFyq7r]zEDKrH`a0:/1">
                    <statement name="#props">
                      <block type="prop_image_key" id="_vrca}g[v-/tj9@2aRiU^">
                        <field name="#prop"></field>
                        <value name="key">
                          <block type="string_value" id="4@1f?D[{ZK!l@2bJLZ4Eg7">
                            <field name="value">bg${drop_background}.png</field>
                          </block>
                        </value>
                        <next>
                          <block type="prop_image_src" id="F%E@2ylS{[%k+,(5FV$!,">
                            <field name="#prop"></field>
                            <value name="src">
                              <block type="string_value" id="i?AtF$PI!9-!09bP$,(0">
                                <field name="value">${background_path}bg${drop_background}.png</field>
                              </block>
                            </value>
                          </block>
                        </next>
                      </block>
                    </statement>
                    <next>
                      <block type="input_param" id="xa]~to/$KCx_l|j==}7O" inline="true">
                        <field name="name">numberOfCorrect</field>
                        <value name="value">
                          <block type="expression" id="lS.xFv@2YpB;$`he^Y/Be">
                            <field name="value">0</field>
                          </block>
                        </value>
                        <next>
                          <block type="input_param" id="gxSEw4,$Nwv}w+)|FXx$" inline="true">
                            <field name="name">numberOfIncorrect</field>
                            <value name="value">
                              <block type="expression" id="are8O|~:_H(nY^Hha!/c">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="variable" id="d=MNyC6kA`J9OHW+;nbp">
                                <field name="name">lst_m</field>
                                <value name="value">
                                  <block type="string_array" id="iz6`dC?[B0CaF.pN`zyf">
                                    <field name="items">Red|Green|Blue|Yellow|Silver|Black|East|West|North|South|Central</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="variable" id="Iborvl}G^zxJUs@1;s[`}">
                                    <field name="name">lst_p</field>
                                    <value name="value">
                                      <block type="string_array" id="#_E:|):~b+{1.kg,uSz]">
                                        <field name="items">Pier|Beach|Hole|Garden|Valley|Forest|Wood|Square|Diamond|Creek|Avenue|Parade</field>
                                      </block>
                                    </value>
                                    <next>
                                      <block type="variable" id="eWlRC]dyL#oy^u^M,-Qx">
                                        <field name="name">lst_t</field>
                                        <value name="value">
                                          <block type="string_array" id="hT?33inF,X9yUxwJeBi$">
                                            <field name="items">train|ferry|tram|bus</field>
                                          </block>
                                        </value>
                                        <next>
                                          <block type="variable" id="OWJ]zwRM@1ZN(|@2KFC,bm">
                                            <field name="name">lst_T</field>
                                            <value name="value">
                                              <block type="string_array" id="]U_Z9@1b7joPpMe}_%+_h">
                                                <field name="items">TRAIN|FERRY|TRAM|BUS</field>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id="Gn_cVuC@2uk=AqHt@1A~;I">
                                                <field name="name">range</field>
                                                <value name="value">
                                                  <block type="expression" id="2JeiYc3CO)Da6+T#dvkP">
                                                    <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="UzL-q[t{f_:{3s#sYJlN">
                                                    <field name="name">an1</field>
                                                    <value name="value">
                                                      <block type="random_number" id=":Gu|Sk(PqUlb2Fb1_Mm7">
                                                        <value name="min">
                                                          <block type="expression" id="+ceJL-=3L_fMYbW`?Xnt">
                                                            <field name="value">0</field>
                                                          </block>
                                                        </value>
                                                        <value name="max">
                                                          <block type="expression" id="m$@1mbA@1Kl{G^cmG?5qz)">
                                                            <field name="value">4</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id="EA1?R@29r1p4Ory1#AA={">
                                                        <field name="name">an2</field>
                                                        <value name="value">
                                                          <block type="random_number" id="pM=VO#+}lIQc1@25^IVLD">
                                                            <value name="min">
                                                              <block type="expression" id="=fs.8$7]na]qSeFzswhH">
                                                                <field name="value">an1 + 1</field>
                                                              </block>
                                                            </value>
                                                            <value name="max">
                                                              <block type="expression" id="bZ86gONucfg:Ck}Xvlar">
                                                                <field name="value">5</field>
                                                              </block>
                                                            </value>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="Uykf?aezVlo@1#Ic0-YDm">
                                                            <field name="name">times</field>
                                                            <value name="value">
                                                              <block type="expression" id="`mn,.R$;wUp.n1}6egLY">
                                                                <field name="value">[12, 13, 14, 15, 16, 17, 18, 24, 25, 26, 27, 28, 29, 30, 36, 37, 38, 39, 40, 41, 42]</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="if_then_else_block" id="=|A4y+o7Cf/]TyGn]W]@1">
                                                                <value name="if">
                                                                  <block type="expression" id="P+I9@1oS=KWw~.I#p@2eV[">
                                                                    <field name="value">range &lt; 0</field>
                                                                  </block>
                                                                </value>
                                                                <statement name="then">
                                                                  <block type="variable" id="zGoO9tXReU[O.zu?r80x">
                                                                    <field name="name">x</field>
                                                                    <value name="value">
                                                                      <block type="random_many" id="[?Hb^?L7Y/~X6zv+)(oc">
                                                                        <value name="count">
                                                                          <block type="expression" id="sU1Di$10pI:Dq5zO#N^a">
                                                                            <field name="value">6</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="items">
                                                                          <block type="expression" id="JbI$]hfQ4.{TyFjE0@2DK">
                                                                            <field name="value">[12, 13, 14, 15, 16, 17, 18, 24, 25, 26, 27, 28, 29, 30, 36, 37]</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="variable" id="ZOOS8PMx1_Awc0otV0_G">
                                                                        <field name="name">i</field>
                                                                        <value name="value">
                                                                          <block type="expression" id="L++O;S#!E#QI|LwOpT4m">
                                                                            <field name="value">0</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="while_do_block" id="h)vL=hL?PQ(?1hUwlO(z">
                                                                            <value name="while">
                                                                              <block type="expression" id="M6J+r3p(q@2;ubSYQ%u?w">
                                                                                <field name="value">i &lt;  6 + an1 + 1</field>
                                                                              </block>
                                                                            </value>
                                                                            <statement name="do">
                                                                              <block type="variable" id="RJlw@1ZYO^l_G(a+O`Y=]">
                                                                                <field name="name">x[i]</field>
                                                                                <value name="value">
                                                                                  <block type="expression" id="Csdp`UA:JmDNhnZG)Je+">
                                                                                    <field name="value">x[i]@25</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="variable" id="!5}k3#jB;/}]@2,4XAkdZ">
                                                                                    <field name="name">i</field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="#Ca|.Ls4f^F6@1/o(-=!t">
                                                                                        <field name="value">i+1</field>
                                                                                      </block>
                                                                                    </value>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </statement>
                                                                            <next>
                                                                              <block type="variable" id="`CDN%gXF,0/NYJZR]`T4">
                                                                                <field name="name">max</field>
                                                                                <value name="value">
                                                                                  <block type="expression" id="kacUzMWhE6#Ix%grC@2ci">
                                                                                    <field name="value">37@25</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </statement>
                                                                <statement name="else">
                                                                  <block type="if_then_else_block" id="v}==AX5~R_p~W@2IM%o2{">
                                                                    <value name="if">
                                                                      <block type="expression" id="B5_3Wf[=;sr^#Nu:a=Ha">
                                                                        <field name="value">range &lt; 4</field>
                                                                      </block>
                                                                    </value>
                                                                    <statement name="then">
                                                                      <block type="variable" id="u{71Ofy[D=t=3a,):nsL">
                                                                        <field name="name">x</field>
                                                                        <value name="value">
                                                                          <block type="random_many" id="6|=#9MqP,Uha#}M44@2fn">
                                                                            <value name="count">
                                                                              <block type="expression" id="CHBl(/w^Zfdx|)IvU0%1">
                                                                                <field name="value">6</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="items">
                                                                              <block type="func_array_of_number" id="-n=p],P7c@1cgX+D$?z$f" inline="true">
                                                                                <value name="items">
                                                                                  <block type="expression" id="F-v7l#tH|g?:B1@1vYyq]">
                                                                                    <field name="value">720</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="from">
                                                                                  <block type="expression" id="K_L?d?cRKU#=7r#?zJ@1{">
                                                                                    <field name="value">60</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="#}gj[LQT^(HRqg];p/t^">
                                                                            <field name="name">max</field>
                                                                            <value name="value">
                                                                              <block type="expression" id="7?+#S`%{(:M2w8!yOxj(">
                                                                                <field name="value">779</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                    <statement name="else">
                                                                      <block type="variable" id="/LR;JV?}ti3hC^bK{g{%">
                                                                        <field name="name">x</field>
                                                                        <value name="value">
                                                                          <block type="random_many" id="7DEVBXcd:iNg34D8;8P8">
                                                                            <value name="count">
                                                                              <block type="expression" id="6qL]6Vdp}ZNac?jf.mqQ">
                                                                                <field name="value">6</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="items">
                                                                              <block type="func_array_of_number" id="^bypc@2RG$B!h[@1`[Ffa5" inline="true">
                                                                                <value name="items">
                                                                                  <block type="expression" id="AIwa[(Q[w8A%sALH5{^h">
                                                                                    <field name="value">1380</field>
                                                                                  </block>
                                                                                </value>
                                                                                <value name="from">
                                                                                  <block type="expression" id="R[[57DpP)F`l5OASHFE3">
                                                                                    <field name="value">60</field>
                                                                                  </block>
                                                                                </value>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="{~:t8#vG!(E.hC,}UUM6">
                                                                            <field name="name">max</field>
                                                                            <value name="value">
                                                                              <block type="expression" id="eQd%.5|GO/|zhc]]@2">
                                                                                <field name="value">1380+59</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </statement>
                                                                  </block>
                                                                </statement>
                                                                <next>
                                                                  <block type="variable" id="a?.=V^Lwrmmvkt2=P}45">
                                                                    <field name="name">x</field>
                                                                    <value name="value">
                                                                      <block type="func_sort" id="e=n,NTbn^^@1q7ZB+}YG$" inline="true">
                                                                        <value name="value">
                                                                          <block type="expression" id="?8Km:jDM(Q.TJD8uC~.D">
                                                                            <field name="value">x</field>
                                                                          </block>
                                                                        </value>
                                                                        <value name="by">
                                                                          <block type="string_value" id="#,QIY6=Y%8|Nm_TChIf~">
                                                                            <field name="value">asc</field>
                                                                          </block>
                                                                        </value>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="variable" id="1ofx$A_[mzapIf50aS/M">
                                                                        <field name="name">t</field>
                                                                        <value name="value">
                                                                          <block type="random_many" id="@1;2^E|O]uk$V)mgX[KA(">
                                                                            <value name="count">
                                                                              <block type="expression" id="h#X~H!i7xcqFx/XH7zp4">
                                                                                <field name="value">2</field>
                                                                              </block>
                                                                            </value>
                                                                            <value name="items">
                                                                              <block type="expression" id="W43rK0Ar7~0W[AOof0`o">
                                                                                <field name="value">lst_t</field>
                                                                              </block>
                                                                            </value>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="statement" id="`l:w0?[GpI)yaFNak7R`">
                                                                            <field name="value">function two(a){
  if(a &lt; 10){return '0' + a;}
  return a;
}

function jsUcfirst(string) 
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function check(x, A) {
    for(var m=0; m&lt;18 || A[m] &lt; 0; m++){
        if(x == A[m]){
            return 1;
        }
    }
    return 0;
}
                                                                            </field>
                                                                            <next>
                                                                              <block type="text_shape" id="9-r;jBf]Xr}u$@21DMC=?">
                                                                                <statement name="#props">
                                                                                  <block type="prop_position" id="8e,Su8?gCGn+3jAj_tVq">
                                                                                    <field name="#prop"></field>
                                                                                    <value name="x">
                                                                                      <block type="resp_x" id=":($V@1@2p_z+?%%/dBGBHR">
                                                                                        <field name="value">cx</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <value name="y">
                                                                                      <block type="expression" id="YKKMu#8[9Rq-TEe_le2h">
                                                                                        <field name="value">40</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="prop_text_contents" id="xs6ga-WLkçGle2B;}3">
                                                                                        <field name="#prop"></field>
                                                                                        <value name="contents">
                                                                                          <block type="string_value" id="M,b-uVy/I+KE~xMWW@2{U">
                                                                                            <field name="value">The timetable below shows the arrival time of the ${t[0]} and the ${t[1]} to different places.</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="prop_text_style" id="hUQ!PgfMwa7s@2#)z^u]8">
                                                                                            <field name="base">text</field>
                                                                                            <statement name="#props">
                                                                                              <block type="prop_text_style_font_size" id="dtqv=OlN?)K}0`H1l=N5">
                                                                                                <field name="#prop"></field>
                                                                                                <value name="fontSize">
                                                                                                  <block type="expression" id="593kTH49dt`NwZig^2LK">
                                                                                                    <field name="value">32</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="prop_text_style_fill" id="YcOpiR6/67XS`QTad]H`">
                                                                                                    <field name="#prop"></field>
                                                                                                    <value name="fill">
                                                                                                      <block type="string_value" id="HxCs9gK5-H@2bkh[z1)/A">
                                                                                                        <field name="value">black</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="prop_text_style_stroke" id="),e$RS:W;x+k;iXV;@2M,">
                                                                                                        <field name="#prop"></field>
                                                                                                        <value name="stroke">
                                                                                                          <block type="string_value" id=";{VWb}g}:Edk~+qFHUD[">
                                                                                                            <field name="value">white</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="prop_text_style_stroke_thickness" id="{lgl(VaqJ~5V6kdO#`h)">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="strokeThickness">
                                                                                                              <block type="expression" id="e;R)v$eLrPh})zfVQ||;">
                                                                                                                <field name="value">2</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </statement>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </statement>
                                                                                <next>
                                                                                  <block type="variable" id=";oOP.qM@1ccx5|Khk;Lyy">
                                                                                    <field name="name">m</field>
                                                                                    <value name="value">
                                                                                      <block type="random_many" id="xE.=+28/s8#xya+Myt,s">
                                                                                        <value name="count">
                                                                                          <block type="expression" id="KxrYDrVxisxf9_w@19gDU">
                                                                                            <field name="value">6+an1+1</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <value name="items">
                                                                                          <block type="expression" id="l#Om+)C,1MsxzfpF]gLV">
                                                                                            <field name="value">lst_m</field>
                                                                                          </block>
                                                                                        </value>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="variable" id="q,t6oEwB|m?_0h_K{CIh">
                                                                                        <field name="name">p</field>
                                                                                        <value name="value">
                                                                                          <block type="random_many" id="FkZBuizeJ#z0R^-6y]n/">
                                                                                            <value name="count">
                                                                                              <block type="expression" id="QW4jixI@1rb!Tp/#-^t%-">
                                                                                                <field name="value">6+an1+1</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <value name="items">
                                                                                              <block type="expression" id="iVvxNmi:4mi`I^]C[Nup">
                                                                                                <field name="value">lst_p</field>
                                                                                              </block>
                                                                                            </value>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="variable" id="01dD@2tMwMy3r8Ur`4$IR">
                                                                                            <field name="name">z</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="enb.$9Dj{?_i_$WJX^hi">
                                                                                                <field name="value">[]</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="variable" id="+@1V-nDBaOLWy,gQ;a3JQ">
                                                                                                <field name="name">i</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="R)njwo.wM!TY@17#hlM0{">
                                                                                                    <field name="value">0</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="while_do_block" id="8$P+bS5=XfZ-bjL2i#!w">
                                                                                                    <value name="while">
                                                                                                      <block type="expression" id="?Ag:q6)tf5+|nL)oBp1j">
                                                                                                        <field name="value">i &lt; 6</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <statement name="do">
                                                                                                      <block type="variable" id="I!-TC5jt@1:14@1g18V1TC">
                                                                                                        <field name="name">z[i]</field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id="Eqj,o5{RsUF~o2aOW|Fq">
                                                                                                            <field name="value">two(Math.floor(x[i]/60)) + ' : ' + two(x[i] % 60)</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="variable" id="LIkf4y0n-_q,}30IsQ,O">
                                                                                                            <field name="name">i</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id="5=Zwus?5F5y4okem?zOY">
                                                                                                                <field name="value">i+1</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </statement>
                                                                                                    <next>
                                                                                                      <block type="image_shape" id="WrulSO^6yNfb9~1.Wx_0">
                                                                                                        <statement name="#props">
                                                                                                          <block type="prop_position" id="uoj%pT5Uhag-X)C?XL%g">
                                                                                                            <field name="#prop"></field>
                                                                                                            <value name="x">
                                                                                                              <block type="expression" id="}kvWQ2L]]}K`Z.kEL{/,">
                                                                                                                <field name="value">400</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <value name="y">
                                                                                                              <block type="expression" id="eC9ApiF6G$QN1uS$3U$3">
                                                                                                                <field name="value">190</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="prop_size" id="vAqPuB?yvc@2o;Ta0^)Y8">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="width">
                                                                                                                  <block type="expression" id="(WFy)+.{2lWgfM[)`X?e">
                                                                                                                    <field name="value">0</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="height">
                                                                                                                  <block type="expression" id="7+.xQ^TbSsC,EqW;#lbA">
                                                                                                                    <field name="value">0</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_anchor" id="%O;m!#58Gf:0UrJ2bKjQ">
                                                                                                                    <field name="#prop">anchor</field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="3t3S9ZYS`@2U9naW}But.">
                                                                                                                        <field name="value">0.5</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="/u??sm812Cb3%QDIY^{1">
                                                                                                                        <field name="value">0.5</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_image_key" id="B:JG.%N@2Q_aS$4ne|^E@1">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="key">
                                                                                                                          <block type="string_value" id="j.-Na-LFhXkVQV#|Mg!Z">
                                                                                                                            <field name="value">table.png</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_image_src" id="Mi_+_88Ej2iWTi!@1#Zab">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="src">
                                                                                                                              <block type="string_value" id="7M,͘;E[R-CJ��">
                                                                                                                                <field name="value">${image_path}table.png</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </statement>
                                                                                                        <next>
                                                                                                          <block type="text_shape" id="UaiklB=Pf2+gyM7o!hD$">
                                                                                                            <statement name="#props">
                                                                                                              <block type="prop_position" id="j+$9VuR,XTlaPnJFri1{">
                                                                                                                <field name="#prop"></field>
                                                                                                                <value name="x">
                                                                                                                  <block type="expression" id="jvm#e^O7Rj6M^eKV[O/E">
                                                                                                                    <field name="value">260</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <value name="y">
                                                                                                                  <block type="expression" id="4-4#RbrFlfg3)KHrB9?o">
                                                                                                                    <field name="value">95</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="prop_text_contents" id="4hM;=ASa4V:]TYt?o-8/">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="contents">
                                                                                                                      <block type="string_value" id="(p$n`e:c#99V~Awe{:$~">
                                                                                                                        <field name="value">${jsUcfirst(t[0])} timetable</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_style" id="8?X-|QSV.ndT-tB8oUQp">
                                                                                                                        <field name="base">text</field>
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_text_style_font_size" id="gTbD@2a;Xgm}BJAl3eTX+">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="fontSize">
                                                                                                                              <block type="expression" id="fk.1xqv;DB:~s+`lkC$k">
                                                                                                                                <field name="value">26</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_text_style_fill" id="z}@2Nu%Ywa#l@1jt,f`WA|">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="fill">
                                                                                                                                  <block type="string_value" id="U+e~RgJ|~S5/,QZxr]5l">
                                                                                                                                    <field name="value">black</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </statement>
                                                                                                            <next>
                                                                                                              <block type="text_shape" id="L]o_={1v87831qfHejhk">
                                                                                                                <statement name="#props">
                                                                                                                  <block type="prop_position" id="``E0XaHPwwHV|LJ8!C?8">
                                                                                                                    <field name="#prop"></field>
                                                                                                                    <value name="x">
                                                                                                                      <block type="expression" id="oUqW1QI-LCAc^Qd|S/h@2">
                                                                                                                        <field name="value">590</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <value name="y">
                                                                                                                      <block type="expression" id="YwhiN/N1AJP|9!yV]S+f">
                                                                                                                        <field name="value">95</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="prop_text_contents" id="^WI=-Q?jV7A0$8y2~a6A">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="contents">
                                                                                                                          <block type="string_value" id="z5vKR3LwWayo`IqZJfw;">
                                                                                                                            <field name="value">${jsUcfirst(t[1])} timetable</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_text_style" id="Y+DAbDt(X`87AxURCYvd">
                                                                                                                            <field name="base">text</field>
                                                                                                                            <statement name="#props">
                                                                                                                              <block type="prop_text_style_font_size" id="8q?6n|j._LDX~$zKOnr~">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="fontSize">
                                                                                                                                  <block type="expression" id="1NahbW-.B6mSfuvyU#QX">
                                                                                                                                    <field name="value">26</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_text_style_fill" id="Hk8k$6BWW6ozX7D+e?m@1">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="fill">
                                                                                                                                      <block type="string_value" id="vjK+(}?HElX4D?Y41L7#">
                                                                                                                                        <field name="value">black</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </statement>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </statement>
                                                                                                                <next>
                                                                                                                  <block type="grid_shape" id="@1ARRbv(WxX.@1COUcF.S]">
                                                                                                                    <statement name="#props">
                                                                                                                      <block type="prop_grid_dimension" id="}5jObUTvK$pyVeBRQ,3~">
                                                                                                                        <field name="#prop"></field>
                                                                                                                        <value name="rows">
                                                                                                                          <block type="expression" id="z-B;x51JUMhDfC}o[q//">
                                                                                                                            <field name="value">6</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <value name="cols">
                                                                                                                          <block type="expression" id="aCCU$dyCE6{F%8WPRQ%o">
                                                                                                                            <field name="value">1</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="prop_position" id="FyJrpPa9Aa$EOVqEgu$g">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="x">
                                                                                                                              <block type="expression" id="29xVeLa@2{Q6NnCDIjvd-">
                                                                                                                                <field name="value">180</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="y">
                                                                                                                              <block type="expression" id="_zrbiQZ=WQezPnVJki(-">
                                                                                                                                <field name="value">205</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_size" id="SZ3l66A-@1rlGQ2,-A,Gi">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="width">
                                                                                                                                  <block type="expression" id="(Qh^y!1[6fNTI#XwLP2)">
                                                                                                                                    <field name="value">220</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="height">
                                                                                                                                  <block type="expression" id="a:mFR)m8[p6P]u-h?H!;">
                                                                                                                                    <field name="value">200</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_grid_cell_source" id="N~A%CIR-;5;t05Up!M/E">
                                                                                                                                    <field name="#prop">cell.source</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="func_array_of_number" id="FO;Dr:BYoQ]3UAv`rj:G" inline="true">
                                                                                                                                        <value name="items">
                                                                                                                                          <block type="expression" id="tLhMtJR-lv;XPAzhyypW">
                                                                                                                                            <field name="value">6</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="from">
                                                                                                                                          <block type="expression" id="@2uvgC[0SVs#N^ac-}iXq">
                                                                                                                                            <field name="value">0</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_anchor" id="}VL(0DUC2ZEM%9|TF|p$">
                                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                                        <value name="x">
                                                                                                                                          <block type="expression" id=",+l40]ZJNMWbZo{y{,Ld">
                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <value name="y">
                                                                                                                                          <block type="expression" id="TL6F$0l@1]c8,+ihD],`W">
                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_grid_show_borders" id="[gH(A6l)yiEafAhlXed3">
                                                                                                                                            <field name="#prop">#showBorders</field>
                                                                                                                                            <field name="value">FALSE</field>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_grid_random" id="_luJnfQC%r,|+]E}H]@1#">
                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                <field name="random">FALSE</field>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_grid_cell_template" id="ly@1i~-6IN%%+i%$as9qu">
                                                                                                                                                    <field name="variable">$cell</field>
                                                                                                                                                    <field name="#prop">cell.template</field>
                                                                                                                                                    <field name="#callback">$cell</field>
                                                                                                                                                    <statement name="body">
                                                                                                                                                      <block type="text_shape" id="nHibzt0H^%Aᾨ+ON]">
                                                                                                                                                        <statement name="#props">
                                                                                                                                                          <block type="prop_position" id="G`ue(9_|5_c]]Gbz@2bL^">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="x">
                                                                                                                                                              <block type="expression" id="@2[f7@2~A88}hw-Pdm)Xar">
                                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="y">
                                                                                                                                                              <block type="expression" id="]=0bhfg5w7J;gnT+SlyC">
                                                                                                                                                                <field name="value">$cell.centerY+5</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_text_contents" id="!@2z(w%5O{$yd)iS/1W!h">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="contents">
                                                                                                                                                                  <block type="string_value" id="a2x{~qDbGS;bT@1RGDtF?">
                                                                                                                                                                    <field name="value">${m[$cell.data]} ${p[$cell.data]}</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_text_style" id=":SvQaj.N%%@2J=M{rkp3o">
                                                                                                                                                                    <field name="base">text</field>
                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                      <block type="prop_text_style_font_size" id="@11/,E@2`y#]?qLU/g-oT{">
                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                        <value name="fontSize">
                                                                                                                                                                          <block type="expression" id="Qf3DNUX:B4oVwXv}s:/%">
                                                                                                                                                                            <field name="value">26</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_text_style_fill" id="%xFM4@2]9vDnr]za7r`Ee">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="fill">
                                                                                                                                                                              <block type="string_value" id="(h]q(Q!i,zFxk`$C@2fq?">
                                                                                                                                                                                <field name="value">black</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </statement>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </statement>
                                                                                                                    <next>
                                                                                                                      <block type="grid_shape" id="-$yF3#?w7.sEvb(iBrA$">
                                                                                                                        <statement name="#props">
                                                                                                                          <block type="prop_grid_dimension" id="[;zPhMnCYn;!3F9/BvvC">
                                                                                                                            <field name="#prop"></field>
                                                                                                                            <value name="rows">
                                                                                                                              <block type="expression" id="5M0LKLDz]Yb:lgu.NKNL">
                                                                                                                                <field name="value">6</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <value name="cols">
                                                                                                                              <block type="expression" id="6uC/a[}K]BX!eb5e[Pgb">
                                                                                                                                <field name="value">1</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="prop_position" id="xxoiV}MGqsK!H{DtG/%K">
                                                                                                                                <field name="#prop"></field>
                                                                                                                                <value name="x">
                                                                                                                                  <block type="expression" id="7pT-{@1Md]-+;`|:w]mvz">
                                                                                                                                    <field name="value">345</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <value name="y">
                                                                                                                                  <block type="expression" id="Fl[9IU69bc_uz?{(4v!K">
                                                                                                                                    <field name="value">205</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="prop_size" id="g//l3n/gg}9_,Z$-J53F">
                                                                                                                                    <field name="#prop"></field>
                                                                                                                                    <value name="width">
                                                                                                                                      <block type="expression" id="e?|N(ZS;W^9@2Ws6VFy1`">
                                                                                                                                        <field name="value">110</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <value name="height">
                                                                                                                                      <block type="expression" id="?!#_zWq@1w(Gz)F]1bl%5">
                                                                                                                                        <field name="value">200</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="prop_grid_cell_source" id="qH|)UK#Z]=0yi=MtQ`-:">
                                                                                                                                        <field name="#prop">cell.source</field>
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="func_array_of_number" id="6YQn#cG1x$lL$k)z~7BN" inline="true">
                                                                                                                                            <value name="items">
                                                                                                                                              <block type="expression" id="5{XUjhJ~1T_OOE`rzk(w">
                                                                                                                                                <field name="value">6</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="from">
                                                                                                                                              <block type="expression" id="lXALg~T[La4Kt$O|6_RS">
                                                                                                                                                <field name="value">0</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="prop_anchor" id="6.4AWD|rWm6WpVrzrj{n">
                                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                                            <value name="x">
                                                                                                                                              <block type="expression" id="Q_G6Y{`0kK?7[Xy?0KGl">
                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <value name="y">
                                                                                                                                              <block type="expression" id="M@1;bCF9axs0OLA#CeJ/r">
                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="prop_grid_show_borders" id=",(Uqz@2s$KWNvPymNnS0:">
                                                                                                                                                <field name="#prop">#showBorders</field>
                                                                                                                                                <field name="value">FALSE</field>
                                                                                                                                                <next>
                                                                                                                                                  <block type="prop_grid_random" id="O7!$%uD+sofEhBm@2w`lf">
                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                    <field name="random">FALSE</field>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="prop_grid_cell_template" id="sc+whOoNRY!~lC/}lhs2">
                                                                                                                                                        <field name="variable">$cell</field>
                                                                                                                                                        <field name="#prop">cell.template</field>
                                                                                                                                                        <field name="#callback">$cell</field>
                                                                                                                                                        <statement name="body">
                                                                                                                                                          <block type="text_shape" id="]wahKS8d~ehpfNP;T+Sl">
                                                                                                                                                            <statement name="#props">
                                                                                                                                                              <block type="prop_position" id="57QwYI(]ZgI2)Ltq|Rz6">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="x">
                                                                                                                                                                  <block type="expression" id="}s(p)D2zEEPhXJmA0+EI">
                                                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <value name="y">
                                                                                                                                                                  <block type="expression" id="Plk}v8QQKaI3JkA.J/K@1">
                                                                                                                                                                    <field name="value">$cell.centerY+5</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_text_contents" id="@2M;~y:7`)XPFy[3INh{K">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="contents">
                                                                                                                                                                      <block type="string_value" id="ppe@1!r(R(l#$?#u3{q8s">
                                                                                                                                                                        <field name="value">${z[$cell.data]}</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_text_style" id="YG!D2):h#8i?h[JNE9Qt">
                                                                                                                                                                        <field name="base">text</field>
                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                          <block type="prop_text_style_font_size" id="zmo0ow@2NJt`?f9q^Bx.v">
                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                            <value name="fontSize">
                                                                                                                                                                              <block type="expression" id="0:OO:uJ0~@2Vw)wqMqGRY">
                                                                                                                                                                                <field name="value">26</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_text_style_fill" id="#[UQdSTHUF=(D@2{?RHA|">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="fill">
                                                                                                                                                                                  <block type="string_value" id="Te%aGiod=4Okxa@2$!W0D">
                                                                                                                                                                                    <field name="value">black</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </statement>
                                                                                                                        <next>
                                                                                                                          <block type="variable" id="R%m#4~kEcP)qG)8r;U/?">
                                                                                                                            <field name="name">w</field>
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="i|ix2xRNRf(zEAz`s04i">
                                                                                                                                <field name="value">[]</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="variable" id="bFz6K=fXa#$.}KoxJBjN">
                                                                                                                                <field name="name">y</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="U5t;/.Y7hnkun4azUNvO">
                                                                                                                                    <field name="value">[]</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="variable" id="%5(dRggnQljsVFoEO%^I">
                                                                                                                                    <field name="name">array</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="=:al:+@2feap8%]L+s?uj">
                                                                                                                                        <field name="value">[]</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="variable" id="aey(.:e=TMoHD48t08$H">
                                                                                                                                        <field name="name">i</field>
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="expression" id="8}zX18U@2aS8RGcN1Guyz">
                                                                                                                                            <field name="value">0</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="while_do_block" id="Xau]0y=ToYRJZ2[O/@2rC">
                                                                                                                                            <value name="while">
                                                                                                                                              <block type="expression" id="LsuC.W[dm+q@1t7ri$e2F">
                                                                                                                                                <field name="value">i &lt; 6</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <statement name="do">
                                                                                                                                              <block type="if_then_else_block" id="756U,6EW)i$3YF=IK%(h">
                                                                                                                                                <value name="if">
                                                                                                                                                  <block type="expression" id="q+!!RD5F9?S|OkOlzOM{">
                                                                                                                                                    <field name="value">i &lt; an1 + 1</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <statement name="then">
                                                                                                                                                  <block type="variable" id="Uv]x;]|OyiAngQ[]n0yL">
                                                                                                                                                    <field name="name">array[i]</field>
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="expression" id="2RgJn3Uu.]neDi:Sb@2EU">
                                                                                                                                                        <field name="value">i+6</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="if_then_else_block" id="h4|Mnj%uLaXL_(;{|knv">
                                                                                                                                                        <value name="if">
                                                                                                                                                          <block type="expression" id="y_4:%KAwZF?l|s:@2ijxn">
                                                                                                                                                            <field name="value">range &lt; 0</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <statement name="then">
                                                                                                                                                          <block type="variable" id="@2(ezt(9+zOdr.=45Cq3P">
                                                                                                                                                            <field name="name">y[i]</field>
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="random_one" id="5]=Gna@1@1UU4fWs/u2CfH">
                                                                                                                                                                <value name="items">
                                                                                                                                                                  <block type="expression" id="vn.(N;PZK0xOysf:[}o`">
                                                                                                                                                                    <field name="value">[12, 13, 14, 15, 16, 17, 18, 24, 25, 26, 27, 28, 29, 30, 36, 37]</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="variable" id="%.t%Ty+QwW/_n/5?6#dw">
                                                                                                                                                                <field name="name">y[i]</field>
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="expression" id=";OcnxK7]N9RE8{XGoM)G">
                                                                                                                                                                    <field name="value">y[i] @2 5</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                        <statement name="else">
                                                                                                                                                          <block type="if_then_else_block" id=":kiS=iqY::!9K~$|~[@1D">
                                                                                                                                                            <value name="if">
                                                                                                                                                              <block type="expression" id="XN([)Ij-|i5[8/?;wr(n">
                                                                                                                                                                <field name="value">range &lt; 4</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <statement name="then">
                                                                                                                                                              <block type="variable" id="@1Tn7yh+U-;g^0#BgEy[$">
                                                                                                                                                                <field name="name">y[i]</field>
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="random_one" id="g2@1.a.Lk^4UC=Bc56G=6">
                                                                                                                                                                    <value name="items">
                                                                                                                                                                      <block type="func_array_of_number" id="-0G_/zJfB0;QA!{ojLn3" inline="true">
                                                                                                                                                                        <value name="items">
                                                                                                                                                                          <block type="expression" id="0M/{dO$8@1gK$ZRv|PiRy">
                                                                                                                                                                            <field name="value">720</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <value name="from">
                                                                                                                                                                          <block type="expression" id="2H_x01waaw32KoBsG`5`">
                                                                                                                                                                            <field name="value">60</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                            <statement name="else">
                                                                                                                                                              <block type="variable" id="#;nD1ZBz^3_/UVZ@2yiza">
                                                                                                                                                                <field name="name">y[i]</field>
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="random_one" id="ObNv@2pq:x@1NFVVeTKn=M">
                                                                                                                                                                    <value name="items">
                                                                                                                                                                      <block type="func_array_of_number" id="L@1itTlBVbU|T++r0D_^v" inline="true">
                                                                                                                                                                        <value name="items">
                                                                                                                                                                          <block type="expression" id="}!@1!X~o?f7DdvUG|ekjr">
                                                                                                                                                                            <field name="value">1380</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <value name="from">
                                                                                                                                                                          <block type="expression" id="jEL#1=y`gOT1yJZSnY@2{">
                                                                                                                                                                            <field name="value">60</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                                <statement name="else">
                                                                                                                                                  <block type="variable" id="dl)IikS)tlV;U}?SQMZU">
                                                                                                                                                    <field name="name">array[i]</field>
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="expression" id="u~VJwze,9I9YVqK@2[GJY">
                                                                                                                                                        <field name="value">i</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="do_while_block" id="1+^)8(@2o!c-@1a)p$b{d]">
                                                                                                                                                        <statement name="do">
                                                                                                                                                          <block type="if_then_else_block" id="ya1S:@2IzEz{l.6pnLVnI">
                                                                                                                                                            <value name="if">
                                                                                                                                                              <block type="expression" id="C8m}1fFaEn-L-=m{K12P">
                                                                                                                                                                <field name="value">range &lt; 0</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <statement name="then">
                                                                                                                                                              <block type="do_while_block" id=",Ie;$q%e-![}c_dy+Tsr">
                                                                                                                                                                <statement name="do">
                                                                                                                                                                  <block type="variable" id="i(fQ}!8Y8+N7SNaXHXe;">
                                                                                                                                                                    <field name="name">rdm</field>
                                                                                                                                                                    <value name="value">
                                                                                                                                                                      <block type="random_one" id="L+7m0vOQi2I(pH(3DZ}~">
                                                                                                                                                                        <value name="items">
                                                                                                                                                                          <block type="expression" id="lOTd^!9%+~G|8FH~p}]:">
                                                                                                                                                                            <field name="value">[5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60]</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </statement>
                                                                                                                                                                <value name="while">
                                                                                                                                                                  <block type="expression" id="Szs0t[t$gj!CDE(i|P+d">
                                                                                                                                                                    <field name="value">check(rdm/5 + x[i]/5, times) == 0 || check(rdm+x[i], y) == 1</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                            <statement name="else">
                                                                                                                                                              <block type="variable" id=")4^l2zFhSArI@18_:H;t3">
                                                                                                                                                                <field name="name">rdm</field>
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="random_number" id="kw/%y`?x)kaE@1z!p,;Z-">
                                                                                                                                                                    <value name="min">
                                                                                                                                                                      <block type="expression" id="z[;N0$LX:VzB4n,/8G$I">
                                                                                                                                                                        <field name="value">5</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <value name="max">
                                                                                                                                                                      <block type="expression" id="YRpT.cheHZQ(r?blH:2b">
                                                                                                                                                                        <field name="value">60</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="variable" id="$DPIjt$mMylMF@2qxccL4">
                                                                                                                                                                <field name="name">y[i]</field>
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="expression" id="%K%.`t_+H[[Bqi[OwgqJ">
                                                                                                                                                                    <field name="value">x[i] + rdm</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                        <value name="while">
                                                                                                                                                          <block type="expression" id="g2wF$g|SXJ=#qXK_oR-)">
                                                                                                                                                            <field name="value">y[i] &gt; max</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </statement>
                                                                                                                                                <next>
                                                                                                                                                  <block type="variable" id="wXQ,i2Vd9Wz2{:ArCS%}">
                                                                                                                                                    <field name="name">w[i]</field>
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="expression" id="[WXiYY,Og=$(2k;F;KB|">
                                                                                                                                                        <field name="value">two(Math.floor(y[i]/60)) + ' : ' + two(y[i] % 60)</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="variable" id="|^pCMlu=-Q_gNJCl(x4@2">
                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="}5D]_rByZg{kOvx?#E_N">
                                                                                                                                                            <field name="value">i+1</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </statement>
                                                                                                                                            <next>
                                                                                                                                              <block type="statement" id="$m;n#BJi(m8,ybWj5O]f">
                                                                                                                                                <field name="value">for(var i=0; i&lt;6; i++){
        for(var j=i+1; j&lt;6; j++){
            if(y[i] &gt; y[j]){
                temp = y[i];
                y[i] = y[j];
                y[j] = temp;

                temp = array[i];
                array[i] = array[j];
                array[j] = temp;

                temp = w[i];
                w[i] = w[j];
                w[j] = temp;
            }
        }
    }
                                                                                                                                                </field>
                                                                                                                                                <next>
                                                                                                                                                  <block type="grid_shape" id="%ardy{b,NHvJhLRSb4P]">
                                                                                                                                                    <statement name="#props">
                                                                                                                                                      <block type="prop_grid_dimension" id="%|y5%Ak9Nx?bJB%.+^-K">
                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                        <value name="rows">
                                                                                                                                                          <block type="expression" id="EktL,k@2?b?!i}Q(D5!Y.">
                                                                                                                                                            <field name="value">6</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <value name="cols">
                                                                                                                                                          <block type="expression" id="rD8kEFqN@20@2@1mBGu-D%}">
                                                                                                                                                            <field name="value">1</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="prop_position" id="gOv#{]RDO/rZy;f%;afv">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="x">
                                                                                                                                                              <block type="expression" id="n}TboQsf]ynoB?@2;}{Jq">
                                                                                                                                                                <field name="value">510</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="y">
                                                                                                                                                              <block type="expression" id="!cLr5v(K;@2#/Mfq}6emA">
                                                                                                                                                                <field name="value">205</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_size" id="jTEae9l~]W3$LB0aFky/">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="width">
                                                                                                                                                                  <block type="expression" id="03@1?.}YZ-]e)i~w.?;:{">
                                                                                                                                                                    <field name="value">210</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <value name="height">
                                                                                                                                                                  <block type="expression" id="Y/vwNAo0_QVo9Z%6id!3">
                                                                                                                                                                    <field name="value">200</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_grid_cell_source" id=",!hPZ={kT0pao+M}^L%$">
                                                                                                                                                                    <field name="#prop">cell.source</field>
                                                                                                                                                                    <value name="value">
                                                                                                                                                                      <block type="func_array_of_number" id="11`g}GR+4duH!dNWWu,Q" inline="true">
                                                                                                                                                                        <value name="items">
                                                                                                                                                                          <block type="expression" id="V?3[0YCf9Nz5=N$^o7C(">
                                                                                                                                                                            <field name="value">6</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <value name="from">
                                                                                                                                                                          <block type="expression" id="h%QV9w{y;2d-SU@1#ALRM">
                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_anchor" id="0v^4JYgGNUNN^eDk0nh9">
                                                                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                                                                        <value name="x">
                                                                                                                                                                          <block type="expression" id="esLx/4LBq6SAuc?57i1b">
                                                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <value name="y">
                                                                                                                                                                          <block type="expression" id="oWl2E{^,layqf|sk^lb@1">
                                                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_grid_show_borders" id="@1a?SY8~LUfDLpDEAHQ,{">
                                                                                                                                                                            <field name="#prop">#showBorders</field>
                                                                                                                                                                            <field name="value">FALSE</field>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_grid_random" id="h2Vs/?Z)t6-Bj~^-iij0">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <field name="random">FALSE</field>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_grid_cell_template" id="QhNw7){K$|#|YR{!7T?K">
                                                                                                                                                                                    <field name="variable">$cell</field>
                                                                                                                                                                                    <field name="#prop">cell.template</field>
                                                                                                                                                                                    <field name="#callback">$cell</field>
                                                                                                                                                                                    <statement name="body">
                                                                                                                                                                                      <block type="text_shape" id="WFt6Z4%ngKQ5pry`UAN5">
                                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                                          <block type="prop_position" id="s-hzk$hi]_)UVQ%ANde!">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <value name="x">
                                                                                                                                                                                              <block type="expression" id="BRg6:@2%I5%ogz+YR[iBt">
                                                                                                                                                                                                <field name="value">$cell.centerX</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <value name="y">
                                                                                                                                                                                              <block type="expression" id="t/@1q{FkIbwu=_o@1PjqKe">
                                                                                                                                                                                                <field name="value">$cell.centerY+5</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_text_contents" id="Uwu7[6d!@2$jZ1(qWT4n!">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <value name="contents">
                                                                                                                                                                                                  <block type="string_value" id="Dci+,aCO9JT@1[cQJmS3P">
                                                                                                                                                                                                    <field name="value">${m[array[$cell.data]]} ${p[array[$cell.data]]}</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="prop_text_style" id="4PJE;@1|f~VT2U)]5;Tm}">
                                                                                                                                                                                                    <field name="base">text</field>
                                                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                                                      <block type="prop_text_style_font_size" id="arE._kf;mI]jMKbM^gI0">
                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                        <value name="fontSize">
                                                                                                                                                                                                          <block type="expression" id="lm7-@2Ln7}C@1L{IBum5aC">
                                                                                                                                                                                                            <field name="value">26</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="prop_text_style_fill" id="/Mw1RPTLgUGJ_AS~)2Iy">
                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                            <value name="fill">
                                                                                                                                                                                                              <block type="string_value" id="E8+GU-}_[c|SFrKa_$+3">
                                                                                                                                                                                                                <field name="value">black</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </statement>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </statement>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </statement>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="grid_shape" id="l0p_TK8;(9QQPn]Qh@2.t">
                                                                                                                                                        <statement name="#props">
                                                                                                                                                          <block type="prop_grid_dimension" id="Op6A=492[9c3J#RDy#LE">
                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                            <value name="rows">
                                                                                                                                                              <block type="expression" id="BSz@2L~6LJxj?!a{ums|H">
                                                                                                                                                                <field name="value">6</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <value name="cols">
                                                                                                                                                              <block type="expression" id="MXtU(rd}P0@2bv=+#X.RW">
                                                                                                                                                                <field name="value">1</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="prop_position" id="7,kd.3fUG_YO3YxSFcu?">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="x">
                                                                                                                                                                  <block type="expression" id="t/8;ptN$2]X;kBW3@2Wcq">
                                                                                                                                                                    <field name="value">670</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <value name="y">
                                                                                                                                                                  <block type="expression" id="PeKi,~:2@1pkzNX.nNLdj">
                                                                                                                                                                    <field name="value">205</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_size" id="cR;Lx:gtN`HFj:D}|-0l">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="width">
                                                                                                                                                                      <block type="expression" id="bmQF[mU?[)8^,/[+%R,!">
                                                                                                                                                                        <field name="value">110</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <value name="height">
                                                                                                                                                                      <block type="expression" id="$M)9u8w1:6iNW#KXV9}:">
                                                                                                                                                                        <field name="value">200</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_grid_cell_source" id="!FKU[U8zKZIQESiJTZdu">
                                                                                                                                                                        <field name="#prop">cell.source</field>
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="func_array_of_number" id="4};P/RHYp^$v6)/M-I8|" inline="true">
                                                                                                                                                                            <value name="items">
                                                                                                                                                                              <block type="expression" id="Hr1gRFH74Woui_YFa,XY">
                                                                                                                                                                                <field name="value">6</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="from">
                                                                                                                                                                              <block type="expression" id="?7cs[6ZP~hF^DlgVgJV~">
                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_anchor" id="ZAlV@1:rA3qoN4cAWR6]s">
                                                                                                                                                                            <field name="#prop">anchor</field>
                                                                                                                                                                            <value name="x">
                                                                                                                                                                              <block type="expression" id="ca(kNsb0jIFU[4p]a$8F">
                                                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <value name="y">
                                                                                                                                                                              <block type="expression" id="m:q:qP}cj[ngmQG!{+(i">
                                                                                                                                                                                <field name="value">0.5</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="prop_grid_show_borders" id="^-{KjXZrTLRM:^v1qW%$">
                                                                                                                                                                                <field name="#prop">#showBorders</field>
                                                                                                                                                                                <field name="value">FALSE</field>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_grid_random" id="$2Y96kQs++F~`)/,:vp%">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <field name="random">FALSE</field>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_grid_cell_template" id="iSX+fukF@1D[/E[rQJ|9H">
                                                                                                                                                                                        <field name="variable">$cell</field>
                                                                                                                                                                                        <field name="#prop">cell.template</field>
                                                                                                                                                                                        <field name="#callback">$cell</field>
                                                                                                                                                                                        <statement name="body">
                                                                                                                                                                                          <block type="text_shape" id="dh55`t85f{k.M,YeWa)T">
                                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                                              <block type="prop_position" id="z[U@1[KXf,b#:yb$|5tlc">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <value name="x">
                                                                                                                                                                                                  <block type="expression" id="DdIJFwR^8kxA-}Hx(nfr">
                                                                                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <value name="y">
                                                                                                                                                                                                  <block type="expression" id="U}mcLka6,`dp11oC%k/G">
                                                                                                                                                                                                    <field name="value">$cell.centerY+5</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="prop_text_contents" id="`INqzZx$HN?cRzAQEmt$">
                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                    <value name="contents">
                                                                                                                                                                                                      <block type="string_value" id="n,gXoU#kM{Mof1+ktGC/">
                                                                                                                                                                                                        <field name="value">${w[$cell.data]}</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="prop_text_style" id="Q7`0[6uEhBq;lb@1/P?$8">
                                                                                                                                                                                                        <field name="base">text</field>
                                                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                                                          <block type="prop_text_style_font_size" id="~O({yo+pgR9UEAk`Cd}g">
                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                            <value name="fontSize">
                                                                                                                                                                                                              <block type="expression" id="(-5@2L@1D]w@1IVX+YqOOpu">
                                                                                                                                                                                                                <field name="value">26</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                            <next>
                                                                                                                                                                                                              <block type="prop_text_style_fill" id="4lIht+6N_sh+:J)AS:NI">
                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                <value name="fill">
                                                                                                                                                                                                                  <block type="string_value" id="#mSC64.d{wCw@2@1.$6W;k">
                                                                                                                                                                                                                    <field name="value">black</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </next>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </statement>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </statement>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </statement>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="text_shape" id="D,L)v+mL3ndThDe6?_;1">
                                                                                                                                                            <statement name="#props">
                                                                                                                                                              <block type="prop_position" id="n;`IoI#.|#5TSHk$G}$4">
                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                <value name="x">
                                                                                                                                                                  <block type="expression" id="Y$a8p|7;@1WW~iyy$;+z|">
                                                                                                                                                                    <field name="value">10</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <value name="y">
                                                                                                                                                                  <block type="expression" id="Ra4C5ws,R8J8p.}=8:iO">
                                                                                                                                                                    <field name="value">360</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="prop_text_contents" id="GY1EP22bhVZVD4zAmVNz">
                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                    <value name="contents">
                                                                                                                                                                      <block type="string_value" id="^eNKPPURBKHgo9I3xX{Z">
                                                                                                                                                                        <field name="value">How long does it take to travel from ${m[an1]} ${p[an1]} to ${m[an2]} ${p[an2]} and wait for a ${t[1]} there?</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="prop_anchor" id="]#]gSCQT=uMYoI8Te7PB">
                                                                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                                                                        <value name="x">
                                                                                                                                                                          <block type="expression" id="$mX8M4Xq:ZacHOw9+69g">
                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <value name="y">
                                                                                                                                                                          <block type="expression" id="ivG?%`muHq;F?yi?hB{:">
                                                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="prop_text_style" id="NdpG6!1Uu)-UVE`kaTSM">
                                                                                                                                                                            <field name="base">text</field>
                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                              <block type="prop_text_style_font_size" id=":;!by-cYZ;^jG?p}l}K(">
                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                <value name="fontSize">
                                                                                                                                                                                  <block type="expression" id="!M%$DzM%UO0OYLkHw8nu">
                                                                                                                                                                                    <field name="value">26</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="prop_text_style_fill" id="%t1!KSpJEbcM)@2r%NwTg">
                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                    <value name="fill">
                                                                                                                                                                                      <block type="string_value" id=";5y0jKsHPii$/wceT9vN">
                                                                                                                                                                                        <field name="value">black</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="prop_text_style_stroke" id="gzJxXMzA4ZCci_@2/1o5`">
                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                        <value name="stroke">
                                                                                                                                                                                          <block type="string_value" id="Az$|Wq`nc,)g-XtU@1C$B">
                                                                                                                                                                                            <field name="value">white</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="prop_text_style_stroke_thickness" id="7TU5i@2fHYu0GR1Gr{n)|">
                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                            <value name="strokeThickness">
                                                                                                                                                                                              <block type="expression" id="0-BytNuY}bxxBI9XHQBn">
                                                                                                                                                                                                <field name="value">2</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="prop_text_style_wordwrap" id="`5(@2R4yGvMN(735JcF-8">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <value name="wordWrap">
                                                                                                                                                                                                  <block type="expression" id="byb~f`SHw6Bf,,etQH9}">
                                                                                                                                                                                                    <field name="value">true</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="prop_text_style_wordWrapWidth" id="Un6C([~WM`.oa{F[cXmq">
                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                    <value name="wordWrapWidth">
                                                                                                                                                                                                      <block type="expression" id="6m7qeOCON[vs6-v}w8$k">
                                                                                                                                                                                                        <field name="value">430</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </statement>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </statement>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="statement" id="KK2H(-Che1=?^eU8(Kq+">
                                                                                                                                                                <field name="value">function out_min(str, array){
    for(var i = 0; i&lt; array.length; i++){
        if(str == array[i]){
          return i;
        }
    }
}
                                                                                                                                                                </field>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="variable" id="}]W!%]os4RXoRk}z}HZ[">
                                                                                                                                                                    <field name="name">time</field>
                                                                                                                                                                    <value name="value">
                                                                                                                                                                      <block type="expression" id="XKELAf5d`wx,P87JiDTq">
                                                                                                                                                                        <field name="value">x</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="variable" id="PjW-`L=S{iYnht~LDD|E">
                                                                                                                                                                        <field name="name">a</field>
                                                                                                                                                                        <value name="value">
                                                                                                                                                                          <block type="expression" id="ZEa|q#K90u^}!Y?lJn|S">
                                                                                                                                                                            <field name="value">time[an2] - time[an1]</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="variable" id="IrM]~BeaYr:(JR[wTPHq">
                                                                                                                                                                            <field name="name">b</field>
                                                                                                                                                                            <value name="value">
                                                                                                                                                                              <block type="expression" id="(wmkg[M_mkMJPkcjk;Pm">
                                                                                                                                                                                <field name="value">a + (y[out_min(an2, array)] - time[an2])</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="if_then_else_block" id="]-H!?PdfEstXtcqs3eb.">
                                                                                                                                                                                <value name="if">
                                                                                                                                                                                  <block type="expression" id="f+w:{0qj-,%v~GX`Fm_X">
                                                                                                                                                                                    <field name="value">Math.floor(a/60) &lt;2</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <statement name="then">
                                                                                                                                                                                  <block type="variable" id="}w+xNpz`7w~73c3uqHuV">
                                                                                                                                                                                    <field name="name">hour</field>
                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                      <block type="expression" id="j-jx.vR=$2J;4T-1Q8k7">
                                                                                                                                                                                        <field name="value">'hour'</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </statement>
                                                                                                                                                                                <statement name="else">
                                                                                                                                                                                  <block type="variable" id="enh?!Wku`xGM6]LFBN_]">
                                                                                                                                                                                    <field name="name">hour</field>
                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                      <block type="expression" id="q@1AUx)lez$10PUyan0qj">
                                                                                                                                                                                        <field name="value">'hours'</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </statement>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="if_then_else_block" id="YpNx-cbG8sv_8_$Q~pl0">
                                                                                                                                                                                    <value name="if">
                                                                                                                                                                                      <block type="expression" id="utmsa(nS_(2r=0#,,T]9">
                                                                                                                                                                                        <field name="value">a % 60 &lt;2</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <statement name="then">
                                                                                                                                                                                      <block type="variable" id="ML,5|xeJ|lBӳ`IMjF">
                                                                                                                                                                                        <field name="name">minute</field>
                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                          <block type="expression" id="K6tPZc6|FU^gZ@2ypiuB#">
                                                                                                                                                                                            <field name="value">'minute'</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </statement>
                                                                                                                                                                                    <statement name="else">
                                                                                                                                                                                      <block type="variable" id="@18~?!%n(#.nv7FFQhr:r">
                                                                                                                                                                                        <field name="name">minute</field>
                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                          <block type="expression" id="w7mMUXb)XB=q?{OTKj5[">
                                                                                                                                                                                            <field name="value">'minutes'</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </statement>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="variable" id="{M+xO=6Xu9a6~gVY0~^H">
                                                                                                                                                                                        <field name="name">str</field>
                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                          <block type="expression" id="txf74KyOR!7_ENnmlR;3">
                                                                                                                                                                                            <field name="value">'_  ' + (Math.floor(b/60) &lt;2 ? 'hour':'hours')+ '  ^  ' + (b % 60 &lt;2 ? 'minute':'minutes')</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="list_shape" id="vLzdKx4brY]$IV=R/YNT">
                                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                                              <block type="prop_list_direction" id="rc:kpC=t?yA3cou@2#s3R">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <field name="dir">horizontal</field>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="prop_position" id="6H3,d/gK{ip7`QlxU3g7">
                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                    <value name="x">
                                                                                                                                                                                                      <block type="expression" id="D^Xi:lH#:|(7|(e`Xs]?">
                                                                                                                                                                                                        <field name="value">780</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <value name="y">
                                                                                                                                                                                                      <block type="expression" id="n4%FGeme=4vaY@2-$9q@1W">
                                                                                                                                                                                                        <field name="value">350</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="prop_anchor" id="Tb%~rX#[Y;/gPp!c5LDZ">
                                                                                                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                                                                                                        <value name="x">
                                                                                                                                                                                                          <block type="expression" id="(N}jRbv?Ctt#2?l.V(ID">
                                                                                                                                                                                                            <field name="value">1</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <value name="y">
                                                                                                                                                                                                          <block type="expression" id="`0FQuo9i#QV|1_%+:80{">
                                                                                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="prop_spacing" id="UU3S:CA$#rm^/m(Z3t87">
                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                            <value name="spacing">
                                                                                                                                                                                                              <block type="expression" id="$yNooz7,LWn7!+,m,Sk:">
                                                                                                                                                                                                                <field name="value">-2</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </statement>
                                                                                                                                                                                            <statement name="items">
                                                                                                                                                                                              <block type="list_item_shape" id=",!Wid96itcS?6G];jIn/">
                                                                                                                                                                                                <statement name="#props">
                                                                                                                                                                                                  <block type="prop_list_align" id="bQeH~S7Hh0QcIdr0~:@2D">
                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                    <field name="align">middle</field>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="prop_list_item_source" id="lFf`FxG!@1l%4Ql^|T)F?">
                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                        <value name="source">
                                                                                                                                                                                                          <block type="expression" id="?Bi`QmN!PZ.LY-0Kk/Yn">
                                                                                                                                                                                                            <field name="value">str</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </statement>
                                                                                                                                                                                                <statement name="template">
                                                                                                                                                                                                  <block type="if_then_else_block" id="$/Df8+p,qDeq[gs5RiEB">
                                                                                                                                                                                                    <value name="if">
                                                                                                                                                                                                      <block type="expression" id="p(sBl|,FD37YNP^vS|Cy">
                                                                                                                                                                                                        <field name="value">$item.data == '_'</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <statement name="then">
                                                                                                                                                                                                      <block type="image_shape" id="k$x`L2}nNiii_}O3AdJu">
                                                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                                                          <block type="prop_position" id="Ior$C{|4k`EO{#U]L/|0">
                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                            <value name="x">
                                                                                                                                                                                                              <block type="expression" id="_+FYx6uN#bBLRV~Fye0L">
                                                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                            <value name="y">
                                                                                                                                                                                                              <block type="expression" id="AAVaYp^J~dC|yYH05YtD">
                                                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                            <next>
                                                                                                                                                                                                              <block type="prop_size" id="(WYj7S[59IqLd,}tU$@1c">
                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                <value name="width">
                                                                                                                                                                                                                  <block type="expression" id="rFKPZAXJr?.4?l]R=5|h">
                                                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                <value name="height">
                                                                                                                                                                                                                  <block type="expression" id="9w@2fe_lEenly]RuTSB;K">
                                                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                  <block type="prop_image_key" id="_Zo//AIS@2Ygm?$tHr%3%">
                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                    <value name="key">
                                                                                                                                                                                                                      <block type="string_value" id="Wt/q50Y`Mk87t#zx`gE!">
                                                                                                                                                                                                                        <field name="value">shape.png</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                      <block type="prop_image_src" id="40j|;O{f^?Lr3y@1)$.wg">
                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                        <value name="src">
                                                                                                                                                                                                                          <block type="string_value" id="u;#$vFuWwE2##^9f`/jw">
                                                                                                                                                                                                                            <field name="value">${image_path}shape.png</field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                          <block type="prop_scale" id="Nv)X)MV;}8@1BGH[qoUz#">
                                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                                            <value name="scale">
                                                                                                                                                                                                                              <block type="expression" id="T@17,#|NQ4=s0K``!o~@17">
                                                                                                                                                                                                                                <field name="value">0.8</field>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </next>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </next>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="algorithmic_input_shape" id="Ap4~)#|yneG~/W@1xBXK;">
                                                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                                                              <block type="prop_value" id="4xDsa!xV7u1.w]748nHD">
                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                  <block type="expression" id="{rnNy2@1JYd0+0r`N2|An">
                                                                                                                                                                                                                    <field name="value">Math.floor(b/60)</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                  <block type="prop_position" id="fsm~(eSs~7YW_xLkeL91">
                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                    <value name="x">
                                                                                                                                                                                                                      <block type="expression" id="oi:17Jg^A#5u_hpUDM@2/">
                                                                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                    <value name="y">
                                                                                                                                                                                                                      <block type="expression" id="^uV#^?!m8Wc82,p6yfxF">
                                                                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                      <block type="prop_size" id="vZw5]}?kK~fX{h3)`Vuf">
                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                        <value name="width">
                                                                                                                                                                                                                          <block type="expression" id="[0k);uLoll}5.%:MTJ?^">
                                                                                                                                                                                                                            <field name="value">75</field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                        <value name="height">
                                                                                                                                                                                                                          <block type="expression" id="lqGq|uiZN3vsEG;Jiqfb">
                                                                                                                                                                                                                            <field name="value">35</field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                          <block type="prop_input_keyboard" id="I~L5Mjx(IzuBR4h:at~k">
                                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                                            <field name="keyboard">numbers1</field>
                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                              <block type="prop_input_max_length" id="#a|cja!;QQI#E]5rLBh#">
                                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                                <value name="maxLength">
                                                                                                                                                                                                                                  <block type="expression" id="(Bc@1@1@25(%R6uF{Cmj0^3">
                                                                                                                                                                                                                                    <field name="value">Math.floor(b/60).toString().length</field>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                  <block type="prop_input_result_position" id="`,aUG()TqcHwq.4-k17T">
                                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                                    <field name="resultPosition">bottom</field>
                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                      <block type="prop_tab_order" id="GbE=(!f:C[`[P]RR+fBe">
                                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                                        <value name="tabOrder">
                                                                                                                                                                                                                                          <block type="expression" id="msc1y]?1q#;!QGxtK7(O">
                                                                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                          <block type="prop_stroke" id="tf(/ui1ZbND-@13m?_UVD">
                                                                                                                                                                                                                                            <field name="#prop">stroke</field>
                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                              <block type="prop_fill" id="TFT!GF%Z{vO#~bIoX,`|">
                                                                                                                                                                                                                                                <field name="#prop">fill</field>
                                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                                  <block type="prop_text_style" id="UxVM1b(;6_I@14B_+S4;E">
                                                                                                                                                                                                                                                    <field name="base">text</field>
                                                                                                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                                                                                                      <block type="prop_text_style_font_size" id="n|8=Ib97qz#DSF9r|[o@1">
                                                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                                                        <value name="fontSize">
                                                                                                                                                                                                                                                          <block type="expression" id="wOx^@1=A#cC~FTm+$@1AY4">
                                                                                                                                                                                                                                                            <field name="value">32</field>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                                          <block type="prop_text_style_fill" id="[c%}HYR2[L++60.qYa7?">
                                                                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                                                                            <value name="fill">
                                                                                                                                                                                                                                                              <block type="string_value" id="QcyE~$Sy)QEVbqmWSvNV">
                                                                                                                                                                                                                                                                <field name="value">black</field>
                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                                              <block type="prop_text_style_stroke" id="3,zU,651J]~qG.@1b(muK">
                                                                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                                                                <value name="stroke">
                                                                                                                                                                                                                                                                  <block type="string_value" id="!Z#~VnoiAC59YRk0BSDB">
                                                                                                                                                                                                                                                                    <field name="value">white</field>
                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                                                  <block type="prop_text_style_stroke_thickness" id="Yd6DK`0@2ZZub;-#ffT@1I">
                                                                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                                                                    <value name="strokeThickness">
                                                                                                                                                                                                                                                                      <block type="expression" id="_C:AUii]2|8zSWr3fvSf">
                                                                                                                                                                                                                                                                        <field name="value">2</field>
                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </next>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                    <statement name="else">
                                                                                                                                                                                                      <block type="if_then_else_block" id="wZn$yW;L!^Rzh0y#~B!H">
                                                                                                                                                                                                        <value name="if">
                                                                                                                                                                                                          <block type="expression" id="7tfKN/AH8B-aRCVPJ91B">
                                                                                                                                                                                                            <field name="value">$item.data == '^'</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <statement name="then">
                                                                                                                                                                                                          <block type="image_shape" id="Fzapt8QXi;@1CZj,@2d9%j">
                                                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                                                              <block type="prop_position" id="W5b0[JO$5t1#RMeQ21F!">
                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                <value name="x">
                                                                                                                                                                                                                  <block type="expression" id="=122S`[[91+WoNZB8cAV">
                                                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                <value name="y">
                                                                                                                                                                                                                  <block type="expression" id="LfDGWRqH,e.L!1[i@2Ypo">
                                                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                  <block type="prop_size" id="NRfkqIt(]C5{OIdh2dT|">
                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                    <value name="width">
                                                                                                                                                                                                                      <block type="expression" id="|hf:~cM,w,[%F./g{EOX">
                                                                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                    <value name="height">
                                                                                                                                                                                                                      <block type="expression" id="TyDPOc$jHugblS2_Q8$i">
                                                                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                      <block type="prop_image_key" id="u]V=Twq}cG{4!`VK$Zk`">
                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                        <value name="key">
                                                                                                                                                                                                                          <block type="string_value" id="5vR+a)nB~{$v^aWC$,Er">
                                                                                                                                                                                                                            <field name="value">shape.png</field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                          <block type="prop_image_src" id="@2}Vk)z7(2jj41k~Be//B">
                                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                                            <value name="src">
                                                                                                                                                                                                                              <block type="string_value" id="1Yx=)IbOSU3tCowQRo5X">
                                                                                                                                                                                                                                <field name="value">${image_path}shape.png</field>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                              <block type="prop_scale" id="a@2Wo;m%R4cl@1D)/W2TC/">
                                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                                <value name="scale">
                                                                                                                                                                                                                                  <block type="expression" id="k{/;VuaOiggUI2.v=q36">
                                                                                                                                                                                                                                    <field name="value">0.8</field>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </next>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                            <next>
                                                                                                                                                                                                              <block type="algorithmic_input_shape" id="[e}LcmqF@19.%T?]@1jB-Z">
                                                                                                                                                                                                                <statement name="#props">
                                                                                                                                                                                                                  <block type="prop_value" id="TN6clfe}:4H;@1k0w%+`S">
                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                      <block type="expression" id="@1Y0=VVDuz^gR8?]A.GgV">
                                                                                                                                                                                                                        <field name="value">b % 60</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                      <block type="prop_position" id="(u^k=nloWKrc75-ihkQ6">
                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                        <value name="x">
                                                                                                                                                                                                                          <block type="expression" id="+SQo+%UqGFz6@107iOi%4">
                                                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                        <value name="y">
                                                                                                                                                                                                                          <block type="expression" id="aOD#P0{Z$l:BCNh)9@2~E">
                                                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                          <block type="prop_size" id="~)S$^+Glx|;{-)@1m?MCt">
                                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                                            <value name="width">
                                                                                                                                                                                                                              <block type="expression" id="wdRRw?lccA]3huvks^0:">
                                                                                                                                                                                                                                <field name="value">75</field>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                            <value name="height">
                                                                                                                                                                                                                              <block type="expression" id="FlQ@1R-hpt5|W3YEsjl5h">
                                                                                                                                                                                                                                <field name="value">35</field>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                              <block type="prop_input_keyboard" id="pX[+1]_P03Uw?r6RhxOm">
                                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                                <field name="keyboard">numbers1</field>
                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                  <block type="prop_input_max_length" id="e^196@2S|6ukB7~2Bxi9C">
                                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                                    <value name="maxLength">
                                                                                                                                                                                                                                      <block type="expression" id="/_E;-7``ONG+]nS_NxH+">
                                                                                                                                                                                                                                        <field name="value">(b % 60).toString().length</field>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                      <block type="prop_input_result_position" id="#LH4fkex}OZhG#[@2d;3P">
                                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                                        <field name="resultPosition">bottom</field>
                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                          <block type="prop_tab_order" id="s|F@1Izb{|@2lRY,asD!hu">
                                                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                                                            <value name="tabOrder">
                                                                                                                                                                                                                                              <block type="expression" id="2[il+_dgVQKQm?SvtFDj">
                                                                                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                              <block type="prop_stroke" id="D`nCliS/s1emx9euC0}t">
                                                                                                                                                                                                                                                <field name="#prop">stroke</field>
                                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                                  <block type="prop_fill" id="Dz-AaH/!wx?BO[MH9dIz">
                                                                                                                                                                                                                                                    <field name="#prop">fill</field>
                                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                                      <block type="prop_text_style" id="!1Qr3{IxD@1PMJ]G^LlcN">
                                                                                                                                                                                                                                                        <field name="base">text</field>
                                                                                                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                                                                                                          <block type="prop_text_style_font_size" id="!(nlSpda9T+o/iXBj}YF">
                                                                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                                                                            <value name="fontSize">
                                                                                                                                                                                                                                                              <block type="expression" id="FQTZZB_+$@1-hM?K.!NSO">
                                                                                                                                                                                                                                                                <field name="value">32</field>
                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                                              <block type="prop_text_style_fill" id="w-4`M-zsemYrO[}g`_|q">
                                                                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                                                                <value name="fill">
                                                                                                                                                                                                                                                                  <block type="string_value" id="G;VmNGg}u8OZR_#3M_Oe">
                                                                                                                                                                                                                                                                    <field name="value">black</field>
                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                                                  <block type="prop_text_style_stroke" id="ahppz0%Y{eD@2C$!vyJ4m">
                                                                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                                                                    <value name="stroke">
                                                                                                                                                                                                                                                                      <block type="string_value" id="}KNcD$1%M)+@1a=7A%7,,">
                                                                                                                                                                                                                                                                        <field name="value">white</field>
                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="TBWlBG{V@2q@2LOjXi7|@16">
                                                                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                                                                        <value name="strokeThickness">
                                                                                                                                                                                                                                                                          <block type="expression" id="bs_awM@2W;X@1OG#fQ+@2no">
                                                                                                                                                                                                                                                                            <field name="value">2</field>
                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </next>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                        <statement name="else">
                                                                                                                                                                                                          <block type="text_shape" id="$8{qT#8xQdInD0pKa/25">
                                                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                                                              <block type="prop_position" id="JRZ7m,[LX8kb5e}uwQEg">
                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                <value name="x">
                                                                                                                                                                                                                  <block type="expression" id="AH}X:TQ0hjhq4=Rz,sET">
                                                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                <value name="y">
                                                                                                                                                                                                                  <block type="expression" id="{B`W34@1|=urfT:t.nROw">
                                                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                  <block type="prop_text_contents" id="}G.ZD7d@2?(JL?/aO)7zG">
                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                    <value name="contents">
                                                                                                                                                                                                                      <block type="string_value" id="({rB(E^RWaIA.eThZ#4Y">
                                                                                                                                                                                                                        <field name="value">${$item.data}</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                      <block type="prop_text_style" id="%Y[PEs3MY#o|f7@2O+cGq">
                                                                                                                                                                                                                        <field name="base">text</field>
                                                                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                                                                          <block type="prop_text_style_font_size" id="87CBOo/HN?MO(/?0fa2l">
                                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                                            <value name="fontSize">
                                                                                                                                                                                                                              <block type="expression" id="Ryk_M#D5L{Z_QG}yj?_P">
                                                                                                                                                                                                                                <field name="value">26</field>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                              <block type="prop_text_style_fill" id="+DTwZUi2SP?tV!!OwNVT">
                                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                                <value name="fill">
                                                                                                                                                                                                                                  <block type="string_value" id="eH_nUejNuO6uSCVI2btj">
                                                                                                                                                                                                                                    <field name="value">black</field>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                  <block type="prop_text_style_stroke" id="B5d3FxW-UD}uww$8l4uh">
                                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                                    <value name="stroke">
                                                                                                                                                                                                                                      <block type="string_value" id="kV43goga^D,dQL~@1_NE:">
                                                                                                                                                                                                                                        <field name="value">white</field>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="qOUr;!}NYdr$};KVbY2m">
                                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                                        <value name="strokeThickness">
                                                                                                                                                                                                                                          <block type="expression" id="EN{L#ZAvL`jd}VU!1n!)">
                                                                                                                                                                                                                                            <field name="value">2</field>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </next>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </statement>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </statement>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="configure_keyboard" id="n~=1_P0GLVFxt4r7~m/9">
                                                                                                                                                                                                <field name="keyboard">numbers1</field>
                                                                                                                                                                                                <value name="anchorX">
                                                                                                                                                                                                  <block type="expression" id="^wbQK+~-R)n[5|_Z%P^V">
                                                                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <value name="anchorY">
                                                                                                                                                                                                  <block type="expression" id="U3$76b(d${_+7An@1yjyK">
                                                                                                                                                                                                    <field name="value">1</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <value name="x">
                                                                                                                                                                                                  <block type="expression" id="1z[K($qAS[~;n${CSqws">
                                                                                                                                                                                                    <field name="value">400</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <value name="y">
                                                                                                                                                                                                  <block type="expression" id="[W]=5o|(ndtBoxqr.@2|O">
                                                                                                                                                                                                    <field name="value">450</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <value name="scale">
                                                                                                                                                                                                  <block type="expression" id="a1{8gqZk}H/Drc,94LzD">
                                                                                                                                                                                                    <field name="value">0.8</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="partial_explanation" id="s+c0.J=$O0`|gt$:Rs-.">
                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                      <block type="string_value" id="1km2@2_72#p[@2qd[QRv|I">
                                                                                                                                                                                                        <field name="value">&lt;u&gt;Step 1: From the question, identify the starting point and ending point of the journey.&lt;/u&gt;&lt;/br&gt;
&lt;/br&gt;
"How long does it take to go from ${m[an1]} ${p[an1]} to ${m[an2]} ${p[an2]}?"&lt;/br&gt;&lt;/br&gt;

Starting point = &lt;b style='color: green'&gt;${m[an1]} ${p[an1]}&lt;/b&gt;&lt;/br&gt;

Ending point = &lt;b style='color: orange'&gt;${m[an2]} ${p[an2]}&lt;/b&gt;
                                                                                                                                                                                                        </field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="end_partial_explanation" id="ftM7:~6+e;I0[]t%SkE!">
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="partial_explanation" id="+}2@2Pko}Gk!P1ECh{v2M" inline="true">
                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                              <block type="string_value" id="^R|q$%gAPku9J86j.@2NM">
                                                                                                                                                                                                                <field name="value">&lt;u&gt;Step 2: From the timetable, identify the departure time at ${m[an1]} ${p[an1]} and arrival time at ${m[an2]} ${p[an2]}.&lt;/u&gt;&lt;/br&gt;
&lt;/br&gt;
&lt;style&gt;
  table{background: rgb(204, 204, 0, 0.3)}
td{border: 1px solid orange; text-align: center; width: 300px}
&lt;/style&gt;
&lt;center&gt;
&lt;table&gt;
&lt;tr&gt;
&lt;td colspan='2'&gt;${t[0].toUpperCase()} TIMETABLE&lt;/td&gt;
&lt;/tr&gt;
                                                                                                                                                                                                                </field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                            <next>
                                                                                                                                                                                                              <block type="variable" id="z0!B-tJ+|Vqk6eF0|h5=">
                                                                                                                                                                                                                <field name="name">i</field>
                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                  <block type="expression" id="BA^2+Gd;1tMCF`@1h-qjz">
                                                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                  <block type="while_do_block" id="6!|7Ysd:/v@2t0c^G^EYa">
                                                                                                                                                                                                                    <value name="while">
                                                                                                                                                                                                                      <block type="expression" id="c}pC;NB]~M3nb?`m4d^V">
                                                                                                                                                                                                                        <field name="value">i &lt; 6</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                    <statement name="do">
                                                                                                                                                                                                                      <block type="if_then_else_block" id="j:ro@2n4)QLR5{@1K@29-83">
                                                                                                                                                                                                                        <value name="if">
                                                                                                                                                                                                                          <block type="expression" id="?T??bj}3L1S{5Bm%L=l(">
                                                                                                                                                                                                                            <field name="value">i == an1</field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                        <statement name="then">
                                                                                                                                                                                                                          <block type="partial_explanation" id="cV}_E7XfX!p6-f^dzJSV" inline="true">
                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                              <block type="string_value" id="X?F+GQI=1wsewyU]@1Y@2S">
                                                                                                                                                                                                                                <field name="value">&lt;tr&gt;
&lt;td&gt;&lt;b style='color: green'&gt;${m[i]} ${p[i]}&lt;/b&gt;&lt;/td&gt;&lt;td&gt;&lt;b style='color: green'&gt;${z[i]}&lt;/b&gt;&lt;/td&gt;
&lt;/tr&gt;
                                                                                                                                                                                                                                </field>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                        <statement name="else">
                                                                                                                                                                                                                          <block type="if_then_else_block" id="){QJc^,mKr@2W@2WloT#qs">
                                                                                                                                                                                                                            <value name="if">
                                                                                                                                                                                                                              <block type="expression" id="9|@2QN]i2[34$R@1sNLZD7">
                                                                                                                                                                                                                                <field name="value">i == an2</field>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                            <statement name="then">
                                                                                                                                                                                                                              <block type="partial_explanation" id="xx;@1nV`jL[GNsQ|X2Z/0" inline="true">
                                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                                  <block type="string_value" id="7u{K}x#IM}[R@1tGi]c:H">
                                                                                                                                                                                                                                    <field name="value">&lt;tr&gt;
&lt;td&gt;&lt;b style='color: orange'&gt;${m[i]} ${p[i]}&lt;/b&gt;&lt;/td&gt;&lt;td&gt;&lt;b style='color: orange'&gt;${z[i]}&lt;/b&gt;&lt;/td&gt;
&lt;/tr&gt;
                                                                                                                                                                                                                                    </field>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                                            <statement name="else">
                                                                                                                                                                                                                              <block type="partial_explanation" id="5z4dfkAXiB9l5lG1TKmW" inline="true">
                                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                                  <block type="string_value" id="{?$,3Ob2vbMpTKbrmm2@2">
                                                                                                                                                                                                                                    <field name="value">&lt;tr&gt;
&lt;td&gt;${m[i]} ${p[i]}&lt;/td&gt;&lt;td&gt;${z[i]}&lt;/td&gt;
&lt;/tr&gt;
                                                                                                                                                                                                                                    </field>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                          <block type="variable" id="iXXMjvF^I.LaI!#f_CRp">
                                                                                                                                                                                                                            <field name="name">i</field>
                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                              <block type="expression" id="zcNU4}r-#ejgm.;3D(sk">
                                                                                                                                                                                                                                <field name="value">i+1</field>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                      <block type="partial_explanation" id="5(b3h]}mO(;e0)?pejq#" inline="true">
                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                          <block type="string_value" id="~Zc`U8n^IKHi4QGNILn|">
                                                                                                                                                                                                                            <field name="value">&lt;/table&gt;
&lt;/center&gt;
                                                                                                                                                                                                                            </field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                          <block type="end_partial_explanation" id="aFI5.V+lP|!nB8~gTh46">
                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                              <block type="partial_explanation" id="EBoWyF:Zrt[Lu)LS:S(2">
                                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                                  <block type="string_value" id="q-/5]GpwF$Dl#@2Mri+x/">
                                                                                                                                                                                                                                    <field name="value">&lt;u&gt;Step 3: Finally, calculate the difference between the times. &lt;/u&gt;

&lt;style&gt;
table{width: 900px}
td{ text-align: center}
&lt;/style&gt;

&lt;center&gt;
&lt;table&gt;
&lt;tr&gt;&lt;td colspan='5'&gt;T - CHART&lt;/td&gt;&lt;/tr&gt;

&lt;tr&gt;
&lt;td rowspan='2' style='background-color: #41fc66'&gt;Start&lt;/br&gt;${z[an1]}&lt;/td&gt;
&lt;td style='border-bottom: 3px solid red; border-right: 3px solid red;'&gt;&lt;/td&gt;&lt;td style='border-bottom: 3px solid red'&gt;&lt;/td&gt;
&lt;td rowspan='2' style='background-color: #fcc040'&gt;End&lt;/br&gt;${z[an2]}&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td style='border-right: 3px solid red;'&gt;&lt;/td&gt;&lt;td&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;&lt;/td&gt;
                                                                                                                                                                                                                                    </field>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                  <block type="partial_explanation" id="nA/W@1$jMCq#bB_06@1KFZ">
                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                      <block type="string_value" id="=r7ugO9!@2n9wk61];jqo">
                                                                                                                                                                                                                                        <field name="value">&lt;td style='border-right: 3px solid red; vertical-align: top'&gt;</field>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                      <block type="partial_explanation" id="(OVNm@2R%BngJqeCCC1pt" inline="true">
                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                          <block type="string_value" id="olT`7{G.zo^wba@1aK:UZ">
                                                                                                                                                                                                                                            <field name="value">&lt;span style='background-color: #41fc66'&gt;${z[an1]}&lt;/span&gt;&lt;/br&gt;</field>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                          <block type="if_then_block" id="pt5]3Z9@1@23BByN.?/Nwq">
                                                                                                                                                                                                                                            <value name="if">
                                                                                                                                                                                                                                              <block type="expression" id="qZ~%m=(lT)[DAWOU[tMr">
                                                                                                                                                                                                                                                <field name="value">60 - (time[an1] % 60) &gt; 0 &amp;&amp; 60 - (time[an1] % 60) &lt; a</field>
                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                            <statement name="then">
                                                                                                                                                                                                                                              <block type="partial_explanation" id="sLq]s=z4ViBKe[Cg/N)?" inline="true">
                                                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                                                  <block type="string_value" id="?Kc:27/WnXF9#+U@2t}_]">
                                                                                                                                                                                                                                                    <field name="value">${two(Math.floor(x[an1]/60)+1) + ' : ' + '00' }&lt;/br&gt;</field>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                              <block type="if_then_block" id="!tD,l{@1[K}XbRkP/`nUF">
                                                                                                                                                                                                                                                <value name="if">
                                                                                                                                                                                                                                                  <block type="expression" id="5Z~}NIdw.3QCGGu=zoy7">
                                                                                                                                                                                                                                                    <field name="value">Math.floor((a- 60 + (time[an1] % 60))/60) &gt; 0</field>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                <statement name="then">
                                                                                                                                                                                                                                                  <block type="partial_explanation" id="9nq._@1s;@1ueUMAaeCPr!" inline="true">
                                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                                      <block type="string_value" id="GZ0dX.2XPS7sz:]@2.XkL">
                                                                                                                                                                                                                                                        <field name="value">${two(Math.floor(x[an2]/60)) + ' : ' + '00' }&lt;/br&gt;</field>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                                  <block type="partial_explanation" id="w^NpwLO+V~R8H2r_@1F/$" inline="true">
                                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                                      <block type="string_value" id="tDtQ.rxF#?W#,4kN8~g1">
                                                                                                                                                                                                                                                        <field name="value">&lt;span style='background-color: #fcc040'&gt;${z[an2]}&lt;/span&gt;&lt;/br&gt;</field>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                                      <block type="partial_explanation" id=".v})c`0B+kXO48U/r_9g">
                                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                                          <block type="string_value" id="5nDoz/%iog8D1nGw#oD@2">
                                                                                                                                                                                                                                                            <field name="value">&lt;/td&gt;&lt;td style='background: rgb(150, 200, 250, 0.3); vertical-align: top;'&gt;&lt;span&gt;</field>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                                          <block type="if_then_else_block" id="vuOR2g%,|p$A5uPHKsge">
                                                                                                                                                                                                                                                            <value name="if">
                                                                                                                                                                                                                                                              <block type="expression" id="5Z,7-U|Igf`Xi05p+T{9">
                                                                                                                                                                                                                                                                <field name="value">60 - (time[an1] % 60) &gt; a</field>
                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                            <statement name="then">
                                                                                                                                                                                                                                                              <block type="partial_explanation" id="Im3i`5#WX/XI4]csS|0q">
                                                                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                                                                  <block type="string_value" id=":l2O=!sSG9()mI]pBeq(">
                                                                                                                                                                                                                                                                    <field name="value">${a} minute${a&lt;2?'':'s'} &lt;/br&gt;</field>
                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                                                                            <statement name="else">
                                                                                                                                                                                                                                                              <block type="if_then_block" id="1T)ET74RuWsrH.m.h2XH">
                                                                                                                                                                                                                                                                <value name="if">
                                                                                                                                                                                                                                                                  <block type="expression" id="JLD!G.8eDn@2dWo=9~,iF">
                                                                                                                                                                                                                                                                    <field name="value">60 - (time[an1] % 60) &gt; 0</field>
                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                                <statement name="then">
                                                                                                                                                                                                                                                                  <block type="partial_explanation" id="uX9s;+S{+Y9c}C0AYpaD">
                                                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                                                      <block type="string_value" id="f3r@1!75l,pW-uF{uAdjM">
                                                                                                                                                                                                                                                                        <field name="value">${60 - (time[an1] % 60)} minute${(60 - (time[an1] % 60))&lt;2?'':'s'} &lt;/br&gt;</field>
                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                                              <block type="if_then_block" id="PxSCZN#psBgE?4x%VF8-">
                                                                                                                                                                                                                                                                <value name="if">
                                                                                                                                                                                                                                                                  <block type="expression" id="U5,y#C/|[Olylf#33He`">
                                                                                                                                                                                                                                                                    <field name="value">Math.floor((a- 60 + (time[an1] % 60))/60) &gt; 0</field>
                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                                <statement name="then">
                                                                                                                                                                                                                                                                  <block type="partial_explanation" id="O1lbRM--DXtl0TYRIvgo">
                                                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                                                      <block type="string_value" id="~lHM1h4?SN8wPnm)fa6_">
                                                                                                                                                                                                                                                                        <field name="value">${Math.floor((a- 60 + (time[an1] % 60))/60)} hour${Math.floor((a- 60 + (time[an1] % 60))/60)&lt;2?'':'s'} &lt;/br&gt;</field>
                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                                                  <block type="if_then_block" id="?zO:eOD9}u.AeCm8QuZ6">
                                                                                                                                                                                                                                                                    <value name="if">
                                                                                                                                                                                                                                                                      <block type="expression" id="^~Z@2#X7hk9B6O_2!9L2s">
                                                                                                                                                                                                                                                                        <field name="value">((a- 60 + (time[an1] % 60)) % 60) &gt; 0</field>
                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                                    <statement name="then">
                                                                                                                                                                                                                                                                      <block type="partial_explanation" id="(-BqQUR)9@2W~8Byr~#rT">
                                                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                                                          <block type="string_value" id="+GJR^tA0W8%`XCfr}c8[">
                                                                                                                                                                                                                                                                            <field name="value">${(a- 60 + (time[an1] % 60)) % 60} minute${((a- 60 + (time[an1] % 60)) % 60)&lt;2?'':'s'} &lt;/br&gt;</field>
                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                                                      <block type="partial_explanation" id="4?q+8if~3+8BK`u%L;sq">
                                                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                                                          <block type="string_value" id="g8z$W:]HCUsEzPa5chiz">
                                                                                                                                                                                                                                                                            <field name="value">&lt;/br&gt;
Total time taken:&lt;/br&gt;
                                                                                                                                                                                                                                                                            </field>
                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                                                          <block type="if_then_else_block" id="s_I(W)_}t?WW0fuz}0YR">
                                                                                                                                                                                                                                                                            <value name="if">
                                                                                                                                                                                                                                                                              <block type="expression" id="+MkK+~h$dDp:1`/jb.85">
                                                                                                                                                                                                                                                                                <field name="value">60 - (time[an1] % 60) &gt; a</field>
                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                                            <statement name="then">
                                                                                                                                                                                                                                                                              <block type="partial_explanation" id="u_SCaJuFZnz󋽂u67">
                                                                                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                                                                                  <block type="string_value" id="?Zl@2b{/#^8V{E7=|uhlv">
                                                                                                                                                                                                                                                                                    <field name="value">${a} minutes</field>
                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                                                                                            <statement name="else">
                                                                                                                                                                                                                                                                              <block type="if_then_block" id="Hb1fMMUfg[XZ_:v;ClZk">
                                                                                                                                                                                                                                                                                <value name="if">
                                                                                                                                                                                                                                                                                  <block type="expression" id="3Gap_U(mSq6Gt?aj1@2Nn">
                                                                                                                                                                                                                                                                                    <field name="value">60 - (time[an1] % 60) &gt; 0</field>
                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                                                <statement name="then">
                                                                                                                                                                                                                                                                                  <block type="partial_explanation" id="kFsrHq79AOXqKtP-d:?=">
                                                                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                                                                      <block type="string_value" id="7jUrO5gul~-tGtB25~4f">
                                                                                                                                                                                                                                                                                        <field name="value">${60 - (time[an1] % 60)} minute${(60 - (time[an1] % 60))&lt;2?'':'s'}</field>
                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                                                              <block type="if_then_block" id="h/yXpdJg$aw6~)xypjmN">
                                                                                                                                                                                                                                                                                <value name="if">
                                                                                                                                                                                                                                                                                  <block type="expression" id="mCe7bP$vlf=Drmdh-(g1">
                                                                                                                                                                                                                                                                                    <field name="value">Math.floor((a- 60 + (time[an1] % 60))/60) &gt; 0</field>
                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                                                <statement name="then">
                                                                                                                                                                                                                                                                                  <block type="if_then_block" id="z/BsrzI7@18-6/AGFuOob">
                                                                                                                                                                                                                                                                                    <value name="if">
                                                                                                                                                                                                                                                                                      <block type="expression" id="kJ^Em@2]+a8bj6ZI-c_({">
                                                                                                                                                                                                                                                                                        <field name="value">60 - (time[an1] % 60) &gt; 0</field>
                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                                                    <statement name="then">
                                                                                                                                                                                                                                                                                      <block type="partial_explanation" id="GJWTk~x|@1}Y}iYypRr!i">
                                                                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                                                                          <block type="expression" id="(M3`|xoNw2%C(X6@28+fd">
                                                                                                                                                                                                                                                                                            <field name="value">' + '</field>
                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                                                                      <block type="partial_explanation" id="`lWjAma@2nQ_0C,N[olDb">
                                                                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                                                                          <block type="string_value" id="[o7Z^N+nkQ0XjVr!-C=+">
                                                                                                                                                                                                                                                                                            <field name="value">${Math.floor((a- 60 + (time[an1] % 60))/60)} hour${Math.floor((a- 60 + (time[an1] % 60))/60)&lt;2?'':'s'}</field>
                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                                                                  <block type="if_then_block" id="1,:To5DXBQx_[#tcoEe|">
                                                                                                                                                                                                                                                                                    <value name="if">
                                                                                                                                                                                                                                                                                      <block type="expression" id="k^-{B_xQ%)))F.5pr%|I">
                                                                                                                                                                                                                                                                                        <field name="value">(a- 60 + (time[an1] % 60) % 60) &gt; 0</field>
                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                                                    <statement name="then">
                                                                                                                                                                                                                                                                                      <block type="if_then_block" id="h@2J;Sck)@2yY+|h50Ab#[">
                                                                                                                                                                                                                                                                                        <value name="if">
                                                                                                                                                                                                                                                                                          <block type="expression" id="n4]@2IGbUyvRmdOLfiR-R">
                                                                                                                                                                                                                                                                                            <field name="value">60 - (time[an1] % 60) &gt; 0  || Math.floor((a- 60 + (time[an1] % 60))/60) &gt; 0</field>
                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                                                        <statement name="then">
                                                                                                                                                                                                                                                                                          <block type="partial_explanation" id="/RQHY|VAgya=x9@1?)$H@2">
                                                                                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                                                                                              <block type="expression" id="Z@2{9t`Gj[PfDXHuGteXq">
                                                                                                                                                                                                                                                                                                <field name="value">' + '</field>
                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                                                                          <block type="partial_explanation" id="72d@2V6AquRWA_U.f@19$d">
                                                                                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                                                                                              <block type="string_value" id=";QnkZVcE;fKz9O01IUH{">
                                                                                                                                                                                                                                                                                                <field name="value">${(a- 60 + (time[an1] % 60)) % 60} minute${((a- 60 + (time[an1] % 60)) % 60)&lt;2?'':'s'}</field>
                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                                                                      <block type="partial_explanation" id="3quKVheC]F@2T}gU!PzKe">
                                                                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                                                                          <block type="expression" id="w#8d6Uel4f#SorkkqSaN">
                                                                                                                                                                                                                                                                                            <field name="value">' = '</field>
                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                                                                          <block type="partial_explanation" id="{,K(h0MtUm(hqbWf_]0Q">
                                                                                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                                                                                              <block type="string_value" id="j)agBtK.BLDT9a_P[R7)">
                                                                                                                                                                                                                                                                                                <field name="value">${Math.floor(a/60)} ${hour} ${a % 60} ${minute}&lt;/span&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;
&lt;/br&gt;
&lt;span style='background: rgb(250, 250, 250, 0.3)'&gt;
It takes &lt;b&gt;${Math.floor(a/60)}&lt;/b&gt; ${hour} &lt;b&gt;${a % 60}&lt;/b&gt; ${minute} to travel from &lt;b&gt;${m[an1]} ${p[an1]}&lt;/b&gt; to &lt;b&gt;${m[an2]} ${p[an2]}&lt;/b&gt;. 
&lt;/span&gt;&lt;/center&gt;
                                                                                                                                                                                                                                                                                                </field>
                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                                                                              <block type="end_partial_explanation" id="|iC7uw^~-BvO)E_`T9fz">
                                                                                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                                                                                  <block type="variable" id="rwi[:ELT--Kej(%2KW5u">
                                                                                                                                                                                                                                                                                                    <field name="name">border</field>
                                                                                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                                                                                      <block type="expression" id="L+pl5:}HBT;zp[]%^#3|">
                                                                                                                                                                                                                                                                                                        <field name="value">[]</field>
                                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                                                                                      <block type="variable" id="pfenHa0O~N8es]%cw`rw">
                                                                                                                                                                                                                                                                                                        <field name="name">bobo</field>
                                                                                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                                                                                          <block type="expression" id="[dS0%TO.?4U8!}2/Np%@2">
                                                                                                                                                                                                                                                                                                            <field name="value">[]</field>
                                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                                                                                          <block type="variable" id="~/r?j4N#J.K%Dk`YsyQM">
                                                                                                                                                                                                                                                                                                            <field name="name">i</field>
                                                                                                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                                                                                                              <block type="expression" id="kQ|/C6^)u=gA?aD;9OBG">
                                                                                                                                                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                                                                                              <block type="while_do_block" id="2@2oKa62YdRx~h,_K]oKi">
                                                                                                                                                                                                                                                                                                                <value name="while">
                                                                                                                                                                                                                                                                                                                  <block type="expression" id=")n@2!BeDLR)7K}.7g?0[G">
                                                                                                                                                                                                                                                                                                                    <field name="value">i &lt; 12</field>
                                                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                                                                                <statement name="do">
                                                                                                                                                                                                                                                                                                                  <block type="if_then_else_block" id="RlEZ+1!h})8A-)I+T:=!">
                                                                                                                                                                                                                                                                                                                    <value name="if">
                                                                                                                                                                                                                                                                                                                      <block type="expression" id="@1-pu5|QROH./isWV!B4v">
                                                                                                                                                                                                                                                                                                                        <field name="value">an2 == i</field>
                                                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                                                                                    <statement name="then">
                                                                                                                                                                                                                                                                                                                      <block type="variable" id="B;`QR}gRI`xFuVH}8@1G;">
                                                                                                                                                                                                                                                                                                                        <field name="name">border[i]</field>
                                                                                                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                                                                                                          <block type="expression" id="0SapRR=rv3gUsusl$G8?">
                                                                                                                                                                                                                                                                                                                            <field name="value">'3px solid blue'</field>
                                                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                                                                                                          <block type="variable" id="(_QVcIzCoWO@2617yI)6`">
                                                                                                                                                                                                                                                                                                                            <field name="name">bobo[i]</field>
                                                                                                                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                                                                                                                              <block type="expression" id="+~yUGAkPZ+Ws[~Yl^?(k">
                                                                                                                                                                                                                                                                                                                                <field name="value">'3px solid red'</field>
                                                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                                                                                                                                    <statement name="else">
                                                                                                                                                                                                                                                                                                                      <block type="if_then_else_block" id="4Oa@1j{L/8oHT?KFXJA$X">
                                                                                                                                                                                                                                                                                                                        <value name="if">
                                                                                                                                                                                                                                                                                                                          <block type="expression" id="?-R@1O@24c2L1Hi,JJHYhk">
                                                                                                                                                                                                                                                                                                                            <field name="value">an2 == array[i]</field>
                                                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                                                                                        <statement name="then">
                                                                                                                                                                                                                                                                                                                          <block type="variable" id="T{?9n-s@1cHJ.o84JaBG-">
                                                                                                                                                                                                                                                                                                                            <field name="name">border[array[i]]</field>
                                                                                                                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                                                                                                                              <block type="expression" id="t,j1)Gs=Ne;%zOw0$[B{">
                                                                                                                                                                                                                                                                                                                                <field name="value">'3px solid blue'</field>
                                                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                                                                                                              <block type="variable" id="+({LpBA%K$j{owuPc8_7">
                                                                                                                                                                                                                                                                                                                                <field name="name">bobo[array[i]]</field>
                                                                                                                                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                                                                                                                                  <block type="expression" id="iIx:|cim]4DUN0c%QzGn">
                                                                                                                                                                                                                                                                                                                                    <field name="value">'3px solid red'</field>
                                                                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                                                                                                                        <statement name="else">
                                                                                                                                                                                                                                                                                                                          <block type="variable" id="K[?lue=AXpx+sd:FVH##">
                                                                                                                                                                                                                                                                                                                            <field name="name">border[i]</field>
                                                                                                                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                                                                                                                              <block type="expression" id="i/L2nP:^.(88d|c`DFmm">
                                                                                                                                                                                                                                                                                                                                <field name="value">''</field>
                                                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                                                                                                              <block type="variable" id="#kuDv#@1R,gmKzumR9lpx">
                                                                                                                                                                                                                                                                                                                                <field name="name">bobo[i]</field>
                                                                                                                                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                                                                                                                                  <block type="expression" id="zewQ7H:m_{Xv%q/WM9]+">
                                                                                                                                                                                                                                                                                                                                    <field name="value">''</field>
                                                                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                                                                                                      <block type="variable" id="7ag|beA{sMzuD%Jsfyf5">
                                                                                                                                                                                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                                                                                                          <block type="expression" id="D2O(+VyOyo%N938|v]3.">
                                                                                                                                                                                                                                                                                                                            <field name="value">i+1</field>
                                                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                                                                                                  <block type="variable" id="_;LDBZxJ%)uvbT+XL1A$">
                                                                                                                                                                                                                                                                                                                    <field name="name">min</field>
                                                                                                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                                                                                                      <block type="expression" id="-rdjI0Iaa/X})~54mHSS">
                                                                                                                                                                                                                                                                                                                        <field name="value">y[out_min(an2, array)] - x[an2]</field>
                                                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                                                                                                      <block type="if_then_else_block" id="a`_@27clk`#@1H[k$KA~nY">
                                                                                                                                                                                                                                                                                                                        <value name="if">
                                                                                                                                                                                                                                                                                                                          <block type="expression" id="Xyr~PUXl_Rxd=6zEdr@1H">
                                                                                                                                                                                                                                                                                                                            <field name="value">Math.floor(b/60) &lt;2</field>
                                                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                                                                                        <statement name="then">
                                                                                                                                                                                                                                                                                                                          <block type="variable" id="w]A~fw9EZK8MN`gWH59q">
                                                                                                                                                                                                                                                                                                                            <field name="name">hour</field>
                                                                                                                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                                                                                                                              <block type="expression" id="FP%#tW#}z8]D,x5Ohd+e">
                                                                                                                                                                                                                                                                                                                                <field name="value">'hour'</field>
                                                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                                                                                                                        <statement name="else">
                                                                                                                                                                                                                                                                                                                          <block type="variable" id="$YVk@28KU+T3=Q0QvhYx?">
                                                                                                                                                                                                                                                                                                                            <field name="name">hour</field>
                                                                                                                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                                                                                                                              <block type="expression" id="G5U/sHp?ljdiXK.8wMj}">
                                                                                                                                                                                                                                                                                                                                <field name="value">'hours'</field>
                                                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                                                                                                          <block type="if_then_else_block" id="+I)5}]J1-9Ls{6beL%NP">
                                                                                                                                                                                                                                                                                                                            <value name="if">
                                                                                                                                                                                                                                                                                                                              <block type="expression" id="ps!sJP}Be^}#zG{{Bd59">
                                                                                                                                                                                                                                                                                                                                <field name="value">b % 60 &lt;2</field>
                                                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                                                                                            <statement name="then">
                                                                                                                                                                                                                                                                                                                              <block type="variable" id="6yr@1I${p!=fEy!EfJQEy">
                                                                                                                                                                                                                                                                                                                                <field name="name">minute</field>
                                                                                                                                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                                                                                                                                  <block type="expression" id="d:@1t;pqTwvDPdK)1c@29x">
                                                                                                                                                                                                                                                                                                                                    <field name="value">'minute'</field>
                                                                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                                                                                                                                            <statement name="else">
                                                                                                                                                                                                                                                                                                                              <block type="variable" id="Azi#f+LPv`Z%|@20qIXy%">
                                                                                                                                                                                                                                                                                                                                <field name="name">minute</field>
                                                                                                                                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                                                                                                                                  <block type="expression" id=":_g]UVr5X|kC3(;fC%XX">
                                                                                                                                                                                                                                                                                                                                    <field name="value">'minutes'</field>
                                                                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                                                                                                              <block type="partial_explanation" id="J@2}XiSe}pCy`:|K|vVtP">
                                                                                                                                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                                                                                                                                  <block type="string_value" id="pE=i5p+)@1]z!shUKZu:?">
                                                                                                                                                                                                                                                                                                                                    <field name="value">&lt;u&gt;Step 4: Now, find out the wait time.&lt;/u&gt;&lt;/br&gt;&lt;/br&gt;</field>
                                                                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                                                                                                                  <block type="partial_explanation" id="?3NA|$[lKuSB%?wmc,kC">
                                                                                                                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                                                                                                                      <block type="string_value" id="3A4ZO$`DbO`vo#~T].E4">
                                                                                                                                                                                                                                                                                                                                        <field name="value">&lt;style&gt;
  table{background: rgb(204, 204, 0, 0.3)}
td{border: 1px solid orange; text-align: center; width: 300px; color: black}
&lt;/style&gt;
&lt;center&gt;
&lt;table&gt;
&lt;tr&gt;
&lt;td colspan='2'&gt;${t[0].toUpperCase()} TIMETABLE&lt;/td&gt;&lt;td colspan='2'&gt;${t[1].toUpperCase()} TIMETABLE&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;&lt;span style='border: ${border[0]}; border-radius: 10px;'&gt;${m[0]} ${p[0]}&lt;/span&gt;&lt;/td&gt;&lt;td&gt;&lt;span style='border: ${bobo[0]}; border-radius: 10px;'&gt;${z[0]}&lt;/span&gt;&lt;/td&gt;&lt;td&gt;&lt;span style='border: ${border[array[0]]}; border-radius: 10px;'&gt;${m[array[0]]} ${p[array[0]]}&lt;/span&gt;&lt;/td&gt;&lt;td&gt;&lt;span style='border: ${bobo[array[0]]}; border-radius: 10px;'&gt;${w[0]}&lt;/span&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;&lt;span style='border: ${border[1]}; border-radius: 10px;'&gt;${m[1]} ${p[1]}&lt;/span&gt;&lt;/td&gt;&lt;td&gt;&lt;span style='border: ${bobo[1]}; border-radius: 10px;'&gt;${z[1]}&lt;/span&gt;&lt;/td&gt;&lt;td&gt;&lt;span style='border: ${border[array[1]]}; border-radius: 10px;'&gt;${m[array[1]]} ${p[array[1]]}&lt;/span&gt;&lt;/td&gt;&lt;td&gt;&lt;span style='border: ${bobo[array[1]]}; border-radius: 10px;'&gt;${w[1]}&lt;/span&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;&lt;span style='border: ${border[2]}; border-radius: 10px;'&gt;${m[2]} ${p[2]}&lt;/span&gt;&lt;/td&gt;&lt;td&gt;&lt;span style='border: ${bobo[2]}; border-radius: 10px;'&gt;${z[2]}&lt;/span&gt;&lt;/td&gt;&lt;td&gt;&lt;span style='border: ${border[array[2]]}; border-radius: 10px;'&gt;${m[array[2]]} ${p[array[2]]}&lt;/span&gt;&lt;/td&gt;&lt;td&gt;&lt;span style='border: ${bobo[array[2]]}; border-radius: 10px;'&gt;${w[2]}&lt;/span&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;&lt;span style='border: ${border[3]}; border-radius: 10px;'&gt;${m[3]} ${p[3]}&lt;/span&gt;&lt;/td&gt;&lt;td&gt;&lt;span style='border: ${bobo[3]}; border-radius: 10px;'&gt;${z[3]}&lt;/span&gt;&lt;/td&gt;&lt;td&gt;&lt;span style='border: ${border[array[3]]}; border-radius: 10px;'&gt;${m[array[3]]} ${p[array[3]]}&lt;/span&gt;&lt;/td&gt;&lt;td&gt;&lt;span style='border: ${bobo[array[3]]}; border-radius: 10px;'&gt;${w[3]}&lt;/span&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;&lt;span style='border: ${border[4]}; border-radius: 10px;'&gt;${m[4]} ${p[4]}&lt;/span&gt;&lt;/td&gt;&lt;td&gt;&lt;span style='border: ${bobo[4]}; border-radius: 10px;'&gt;${z[4]}&lt;/span&gt;&lt;/td&gt;&lt;td&gt;&lt;span style='border: ${border[array[4]]}; border-radius: 10px;'&gt;${m[array[4]]} ${p[array[4]]}&lt;/span&gt;&lt;/td&gt;&lt;td&gt;&lt;span style='border: ${bobo[array[4]]}; border-radius: 10px;'&gt;${w[4]}&lt;/span&gt;&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;&lt;span style='border: ${border[5]}; border-radius: 10px;'&gt;${m[5]} ${p[5]}&lt;/span&gt;&lt;/td&gt;&lt;td&gt;&lt;span style='border: ${bobo[5]}; border-radius: 10px;'&gt;${z[5]}&lt;/span&gt;&lt;/td&gt;&lt;td&gt;&lt;span style='border: ${border[array[5]]}; border-radius: 10px;'&gt;${m[array[5]]} ${p[array[5]]}&lt;/span&gt;&lt;/td&gt;&lt;td&gt;&lt;span style='border: ${bobo[array[5]]}; border-radius: 10px;'&gt;${w[5]}&lt;/span&gt;&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;

&lt;/br&gt;
&lt;span style='background: rgb(250, 250, 250, 0.4)'&gt;
It will take &lt;b&gt;${min + (min &gt; 2 ? ' minutes': 'minute')}&lt;/b&gt; to wait for the bus&lt;/br&gt;
Totally, it takes &lt;b&gt;${Math.floor(b/60)}&lt;/b&gt; ${hour} and &lt;b&gt;${b % 60}&lt;/b&gt; ${minute}.&lt;/span&gt;
&lt;/center&gt;
                                                                                                                                                                                                                                                                                                                                        </field>
                                                                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </next>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </next>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
  <block type="partial_explanation" id="XF:jqd`uW[[)WB#{.owz" inline="true" x="777" y="8939">
    <value name="value">
      <block type="string_value" id=",PoW1@19J[+!K8Z7)^{;5">
        <field name="value">&lt;u&gt;Step 2: From the timetable, Identify the departure time at ${m[an1]} ${p[an1]} and arrival time at ${m[an2]} ${p[an2]}.&lt;/u&gt;&lt;/br&gt;
&lt;/br&gt;
&lt;style&gt;
  table{background: rgb(204, 204, 0, 0.3)}
td{border: 1px solid orange; text-align: center; width: 300px}
&lt;/style&gt;
&lt;center&gt;
&lt;table&gt;
&lt;tr&gt;
&lt;td colspan='2'&gt;${t[0].toUpperCase()} TIMETABLE&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;${m[0]} ${p[0]}&lt;/td&gt;&lt;td&gt;${z[0]}&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;${m[1]} ${p[1]}&lt;/td&gt;&lt;td&gt;${z[1]}&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;${m[2]} ${p[2]}&lt;/td&gt;&lt;td&gt;${z[2]}&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;${m[3]} ${p[3]}&lt;/td&gt;&lt;td&gt;${z[3]}&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;${m[4]} ${p[4]}&lt;/td&gt;&lt;td&gt;${z[4]}&lt;/td&gt;
&lt;/tr&gt;

&lt;tr&gt;
&lt;td&gt;${m[5]} ${p[5]}&lt;/td&gt;&lt;td&gt;${z[5]}&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;/center&gt;

&lt;/br&gt;
${m[an1]} ${p[an1]} = ${z[an1]}&lt;/br&gt;


${m[an2]} ${p[an2]} = ${z[an2]}
        </field>
      </block>
    </value>
  </block>
  <block type="partial_explanation" id="8iq}wg/BCpnKc?Yg%`qW" inline="true" x="643" y="9047">
    <value name="value">
      <block type="string_value" id="S:To7%^#h,Cgg6PDf2Bf">
        <field name="value">&lt;/br&gt;
${m[an1]} ${p[an1]} = ${z[an1]}&lt;/br&gt;


${m[an2]} ${p[an2]} = ${z[an2]}
        </field>
      </block>
    </value>
  </block>
  <block type="if_then_else_block" id="2QzCbkn|V6otHL`=-b33" x="717" y="9811">
    <value name="if">
      <block type="expression" id="HdObkB}]1@2Hj}LkV@1bk!">
        <field name="value">60 - (time[an1] % 60) &gt; a</field>
      </block>
    </value>
    <statement name="then">
      <block type="partial_explanation" id="vB{+,DeR3|4/Zoi6DscD" inline="true">
        <value name="value">
          <block type="string_value" id="^NxnXg%d?oeKL8y_GFWV">
            <field name="value">${two(Math.floor((x[an1]+a)/60)) + ' : ' + two((x[an1] + a) % 60)} &lt;/br&gt;</field>
          </block>
        </value>
      </block>
    </statement>
  </block>
  <block type="while_do_block" id="b=xEnFs,OVZxNB7W(ZFv" x="858" y="9958">
    <value name="while">
      <block type="expression" id="ev@1l}e.k!89Bw|:9ye#z">
        <field name="value">i &lt; 6</field>
      </block>
    </value>
    <statement name="do">
      <block type="variable" id="|9y),6f{U6.Uq2zeJ`HF">
        <field name="name">i</field>
        <value name="value">
          <block type="expression" id="FFfzWkrs:92Vqe)#(rCa">
            <field name="value">0</field>
          </block>
        </value>
        <next>
          <block type="if_then_else_block" id=",^rm^_|KxXZIC}N6BstY">
            <value name="if">
              <block type="expression" id="emXR@1;|OZh@14eARa`#(A">
                <field name="value">z[i] === z[an1]</field>
              </block>
            </value>
            <statement name="else">
              <block type="if_then_else_block" id="fk(%-mn!Vaɴc(zTuR">
                <value name="if">
                  <block type="expression" id="Y}U:Up@2+s`02h0/}GZLl">
                    <field name="value">z[i] === z[an2]</field>
                  </block>
                </value>
                <statement name="else">
                  <block type="partial_explanation" id="sNzggb/;_34F,V6-f(xc" inline="true">
                    <value name="value">
                      <block type="string_value" id="a%ZK$=1)X?S.Zj4:=HC-">
                        <field name="value">&lt;span style='background-color: '&gt;${z[i]}&lt;/span&gt;&lt;/br&gt;</field>
                      </block>
                    </value>
                  </block>
                </statement>
              </block>
            </statement>
            <next>
              <block type="variable" id="QM8_RVmqO?Lnj:uM94$a">
                <field name="name">i</field>
                <value name="value">
                  <block type="expression" id="H=%=v!BxI;@1jt6hm,:(P">
                    <field name="value">i+1</field>
                  </block>
                </value>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
</xml>
END_XML]] */