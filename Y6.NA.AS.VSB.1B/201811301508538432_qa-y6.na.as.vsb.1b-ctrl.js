
module.exports = [
  {
    "#type": "question",
    "name": "Y6.NA.AS.VSB.1B",
    "formula": "equality",
    "contents": [
      {
        "#type": "variable",
        "name": "image_path",
        "value": "Develop/ImageQAs/Y6.NA.AS.VSB.1B/"
      },
      {
        "#type": "variable",
        "name": "background",
        "value": {
          "#type": "string_array",
          "items": "bg1|bg2|bg3|bg4|bg5|bg6"
        }
      },
      {
        "#type": "variable",
        "name": "drop_background",
        "value": {
          "#type": "random_one",
          "items": {
            "#type": "expression",
            "value": "background"
          }
        }
      },
      {
        "#type": "generic_shape",
        "#props": [
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "${drop_background}.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}${drop_background}.png"
          }
        ],
        "type": "background"
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfCorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addInputParam",
        "args": [
          "numberOfIncorrect",
          {
            "#type": "expression",
            "value": "0"
          }
        ]
      },
      {
        "#type": "variable",
        "name": "range",
        "value": {
          "#type": "expression",
          "value": "$params.numberOfCorrect - $params.numberOfIncorrect"
        }
      },
      {
        "#type": "if_then_else_block",
        "if": {
          "#type": "expression",
          "value": "range < 0"
        },
        "then": [
          {
            "#type": "variable",
            "name": "x",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "10000"
              },
              "max": {
                "#type": "expression",
                "value": "90000"
              }
            }
          },
          {
            "#type": "variable",
            "name": "y",
            "value": {
              "#type": "random_number",
              "min": {
                "#type": "expression",
                "value": "10000"
              },
              "max": {
                "#type": "expression",
                "value": "x"
              }
            }
          }
        ],
        "else": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "range < 4"
            },
            "then": [
              {
                "#type": "variable",
                "name": "x",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "99000"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "499000"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "y",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "99000"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "x"
                  }
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "x",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "500000"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "999999"
                  }
                }
              },
              {
                "#type": "variable",
                "name": "y",
                "value": {
                  "#type": "random_number",
                  "min": {
                    "#type": "expression",
                    "value": "500000"
                  },
                  "max": {
                    "#type": "expression",
                    "value": "x"
                  }
                }
              }
            ]
          }
        ]
      },
      {
        "#type": "variable",
        "name": "A",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "x1",
        "value": {
          "#type": "expression",
          "value": "Math.floor(x/100000)"
        }
      },
      {
        "#type": "variable",
        "name": "x2",
        "value": {
          "#type": "expression",
          "value": "Math.floor((x-x1*100000)/10000)"
        }
      },
      {
        "#type": "variable",
        "name": "x3",
        "value": {
          "#type": "expression",
          "value": "Math.floor((x-x1*100000-x2*10000)/1000)"
        }
      },
      {
        "#type": "variable",
        "name": "x4",
        "value": {
          "#type": "expression",
          "value": "Math.floor((x-x1*100000-x2*10000-x3*1000)/100)"
        }
      },
      {
        "#type": "variable",
        "name": "x5",
        "value": {
          "#type": "expression",
          "value": "Math.floor((x-x1*100000-x2*10000-x3*1000-x4*100)/10)"
        }
      },
      {
        "#type": "variable",
        "name": "x6",
        "value": {
          "#type": "expression",
          "value": "x-x1*100000-x2*10000-x3*1000-x4*100-x5*10"
        }
      },
      {
        "#type": "variable",
        "name": "A[0]",
        "value": {
          "#type": "expression",
          "value": "x6"
        }
      },
      {
        "#type": "variable",
        "name": "A[1]",
        "value": {
          "#type": "expression",
          "value": "x5"
        }
      },
      {
        "#type": "variable",
        "name": "A[2]",
        "value": {
          "#type": "expression",
          "value": "x4"
        }
      },
      {
        "#type": "variable",
        "name": "A[3]",
        "value": {
          "#type": "expression",
          "value": "x3"
        }
      },
      {
        "#type": "variable",
        "name": "A[4]",
        "value": {
          "#type": "expression",
          "value": "x2"
        }
      },
      {
        "#type": "variable",
        "name": "A[5]",
        "value": {
          "#type": "expression",
          "value": "x1"
        }
      },
      {
        "#type": "variable",
        "name": "B",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "y1",
        "value": {
          "#type": "expression",
          "value": "Math.floor(y/100000)"
        }
      },
      {
        "#type": "variable",
        "name": "y2",
        "value": {
          "#type": "expression",
          "value": "Math.floor((y-y1*100000)/10000)"
        }
      },
      {
        "#type": "variable",
        "name": "y3",
        "value": {
          "#type": "expression",
          "value": "Math.floor((y-y1*100000-y2*10000)/1000)"
        }
      },
      {
        "#type": "variable",
        "name": "y4",
        "value": {
          "#type": "expression",
          "value": "Math.floor((y-y1*100000-y2*10000-y3*1000)/100)"
        }
      },
      {
        "#type": "variable",
        "name": "y5",
        "value": {
          "#type": "expression",
          "value": "Math.floor((y-y1*100000-y2*10000-y3*1000-y4*100)/10)"
        }
      },
      {
        "#type": "variable",
        "name": "y6",
        "value": {
          "#type": "expression",
          "value": "y-y1*100000-y2*10000-y3*1000-y4*100-y5*10"
        }
      },
      {
        "#type": "variable",
        "name": "B[0]",
        "value": {
          "#type": "expression",
          "value": "y6"
        }
      },
      {
        "#type": "variable",
        "name": "B[1]",
        "value": {
          "#type": "expression",
          "value": "y5"
        }
      },
      {
        "#type": "variable",
        "name": "B[2]",
        "value": {
          "#type": "expression",
          "value": "y4"
        }
      },
      {
        "#type": "variable",
        "name": "B[3]",
        "value": {
          "#type": "expression",
          "value": "y3"
        }
      },
      {
        "#type": "variable",
        "name": "B[4]",
        "value": {
          "#type": "expression",
          "value": "y2"
        }
      },
      {
        "#type": "variable",
        "name": "B[5]",
        "value": {
          "#type": "expression",
          "value": "y1"
        }
      },
      {
        "#type": "variable",
        "name": "A[6]",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "variable",
        "name": "B[6]",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "variable",
        "name": "C",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "temp",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "temp[0]",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < 7"
        },
        "do": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "A[i] < B[i] + temp[i]"
            },
            "then": [
              {
                "#type": "variable",
                "name": "C[i]",
                "value": {
                  "#type": "expression",
                  "value": "10 + A[i] - B[i] - temp[i]"
                }
              },
              {
                "#type": "variable",
                "name": "temp[i+1]",
                "value": {
                  "#type": "expression",
                  "value": "1"
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "C[i]",
                "value": {
                  "#type": "expression",
                  "value": "A[i] - B[i] - temp[i]"
                }
              },
              {
                "#type": "variable",
                "name": "temp[i+1]",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i + 1"
            }
          }
        ]
      },
      {
        "#type": "variable",
        "name": "D",
        "value": {
          "#type": "expression",
          "value": "[]"
        }
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "0"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i < 7"
        },
        "do": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "temp[i] == 0"
            },
            "then": [
              {
                "#type": "variable",
                "name": "D[i]",
                "value": {
                  "#type": "expression",
                  "value": "-1"
                }
              }
            ],
            "else": [
              {
                "#type": "variable",
                "name": "D[i]",
                "value": {
                  "#type": "expression",
                  "value": "(A[i]+9) % 10"
                }
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i+1"
            }
          }
        ]
      },
      {
        "#type": "statement",
        "value": "temp.splice(0,1);"
      },
      {
        "#type": "text_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "resp_x",
              "value": "cx"
            },
            "y": {
              "#type": "expression",
              "value": "60"
            }
          },
          {
            "#type": "prop_text_contents",
            "#prop": "",
            "contents": "Complete the following subtraction:"
          },
          {
            "#prop": "",
            "style": {
              "#type": "json",
              "base": "text",
              "#props": [
                {
                  "#type": "prop_text_style_font_size",
                  "#prop": "",
                  "fontSize": {
                    "#type": "expression",
                    "value": "42"
                  }
                },
                {
                  "#type": "prop_text_style_fill",
                  "#prop": "",
                  "fill": "black"
                },
                {
                  "#type": "prop_text_style_stroke",
                  "#prop": "",
                  "stroke": "white"
                },
                {
                  "#type": "prop_text_style_stroke_thickness",
                  "#prop": "",
                  "strokeThickness": {
                    "#type": "expression",
                    "value": "2"
                  }
                }
              ]
            }
          }
        ]
      },
      {
        "#type": "grid_shape",
        "#props": [
          {
            "#type": "prop_grid_dimension",
            "#prop": "",
            "rows": {
              "#type": "expression",
              "value": "4"
            },
            "cols": {
              "#type": "expression",
              "value": "1"
            }
          },
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "resp_x",
              "value": "cx"
            },
            "y": {
              "#type": "expression",
              "value": "100"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "300"
            },
            "height": {
              "#type": "expression",
              "value": "280"
            }
          },
          {
            "#type": "prop_anchor",
            "#prop": "anchor",
            "x": {
              "#type": "expression",
              "value": "0.5"
            },
            "y": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_grid_cell_source",
            "#prop": "cell.source",
            "value": {
              "#type": "func",
              "name": "mix",
              "args": [
                {
                  "#type": "expression",
                  "value": "4"
                },
                {
                  "#type": "expression",
                  "value": "[1]"
                },
                {
                  "#callback": "$item",
                  "variable": "$item",
                  "body": [
                    {
                      "#type": "expression",
                      "value": "$item"
                    }
                  ]
                }
              ]
            }
          },
          {
            "#type": "prop_grid_random",
            "#prop": "",
            "random": false
          },
          {
            "#type": "prop_grid_show_borders",
            "#prop": "#showBorders",
            "value": false
          },
          {
            "#type": "prop_grid_cell_template_for",
            "variable": "$cell",
            "condition": "$cell.row == 0",
            "#prop": "cell.templates[]",
            "#callback": "$cell",
            "body": [
              {
                "#type": "list_shape",
                "#props": [
                  {
                    "#type": "prop_list_direction",
                    "#prop": "",
                    "dir": "horizontal"
                  },
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.right"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY"
                    }
                  },
                  {
                    "#type": "prop_spacing",
                    "#prop": "",
                    "spacing": {
                      "#type": "expression",
                      "value": "15"
                    }
                  },
                  {
                    "#type": "prop_anchor",
                    "#prop": "anchor",
                    "x": {
                      "#type": "expression",
                      "value": "1"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "0.5"
                    }
                  }
                ],
                "items": [
                  {
                    "#type": "json",
                    "#props": [
                      {
                        "#type": "prop_list_align",
                        "#prop": "",
                        "align": "middle"
                      },
                      {
                        "#type": "prop_list_item_source",
                        "#prop": "",
                        "source": {
                          "#type": "func",
                          "name": "charactersOf",
                          "args": [
                            {
                              "#type": "expression",
                              "value": "x"
                            }
                          ]
                        }
                      }
                    ],
                    "template": {
                      "#callback": "$item",
                      "variable": "$item",
                      "body": [
                        {
                          "#type": "image_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "0"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "0"
                              }
                            },
                            {
                              "#type": "prop_size",
                              "#prop": "",
                              "width": {
                                "#type": "expression",
                                "value": "0"
                              },
                              "height": {
                                "#type": "expression",
                                "value": "0"
                              }
                            },
                            {
                              "#type": "prop_image_key",
                              "#prop": "",
                              "key": "${$item.data}.png"
                            },
                            {
                              "#type": "prop_image_src",
                              "#prop": "",
                              "src": "${image_path}${$item.data}.png"
                            }
                          ]
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "prop_grid_cell_template_for",
            "variable": "$cell",
            "condition": "$cell.row == 1",
            "#prop": "cell.templates[]",
            "#callback": "$cell",
            "body": [
              {
                "#type": "list_shape",
                "#props": [
                  {
                    "#type": "prop_list_direction",
                    "#prop": "",
                    "dir": "horizontal"
                  },
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.right"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY"
                    }
                  },
                  {
                    "#type": "prop_spacing",
                    "#prop": "",
                    "spacing": {
                      "#type": "expression",
                      "value": "15"
                    }
                  },
                  {
                    "#type": "prop_anchor",
                    "#prop": "anchor",
                    "x": {
                      "#type": "expression",
                      "value": "1"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "0.5"
                    }
                  }
                ],
                "items": [
                  {
                    "#type": "json",
                    "#props": [
                      {
                        "#type": "prop_list_align",
                        "#prop": "",
                        "align": "middle"
                      },
                      {
                        "#type": "prop_list_item_source",
                        "#prop": "",
                        "source": {
                          "#type": "func",
                          "name": "charactersOf",
                          "args": [
                            {
                              "#type": "expression",
                              "value": "y"
                            }
                          ]
                        }
                      }
                    ],
                    "template": {
                      "#callback": "$item",
                      "variable": "$item",
                      "body": [
                        {
                          "#type": "image_shape",
                          "#props": [
                            {
                              "#type": "prop_position",
                              "#prop": "",
                              "x": {
                                "#type": "expression",
                                "value": "0"
                              },
                              "y": {
                                "#type": "expression",
                                "value": "0"
                              }
                            },
                            {
                              "#type": "prop_size",
                              "#prop": "",
                              "width": {
                                "#type": "expression",
                                "value": "0"
                              },
                              "height": {
                                "#type": "expression",
                                "value": "0"
                              }
                            },
                            {
                              "#type": "prop_image_key",
                              "#prop": "",
                              "key": "${$item.data}.png"
                            },
                            {
                              "#type": "prop_image_src",
                              "#prop": "",
                              "src": "${image_path}${$item.data}.png"
                            }
                          ]
                        }
                      ]
                    }
                  }
                ]
              }
            ]
          },
          {
            "#type": "prop_grid_cell_template_for",
            "variable": "$cell",
            "condition": "$cell.row == 2",
            "#prop": "cell.templates[]",
            "#callback": "$cell",
            "body": [
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "300"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "line.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}line.png"
                  }
                ]
              }
            ]
          },
          {
            "#type": "prop_grid_cell_template_for",
            "variable": "$cell",
            "condition": "$cell.row == 3",
            "#prop": "cell.templates[]",
            "#callback": "$cell",
            "body": [
              {
                "#type": "image_shape",
                "#props": [
                  {
                    "#type": "prop_position",
                    "#prop": "",
                    "x": {
                      "#type": "expression",
                      "value": "$cell.centerX + 20"
                    },
                    "y": {
                      "#type": "expression",
                      "value": "$cell.centerY"
                    }
                  },
                  {
                    "#type": "prop_size",
                    "#prop": "",
                    "width": {
                      "#type": "expression",
                      "value": "300"
                    },
                    "height": {
                      "#type": "expression",
                      "value": "0"
                    }
                  },
                  {
                    "#type": "prop_image_key",
                    "#prop": "",
                    "key": "shape.png"
                  },
                  {
                    "#type": "prop_image_src",
                    "#prop": "",
                    "src": "${image_path}shape.png"
                  }
                ]
              },
              {
                "#type": "variable",
                "name": "i",
                "value": {
                  "#type": "expression",
                  "value": "0"
                }
              },
              {
                "#type": "while_do_block",
                "while": {
                  "#type": "expression",
                  "value": "i < x.toString().length"
                },
                "do": [
                  {
                    "#type": "choice_input_shape",
                    "#props": [
                      {
                        "#type": "prop_value",
                        "#prop": "",
                        "value": {
                          "#type": "expression",
                          "value": "C[i]"
                        }
                      },
                      {
                        "#type": "prop_position",
                        "#prop": "",
                        "x": {
                          "#type": "expression",
                          "value": "$cell.right - 10 - i*45"
                        },
                        "y": {
                          "#type": "expression",
                          "value": "$cell.centerY"
                        }
                      },
                      {
                        "#type": "prop_size",
                        "#prop": "",
                        "width": {
                          "#type": "expression",
                          "value": "27"
                        },
                        "height": {
                          "#type": "expression",
                          "value": "50"
                        }
                      },
                      {
                        "#type": "prop_input_keyboard",
                        "#prop": "",
                        "keyboard": "numbers1"
                      },
                      {
                        "#type": "prop_input_max_length",
                        "#prop": "",
                        "maxLength": {
                          "#type": "expression",
                          "value": "1"
                        }
                      },
                      {
                        "#type": "prop_input_result_position",
                        "#prop": "",
                        "resultPosition": "bottom"
                      },
                      {
                        "#type": "prop_stroke",
                        "#prop": "stroke"
                      },
                      {
                        "#type": "prop_fill",
                        "#prop": "fill"
                      },
                      {
                        "#prop": "",
                        "style": {
                          "#type": "json",
                          "base": "remainder_input",
                          "#props": [
                            {
                              "#type": "prop_text_style_font_size",
                              "#prop": "",
                              "fontSize": {
                                "#type": "expression",
                                "value": "42"
                              }
                            },
                            {
                              "#type": "prop_text_style_fill",
                              "#prop": "",
                              "fill": "black"
                            },
                            {
                              "#type": "prop_text_style_stroke",
                              "#prop": "",
                              "stroke": "white"
                            },
                            {
                              "#type": "prop_text_style_stroke_thickness",
                              "#prop": "",
                              "strokeThickness": {
                                "#type": "expression",
                                "value": "2"
                              }
                            }
                          ]
                        }
                      }
                    ],
                    "#init": "algorithmic_remainder"
                  },
                  {
                    "#type": "variable",
                    "name": "i",
                    "value": {
                      "#type": "expression",
                      "value": "i+1"
                    }
                  }
                ]
              }
            ]
          }
        ]
      },
      {
        "#type": "image_shape",
        "#props": [
          {
            "#type": "prop_position",
            "#prop": "",
            "x": {
              "#type": "expression",
              "value": "260"
            },
            "y": {
              "#type": "expression",
              "value": "170"
            }
          },
          {
            "#type": "prop_size",
            "#prop": "",
            "width": {
              "#type": "expression",
              "value": "0"
            },
            "height": {
              "#type": "expression",
              "value": "0"
            }
          },
          {
            "#type": "prop_image_key",
            "#prop": "",
            "key": "sub.png"
          },
          {
            "#type": "prop_image_src",
            "#prop": "",
            "src": "${image_path}sub.png"
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<u>Let's use the below table to complete this subtraction problem.</u>\n\n<style>\ntable{}\ntd{text-align: center; width: 60px}\n</style>\n</br></br>\n\n<center>\n<table style='background: rgb(150, 200, 250, 0.2)'><tr>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "6"
        }
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<td>Borrowing</td>"
        ]
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i>=0"
        },
        "do": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "temp[i] != 0"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<td>${temp[i]}</td>"
                ]
              }
            ],
            "else": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<td></td>"
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i-1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</tr><tr>"
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<td></td>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "6"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i>=0"
        },
        "do": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "D[i] == -1"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<td></td>"
                ]
              }
            ],
            "else": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<td style='color: blue'>${D[i]}</td>"
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i-1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</tr><tr>"
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<td rowspan='2'>-</td>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "6"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i>=0"
        },
        "do": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "i == 6 || (i == 5 && A[i] == 0)"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<td></td>"
                ]
              }
            ],
            "else": [
              {
                "#type": "if_then_else_block",
                "if": {
                  "#type": "expression",
                  "value": "D[i] == -1"
                },
                "then": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<td>${A[i]}</td>"
                    ]
                  }
                ],
                "else": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<td><strike>${A[i]}</strike></td>"
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i-1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</tr><tr>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "6"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i>=0"
        },
        "do": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "i == 6 || (B[i] == 0 && i ==5)"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<td></td>"
                ]
              }
            ],
            "else": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<td>${B[i]}</td>"
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i-1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</tr><tr>"
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "<td>Answer:</td>"
        ]
      },
      {
        "#type": "variable",
        "name": "i",
        "value": {
          "#type": "expression",
          "value": "6"
        }
      },
      {
        "#type": "while_do_block",
        "while": {
          "#type": "expression",
          "value": "i>=0"
        },
        "do": [
          {
            "#type": "if_then_else_block",
            "if": {
              "#type": "expression",
              "value": "i == 6 && C[i] == 0"
            },
            "then": [
              {
                "#type": "func",
                "name": "addPartialExplanation",
                "args": [
                  "<td style='border-top: 2px solid black;'></td>"
                ]
              }
            ],
            "else": [
              {
                "#type": "if_then_else_block",
                "if": {
                  "#type": "expression",
                  "value": "C[6] == 0 && C[i] == 0 && i == 5"
                },
                "then": [
                  {
                    "#type": "func",
                    "name": "addPartialExplanation",
                    "args": [
                      "<td style='border-top: 2px solid black;'></td>"
                    ]
                  }
                ],
                "else": [
                  {
                    "#type": "if_then_else_block",
                    "if": {
                      "#type": "expression",
                      "value": "C[6] == 0 && C[5] == 0 && C[i] == 0 && i == 4"
                    },
                    "then": [
                      {
                        "#type": "func",
                        "name": "addPartialExplanation",
                        "args": [
                          "<td style='border-top: 2px solid black;'></td>"
                        ]
                      }
                    ],
                    "else": [
                      {
                        "#type": "if_then_else_block",
                        "if": {
                          "#type": "expression",
                          "value": "C[6] == 0 && C[5]==0 && C[4] == 0 && C[i] == 0 && i == 3"
                        },
                        "then": [
                          {
                            "#type": "func",
                            "name": "addPartialExplanation",
                            "args": [
                              "<td style='border-top: 2px solid black;'></td>"
                            ]
                          }
                        ],
                        "else": [
                          {
                            "#type": "if_then_else_block",
                            "if": {
                              "#type": "expression",
                              "value": "C[6] == 0 && C[5]==0 && C[4] == 0 && C[3] == 0 && C[i] == 0 && i == 2"
                            },
                            "then": [
                              {
                                "#type": "func",
                                "name": "addPartialExplanation",
                                "args": [
                                  "<td style='border-top: 2px solid black;'></td>"
                                ]
                              }
                            ],
                            "else": [
                              {
                                "#type": "if_then_else_block",
                                "if": {
                                  "#type": "expression",
                                  "value": "C[6] == 0 && C[5]==0 && C[4] == 0 && C[3] == 0 && C[2] == 0 && C[i] == 0 && i == 1"
                                },
                                "then": [
                                  {
                                    "#type": "func",
                                    "name": "addPartialExplanation",
                                    "args": [
                                      "<td style='border-top: 2px solid black;'></td>"
                                    ]
                                  }
                                ],
                                "else": [
                                  {
                                    "#type": "func",
                                    "name": "addPartialExplanation",
                                    "args": [
                                      "<td  style='border-top: 2px solid black;'>${C[i]}</td>"
                                    ]
                                  }
                                ]
                              }
                            ]
                          }
                        ]
                      }
                    ]
                  }
                ]
              }
            ]
          },
          {
            "#type": "variable",
            "name": "i",
            "value": {
              "#type": "expression",
              "value": "i-1"
            }
          }
        ]
      },
      {
        "#type": "func",
        "name": "addPartialExplanation",
        "args": [
          "</tr></table><center>"
        ]
      },
      {
        "#type": "func",
        "name": "endPartialExplanation",
        "args": []
      }
    ]
  }
];

/** DO NOT REMOVE BELOW SETTINGS */
/** [[BEGIN_XML
<xml xmlns="http://www.w3.org/1999/xhtml">
  <block type="question" id="5gGw$H.=Ik)3{`iP(j!T" x="0" y="0">
    <field name="name">Y6.NA.AS.VSB.1B</field>
    <field name="formula">equality</field>
    <statement name="contents">
      <block type="variable" id="^68J,$)07KvC79~[zz9i">
        <field name="name">image_path</field>
        <value name="value">
          <block type="string_value" id="jPmvj[rgmDZic)5N|DA.">
            <field name="value">Develop/ImageQAs/Y6.NA.AS.VSB.1B/</field>
          </block>
        </value>
        <next>
          <block type="variable" id="FWRGA@2wV4O[o!oJdr:RO">
            <field name="name">background</field>
            <value name="value">
              <block type="string_array" id="iJ==(dHlAkQL5RE58[Pf">
                <field name="items">bg1|bg2|bg3|bg4|bg5|bg6</field>
              </block>
            </value>
            <next>
              <block type="variable" id="QCRoBLD{67}!upChP-{L">
                <field name="name">drop_background</field>
                <value name="value">
                  <block type="random_one" id="x(nV0m[bFs1g2:EvdmlU">
                    <value name="items">
                      <block type="expression" id="/_t@2vuD~wQ-Z,Wu#,/zX">
                        <field name="value">background</field>
                      </block>
                    </value>
                  </block>
                </value>
                <next>
                  <block type="background_shape" id="EN#?w;AHSW!~,@1Xm(|0p">
                    <statement name="#props">
                      <block type="prop_image_key" id="V3L`+v9#9w3/4GTaf/rl">
                        <field name="#prop"></field>
                        <value name="key">
                          <block type="string_value" id="]D(y7BWEOj3kiTcDT|}j">
                            <field name="value">${drop_background}.png</field>
                          </block>
                        </value>
                        <next>
                          <block type="prop_image_src" id="O6z@1/a#B:mN8|waz@24!j">
                            <field name="#prop"></field>
                            <value name="src">
                              <block type="string_value" id="h!|mKnw`ukGW;nf2ME1W">
                                <field name="value">${image_path}${drop_background}.png</field>
                              </block>
                            </value>
                          </block>
                        </next>
                      </block>
                    </statement>
                    <next>
                      <block type="input_param" id="hzOgPQ4vabROkeiDbwV/" inline="true">
                        <field name="name">numberOfCorrect</field>
                        <value name="value">
                          <block type="expression" id="RmUys@1(jG(@2A$!XxJAPQ">
                            <field name="value">0</field>
                          </block>
                        </value>
                        <next>
                          <block type="input_param" id="cgu@2M::O5O[iXGW^5p.M" inline="true">
                            <field name="name">numberOfIncorrect</field>
                            <value name="value">
                              <block type="expression" id="ilsqIzW?56adfinQve=g">
                                <field name="value">0</field>
                              </block>
                            </value>
                            <next>
                              <block type="variable" id="}OBMY^Dw|lcyd$wtH:2q">
                                <field name="name">range</field>
                                <value name="value">
                                  <block type="expression" id="rf];0Hi3O1S;++[V9{;d">
                                    <field name="value">$params.numberOfCorrect - $params.numberOfIncorrect</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="if_then_else_block" id="eS[x1NT`nPB=[hPW-iVC">
                                    <value name="if">
                                      <block type="expression" id="7@1}x-D:Q?Z.g;D^n5B[D">
                                        <field name="value">range &lt; 0</field>
                                      </block>
                                    </value>
                                    <statement name="then">
                                      <block type="variable" id="ysd?4D~tnt_5;ERtX~i:">
                                        <field name="name">x</field>
                                        <value name="value">
                                          <block type="random_number" id="u|aEXt%2G,vl^F#o4-oX">
                                            <value name="min">
                                              <block type="expression" id="GF-El[`+Agq)0^ovB8QF">
                                                <field name="value">10000</field>
                                              </block>
                                            </value>
                                            <value name="max">
                                              <block type="expression" id=",aB@1!Zpgh37U{-d7kvmu">
                                                <field name="value">90000</field>
                                              </block>
                                            </value>
                                          </block>
                                        </value>
                                        <next>
                                          <block type="variable" id="bENSsrE%7Nv^n7a^0}#z">
                                            <field name="name">y</field>
                                            <value name="value">
                                              <block type="random_number" id="Ot.1`07FyQJ:C%t+Nd=x">
                                                <value name="min">
                                                  <block type="expression" id="na_UNci,Zv8i}r1dSATf">
                                                    <field name="value">10000</field>
                                                  </block>
                                                </value>
                                                <value name="max">
                                                  <block type="expression" id="!HRtEZE8wmm:8]4SFs~!">
                                                    <field name="value">x</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </value>
                                          </block>
                                        </next>
                                      </block>
                                    </statement>
                                    <statement name="else">
                                      <block type="if_then_else_block" id="ElpV2P6.k!wcYh[aaWbD">
                                        <value name="if">
                                          <block type="expression" id="nCB[(4MR25/Ri60owvM,">
                                            <field name="value">range &lt; 4</field>
                                          </block>
                                        </value>
                                        <statement name="then">
                                          <block type="variable" id="rOG(e`vv$f/D_Bjuy4?l">
                                            <field name="name">x</field>
                                            <value name="value">
                                              <block type="random_number" id="2IDL;He9?!7BgV/M0/z$">
                                                <value name="min">
                                                  <block type="expression" id="+eON]dMw%jmc(+Sd]RzT">
                                                    <field name="value">99000</field>
                                                  </block>
                                                </value>
                                                <value name="max">
                                                  <block type="expression" id="2YPR_vl;~)[7xh$bBn]#">
                                                    <field name="value">499000</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id="rvR_j^F2Pd+-PO|BC69n">
                                                <field name="name">y</field>
                                                <value name="value">
                                                  <block type="random_number" id=":O|D~H^m[.L.hN_n-E60">
                                                    <value name="min">
                                                      <block type="expression" id="-`Zxt~l=Kjcwg^brp0dy">
                                                        <field name="value">99000</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="PD!i@1gU0?A?USf3v4Jt:">
                                                        <field name="value">x</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                              </block>
                                            </next>
                                          </block>
                                        </statement>
                                        <statement name="else">
                                          <block type="variable" id="J;@1]IPzN:NR_9.2t;T)3">
                                            <field name="name">x</field>
                                            <value name="value">
                                              <block type="random_number" id="MnURDtFKkYTr72xBzUm[">
                                                <value name="min">
                                                  <block type="expression" id="dARaYC{e7#t{C=v-w0FF">
                                                    <field name="value">500000</field>
                                                  </block>
                                                </value>
                                                <value name="max">
                                                  <block type="expression" id="FChe0Q@1jMr6+sBW#+Oqf">
                                                    <field name="value">999999</field>
                                                  </block>
                                                </value>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id="t/a!RW8}hf3h8^4`pVJw">
                                                <field name="name">y</field>
                                                <value name="value">
                                                  <block type="random_number" id="f6Q`L|q1b#suOd_a/!/d">
                                                    <value name="min">
                                                      <block type="expression" id="vfU#V%1Wl2)~ZYdD@2B.f">
                                                        <field name="value">500000</field>
                                                      </block>
                                                    </value>
                                                    <value name="max">
                                                      <block type="expression" id="ZW-Xlgctv[JyT9Q03+d/">
                                                        <field name="value">x</field>
                                                      </block>
                                                    </value>
                                                  </block>
                                                </value>
                                              </block>
                                            </next>
                                          </block>
                                        </statement>
                                      </block>
                                    </statement>
                                    <next>
                                      <block type="variable" id="Nje9!`ECO9DX.:VuSc;i">
                                        <field name="name">A</field>
                                        <value name="value">
                                          <block type="expression" id="5=1;YeZz;Fioix))IC5C">
                                            <field name="value">[]</field>
                                          </block>
                                        </value>
                                        <next>
                                          <block type="variable" id="8emot0G+Al]jcI.,6@2@2a">
                                            <field name="name">x1</field>
                                            <value name="value">
                                              <block type="expression" id="}a(l9?f$3tY##|vy,?~0">
                                                <field name="value">Math.floor(x/100000)</field>
                                              </block>
                                            </value>
                                            <next>
                                              <block type="variable" id="@1=Xo:VBFQ~9[$]]??P@1o">
                                                <field name="name">x2</field>
                                                <value name="value">
                                                  <block type="expression" id="#+@1[2VB{[.|mj6c)Xs;8">
                                                    <field name="value">Math.floor((x-x1@2100000)/10000)</field>
                                                  </block>
                                                </value>
                                                <next>
                                                  <block type="variable" id="#1uMEO}RLp@2N_c+kr#E}">
                                                    <field name="name">x3</field>
                                                    <value name="value">
                                                      <block type="expression" id="Q_Ayj9/4az9E9XDbeedM">
                                                        <field name="value">Math.floor((x-x1@2100000-x2@210000)/1000)</field>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="variable" id="GHF.@1dw_!J#2?5{i~Gqy">
                                                        <field name="name">x4</field>
                                                        <value name="value">
                                                          <block type="expression" id="T%iv)$XzKuyKf//VzjcU">
                                                            <field name="value">Math.floor((x-x1@2100000-x2@210000-x3@21000)/100)</field>
                                                          </block>
                                                        </value>
                                                        <next>
                                                          <block type="variable" id="2WmNFvHk@2Be:Nx(.F/bw">
                                                            <field name="name">x5</field>
                                                            <value name="value">
                                                              <block type="expression" id="h]3Q~`$k(G}zDc$t[3NS">
                                                                <field name="value">Math.floor((x-x1@2100000-x2@210000-x3@21000-x4@2100)/10)</field>
                                                              </block>
                                                            </value>
                                                            <next>
                                                              <block type="variable" id="~_|62(gtYv3gu/BR:b,!">
                                                                <field name="name">x6</field>
                                                                <value name="value">
                                                                  <block type="expression" id="UO#@2i815%_Q8:;ns=b%K">
                                                                    <field name="value">x-x1@2100000-x2@210000-x3@21000-x4@2100-x5@210</field>
                                                                  </block>
                                                                </value>
                                                                <next>
                                                                  <block type="variable" id="t(P07{=TW6^QDB9Xp1sW">
                                                                    <field name="name">A[0]</field>
                                                                    <value name="value">
                                                                      <block type="expression" id="_-êdtAtydcK~(c-zW,">
                                                                        <field name="value">x6</field>
                                                                      </block>
                                                                    </value>
                                                                    <next>
                                                                      <block type="variable" id="P@2@1.;8ibn;@2~R!31QrBw">
                                                                        <field name="name">A[1]</field>
                                                                        <value name="value">
                                                                          <block type="expression" id="Olz77J)}7w.~7,ab|E5D">
                                                                            <field name="value">x5</field>
                                                                          </block>
                                                                        </value>
                                                                        <next>
                                                                          <block type="variable" id="mPethRDJrPRd1!pkUbJA">
                                                                            <field name="name">A[2]</field>
                                                                            <value name="value">
                                                                              <block type="expression" id="VBu;?RUjSyxN@2(s)jy$h">
                                                                                <field name="value">x4</field>
                                                                              </block>
                                                                            </value>
                                                                            <next>
                                                                              <block type="variable" id="0{;41@1IG1vvvF3e:eA4e">
                                                                                <field name="name">A[3]</field>
                                                                                <value name="value">
                                                                                  <block type="expression" id="k^,.tq^YiD|O6l07;i3b">
                                                                                    <field name="value">x3</field>
                                                                                  </block>
                                                                                </value>
                                                                                <next>
                                                                                  <block type="variable" id="q}KL4#wwh/6,mE|1^9D7">
                                                                                    <field name="name">A[4]</field>
                                                                                    <value name="value">
                                                                                      <block type="expression" id="BluVw%[uOfx7jw;p3LyK">
                                                                                        <field name="value">x2</field>
                                                                                      </block>
                                                                                    </value>
                                                                                    <next>
                                                                                      <block type="variable" id="_5;tozRD[;+U|UJW3`(/">
                                                                                        <field name="name">A[5]</field>
                                                                                        <value name="value">
                                                                                          <block type="expression" id="Rx}LmeHwoG(-teR5=QS0">
                                                                                            <field name="value">x1</field>
                                                                                          </block>
                                                                                        </value>
                                                                                        <next>
                                                                                          <block type="variable" id="q8qzCj7]d(k^?NtMbxsG">
                                                                                            <field name="name">B</field>
                                                                                            <value name="value">
                                                                                              <block type="expression" id="6X+5)W~lcH#6N@1Z}MHh/">
                                                                                                <field name="value">[]</field>
                                                                                              </block>
                                                                                            </value>
                                                                                            <next>
                                                                                              <block type="variable" id="L/3I0[YVa@2j-FC,2AGif">
                                                                                                <field name="name">y1</field>
                                                                                                <value name="value">
                                                                                                  <block type="expression" id="A+6nBFS__4G?#mF8X)mv">
                                                                                                    <field name="value">Math.floor(y/100000)</field>
                                                                                                  </block>
                                                                                                </value>
                                                                                                <next>
                                                                                                  <block type="variable" id="ESll`3cOvdg`gj!8%?(q">
                                                                                                    <field name="name">y2</field>
                                                                                                    <value name="value">
                                                                                                      <block type="expression" id="^08nWF-yEdnc=:8M%ts{">
                                                                                                        <field name="value">Math.floor((y-y1@2100000)/10000)</field>
                                                                                                      </block>
                                                                                                    </value>
                                                                                                    <next>
                                                                                                      <block type="variable" id="PNu-7$ts2;QMPyq)-uNy">
                                                                                                        <field name="name">y3</field>
                                                                                                        <value name="value">
                                                                                                          <block type="expression" id="`X[BxAwu?Gj/yFAy!+;H">
                                                                                                            <field name="value">Math.floor((y-y1@2100000-y2@210000)/1000)</field>
                                                                                                          </block>
                                                                                                        </value>
                                                                                                        <next>
                                                                                                          <block type="variable" id="#OJE?@1;%8HmPr{u1-Fr9">
                                                                                                            <field name="name">y4</field>
                                                                                                            <value name="value">
                                                                                                              <block type="expression" id=".~^x^+{S6%?CRd?H9FE!">
                                                                                                                <field name="value">Math.floor((y-y1@2100000-y2@210000-y3@21000)/100)</field>
                                                                                                              </block>
                                                                                                            </value>
                                                                                                            <next>
                                                                                                              <block type="variable" id="5_7xvgR@1sy#91rcZ%jxC">
                                                                                                                <field name="name">y5</field>
                                                                                                                <value name="value">
                                                                                                                  <block type="expression" id="r4X@2$@1h4`2;9eQ])OF.)">
                                                                                                                    <field name="value">Math.floor((y-y1@2100000-y2@210000-y3@21000-y4@2100)/10)</field>
                                                                                                                  </block>
                                                                                                                </value>
                                                                                                                <next>
                                                                                                                  <block type="variable" id="r0r/WJQ_;QhP!lHp2QId">
                                                                                                                    <field name="name">y6</field>
                                                                                                                    <value name="value">
                                                                                                                      <block type="expression" id="n|_I{-Uvlvb^6AunNQzk">
                                                                                                                        <field name="value">y-y1@2100000-y2@210000-y3@21000-y4@2100-y5@210</field>
                                                                                                                      </block>
                                                                                                                    </value>
                                                                                                                    <next>
                                                                                                                      <block type="variable" id="+LjTSy~^Moe.KF1^gT}=">
                                                                                                                        <field name="name">B[0]</field>
                                                                                                                        <value name="value">
                                                                                                                          <block type="expression" id="LE4Uo@1mGTj@2y+FhjE6!i">
                                                                                                                            <field name="value">y6</field>
                                                                                                                          </block>
                                                                                                                        </value>
                                                                                                                        <next>
                                                                                                                          <block type="variable" id="|c!?GQx=E6kORG@1QavJV">
                                                                                                                            <field name="name">B[1]</field>
                                                                                                                            <value name="value">
                                                                                                                              <block type="expression" id="VT-Xg7M%iRZ0kV%TRb%m">
                                                                                                                                <field name="value">y5</field>
                                                                                                                              </block>
                                                                                                                            </value>
                                                                                                                            <next>
                                                                                                                              <block type="variable" id="_KL=s}T/.NtJn3;2O72k">
                                                                                                                                <field name="name">B[2]</field>
                                                                                                                                <value name="value">
                                                                                                                                  <block type="expression" id="e!#Y:3wLi=14R@2RXz1$P">
                                                                                                                                    <field name="value">y4</field>
                                                                                                                                  </block>
                                                                                                                                </value>
                                                                                                                                <next>
                                                                                                                                  <block type="variable" id="~a2=dIv?cd?+0,1m{.2S">
                                                                                                                                    <field name="name">B[3]</field>
                                                                                                                                    <value name="value">
                                                                                                                                      <block type="expression" id="QXE@1@2H%fn`9Ukxn`@1iOW">
                                                                                                                                        <field name="value">y3</field>
                                                                                                                                      </block>
                                                                                                                                    </value>
                                                                                                                                    <next>
                                                                                                                                      <block type="variable" id="J$$c(ea0+~UD3@2+,L}iI">
                                                                                                                                        <field name="name">B[4]</field>
                                                                                                                                        <value name="value">
                                                                                                                                          <block type="expression" id="(XI=p8=6jV6mSdb-_`JA">
                                                                                                                                            <field name="value">y2</field>
                                                                                                                                          </block>
                                                                                                                                        </value>
                                                                                                                                        <next>
                                                                                                                                          <block type="variable" id="a@1vBczb@2i2q!t^e=vI^w">
                                                                                                                                            <field name="name">B[5]</field>
                                                                                                                                            <value name="value">
                                                                                                                                              <block type="expression" id="rc@2D|,GRF1b_cMJ~j@2z?">
                                                                                                                                                <field name="value">y1</field>
                                                                                                                                              </block>
                                                                                                                                            </value>
                                                                                                                                            <next>
                                                                                                                                              <block type="variable" id="xc5,JS.$B@1(tq6~}$e]P">
                                                                                                                                                <field name="name">A[6]</field>
                                                                                                                                                <value name="value">
                                                                                                                                                  <block type="expression" id="%#)k%eI=bS,B`i4RnaWR">
                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                  </block>
                                                                                                                                                </value>
                                                                                                                                                <next>
                                                                                                                                                  <block type="variable" id="0_wa8{]_+_`~B{Wi9;bS">
                                                                                                                                                    <field name="name">B[6]</field>
                                                                                                                                                    <value name="value">
                                                                                                                                                      <block type="expression" id="XfuRWLkMTKVpB$90Ma)E">
                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                      </block>
                                                                                                                                                    </value>
                                                                                                                                                    <next>
                                                                                                                                                      <block type="variable" id="VOD;X5UqrDZ:nS18Kl]s">
                                                                                                                                                        <field name="name">C</field>
                                                                                                                                                        <value name="value">
                                                                                                                                                          <block type="expression" id="}W1f5wdBmytY@14]])7z!">
                                                                                                                                                            <field name="value">[]</field>
                                                                                                                                                          </block>
                                                                                                                                                        </value>
                                                                                                                                                        <next>
                                                                                                                                                          <block type="variable" id="7h~lu570V=Mpp^0_#:S9">
                                                                                                                                                            <field name="name">temp</field>
                                                                                                                                                            <value name="value">
                                                                                                                                                              <block type="expression" id="}^GR;t6mms:t?Gt/)K5h">
                                                                                                                                                                <field name="value">[]</field>
                                                                                                                                                              </block>
                                                                                                                                                            </value>
                                                                                                                                                            <next>
                                                                                                                                                              <block type="variable" id="-ohK)M/;E{zEMy5/A)~4">
                                                                                                                                                                <field name="name">temp[0]</field>
                                                                                                                                                                <value name="value">
                                                                                                                                                                  <block type="expression" id="~xx#iN_pn0b]$0mU?E@1!">
                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                  </block>
                                                                                                                                                                </value>
                                                                                                                                                                <next>
                                                                                                                                                                  <block type="variable" id=".P[YC5zA59NH:lywc:Of">
                                                                                                                                                                    <field name="name">i</field>
                                                                                                                                                                    <value name="value">
                                                                                                                                                                      <block type="expression" id="cybiJsk{t^G5-b6`U(Wk">
                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                      </block>
                                                                                                                                                                    </value>
                                                                                                                                                                    <next>
                                                                                                                                                                      <block type="while_do_block" id="6}Th=h%ssqH%jNlmKx~@1">
                                                                                                                                                                        <value name="while">
                                                                                                                                                                          <block type="expression" id="Y4WU(:BC+?FUKIr2NNdd">
                                                                                                                                                                            <field name="value">i &lt; 7</field>
                                                                                                                                                                          </block>
                                                                                                                                                                        </value>
                                                                                                                                                                        <statement name="do">
                                                                                                                                                                          <block type="if_then_else_block" id="HKR@1q8@1@1I5Yvoy}FA^4@1">
                                                                                                                                                                            <value name="if">
                                                                                                                                                                              <block type="expression" id="l%LAVUt=+h2KM!ZSjPl6">
                                                                                                                                                                                <field name="value">A[i] &lt; B[i] + temp[i]</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <statement name="then">
                                                                                                                                                                              <block type="variable" id="NMl$ohQV6dR#MyX:1]/!">
                                                                                                                                                                                <field name="name">C[i]</field>
                                                                                                                                                                                <value name="value">
                                                                                                                                                                                  <block type="expression" id="|ZliY,,X2@2O#QU1A:l5P">
                                                                                                                                                                                    <field name="value">10 + A[i] - B[i] - temp[i]</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="variable" id="kPUrOYEU+++2diHuOBFo">
                                                                                                                                                                                    <field name="name">temp[i+1]</field>
                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                      <block type="expression" id="hzWJ2[J|1CGumO+]qj+E">
                                                                                                                                                                                        <field name="value">1</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </statement>
                                                                                                                                                                            <statement name="else">
                                                                                                                                                                              <block type="variable" id="Z6cELV16?kIW-k226ij}">
                                                                                                                                                                                <field name="name">C[i]</field>
                                                                                                                                                                                <value name="value">
                                                                                                                                                                                  <block type="expression" id="dc`itZSMEzb^n#Bt-XYN">
                                                                                                                                                                                    <field name="value">A[i] - B[i] - temp[i]</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="variable" id="oRNecupM-h-Q@11+(_J4o">
                                                                                                                                                                                    <field name="name">temp[i+1]</field>
                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                      <block type="expression" id="}k6sj`Qijd8Qf1vA8QjK">
                                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </statement>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="variable" id="dY@2l#4zXUruACM{;pw..">
                                                                                                                                                                                <field name="name">i</field>
                                                                                                                                                                                <value name="value">
                                                                                                                                                                                  <block type="expression" id="+5cu+(Og@1XeuUyK4}SXp">
                                                                                                                                                                                    <field name="value">i + 1</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </statement>
                                                                                                                                                                        <next>
                                                                                                                                                                          <block type="variable" id="0p)VYCkos@1.z]`p`qdTf">
                                                                                                                                                                            <field name="name">D</field>
                                                                                                                                                                            <value name="value">
                                                                                                                                                                              <block type="expression" id="lg/4_F;Bsi%K[9@16@2}$C">
                                                                                                                                                                                <field name="value">[]</field>
                                                                                                                                                                              </block>
                                                                                                                                                                            </value>
                                                                                                                                                                            <next>
                                                                                                                                                                              <block type="variable" id="$l3,fngpL8pl@2H!jL4b@2">
                                                                                                                                                                                <field name="name">i</field>
                                                                                                                                                                                <value name="value">
                                                                                                                                                                                  <block type="expression" id="(S:i]wa(PFtK057~A#T2">
                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </value>
                                                                                                                                                                                <next>
                                                                                                                                                                                  <block type="while_do_block" id="lZ1}tH=vBl?_.59y.f+S">
                                                                                                                                                                                    <value name="while">
                                                                                                                                                                                      <block type="expression" id="5_7V2dwdo^(H$+C(W$mD">
                                                                                                                                                                                        <field name="value">i &lt; 7</field>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </value>
                                                                                                                                                                                    <statement name="do">
                                                                                                                                                                                      <block type="if_then_else_block" id="C1#l(oM@1BYPe/V9_[pB_">
                                                                                                                                                                                        <value name="if">
                                                                                                                                                                                          <block type="expression" id="lXC?zWb`}hDLZ`]s^`,n">
                                                                                                                                                                                            <field name="value">temp[i] == 0</field>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </value>
                                                                                                                                                                                        <statement name="then">
                                                                                                                                                                                          <block type="variable" id="|0c7Ck8je=Y|@1EEEJnz)">
                                                                                                                                                                                            <field name="name">D[i]</field>
                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                              <block type="expression" id="KdJEWFh#H!uB@2j;wH.|.">
                                                                                                                                                                                                <field name="value">-1</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </statement>
                                                                                                                                                                                        <statement name="else">
                                                                                                                                                                                          <block type="variable" id="[O92)U$c)aP0ieZYYhCE">
                                                                                                                                                                                            <field name="name">D[i]</field>
                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                              <block type="expression" id="P)HB{;],I/coPrhYs8}J">
                                                                                                                                                                                                <field name="value">(A[i]+9) % 10</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </statement>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="variable" id="3GE5XH~]cu@2XlsuyZU@2~">
                                                                                                                                                                                            <field name="name">i</field>
                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                              <block type="expression" id="ih.f/$,a-=fasp@1^0T7u">
                                                                                                                                                                                                <field name="value">i+1</field>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </value>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </statement>
                                                                                                                                                                                    <next>
                                                                                                                                                                                      <block type="statement" id="dl^mut[(}5S+@1DBudkv.">
                                                                                                                                                                                        <field name="value">temp.splice(0,1);</field>
                                                                                                                                                                                        <next>
                                                                                                                                                                                          <block type="text_shape" id="`)p#}DxHk.+yvGfoCJet">
                                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                                              <block type="prop_position" id="h3tYh1-mGwLrM5:SL~Du">
                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                <value name="x">
                                                                                                                                                                                                  <block type="resp_x" id="n@1x{aqUDuliWtL9R,w7.">
                                                                                                                                                                                                    <field name="value">cx</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <value name="y">
                                                                                                                                                                                                  <block type="expression" id="R-;A@1+)@1ZXDuf]It:x6E">
                                                                                                                                                                                                    <field name="value">60</field>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </value>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="prop_text_contents" id="P[JGH]{MEryg9B[4y/@2k">
                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                    <value name="contents">
                                                                                                                                                                                                      <block type="string_value" id="e12J!gYy(!ET0gK=uf];">
                                                                                                                                                                                                        <field name="value">Complete the following subtraction:</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="prop_text_style" id="n=jsQQ0r._5RpI,plHki">
                                                                                                                                                                                                        <field name="base">text</field>
                                                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                                                          <block type="prop_text_style_font_size" id="0]~WR.C6o-TTGHNg[_v;">
                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                            <value name="fontSize">
                                                                                                                                                                                                              <block type="expression" id="6O]m6RDXVXAF7BHbo4$`">
                                                                                                                                                                                                                <field name="value">42</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                            <next>
                                                                                                                                                                                                              <block type="prop_text_style_fill" id="#{h7mLkz-Bd2Ine;%F!S">
                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                <value name="fill">
                                                                                                                                                                                                                  <block type="string_value" id="Z?ZsP,6f/dEC3Fk4ywal">
                                                                                                                                                                                                                    <field name="value">black</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                  <block type="prop_text_style_stroke" id="hE(uSAmy3JoEv%#K,rea">
                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                    <value name="stroke">
                                                                                                                                                                                                                      <block type="string_value" id="I0w4FO@1O}1/a9@1Ser))@2">
                                                                                                                                                                                                                        <field name="value">white</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                      <block type="prop_text_style_stroke_thickness" id="s6V4Li1Y(R`d-tD8nh8^">
                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                        <value name="strokeThickness">
                                                                                                                                                                                                                          <block type="expression" id="Wg{95ax}@26-}@2e#Lv{U]">
                                                                                                                                                                                                                            <field name="value">2</field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </next>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </next>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </statement>
                                                                                                                                                                                            <next>
                                                                                                                                                                                              <block type="grid_shape" id="_Sx:ES:,u|ery[Ix7v`h">
                                                                                                                                                                                                <statement name="#props">
                                                                                                                                                                                                  <block type="prop_grid_dimension" id="62-nn?))nuP-kvp]j?V9">
                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                    <value name="rows">
                                                                                                                                                                                                      <block type="expression" id=")Ob;UDFnD[^Sk/ysfNrI">
                                                                                                                                                                                                        <field name="value">4</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <value name="cols">
                                                                                                                                                                                                      <block type="expression" id="Ml?WS_(BCq`Ho[O7wfs7">
                                                                                                                                                                                                        <field name="value">1</field>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </value>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="prop_position" id="DL{-X#H)V2YiThg2w{9d">
                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                        <value name="x">
                                                                                                                                                                                                          <block type="resp_x" id="A2Si?MX)=@2KKX`=3LUaF">
                                                                                                                                                                                                            <field name="value">cx</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <value name="y">
                                                                                                                                                                                                          <block type="expression" id="kzWc.%_|![~WbjObG@2v`">
                                                                                                                                                                                                            <field name="value">100</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="prop_size" id="K@2b$4zV!BD0:IXT~S6Q9">
                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                            <value name="width">
                                                                                                                                                                                                              <block type="expression" id="Zu8eyk1.y@2dcdjf=iKx.">
                                                                                                                                                                                                                <field name="value">300</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                            <value name="height">
                                                                                                                                                                                                              <block type="expression" id="m[y?wJ6ZxlT3Qh2e.:G4">
                                                                                                                                                                                                                <field name="value">280</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                            <next>
                                                                                                                                                                                                              <block type="prop_anchor" id="P6GjP10SYZ.kMaF,k`G_">
                                                                                                                                                                                                                <field name="#prop">anchor</field>
                                                                                                                                                                                                                <value name="x">
                                                                                                                                                                                                                  <block type="expression" id="e6)]l37u+fmqW$xgiXg[">
                                                                                                                                                                                                                    <field name="value">0.5</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                <value name="y">
                                                                                                                                                                                                                  <block type="expression" id="H-D+9)r,OF.?oy$%Wf1C">
                                                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                  <block type="prop_grid_cell_source" id="aGzqs[`uE/lN+31?/}J8">
                                                                                                                                                                                                                    <field name="#prop">cell.source</field>
                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                      <block type="func_mix" id="qgfs)i^3itC$?d?|Y|X]" inline="true">
                                                                                                                                                                                                                        <field name="collections">1</field>
                                                                                                                                                                                                                        <field name="variable">$item</field>
                                                                                                                                                                                                                        <field name="count">4</field>
                                                                                                                                                                                                                        <field name="mixer">$item</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                      <block type="prop_grid_random" id="#3:t)xgzG/}HS_@1H_/6o">
                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                        <field name="random">FALSE</field>
                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                          <block type="prop_grid_show_borders" id="_l[~DeIh#r-uD4@2jOSGR">
                                                                                                                                                                                                                            <field name="#prop">#showBorders</field>
                                                                                                                                                                                                                            <field name="value">FALSE</field>
                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                              <block type="prop_grid_cell_template_for" id="#?$wR36iH]soMMebuSiF">
                                                                                                                                                                                                                                <field name="variable">$cell</field>
                                                                                                                                                                                                                                <field name="condition">$cell.row == 0</field>
                                                                                                                                                                                                                                <field name="#prop">cell.templates[]</field>
                                                                                                                                                                                                                                <field name="#callback">$cell</field>
                                                                                                                                                                                                                                <statement name="body">
                                                                                                                                                                                                                                  <block type="list_shape" id="/32HhVLT;I)U7#X/^be8">
                                                                                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                                                                                      <block type="prop_list_direction" id="S7lh-M3M0}MReL8HZ$0a">
                                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                                        <field name="dir">horizontal</field>
                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                          <block type="prop_position" id="g:me9CdMeY!Xa9`;{yKo">
                                                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                                                            <value name="x">
                                                                                                                                                                                                                                              <block type="expression" id="SaxJU.3s3m0z~T6^v)PY">
                                                                                                                                                                                                                                                <field name="value">$cell.right</field>
                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                            <value name="y">
                                                                                                                                                                                                                                              <block type="expression" id=";X?tKUAK022)WnD?F__8">
                                                                                                                                                                                                                                                <field name="value">$cell.centerY</field>
                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                              <block type="prop_spacing" id="XLbG!MjnRNRFO_br0d@20">
                                                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                                                <value name="spacing">
                                                                                                                                                                                                                                                  <block type="expression" id="ZY[.Lo!,)0RF,H`fu2y+">
                                                                                                                                                                                                                                                    <field name="value">15</field>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                                  <block type="prop_anchor" id="a2HZilWA1IK9$m0]6JbR">
                                                                                                                                                                                                                                                    <field name="#prop">anchor</field>
                                                                                                                                                                                                                                                    <value name="x">
                                                                                                                                                                                                                                                      <block type="expression" id="0Q|nkA6}Mfz[`~5vR+QS">
                                                                                                                                                                                                                                                        <field name="value">1</field>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                    <value name="y">
                                                                                                                                                                                                                                                      <block type="expression" id="1_WdhT6)nJlZy.ph@1,)#">
                                                                                                                                                                                                                                                        <field name="value">0.5</field>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                                                    <statement name="items">
                                                                                                                                                                                                                                      <block type="list_item_shape" id="Tk,bXO[ll0+-c9`xpTM8">
                                                                                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                                                                                          <block type="prop_list_align" id="({UX#$aQ]@2~F=AICd[HW">
                                                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                                                            <field name="align">middle</field>
                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                              <block type="prop_list_item_source" id="^m@1RE!owml^Pc$l-W8KF">
                                                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                                                <value name="source">
                                                                                                                                                                                                                                                  <block type="func_characters_of" id="BD{vLk8LijP]kEfNj)?r" inline="true">
                                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                                      <block type="expression" id="[2{sYj0@2N?MS%$Y21Ssn">
                                                                                                                                                                                                                                                        <field name="value">x</field>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                                        <statement name="template">
                                                                                                                                                                                                                                          <block type="image_shape" id="p$6bAD[9Dn]1usaOcUrS">
                                                                                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                                                                                              <block type="prop_position" id="#)kH9_ZjOÜ_Pl09pE7">
                                                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                                                <value name="x">
                                                                                                                                                                                                                                                  <block type="expression" id="V|z99]pkHq6ziU3jr}/M">
                                                                                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                <value name="y">
                                                                                                                                                                                                                                                  <block type="expression" id="8@1t($}98DM:;~@1PcpBoe">
                                                                                                                                                                                                                                                    <field name="value">0</field>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                                  <block type="prop_size" id="2!vYA_[oG/Ka3$m3Z^0)">
                                                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                                                    <value name="width">
                                                                                                                                                                                                                                                      <block type="expression" id="!n6et#T,1]H{fWlTFV]i">
                                                                                                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                    <value name="height">
                                                                                                                                                                                                                                                      <block type="expression" id="AER[RnRERJ0`kr={sQ1~">
                                                                                                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                                      <block type="prop_image_key" id="PH_E%eo!em!Dn-Ixl7^i">
                                                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                                                        <value name="key">
                                                                                                                                                                                                                                                          <block type="string_value" id="f6k}C?M/.GO,+e}@1=PY?">
                                                                                                                                                                                                                                                            <field name="value">${$item.data}.png</field>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                                          <block type="prop_image_src" id="Z2@2gODWKO!m9eH6QifkO">
                                                                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                                                                            <value name="src">
                                                                                                                                                                                                                                                              <block type="string_value" id="Bir9(PZ3WC%Tl:RzN-xG">
                                                                                                                                                                                                                                                                <field name="value">${image_path}${$item.data}.png</field>
                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                  <block type="prop_grid_cell_template_for" id="=59)UsxVwe|rk3-M5?gk">
                                                                                                                                                                                                                                    <field name="variable">$cell</field>
                                                                                                                                                                                                                                    <field name="condition">$cell.row == 1</field>
                                                                                                                                                                                                                                    <field name="#prop">cell.templates[]</field>
                                                                                                                                                                                                                                    <field name="#callback">$cell</field>
                                                                                                                                                                                                                                    <statement name="body">
                                                                                                                                                                                                                                      <block type="list_shape" id="3YUo!Ea-ZZF+I5|2M]hi">
                                                                                                                                                                                                                                        <statement name="#props">
                                                                                                                                                                                                                                          <block type="prop_list_direction" id="itl:m;sKXg#iMB9fOGv$">
                                                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                                                            <field name="dir">horizontal</field>
                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                              <block type="prop_position" id="Er;M#fGL2(0r:xS!:]zM">
                                                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                                                <value name="x">
                                                                                                                                                                                                                                                  <block type="expression" id="tK$AD2UCYDGNpH^3uXe_">
                                                                                                                                                                                                                                                    <field name="value">$cell.right</field>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                <value name="y">
                                                                                                                                                                                                                                                  <block type="expression" id="^A+?!wgC~D7]W@2MM]%VD">
                                                                                                                                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                                  <block type="prop_spacing" id="=PF;BRRO]X]F4cX8mB+g">
                                                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                                                    <value name="spacing">
                                                                                                                                                                                                                                                      <block type="expression" id="=GIcEjn}A4j|7]257nhL">
                                                                                                                                                                                                                                                        <field name="value">15</field>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                                      <block type="prop_anchor" id="miYanw%^5^=[x!=i#d9X">
                                                                                                                                                                                                                                                        <field name="#prop">anchor</field>
                                                                                                                                                                                                                                                        <value name="x">
                                                                                                                                                                                                                                                          <block type="expression" id="PX[e7q@1=+J7@20V`.yJf6">
                                                                                                                                                                                                                                                            <field name="value">1</field>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                        <value name="y">
                                                                                                                                                                                                                                                          <block type="expression" id="U$!h9M15qcR%l)d4U,ov">
                                                                                                                                                                                                                                                            <field name="value">0.5</field>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                                        <statement name="items">
                                                                                                                                                                                                                                          <block type="list_item_shape" id="Qn_87%G@1c-jdH;J7)jc;">
                                                                                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                                                                                              <block type="prop_list_align" id="Z.xEA?6]!+-K~FZHUfcc">
                                                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                                                <field name="align">middle</field>
                                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                                  <block type="prop_list_item_source" id="Y9;|?%KywLQ!NHVhkJsO">
                                                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                                                    <value name="source">
                                                                                                                                                                                                                                                      <block type="func_characters_of" id="a:p-;2F]-}@1E7sbST,oz" inline="true">
                                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                                          <block type="expression" id=".RcotG@2_-sBs#D`LY~6m">
                                                                                                                                                                                                                                                            <field name="value">y</field>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                                                            <statement name="template">
                                                                                                                                                                                                                                              <block type="image_shape" id="83TLT|yI?H52AbRSw|/g">
                                                                                                                                                                                                                                                <statement name="#props">
                                                                                                                                                                                                                                                  <block type="prop_position" id="s4|GE=C}KBCGCY[;o0v4">
                                                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                                                    <value name="x">
                                                                                                                                                                                                                                                      <block type="expression" id="n|Oeo0zBo5@1mU)zmwWe|">
                                                                                                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                    <value name="y">
                                                                                                                                                                                                                                                      <block type="expression" id="^i|so?x`49iKWS#nAF=2">
                                                                                                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                                      <block type="prop_size" id="#POx;0j%(~W$3~_QLI1#">
                                                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                                                        <value name="width">
                                                                                                                                                                                                                                                          <block type="expression" id="Zjh_(vaUp[yvGnawn=?@2">
                                                                                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                        <value name="height">
                                                                                                                                                                                                                                                          <block type="expression" id="AZvTmc9k@1}JnZd4)7?,x">
                                                                                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                                          <block type="prop_image_key" id=":XWCC7Y.G@1OSL{aeSc:q">
                                                                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                                                                            <value name="key">
                                                                                                                                                                                                                                                              <block type="string_value" id="L@1h-be53r;O8TEURv!XU">
                                                                                                                                                                                                                                                                <field name="value">${$item.data}.png</field>
                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                                              <block type="prop_image_src" id="9KKn,[aU(]s#=QD,$@2j7">
                                                                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                                                                <value name="src">
                                                                                                                                                                                                                                                                  <block type="string_value" id="I11H_mY)i@10dZnJnoSi|">
                                                                                                                                                                                                                                                                    <field name="value">${image_path}${$item.data}.png</field>
                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                      <block type="prop_grid_cell_template_for" id="V1~zm.8dr92/[y}J~Nut">
                                                                                                                                                                                                                                        <field name="variable">$cell</field>
                                                                                                                                                                                                                                        <field name="condition">$cell.row == 2</field>
                                                                                                                                                                                                                                        <field name="#prop">cell.templates[]</field>
                                                                                                                                                                                                                                        <field name="#callback">$cell</field>
                                                                                                                                                                                                                                        <statement name="body">
                                                                                                                                                                                                                                          <block type="image_shape" id="@2B@2lv[N37^_$L^PgIA0H">
                                                                                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                                                                                              <block type="prop_position" id="CE,nUxCp?My[Syi]tNr+">
                                                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                                                <value name="x">
                                                                                                                                                                                                                                                  <block type="expression" id="WQ9G4OO)C(6Ezuby@2i?C">
                                                                                                                                                                                                                                                    <field name="value">$cell.centerX</field>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                <value name="y">
                                                                                                                                                                                                                                                  <block type="expression" id="65Do/@26B7K1zQSMDNr#(">
                                                                                                                                                                                                                                                    <field name="value">$cell.centerY</field>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                                  <block type="prop_size" id="LQg/i@1}3PX1Qu+TW1Y2=">
                                                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                                                    <value name="width">
                                                                                                                                                                                                                                                      <block type="expression" id="Gk=%8.t#A#1WHcgtoJc.">
                                                                                                                                                                                                                                                        <field name="value">300</field>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                    <value name="height">
                                                                                                                                                                                                                                                      <block type="expression" id="FT]y.kSa}QE8Fx{@2-sC$">
                                                                                                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                                      <block type="prop_image_key" id=")#ZqVaz0I~;]c@2mf4k$c">
                                                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                                                        <value name="key">
                                                                                                                                                                                                                                                          <block type="string_value" id="T)0owmPpJNd5jvm?I53X">
                                                                                                                                                                                                                                                            <field name="value">line.png</field>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                                          <block type="prop_image_src" id="xy:|=gTh]1Gu_9fnNm]z">
                                                                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                                                                            <value name="src">
                                                                                                                                                                                                                                                              <block type="string_value" id="Vf]C6T6?%$Fv!WLR1doU">
                                                                                                                                                                                                                                                                <field name="value">${image_path}line.png</field>
                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                          <block type="prop_grid_cell_template_for" id="_5/:JO5H4K~D0B@1]Vxp[">
                                                                                                                                                                                                                                            <field name="variable">$cell</field>
                                                                                                                                                                                                                                            <field name="condition">$cell.row == 3</field>
                                                                                                                                                                                                                                            <field name="#prop">cell.templates[]</field>
                                                                                                                                                                                                                                            <field name="#callback">$cell</field>
                                                                                                                                                                                                                                            <statement name="body">
                                                                                                                                                                                                                                              <block type="image_shape" id="%0w:}@1p2)0^%}A[xytdS">
                                                                                                                                                                                                                                                <statement name="#props">
                                                                                                                                                                                                                                                  <block type="prop_position" id="r+FOG]W/b0+T$+2@11[9A">
                                                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                                                    <value name="x">
                                                                                                                                                                                                                                                      <block type="expression" id="|nAXCTi]CB{U0uC1@2nqr">
                                                                                                                                                                                                                                                        <field name="value">$cell.centerX + 20</field>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                    <value name="y">
                                                                                                                                                                                                                                                      <block type="expression" id="kL4q^])w,Fnz^F+@2nAKs">
                                                                                                                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                                      <block type="prop_size" id="r/tV}=FfBaJxcIqhr.oF">
                                                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                                                        <value name="width">
                                                                                                                                                                                                                                                          <block type="expression" id="[Pf!!m2[S]XMbSictQ:(">
                                                                                                                                                                                                                                                            <field name="value">300</field>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                        <value name="height">
                                                                                                                                                                                                                                                          <block type="expression" id="Pwo6X-sy@2Y}bkt?49ETB">
                                                                                                                                                                                                                                                            <field name="value">0</field>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                                          <block type="prop_image_key" id="8}c5o2#IVwlWH0Ww}Yie">
                                                                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                                                                            <value name="key">
                                                                                                                                                                                                                                                              <block type="string_value" id="b0b821w]LiL-eQE~/iFt">
                                                                                                                                                                                                                                                                <field name="value">shape.png</field>
                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                                              <block type="prop_image_src" id="W10?le,:3Ud$v7n(^ONb">
                                                                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                                                                <value name="src">
                                                                                                                                                                                                                                                                  <block type="string_value" id="p?!{$l_Cy9U|tf$|BpND">
                                                                                                                                                                                                                                                                    <field name="value">${image_path}shape.png</field>
                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                                  <block type="variable" id="KMexl%fGe6eX~0c2u]F3">
                                                                                                                                                                                                                                                    <field name="name">i</field>
                                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                                      <block type="expression" id="w;MMNh1SDl^9,B]]qRzs">
                                                                                                                                                                                                                                                        <field name="value">0</field>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                                      <block type="while_do_block" id="6eSt}L4QF$p_tI]m@1WH!">
                                                                                                                                                                                                                                                        <value name="while">
                                                                                                                                                                                                                                                          <block type="expression" id="og}K-ynoP,ggIHH@156PH">
                                                                                                                                                                                                                                                            <field name="value">i &lt; x.toString().length</field>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                        <statement name="do">
                                                                                                                                                                                                                                                          <block type="algorithmic_remainder_shape" id="w#xdhvXO)wJh$vNrTBZ`">
                                                                                                                                                                                                                                                            <statement name="#props">
                                                                                                                                                                                                                                                              <block type="prop_value" id="#;)eq~tRAa}$,lKZO7?D">
                                                                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                                                                  <block type="expression" id="WE_ox1E#h9eag%.!w#w4">
                                                                                                                                                                                                                                                                    <field name="value">C[i]</field>
                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                                                  <block type="prop_position" id="e#O]3[xAs/(^cq=!BA@1x">
                                                                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                                                                    <value name="x">
                                                                                                                                                                                                                                                                      <block type="expression" id="4@2a0#uTb_h!tF{Mt0MoP">
                                                                                                                                                                                                                                                                        <field name="value">$cell.right - 10 - i@245</field>
                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                                    <value name="y">
                                                                                                                                                                                                                                                                      <block type="expression" id="bjh8!:CHqH%j/widtWoL">
                                                                                                                                                                                                                                                                        <field name="value">$cell.centerY</field>
                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                                                      <block type="prop_size" id="AgjD[XU!w@2Wgu|~|+B49">
                                                                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                                                                        <value name="width">
                                                                                                                                                                                                                                                                          <block type="expression" id="Zzw.SeIt=x2GHQ|M7.|t">
                                                                                                                                                                                                                                                                            <field name="value">27</field>
                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                                        <value name="height">
                                                                                                                                                                                                                                                                          <block type="expression" id="].#Cc3ct401cq3b2sa!F">
                                                                                                                                                                                                                                                                            <field name="value">50</field>
                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                                                          <block type="prop_input_keyboard" id="CC8(#OP,^bHIbm@2h_s[u">
                                                                                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                                                                                            <field name="keyboard">numbers1</field>
                                                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                                                              <block type="prop_input_max_length" id="woYtS[Ü]8H)%@1|]TA}">
                                                                                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                                                                                <value name="maxLength">
                                                                                                                                                                                                                                                                                  <block type="expression" id="(X6PrQ8v0E/eon:q)WXS">
                                                                                                                                                                                                                                                                                    <field name="value">1</field>
                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                                                                  <block type="prop_input_result_position" id="In[J1oWdAi^$4Hi5E!(W">
                                                                                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                                                                                    <field name="resultPosition">bottom</field>
                                                                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                                                                      <block type="prop_stroke" id="{?h:bNv.E$uQhPNGPhug">
                                                                                                                                                                                                                                                                                        <field name="#prop">stroke</field>
                                                                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                                                                          <block type="prop_fill" id="KUlx,FItt[95u@1ULdsO7">
                                                                                                                                                                                                                                                                                            <field name="#prop">fill</field>
                                                                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                                                                              <block type="prop_text_style" id="nq2PFI%T-WfRsEgAWf)t">
                                                                                                                                                                                                                                                                                                <field name="base">remainder_input</field>
                                                                                                                                                                                                                                                                                                <statement name="#props">
                                                                                                                                                                                                                                                                                                  <block type="prop_text_style_font_size" id="F=A|!`,5,Po9#-;`gAdc">
                                                                                                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                                                                                                    <value name="fontSize">
                                                                                                                                                                                                                                                                                                      <block type="expression" id="CQb{w|U9OrN`~f5K~Fh{">
                                                                                                                                                                                                                                                                                                        <field name="value">42</field>
                                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                                                                                      <block type="prop_text_style_fill" id="X=}JMUg0BMaecnn9flzd">
                                                                                                                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                                                                                                                        <value name="fill">
                                                                                                                                                                                                                                                                                                          <block type="string_value" id="bF?y?A_MNBC-gW$C]Z|q">
                                                                                                                                                                                                                                                                                                            <field name="value">black</field>
                                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                                                                                          <block type="prop_text_style_stroke" id="N)Se;-5~zO}m8h4BDW;=">
                                                                                                                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                                                                                                                            <value name="stroke">
                                                                                                                                                                                                                                                                                                              <block type="string_value" id="5PPkDUU@1LoxU~VU[-J@1O">
                                                                                                                                                                                                                                                                                                                <field name="value">white</field>
                                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                                                                                              <block type="prop_text_style_stroke_thickness" id="H%Pnb]yfTO7@2tUmcPoHN">
                                                                                                                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                                                                                                                <value name="strokeThickness">
                                                                                                                                                                                                                                                                                                                  <block type="expression" id="~i1e$#Qz4;SudjlO@2MZ6">
                                                                                                                                                                                                                                                                                                                    <field name="value">2</field>
                                                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                                              <block type="variable" id="`f,+iF.q~}Q7A2j}KGx%">
                                                                                                                                                                                                                                                                <field name="name">i</field>
                                                                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                                                                  <block type="expression" id="vd.-M]@2%t[c{eyPFANrz">
                                                                                                                                                                                                                                                                    <field name="value">i+1</field>
                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </next>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </next>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </statement>
                                                                                                                                                                                                <next>
                                                                                                                                                                                                  <block type="image_shape" id="68N[A?!FhBX13IEy[S^o">
                                                                                                                                                                                                    <statement name="#props">
                                                                                                                                                                                                      <block type="prop_position" id="e5BB3;A@26pX~/v`xDJ~4">
                                                                                                                                                                                                        <field name="#prop"></field>
                                                                                                                                                                                                        <value name="x">
                                                                                                                                                                                                          <block type="expression" id="m{c@2jFiRgAdmNS{QuR?_">
                                                                                                                                                                                                            <field name="value">260</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <value name="y">
                                                                                                                                                                                                          <block type="expression" id="hGNxuppF_XX:ozJ/!0tp">
                                                                                                                                                                                                            <field name="value">170</field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="prop_size" id="q+/KFtzIT|;c)2@2j%AHP">
                                                                                                                                                                                                            <field name="#prop"></field>
                                                                                                                                                                                                            <value name="width">
                                                                                                                                                                                                              <block type="expression" id="v9jYm)FDQTK1L-#)RQpO">
                                                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                            <value name="height">
                                                                                                                                                                                                              <block type="expression" id="PNC%DN?;rL}j@1lTG?5+^">
                                                                                                                                                                                                                <field name="value">0</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                            <next>
                                                                                                                                                                                                              <block type="prop_image_key" id="vzp.{;+Zq#d?Eg{4e6it">
                                                                                                                                                                                                                <field name="#prop"></field>
                                                                                                                                                                                                                <value name="key">
                                                                                                                                                                                                                  <block type="string_value" id="~+jjHX8,Ao:h?t6b8nKE">
                                                                                                                                                                                                                    <field name="value">sub.png</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                  <block type="prop_image_src" id="75:9eb{a_D+K/Dz(+0Ct">
                                                                                                                                                                                                                    <field name="#prop"></field>
                                                                                                                                                                                                                    <value name="src">
                                                                                                                                                                                                                      <block type="string_value" id="IL6u:cWC8ys}Pu^Wv$Ea">
                                                                                                                                                                                                                        <field name="value">${image_path}sub.png</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </next>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </next>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                    <next>
                                                                                                                                                                                                      <block type="partial_explanation" id="mC9w9}@12!s1~V=$-8e6a" inline="true">
                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                          <block type="string_value" id="(kBuMI56eVzCl1-y.,,q">
                                                                                                                                                                                                            <field name="value">&lt;u&gt;Let's use the below table to complete this subtraction problem.&lt;/u&gt;

&lt;style&gt;
table{}
td{text-align: center; width: 60px}
&lt;/style&gt;
&lt;/br&gt;&lt;/br&gt;

&lt;center&gt;
&lt;table style='background: rgb(150, 200, 250, 0.2)'&gt;&lt;tr&gt;
                                                                                                                                                                                                            </field>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </value>
                                                                                                                                                                                                        <next>
                                                                                                                                                                                                          <block type="variable" id="W!u4J`T?WRTI^MdC)w9b">
                                                                                                                                                                                                            <field name="name">i</field>
                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                              <block type="expression" id="d/Y`H9j}!3d.C?QnBGsP">
                                                                                                                                                                                                                <field name="value">6</field>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </value>
                                                                                                                                                                                                            <next>
                                                                                                                                                                                                              <block type="partial_explanation" id="mI^OHD.Z-`3yh^D|!gx0" inline="true">
                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                  <block type="string_value" id=";ENV@2YIKM)C]H|7wALrS">
                                                                                                                                                                                                                    <field name="value">&lt;td&gt;Borrowing&lt;/td&gt;</field>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                  <block type="while_do_block" id="Cb1HSL[qHz!N5~:|eQUh">
                                                                                                                                                                                                                    <value name="while">
                                                                                                                                                                                                                      <block type="expression" id="aVwR;oKg56NvVG8?R#^z">
                                                                                                                                                                                                                        <field name="value">i&gt;=0</field>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                    <statement name="do">
                                                                                                                                                                                                                      <block type="if_then_else_block" id="sgCy68r`~;s#TS_Jq(ah">
                                                                                                                                                                                                                        <value name="if">
                                                                                                                                                                                                                          <block type="expression" id=".FXuKMs~[R1:y9MLx;TE">
                                                                                                                                                                                                                            <field name="value">temp[i] != 0</field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                        <statement name="then">
                                                                                                                                                                                                                          <block type="partial_explanation" id="BIS[;IhqyF:6TF3g7?#[" inline="true">
                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                              <block type="string_value" id="CeI@212rXn/7H=E.L5v^w">
                                                                                                                                                                                                                                <field name="value">&lt;td&gt;${temp[i]}&lt;/td&gt;</field>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                        <statement name="else">
                                                                                                                                                                                                                          <block type="partial_explanation" id="9UH,EYq972)1TY?FN2T^" inline="true">
                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                              <block type="string_value" id="dbV(MJ9G6%_qQ;Kn!:7!">
                                                                                                                                                                                                                                <field name="value">&lt;td&gt;&lt;/td&gt;</field>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                          <block type="variable" id="2+C)HzJC3dA{#4WuQ`[k">
                                                                                                                                                                                                                            <field name="name">i</field>
                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                              <block type="expression" id="d,:@1-^)9X7G(#@2rss({t">
                                                                                                                                                                                                                                <field name="value">i-1</field>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                      <block type="partial_explanation" id="{-hUF{(i#,Gfj9NB!{.i" inline="true">
                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                          <block type="string_value" id="b4mY(|_/Vem;FA8QUg(e">
                                                                                                                                                                                                                            <field name="value">&lt;/tr&gt;&lt;tr&gt;</field>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                          <block type="partial_explanation" id="$QNHt/$}cy.c,|Nm7K{K" inline="true">
                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                              <block type="string_value" id="$ZpWhd=,+Nc4tC|48UOG">
                                                                                                                                                                                                                                <field name="value">&lt;td&gt;&lt;/td&gt;</field>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                              <block type="variable" id="E3:vX7`s`[0wRKDpqRgy">
                                                                                                                                                                                                                                <field name="name">i</field>
                                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                                  <block type="expression" id="AAE9R,3NbIaq?}3WjF.5">
                                                                                                                                                                                                                                    <field name="value">6</field>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                  <block type="while_do_block" id="o`Bjns@25.$Lw;XWV-JmU">
                                                                                                                                                                                                                                    <value name="while">
                                                                                                                                                                                                                                      <block type="expression" id="kW4o#HKiHJ6%T|/;R^Iu">
                                                                                                                                                                                                                                        <field name="value">i&gt;=0</field>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                    <statement name="do">
                                                                                                                                                                                                                                      <block type="if_then_else_block" id="yD-n0g2f4@1zk^|i:jv`{">
                                                                                                                                                                                                                                        <value name="if">
                                                                                                                                                                                                                                          <block type="expression" id="0j8NF#?=h{%4W!`B,M,R">
                                                                                                                                                                                                                                            <field name="value">D[i] == -1</field>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                        <statement name="then">
                                                                                                                                                                                                                                          <block type="partial_explanation" id="7{%!E57SrZa:HUY;xcPQ" inline="true">
                                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                                              <block type="string_value" id=":z%)`}tEn(mrJ{vt5d@1G">
                                                                                                                                                                                                                                                <field name="value">&lt;td&gt;&lt;/td&gt;</field>
                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                                        <statement name="else">
                                                                                                                                                                                                                                          <block type="partial_explanation" id="PqMg5B^lkFev#?)j3@2Ru" inline="true">
                                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                                              <block type="string_value" id="{B=.Kx;A3~a5sa@2Q4FaP">
                                                                                                                                                                                                                                                <field name="value">&lt;td style='color: blue'&gt;${D[i]}&lt;/td&gt;</field>
                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                          <block type="variable" id="bDQ3Lru;e2new;D(CeBA">
                                                                                                                                                                                                                                            <field name="name">i</field>
                                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                                              <block type="expression" id="wpc5]Lp;bbKUU22;_}^W">
                                                                                                                                                                                                                                                <field name="value">i-1</field>
                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                      <block type="partial_explanation" id="{.hODj4Xx}]gw7zu(!qq" inline="true">
                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                          <block type="string_value" id="$I.o4vPytI7N@1s[DGu)o">
                                                                                                                                                                                                                                            <field name="value">&lt;/tr&gt;&lt;tr&gt;</field>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                          <block type="partial_explanation" id="[i1b2nSQC}7Hg(TI3_j2" inline="true">
                                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                                              <block type="string_value" id="Mv8U;]cr$IFE=n@11BLNY">
                                                                                                                                                                                                                                                <field name="value">&lt;td rowspan='2'&gt;-&lt;/td&gt;</field>
                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                              <block type="variable" id="QXCKB-QY/R:eg470bKkZ">
                                                                                                                                                                                                                                                <field name="name">i</field>
                                                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                                                  <block type="expression" id="X)sN]2YO.7c]l!$)BNg[">
                                                                                                                                                                                                                                                    <field name="value">6</field>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                                  <block type="while_do_block" id="kaeQ+;{36E~6x6XEu=!w">
                                                                                                                                                                                                                                                    <value name="while">
                                                                                                                                                                                                                                                      <block type="expression" id="BNQ2)!H5xjjuN^NqygKJ">
                                                                                                                                                                                                                                                        <field name="value">i&gt;=0</field>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                    <statement name="do">
                                                                                                                                                                                                                                                      <block type="if_then_else_block" id="_I@1p9-_8z8IQcN#@19MGE">
                                                                                                                                                                                                                                                        <value name="if">
                                                                                                                                                                                                                                                          <block type="expression" id="d7P2z;]bjcc@2c_8Zf::y">
                                                                                                                                                                                                                                                            <field name="value">i == 6 || (i == 5 &amp;&amp; A[i] == 0)</field>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                        <statement name="then">
                                                                                                                                                                                                                                                          <block type="partial_explanation" id="2F_r^{MVn];+/#l;M4te" inline="true">
                                                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                                                              <block type="string_value" id="2VCLcy)4k$4x_yD.lE.7">
                                                                                                                                                                                                                                                                <field name="value">&lt;td&gt;&lt;/td&gt;</field>
                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                                                        <statement name="else">
                                                                                                                                                                                                                                                          <block type="if_then_else_block" id="Hi=MW(Yb]Qoqe7IE^tc9">
                                                                                                                                                                                                                                                            <value name="if">
                                                                                                                                                                                                                                                              <block type="expression" id="g7`OJl}qPn@1GYK6|QIOM">
                                                                                                                                                                                                                                                                <field name="value">D[i] == -1</field>
                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                            <statement name="then">
                                                                                                                                                                                                                                                              <block type="partial_explanation" id="uN(jX0%7M6z.YZ}K!m6[" inline="true">
                                                                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                                                                  <block type="string_value" id="u1`tn!gjb#vS5sYDqkAY">
                                                                                                                                                                                                                                                                    <field name="value">&lt;td&gt;${A[i]}&lt;/td&gt;</field>
                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                                                                            <statement name="else">
                                                                                                                                                                                                                                                              <block type="partial_explanation" id="fOYNoh/#H/7t?rSRPE@1l" inline="true">
                                                                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                                                                  <block type="string_value" id="0,K$1A9af/ecbn^x5ma$">
                                                                                                                                                                                                                                                                    <field name="value">&lt;td&gt;&lt;strike&gt;${A[i]}&lt;/strike&gt;&lt;/td&gt;</field>
                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                                          <block type="variable" id="K=WEU5%,pv1I)kGkjfa[">
                                                                                                                                                                                                                                                            <field name="name">i</field>
                                                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                                                              <block type="expression" id="my/,RhqF@26x48z+b:gY%">
                                                                                                                                                                                                                                                                <field name="value">i-1</field>
                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                                      <block type="partial_explanation" id="mTdkmDmx1}c)7B`]S%b:" inline="true">
                                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                                          <block type="string_value" id="@2)v|3S?GYW8a216SL%s@2">
                                                                                                                                                                                                                                                            <field name="value">&lt;/tr&gt;&lt;tr&gt;</field>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                                          <block type="variable" id="vNCW{hrW=N+t/xRK^]RR">
                                                                                                                                                                                                                                                            <field name="name">i</field>
                                                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                                                              <block type="expression" id="@1OXk2z_LS)?KRgyK=_R,">
                                                                                                                                                                                                                                                                <field name="value">6</field>
                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                                              <block type="while_do_block" id="M[?#,{Ga9{ZwFM2Pp3s=">
                                                                                                                                                                                                                                                                <value name="while">
                                                                                                                                                                                                                                                                  <block type="expression" id="8_]|[PEs.JeW:?[AA|/D">
                                                                                                                                                                                                                                                                    <field name="value">i&gt;=0</field>
                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                                <statement name="do">
                                                                                                                                                                                                                                                                  <block type="if_then_else_block" id="^+A8##zfiJu2kXCi}DsK">
                                                                                                                                                                                                                                                                    <value name="if">
                                                                                                                                                                                                                                                                      <block type="expression" id=")+c#{?05A}@2:kS-?):@1,">
                                                                                                                                                                                                                                                                        <field name="value">i == 6 || (B[i] == 0 &amp;&amp; i ==5)</field>
                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                                    <statement name="then">
                                                                                                                                                                                                                                                                      <block type="partial_explanation" id="tZlluDanX-4%ngci#/0~" inline="true">
                                                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                                                          <block type="string_value" id="nREpR_=|1cI!g9Czsd66">
                                                                                                                                                                                                                                                                            <field name="value">&lt;td&gt;&lt;/td&gt;</field>
                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                                                                                    <statement name="else">
                                                                                                                                                                                                                                                                      <block type="partial_explanation" id="MIZ{XaovNrel)m5~{t-3" inline="true">
                                                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                                                          <block type="string_value" id="^vWWb!UPD%N!nu3Wg62v">
                                                                                                                                                                                                                                                                            <field name="value">&lt;td&gt;${B[i]}&lt;/td&gt;</field>
                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                                                      <block type="variable" id="C~:iBbG_fW)@2=UF(8[oF">
                                                                                                                                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                                                          <block type="expression" id="h0yEa~dds8bfHyouK_64">
                                                                                                                                                                                                                                                                            <field name="value">i-1</field>
                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                                                  <block type="partial_explanation" id="DVQY{bQ{C/al8DI~bMRf" inline="true">
                                                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                                                      <block type="string_value" id="LzN8@1`={[oVTE#RHr`a3">
                                                                                                                                                                                                                                                                        <field name="value">&lt;/tr&gt;&lt;tr&gt;</field>
                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                                                      <block type="partial_explanation" id="z@1ty?#3~5`@1x6AgD(hBt" inline="true">
                                                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                                                          <block type="string_value" id="/e-aXti6Bu(d()}U.ji(">
                                                                                                                                                                                                                                                                            <field name="value">&lt;td&gt;Answer:&lt;/td&gt;</field>
                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                                        <next>
                                                                                                                                                                                                                                                                          <block type="variable" id="+I}H%)y9AJ^auV?Fp7NA">
                                                                                                                                                                                                                                                                            <field name="name">i</field>
                                                                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                                                                              <block type="expression" id="tKz}54_%V~qR;u+)!f%}">
                                                                                                                                                                                                                                                                                <field name="value">6</field>
                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                                            <next>
                                                                                                                                                                                                                                                                              <block type="while_do_block" id="U|C{L,k%#l$o)xdGBHPu">
                                                                                                                                                                                                                                                                                <value name="while">
                                                                                                                                                                                                                                                                                  <block type="expression" id="+Q8%~;5-%i!eVCLQ;o1_">
                                                                                                                                                                                                                                                                                    <field name="value">i&gt;=0</field>
                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                                                <statement name="do">
                                                                                                                                                                                                                                                                                  <block type="if_then_else_block" id="c9U:?rojF(|(X.c(}0J=">
                                                                                                                                                                                                                                                                                    <value name="if">
                                                                                                                                                                                                                                                                                      <block type="expression" id="h_oSgv66lgY0O=]=e=Q,">
                                                                                                                                                                                                                                                                                        <field name="value">i == 6 &amp;&amp; C[i] == 0</field>
                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                                                    <statement name="then">
                                                                                                                                                                                                                                                                                      <block type="partial_explanation" id="eVgI~V6|hu{#]F(h@2i%v" inline="true">
                                                                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                                                                          <block type="string_value" id="O+7;6GFDec/z]ipx#(cZ">
                                                                                                                                                                                                                                                                                            <field name="value">&lt;td style='border-top: 2px solid black;'&gt;&lt;/td&gt;</field>
                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                                                                                                    <statement name="else">
                                                                                                                                                                                                                                                                                      <block type="if_then_else_block" id="2HlM{Lcy2@19jdvJ!X0x+">
                                                                                                                                                                                                                                                                                        <value name="if">
                                                                                                                                                                                                                                                                                          <block type="expression" id="/yj2n`x`_#U.#At;0x+R">
                                                                                                                                                                                                                                                                                            <field name="value">C[6] == 0 &amp;&amp; C[i] == 0 &amp;&amp; i == 5</field>
                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                                                        <statement name="then">
                                                                                                                                                                                                                                                                                          <block type="partial_explanation" id="|tz7;w;sUN{-CIPT{dZ}" inline="true">
                                                                                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                                                                                              <block type="string_value" id="7.9FmzygrYqks!+(,]X/">
                                                                                                                                                                                                                                                                                                <field name="value">&lt;td style='border-top: 2px solid black;'&gt;&lt;/td&gt;</field>
                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                                                                                        <statement name="else">
                                                                                                                                                                                                                                                                                          <block type="if_then_else_block" id="HF)!TG$3q%OUsUVZPnf?">
                                                                                                                                                                                                                                                                                            <value name="if">
                                                                                                                                                                                                                                                                                              <block type="expression" id="jw~e;=^KS(t|JbY,Vm.#">
                                                                                                                                                                                                                                                                                                <field name="value">C[6] == 0 &amp;&amp; C[5] == 0 &amp;&amp; C[i] == 0 &amp;&amp; i == 4</field>
                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                                                            <statement name="then">
                                                                                                                                                                                                                                                                                              <block type="partial_explanation" id="/?B.x]}wgUTIPzEa.+{0" inline="true">
                                                                                                                                                                                                                                                                                                <value name="value">
                                                                                                                                                                                                                                                                                                  <block type="string_value" id="pVNJ;uCH^:]xhO$5,pS-">
                                                                                                                                                                                                                                                                                                    <field name="value">&lt;td style='border-top: 2px solid black;'&gt;&lt;/td&gt;</field>
                                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                                                                                                            <statement name="else">
                                                                                                                                                                                                                                                                                              <block type="if_then_else_block" id="9L(~Rt2._QNCF[vpiw^2">
                                                                                                                                                                                                                                                                                                <value name="if">
                                                                                                                                                                                                                                                                                                  <block type="expression" id="sG;5n6u4AT)Df!q4J1Dy">
                                                                                                                                                                                                                                                                                                    <field name="value">C[6] == 0 &amp;&amp; C[5]==0 &amp;&amp; C[4] == 0 &amp;&amp; C[i] == 0 &amp;&amp; i == 3</field>
                                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                                </value>
                                                                                                                                                                                                                                                                                                <statement name="then">
                                                                                                                                                                                                                                                                                                  <block type="partial_explanation" id=":Im()/L^:$PbGltneNZn" inline="true">
                                                                                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                                                                                      <block type="string_value" id="`)Va^gF#ZLv;0ET~T2Y3">
                                                                                                                                                                                                                                                                                                        <field name="value">&lt;td style='border-top: 2px solid black;'&gt;&lt;/td&gt;</field>
                                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                                                                                                                <statement name="else">
                                                                                                                                                                                                                                                                                                  <block type="if_then_else_block" id=")aA3Sol}b6E$U=56Z7HG">
                                                                                                                                                                                                                                                                                                    <value name="if">
                                                                                                                                                                                                                                                                                                      <block type="expression" id="K=/_-{:[D+sTfjUHjDQ;">
                                                                                                                                                                                                                                                                                                        <field name="value">C[6] == 0 &amp;&amp; C[5]==0 &amp;&amp; C[4] == 0 &amp;&amp; C[3] == 0 &amp;&amp; C[i] == 0 &amp;&amp; i == 2</field>
                                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                                                                    <statement name="then">
                                                                                                                                                                                                                                                                                                      <block type="partial_explanation" id="S+/d?#=7DX]+%.lB[a-A" inline="true">
                                                                                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                                                                                          <block type="string_value" id="4IeZq-W^vDV-0m5wiGWu">
                                                                                                                                                                                                                                                                                                            <field name="value">&lt;td style='border-top: 2px solid black;'&gt;&lt;/td&gt;</field>
                                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                                                                                                                    <statement name="else">
                                                                                                                                                                                                                                                                                                      <block type="if_then_else_block" id="S9Q2wZHGP3hyC}q5(wPM">
                                                                                                                                                                                                                                                                                                        <value name="if">
                                                                                                                                                                                                                                                                                                          <block type="expression" id="hhyG]85w_@1!/xi+piozb">
                                                                                                                                                                                                                                                                                                            <field name="value">C[6] == 0 &amp;&amp; C[5]==0 &amp;&amp; C[4] == 0 &amp;&amp; C[3] == 0 &amp;&amp; C[2] == 0 &amp;&amp; C[i] == 0 &amp;&amp; i == 1</field>
                                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                                                                        <statement name="then">
                                                                                                                                                                                                                                                                                                          <block type="partial_explanation" id="T~1R7IJhmBR+b-L=Jjhy" inline="true">
                                                                                                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                                                                                                              <block type="string_value" id="RCZV[TSf3iMnU-@1_x[;U">
                                                                                                                                                                                                                                                                                                                <field name="value">&lt;td style='border-top: 2px solid black;'&gt;&lt;/td&gt;</field>
                                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                                                                                                        <statement name="else">
                                                                                                                                                                                                                                                                                                          <block type="partial_explanation" id="K=AOIEWRN2NQsr/m9}nU" inline="true">
                                                                                                                                                                                                                                                                                                            <value name="value">
                                                                                                                                                                                                                                                                                                              <block type="string_value" id="@2Rkg^N[F@11b3JO!L?8R+">
                                                                                                                                                                                                                                                                                                                <field name="value">&lt;td  style='border-top: 2px solid black;'&gt;${C[i]}&lt;/td&gt;</field>
                                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                                            </value>
                                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                                            </statement>
                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                        </statement>
                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                    </statement>
                                                                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                                                                      <block type="variable" id="=uu#m7{RF2sCXIhUBKoH">
                                                                                                                                                                                                                                                                                        <field name="name">i</field>
                                                                                                                                                                                                                                                                                        <value name="value">
                                                                                                                                                                                                                                                                                          <block type="expression" id="`$}Aa_L_z!f`?=v(]6$l">
                                                                                                                                                                                                                                                                                            <field name="value">i-1</field>
                                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                                        </value>
                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                </statement>
                                                                                                                                                                                                                                                                                <next>
                                                                                                                                                                                                                                                                                  <block type="partial_explanation" id="%}bgHU!:NE?Vh7J@22pr(" inline="true">
                                                                                                                                                                                                                                                                                    <value name="value">
                                                                                                                                                                                                                                                                                      <block type="string_value" id="3m[q=x%E:tT8BM{?Qji@2">
                                                                                                                                                                                                                                                                                        <field name="value">&lt;/tr&gt;&lt;/table&gt;&lt;center&gt;</field>
                                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                                    </value>
                                                                                                                                                                                                                                                                                    <next>
                                                                                                                                                                                                                                                                                      <block type="end_partial_explanation" id="`ji5YI)v}PEAXtDUIne2"></block>
                                                                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                                </next>
                                                                                                                                                                                                                              </block>
                                                                                                                                                                                                                            </next>
                                                                                                                                                                                                                          </block>
                                                                                                                                                                                                                        </next>
                                                                                                                                                                                                                      </block>
                                                                                                                                                                                                                    </next>
                                                                                                                                                                                                                  </block>
                                                                                                                                                                                                                </next>
                                                                                                                                                                                                              </block>
                                                                                                                                                                                                            </next>
                                                                                                                                                                                                          </block>
                                                                                                                                                                                                        </next>
                                                                                                                                                                                                      </block>
                                                                                                                                                                                                    </next>
                                                                                                                                                                                                  </block>
                                                                                                                                                                                                </next>
                                                                                                                                                                                              </block>
                                                                                                                                                                                            </next>
                                                                                                                                                                                          </block>
                                                                                                                                                                                        </next>
                                                                                                                                                                                      </block>
                                                                                                                                                                                    </next>
                                                                                                                                                                                  </block>
                                                                                                                                                                                </next>
                                                                                                                                                                              </block>
                                                                                                                                                                            </next>
                                                                                                                                                                          </block>
                                                                                                                                                                        </next>
                                                                                                                                                                      </block>
                                                                                                                                                                    </next>
                                                                                                                                                                  </block>
                                                                                                                                                                </next>
                                                                                                                                                              </block>
                                                                                                                                                            </next>
                                                                                                                                                          </block>
                                                                                                                                                        </next>
                                                                                                                                                      </block>
                                                                                                                                                    </next>
                                                                                                                                                  </block>
                                                                                                                                                </next>
                                                                                                                                              </block>
                                                                                                                                            </next>
                                                                                                                                          </block>
                                                                                                                                        </next>
                                                                                                                                      </block>
                                                                                                                                    </next>
                                                                                                                                  </block>
                                                                                                                                </next>
                                                                                                                              </block>
                                                                                                                            </next>
                                                                                                                          </block>
                                                                                                                        </next>
                                                                                                                      </block>
                                                                                                                    </next>
                                                                                                                  </block>
                                                                                                                </next>
                                                                                                              </block>
                                                                                                            </next>
                                                                                                          </block>
                                                                                                        </next>
                                                                                                      </block>
                                                                                                    </next>
                                                                                                  </block>
                                                                                                </next>
                                                                                              </block>
                                                                                            </next>
                                                                                          </block>
                                                                                        </next>
                                                                                      </block>
                                                                                    </next>
                                                                                  </block>
                                                                                </next>
                                                                              </block>
                                                                            </next>
                                                                          </block>
                                                                        </next>
                                                                      </block>
                                                                    </next>
                                                                  </block>
                                                                </next>
                                                              </block>
                                                            </next>
                                                          </block>
                                                        </next>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </next>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </next>
      </block>
    </statement>
  </block>
  <block type="prop_grid_cell_template_for" id="}SJUwl|(DX1A+ihi(2iK" x="894" y="4000">
    <field name="variable">$cell</field>
    <field name="condition">$cell.row == 3</field>
    <field name="#prop">cell.templates[]</field>
    <field name="#callback">$cell</field>
    <statement name="body">
      <block type="image_shape" id="72@2WjPuHGX_}4M8m)[so">
        <statement name="#props">
          <block type="prop_position" id="]l0Ncy{?;(rbFas:_XPv">
            <field name="#prop"></field>
            <value name="x">
              <block type="expression" id="FD{%vj7KA7s0zZqe_i.p">
                <field name="value">$cell.centerX</field>
              </block>
            </value>
            <value name="y">
              <block type="expression" id="n+al(MpDQDl%ziH+Pc;E">
                <field name="value">$cell.centerY</field>
              </block>
            </value>
            <next>
              <block type="prop_size" id="G-(m;%+(97mUt(]#BWz@2">
                <field name="#prop"></field>
                <value name="width">
                  <block type="expression" id="w=a((HWc#8GB5=ySH^EK">
                    <field name="value">300</field>
                  </block>
                </value>
                <value name="height">
                  <block type="expression" id="r@10wTIF5FH8,Z=dCZ;WC">
                    <field name="value">0</field>
                  </block>
                </value>
                <next>
                  <block type="prop_image_key" id="oO[;[u@2Hw5Qm?2E[0up9">
                    <field name="#prop"></field>
                    <value name="key">
                      <block type="string_value" id="{-rb/i3EhnKr?1SFBu0p">
                        <field name="value">shape.png</field>
                      </block>
                    </value>
                    <next>
                      <block type="prop_image_src" id="GM08b`-EnNK0@2@1h;@1LZI">
                        <field name="#prop"></field>
                        <value name="src">
                          <block type="string_value" id="J2GN?Lo?bw!xj7.~!(0g">
                            <field name="value">${image_path}shape.png</field>
                          </block>
                        </value>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </next>
          </block>
        </statement>
        <next>
          <block type="algorithmic_remainder_shape" id="3b7p/{q348!]SVr);BHA">
            <statement name="#props">
              <block type="prop_value" id="}a1/$9P}|oxJCzr=:.6U">
                <field name="#prop"></field>
                <value name="value">
                  <block type="expression" id="4{LUo(H;tOLgcCAQuC2w">
                    <field name="value">x - y</field>
                  </block>
                </value>
                <next>
                  <block type="prop_position" id="1T;Lu+WgsL|#1lF}N;--">
                    <field name="#prop"></field>
                    <value name="x">
                      <block type="expression" id=".Su1beT1ZbSgxGQc{cr#">
                        <field name="value">$cell.centerX</field>
                      </block>
                    </value>
                    <value name="y">
                      <block type="expression" id="29xb%ehD+H5]e-BqrHz]">
                        <field name="value">$cell.centerY</field>
                      </block>
                    </value>
                    <next>
                      <block type="prop_size" id="64n~FbM)L^+_0X!c%Z,W">
                        <field name="#prop"></field>
                        <value name="width">
                          <block type="expression" id="Qf`ER`~k^n;!DN7S5_dP">
                            <field name="value">300</field>
                          </block>
                        </value>
                        <value name="height">
                          <block type="expression" id="3=NKizx,]k`@2V@2ETj+oG">
                            <field name="value">60</field>
                          </block>
                        </value>
                        <next>
                          <block type="prop_input_keyboard" id="$(E|UuN=)=tDB5WY#9kh">
                            <field name="#prop"></field>
                            <field name="keyboard">numbers1</field>
                            <next>
                              <block type="prop_input_max_length" id="GKEtmR/3lp6AB0%^S}nh">
                                <field name="#prop"></field>
                                <value name="maxLength">
                                  <block type="expression" id="?FL`^GkfF|eCCVQ0slBo">
                                    <field name="value">(x+y).toString().length</field>
                                  </block>
                                </value>
                                <next>
                                  <block type="prop_input_result_position" id="I~pKAhmYVlt8BN62I@2^n">
                                    <field name="#prop"></field>
                                    <field name="resultPosition">bottom</field>
                                    <next>
                                      <block type="prop_stroke" id="+gwa#d;pT6sXaP/wi]nd">
                                        <field name="#prop">stroke</field>
                                        <next>
                                          <block type="prop_fill" id="36b6zu+dxt=r9U|%;5ho">
                                            <field name="#prop">fill</field>
                                            <next>
                                              <block type="prop_text_style" id="Hdwi^N7L!5@2JZY(GW|{{">
                                                <field name="base">remainder_input</field>
                                                <statement name="#props">
                                                  <block type="prop_text_style_font_size" id="#[bS+@2dTnYwBz.GBWU[f">
                                                    <field name="#prop"></field>
                                                    <value name="fontSize">
                                                      <block type="expression" id="Qu0l{`TL-oi@1MlH,%zZ2">
                                                        <field name="value">42</field>
                                                      </block>
                                                    </value>
                                                    <next>
                                                      <block type="prop_text_style_fill" id="!N3iZYDk~~sif~lVHoWR">
                                                        <field name="#prop"></field>
                                                        <value name="fill">
                                                          <block type="string_value" id="B%o~5F[q|vTZy_ZsjOPf">
                                                            <field name="value">black</field>
                                                          </block>
                                                        </value>
                                                      </block>
                                                    </next>
                                                  </block>
                                                </statement>
                                              </block>
                                            </next>
                                          </block>
                                        </next>
                                      </block>
                                    </next>
                                  </block>
                                </next>
                              </block>
                            </next>
                          </block>
                        </next>
                      </block>
                    </next>
                  </block>
                </next>
              </block>
            </statement>
          </block>
        </next>
      </block>
    </statement>
  </block>
</xml>
END_XML]] */